/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/FTDigit.h"
#include "FTDet/DeFTDetector.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/FTChannelID.h"
#include "Kernel/FastClusterContainer.h"
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <DetDesc/ConditionAccessorHolder.h>
#include <GaudiKernel/GaudiException.h>
#include <boost/range/irange.hpp>
#include <optional>

/** @class FTDigitMonitor FTDigitMonitor.cpp
 *
 *
 *  @author Lex Greeven, Sevda Esen
 *  @date   2020-02-17
 */

class FTDigitMonitor
    : public Gaudi::Functional::Consumer<void( const LHCb::FTDigit::FTDigits&, const DeFTDetector& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFTDetector>> {
public:
  FTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::FTDigit::FTDigits& digits, const DeFTDetector& ) const override;

private:
  void fillHistograms( const LHCb::FTDigit& Digit, const DeFTDetector& deFT ) const;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTDigitMonitor )

FTDigitMonitor::FTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"DigitLocation", LHCb::FTDigitLocation::Default},
                 KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}} ) {}
//=============================================================================
// Initialization
//=============================================================================
StatusCode FTDigitMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void FTDigitMonitor::operator()( const LHCb::FTDigit::FTDigits& DigitsCont, const DeFTDetector& deFT ) const {
  if ( deFT.version() < 61 ) {
    throw GaudiException( "This version requires FTDet v6.1 or higher", "", StatusCode::FAILURE );
  };
  for ( const auto& Digit : DigitsCont.range() ) { fillHistograms( Digit, deFT ); }
  plot( DigitsCont.size(), "nDigits", "Number of digits; Digits/event; Events", 0., 60e3, 50 );
}

void FTDigitMonitor::fillHistograms( const LHCb::FTDigit& Digit, const DeFTDetector& deFT ) const {

  plot( Digit.adcCount(), "ADCCounts", "Charge in 2bit ADC; Charge/digit [ADC]; Number of digits", -0.5, 3.5, 4 );

  // plot occupancy
  const LHCb::FTChannelID chanID = Digit.channelID();
  plot( (float)chanID.station(), "DigitsPerStation", "Digits per station; Station; Digits", 0.5, 3.5, 3 );
  plot( (float)chanID.module(), "DigitsPerModule", "Digits per module; Module; Digits", -0.5, 5.5, 6 );
  plot( (float)chanID.sipmInModule(), "DigitsPerSiPM", "Digits per SiPM; SiPMID; Digits", -0.5, 15.5, 16 );
  plot( (float)chanID.channel(), "DigitsPerChannel", "Digits per channel; Channel; Digits", -0.5, 127.5, 128 );

  const DeFTModule* module = deFT.findModule( chanID );
  if ( module != nullptr ) {
    int pseudoChannel = module->pseudoChannel( chanID );
    plot( pseudoChannel, "DigitsPerPseudoChannel", "Digits per pseudochannel;Pseudochannel;Digits/(64 channels)", 0.,
          12288., 192 );
  }
}

//=============================================================================
