/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include <DetDesc/ConditionAccessorHolder.h>
#include <GaudiCommonSvc/H1D.h>
#include <GaudiKernel/GaudiException.h>

// from LHCb
#include "Event/FTLiteCluster.h"
#include "FTDet/DeFTDetector.h"
#include "Kernel/FTChannelID.h"
#include "Kernel/FastClusterContainer.h"

#include "boost/range/combine.hpp"
#include <optional>

// from AIDA
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>

#include "FTNZSHist.h"

/** @class FTNZSClusterMonitor FTNZSClusterMonitor.cpp
 *
 *
 *  @author Lex Greeven
 *  @date   2020-04-09
 */
namespace {
  using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;
  using std::string;
  using std::to_string;
} // namespace

class FTNZSClusterMonitor
    : public Gaudi::Functional::Consumer<void( const FTLiteClusters&, const FTLiteClusters&, const DeFTDetector& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeFTDetector>> {
public:
  FTNZSClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void operator()( const FTLiteClusters& clustersNZS, const FTLiteClusters& clustersNZSdigits,
                   const DeFTDetector& deFT ) const override;

private:
  FTNZSHist createFTNZSHist( string histName, string histTitle, string xAxis, string yAxis, float xMin, float xMax,
                             int nBins );

  std::optional<FTNZSHist> m_nc_NZS;
  std::optional<FTNZSHist> m_lcps_NZS;
  std::optional<FTNZSHist> m_lcpm_NZS;
  std::optional<FTNZSHist> m_lcpsipm_NZS;
  std::optional<FTNZSHist> m_lcpc_NZS;
  std::optional<FTNZSHist> m_lcppc_NZS;
  std::optional<FTNZSHist> m_lcf_NZS;
  std::optional<FTNZSHist> m_lcs_NZS;
  std::optional<FTNZSHist> m_lcisipm_NZS;
  std::optional<FTNZSHist> m_lciq_NZS;

  // Histogram vectors for per station/quarter/module
  mutable std::vector<FTNZSHist> m_lcichpm_NZS;
  mutable std::vector<FTNZSHist> m_lcipchpq_NZS;
  mutable std::vector<FTNZSHist> m_sipmpq_NZS;
  mutable std::vector<FTNZSHist> m_fracpq_NZS;
  mutable std::vector<FTNZSHist> m_pseusizepq_NZS;
  mutable std::vector<FTNZSHist> m_lcpst_NZS;

  AIDA::IHistogram2D* m_lcsf_NZS           = nullptr;
  AIDA::IHistogram2D* m_lcsf_NZS_digits    = nullptr;
  AIDA::IHistogram2D* m_lcqsipm_NZS        = nullptr;
  AIDA::IHistogram2D* m_lcqsipm_NZS_digits = nullptr;

  // Properties for drawing histograms per X
  Gaudi::Property<bool> m_drawHistsPerStation{this, "DrawHistsPerStation", true,
                                              "Boolean for drawing histrograms per station"};
  Gaudi::Property<bool> m_drawHistsPerQuarter{this, "DrawHistsPerQuarter", true,
                                              "Boolean for drawing histrograms per quarter"};
  Gaudi::Property<bool> m_drawHistsPerModule{this, "DrawHistsPerModule", true,
                                             "Boolean for drawing histrograms per module"};
  Gaudi::Property<bool> m_drawHistsPerSiPM{this, "DrawHistsPerSiPM", true, "Boolean for drawing histrograms per SiPM"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTNZSClusterMonitor )

//=============================================================================
FTNZSClusterMonitor::FTNZSClusterMonitor( const string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocationNZS", LHCb::FTLiteClusterLocation::Default + "NZS"},
                 KeyValue{"ClusterLocationfromNZSdigits", LHCb::FTLiteClusterLocation::Default + "fromNZSdigits"},
                 KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default}} ) {}

//=============================================================================
StatusCode FTNZSClusterMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;

  // Overview histograms
  m_nc_NZS.emplace(
      createFTNZSHist( "NZSnClusters", "Number of NZS clusters", "Clusters/event", "Events", 0., 1000, 100 ) );
  m_lciq_NZS.emplace(
      createFTNZSHist( "NZSLiteClustersPerQuarter", "Number of NZS clusters", "Quarter", "Clusters", 0., 47., 47 ) );
  m_lcpm_NZS.emplace(
      createFTNZSHist( "NZSLiteClustersPerModule", "NZS clusters per module", "Module", "Clusters", -0.5, 5.5, 6 ) );
  m_lcps_NZS.emplace(
      createFTNZSHist( "NZSLiteClustersPerStation", "NZS clusters per station", "Station", "Clusters", 0.5, 3.5, 3 ) );
  m_lcpsipm_NZS.emplace(
      createFTNZSHist( "NZSLiteClustersPerSiPM", "NZS clusters per SiPM", "SiPMID", "Clusters", 0., 16., 16 ) );
  m_lcpc_NZS.emplace( createFTNZSHist( "NZSLiteClustersPerChannel", "NZS clusters per channel", "Channel", "Clusters",
                                       -0.5, 127.5, 128 ) );
  m_lcppc_NZS.emplace( createFTNZSHist( "NZSLiteClustersPerPseudoChannel", "NZS clusters per pseudo channel",
                                        "Pseudo channel", "Clusters/(64 channels)", 0., 12288., 192 ) );
  m_lcf_NZS.emplace(
      createFTNZSHist( "NZSLiteClusterFraction", "", "NZS cluster fraction", "Fraction", -0.25, 0.75, 2 ) );
  m_lcs_NZS.emplace( createFTNZSHist( "NZSLiteClusterSize", "", "NZS cluster size", "Size", -0.5, 8.5, 9 ) );
  m_lcisipm_NZS.emplace( createFTNZSHist( "NZSLiteClustersInSiPM", "NZS clusters per SiPM", "Number of clusters",
                                          "Number of SiPMs", -0.5, 20.5, 21 ) );

  // Per quarter: 16 * (3 - 1)(S) + 4 * 3 (L) * 3 (Q) = 47
  m_fracpq_NZS.reserve( 47 );
  m_pseusizepq_NZS.reserve( 47 );
  m_sipmpq_NZS.reserve( 47 );
  m_lcipchpq_NZS.reserve( 47 );
  // Per module: 6 * ( 4 * ( 4 * ( 3 - 1 (S) ) + 3 (L) ) + 3 (Q) ) + 5 = 287
  m_lcichpm_NZS.reserve( 287 );
  // Per station: 3 (S)
  m_lcpst_NZS.reserve( 3 );

  // Histograms per station/quarter/module
  size_t prev_stationID( 999 );
  size_t prev_quarter( 999 );
  for ( size_t station = 1; station < 4; station++ ) {
    for ( size_t layer = 0; layer < 4; layer++ ) {
      for ( size_t quarter = 0; quarter < 4; quarter++ ) {
        for ( size_t module = 0; module < 6; module++ ) {

          // Define the histograms per quarter if asked for
          if ( m_drawHistsPerQuarter && ( quarter != prev_quarter ) ) {
            string locationString = to_string( station ) + "L" + to_string( layer ) + "Q" + to_string( quarter );
            // Now create all hists
            m_fracpq_NZS.emplace_back( createFTNZSHist( "NZSFractionInQuarter_S" + locationString,
                                                        "NZS Fraction in quarter_S" + locationString, "Fraction",
                                                        "Number of clusters", -0.25, 0.75, 2 ) );
            m_pseusizepq_NZS.emplace_back( createFTNZSHist( "NZSPseudosizeInQuarter_S" + locationString,
                                                            "NZS Pseudosize in quarter_S" + locationString,
                                                            "Pseudosize", "Number of clusters", -0.5, 8.5, 9 ) );
            m_sipmpq_NZS.emplace_back( createFTNZSHist( "NZSClustersPerSiPMInQuarter_S" + locationString,
                                                        "NZS Clusters per SiPM in quarter_S" + locationString,
                                                        "SiPM number", "Number of clusters", 0., 96., 97 ) );
            m_lcipchpq_NZS.emplace_back(
                createFTNZSHist( "NZSClustersPerPseudochannelInQuarter_S" + locationString,
                                 "NZS Clusters per Pseudochannel in quarter_S" + locationString, "Pseudochannel",
                                 "Number of clusters/8 pseudochannels", 0., 12282., 1536 ) );
          }
          prev_quarter = quarter;

          // Histograms per module
          if ( m_drawHistsPerModule ) {
            string locationString = to_string( station ) + "L" + to_string( layer ) + "Q" + to_string( quarter ) + "M" +
                                    to_string( module );
            m_lcichpm_NZS.emplace_back( createFTNZSHist( "NZSLiteClustersPerChannelinModuleM_S" + locationString,
                                                         "NZS Clusters Per Channel in Module_S" + locationString,
                                                         "Channel number", "Number of clusters", 0., 2047., 2048 ) );
          }

          // Histograms per station
          if ( m_drawHistsPerStation && ( station != prev_stationID ) ) {
            m_lcpst_NZS.emplace_back( createFTNZSHist( "NZSLiteClustersInStation_S" + to_string( station ),
                                                       "NZS Clusters In Station " + to_string( station ), "Module ID",
                                                       "Number of clusters", 0., 96., 97 ) );
          } // m_drawHistsPerStation
          prev_stationID = station;
        } // module loop
      }   // quarter loop
    }     // layer loop
  }       // station loop

  // 2D histograms
  m_lcsf_NZS = book2D( "NZSLiteClusterSizeVsFraction", "NZS Cluster size vs fraction ; Size; Fraction", -0.5, 8.5, 9,
                       -0.25, 0.75, 100 );
  declareInfo( "NZSLiteClusterSizeVsFraction", m_lcsf_NZS, "NZS Cluster size vs fraction ; Size; Fraction" );
  m_lcsf_NZS_digits = book2D( "NZSDigitLiteClusterSizeVsFraction",
                              "NZS digit Cluster size vs fraction ; Size; Fraction", -0.5, 8.5, 9, -0.25, 0.75, 100 );
  declareInfo( "NZSDigitLiteClusterSizeVsFraction", m_lcsf_NZS_digits,
               "NZS digit Cluster size vs fraction ; Size; Fraction" );

  m_lcqsipm_NZS = book2D( "NZSQuarterVsSiPMNumber", "NZS Quarter ID vs SiPMNumber ; Quarter ID; SiPM number", 0., 47.,
                          48, 0., 115., 116 );
  declareInfo( "NZSQuarterVsSiPMNumber", m_lcqsipm_NZS, "NZS Quarter ID vs SiPM Number ; Quarter ID; SiPM number" );
  m_lcqsipm_NZS_digits =
      book2D( "NZSDigitQuarterVsSiPMNumber", "NZS digit Quarter ID vs SiPMNumber ; Quarter ID; SiPM number", 0., 47.,
              48, 0., 115., 116 );
  declareInfo( "NZSDigitQuarterVsSiPMNumber", m_lcqsipm_NZS_digits,
               "NZS digit Quarter ID vs SiPM Number ; Quarter ID; SiPM number" );
  return sc;
}

//=============================================================================
void FTNZSClusterMonitor::operator()( const FTLiteClusters& clustersNZS, const FTLiteClusters& clustersNZSdigits,
                                      const DeFTDetector& deFT ) const {

  if ( deFT.version() < 61 ) {
    throw GaudiException( "This version requires FTDet v6.1 or higher", "", StatusCode::FAILURE );
  };

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Filling the histograms" << endmsg;
  m_nc_NZS->fillNZS( clustersNZS.size() );
  m_nc_NZS->fillDigit( clustersNZSdigits.size() );
  std::optional<uint> prevSiPM, prevModuleID;
  int                 clustersInSiPM = 0;

  // Loop over NZS FTLiteCluster
  for ( const auto& cluster : clustersNZS.range() ) {
    LHCb::FTChannelID chanID = cluster.channelID();
    const DeFTModule* mod    = deFT.findModule( chanID );

    size_t q_ID          = chanID.moniQuarterID();
    uint   SiPM_ID       = chanID.moniSiPMID();
    int    pseudoChannel = mod->pseudoChannel( chanID );

    // Fill cluster channel properties
    m_lcps_NZS->fillNZS( chanID.station() );
    m_lcpm_NZS->fillNZS( chanID.module() );
    m_lcpsipm_NZS->fillNZS( chanID.sipmInModule() );
    m_lcpc_NZS->fillNZS( chanID.channel() );
    if ( mod != nullptr ) { m_lcppc_NZS->fillNZS( pseudoChannel ); }
    m_lcf_NZS->fillNZS( cluster.fraction() );
    m_lcs_NZS->fillNZS( cluster.pseudoSize() );

    // 2D hists are not FTNZSHist objects, so "normal" fill
    m_lcsf_NZS->fill( cluster.pseudoSize(), cluster.fraction() );
    m_lcqsipm_NZS->fill( q_ID, SiPM_ID );
    if ( m_drawHistsPerQuarter ) {
      m_lciq_NZS->fillNZS( q_ID );
      m_fracpq_NZS.at( q_ID ).fillNZS( cluster.fraction() );
      m_pseusizepq_NZS.at( q_ID ).fillNZS( cluster.pseudoSize() );
      m_sipmpq_NZS.at( q_ID ).fillNZS( SiPM_ID );
      m_lcipchpq_NZS.at( q_ID ).fillNZS( pseudoChannel );
    }
    if ( m_drawHistsPerStation ) { m_lcpst_NZS.at( mod->stationID() - 1 ).fillNZS( chanID.moniModuleIDstation() ); }
    if ( m_drawHistsPerModule ) { m_lcichpm_NZS.at( chanID.moniModuleID() ).fillNZS( chanID.moniChannelID() ); }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if ( prevSiPM && thisSiPM != *prevSiPM && clustersInSiPM != 0 ) {
      m_lcisipm_NZS->fillNZS( clustersInSiPM );
      clustersInSiPM = 0;
    }
    prevSiPM     = thisSiPM;
    prevModuleID = chanID.moniModuleID();
    ++clustersInSiPM;

  } // Loop over NZS FTLiteClusters
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Done looping over NZS clusters!" << endmsg;
  m_lcisipm_NZS->fillNZS( clustersInSiPM ); // fill this for the last time

  // Now do the same for the NZS digits clusters
  clustersInSiPM = 0;
  for ( const auto& cluster : clustersNZSdigits.range() ) {
    LHCb::FTChannelID chanID = cluster.channelID();
    const DeFTModule* mod    = deFT.findModule( chanID );

    size_t q_ID          = chanID.moniQuarterID();
    uint   SiPM_ID       = chanID.moniSiPMID();
    int    pseudoChannel = mod->pseudoChannel( chanID );

    // Draw cluster channel properties
    m_lcps_NZS->fillDigit( chanID.station() );
    m_lcpm_NZS->fillDigit( chanID.module() );
    m_lcpsipm_NZS->fillDigit( chanID.sipmInModule() );
    m_lcpc_NZS->fillDigit( chanID.channel() );
    if ( mod != nullptr ) { m_lcppc_NZS->fillDigit( pseudoChannel ); }
    m_lcf_NZS->fillDigit( cluster.fraction() );
    m_lcs_NZS->fillDigit( cluster.pseudoSize() );

    // 2D hists are not FTNZSHist objects, so "normal" fill
    m_lcsf_NZS_digits->fill( cluster.pseudoSize(), cluster.fraction() );
    m_lcqsipm_NZS_digits->fill( q_ID, SiPM_ID );

    // Check if properties were set
    if ( m_drawHistsPerQuarter ) {
      m_lciq_NZS->fillDigit( q_ID );
      m_fracpq_NZS.at( q_ID ).fillDigit( cluster.fraction() );
      m_pseusizepq_NZS.at( q_ID ).fillDigit( cluster.pseudoSize() );
      m_sipmpq_NZS.at( q_ID ).fillDigit( SiPM_ID );
      m_lcipchpq_NZS.at( q_ID ).fillDigit( pseudoChannel );
    }
    if ( m_drawHistsPerStation ) { m_lcpst_NZS.at( mod->stationID() - 1 ).fillDigit( chanID.moniModuleIDstation() ); }
    if ( m_drawHistsPerModule ) { m_lcichpm_NZS.at( chanID.moniModuleID() ).fillDigit( chanID.moniChannelID() ); }

    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.uniqueSiPM();
    if ( prevSiPM && thisSiPM != *prevSiPM && clustersInSiPM != 0 ) {
      m_lcisipm_NZS->fillDigit( clustersInSiPM );
      clustersInSiPM = 0;
    }
    prevSiPM     = thisSiPM;
    prevModuleID = chanID.moniModuleID();
    ++clustersInSiPM;
  } // Loop over NZS digits FTLiteClusters
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Done looping over NZS from digits clusters!" << endmsg;
  m_lcisipm_NZS->fillDigit( clustersInSiPM ); // Fill this for the last time
}

//=============================================================================
// Function to book a NZS and NZS digits hist, return FTNZSHist object
FTNZSHist FTNZSClusterMonitor::createFTNZSHist( string histName, string histTitle, string xAxis, string yAxis,
                                                float xMin, float xMax, int nBins ) {

  std::string titleString = histTitle + "; " + xAxis + "; " + yAxis;

  // Book histograms and set info
  AIDA::IHistogram1D* histNZS = book1D( histName, titleString, xMin, xMax, nBins );
  declareInfo( histName, histNZS, titleString );
  AIDA::IHistogram1D* histDigits = book1D( "Digits" + histName, "Digits" + titleString, xMin, xMax, nBins );
  declareInfo( "Digits" + histName, histDigits, "Digits" + titleString );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Created FTNZSHist object for " << histName << endmsg;
  return {std::move( histName ),
          std::move( histTitle ),
          std::move( xAxis ),
          std::move( yAxis ),
          xMin,
          xMax,
          nBins,
          histNZS,
          histDigits};
}
