/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/bit_cast.h"
#include "PrKernel/PrMutUTHits.h"
#include "PrKernel/PrUTHitHandler.h"
#include "PrKernel/PrVeloUTTrack.h"
#include "PrKernel/UTHitInfo.h"
#include "PrUTMagnetTool.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTInfo.h"
#include "UTDet/DeUTSector.h"
#include "boost/container/small_vector.hpp"
#include "vdt/log.h"
#include "vdt/sqrt.h"
#include <numeric>

/** @class PrVeloUT PrVeloUT.h
 *
 *  PrVeloUT algorithm. This is just a wrapper,
 *  the actual pattern recognition is done in the 'PrVeloUTTool'.
 *
 *  - InputTracksName: Input location for Velo tracks
 *  - OutputTracksName: Output location for VeloTT tracks
 *  - TimingMeasurement: Do a timing measurement?
 *
 *  @author Mariusz Witek
 *  @date   2007-05-08
 *  @update for A-Team framework 2007-08-20 SHM
 *
 *  2017-03-01: Christoph Hasse (adapt to future framework)
 *  2019-04-26: Arthur Hennequin (change data Input/Output)
 *  2020-08-26: Peilian Li (change data Input/Output to SOACollection)
 */

namespace LHCb::Pr {

  constexpr static int batchSize     = align_size( 48 );
  constexpr static int maxNumCols    = 3;                       // if needed, algo can be templated with this
  constexpr static int maxNumRows    = 3;                       // if needed, algo can be templated with this
  constexpr static int maxNumSectors = maxNumCols * maxNumRows; // if needed, algo can be templated with this

  using simd   = SIMDWrapper::best::types;
  using scalar = SIMDWrapper::scalar::types;

  namespace MiniStateTag {
    struct StatePosition : V2::state_field {};
    struct StateQoP : V2::float_field {};
    struct p : V2::float_field {};
    struct index : V2::int_field {};

    template <typename T>
    using ministate_t = V2::SOACollection<T, StateQoP, p, index, StatePosition>;
  } // namespace MiniStateTag

  struct MiniStatesArray : MiniStateTag::ministate_t<MiniStatesArray> {
    using base_t = typename MiniStateTag::ministate_t<MiniStatesArray>;
    using base_t::base_t;
  };

  namespace ExtStateTag {
    struct xLayer : V2::float_field {};
    struct yLayer : V2::float_field {};
    struct xTol : V2::float_field {};
    struct tx : V2::float_field {};

    template <typename T>
    using extstate_t = V2::SOACollection<T, xLayer, yLayer, xTol, tx>;
  } // namespace ExtStateTag

  struct ExtrapolatedStates : ExtStateTag::extstate_t<ExtrapolatedStates> {
    using base_t = typename ExtStateTag::extstate_t<ExtrapolatedStates>;
    using base_t::base_t;
  };

  namespace BoundariesTag {
    struct sects : V2::ints_field<maxNumSectors> {};
    struct xTol : V2::float_field {};
    struct nPos : V2::int_field {};

    template <typename T>
    using boundary_t = V2::SOACollection<T, sects, xTol, nPos>;
  } // namespace BoundariesTag

  struct Boundaries : BoundariesTag::boundary_t<Boundaries> {
    using base_t = typename BoundariesTag::boundary_t<Boundaries>;
    using base_t::base_t;
  };

  /// template.. maybe not needed ???
  namespace pTrackTag {
    constexpr static int maxHits = 4; // no overlap hits, only 4 per track

    // -- this is for the hits
    // -- this does _not_ include overlap hits, so only 4 per track
    struct xs : V2::floats_field<maxHits> {};
    struct zs : V2::floats_field<maxHits> {};
    struct weights : V2::floats_field<maxHits> {};
    struct sins : V2::floats_field<maxHits> {};
    struct ids : V2::ints_field<maxHits> {};
    struct hitIndexs : V2::ints_field<maxHits> {};

    // -- this is the output of the fit
    struct qps : V2::float_field {};
    struct chi2TTs : V2::float_field {};
    struct xTTs : V2::float_field {};
    struct xSlopeTTs : V2::float_field {};
    struct ys : V2::float_field {};

    // -- and this the original state (in the Velo)
    struct StatePosition : V2::state_field {};
    struct indexs : V2::int_field {};
    struct hitContIndexs : V2::int_field {};

    template <typename T>
    using extstate_t = V2::SOACollection<T, xs, zs, weights, sins, ids, hitIndexs, qps, chi2TTs, xTTs, xSlopeTTs, ys,
                                         StatePosition, indexs, hitContIndexs>;
  } // namespace pTrackTag

  struct ProtoTracks final {

    std::array<float, simd::size> wbs;
    std::array<float, simd::size> xMidFields;
    std::array<float, simd::size> invKinkVeloDists;

    // -- this is for the hits
    // -- this does _not_ include overlap hits, so only 4 per track
    std::array<float, 4 * batchSize> xs;
    std::array<float, 4 * batchSize> zs;
    std::array<float, 4 * batchSize> weightss{}; // this needs to be zero-initialized
    std::array<float, 4 * batchSize> sins;
    std::array<int, 4 * batchSize>   ids;
    std::array<int, 4 * batchSize>   hitIndexs{-1};

    // -- this is the output of the fit
    std::array<float, batchSize> qps;
    std::array<float, batchSize> chi2TTs;
    std::array<float, batchSize> xTTs;
    std::array<float, batchSize> xSlopeTTs;
    std::array<float, batchSize> ys;

    // -- and this the original state (in the Velo)
    std::array<float, 3 * batchSize> statePoss;
    std::array<float, 2 * batchSize> stateDirs;
    std::array<int, batchSize>       indexs;

    // -- and this an index to find the hit containers
    std::array<int, batchSize> hitContIndexs;

    std::size_t size{0};
    SOA_ACCESSOR_VAR( x, &( xs[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( z, &( zs[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( weight, &( weightss[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( sin, &( sins[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( id, &( ids[pos * batchSize] ), int pos )
    SOA_ACCESSOR_VAR( hitIndex, &( hitIndexs[pos * batchSize] ), int pos )

    SOA_ACCESSOR( qp, qps.data() )
    SOA_ACCESSOR( chi2TT, chi2TTs.data() )
    SOA_ACCESSOR( xTT, xTTs.data() )
    SOA_ACCESSOR( xSlopeTT, xSlopeTTs.data() )
    SOA_ACCESSOR( y, ys.data() )

    SOA_ACCESSOR( index, indexs.data() )
    SOA_ACCESSOR( hitContIndex, hitContIndexs.data() )
    VEC3_SOA_ACCESSOR( pos, (float*)&( statePoss[0] ), (float*)&( statePoss[batchSize] ),
                       (float*)&( statePoss[2 * batchSize] ) )
    VEC3_XY_SOA_ACCESSOR( dir, (float*)&( stateDirs[0] ), (float*)&( stateDirs[batchSize] ), 1.0f )

    SOA_ACCESSOR( wb, wbs.data() )
    SOA_ACCESSOR( xMidField, xMidFields.data() )
    SOA_ACCESSOR( invKinkVeloDist, invKinkVeloDists.data() )

    template <typename dType>
    void fillHelperParams( Vec3<typename dType::float_v> pos, Vec3<typename dType::float_v> dir, const float zKink,
                           const float sigmaVeloSlope ) {

      using F = typename dType::float_v;

      F( pos.x + dir.x * ( zKink - pos.z ) ).store( xMidFields.data() );
      F a = sigmaVeloSlope * ( zKink - pos.z );
      F( 1.0f / ( a * a ) ).store( wbs.data() );
      F( 1.0f / ( zKink - pos.z ) ).store( invKinkVeloDists.data() );
    }
  };

  class VeloUT : public Gaudi::Functional::Transformer<Upstream::Tracks( const EventContext&, const Velo::Tracks&,
                                                                         const LHCb::Pr::UT::HitHandler&,
                                                                         const UTDAQ::GeomCache& ),
                                                       LHCb::DetDesc::usesConditions<UTDAQ::GeomCache>> {

  public:
    /// Standard constructor
    VeloUT( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;

    Upstream::Tracks operator()( const EventContext&, const Velo::Tracks&, const LHCb::Pr::UT::HitHandler&,
                                 const UTDAQ::GeomCache& ) const override final;

  private:
    Gaudi::Property<float> m_minMomentum{this, "minMomentum", 1.5 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minPT{this, "minPT", 0.3 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minMomentumFinal{this, "minMomentumFinal", 2.5 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_minPTFinal{this, "minPTFinal", 0.425 * Gaudi::Units::GeV};
    Gaudi::Property<float> m_maxPseudoChi2{this, "maxPseudoChi2", 1280.};
    Gaudi::Property<float> m_yTol{this, "YTolerance", 0.5 * Gaudi::Units::mm}; // 0.8
    Gaudi::Property<float> m_yTolSlope{this, "YTolSlope", 0.08};               // 0.2
    Gaudi::Property<float> m_hitTol{this, "HitTol", 0.8 * Gaudi::Units::mm};   // 0.8
    Gaudi::Property<float> m_deltaTx{this, "DeltaTx", 0.018};                  // 0.02
    Gaudi::Property<float> m_maxXSlope{this, "MaxXSlope", 0.350};
    Gaudi::Property<float> m_maxYSlope{this, "MaxYSlope", 0.300};
    Gaudi::Property<float> m_centralHoleSize{this, "centralHoleSize", 33. * Gaudi::Units::mm};
    Gaudi::Property<float> m_intraLayerDist{this, "IntraLayerDist", 15.0 * Gaudi::Units::mm};
    Gaudi::Property<float> m_overlapTol{this, "OverlapTol", 0.5 * Gaudi::Units::mm};
    Gaudi::Property<float> m_passHoleSize{this, "PassHoleSize", 40. * Gaudi::Units::mm};
    Gaudi::Property<float> m_LD3Hits{this, "LD3HitsMin", -0.5};
    Gaudi::Property<float> m_LD4Hits{this, "LD4HitsMin", -0.5};

    Gaudi::Property<bool> m_printVariables{this, "PrintVariables", false};
    Gaudi::Property<bool> m_passTracks{this, "PassTracks", false};
    Gaudi::Property<bool> m_doTiming{this, "TimingMeasurement", false};
    Gaudi::Property<bool> m_finalFit{this, "FinalFit", true};
    Gaudi::Property<bool> m_fiducialCuts{this, "FiducialCuts", true};

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_seedsCounter{this, "#seeds"};
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCounter{this, "#tracks"};

    StatusCode recomputeGeometry();

    MiniStatesArray getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks ) const;

    std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
    extrapStates( const MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const;

    std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> findAllSectors(
        const std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& eStatesArray,
        MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const;

    bool getHitsScalar( const LHCb::Pr::UT::HitHandler& hh, const MiniStatesArray& filteredStates,
                        const std::array<Boundaries, 4>& compBoundsArray, LHCb::Pr::UT::Mut::Hits& hitsInLayers,
                        const std::size_t t ) const;

    inline void findHits( const LHCb::Pr::UT::HitHandler& hh, const simd::float_v& yProto, const simd::float_v& ty,
                          const simd::float_v& tx, const simd::float_v& xOnTrackProto, const simd::float_v& tolProto,
                          const simd::float_v& xTolNormFact, LHCb::Pr::UT::Mut::Hits& mutHits,
                          const simd::float_v& yTol, const int firstIndex, const int lastIndex ) const;

    template <bool forward>
    bool formClusters( const LHCb::Pr::UT::Mut::Hits& hitsInLayers, ProtoTracks& pTracks, const int trackIndex ) const;

    template <typename BdlTable>
    void prepareOutputTrackSIMD( const ProtoTracks&                                    protoTracks,
                                 const std::array<LHCb::Pr::UT::Mut::Hits, batchSize>& hitsInLayers,
                                 Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks,
                                 const BdlTable& bdlTable ) const;

    DeUTDetector* m_utDet = nullptr;

    /// Multipupose tool for Bdl and deflection
    ToolHandle<UTMagnetTool>      m_PrUTMagnetTool{this, "PrUTMagnetTool", "PrUTMagnetTool"};
    ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticField", "MagneticFieldSvc"};
    /// timing tool
    mutable ToolHandle<ISequencerTimerTool> m_timerTool{this, "SequencerTimerTool", "SequencerTimerTool"}; // FIXME
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_filtered{
        this, "Reached the maximum number of tracks in filteredStates!!"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_much_in_boundaries{
        this, "Reached the maximum number of tracks in Boundaries!!"};

    ///< Counter for timing tool
    int m_veloUTTime{0};

    float m_zMidUT;
    float m_distToMomentum;

    constexpr static float c_zKink{1780.0};
    constexpr static float c_sigmaVeloSlope{0.10 * Gaudi::Units::mrad};
    constexpr static float c_invSigmaVeloSlope{10.0 / Gaudi::Units::mrad};
  };
} // namespace LHCb::Pr

//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08: Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
// 2019-04-26: Arthur Hennequin (change data Input/Output)
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::VeloUT, "PrVeloUT" )

namespace TracksTag = LHCb::Pr::Upstream::Tag;
namespace HitTag    = LHCb::Pr::UT::Mut::HitTag;
namespace LHCb::Pr {
  namespace {

    simd::mask_v CholeskyDecomposition3( const std::array<simd::float_v, 6>& mat, std::array<simd::float_v, 3>& rhs ) {
      // -- copied from Root::Math::CholeskyDecomp
      // -- first decompose
      std::array<simd::float_v, 6> dst;
      simd::mask_v                 mask = mat[0] < simd::float_v{0.0f};
      dst[0]                            = max( 1e-6f, dst[0] ); // that's only needed if you care about FPE
      // dst[0]                            = rsqrt( mat[0] ); // this seems to give precision issues
      dst[0] = 1.0f / sqrt( mat[0] );
      dst[1] = mat[1] * dst[0];
      dst[2] = mat[2] - dst[1] * dst[1];
      mask   = mask || ( dst[2] < simd::float_v{0.0f} );
      dst[2] = max( 1e-6f, dst[2] ); // that's only needed if you care about FPE
      // dst[2]                            = rsqrt( dst[2] ); // this seems to give precision issues
      dst[2] = 1.0f / sqrt( dst[2] );
      dst[3] = mat[3] * dst[0];
      dst[4] = ( mat[4] - dst[1] * dst[3] ) * dst[2];
      dst[5] = mat[5] - ( dst[3] * dst[3] + dst[4] * dst[4] );
      mask   = mask || ( dst[5] < simd::float_v{0.0f} );
      dst[5] = max( 1e-6f, dst[5] ); // that's only needed if you care about FPE
      // dst[5]                            = rsqrt( dst[5] ); // this seems to give precision issues
      dst[5] = 1.0f / sqrt( dst[5] );

      // -- then solve
      // -- solve Ly = rhs
      const simd::float_v y0 = rhs[0] * dst[0];
      const simd::float_v y1 = ( rhs[1] - dst[1] * y0 ) * dst[2];
      const simd::float_v y2 = ( rhs[2] - ( dst[3] * y0 + dst[4] * y1 ) ) * dst[5];
      // solve L^Tx = y, and put x into rhs
      rhs[2] = (y2)*dst[5];
      rhs[1] = ( y1 - ( dst[4] * rhs[2] ) ) * dst[2];
      rhs[0] = ( y0 - ( dst[3] * rhs[2] + dst[1] * rhs[1] ) ) * dst[0];

      return mask;
    }

    // -- parameters that describe the z position of the kink point as a function of ty in a 4th order polynomial (even
    // terms only)
    constexpr auto magFieldParams = std::array{2010.0f, -2240.0f, -71330.f};

    // perform a fit using trackhelper's best hits with y correction, improve qop estimate
    simd::float_v fastfitterSIMD( std::array<simd::float_v, 4>& improvedParams, const ProtoTracks& protoTracks,
                                  const float zMidUT, const simd::float_v qpxz2p, const int t,
                                  simd::mask_v& goodFitMask ) {

      const Vec3<simd::float_v> pos = protoTracks.pos<simd::float_v>( t );
      const Vec3<simd::float_v> dir = protoTracks.dir<simd::float_v>( t );

      const simd::float_v x  = pos.x;
      const simd::float_v y  = pos.y;
      const simd::float_v z  = pos.z;
      const simd::float_v tx = dir.x;
      const simd::float_v ty = dir.y;
      const simd::float_v zKink =
          magFieldParams[0] - ty * ty * magFieldParams[1] - ty * ty * ty * ty * magFieldParams[2];
      const simd::float_v xMidField = x + tx * ( zKink - z );

      const simd::float_v zDiff = 0.001f * ( zKink - zMidUT );

      // -- This is to avoid division by zero...
      const simd::float_v pHelper = max( abs( protoTracks.qp<simd::float_v>( t ) * qpxz2p ), 1e-9f );
      const simd::float_v invP    = pHelper * rsqrt( 1.0f + ty * ty );

      // these resolution are semi-empirical, could be tuned and might not be correct for low momentum.
      const simd::float_v error1 =
          0.14f + 10000.0f * invP; // this is the resolution due to multiple scattering between Velo and UT
      const simd::float_v error2 = 0.12f + 3000.0f * invP; // this is the resolution due to the finite Velo resolution
      const simd::float_v error  = error1 * error1 + error2 * error2;
      const simd::float_v weight = 1.0f / error;

      std::array<simd::float_v, 6> mat = {weight, weight * zDiff, weight * zDiff * zDiff, 0.0f, 0.0f, 0.0f};
      std::array<simd::float_v, 3> rhs = {weight * xMidField, weight * xMidField * zDiff, 0.0f};

      for ( int i = 0; i < 4; ++i ) {

        // -- there are 3-hit candidates, but we'll
        // -- just treat them like 4-hit candidates
        // -- with 0 weight for the last hit
        const simd::float_v ui = protoTracks.x<simd::float_v>( t, i );
        const simd::float_v dz = 0.001f * ( protoTracks.z<simd::float_v>( t, i ) - zMidUT );
        const simd::float_v w  = protoTracks.weight<simd::float_v>( t, i );
        const simd::float_v ta = protoTracks.sin<simd::float_v>( t, i );
        mat[0] += w;
        mat[1] += w * dz;
        mat[2] += w * dz * dz;
        mat[3] += w * ta;
        mat[4] += w * dz * ta;
        mat[5] += w * ta * ta;
        rhs[0] += w * ui;
        rhs[1] += w * ui * dz;
        rhs[2] += w * ui * ta;
      }

      goodFitMask = !CholeskyDecomposition3( mat, rhs );

      const simd::float_v xUTFit      = rhs[0];
      const simd::float_v xSlopeUTFit = 0.001f * rhs[1];
      const simd::float_v offsetY     = rhs[2];

      const simd::float_v distX = ( xMidField - xUTFit - xSlopeUTFit * ( zKink - zMidUT ) );
      // -- This takes into account that the distance between a point and track is smaller than the distance on the
      // x-axis
      const simd::float_v distCorrectionX2 = 1.0f / ( 1 + xSlopeUTFit * xSlopeUTFit );
      simd::float_v       chi2 = weight * ( distX * distX * distCorrectionX2 + offsetY * offsetY / ( 1.0f + ty * ty ) );

      for ( int i = 0; i < 4; ++i ) {

        const simd::float_v dz   = protoTracks.z<simd::float_v>( t, i ) - zMidUT;
        const simd::float_v w    = protoTracks.weight<simd::float_v>( t, i );
        const simd::float_v dist = ( protoTracks.x<simd::float_v>( t, i ) - xUTFit - xSlopeUTFit * dz -
                                     offsetY * protoTracks.sin<simd::float_v>( t, i ) );

        chi2 += w * dist * dist * distCorrectionX2;
      }

      // new VELO slope x
      const simd::float_v xb =
          0.5f * ( ( xUTFit + xSlopeUTFit * ( zKink - zMidUT ) ) + xMidField ); // the 0.5 is empirical
      const simd::float_v xSlopeVeloFit = ( xb - x ) / ( zKink - z );

      improvedParams = {xUTFit, xSlopeUTFit, y + ty * ( zMidUT - z ) + offsetY, chi2};

      // calculate q/p
      const simd::float_v sinInX  = xSlopeVeloFit * rsqrt( 1.0f + xSlopeVeloFit * xSlopeVeloFit + ty * ty );
      const simd::float_v sinOutX = xSlopeUTFit * rsqrt( 1.0f + xSlopeUTFit * xSlopeUTFit + ty * ty );
      return ( sinInX - sinOutX );
    }

    // -- Evaluate the linear discriminant
    // -- Coefficients derived with LD method for p, pT and chi2 with TMVA
    template <int nHits>
    simd::float_v evaluateLinearDiscriminantSIMD( const std::array<simd::float_v, 3>& inputValues ) {

      constexpr auto coeffs =
          ( nHits == 3 ? std::array{0.162880166064f, -0.107081172665f, 0.134153123662f, -0.137764853657f}
                       : std::array{0.235010729187f, -0.0938323617311f, 0.110823681145f, -0.170467109599f} );

      assert( coeffs.size() == inputValues.size() + 1 );

      return simd::float_v{coeffs[0]} + coeffs[1] * log<simd::float_v>( inputValues[0] ) +
             coeffs[2] * log<simd::float_v>( inputValues[1] ) + coeffs[3] * log<simd::float_v>( inputValues[2] );
    }

    /*
    simd::float_v calcXTol( const simd::float_v minMom, const simd::float_v ty ) {
      return ( 38000.0f / minMom + 0.25f ) * ( 1.0f + ty * ty * 0.8f );
    }
    */
    // --------------------------------------------------------------------
    // -- Helper function to calculate the planeCode: 0 - 1 - 2 - 3
    // --------------------------------------------------------------------
    int planeCode( unsigned int id ) {
      const int station = ( (unsigned int)id & static_cast<unsigned int>( UTInfo::MasksBits::StationMask ) ) >>
                          static_cast<int>( UTInfo::MasksBits::StationBits );
      const int layer = ( (unsigned int)id & static_cast<unsigned int>( UTInfo::MasksBits::LayerMask ) ) >>
                        static_cast<int>( UTInfo::MasksBits::LayerBits );
      return 2 * ( station - 1 ) + ( layer - 1 );
    }

    // --------------------------------------------------------------------
    // -- Helper function to find duplicates in hits in the output
    // --------------------------------------------------------------------
    [[maybe_unused]] bool findDuplicates( const Upstream::Tracks& outputTracks ) {
      auto const uttracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( outputTracks );
      for ( auto const& track : uttracks ) {
        std::vector<int> IDs;
        IDs.reserve( track.nUTHits().cast() );
        for ( int h = track.nVPHits().cast(); h < track.nHits().cast(); h++ ) {
          const int id = track.lhcbID( h ).cast();
          IDs.push_back( id );
        }
        std::sort( IDs.begin(), IDs.end() );
        if ( std::adjacent_find( IDs.begin(), IDs.end() ) != IDs.end() ) return false;
      }
      return true;
    }
    // --------------------------------------------------------------------

    // -- bubble sort is slow, but we never have more than 9 elements (horizontally)
    // -- and can act on 8 elements at once vertically (with AVX)
    void bubbleSortSIMD(
        const int maxColsMaxRows,
        std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& helper,
        const int                                                                                         start ) {
      for ( int i = 0; i < maxColsMaxRows - 1; i++ ) {
        for ( int j = 0; j < maxColsMaxRows - i - 1; j++ ) {
          swap( helper[start + j] > helper[start + j + 1], helper[start + j], helper[start + j + 1] );
        }
      }
    }

    // -- not sure that is the smartest solution
    // -- but I could not come up with anything better
    // -- inspired by: https://lemire.me/blog/2017/04/10/removing-duplicates-from-lists-quickly/
    simd::int_v makeUniqueSIMD(
        std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& out,
        int start, size_t len ) {
      simd::int_v pos  = start + 1;
      simd::int_v oldv = out[start];
      for ( size_t j = start + 1; j < start + len; ++j ) {
        simd::int_v  newv      = out[j];
        simd::mask_v blendMask = ( newv == oldv );
        for ( size_t k = j + 1; k < start + len; ++k ) { out[k - 1] = select( blendMask, out[k], out[k - 1] ); }
        oldv = newv;
        pos  = pos + select( blendMask, simd::int_v{0}, simd::int_v{1} );
      }
      return pos;
    }

    // -- These things are all hardcopied from the PrTableForFunction
    // -- and PrUTMagnetTool
    // -- If the granularity or whatever changes, this will give wrong results
    simd::int_v masterIndexSIMD( const simd::int_v index1, const simd::int_v index2, const simd::int_v index3 ) {
      return ( index3 * 11 + index2 ) * 31 + index1;
    }

    constexpr auto minValsBdl = std::array{-0.3f, -250.0f, 0.0f};
    constexpr auto maxValsBdl = std::array{0.3f, 250.0f, 800.0f};
    constexpr auto deltaBdl   = std::array{0.02f, 50.0f, 80.0f};
    // constexpr auto dxDyHelper = std::array{0.0f, 1.0f, -1.0f, 0.0f};
    // ===========================================================================================
    // -- 2 helper functions for fit
    // -- Pseudo chi2 fit, templated for 3 or 4 hits
    // ===========================================================================================
    inline __attribute__( ( always_inline ) ) void
    addHit( span<float, 3> mat, span<float, 2> rhs, const LHCb::Pr::UT::Mut::Hits& hits, int index, float zMidUT ) {
      const auto& hit = hits.scalar()[index];
      const float ui  = hit.x().cast();
      const float ci  = hit.cos().cast();
      const float dz  = 0.001f * ( hit.z().cast() - zMidUT );
      const float wi  = hit.weight().cast();
      mat[0] += wi * ci;
      mat[1] += wi * ci * dz;
      mat[2] += wi * ci * dz * dz;
      rhs[0] += wi * ui;
      rhs[1] += wi * ui * dz;
    }
    template <std::size_t N>
    inline __attribute__( ( always_inline ) ) void
    simpleFit( const std::array<int, N>& indices, const LHCb::Pr::UT::Mut::Hits& hits, ProtoTracks& pTracks,
               const int trackIndex, float zMidUT, float zKink, float invSigmaVeloSlope ) {
      static_assert( N == 3 || N == 4 );

      // -- Scale the z-component, to not run into numerical problems
      // -- with floats
      const float wb              = pTracks.wb<scalar::float_v>( 0 ).cast();
      const float xMidField       = pTracks.xMidField<scalar::float_v>( 0 ).cast();
      const float invKinkVeloDist = pTracks.invKinkVeloDist<scalar::float_v>( 0 ).cast();
      const float stateX          = pTracks.pos<scalar::float_v>( trackIndex ).x.cast();
      const float stateTx         = pTracks.dir<scalar::float_v>( trackIndex ).x.cast();

      const float zDiff = 0.001f * ( zKink - zMidUT );
      auto        mat   = std::array{wb, wb * zDiff, wb * zDiff * zDiff};
      auto        rhs   = std::array{wb * xMidField, wb * xMidField * zDiff};

      auto const muthit = hits.scalar();
      std::for_each( indices.begin(), indices.end(),
                     [&]( const auto index ) { addHit( mat, rhs, hits, index, zMidUT ); } );

      ROOT::Math::CholeskyDecomp<float, 2> decomp( mat.data() );
      if ( UNLIKELY( !decomp ) ) return;

      decomp.Solve( rhs );

      const float xSlopeTTFit = 0.001f * rhs[1];
      const float xTTFit      = rhs[0];

      // new VELO slope x
      const float xb            = xTTFit + xSlopeTTFit * ( zKink - zMidUT );
      const float xSlopeVeloFit = ( xb - stateX ) * invKinkVeloDist;
      const float chi2VeloSlope = ( stateTx - xSlopeVeloFit ) * invSigmaVeloSlope;

      const float chi2TT = std::accumulate( indices.begin(), indices.end(), chi2VeloSlope * chi2VeloSlope,
                                            [&]( float chi2, const int index ) {
                                              const float du =
                                                  ( xTTFit + xSlopeTTFit * ( muthit[index].z().cast() - zMidUT ) ) -
                                                  muthit[index].x().cast();
                                              return chi2 + muthit[index].weight().cast() * ( du * du );
                                            } ) /
                           ( N + 1 - 2 );

      if ( chi2TT < pTracks.chi2TT<scalar::float_v>( trackIndex ).cast() ) {

        // calculate q/p
        const float sinInX  = xSlopeVeloFit * vdt::fast_isqrtf( 1.0f + xSlopeVeloFit * xSlopeVeloFit );
        const float sinOutX = xSlopeTTFit * vdt::fast_isqrtf( 1.0f + xSlopeTTFit * xSlopeTTFit );
        const float qp      = ( sinInX - sinOutX );

        pTracks.store_chi2TT<scalar::float_v>( trackIndex, chi2TT );
        pTracks.store_qp<scalar::float_v>( trackIndex, qp );
        pTracks.store_xTT<scalar::float_v>( trackIndex, xTTFit );
        pTracks.store_xSlopeTT<scalar::float_v>( trackIndex, xSlopeTTFit );
        for ( std::size_t i = 0; i < N; i++ ) { pTracks.store_hitIndex<scalar::int_v>( trackIndex, i, indices[i] ); }
        if constexpr ( N == 3 ) { pTracks.store_hitIndex<scalar::int_v>( trackIndex, 3, -1 ); }
      }
    }
  } // namespace

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  VeloUT::VeloUT( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"InputTracksName", "Rec/Track/Velo"}, KeyValue{"UTHits", ::UT::Info::HitLocation},
                      KeyValue{"GeometryInfo", "AlgorithmSpecific-" + name + "-UTGeometryInfo"}},
                     KeyValue{"OutputTracksName", "Rec/Track/UT"} ) {}

  /// Initialization
  StatusCode VeloUT::initialize() {

    return Transformer::initialize().andThen( [&] { return m_PrUTMagnetTool.retrieve(); } ).andThen( [&] {
      // m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
      // Cached once in VeloUTTool at initialization. No need to update with small UT movement.
      m_zMidUT = m_PrUTMagnetTool->zMidUT();
      // zMidField and distToMomentum is properly recalculated in PrUTMagnetTool when B field changes
      m_distToMomentum = m_PrUTMagnetTool->averageDist2mom();

      if ( m_doTiming ) {
        m_timerTool->increaseIndent();
        m_veloUTTime = m_timerTool->addTimer( "Internal VeloUT Tracking" );
        m_timerTool->decreaseIndent();
      }
      addConditionDerivation<UTDAQ::GeomCache( const DeUTDetector& )>( {DeUTDetLocation::UT},
                                                                       inputLocation<UTDAQ::GeomCache>() );
    } );
  }
  //=============================================================================
  // Main execution
  //=============================================================================
  Upstream::Tracks VeloUT::operator()( const EventContext& evtCtx, const Velo::Tracks& inputTracks,
                                       const LHCb::Pr::UT::HitHandler& hh, const UTDAQ::GeomCache& geometry ) const {

    if ( m_doTiming ) m_timerTool->start( m_veloUTTime );
    Upstream::Tracks outputTracks{&inputTracks, Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
    outputTracks.reserve( inputTracks.size() );
    m_seedsCounter += inputTracks.size();

    // const auto& fudgeFactors = m_PrUTMagnetTool->DxLayTable();
    const auto& bdlTable = m_PrUTMagnetTool->BdlTable();

    MiniStatesArray filteredStates = getStates( inputTracks, outputTracks );

    std::array<ExtrapolatedStates, 4> extrapStatesArray = extrapStates( filteredStates, geometry );

    auto compBoundsArray = findAllSectors( extrapStatesArray, filteredStates, geometry );

    std::array<LHCb::Pr::UT::Mut::Hits, batchSize> hitsInLayers;
    ProtoTracks                                    pTracks;

    // -- We cannot put all found hits in an array, as otherwise the stack overflows
    // -- so we just do the whole thing in batches
    const std::size_t filteredStatesSize = filteredStates.size();
    for ( std::size_t t = 0; t < filteredStatesSize; t += batchSize ) {

      // -- This is scalar, as the hits are found in a scalar way
      filteredStates.resize( 0 );
      for ( std::size_t t2 = 0; t2 < batchSize && t2 + t < filteredStatesSize; ++t2 ) {
        hitsInLayers[filteredStates.size()].reserve( 256 );
        hitsInLayers[filteredStates.size()].clear();
        for ( auto& it : hitsInLayers[filteredStates.size()].layerIndices ) it = -1;
        const bool foundHits =
            getHitsScalar( hh, filteredStates, compBoundsArray, hitsInLayers[filteredStates.size()], t + t2 );
        filteredStates.copy_back<scalar>( filteredStates, t + t2, foundHits );
      }
      pTracks.size = 0;
      for ( const auto& fState : filteredStates.scalar() ) {
        const auto tEff = fState.offset();

        Vec3<scalar::float_v> pos{fState.get<MiniStateTag::StatePosition>().x(),
                                  fState.get<MiniStateTag::StatePosition>().y(),
                                  fState.get<MiniStateTag::StatePosition>().z()};
        Vec3<scalar::float_v> dir{fState.get<MiniStateTag::StatePosition>().tx(),
                                  fState.get<MiniStateTag::StatePosition>().ty(), 1.f};

        int trackIndex = pTracks.size;
        pTracks.fillHelperParams<scalar>( pos, dir, c_zKink, c_sigmaVeloSlope );
        pTracks.store_pos<scalar::float_v>( trackIndex, pos );
        pTracks.store_dir<scalar::float_v>( trackIndex, dir );
        pTracks.store_chi2TT<scalar::float_v>( trackIndex, m_maxPseudoChi2.value() );

        pTracks.store_hitIndex<scalar::int_v>( trackIndex, 0, -1 );
        if ( !formClusters<true>( hitsInLayers[tEff], pTracks, trackIndex ) ) {
          formClusters<false>( hitsInLayers[tEff], pTracks, trackIndex );
        }
        if ( pTracks.hitIndex<scalar::int_v>( trackIndex, 0 ).cast() == -1 ) continue;

        scalar::int_v ancestorIndex = fState.get<MiniStateTag::index>();
        pTracks.store_index<scalar::int_v>( trackIndex, ancestorIndex );
        pTracks.store_hitContIndex<scalar::int_v>( trackIndex, tEff );
        // -- this runs over all 4 layers, even if no hit was found
        // -- but it fills a weight of 0
        // -- Note: These are not "physical" layers, as the hits are ordered such that only
        // -- the last one can be not filled.
        for ( int i = 0; i < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++i ) {
          int             hitI    = pTracks.hitIndex<scalar::int_v>( trackIndex, i ).cast();
          auto            hitsInL = hitsInLayers[tEff].scalar();
          scalar::float_v weight  = ( hitI == -1 ) ? 0.0f : hitsInL[hitI].weight();
          pTracks.store_weight<scalar::float_v>( trackIndex, i, weight );
          hitI = std::max( 0, hitI ); // prevent index out of bound

          pTracks.store_x<scalar::float_v>( trackIndex, i, hitsInL[hitI].x() );
          pTracks.store_z<scalar::float_v>( trackIndex, i, hitsInL[hitI].z() );
          pTracks.store_sin<scalar::float_v>( trackIndex, i, hitsInL[hitI].sin() );

          LHCb::LHCbID id( LHCb::UTChannelID( hitsInL[hitI].channelID().cast() ) );
          pTracks.store_id<scalar::int_v>( trackIndex, i, id.lhcbID() );
          pTracks.store_hitIndex<scalar::int_v>( trackIndex, i, hitsInL[hitI].index() );
        }
        pTracks.size++;
      }

      // padding to avoid FPEs
      if ( ( pTracks.size + simd::size ) < batchSize ) {
        pTracks.store_pos<simd::float_v>( pTracks.size, Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
        pTracks.store_dir<simd::float_v>( pTracks.size, Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
      }
      prepareOutputTrackSIMD( pTracks, hitsInLayers, outputTracks, inputTracks, bdlTable );
    }

    // -- The algorithm should not store duplicated hits...
    assert( findDuplicates( outputTracks ) && "Hit duplicates found" );

    m_tracksCounter += outputTracks.size();
    if ( m_doTiming ) m_timerTool->stop( m_veloUTTime );
    return outputTracks;
  }
  //=============================================================================
  // Get the state, do some cuts
  //=============================================================================
  MiniStatesArray VeloUT::getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks ) const {

    const int          EndVelo    = 1;
    const simd::mask_v passTracks = m_passTracks.value() ? simd::mask_true() : simd::mask_false();

    MiniStatesArray filteredStates;
    filteredStates.reserve( inputTracks.size() );

    auto const velozipped = LHCb::Pr::make_zip( inputTracks );
    for ( auto const& velotrack : velozipped ) {
      auto const loopMask = velotrack.loop_mask();
      auto const trackVP  = velotrack.indices();
      auto       pos      = velotrack.StatePos( EndVelo );
      auto       dir      = velotrack.StateDir( EndVelo );
      auto       covX     = velotrack.StateCovX( EndVelo );

      simd::float_v xMidUT = pos.x + dir.x * ( m_zMidUT - pos.z );
      simd::float_v yMidUT = pos.y + dir.y * ( m_zMidUT - pos.z );

      simd::mask_v centralHoleMask = xMidUT * xMidUT + yMidUT * yMidUT < simd::float_v{m_centralHoleSize.value()} *
                                                                             simd::float_v{m_centralHoleSize.value()};
      simd::mask_v slopesMask   = ( ( abs( dir.x ) > m_maxXSlope.value() ) || ( abs( dir.y ) > m_maxYSlope.value() ) );
      simd::mask_v passHoleMask = abs( xMidUT ) < m_passHoleSize.value() && abs( yMidUT ) < m_passHoleSize.value();
      simd::mask_v mask         = centralHoleMask || slopesMask;
      simd::mask_v csMask       = loopMask && !mask && ( !passTracks || !passHoleMask );

      auto fState = filteredStates.compress_back<SIMDWrapper::InstructionSet::Best>( csMask );
      fState.field<MiniStateTag::StatePosition>().set( pos.x, pos.y, pos.z, dir.x, dir.y );
      fState.field<MiniStateTag::index>().set( trackVP );

      if ( m_passTracks ) {

        auto outMask = loopMask && passHoleMask; // not sure if correct...
        auto i       = outputTracks.size();
        outputTracks.resize( i + simd::popcount( outMask ) );
        outputTracks.store<TracksTag::trackVP>( i, velotrack.indices(), outMask );
        outputTracks.store<TracksTag::StateQoP>( i, simd::float_v{0.f}, outMask );
        outputTracks.store<TracksTag::nUTHits>( i, simd::int_v{0}, outMask );
        outputTracks.store<TracksTag::nVPHits>( i, velotrack.nHits(), outMask );
        outputTracks.store_StatePosDir( i, pos, dir, outMask );
        outputTracks.store_StateCovX( i, covX, outMask );
        for ( int idx = 0; idx < velotrack.nHits().hmax( outMask ); ++idx ) {
          outputTracks.store_vp_index( i, idx, velotrack.vp_index( idx ), outMask );
        }
      }
    }
    return filteredStates;
  }
  //=============================================================================
  // Extrapolate the states
  //=============================================================================
  std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
  VeloUT::extrapStates( const MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const {

    std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> eStatesArray;
    for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex )
      eStatesArray[layerIndex].reserve( filteredStates.size() );

    // -- Used for the calculation of the size of the search windows
    constexpr const std::array<float, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> normFact{0.95f, 1.0f,
                                                                                                         1.36f, 1.41f};

    for ( const auto& fState : filteredStates.simd() ) {

      const auto x  = fState.get<MiniStateTag::StatePosition>().x();
      const auto y  = fState.get<MiniStateTag::StatePosition>().y();
      const auto z  = fState.get<MiniStateTag::StatePosition>().z();
      const auto tx = fState.get<MiniStateTag::StatePosition>().tx();
      const auto ty = fState.get<MiniStateTag::StatePosition>().ty();

      // -- this 500 seems a little odd...
      const simd::float_v invTheta = min( 500.0f, 1.0f * rsqrt( tx * tx + ty * ty ) );
      const simd::float_v minMom   = max( m_minPT.value() * invTheta, m_minMomentum.value() );

      for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

        const simd::float_v xTol = abs( 1.0f / ( m_distToMomentum * minMom ) ) * normFact[layerIndex];
        const simd::float_v yTol = m_yTol.value() + m_yTolSlope.value() * xTol;

        const simd::float_v zGeo{geom.layers[layerIndex].z};
        const simd::float_v dxDy{geom.layers[layerIndex].dxDy};

        const simd::float_v yAtZ   = y + ty * ( zGeo - z );
        const simd::float_v xLayer = x + tx * ( zGeo - z );
        const simd::float_v yLayer = yAtZ + yTol * dxDy;

        auto eStatesArr =
            eStatesArray[layerIndex].emplace_back<SIMDWrapper::InstructionSet::Best>( filteredStates.size() );
        eStatesArr.field<ExtStateTag::xLayer>().set( xLayer );
        eStatesArr.field<ExtStateTag::yLayer>().set( yLayer );
        eStatesArr.field<ExtStateTag::xTol>().set( xTol );
        eStatesArr.field<ExtStateTag::tx>().set( tx );
      }
    }

    return eStatesArray;
  }
  //=============================================================================
  // -- find the sectors
  //=============================================================================
  inline __attribute__( ( always_inline ) )
  std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
  VeloUT::findAllSectors(
      const std::array<ExtrapolatedStates, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& eStatesArray,
      MiniStatesArray& filteredStates, const UTDAQ::GeomCache& geom ) const {

    std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> compBoundsArray;
    for ( int iLayer = 0; iLayer < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++iLayer )
      compBoundsArray[iLayer].reserve( filteredStates.size() );

    int contSize = filteredStates.size();
    filteredStates.resize( 0 );

    std::array<simd::int_v, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> posArray{};
    std::array<simd::int_v, maxNumSectors* static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>
                                                                              helperArray{}; // 4 layers x maximum 9 sectors
    std::array<int, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )> maxColsRows;

    // -- This now works with up to 9 sectors
    for ( int t = 0; t < contSize; t += simd::size ) {
      auto loopMask = simd::loop_mask( t, contSize );

      simd::int_v nLayers{0};

      for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

        const simd::int_v regionBoundary1 = ( 2 * geom.layers[layerIndex].nColsPerSide + 3 );
        const simd::int_v regionBoundary2 = ( 2 * geom.layers[layerIndex].nColsPerSide - 5 );

        simd::int_v subcolmin{0};
        simd::int_v subcolmax{0};
        simd::int_v subrowmin{0};
        simd::int_v subrowmax{0};

        const auto    eStatesArr = eStatesArray[layerIndex].simd();
        simd::float_v xLayer     = eStatesArr[t].get<ExtStateTag::xLayer>();
        simd::float_v yLayer     = eStatesArr[t].get<ExtStateTag::yLayer>();
        simd::float_v xTol       = eStatesArr[t].get<ExtStateTag::xTol>();
        simd::float_v tx         = eStatesArr[t].get<ExtStateTag::tx>();

        simd::mask_v mask = UTDAQ::findSectors( layerIndex, xLayer, yLayer, xTol - abs( tx ) * m_intraLayerDist.value(),
                                                m_yTol.value() + m_yTolSlope.value() * abs( xTol ),
                                                geom.layers[layerIndex], subcolmin, subcolmax, subrowmin, subrowmax );

        const simd::mask_v gathermask = loopMask && mask;

        // -- Determine the maximum number of rows and columns we have to take into account
        // -- maximum 3, minimum 0
        // -- The 'clamp' is needed to prevent large negative values from 'hmax' when gathermask has no true entries
        const int maxCols = std::clamp( ( subcolmax - subcolmin ).hmax( gathermask ) + 1, 0, maxNumCols );
        const int maxRows = std::clamp( ( subrowmax - subrowmin ).hmax( gathermask ) + 1, 0, maxNumRows );

        maxColsRows[layerIndex] = maxCols * maxRows;

        int counter = 0;
        for ( int sc = 0; sc < maxCols; sc++ ) {

          simd::int_v realSC = min( subcolmax, subcolmin + sc );
          // -- Gives the region (actually region - 1): left 0, center 1, right 2
          simd::int_v region = select( realSC > regionBoundary1, simd::int_v{1}, simd::int_v{0} ) +
                               select( realSC > regionBoundary2, simd::int_v{1}, simd::int_v{0} );

          for ( int sr = 0; sr < maxRows; sr++ ) {

            simd::int_v realSR = min( subrowmax, subrowmin + sr );
            simd::int_v sectorIndex =
                realSR + static_cast<int>( UTInfo::SectorNumbers::EffectiveSectorsPerColumn ) * realSC;

            // -- only gather when we are not outside the acceptance
            // -- if we are outside, fill 1 which is the lowest possible sector number
            // -- We need to fill a valid number, as one can have 3 layers with a correct sector
            // -- and one without a correct sector, in which case the track will not be masked off.
            // -- However, these cases should happen very rarely
            simd::int_v sect = ( layerIndex < 2 )
                                   ? geom.sectorLUT.maskgather_station1<simd::int_v>( sectorIndex, gathermask, 1 )
                                   : geom.sectorLUT.maskgather_station2<simd::int_v>( sectorIndex, gathermask, 1 );

            // -- ID is: sectorIndex (from LUT) + (layerIndex * 3 + region - 1 ) * 98
            // -- The regions are already calculated with a -1
            helperArray[maxNumSectors * layerIndex + counter] =
                sect +
                ( layerIndex * static_cast<int>( UTInfo::DetectorNumbers::Regions ) + region ) *
                    static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion ) -
                1;
            counter++;
          }
        }

        // -- This is sorting
        bubbleSortSIMD( maxCols * maxRows, helperArray, maxNumSectors * layerIndex );
        // -- This is uniquifying
        posArray[layerIndex] = makeUniqueSIMD( helperArray, maxNumSectors * layerIndex, maxCols * maxRows );
        // -- count the number of layers which are 'valid'
        nLayers += select( mask, simd::int_v{1}, simd::int_v{0} );
      }

      // -- We need at least three layers
      const simd::mask_v compressMask = ( nLayers > 2 ) && loopMask;

      for ( int iLayer = 0; iLayer < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++iLayer ) {
        auto compBoundsArr = compBoundsArray[iLayer].compress_back<SIMDWrapper::InstructionSet::Best>( compressMask );
        for ( int iSector = 0; iSector < maxColsRows[iLayer]; ++iSector ) {
          compBoundsArr.field<BoundariesTag::sects>( iSector ).set( helperArray[maxNumSectors * iLayer + iSector] );
        }
        const auto    eStatesArr = eStatesArray[iLayer].simd();
        simd::float_v xTol       = eStatesArr[t].get<ExtStateTag::xTol>();
        compBoundsArr.field<BoundariesTag::xTol>().set( xTol );
        compBoundsArr.field<BoundariesTag::nPos>().set( posArray[iLayer] - maxNumSectors * iLayer );
      }

      // -- Now need to compress the filtered states, such that they are
      // -- in sync with the sectors
      filteredStates.copy_back<simd>( filteredStates, t, compressMask );
    }

    return compBoundsArray;
  }
  //=============================================================================
  // Find the hits
  //=============================================================================
  inline __attribute__( ( always_inline ) ) bool VeloUT::getHitsScalar(
      const LHCb::Pr::UT::HitHandler& hh, const MiniStatesArray& filteredStates,
      const std::array<Boundaries, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>& compBoundsArray,
      LHCb::Pr::UT::Mut::Hits& hitsInLayers, const std::size_t t ) const {

    // -- This is for some sanity checks later
    constexpr const int maxSectorsPerRegion = static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion );
    constexpr const int maxLayer            = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    constexpr const int maxRegion           = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    [[maybe_unused]] constexpr const int maxSectorNumber =
        maxSectorsPerRegion + ( ( maxLayer - 1 ) * maxRegion + ( maxRegion - 1 ) ) * maxSectorsPerRegion;

    const simd::float_v yTolSlope{m_yTolSlope.value()};

    const auto fState  = filteredStates.scalar();
    const auto xState  = fState[t].get<MiniStateTag::StatePosition>().x().cast();
    const auto yState  = fState[t].get<MiniStateTag::StatePosition>().y().cast();
    const auto zState  = fState[t].get<MiniStateTag::StatePosition>().z().cast();
    const auto txState = fState[t].get<MiniStateTag::StatePosition>().tx().cast();
    const auto tyState = fState[t].get<MiniStateTag::StatePosition>().ty().cast();

    std::size_t nSize   = 0;
    std::size_t nLayers = 0;

    // -- the protos could be precomputed
    const simd::float_v yProto{yState - tyState * zState};
    const simd::float_v xOnTrackProto{xState - txState * zState};
    const simd::float_v ty{tyState};
    const simd::float_v tx{txState};

    for ( int layerIndex = 0; layerIndex < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++layerIndex ) {

      if ( ( layerIndex == 2 && nLayers == 0 ) || ( layerIndex == 3 && nLayers < 2 ) ) return false;

      const auto          compBoundsArr = compBoundsArray[layerIndex].scalar();
      const auto          xTolS         = compBoundsArr[t].get<BoundariesTag::xTol>().cast();
      const auto          nPos          = compBoundsArr[t].get<BoundariesTag::nPos>().cast();
      const simd::float_v yTol          = m_yTol.value() + m_yTolSlope.value() * xTolS;

      assert( nPos < maxNumSectors && "nPos out of bound" );

      const simd::float_v tolProto{m_yTol.value()};
      const simd::float_v xTol{xTolS};

      std::array<int, maxNumSectors + 1> sectors{0};

      for ( int i = 0; i < nPos; ++i ) {
        sectors[i] =
            std::min( maxSectorNumber - 1, std::max( compBoundsArr[t].get<BoundariesTag::sects>( i ).cast(), 0 ) );
      }
      for ( int j = 0; j < nPos; j++ ) {

        assert( ( sectors[j] > -1 ) && ( sectors[j] < maxSectorNumber ) && "sector number out of bound" );
        if ( ( sectors[j] <= -1 ) || ( sectors[j] > maxSectorNumber ) ) {
          error() << "Sector number out of bound, something wrong!" << endmsg;
        }

        // -- let's try to make it branchless
        const std::pair<int, int>& temp       = hh.indices( sectors[j] );
        const std::pair<int, int>& temp2      = hh.indices( sectors[j + 1] );
        const int                  firstIndex = temp.first;
        // const int                  lastIndex = temp.second;
        const int shift =
            ( temp2.first == temp.second || ( temp.first == temp2.first && temp.second == temp2.second ) );
        if ( temp2.first == 0 && temp2.second == 0 && temp2.first != temp.second ) j += 1;
        const int lastIndex = ( shift == 1 && ( j + 1 ) < nPos ) ? temp2.second : temp.second;
        j += shift;
        findHits( hh, yProto, ty, tx, xOnTrackProto, tolProto, xTol, hitsInLayers, yTol, firstIndex, lastIndex );
      }

      nLayers += int( nSize != hitsInLayers.size() );
      hitsInLayers.layerIndices[layerIndex] = nSize;
      nSize                                 = hitsInLayers.size();
    }
    // -- only use these hits, if we have at least 3 layers
    return nLayers > 2;
  }
  // ==============================================================================
  // -- Method that finds the hits in a given layer within a certain range
  // ==============================================================================
  inline __attribute__( ( always_inline ) ) void
  VeloUT::findHits( const LHCb::Pr::UT::HitHandler& hh, const simd::float_v& yProto, const simd::float_v& ty,
                    const simd::float_v& tx, const simd::float_v& xOnTrackProto, const simd::float_v& tolProto,
                    const simd::float_v& xTolNormFact, LHCb::Pr::UT::Mut::Hits& mutHits, const simd::float_v& yTol,
                    const int firstIndex, const int lastIndex ) const {

    const LHCb::Pr::UT::Hits& myHits = hh.hits();

    for ( int i = firstIndex; i < lastIndex; i += simd::size ) {
      // -- Calculate distance between straight line extrapolation from Velo and hit position
      const simd::float_v yy       = yProto + ty * myHits.zAtYEq0<simd::float_v>( i );
      const simd::float_v xx       = myHits.xAtYEq0<simd::float_v>( i ) + yy * myHits.dxDy<simd::float_v>( i );
      const simd::float_v xOnTrack = xOnTrackProto + tx * myHits.zAtYEq0<simd::float_v>( i );
      const simd::float_v absdx    = abs( xx - xOnTrack );

      if ( none( absdx < xTolNormFact ) ) continue;
      auto loopMask = simd::loop_mask( i, lastIndex );

      // is there anything like minmax?
      const simd::float_v yMin = min( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );
      const simd::float_v yMax = max( myHits.yBegin<simd::float_v>( i ), myHits.yEnd<simd::float_v>( i ) );

      const simd::float_v tol  = yTol + absdx * tolProto;
      auto                mask = ( yMin - tol < yy && yy < yMax + tol ) && ( absdx < xTolNormFact ) && loopMask;

      if ( none( mask ) ) continue;
      auto muthit = mutHits.compress_back<SIMDWrapper::InstructionSet::Best>( mask );
      muthit.field<HitTag::xs>().set( xx );
      muthit.field<HitTag::zs>().set( myHits.zAtYEq0<simd::float_v>( i ) );
      muthit.field<HitTag::coss>().set( myHits.cos<simd::float_v>( i ) );
      muthit.field<HitTag::sins>().set( myHits.cos<simd::float_v>( i ) * -1.0f * myHits.dxDy<simd::float_v>( i ) );
      muthit.field<HitTag::weights>().set( myHits.weight<simd::float_v>( i ) );
      muthit.field<HitTag::channelIDs>().set( myHits.channelID<simd::int_v>( i ) );
      muthit.field<HitTag::indexs>().set( simd::indices( i ) ); // fill the index in the original hit container
    }
  }
  //=========================================================================
  // Form clusters
  //=========================================================================
  template <bool forward>
  inline __attribute__( ( always_inline ) ) bool VeloUT::formClusters( const LHCb::Pr::UT::Mut::Hits& hitsInLayers,
                                                                       ProtoTracks&                   pTracks,
                                                                       const int trackIndex ) const {

    const int begin0 = forward ? hitsInLayers.layerIndices[0] : hitsInLayers.layerIndices[3];
    const int end0   = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.size();

    const int begin1 = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.layerIndices[2];
    const int end1   = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[3];

    const int begin2 = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[1];
    const int end2   = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[2];

    const int begin3            = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[0];
    const int end3              = forward ? hitsInLayers.size() : hitsInLayers.layerIndices[1];
    bool      fourLayerSolution = false;

    const float stateTx = pTracks.dir<scalar::float_v>( trackIndex ).x.cast();
    const auto& hitsInL = hitsInLayers.scalar();

    // -- this is scalar for the moment
    for ( int i0 = begin0; i0 < end0; ++i0 ) {

      const float xhitLayer0 = hitsInL[i0].x().cast();
      const float zhitLayer0 = hitsInL[i0].z().cast();

      // Loop over Second Layer
      for ( int i2 = begin2; i2 < end2; ++i2 ) {

        const float xhitLayer2 = hitsInL[i2].x().cast();
        const float zhitLayer2 = hitsInL[i2].z().cast();

        const float tx = ( xhitLayer2 - xhitLayer0 ) / ( zhitLayer2 - zhitLayer0 );

        if ( std::abs( tx - stateTx ) > m_deltaTx ) continue;

        int   bestHit1Index = -1;
        float hitTol        = m_hitTol;

        for ( int i1 = begin1; i1 < end1; ++i1 ) {

          const float xhitLayer1 = hitsInL[i1].x().cast();
          const float zhitLayer1 = hitsInL[i1].z().cast();

          const float xextrapLayer1 = xhitLayer0 + tx * ( zhitLayer1 - zhitLayer0 );
          if ( std::abs( xhitLayer1 - xextrapLayer1 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer1 - xextrapLayer1 );
            bestHit1Index = i1;
          }
        }

        if ( fourLayerSolution && bestHit1Index == -1 ) continue;

        int bestHit3Index = -1;
        hitTol            = m_hitTol;
        for ( int i3 = begin3; i3 < end3; ++i3 ) {

          const float xhitLayer3 = hitsInL[i3].x().cast();
          const float zhitLayer3 = hitsInL[i3].z().cast();

          const float xextrapLayer3 = xhitLayer2 + tx * ( zhitLayer3 - zhitLayer2 );

          if ( std::abs( xhitLayer3 - xextrapLayer3 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer3 - xextrapLayer3 );
            bestHit3Index = i3;
          }
        }
        // -- All hits found
        if ( bestHit1Index != -1 && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2, bestHit3Index}, hitsInLayers, pTracks, trackIndex, m_zMidUT,
                     c_zKink, c_invSigmaVeloSlope );

          if ( !fourLayerSolution && pTracks.hitIndex<scalar::int_v>( trackIndex, 0 ).cast() != -1 ) {
            fourLayerSolution = true;
          }
          continue;
        }

        // -- Nothing found in layer 3
        if ( !fourLayerSolution && bestHit1Index != -1 ) {
          simpleFit( std::array{i0, bestHit1Index, i2}, hitsInLayers, pTracks, trackIndex, m_zMidUT, c_zKink,
                     c_invSigmaVeloSlope );
          continue;
        }
        // -- Noting found in layer 1
        if ( !fourLayerSolution && bestHit3Index != -1 ) {
          simpleFit( std::array{i0, bestHit3Index, i2}, hitsInLayers, pTracks, trackIndex, m_zMidUT, c_zKink,
                     c_invSigmaVeloSlope );
          continue;
        }
      }
    }
    return fourLayerSolution;
  }
  //=========================================================================
  // Create the Velo-UT tracks
  //=========================================================================
  template <typename BdlTable>
  inline __attribute__( ( always_inline ) ) void VeloUT::prepareOutputTrackSIMD(
      const ProtoTracks& protoTracks, const std::array<LHCb::Pr::UT::Mut::Hits, batchSize>& hitsInLayers,
      Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks, const BdlTable& bdlTable ) const {

    auto const velozipped = LHCb::Pr::make_zip( inputTracks );
    for ( std::size_t t = 0; t < protoTracks.size; t += simd::size ) {

      //== Handle states. copy Velo one, add TT.
      const simd::float_v zOrigin =
          select( protoTracks.dir<simd::float_v>( t ).y > 0.001f,
                  protoTracks.pos<simd::float_v>( t ).z -
                      protoTracks.pos<simd::float_v>( t ).y / protoTracks.dir<simd::float_v>( t ).y,
                  protoTracks.pos<simd::float_v>( t ).z -
                      protoTracks.pos<simd::float_v>( t ).x / protoTracks.dir<simd::float_v>( t ).x );

      auto loopMask = simd::loop_mask( t, protoTracks.size );
      // -- this is to filter tracks where the fit had a too large chi2
      simd::mask_v fourHitTrack = protoTracks.weight<simd::float_v>( t, 3 ) > 0.0001f;

      // const float bdl1    = m_PrUTMagnetTool->bdlIntegral(helper.state.ty,zOrigin,helper.state.z);

      // -- These are calculations, copied and simplified from PrTableForFunction
      // -- FIXME: these rely on the internal details of PrTableForFunction!!!
      //           and should at least be put back in there, and used from here
      //           to make sure everything _stays_ consistent...
      auto var = std::array{protoTracks.dir<simd::float_v>( t ).y, zOrigin, protoTracks.pos<simd::float_v>( t ).z};

      simd::int_v index1 = min( max( simd::int_v{( var[0] + 0.3f ) / 0.6f * 30}, 0 ), 30 );
      simd::int_v index2 = min( max( simd::int_v{( var[1] + 250 ) / 500 * 10}, 0 ), 10 );
      simd::int_v index3 = min( max( simd::int_v{var[2] / 800 * 10}, 0 ), 10 );

      simd::float_v bdl = gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 ) );

      // -- TODO: check if we can go outside this table...
      const std::array<simd::float_v, 3> bdls =
          std::array{gather( bdlTable.table().data(), masterIndexSIMD( index1 + 1, index2, index3 ) ),
                     gather( bdlTable.table().data(), masterIndexSIMD( index1, index2 + 1, index3 ) ),
                     gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 + 1 ) )};

      const std::array<simd::float_v, 3> boundaries = {-0.3f + simd::float_v{index1} * deltaBdl[0],
                                                       -250.0f + simd::float_v{index2} * deltaBdl[1],
                                                       0.0f + simd::float_v{index3} * deltaBdl[2]};

      // -- This is an interpolation, to get a bit more precision
      simd::float_v addBdlVal{0.0f};
      for ( int i = 0; i < 3; ++i ) {

        // -- this should make sure that values outside the range add nothing to the sum
        var[i] = select( minValsBdl[i] > var[i], boundaries[i], var[i] );
        var[i] = select( maxValsBdl[i] < var[i], boundaries[i], var[i] );

        const simd::float_v dTab_dVar = ( bdls[i] - bdl ) / deltaBdl[i];
        const simd::float_v dVar      = ( var[i] - boundaries[i] );
        addBdlVal += dTab_dVar * dVar;
      }
      bdl += addBdlVal;
      // ----

      // -- order is: x, tx, y, chi2
      std::array<simd::float_v, 4> finalParams = {
          protoTracks.xTT<simd::float_v>( t ), protoTracks.xSlopeTT<simd::float_v>( t ),
          protoTracks.pos<simd::float_v>( t ).y +
              protoTracks.dir<simd::float_v>( t ).y * ( m_zMidUT - protoTracks.pos<simd::float_v>( t ).z ),
          protoTracks.chi2TT<simd::float_v>( t )};

      const simd::float_v qpxz2p  = -1.0f / bdl * 3.3356f / Gaudi::Units::GeV;
      simd::mask_v        fitMask = simd::mask_true();
      simd::float_v       qp = m_finalFit ? fastfitterSIMD( finalParams, protoTracks, m_zMidUT, qpxz2p, t, fitMask )
                                    : protoTracks.qp<simd::float_v>( t ) *
                                          rsqrt( 1.0f + protoTracks.dir<simd::float_v>( t ).y *
                                                            protoTracks.dir<simd::float_v>( t ).y ); // is this correct?

      qp                      = select( fitMask, qp, protoTracks.qp<simd::float_v>( t ) );
      const simd::float_v qop = select( abs( bdl ) < 1.e-8f, simd::float_v{1000.0f}, qp * qpxz2p );

      // -- Don't make tracks that have grossly too low momentum
      // -- Beware of the momentum resolution!
      const simd::float_v p = abs( 1.0f / qop );
      const simd::float_v pt =
          p * sqrt( protoTracks.dir<simd::float_v>( t ).x * protoTracks.dir<simd::float_v>( t ).x +
                    protoTracks.dir<simd::float_v>( t ).y * protoTracks.dir<simd::float_v>( t ).y );
      const simd::mask_v pPTMask = ( p > m_minMomentumFinal.value() && pt > m_minPTFinal.value() );

      const simd::float_v xUT  = finalParams[0];
      const simd::float_v txUT = finalParams[1];
      const simd::float_v yUT  = finalParams[2];

      // -- apply some fiducial cuts
      // -- they are optimised for high pT tracks (> 500 MeV)
      simd::mask_v fiducialMask = simd::mask_false();

      if ( m_fiducialCuts ) {
        const float magSign = m_magFieldSvc->signedRelativeCurrent();

        fiducialMask = ( magSign * qop < 0.0f && xUT > -48.0f && xUT < 0.0f && abs( yUT ) < 33.0f );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && xUT < 48.0f && xUT > 0.0f && abs( yUT ) < 33.0f );

        fiducialMask = fiducialMask || ( magSign * qop < 0.0f && txUT > 0.09f + 0.0003f * pt );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && txUT < -0.09f - 0.0003f * pt );
      }

      // -- evaluate the linear discriminant and reject ghosts
      // -- the values only make sense if the final fit is performed
      simd::mask_v mvaMask = simd::mask_true();

      if ( m_finalFit ) {

        const simd::float_v fourHitDisc  = evaluateLinearDiscriminantSIMD<4>( {p, pt, finalParams[3]} );
        const simd::float_v threeHitDisc = evaluateLinearDiscriminantSIMD<3>( {p, pt, finalParams[3]} );

        simd::mask_v fourHitMask  = fourHitDisc > m_LD4Hits.value();
        simd::mask_v threeHitMask = threeHitDisc > m_LD3Hits.value();

        // -- only have 3 or 4 hit tracks
        mvaMask = ( fourHitTrack && fourHitMask ) || ( !fourHitTrack && threeHitMask );
      }

      simd::mask_v validTrackMask = !fiducialMask && pPTMask && loopMask && mvaMask;

      // ==========================================================================================

      const simd::int_v ancestor      = protoTracks.index<simd::int_v>( t );
      auto              indices       = select( validTrackMask, ancestor, 0 );
      auto              velo_ancestor = velozipped.gather( indices );
      const int         EndVelo       = 1;
      auto const        velo_scalar   = velozipped.with<SIMDWrapper::InstructionSet::Scalar>();

      auto currentsize = outputTracks.size();
      outputTracks.resize( currentsize + simd::popcount( validTrackMask ) );
      outputTracks.store<TracksTag::StateQoP>( currentsize, qop, validTrackMask );
      outputTracks.store<TracksTag::trackVP>( currentsize, indices, validTrackMask );
      outputTracks.store<TracksTag::nVPHits>( currentsize, velo_ancestor.nHits(), validTrackMask );
      outputTracks.store_StatePosDir( currentsize, velo_ancestor.StatePos( EndVelo ), velo_ancestor.StateDir( EndVelo ),
                                      validTrackMask );
      outputTracks.store_StateCovX( currentsize, velo_ancestor.StateCovX( EndVelo ), validTrackMask );
      // store velo index and lhcbids
      for ( auto idx = 0; idx < velo_ancestor.nHits().hmax( validTrackMask ); ++idx ) {
        outputTracks.store_vp_index( currentsize, idx, velo_ancestor.vp_index( idx ), validTrackMask );
        outputTracks.store_lhcbID( currentsize, idx, velo_ancestor.lhcbID( idx ), validTrackMask );
      }

      float txArray[simd::size];
      txUT.store( txArray );
      float xArray[simd::size];
      xUT.store( xArray );
      int nUTHits[simd::size] = {0};
      // -- This is needed to find the planeCode of the layer with the missing hit
      float sumLayArray[simd::size] = {};

      // -- from here on, go over each track individually to find and add the overlap hits
      // -- this is not particularly elegant...
      // -- As before, these are "pseudo layers", i.e. it is not guaranteed that if i > j, z[i] > z[j]
      for ( int iLayer = 0; iLayer < static_cast<int>( UTInfo::DetectorNumbers::TotalLayers ); ++iLayer ) {

        int trackIndex2 = 0;
        for ( unsigned int t2 = 0; t2 < simd::size; ++t2 ) {
          if ( !testbit( validTrackMask, t2 ) ) continue;
          auto const veloIdx = protoTracks.index<scalar::int_v>( t + t2 ).cast();

          const auto tscalar = t + t2;

          const bool goodHit = ( protoTracks.weight<scalar::float_v>( tscalar, iLayer ).cast() > 0.0001f );
          const auto hitIdx  = protoTracks.hitIndex<scalar::int_v>( tscalar, iLayer );
          const auto id      = protoTracks.id<scalar::int_v>( tscalar, iLayer );

          // -- Only add the hit, if it is not in an empty layer (that sounds like a tautology,
          // -- but given that one always has 4 hits, even if only 3 make sense, it is needed)
          // -- Only the last pseudo-layer can be an empty layer
          if ( goodHit ) {
            auto const idIdx = velo_scalar[veloIdx].nHits().cast() + nUTHits[t2];
            outputTracks.store_ut_index( currentsize + trackIndex2, nUTHits[t2], hitIdx );
            outputTracks.store_lhcbID( currentsize + trackIndex2, idIdx, id );
            nUTHits[t2] += 1;
          }
          // --
          // -----------------------------------------------------------------------------------
          // -- The idea of the following code is: In layers where we have found a hit, we search for
          // -- overlap hits.
          // -- In layers where no hit was found initially, we use the better parametrization of the final
          // -- track fit to pick up hits that were lost in the initial search
          // -----------------------------------------------------------------------------------
          const float zhit         = goodHit ? protoTracks.z<scalar::float_v>( tscalar, iLayer ).cast() : m_zMidUT;
          const float xhit         = goodHit ? protoTracks.x<scalar::float_v>( tscalar, iLayer ).cast() : xArray[t2];
          const int   hitContIndex = protoTracks.hitContIndex<scalar::int_v>( tscalar ).cast();

          // -- The total sum of all plane codes is: 0 + 1 + 2 + 3 = 6
          // -- We can therefore get the plane code of the last pseudo-layer
          // -- as: 6 - sumOfAllOtherPlaneCodes
          const int pC = goodHit ? planeCode( id.cast() ) : 6 - sumLayArray[t2];
          sumLayArray[t2] += pC;

          const float txUTS = txArray[t2];

          const int begin = hitsInLayers[hitContIndex].layerIndices[pC];
          const int end =
              ( pC == 3 ) ? hitsInLayers[hitContIndex].size() : hitsInLayers[hitContIndex].layerIndices[pC + 1];
          const auto& hitsInL = hitsInLayers[hitContIndex].scalar();
          for ( int index2 = begin; index2 < end; ++index2 ) {
            const float zohit = hitsInL[index2].z().cast();
            if ( zohit == zhit ) continue;

            const float xohit   = hitsInL[index2].x().cast();
            const float xextrap = xhit + txUTS * ( zohit - zhit );
            if ( xohit - xextrap < -m_overlapTol ) continue;
            if ( xohit - xextrap > m_overlapTol ) break;

            if ( nUTHits[t2] >= int( LHCb::Pr::Upstream::Tracks::MaxUTHits ) )
              continue; // get this number from PrUpstreamTracks!!!
            const scalar::int_v utidx = hitsInL[index2].index();
            outputTracks.store_ut_index( currentsize + trackIndex2, nUTHits[t2], utidx );
            LHCb::LHCbID        oid( LHCb::UTChannelID( hitsInL[index2].channelID().cast() ) );
            const scalar::int_v lhcbid = bit_cast<int, unsigned int>( oid.lhcbID() );
            auto const          idIdx  = velo_scalar[veloIdx].nHits().cast() + nUTHits[t2];
            outputTracks.store_lhcbID( currentsize + trackIndex2, idIdx, lhcbid );
            nUTHits[t2] += 1;
            // only one overlap hit
            break; // this should ensure there are never more than 8 hits on the track
          }
          trackIndex2++;
        }
      }
      const simd::int_v n_uthits = nUTHits;
      outputTracks.store<TracksTag::nUTHits>( currentsize, n_uthits, validTrackMask );
    }
  }
} // namespace LHCb::Pr
