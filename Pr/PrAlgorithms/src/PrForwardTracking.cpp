/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// from Gaudi
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiAlg/Transformer.h"

// from LHCb
#include "Event/PrLongTracks.h"
#include "Event/StateParameters.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "FTDet/DeFTDetector.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "LHCbMath/SIMDWrapper.h"

// local
#include "IPrAddUTHitsTool.h"
#include "PrForwardTrack.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrSciFiHits.h"

// NN for ghostprob
#include "weights/TMVA_MLP_Forward1stLoop.h"
#include "weights/TMVA_MLP_Forward2ndLoop.h"

// boost
#include "boost/container/static_vector.hpp"

// standard
#include <algorithm>
#include <array>
#include <functional>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

// ranges
#include "range/v3/numeric/accumulate.hpp"

/**
  ++++++++++++++++++++++++++++++++ Forward Tracking +++++++++++++++++++++++++++++++
*/

/** +++++++++++++++++++++ Short Introduction in geometry ++++++++++++++++++++++++++:
 *
 * The SciFi Tracker Detector, or simple Fibre Tracker (FT) consits out of 3 stations.
 * Each station consists out of 4 planes/layers. Thus there are in total 12 layers,
 * in which a particle can leave a hit. The reasonable maximum number of hits a track
 * can have is thus also 12.
 *
 * Each layer consists out of several Fibre mats. A fibre has a diameter of below a mm.
 * Several fibres are glued alongside each other to form a mat.
 * A Scintilating Fibre produces light, if a charged particle traverses. This light is then
 * detected on the outside of the Fibre mat.
 *
 * Looking from the collision point, one (X-)layer looks like the following:
 *
 *                    y       6m
 *                    ^  ||||||||||||| Upper side
 *                    |  ||||||||||||| 2.5m
 *                    |  |||||||||||||
 *                   -|--||||||o||||||----> -x
 *                       |||||||||||||
 *                       ||||||||||||| Lower side
 *                       ||||||||||||| 2.5m
 *
 * All fibres are aranged parallel to the y-axis. There are three different
 * kinds of layers, denoted by X,U,V. The U/V layers are rotated with respect to
 * the X-layers by +/- 5 degrees, to also get a handle of the y position of the
 * particle. As due to the magnetic field particles are deflected in
 * x-direction (only small deflection in y-direction).
 * The layer structure in the FT is XUVX-XUVX-XUVX.
 *
 * The detector is divided into an upeer and a lower side (>/< y=0). As particles
 * are deflected in x direction there are only very(!) few particles that go
 * from the lower to the upper side, or vice versa. The reconstruction algorithm therefore
 * treats upper and lower side tracks independently such that only hits in the respective
 * detector half are considered.
 * Only for U/V layers this can be different because in these layers the
 * complete fibre modules are rotated, producing a zic-zac pattern at y=0, also
 * called  "the triangles". In these layers hits are also searched for on the "other side",
 * if the track is close to
 * y=0. Sketch (rotation exagerated):
 *                                          _.*
 *     y ^   _.*                         _.*
 *       | .*._      Upper side       _.*._
 *       |     *._                 _.*     *._
 *       |--------*._           _.*           *._----------------> x
 *       |           *._     _.*                 *._     _.*
 *                      *._.*       Lower side      *._.*
 *
 *
 *
 *
 *
 *       Zone ordering defined on PrKernel/PrFTInfo.h
 *
 *     y ^
 *       |    1  3  5  7     9 11 13 15    17 19 21 23
 *       |    |  |  |  |     |  |  |  |     |  |  |  |
 *       |    x  u  v  x     x  u  v  x     x  u  v  x   <-- type of layer
 *       |    |  |  |  |     |  |  |  |     |  |  |  |
 *       |------------------------------------------------> z
 *       |    |  |  |  |     |  |  |  |     |  |  |  |
 *       |    |  |  |  |     |  |  |  |     |  |  |  |
 *       |    0  2  4  6     8 10 12 14    16 18 20 22
 *
 *
 *
 *  A detailed introduction to the basic ideas of the Forward tracking can be
 *   found here:
 *   (2002) http://cds.cern.ch/record/684710/files/lhcb-2002-008.pdf
 *   (2007) http://cds.cern.ch/record/1033584/files/lhcb-2007-015.pdf
 *  The most recent note contains (now slightly outdated) information about parameterisations
 *   (2014) http://cds.cern.ch/record/1641927/files/LHCb-PUB-2014-001.pdf
 *
 *
 * Method overview
 *
 * The track reconstruction is done in several steps:
 *
 * 1) Hough-like transformation of SciFi hits using input tracks
 *    -> projectXHitsToHoughSpace and projectStereoHitsToHoughSpace
 *       filling a histogram-like data structure
 * 2) Threshold scan over "histogram" selecting local accumulations of hits
 *    -> pickUpCandidateBins and sortAndCopyBinContents
 * 3) Detailed selection of sets of x hits matching the input track
 *    -> selextXCandidates
 * 4) Completion of candidates by finding best matching u/v hits
 *    -> selectFullCandidates
 *        also assigning a "quality" to each candidate indicating a ghost
 *        probability
 * 5) Removing duplicates among all found tracks
 *    -> removeDuplicates
 */

/** @class PrForwardTracking PrForwardTracking.cpp
 *
 *  @author Olivier Callot
 *  @date   2012-03-20
 *  @author Thomas Nikodem
 *  @date   2013-03-15
 *  @author Michel De Cian
 *  @date   2014-03-12 Changes with make code more standard and faster
 *  @author Sevda Esen
 *  @date   2015-02-13 additional search in the triangles by Marian Stahl
 *  @author Thomas Nikodem
 *  @date   2016-03-09 complete restructuring
 *  @author Olli Lupton
 *  @date   2018-11-07 Imported PrForwardTool into PrForwardTracking as a step towards making PrForwardTracking accept
 *                     selections
 *  @author André Günther
 *  @date 2019-12-03 adapted PrForwardTracking to new SoA hit class PrSciFiHits
 *  @date 2021-03-07 complete redesign
 */

namespace {
  using namespace Forward;
  using namespace SciFiHits;
  namespace TracksTag = LHCb::Pr::Long::Tag;
  using TracksFT      = LHCb::Pr::Long::Tracks;
  using TracksUT      = LHCb::Pr::Upstream::Tracks;
  using TracksVP      = LHCb::Pr::Velo::Tracks;
  using scalar        = SIMDWrapper::scalar::types;
  using simd          = SIMDWrapper::best::types;

  constexpr float zReference{8520.f};
  // constants for the extrapolation polynomial from Velo to SciFi, used to determine minPT cut border in S3L0
  // and momentum dependent search window in case of VeloUT input
  constexpr std::array toSciFiExtParams{4824.31956565f,  426.26974766f,   7071.08408876f, 12080.38364257f,
                                        14077.79607408f, 13909.31561208f, 9315.34184959f, 3209.49021545f};
  // constants for determination of z position of the magnet kink
  constexpr std::array zMagnetParams{5212.38f, 406.609f, -1102.35f, -498.039f};

  // constants used in track parametrisation
  constexpr std::array xParams{18.6195f, -5.55793f};
  constexpr std::array yParams{-0.667996f, -3.68424e-05f};
  // constants used in momentum determination
  constexpr std::array momentumParams{1.21014f, 0.637339f, -0.200292f, 0.632298f, 3.23793f, -27.0259f};
  // names of variables used in multilayer perceptron used to identify ghosts
  const std::vector<std::string> mlpInputVars{{"nPlanes"}, {"dSlope"}, {"dp"}, {"slope2"}, {"dby"}, {"dbx"}, {"day"}};

  /**
   * @class ZoneCache PrForwardTracking.cpp
   * @brief Caches derived conditions, e.g. positions of SciFi layers
   */
  struct ZoneCache {
    PrFTZoneHandler handler{};
    PrHitZone       lowerLastZone{}, upperLastZone{};
    // access z position of zone by zonenumber as index
    alignas( 64 ) std::array<float, PrFTInfo::NFTZones> zoneZPos{std::numeric_limits<float>::signaling_NaN()};
    alignas( 64 ) std::array<float, PrFTInfo::NFTZones> zoneDzdy{std::numeric_limits<float>::signaling_NaN()};
    alignas( 64 ) std::array<float, PrFTInfo::NFTZones> zoneDxdy{std::numeric_limits<float>::signaling_NaN()};
    ZoneCache( const DeFTDetector& ftDet ) : handler( ftDet ) {
      lowerLastZone = handler.zone( PrFTInfo::NFTZones - 2 );
      upperLastZone = handler.zone( PrFTInfo::NFTZones - 1 );
      for ( int i{0}; i < PrFTInfo::NFTLayers; ++i ) {
        zoneZPos[2 * i]     = handler.zone( 2 * i ).z();
        zoneZPos[2 * i + 1] = handler.zone( 2 * i + 1 ).z();
        zoneDzdy[2 * i]     = handler.zone( 2 * i ).dzDy();
        zoneDzdy[2 * i + 1] = handler.zone( 2 * i + 1 ).dzDy();
        zoneDxdy[2 * i]     = handler.zone( 2 * i ).dxDy();
        zoneDxdy[2 * i + 1] = handler.zone( 2 * i + 1 ).dxDy();
      }
    }
  };

  /**
   * @class PrParametersX PrForwardTracking.cpp
   * @brief Bundles parameters used selection of x candidates
   */
  struct PrParametersX {
    unsigned minXHits{};
    float    maxXWindow{};
    float    maxXWindowSlope{};
    float    maxXGap{};
    unsigned minStereoHits{};
    float    maxChi2PerDoF{};
    float    maxChi2XProjection{};
    float    maxChi2LinearFit{};
  };
  /**
   * @class PrParametersY PrForwardTracking.cpp
   * @brief Bundles parameters used selection of stereo and full candidates
   */
  struct PrParametersY {
    float    tolY{};
    float    tolYSlope{};
    float    yTolUVSearch{};
    float    tolYTriangleSearch{};
    unsigned minStereoHits{};
    float    minYGap{};
    float    maxChi2StereoLinear{};
    float    maxChi2Stereo{};
    float    maxChi2StereoAdd{};
    float    tolYMag{};
    float    tolYMagSlope{};
  };

  template <typename TrackType>
  auto get_ancestors( const TrackType& input_tracks ) {
    const TracksVP*                  velo_ancestors{nullptr};
    const TracksUT*                  upstream_ancestors{nullptr};
    const LHCb::Pr::Seeding::Tracks* seed_ancestors{nullptr};
    if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
      velo_ancestors     = input_tracks.getVeloAncestors();
      upstream_ancestors = &input_tracks;
    } else {
      velo_ancestors = &input_tracks;
    }
    return std::tuple{velo_ancestors, upstream_ancestors, seed_ancestors};
  }

  /**
   * @brief remove hits from range if found on track
   * @param idx1 first index of range
   * @param idxEnd end index of range
   * @param track track containing hits that are removed from range
   * @details Note that idxEnd is modified by this function because hits are removed by shuffling them to the
   * end of the range and simply moving the end index such that they are not contained anymore.
   * @return true if range is not empty after removing hits
   */
  auto removeUsedHits( int idx1, int& idxEnd, const TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& track,
                       ModSciFiHits::ModPrSciFiHitsSOA& allXHits ) {
    const auto& hitsOnTrack = track.getCoordsToFit();
    for ( auto idx{idx1}; idx < idxEnd; idx += simd::size ) {
      const auto loopMask  = simd::loop_mask( idx, idxEnd );
      auto       keepMask  = loopMask;
      const auto fulldexes = allXHits.fulldex<simd::int_v>( idx );
      for ( auto iHit : hitsOnTrack ) {
        const auto foundMask = fulldexes == iHit;
        keepMask             = keepMask && !foundMask;
      }
      const auto coords = allXHits.coord<simd::float_v>( idx );
      allXHits.compressstore_fulldex( idx1, keepMask, fulldexes );
      allXHits.compressstore_coord( idx1, keepMask, coords );
      idx1 += simd::popcount( keepMask );
      if ( any( !loopMask ) ) {
        allXHits.store_fulldex( idx, select( loopMask, allXHits.fulldex<simd::int_v>( idx ), fulldexes ) );
        allXHits.store_coord( idx, select( loopMask, allXHits.coord<simd::float_v>( idx ), coords ) );
      }
    }
    const auto ok = idx1 != idxEnd;
    idxEnd        = idx1;
    return ok;
  }

  /**
   * @brief calculates the final estimate of charge over momentum
   * @param track fitted PrForwardTrack
   * @param magScaleFactor polarity and relative magnitude of B field
   * @param veloSeed velo track information
   */
  template <typename VeloSeedExtended>
  auto calculateQoP( const PrForwardTrack& track, float magScaleFactor, const VeloSeedExtended& veloSeed ) {
    const auto bx   = track.getXParams().get( 1 );
    const auto bx2  = bx * bx;
    const auto obs  = {bx2, bx2 * bx2, bx * veloSeed.seed.tx, veloSeed.seed.ty2, veloSeed.seed.ty2 * veloSeed.seed.ty2};
    const auto coef = std::inner_product( std::begin( obs ), std::end( obs ), std::next( std::begin( momentumParams ) ),
                                          momentumParams[0] );
    const auto proj = std::sqrt( ( 1.f + veloSeed.seed.slope2 ) / ( 1.f + veloSeed.seed.tx2 ) );
    assert( std::abs( magScaleFactor ) > 0.f && "magScaleFactor = 0 dividing by zero -> infinite momentum!" );
    return ( veloSeed.seed.tx - bx ) / ( coef * proj * magScaleFactor * float{Gaudi::Units::GeV} );
  }

  /**
   * @brief used to count number of same hits in two tracks
   * @details fulfills https://en.cppreference.com/w/cpp/named_req/OutputIterator
   * and is used in a std::set_intersection
   */
  struct counting_inserter {
    int                count{0};
    counting_inserter& operator++() { return *this; }
    counting_inserter& operator*() { return *this; }
    template <typename T>
    counting_inserter& operator=( const T& ) {
      ++count;
      return *this;
    }
  };

  /**
   * @class yShiftCalculator PrForwardTracking.cpp
   * @brief Handles correction of y position which comes from small small z component of B field
   * @details shamelessly stolen from the SciFiTrackForwarding
   * for more info on how the y correction works see e.g.
   * https://indico.cern.ch/event/810764/contributions/3394473/attachments/1830241/2997521/Fast_Upgrade_Forward_Tracking.pdf
   */
  struct yShiftCalculator {

    yShiftCalculator( float tx, float ty ) {
      const auto ty3 = ty * ty * ty;
      const auto tx2 = tx * tx;
      const auto tx3 = tx * tx * tx;
      m_term1 = ( m_dYParams[1] * ty + m_dYParams[3] * tx2 * ty + m_dYParams[5] * ty3 + m_dYParams[7] * tx2 * ty3 );
      m_term2 = ( m_dYParams[0] + m_dYParams[2] * tx * ty + m_dYParams[4] * tx3 * ty + m_dYParams[6] * tx * ty3 );
    }

    auto calcYCorrection( simd::float_v dSlope, int layer ) const {
      return ( m_term1 * abs( dSlope ) + m_term2 * dSlope ) * m_scales[layer];
    }

  private:
    static constexpr std::array m_dYParams{3.78837f, 73.1636f, 7353.89f,  -6347.68f,
                                           20270.3f, 3721.02f, -46038.2f, 230943.f};
    alignas( 64 ) static constexpr std::array m_scales{Forward::nan, 0.615f, 0.63f, Forward::nan,
                                                       Forward::nan, 0.83f,  0.85f, Forward::nan,
                                                       Forward::nan, 1.005f, 1.03f, Forward::nan};
    float m_term1{0.f};
    float m_term2{0.f};
  };

  /**
   * @brief initialises parameters of x projection parameterisation
   */
  template <typename Candidate, typename VeloSeedExtended>
  void initXFitParameters( Candidate& trackCandidate, const VeloSeedExtended& veloSeed ) {
    const auto xAtRef           = trackCandidate.xAtRef();
    auto       dSlope           = ( xAtRef - veloSeed.xStraightAtRef ) / ( zReference - veloSeed.zMag );
    const auto zMag             = veloSeed.zMag + zMagnetParams[1] * dSlope * dSlope;
    const auto xMag             = veloSeed.seed.x( zMag );
    const auto slopeT           = ( xAtRef - xMag ) / ( zReference - zMag );
    dSlope                      = slopeT - veloSeed.seed.tx;
    trackCandidate.getXParams() = {xAtRef, slopeT, 1.e-6f * xParams[0] * dSlope, 1.e-9f * xParams[1] * dSlope};
  }

  /**
   * @brief initialises parameters of y projection parameterisation
   */
  template <typename Candidate, typename VeloSeedExtended>
  void initYFitParameters( Candidate& trackCandidate, const VeloSeedExtended& veloSeed, const ZoneCache& cache ) {
    const auto dSlope           = trackCandidate.getXParams().get( 1 ) - veloSeed.seed.tx;
    const auto dyCoef           = dSlope * dSlope * veloSeed.seed.ty;
    trackCandidate.getYParams() = {veloSeed.yStraightAtRef, veloSeed.seed.ty + dyCoef * yParams[0],
                                   dyCoef * yParams[1]};
    auto& betterDz              = trackCandidate.getBetterDz();
    for ( auto zone : PrFTInfo::xZonesLower ) {
      zone += veloSeed.upperHalfTrack;
      const auto dz       = cache.zoneZPos[zone] - zReference;
      betterDz[zone / 4u] = dz + cache.zoneDzdy[zone] * trackCandidate.y( dz );
    }
  }

  /**
   * @class HoughTransformation PrForwardTracking.cpp
   * @brief Contains data and methods used by the Hough-transformation-like step in the Forward Tracking
   * Main methods are defined outside of class
   * Short methods are defined inline
   */
  class HoughTransformation {
  public:
    static constexpr int      minBinOffset{2};
    static constexpr int      nBins{1020 + minBinOffset};
    static constexpr int      reservedBinContent{16};
    static constexpr float    rangeMax{3000.f};
    static constexpr float    rangeMin{-3000.f};
    static constexpr unsigned nDiffPlanesBits{8};
    static constexpr unsigned diffPlanesFlags{0xFF}; // 1111 1111
    static constexpr unsigned uvFlags{0x666};        // 0110 0110 0110
    static constexpr unsigned xFlags{0x999};         // 1001 1001 1001
    static_assert( sizeof( unsigned ) >= 4 );        // at the moment at least 32 bit datatype for plane encoding
    static_assert( ( reservedBinContent % simd::size == 0 ) &&
                   "Currently only multiples of avx2 vector width supported" );

    template <typename VeloSeedExtended>
    void projectStereoHitsToHoughSpace( const VeloSeedExtended&, const ZoneCache&, std::pair<float, float>,
                                        const PrSciFiHits& );
    template <typename VeloSeedExtended>
    void projectXHitsToHoughSpace( const VeloSeedExtended&, std::pair<float, float>, const PrSciFiHits& );
    auto pickUpCandidateBins( int, int, int, int );
    template <typename Container>
    void sortAndCopyBinContents( int, Container& );
    /**
     * @brief clears containers for size and therefore also content, plane counter and candidates
     */
    void clear() {
      m_candSize = 0;
      m_planeCounter.fill( 0 );
      m_binContentSize.fill( 0 );
    }

    /**
     * @brief Calculates bin numbers for x values in "Hough space".
     * @param xAtRef x values in "Hough space", i.e. on reference plane.
     * @param idx index of x hit
     * @param maxIdx last index/end of range
     * @return bin numbers
     * @details The binning is non-simple and follows the occupancy in the detector. Furthermore, only values within
     * a certain range are considered. x values outside of this range will cause a return of bin number 0 the "garbage
     * bin". The binning function looks similar to a sigmoid, however, a simpler function is used because of better
     * performance. The function is i(x) = p0 + p3 + (p1 * x) / (1 + abs(p2 * x)). To avoid out-of-bounds access at the
     * edges, the function is shifted by p3.
     */
    auto calculateBinNumber( simd::float_v xAtRef, int idx, int maxIdx ) const {

      const auto boundaryMask      = xAtRef < rangeMax && xAtRef > rangeMin && simd::loop_mask( idx, maxIdx );
      const auto floatingBinNumber = ( binningParams[0] + binningParams[3] ) +
                                     ( binningParams[1] * xAtRef ) / ( 1.f + binningParams[2] * abs( xAtRef ) );

      const auto binNumber = static_cast<simd::int_v>( floatingBinNumber );
      return select( boundaryMask, binNumber, 0 );
    }

    SOA_ACCESSOR( planes, m_planeCounter.data() )
    SOA_ACCESSOR( candidates, m_candidateBins.data() )
    SOA_ACCESSOR( binSize, m_binContentSize.data() )
    SOA_ACCESSOR( binPerm, m_binPermutations.data() )

    /**
     * @brief Performs a compressed store of candidates and handles size incrementation.
     * @param mask Mask of candidates to store.
     * @param bins The bin number(s) qualifying as candidates.
     */
    void compressstoreCandidates( simd::mask_v mask, simd::int_v bins ) {
      compressstore_candidates( m_candSize, mask, bins );
      m_candSize += simd::popcount( mask );
    }

    /**
     * @brief Decodes number of different planes encoded in number stored in plane counting array.
     * @param bin The bin number
     */
    template <typename I>
    auto decodeNbDifferentPlanes( int bin ) const {
      return planes<I>( bin ) & diffPlanesFlags;
    }

    /**
     * @brief Make a span for one bin
     * @param iBin bin number
     * @return span for the coordinates in this bin
     */
    auto getBinContentCoord( int iBin ) {
      return LHCb::span{m_binContentCoord}.subspan( iBin * reservedBinContent, m_binContentSize[iBin] );
    }

    /**
     * @brief Make a span for one bin
     * @param iBin bin number
     * @return span for the PrSciFiHits indices in this bin
     */
    auto getBinContentFulldex( int iBin ) {
      return LHCb::span{m_binContentFulldex}.subspan( iBin * reservedBinContent, m_binContentSize[iBin] );
    }
    auto getBinContentFulldex( int iBin ) const {
      return LHCb::span{m_binContentFulldex}.subspan( iBin * reservedBinContent, m_binContentSize[iBin] );
    }

    /**
     * @brief Sorts and then copies content of a bin to a new container
     * @param bin bin number
     * @param allXHits container to copy to (SoA)
     * @details The sorting is done by an insertion sort because the number of elements to sort is small by design.
     * The sorting is done by storing the permutations and then using a gather operation to shuffle according to the
     * permutations, followed by the storing.
     */
    template <typename Container>
    void sortAndCopyBin( int bin, Container& allXHits ) {
      if ( const auto binSize = m_binContentSize[bin]; binSize ) {
        std::iota( m_binPermutations.begin(), m_binPermutations.end(), 0 );
        const auto coordContent = getBinContentCoord( bin );
        for ( auto iCoord{1}; iCoord < binSize; ++iCoord ) {
          const auto insertVal = coordContent[iCoord];
          auto       insertPos = iCoord;
          for ( auto movePos = iCoord; movePos && insertVal < coordContent[m_binPermutations[--movePos]];
                --insertPos ) {
            m_binPermutations[insertPos] = m_binPermutations[movePos];
          }
          m_binPermutations[insertPos] = iCoord;
        }
        const auto fulldexContent = getBinContentFulldex( bin );
        const auto currentSize    = allXHits.size();
        auto       i{0};
        do {
          const auto permut_v  = binPerm<simd::int_v>( i );
          const auto fulldex_v = gather( fulldexContent.data(), permut_v );
          const auto coord_v   = gather( coordContent.data(), permut_v );
          allXHits.store_fulldex( currentSize + i, fulldex_v );
          allXHits.store_coord( currentSize + i, coord_v );
          i += simd::size;
        } while ( i < binSize );
        allXHits.updateSizeBy( binSize );
      }
    }

  private:
    /**
     * @property array of parameters that define the non-simple binning of the "Hough-Histogram"
     */
    static constexpr std::array binningParams{5.05675291e+02f, 4.53316164e-01f, 5.57027462e-04f, 5.5f};

    /**
     * @property Contains the the number of unique planes within a bin encoded together with bit flags for each plane.
     */
    alignas( 64 ) std::array<int, SciFiHits::align_size( nBins )> m_planeCounter{};
    /**
     * @property Contain the coordinates (x hits on reference plane) and PrSciFiHits indices in each bin
     */
    alignas( 64 ) std::array<float, SciFiHits::align_size( nBins ) * reservedBinContent> m_binContentCoord{};
    alignas( 64 ) std::array<int, SciFiHits::align_size( nBins ) * reservedBinContent> m_binContentFulldex{};
    /**
     * @property Contains the number of hits in each bin
     */
    alignas( 64 ) std::array<int, SciFiHits::align_size( nBins )> m_binContentSize{};
    /**
     * @property sstorage that contains temporary permutations from insertion sort
     */
    alignas( 64 ) std::array<int, reservedBinContent> m_binPermutations{};
    /**
     * @property Contains a selection of bins that qualify as part of a track candidates.
     * @details The number of candidates is tracked by candSize.
     */
    alignas( 64 ) std::array<int, SciFiHits::align_size( nBins )> m_candidateBins{};
    int m_candSize{0};
  };

  /**
   * @brief separate hits that are alone on their SciFi plane from ones that have friends on the plane
   * @param otherPlanes Container keeping track of the plane numbers containing multiples
   * @param protoCand the candidate under consideration
   * @details single hits are directly put into coordsToFit container inside protoCand
   */
  template <typename Container, typename VeloSeedExtended>
  [[gnu::always_inline]] inline void
  separateSingleHitsForFit( Container& otherPlanes, TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& protoCand,
                            const VeloSeedExtended& veloSeed ) {
    otherPlanes.clear();
    for ( auto iPlane{0}; iPlane < PrFTInfo::NFTXLayers; ++iPlane ) {
      if ( protoCand.planeSize( iPlane ) == 1 ) {
        const auto idx = protoCand.getIdx( iPlane * protoCand.planeMulti );
        protoCand.addHitForLineFit( idx, veloSeed );
      } else if ( protoCand.planeSize( iPlane ) ) {
        otherPlanes.push_back( iPlane );
      }
    }
  }

  /**
   * @brief add hits to candidate from planes that have multiple hits
   * @param otherPlanes Container keeping track of the plane numbers containing multiples
   * @param protoCand the candidate under consideration
   * @details only one hit per plane is allowed, selected by smallest chi2 calculated
   * using a straight line fit and putting them into coordsToFit container inside protoCand
   */
  template <typename Container, typename VeloSeedExtended>
  [[gnu::always_inline]] inline void addBestOtherHits( const Container&                                 otherPlanes,
                                                       TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& protoCand,
                                                       const VeloSeedExtended&                          veloSeed ) {
    for ( auto iPlane : otherPlanes ) {
      const auto idxSpan = protoCand.getIdxSpan( iPlane );
      assert( idxSpan.size() > 0 );
      const auto bestIdx = [&] {
        auto best{0};
        auto bestChi2{std::numeric_limits<float>::max()};
        for ( auto idx : idxSpan ) {
          const auto chi2 = protoCand.lineChi2( idx, veloSeed );
          if ( chi2 < bestChi2 ) {
            bestChi2 = chi2;
            best     = idx;
          }
        }
        return best;
      }();
      protoCand.addHitForLineFit( bestIdx, veloSeed );
      protoCand.setPlaneSize( iPlane, 1 );
      protoCand.solveLineFit();
    }
  }

  /**
   * @brief store all hits present in candidate for fitting
   */
  template <typename Container>
  void prepareAllHitsForFit( const Container& allXHits, TrackCandidate<Container>& protoCand ) {
    for ( auto iPlane{0}; iPlane < PrFTInfo::NFTXLayers; ++iPlane ) {
      const auto idxSpan = protoCand.getIdxSpan( iPlane );
      std::transform( idxSpan.begin(), idxSpan.end(), std::back_inserter( protoCand.getCoordsToFit() ),
                      [&]( auto idx ) {
                        protoCand.xAtRef() += allXHits.coord( idx );
                        return allXHits.fulldex( idx );
                      } );
    }
    protoCand.xAtRef() /= static_cast<float>( protoCand.getCoordsToFit().size() );
  }

  /**
   * @brief fit linear parameters of parameterisation in x and remove bad hits
   * @param protoCand candidate to fit
   * @param pars bundle of parameters that tune hit removal
   * @param SciFiHits PrSciFiHits container
   * @details although only the linear parameters are fitted, the full parameterisation is used meaning
   * that the quadratic and the cubic term are fixed to their initial values.
   */
  template <bool secondLoop, typename VeloSeedExtended>
  void fitLinearXProjection( TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& protoCand, const PrParametersX& pars,
                             const PrSciFiHits& SciFiHits, const VeloSeedExtended& veloSeed ) {

    auto& coordsToFit = protoCand.getCoordsToFit();
    bool  fit{true};
    while ( fit ) {
      float s0{0.f}, sz{0.f}, sz2{0.f}, sd{0.f}, sdz{0.f};
      for ( auto iHit : coordsToFit ) {
        const auto dz = veloSeed.betterZ[SciFiHits.planeCode( iHit )] - zReference;
        const auto d  = SciFiHits.x( iHit ) - protoCand.x( dz );
        const auto w  = SciFiHits.w( iHit );
        s0 += w;
        sz += w * dz;
        sz2 += w * dz * dz;
        sd += w * d;
        sdz += w * d * dz;
      }
      const auto den = sz * sz - s0 * sz2;
      if ( den == 0.f ) return;
      const auto da = ( sdz * sz - sd * sz2 ) / den;
      const auto db = ( sd * sz - s0 * sdz ) / den;
      protoCand.addXParams<2>( std::array{da, db} );

      const auto itEnd = coordsToFit.end();
      auto       worst = itEnd;
      auto       maxChi2{0.f};
      const bool notMultiple = protoCand.nDifferentPlanes() == coordsToFit.size();
      for ( auto itH = coordsToFit.begin(); itH != itEnd; ++itH ) {
        const auto d = SciFiHits.x( *itH ) - protoCand.x( veloSeed.betterZ[SciFiHits.planeCode( *itH )] - zReference );
        const auto chi2 = d * d * SciFiHits.w( *itH );
        if ( chi2 > maxChi2 && ( notMultiple || protoCand.nInSamePlane( *itH ) > 1 ) ) {
          maxChi2 = chi2;
          worst   = itH;
        }
      }
      fit = false;
      if ( const int ip = SciFiHits.planeCode( *worst ) / 2u;
           maxChi2 > pars.maxChi2LinearFit || protoCand.planeSize( ip ) > 1 ) {
        protoCand.removePlane( ip );
        std::iter_swap( worst, std::prev( itEnd ) );
        coordsToFit.pop_back();
        if ( coordsToFit.size() < pars.minXHits ) return;
        fit = true;
      }
    }
  }

  /**
   * @brief try to find an x hit on a still empty x plane that matches the track
   * @param protoCand track candidate
   * @param pars bundle of parameters that tune the window defining if an x hit matches or not
   * @param veloSeed extended velo track
   * @param SciFiHits PrSciFiHits container
   * @param maxChi2XAdd defines the maximum allowed chi2 deviation from track
   * @details Hits are only added if they are within a window around the extrapolated position on the plane
   * under consideration and do not deviate too much in chi2.
   */
  template <bool secondLoop, typename VeloSeedExtended>
  auto fillEmptyXPlanes( TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& protoCand, const PrParametersX& pars,
                         const VeloSeedExtended& veloSeed, const PrSciFiHits& SciFiHits, float maxChi2XAdd ) {

    if ( protoCand.nDifferentPlanes() == PrFTInfo::NFTXLayers ) return false;

    bool       added{false};
    const auto xAtRef = protoCand.getXParams().get( 0 );
    const auto xWindow =
        pars.maxXWindow + ( std::abs( xAtRef ) + std::abs( xAtRef - veloSeed.xStraightAtRef ) ) * pars.maxXWindowSlope;

    for ( auto iPlane{0}; iPlane < PrFTInfo::NFTXLayers; ++iPlane ) {
      if ( protoCand.planeSize( iPlane ) ) continue;
      const auto pc             = PrFTInfo::xLayers[iPlane];
      const auto [xStart, xEnd] = SciFiHits.getZoneIndices( 2 * pc + veloSeed.upperHalfTrack );

      const auto xPred = protoCand.x( veloSeed.betterZ[pc] - zReference );

      const auto startwin = SciFiHits.get_lower_bound_fast<4>( xStart, xEnd, xPred - xWindow );
      auto       endwin{startwin};
      auto       bestChi2{maxChi2XAdd};
      auto       best{0};
      for ( const auto maxX = xPred + xWindow; SciFiHits.x( endwin ) <= maxX; ++endwin ) {
        const auto d = SciFiHits.x( endwin ) - xPred;
        if ( const auto chi2 = d * d * SciFiHits.w( endwin ); chi2 < bestChi2 ) {
          bestChi2 = chi2;
          best     = endwin;
        }
      }
      if ( best ) {
        protoCand.getCoordsToFit().push_back( best );
        ++protoCand.nDifferentPlanes();
        ++protoCand.planeSize( iPlane );
        added = true;
      }
    }
    return added;
  }
  /**
   * @brief try to find an x hit on a still empty x plane that matches the track
   * @param track track candidate
   * @param pars bundle of parameters that tune the window defining if an x hit matches or not
   * @param veloSeed extended velo track
   * @param SciFiHits PrSciFiHits container
   * @param maxChi2XAdd defines the maximum allowed chi2 deviation from track
   * @details Hits are only added if they are within a window around the extrapolated position on the plane
   * under consideration and do not deviate too much in chi2. The empty planes are determined first.
   */
  template <bool secondLoop, typename VeloSeedExtended>
  auto fillEmptyXPlanes( PrForwardTrack& track, const PrParametersX& pars, const VeloSeedExtended& veloSeed,
                         const PrSciFiHits& SciFiHits, float maxChi2XAdd ) {

    if ( track.size() == PrFTInfo::NFTLayers ) return false;
    auto&                             coordsToFit = track.getCoordsToFit();
    std::bitset<PrFTInfo::NFTXLayers> hasXLayer{};
    auto                              nX{0};
    for ( auto iHit : coordsToFit ) {
      if ( const auto pc = SciFiHits.planeCode( iHit ); PrFTInfo::isXLayer[pc] ) {
        hasXLayer.set( pc / 2u );
        ++nX;
      }
    }
    if ( nX == PrFTInfo::NFTXLayers ) return false;
    bool       added{false};
    const auto xAtRef = track.getXParams().get( 0 );
    const auto xWindow =
        pars.maxXWindow + ( std::abs( xAtRef ) + std::abs( xAtRef - veloSeed.xStraightAtRef ) ) * pars.maxXWindowSlope;

    for ( auto iPlane{0}; iPlane < PrFTInfo::NFTXLayers; ++iPlane ) {
      if ( hasXLayer.test( iPlane ) ) continue;
      const auto pc             = PrFTInfo::xLayers[iPlane];
      const auto [xStart, xEnd] = SciFiHits.getZoneIndices( 2 * pc + veloSeed.upperHalfTrack );

      const auto xPred = track.x( veloSeed.betterZ[pc] - zReference );

      const auto startwin = SciFiHits.get_lower_bound_fast<4>( xStart, xEnd, xPred - xWindow );
      auto       endwin{startwin};
      auto       bestChi2{maxChi2XAdd};
      auto       best{0};
      for ( const auto maxX = xPred + xWindow; SciFiHits.x( endwin ) <= maxX; ++endwin ) {
        const auto d = SciFiHits.x( endwin ) - xPred;
        if ( const auto chi2 = d * d * SciFiHits.w( endwin ); chi2 < bestChi2 ) {
          bestChi2 = chi2;
          best     = endwin;
        }
      }
      if ( best ) {
        track.getCoordsToFit().push_back( best );
        added = true;
      }
    }
    return added;
  }

  /**
   * @brief fit up to quadratic parameters of parameterisation in x and remove bad hits
   * @param track candidate to fit
   * @param pars bundle of parameters that tune hit removal
   * @param SciFiHits PrSciFiHits container
   * @details although only the parameters up to the quadratic term are fitted, the full parameterisation is used
   * meaning that the cubic term is fixed to its initial value.
   */
  auto fitXProjection( TrackCandidate<ModSciFiHits::ModPrSciFiHitsSOA>& track, const PrParametersX& pars,
                       const PrSciFiHits& SciFiHits ) {
    auto&      coordsToFit = track.getCoordsToFit();
    const auto minHits     = pars.minXHits;

    bool fit{true};
    while ( fit ) {
      // plus one because we are fitting all params but one
      const auto nDoF = coordsToFit.size() - track.nXPars + 1;
      if ( nDoF < 1 ) return false;
      auto s0{0.f}, sz{0.f}, sz2{0.f}, sz3{0.f}, sz4{0.f}, sd{0.f}, sdz{0.f}, sdz2{0.f};
      for ( auto iHit : coordsToFit ) {
        const auto dzNoScale = track.getBetterDz( iHit );
        const auto d         = track.distanceXHit( iHit, dzNoScale );
        const auto w         = SciFiHits.w( iHit );
        const auto dz        = .001f * dzNoScale;
        const auto dz2       = dz * dz;
        const auto wdz       = w * dz;
        s0 += w;
        sz += wdz;
        sz2 += wdz * dz;
        sz3 += wdz * dz2;
        sz4 += wdz * dz2 * dz;
        sd += w * d;
        sdz += wdz * d;
        sdz2 += w * d * dz2;
      }
      const auto b1  = sz * sz - s0 * sz2;
      const auto c1  = sz2 * sz - s0 * sz3;
      const auto d1  = sd * sz - s0 * sdz;
      const auto b2  = sz2 * sz2 - sz * sz3;
      const auto c2  = sz3 * sz2 - sz * sz4;
      const auto d2  = sdz * sz2 - sz * sdz2;
      const auto den = b1 * c2 - b2 * c1;
      if ( den == 0.f ) return false;
      const auto db = ( d1 * c2 - d2 * c1 ) / den;
      const auto dc = ( d2 * b1 - d1 * b2 ) / den;
      const auto da = ( sd - db * sz - dc * sz2 ) / s0;
      track.addXParams<3>( std::array{da, db * 1.e-3f, dc * 1.e-6f} );

      auto maxChi2{0.f};
      auto totChi2{0.f};

      const auto itEnd = coordsToFit.end();
      auto       worst = itEnd;
      for ( auto itH = coordsToFit.begin(); itH != itEnd; ++itH ) {
        const auto chi2 = track.chi2XHit( *itH, track.getBetterDz( *itH ) );
        totChi2 += chi2;
        if ( chi2 > maxChi2 ) {
          maxChi2 = chi2;
          worst   = itH;
        }
      }
      track.setChi2NDoF( {totChi2, static_cast<float>( nDoF )} );

      if ( worst == itEnd ) return true;

      fit = false;
      if ( totChi2 > pars.maxChi2PerDoF * nDoF || maxChi2 > pars.maxChi2XProjection ) {
        track.removeFromPlane( *worst );
        std::iter_swap( worst, std::prev( itEnd ) );
        coordsToFit.pop_back();
        if ( coordsToFit.size() < minHits ) return false;
        fit = true;
      }
    }
    return true;
  }

  /**
   * @brief fit up to quadratic parameters of parameterisation in x and remove bad hits
   * @param track candidate to fit
   * @param pars bundle of parameters that tune hit removal
   * @param SciFiHits PrSciFiHits container
   * @details although only the parameters up to the quadratic term are fitted, the full parameterisation is used
   * meaning that the cubic term is fixed to its initial value.
   */
  auto fitXProjection( PrForwardTrack& track, const PrParametersX& pars, const PrSciFiHits& SciFiHits ) {
    auto&      coordsToFit = track.getCoordsToFit();
    const auto minHits     = pars.minXHits + pars.minStereoHits;

    bool fit{true};
    while ( fit ) {
      // plus one because we are fitting all params but one
      const auto nDoF = coordsToFit.size() - track.nXPars + 1;
      if ( nDoF < 1 ) return false;
      auto s0{0.f}, sz{0.f}, sz2{0.f}, sz3{0.f}, sz4{0.f}, sd{0.f}, sdz{0.f}, sdz2{0.f};
      for ( auto iHit : coordsToFit ) {
        const auto dzNoScale = track.getBetterDz( iHit, SciFiHits.z( iHit ) - zReference, SciFiHits );
        const auto d         = track.distance( iHit, dzNoScale, SciFiHits );
        const auto w         = SciFiHits.w( iHit );
        const auto dz        = .001f * dzNoScale;
        const auto dz2       = dz * dz;
        const auto wdz       = w * dz;
        s0 += w;
        sz += wdz;
        sz2 += wdz * dz;
        sz3 += wdz * dz2;
        sz4 += wdz * dz2 * dz;
        sd += w * d;
        sdz += wdz * d;
        sdz2 += w * d * dz2;
      }
      const auto b1  = sz * sz - s0 * sz2;
      const auto c1  = sz2 * sz - s0 * sz3;
      const auto d1  = sd * sz - s0 * sdz;
      const auto b2  = sz2 * sz2 - sz * sz3;
      const auto c2  = sz3 * sz2 - sz * sz4;
      const auto d2  = sdz * sz2 - sz * sdz2;
      const auto den = b1 * c2 - b2 * c1;
      if ( den == 0.f ) return false;
      const auto db = ( d1 * c2 - d2 * c1 ) / den;
      const auto dc = ( d2 * b1 - d1 * b2 ) / den;
      const auto da = ( sd - db * sz - dc * sz2 ) / s0;
      track.addXParams<3>( std::array{da, db * 1.e-3f, dc * 1.e-6f} );

      auto maxChi2{0.f};
      auto totChi2{0.f};

      const auto itEnd = coordsToFit.end();
      auto       worst = itEnd;
      for ( auto itH = coordsToFit.begin(); itH != itEnd; ++itH ) {
        const auto dz   = track.getBetterDz( *itH, SciFiHits.z( *itH ) - zReference, SciFiHits );
        const auto chi2 = track.chi2( *itH, dz, SciFiHits );
        totChi2 += chi2;
        if ( chi2 > maxChi2 ) {
          maxChi2 = chi2;
          worst   = itH;
        }
      }
      track.setChi2NDoF( {totChi2, static_cast<float>( nDoF )} );

      if ( worst == itEnd ) return true;

      fit = false;
      if ( totChi2 > pars.maxChi2PerDoF * nDoF || maxChi2 > pars.maxChi2XProjection ) {
        std::iter_swap( worst, std::prev( itEnd ) );
        coordsToFit.pop_back();
        if ( coordsToFit.size() < minHits ) return false;
        fit = true;
      }
    }
    return true;
  }

  /**
   * @brief Collect hits from the triangle zone.
   */
  template <typename Container>
  [[gnu::noinline]] void collectTriangleHits( int zoneNumberOS, float xMin, float xMax, float dxDySign,
                                              float xPredShifted, float yInZone, Container& stereoHits,
                                              const PrSciFiHits& SciFiHits, const PrParametersY& pars ) {

    const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumberOS );
    const auto iUVStart         = SciFiHits.get_lower_bound_fast<4>( uvStart, uvEnd, xMin );
    const auto yMin             = yInZone + pars.yTolUVSearch;
    const auto yMax             = yInZone - pars.yTolUVSearch;
    auto       triangleOK       = [&]( int index ) {
      return ( yMax < SciFiHits.yMax( index ) ) && ( yMin > SciFiHits.yMin( index ) );
    };
    for ( auto iUV{iUVStart}; SciFiHits.x( iUV ) <= xMax; ++iUV ) {
      const auto size     = stereoHits.size();
      const auto signedDx = ( SciFiHits.x( iUV ) - xPredShifted ) * dxDySign;
      if ( triangleOK( iUV ) && size < stereoHits.capacity() ) {
        stereoHits.modHits[size] = {signedDx, iUV};
        stereoHits.updateSizeBy( 1 );
      } else if ( size >= stereoHits.capacity() ) {
        return;
      }
    }
  }

  /**
   * @brief Collect hits from stereo layers that match the x candidate
   * @param stereoHits container to store the stereo hits
   * @param track x candidate
   * @param SciFiHits PrSciFiHits container
   * @param veloSeed velo track extension
   * @param cache cache for zone properties
   * @param pars parameters that tune the window used for collection.
   * @details stereo hits are collected using a narrow window around the position of the track extrapolated
   * to the layer under consideration. If the track is close to the triangle zones, also check these for hits.
   * The coordinate stored for the stereo hits is the distance to the x track multiplied by the sign of the tangens
   * of the stereo angle of the plane.
   */
  template <typename Container, typename VeloSeedExtended>
  void collectStereoHits( Container& stereoHits, const PrForwardTrack& track, const PrSciFiHits& SciFiHits,
                          const VeloSeedExtended& veloSeed, const ZoneCache& cache, const PrParametersY& pars ) {

    const auto& uvZones = veloSeed.upperHalfTrack ? PrFTInfo::uvZonesUpper : PrFTInfo::uvZonesLower;
    for ( auto zoneNumber : uvZones ) {
      const auto zZone        = cache.zoneZPos[zoneNumber];
      const auto yInZone      = track.y( zZone - zReference );
      const auto betterZ      = zZone + yInZone * cache.zoneDzdy[zoneNumber];
      const auto xPred        = track.x( betterZ - zReference );
      const auto dxDy         = cache.zoneDxdy[zoneNumber];
      const auto xPredShifted = xPred - yInZone * dxDy;
      const auto dxTol = pars.tolY + pars.tolYSlope * ( std::abs( xPred - veloSeed.xStraightInZone[zoneNumber / 2u] ) +
                                                        std::abs( yInZone ) );
      const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumber );
      const auto xMin             = xPredShifted - dxTol;
      const auto xMax             = xPredShifted + dxTol;
      // the difference only is interepreation: currently: negative dx * sign means underestimated y position and vice
      // versa but it makes a difference for the sorting!
      const auto dxDySign = std::copysign( 1.f, dxDy );
      const auto iUVStart = SciFiHits.get_lower_bound_fast<4>( uvStart, uvEnd, xMin );
      for ( auto iUV{iUVStart}; SciFiHits.x( iUV ) <= xMax; ++iUV ) {
        const auto size     = stereoHits.size();
        const auto signedDx = ( SciFiHits.x( iUV ) - xPredShifted ) * dxDySign;
        if ( LIKELY( size < stereoHits.capacity() ) ) {
          stereoHits.modHits[size] = {signedDx, iUV};
          stereoHits.updateSizeBy( 1 );
        } else {
          return;
        }
      }
      if ( std::abs( yInZone ) < pars.tolYTriangleSearch ) {
        const auto zoneNumberOS = veloSeed.upperHalfTrack ? zoneNumber - 1 : zoneNumber + 1;
        collectTriangleHits( zoneNumberOS, xMin, xMax, dxDySign, xPredShifted, yInZone, stereoHits, SciFiHits, pars );
      }
    }
  }

  /**
   * @brief fit linear parameters of parameterisation in y and remove bad hits
   * @param track candidate to fit
   * @param pars bundle of parameters that tune hit removal
   * @param SciFiHits PrSciFiHits container
   * @details although only the linear parameters are fitted, the full parameterisation is used meaning
   * that the quadratic term is fixed to its initial values.
   */
  auto fitLinearYProjection( TrackCandidate<ModSciFiHits::ModPrSciFiHitsAOS<32>>& track, const PrParametersY& pars,
                             const PrSciFiHits& SciFiHits ) {

    auto& coordsToFit = track.getCoordsToFit();
    auto  fit{true};
    while ( fit ) {
      auto s0{0.f}, sz{0.f}, sz2{0.f}, sd{0.f}, sdz{0.f};
      for ( auto iHit : coordsToFit ) {
        const auto dz = track.calcBetterDz( iHit );
        const auto d  = track.distanceStereoHit( iHit, dz );
        const auto w  = SciFiHits.w( iHit );
        s0 += w;
        sz += w * dz;
        sz2 += w * dz * dz;
        sd += w * d;
        sdz += w * d * dz;
      }
      const auto den = s0 * sz2 - sz * sz;
      if ( den == 0.f ) return false;
      const auto da = ( sd * sz2 - sdz * sz ) / den;
      const auto db = ( sdz * s0 - sd * sz ) / den;
      track.addYParams<2>( std::array{da, db} );

      const auto itEnd = coordsToFit.end();
      auto       worst = itEnd;
      auto       maxChi2{0.f};
      for ( auto itH = coordsToFit.begin(); itH != itEnd; ++itH ) {
        const auto chi2 = track.chi2StereoHits( *itH );
        if ( chi2 > maxChi2 ) {
          maxChi2 = chi2;
          worst   = itH;
        }
      }
      if ( worst == itEnd ) return true;
      fit = false;
      if ( maxChi2 > pars.maxChi2StereoLinear ) {
        if ( coordsToFit.size() < pars.minStereoHits + 1 ) return false;
        track.removeFromPlane( *worst );
        std::iter_swap( worst, std::prev( itEnd ) );
        coordsToFit.pop_back();
        fit = true;
      }
    }
    return true;
  }

  /**
   * @brief fit parameters of parameterisation in y and remove bad hits
   * @param track candidate to fit
   * @param pars bundle of parameters that tune hit removal
   * @param SciFiHits PrSciFiHits container
   */
  template <typename VeloSeedExtended>
  auto fitYProjection( TrackCandidate<ModSciFiHits::ModPrSciFiHitsAOS<32>>& track, const PrParametersY& pars,
                       const PrSciFiHits& SciFiHits, const VeloSeedExtended& veloSeed ) {

    const auto tolYMag =
        pars.tolYMag + pars.tolYMagSlope * std::abs( track.getXParams().get( 0 ) - veloSeed.xStraightAtRef );
    const auto wMag        = 1.f / ( tolYMag * tolYMag );
    const auto dzMagRef    = .001f * veloSeed.dzMagRef;
    auto&      coordsToFit = track.getCoordsToFit();
    bool       fit{true};
    while ( fit ) {
      assert( pars.minStereoHits >= coordsToFit.size() - track.nYPars );
      const auto dyMag = track.yStraight( veloSeed.zMag ) - veloSeed.yStraightAtMag;
      auto       s0    = wMag;
      auto       sz    = wMag * dzMagRef;
      auto       sz2   = wMag * dzMagRef * dzMagRef;
      auto       sd    = wMag * dyMag;
      auto       sdz   = wMag * dyMag * dzMagRef;
      auto       sz2m{0.f}, sz3{0.f}, sz4{0.f}, sdz2{0.f};

      for ( auto iHit : coordsToFit ) {
        const auto dzNoScale = track.calcBetterDz( iHit );
        const auto d         = track.distanceStereoHit( iHit, dzNoScale );
        const auto w         = SciFiHits.w( iHit );
        const auto dz        = .001f * dzNoScale;
        const auto dzdz      = dz * dz;
        const auto wdz       = w * dz;
        s0 += w;
        sz += wdz;
        sz2m += w * dzdz;
        sz2 += w * dzdz;
        sz3 += wdz * dzdz;
        sz4 += wdz * dz * dzdz;
        sd += w * d;
        sdz += wdz * d;
        sdz2 += wdz * d * dz;
      }
      const auto b1  = sz * sz - s0 * sz2;
      const auto c1  = sz2m * sz - s0 * sz3;
      const auto d1  = sd * sz - s0 * sdz;
      const auto b2  = sz2 * sz2m - sz * sz3;
      const auto c2  = sz3 * sz2m - sz * sz4;
      const auto d2  = sdz * sz2m - sz * sdz2;
      const auto den = b1 * c2 - b2 * c1;
      if ( den == 0.f ) return false;
      const auto db = ( d1 * c2 - d2 * c1 ) / den;
      const auto dc = ( d2 * b1 - d1 * b2 ) / den;
      const auto da = ( sd - db * sz - dc * sz2 ) / s0;
      track.addYParams<3>( std::array{da, db * 1.e-3f, dc * 1.e-6f} );

      const auto itEnd = coordsToFit.end();
      auto       worst = itEnd;
      auto       maxChi2{0.f};
      for ( auto itH = coordsToFit.begin(); itH != itEnd; ++itH ) {
        const auto chi2 = track.chi2StereoHits( *itH );
        if ( chi2 > maxChi2 ) {
          maxChi2 = chi2;
          worst   = itH;
        }
      }
      if ( worst == itEnd ) return true;
      fit = false;
      if ( maxChi2 > pars.maxChi2Stereo ) {
        if ( coordsToFit.size() - 1 < pars.minStereoHits ) return false;
        track.removeFromPlane( *worst );
        std::iter_swap( worst, std::prev( itEnd ) );
        coordsToFit.pop_back();
        fit = true;
      }
    }
    return true;
  }

  /**
   * @brief check for hits in triangle zones
   */
  [[gnu::noinline]] auto checkTriangleOnEmptyPlane( int iPlane, int zoneNumberOS, float xMin, float xMax,
                                                    float xPredShifted, float yInZone,
                                                    TrackCandidate<ModSciFiHits::ModPrSciFiHitsAOS<32>>& track,
                                                    const PrSciFiHits& SciFiHits, const PrParametersY& pars ) {

    const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumberOS );
    const auto iUVStart         = SciFiHits.get_lower_bound_fast<4>( uvStart, uvEnd, xMin );
    const auto yMin             = yInZone + pars.yTolUVSearch;
    const auto yMax             = yInZone - pars.yTolUVSearch;
    auto       triangleOK       = [&]( int index ) {
      return ( yMax < SciFiHits.yMax( index ) ) && ( yMin > SciFiHits.yMin( index ) );
    };
    auto bestChi2{pars.maxChi2Stereo};
    auto best{0};
    for ( auto iUV{iUVStart}; SciFiHits.x( iUV ) <= xMax; ++iUV ) {
      const auto d = SciFiHits.x( iUV ) - xPredShifted;
      if ( const auto chi2 = d * d * SciFiHits.w( iUV ); chi2 < bestChi2 && triangleOK( iUV ) ) {
        bestChi2 = chi2;
        best     = iUV;
      }
    }
    if ( best ) {
      track.getCoordsToFit().push_back( best );
      ++track.nDifferentPlanes();
      ++track.planeSize( iPlane );
      return true;
    }
    return false;
  }

  /**
   * @brief try to find an stereo hit on a still empty stereo plane that matches the track
   * @param track track candidate
   * @param SciFiHits PrSciFiHits container
   * @param veloSeed extended velo track
   * @param cache cache for zone properties
   * @param pars bundle of parameters that tune the window defining if an stereo hit matches or not
   * @details Hits are only added if they are within a window around the extrapolated position on the plane
   * under consideration and do not deviate too much in chi2. If the track is close to the triangle zones,
   * these are also checked for hits.
   */
  template <typename VeloSeedExtended>
  auto fillEmptyStereoPlanes( TrackCandidate<ModSciFiHits::ModPrSciFiHitsAOS<32>>& track, const PrSciFiHits& SciFiHits,
                              const VeloSeedExtended& veloSeed, const ZoneCache& cache, const PrParametersY& pars ) {

    if ( track.nDifferentPlanes() == PrFTInfo::NFTUVLayers ) return false;
    bool added{false};
    for ( auto iPlane{0}; iPlane < PrFTInfo::NFTUVLayers; ++iPlane ) {
      if ( track.planeSize( iPlane ) ) continue;
      const auto pc           = PrFTInfo::stereoLayers[iPlane];
      const auto zoneNumber   = 2 * pc + veloSeed.upperHalfTrack;
      const auto zZone        = cache.zoneZPos[zoneNumber];
      const auto betterZ      = zZone + track.y( zZone - zReference ) * cache.zoneDzdy[zoneNumber];
      const auto yInZone      = track.y( betterZ - zReference );
      const auto xPred        = track.x( betterZ - zReference );
      const auto xPredShifted = xPred - yInZone * cache.zoneDxdy[zoneNumber];
      const auto dxTol =
          pars.tolY + pars.tolYSlope * ( std::abs( xPred - veloSeed.xStraightInZone[pc] ) + std::abs( yInZone ) );
      const auto [uvStart, uvEnd] = SciFiHits.getZoneIndices( zoneNumber );
      const auto xMin             = xPredShifted - dxTol;
      const auto xMax             = xPredShifted + dxTol;
      const auto iUVStart         = SciFiHits.get_lower_bound_fast<4>( uvStart, uvEnd, xMin );
      auto       bestChi2{pars.maxChi2StereoAdd};
      auto       best{0};
      for ( auto iUV{iUVStart}; SciFiHits.x( iUV ) <= xMax; ++iUV ) {
        const auto d = SciFiHits.x( iUV ) - xPredShifted;
        if ( const auto chi2 = d * d * SciFiHits.w( iUV ); chi2 < bestChi2 ) {
          bestChi2 = chi2;
          best     = iUV;
        }
      }
      if ( best ) {
        track.getCoordsToFit().push_back( best );
        ++track.nDifferentPlanes();
        ++track.planeSize( iPlane );
        added = true;
      }
      if ( !added && std::abs( yInZone ) < pars.tolYTriangleSearch ) {
        const auto zoneNumberOS = veloSeed.upperHalfTrack ? zoneNumber - 1 : zoneNumber + 1;
        added = checkTriangleOnEmptyPlane( iPlane, zoneNumberOS, xMin, xMax, xPredShifted, yInZone, track, SciFiHits,
                                           pars );
      }
    }
    return added;
  }

  /**
   * @brief stereo hit selection for given x candidate
   * @details The idea is to check for stereo hits that have a similar deviation form the x track. This takes into
   * account that the y position is not well known and thus often over- or underestimated which leads to similar
   * systematic shifts of the stereo hits in x and hence similar distances to the x track. A best set of stereo hits is
   * selected by using the total chi2 of the y projection fit and the total number of stereo hits found.
   */
  template <typename Container, typename VeloSeedExtended>
  auto selectStereoHits( PrForwardTrack& track, TrackCandidate<ModSciFiHits::ModPrSciFiHitsAOS<32>>& stereoCand,
                         const PrSciFiHits& SciFiHits, const Container& stereoHits, const PrParametersY& yPars,
                         const ZoneCache& cache, const VeloSeedExtended& veloSeed ) {

    static_vector<int, PrFTInfo::NFTUVLayers> bestStereoHits{};
    TrackPars<3>                              bestYParams{};
    auto                                      bestMeanDy2{std::numeric_limits<float>::max()};
    const auto                                idxEnd = stereoHits.size();

    stereoCand.getXParams() = track.getXParams();
    // unsigned to avoid warning
    for ( unsigned idx1{0}; idx1 + yPars.minStereoHits <= idxEnd; ++idx1 ) {
      auto idx2{idx1};
      auto sumCoord{0.f};
      stereoCand.clear();
      for ( ; idx2 < idxEnd && stereoCand.nDifferentPlanes() < yPars.minStereoHits; ++idx2 ) {
        stereoCand.addHit( idx2 );
        sumCoord += stereoHits.coord( idx2 );
      }
      if ( idx2 == idxEnd && stereoCand.nDifferentPlanes() < yPars.minStereoHits ) break;
      // TODO: test median
      idx2 = [&] {
        for ( auto idxLast = idx2 - 1; idx2 < idxEnd; ++idx2 ) {
          const auto smallGap = stereoHits.coord( idx2 ) - stereoHits.coord( idxLast ) < yPars.minYGap;
          const auto inWindow =
              ( idx2 - idx1 ) * ( stereoHits.coord( idx1 ) + stereoHits.coord( idx2 ) ) < 2 * sumCoord;
          if ( !( smallGap || ( stereoCand.planeEmpty( idx2 ) && inWindow ) ) ) return idx2;
          stereoCand.addHit( idx2 );
          sumCoord += stereoHits.coord( idx2 );
          idxLast = idx2;
        }
        return idx2;
      }();

      const auto mean = sumCoord / static_cast<float>( idx2 - idx1 );
      for ( auto iPlane{0}; iPlane < PrFTInfo::NFTUVLayers; ++iPlane ) {
        if ( stereoCand.planeSize( iPlane ) ) {
          const auto idxSpan = stereoCand.getIdxSpan( iPlane );
          auto       bestIdx = idxSpan[0];
          if ( stereoCand.planeSize( iPlane ) > 1 ) {
            auto bestDist{std::numeric_limits<float>::max()};
            for ( auto idx : idxSpan ) {
              const auto dist = std::abs( stereoHits.coord( idx ) - mean );
              if ( dist < bestDist ) {
                bestIdx  = idx;
                bestDist = dist;
              }
            }
            stereoCand.setPlaneSize( iPlane, 1 );
          }
          stereoCand.getCoordsToFit().push_back( stereoHits.fulldex( bestIdx ) );
        }
      }

      stereoCand.getYParams() = track.getYParams();
      if ( !fitLinearYProjection( stereoCand, yPars, SciFiHits ) ) continue;
      if ( !fitYProjection( stereoCand, yPars, SciFiHits, veloSeed ) ) continue;

      if ( fillEmptyStereoPlanes( stereoCand, SciFiHits, veloSeed, cache, yPars ) ) {
        if ( !fitYProjection( stereoCand, yPars, SciFiHits, veloSeed ) ) continue;
      }
      auto& candidateHits = stereoCand.getCoordsToFit();
      if ( candidateHits.size() >= bestStereoHits.size() ) {
        const auto meanDy2 = std::accumulate( candidateHits.begin(), candidateHits.end(), 0.f,
                                              [&]( auto init, auto iHit ) {
                                                const auto dz = stereoCand.calcBetterDz( iHit );
                                                const auto d  = stereoCand.distanceStereoHit( iHit, dz );
                                                return init + d * d;
                                              } ) /
                             static_cast<float>( candidateHits.size() - 1 );
        if ( candidateHits.size() > bestStereoHits.size() || meanDy2 < bestMeanDy2 ) {
          bestYParams = stereoCand.getYParams();
          bestMeanDy2 = meanDy2;
          bestStereoHits.clear();
          std::copy( candidateHits.begin(), candidateHits.end(), std::back_inserter( bestStereoHits ) );
        }
      }
    }
    if ( !bestStereoHits.empty() ) {
      track.getYParams() = bestYParams;
      track.addHits( bestStereoHits );
      return true;
    }
    return false;
  }

} // namespace

template <typename TrackType>
class PrForwardTracking
    : public Gaudi::Functional::Transformer<TracksFT( const PrSciFiHits&, const TrackType&, const ZoneCache& ),
                                            LHCb::DetDesc::usesConditions<ZoneCache>> {
public:
  using base_class_t =
      Gaudi::Functional::Transformer<TracksFT( const PrSciFiHits&, const TrackType&, const ZoneCache& ),
                                     LHCb::DetDesc::usesConditions<ZoneCache>>;
  using base_class_t::addConditionDerivation;
  using base_class_t::debug;
  using base_class_t::error;
  using base_class_t::info;
  using base_class_t::inputLocation;
  using base_class_t::msgLevel;
  using ErrorCounter = Gaudi::Accumulators::MsgCounter<MSG::ERROR>;

  /********************************************************************************
   * Standard Constructor, initialise member variables (such as NN variables) here.
   * Paths are figured out by the scheduler.
   ********************************************************************************/
  PrForwardTracking( std::string const& name, ISvcLocator* pSvcLocator )
      : base_class_t(
            name, pSvcLocator,
            std::array{typename base_class_t::KeyValue{"SciFiHits", ""},
                       typename base_class_t::KeyValue{"InputTracks", ""},
                       typename base_class_t::KeyValue{"ZoneCache", "AlgorithmSpecific-" + name + "-ZoneCache"}},
            typename base_class_t::KeyValue{"OutputTracks", ""} )
      , m_MLPReader_1st{mlpInputVars}
      , m_MLPReader_2nd{mlpInputVars} {}

  /********************************************************************************
   * Initialisation, set some member variables and cache
   ********************************************************************************/
  StatusCode initialize() override {
    auto sc = base_class_t::initialize();
    if ( sc.isFailure() ) return sc;
    this->template addConditionDerivation<ZoneCache( const DeFTDetector& )>(
        {DeFTDetectorLocation::Default}, this->template inputLocation<ZoneCache>() );
    return sc;
  }

  // main call
  TracksFT operator()( const PrSciFiHits&, const TrackType&, const ZoneCache& ) const override;
  auto     forwardTracks( const TrackType&, const PrSciFiHits&, const ZoneCache& ) const;

  auto calculateMomentumBorders( const VeloSeed&, float ) const;

  template <bool secondLoop, typename Container, typename VeloSeedExtended>
  void selectXCandidates( Container&, const VeloSeedExtended&, ModSciFiHits::ModPrSciFiHitsSOA&, const PrSciFiHits&,
                          const PrParametersX&, const ZoneCache& ) const;

  template <bool secondLoop, typename Container, typename Container2, typename VeloSeedExtended>
  auto selectFullCandidates( Container&, Container2&, PrParametersX&, const ZoneCache&, const VeloSeedExtended&,
                             const PrSciFiHits&, float ) const;

  template <typename Container>
  auto removeDuplicates( Container&, const PrSciFiHits& ) const;

  template <typename Container, typename Container2>
  auto makeLHCbLongTracks( Container&&, Container2&&, const TrackType& ) const;

  template <bool enable = false, typename... Args>
  void debug( Args&&... args ) const {
    if constexpr ( enable ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { ( ( debug() << std::forward<Args>( args ) << " " ), ... ) << endmsg; }
    }
  }

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_inputTracksCnt{this, "Input tracks"};
  mutable Gaudi::Accumulators::SummingCounter<> m_acceptedInputTracksCnt{this, "Accepted input tracks"};
  mutable Gaudi::Accumulators::SummingCounter<> m_outputTracksCnt{this, "Created long tracks"};

  // avoid sign comparison warning
  Gaudi::Property<unsigned> m_minTotalHits{this, "MinTotalHits", 10};
  Gaudi::Property<bool>     m_secondLoop{this, "SecondLoop", true};

  Gaudi::Property<float> m_minPT{this, "MinPT", 50.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_minP{this, "MinP", 1500.f * Gaudi::Units::MeV};
  Gaudi::Property<int>   m_minPlanesCompl{this, "MinPlanesCompl", 10};
  Gaudi::Property<int>   m_minPlanes{this, "MinPlanes", 4};
  Gaudi::Property<int>   m_minPlanesComplX{this, "MinPlanesComplX", 4};
  Gaudi::Property<int>   m_minPlanesComplUV{this, "MinPlanesComplUV", 4};

  // first loop Hough Cluster search
  // unsigned to avoid warnings
  Gaudi::Property<unsigned> m_minXHits{this, "MinXHits", 5};
  Gaudi::Property<float>    m_maxXWindow{this, "MaxXWindow", 1.2f * Gaudi::Units::mm};
  Gaudi::Property<float>    m_maxXWindowSlope{this, "MaxXWindowSlope", 0.002f * Gaudi::Units::mm};
  Gaudi::Property<float>    m_maxXGap{this, "MaxXGap", 1.2f * Gaudi::Units::mm};
  Gaudi::Property<int>      m_minSingleHits{this, "MinSingleHits", 2};
  Gaudi::Property<float>    m_minInterval{this, "MinInterval", 1.f * Gaudi::Units::mm};

  // second loop Hough Cluster search
  // unsigned to avoid warnings
  Gaudi::Property<unsigned> m_minXHits2nd{this, "MinXHits2nd", 4};
  Gaudi::Property<float>    m_maxXWindow2nd{this, "MaxXWindow2nd", 1.5f * Gaudi::Units::mm};
  Gaudi::Property<float>    m_maxXWindowSlope2nd{this, "MaxXWindowSlope2nd", 0.002f * Gaudi::Units::mm};
  Gaudi::Property<float>    m_maxXGap2nd{this, "MaxXGap2nd", 0.5f * Gaudi::Units::mm};

  Gaudi::Property<float> m_maxChi2LinearFit{this, "MaxChi2LinearFit", 100.f};
  Gaudi::Property<float> m_maxChi2XProjection{this, "MaxChi2XProjection", 15.f};
  Gaudi::Property<float> m_maxChi2PerDoF{this, "MaxChi2PerDoF", 7.f};
  Gaudi::Property<float> m_maxChi2XAddLinear{this, "MaxChi2XAddLinear", 2000.f};
  Gaudi::Property<float> m_maxChi2XAddFull{this, "MaxChi2XAddFull", 500.f};

  Gaudi::Property<float> m_tolYUVSearch{this, "YTolUVsearch", 11.f * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolY{this, "TolY", 5.f * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYSlope{this, "TolYSlope", 0.002f * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYMag{this, "TolYMag", 10.f * Gaudi::Units::mm};
  Gaudi::Property<float> m_tolYMagSlope{this, "TolYMagSlope", 0.015f * Gaudi::Units::mm};
  Gaudi::Property<float> m_minYGap{this, "MinYGap", 0.4f * Gaudi::Units::mm};
  Gaudi::Property<float> m_maxChi2StereoLinear{this, "MaxChi2StereoLinear", 60.f};
  Gaudi::Property<float> m_maxChi2Stereo{this, "MaxChi2Stereo", 4.5f};
  Gaudi::Property<float> m_maxChi2StereoAdd{this, "MaxChi2StereoAdd", 4.5f};

  // unsigned to avoid warnings
  Gaudi::Property<unsigned> m_minStereoHits{this, "MinStereoHits", 4};
  Gaudi::Property<float>    m_tolYTriangleSearch{this, "TolYTriangleSearch", PrFTInfo::triangleHeight};

  // momentum preselection if preselection is set and veloUT tracks
  Gaudi::Property<float> m_preselectionMinPT{this, "PreselectionMinPT", 425.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_preselectionMinP{this, "PreselectionMinP", 2500.f * Gaudi::Units::MeV};

  // Momentum guided search window switch
  Gaudi::Property<bool>  m_useMomentumSearchWindow{this, "UseMomentumSearchWindow", false};
  Gaudi::Property<bool>  m_useWrongSignWindow{this, "UseWrongSignWindow", true};
  Gaudi::Property<float> m_wrongSignPT{this, "WrongSignPT", 2000.f * Gaudi::Units::MeV};

  // calculate an upper error boundary on the tracks x-prediction,
  Gaudi::Property<float> m_upperLimitOffset{this, "UpperLimitOffset", 100.f};
  Gaudi::Property<float> m_upperLimitSlope{this, "UpperLimitSlope", 2800.f};
  Gaudi::Property<float> m_upperLimitMax{this, "UpperLimitMax", 600.f};
  Gaudi::Property<float> m_upperLimitMin{this, "UpperLimitMin", 150.f};
  // same as above for the lower limit
  Gaudi::Property<float> m_lowerLimitOffset{this, "LowerLimitOffset", 50.f};
  Gaudi::Property<float> m_lowerLimitSlope{this, "LowerLimitSlope", 1400.f};
  Gaudi::Property<float> m_lowerLimitMax{this, "LowerLimitMax", 600.f};

  // Track Quality (Neural Net)
  Gaudi::Property<float> m_maxQuality{this, "MaxQuality", 0.9f};
  Gaudi::Property<float> m_deltaQuality{this, "DeltaQuality", 0.1f};

  // duplicate removal
  Gaudi::Property<float> m_minXInterspace{this, "MinXInterspace", 50.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_minYInterspace{this, "MinYInterspace", 100.f * Gaudi::Units::MeV};
  Gaudi::Property<float> m_maxCommonFrac{this, "MaxCommonFrac", 0.4f};

  ToolHandle<IPrAddUTHitsTool>  m_addUTHitsTool{this, "AddUTHitsToolName", "PrAddUTHitsTool"};
  ServiceHandle<ILHCbMagnetSvc> m_magFieldSvc{this, "MagneticFieldService", "MagneticFieldSvc"};
  ReadMLP_Forward1stLoop        m_MLPReader_1st;
  ReadMLP_Forward2ndLoop        m_MLPReader_2nd;
};

DECLARE_COMPONENT_WITH_ID( PrForwardTracking<TracksUT>, "PrForwardTracking" )
DECLARE_COMPONENT_WITH_ID( PrForwardTracking<TracksVP>, "PrForwardTrackingVelo" )

/**
 * @brief Main execution of the Forward Tracking.
 * @param SciFiHits SOAContainer containing SciFi hits.
 * @param input_tracks Tracks that shall be extended to SciFi stations. Either Velo tracks or Upstream tracks.
 * @param cache Cache containing useful information about SciFi zones.
 */
template <typename TrackType>
TracksFT PrForwardTracking<TrackType>::operator()( const PrSciFiHits& SciFiHits, const TrackType& input_tracks,
                                                   const ZoneCache& cache ) const {

  if ( UNLIKELY( input_tracks.size() == 0 ) ) {
    return std::make_from_tuple<TracksFT>( get_ancestors( input_tracks ) );
  }
  auto candidates = forwardTracks( input_tracks, SciFiHits, cache );

  const auto idSets = removeDuplicates( candidates, SciFiHits );

  return makeLHCbLongTracks( std::move( candidates ), std::move( idSets ), input_tracks );
}

/**
 * @brief Main work of the Hough transformation: projecting hits to "Hough Space".
 * @param veloSeed Contains useful information about the input track.
 * @param momentumBorders Only hits within a search window are projected, defined by this pair of bounds.
 * @details This method is very heavy and should be as optimised as possible. It calculates the projection of
 * each x hit on each SciFi layer within the serach window to the reference plane. For each hit a bin number is
 * calculated and the necessary information about the hit is stored in the containers of the histogram.
 */
template <typename VeloSeedExtended>
void HoughTransformation::projectXHitsToHoughSpace( const VeloSeedExtended& veloSeed,
                                                    std::pair<float, float> momentumBorders,
                                                    const PrSciFiHits&      SciFiHits ) {

  const auto dxMin = momentumBorders.first;
  const auto dxMax = momentumBorders.second;

  // get z position for station 3 layer 0 because that's where the initial search window is defined
  const auto zS3L0     = veloSeed.betterZ[8];
  const auto dzInvS3L0 = 1.f / ( zS3L0 - veloSeed.zMag );

  for ( const auto iLayer : PrFTInfo::xLayers ) {
    const auto zZone = veloSeed.betterZ[iLayer];
    // serach window size is scaled by distance to magnet kink position
    const auto searchWindowScaling = ( zZone - veloSeed.zMag ) * dzInvS3L0;

    const auto xStraightInZone = veloSeed.xStraightInZone[iLayer];

    const auto xMin = xStraightInZone + dxMin * searchWindowScaling;
    const auto xMax = xStraightInZone + dxMax * searchWindowScaling;

    // lower zone numbers are always even, corresponding upper half number has + 1, i.e. add boolean
    // get zone indices from hit container
    auto [minIdx, maxIdx] = SciFiHits.getZoneIndices( 2 * iLayer + veloSeed.upperHalfTrack );

    // binary search to find indicies corresponding to search window
    minIdx = SciFiHits.get_lower_bound_fast<2>( minIdx, maxIdx, xMin );
    maxIdx = SciFiHits.get_lower_bound_fast<4>( minIdx, maxIdx, xMax );

    const auto tx               = veloSeed.seed.tx;
    const auto dSlopeDivPart    = 1.f / ( zZone - veloSeed.zMag );
    const auto dz               = 1.e-3f * ( zZone - zReference );
    const auto dz2              = 1.e-6f * ( zZone - zReference ) * ( zZone - zReference );
    const auto xCorrectionConst = dz2 * ( xParams[0] + dz * xParams[1] );
    // encode plane in bits above first nDiffPlanesBits (8) bits, this will be used to check if plane is already used
    const auto encodedPlaneNumber = 1u << ( iLayer + nDiffPlanesBits );

    for ( auto i{minIdx}; i < maxIdx; i += simd::size ) {
      const auto hits        = SciFiHits.x<simd::float_v>( i );
      const auto dSlope      = ( hits - xStraightInZone ) * dSlopeDivPart;
      const auto zMagPrecise = veloSeed.zMag + zMagnetParams[1] * dSlope * dSlope;
      const auto xMag        = xStraightInZone + tx * ( zMagPrecise - zZone );
      // using a lower precision division (rcp) here for a bit more speed in this hot loop
      // this changes the resulting projected hit position by O(0.1mm) which is less than
      // the resolution of the parameterised projection and thus fine
      const auto ratio       = ( zReference - zMagPrecise ) * rcp( zZone - zMagPrecise );
      const auto xCorrection = xCorrectionConst * dSlope;
      // position of hits is moved by a correction to account for curvature
      const auto projHit         = xMag + ratio * ( hits - xCorrection - xMag );
      const auto binNumber       = calculateBinNumber( projHit, i, maxIdx );
      const auto encodedPlaneCnt = gather_planes( binNumber );
      // OR with current plane to set flag if plane is not present in the bin yet
      const auto encodedPlaneCntNewFlag = encodedPlaneCnt | encodedPlaneNumber;
      // remember: the first nDiffPlanesBits (8) encode the number of different planes
      // so let's simply check if we added a flag, and if so, add 1 to number of diff planes
      const auto newEncodedPlaneCnt =
          encodedPlaneCntNewFlag + select( encodedPlaneCnt < encodedPlaneCntNewFlag, simd::int_v{1}, 0 );
      // scattering is not supported by avx2, do it by hand TODO: implement avx512 version in SIMDWrapper
      const auto binNumberBase = SIMDWrapper::to_array( binNumber );
      const auto planeCntBase  = SIMDWrapper::to_array( newEncodedPlaneCnt );
      const auto projHitBase   = SIMDWrapper::to_array( projHit );
      // the bins are static, always check that they do not overflow
      LHCb::Utils::unwind<0, simd::size>( [&]( int idx ) {
        if ( const auto iBin = binNumberBase[idx]; LIKELY( m_binContentSize[iBin] < reservedBinContent ) ) {
          m_planeCounter[iBin]         = planeCntBase[idx];
          const auto iBinIdx           = reservedBinContent * iBin + m_binContentSize[iBin]++;
          m_binContentCoord[iBinIdx]   = projHitBase[idx];
          m_binContentFulldex[iBinIdx] = i + idx;
        }
      } );
    } // end of loop over hits
    // bin 0 is the garbage bin, it contains out of range hits, let's empty it
    m_binContentSize[0] = 0;
  } // end of loop over zones
}

/**
 * @brief Main work of the Hough transformation: projecting hits to "Hough Space".
 * @param veloSeed Contains useful information about the input track.
 * @param cache cache for zone properties
 * @param momentumBorders Only hits within a search window are projected, defined by this pair of bounds.
 * @details This method is very heavy and should be as optimised as possible. It calculates the projection of
 * each stereo hit on each SciFi layer within the serach window to the reference plane. For each hit a bin number is
 * calculated and the necessary information about the hit is stored in the containers of the histogram.
 */
template <typename VeloSeedExtended>
void HoughTransformation::projectStereoHitsToHoughSpace( const VeloSeedExtended& veloSeed, const ZoneCache& cache,
                                                         std::pair<float, float> momentumBorders,
                                                         const PrSciFiHits&      SciFiHits ) {

  const auto dxMin = momentumBorders.first;
  const auto dxMax = momentumBorders.second;

  const auto zS3L0     = veloSeed.betterZ[8];
  const auto dzInvS3L0 = 1.f / ( zS3L0 - veloSeed.zMag );

  // initialise numbers used by calculation of y position correction
  const yShiftCalculator yc{veloSeed.seed.tx, veloSeed.seed.ty};

  for ( const auto iLayer : PrFTInfo::stereoLayers ) {
    const auto zZone               = veloSeed.betterZ[iLayer];
    const auto searchWindowScaling = ( zZone - veloSeed.zMag ) * dzInvS3L0;
    // dxdy is what makes stereo hits shifty
    const auto dxDy            = cache.zoneDxdy[2 * iLayer + veloSeed.upperHalfTrack];
    const auto xShiftAtY       = veloSeed.seed.y( zZone ) * dxDy;
    const auto xStraightInZone = veloSeed.xStraightInZone[iLayer];

    const auto xMin = xStraightInZone - xShiftAtY + dxMin * searchWindowScaling;
    const auto xMax = xStraightInZone - xShiftAtY + dxMax * searchWindowScaling;

    auto [minIdx, maxIdx] = SciFiHits.getZoneIndices( 2 * iLayer + veloSeed.upperHalfTrack );

    minIdx = SciFiHits.get_lower_bound_fast<2>( minIdx, maxIdx, xMin );
    maxIdx = SciFiHits.get_lower_bound_fast<4>( minIdx, maxIdx, xMax );

    const auto tx                 = veloSeed.seed.tx;
    const auto dSlopeDivPart      = 1.f / ( zZone - veloSeed.zMag );
    const auto dz                 = 1.e-3f * ( zZone - zReference );
    const auto dz2                = 1.e-6f * ( zZone - zReference ) * ( zZone - zReference );
    const auto xCorrectionConst   = dz2 * ( xParams[0] + dz * xParams[1] );
    const auto encodedPlaneNumber = 1u << ( iLayer + nDiffPlanesBits );
    for ( auto i{minIdx}; i < maxIdx; i += simd::size ) {
      // first shift hits by total shift determined by straight line y extrapolation
      auto       hits   = SciFiHits.x<simd::float_v>( i ) + xShiftAtY;
      const auto dSlope = ( hits - xStraightInZone ) * dSlopeDivPart;
      // then correct shift using parameterisation
      hits                   = hits + yc.calcYCorrection( dSlope, iLayer ) * dxDy;
      const auto zMagPrecise = veloSeed.zMag + zMagnetParams[1] * dSlope * dSlope;
      const auto xMag        = xStraightInZone + tx * ( zMagPrecise - zZone );
      // using a lower precision division (rcp) here for a bit more speed in this hot loop
      // this changes the resulting projected hit position by O(0.1mm) which is less than
      // the resolution of the parameterised projection and thus fine
      const auto ratio                  = ( zReference - zMagPrecise ) * rcp( zZone - zMagPrecise );
      const auto xCorrection            = xCorrectionConst * dSlope;
      const auto projHit                = xMag + ratio * ( hits - xCorrection - xMag );
      const auto binNumber              = calculateBinNumber( projHit, i, maxIdx );
      const auto encodedPlaneCnt        = gather_planes( binNumber );
      const auto encodedPlaneCntNewFlag = encodedPlaneCnt | encodedPlaneNumber;
      const auto newEncodedPlaneCnt =
          encodedPlaneCntNewFlag + select( encodedPlaneCnt < encodedPlaneCntNewFlag, simd::int_v{1}, 0 );

      const auto binNumberBase = SIMDWrapper::to_array( binNumber );
      const auto planeCntBase  = SIMDWrapper::to_array( newEncodedPlaneCnt );
      // stereo hits only have their plane scattered to the bins
      LHCb::Utils::unwind<0, simd::size>( [&]( int idx ) { m_planeCounter[binNumberBase[idx]] = planeCntBase[idx]; } );
    } // end of loop over hits
  }   // end of loop over zones
}

/**
 * @brief Sort each picked up candidate bin and copy content to modifiable hit container.
 * @param neighbourCheck number of different planes in bin up to which both direct neighbours are also sorted and copied
 * @param allXHits the container to copy to
 * @details Checking of neighbours makes it necessary to take care of not sorting and selecting a bin twice. This is
 * simply done by storing the last two copied bin numbers. The start and end indices of each candidates are stored for
 * direct candidate access later. If there's an overlap between different candidates' bins, the start
 * and end index is removed which means that the index range for both candidates is merged.
 */
template <typename Container>
void HoughTransformation::sortAndCopyBinContents( int neighbourCheck, Container& allXHits ) {

  auto lastBinCopied{0};
  auto last2ndBinCopied{0};
  for ( auto iCand{0}; iCand < m_candSize; ++iCand ) {
    const auto iBin = m_candidateBins[iCand];
    assert( iBin );
    const int nDiff = decodeNbDifferentPlanes<scalar::int_v>( iBin ).cast();
    // TODO: also check neighbours if two times more hits than planes or so!
    const auto checkNeighbours = nDiff <= neighbourCheck;
    allXHits.candidateStartIndex.push_back( allXHits.size() );
    if ( checkNeighbours ) {
      if ( iBin - 1 != lastBinCopied && iBin - 1 != last2ndBinCopied ) {
        sortAndCopyBin( iBin - 1, allXHits );
      } else {
        assert( !allXHits.candidateStartIndex.empty() );
        // merge candidate index range with previous candidate
        allXHits.candidateStartIndex.pop_back();
        allXHits.candidateEndIndex.pop_back();
      }

      if ( iBin != lastBinCopied && iBin != last2ndBinCopied ) { sortAndCopyBin( iBin, allXHits ); }
      sortAndCopyBin( iBin + 1, allXHits );

    } else if ( iBin != lastBinCopied ) {
      sortAndCopyBin( iBin, allXHits );
    } else {
      // we already got this bin included in the previous range
      allXHits.candidateStartIndex.pop_back();
      allXHits.candidateEndIndex.pop_back();
    }
    allXHits.candidateEndIndex.push_back( allXHits.size() );
    lastBinCopied    = iBin + checkNeighbours;
    last2ndBinCopied = iBin;
  }
}

/**
 * @brief Scanning through encoded planes in each bin to select promising candidates.
 * @param minPlanes Threshold number of different planes in bin.
 * @param minPlanesComplX Threshold number of different x planes including complementary x planes from neighbour bins.
 * @param minPlanesComplUV Threshold number of different stereo planes including complementary stereo planes from
 * neighbour bins.
 * @param minPlanesCompl Threshold number of total different planes including complementary planes from neighbour bins.
 * @param minKeepBoth Minimum number of planes in single bin leading to it being selected in any case.
 * @return Number of candidates.
 * @details Most hits from true tracks a distributed across 1,2 or 3 bins. By simply selecting bins over threshold,
 * a lot of clones are picked up if several of the contributing bins are above threshold. Therefore in the remove step,
 * the next bin in the list of candidates after threshold scan is peeked and if it's the direct neighbour only the one
 * with more total planes is kept.
 */
auto HoughTransformation::pickUpCandidateBins( int minPlanes_, int minPlanesComplX, int minPlanesComplUV,
                                               int minPlanesCompl ) {
  // threshold values imply "equal or less" which is not implemented in SIMDWrapper thus simply decrement
  const auto minPlanes = minPlanes_ - 1;
  // it's better to first quickly collect all bins that have at least a minimum amount of planes
  for ( auto iBin{minBinOffset}; iBin < nBins; iBin += simd::size ) {
    const auto aboveThreshold = decodeNbDifferentPlanes<simd::int_v>( iBin ) > minPlanes;
    compressstoreCandidates( aboveThreshold, simd::indices( iBin ) );
  }
  const auto originalSize = m_candSize;
  m_candSize              = 0;
  // then check if there's really a local excess of planes
  // this vectorised version is marginally faster than a simple remove if
  for ( auto i{0}; i < originalSize; i += simd::size ) {
    const auto iBin    = candidates<simd::int_v>( i );
    const auto pc      = gather_planes( iBin ) >> nDiffPlanesBits;
    const auto pcLeft  = gather_planes( iBin - 1 ) >> nDiffPlanesBits;
    const auto pcRight = gather_planes( iBin + 1 ) >> nDiffPlanesBits;

    const auto pcTot  = pc | pcLeft | pcRight;
    const auto nTot   = popcount_v( pcTot );
    const auto nTotX  = popcount_v( pcTot & xFlags );
    const auto nTotUV = popcount_v( pcTot & uvFlags );

    const auto removeMask = nTot < minPlanesCompl || nTotX < minPlanesComplX || nTotUV < minPlanesComplUV;
    compressstoreCandidates( !removeMask && simd::loop_mask( i, originalSize ), iBin );
  }
  /**
   * the problem now is that we picked some bins "twice", meaning that we have two adjacent bins
   * which are really only one candidate and were only selected both, because we are checking the neighbours for
   * planes. This remove_if mitigates this problem. It's a trade off between efficiencies and speed because
   * we might loose some hits (and even candidates) by removing one the bins. Always removing the one that has less
   * total planes and if both have an equal number of total planes removing the next (right) one has a good tradeoff
   * between speed and efficiencies. Always keeping both sacrifices ~5% throughput for O(0.1%) efficiency.
   */
  auto candSpan = LHCb::make_span( m_candidateBins.begin(), m_candSize );
  auto removeNext{false};
  m_candSize =
      std::distance( candSpan.begin(), std::remove_if( candSpan.begin(), candSpan.end(), [&]( const auto& iBin ) {
                       if ( removeNext ) {
                         removeNext = false;
                         return true;
                       }
                       const auto iBinNext = *( std::next( &iBin ) );
                       const auto adjacent = iBin == iBinNext - 1;
                       if ( !adjacent || iBin == candSpan.back() ) return false;

                       const auto pcLeft   = planes<scalar::int_v>( iBin - 1 ) >> nDiffPlanesBits;
                       const auto pc       = planes<scalar::int_v>( iBin ) >> nDiffPlanesBits;
                       const auto pcRight  = planes<scalar::int_v>( iBin + 1 ) >> nDiffPlanesBits;
                       const auto pcRight2 = planes<scalar::int_v>( iBin + 2 ) >> nDiffPlanesBits;

                       const auto nTot     = popcount_v( pc | pcRight | pcLeft ).cast();
                       const auto nTotNext = popcount_v( pc | pcRight | pcRight2 ).cast();

                       removeNext = nTot >= nTotNext;
                       return !removeNext;
                     } ) );
  return m_candSize;
}

/**
 * @brief Method calling the main components of the Forward Tracking, i.e. "forwarding the tracks".
 * @param input_tracks Tracks that shall be extended to SciFi stations. Either Velo tracks or Upstream tracks.
 * @param SciFiHits SOAContainer containing SciFi hits.
 * @param cache Cache containing useful information about SciFi zones.
 */
template <typename TrackType>
auto PrForwardTracking<TrackType>::forwardTracks( const TrackType& input_tracks, const PrSciFiHits& SciFiHits,
                                                  const ZoneCache& cache ) const {

  m_inputTracksCnt += input_tracks.size();

  std::vector<PrForwardTrack> candidates{};
  std::vector<PrForwardTrack> tracks{};
  candidates.reserve( input_tracks.size() );
  tracks.reserve( input_tracks.size() );

  HoughTransformation houghTransformation{};

  ModSciFiHits::ModPrSciFiHitsSOA allXHits{SciFiHits.size(), HoughTransformation::nBins};
  // the maximum size 32 is a somewhat arbitrary number, it's important that it's large enough
  ModSciFiHits::ModPrSciFiHitsAOS<32> stereoHits{};

  PrParametersX pars{m_minXHits,      m_maxXWindow,    m_maxXWindowSlope,    m_maxXGap,
                     m_minStereoHits, m_maxChi2PerDoF, m_maxChi2XProjection, m_maxChi2LinearFit};
  PrParametersX pars2ndLoop{m_minXHits2nd,   m_maxXWindow2nd, m_maxXWindowSlope2nd, m_maxXGap2nd,
                            m_minStereoHits, m_maxChi2PerDoF, m_maxChi2XProjection, m_maxChi2LinearFit};
  // TODO: this should be a derived condition
  const auto magScaleFactor = m_magFieldSvc->signedRelativeCurrent();
  // main loop over input tracks
  const auto inputTrks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( input_tracks );
  auto       acceptedInputTracks{0};
  for ( const auto& tr : inputTrks ) {
    const auto iTrack = tr.offset();
    debug( "" );
    debug( "============================VELO TRACK No.", iTrack, "=================================" );
    debug( "" );
    // get position and direction from velo track (state=1 for forward tracks state=0 for backward tracks)
    const auto [endv_pos, endv_dir, qOverP] = [&] {
      if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
        const Vec3<scalar::float_v> endv_pos = tr.StatePos();
        const Vec3<scalar::float_v> endv_dir = tr.StateDir();
        return std::tuple{endv_pos, endv_dir, tr.qOverP().cast()};
      } else {
        const Vec3<scalar::float_v> endv_pos = tr.StatePos( 1 );
        const Vec3<scalar::float_v> endv_dir = tr.StateDir( 1 );
        return std::tuple{endv_pos, endv_dir, 1.f};
      }
    }();
    const VeloSeed seed{endv_pos.x.cast(), endv_pos.y.cast(), endv_pos.z.cast(),
                        endv_dir.x.cast(), endv_dir.y.cast(), qOverP};

    if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
      // we have VeloUT to make a preselection according to momentum
      const auto p  = std::abs( 1.f / qOverP ) * float{Gaudi::Units::MeV};
      const auto pt = p * seed.momProj * float{Gaudi::Units::MeV};
      if ( p < m_preselectionMinP || pt < m_preselectionMinPT ) continue;
    }
    // remove tracks that are so steep in y that they go out of acceptance
    if ( const float yLastStation = seed.y( cache.zoneZPos.back() );
         !cache.lowerLastZone.isInsideY( yLastStation ) && !cache.upperLastZone.isInsideY( yLastStation ) ) {
      continue;
    }
    ++acceptedInputTracks;

    const VeloSeedExtended veloSeed{iTrack, zReference, seed, cache, zMagnetParams};

    houghTransformation.clear();

    debug( "Entering projectHitToHoughSpace" );
    const auto momentumBorders = calculateMomentumBorders( seed, magScaleFactor );
    debug( "momentumBorders =", momentumBorders );
    houghTransformation.projectStereoHitsToHoughSpace( veloSeed, cache, momentumBorders, SciFiHits );
    houghTransformation.projectXHitsToHoughSpace( veloSeed, momentumBorders, SciFiHits );

    debug( "Entering searchCandidates" );
    if ( !houghTransformation.pickUpCandidateBins( m_minPlanes, m_minPlanesComplX, m_minPlanesComplUV,
                                                   m_minPlanesCompl ) )
      continue;

    debug( "Entering sortCandidates" );
    allXHits.clear();
    houghTransformation.sortAndCopyBinContents( m_minTotalHits, allXHits );

    assert( std::is_sorted( allXHits.coords.begin(), allXHits.coords.begin() + allXHits.size() ) );

    debug( "Entering selectXCandidates" );
    candidates.clear();
    selectXCandidates<false>( candidates, veloSeed, allXHits, SciFiHits, pars, cache );

    debug( "Entering selectFullCandidates" );
    auto [good, ok] =
        selectFullCandidates<false>( candidates, stereoHits, pars, cache, veloSeed, SciFiHits, magScaleFactor );

    if ( !good && m_secondLoop ) {

      debug( "Now second loop" );

      const auto candStart2nd = candidates.size();
      selectXCandidates<true>( candidates, veloSeed, allXHits, SciFiHits, pars2ndLoop, cache );
      auto       candidates2ndLoop = LHCb::make_span( candidates.begin() + candStart2nd, candidates.end() );
      const auto ok2nd = selectFullCandidates<true>( candidates2ndLoop, stereoHits, pars2ndLoop, cache, veloSeed,
                                                     SciFiHits, magScaleFactor )
                             .second;
      // a short circuit booleon eval would be wrong here!
      ok = ok ? ok : ok2nd;
    }
    debug( "select best candidates for this velo track" );
    if ( ok || !m_secondLoop ) {
      auto bestQuality{m_maxQuality};
      for ( const auto& track : candidates ) {
        if ( const auto quality = track.quality(); quality < bestQuality ) { bestQuality = quality; }
      }
      const auto selectQuality = bestQuality + m_deltaQuality.value();
      std::copy_if( std::make_move_iterator( candidates.begin() ), std::make_move_iterator( candidates.end() ),
                    std::back_inserter( tracks ),
                    [&]( const auto& track ) { return track.quality() <= selectQuality && track.valid(); } );
    }
  } // ================end of input track loop====================
  m_acceptedInputTracksCnt += acceptedInputTracks;
  return tracks;
}

/**
 * @brief select candidate tracks using x hits
 * @param candidates container for x candidates
 * @param veloSeed useful info from the input track
 * @param allXHits contains all x hits that might form a candidate
 * @param SciFiHits PrSciFiHits container
 * @param xPars x parameter bundle
 * @details Loops over previously found ranges of the x hits trying to form one or more track candidates out of them.
 * Candidates must have a minimum number of planes, a central part of the x positions on the reference plane have to lie
 * within a window. To get clean candidates, at least two fits of the x projection are performed. In case a candidate
 * was found the used hits are removed from the range such that they are not picked up again while iterating further
 * through the range or in a second loop.
 */
template <typename TrackType>
template <bool secondLoop, typename Container, typename VeloSeedExtended>
void PrForwardTracking<TrackType>::selectXCandidates( Container& candidates, const VeloSeedExtended& veloSeed,
                                                      ModSciFiHits::ModPrSciFiHitsSOA& allXHits,
                                                      const PrSciFiHits& SciFiHits, const PrParametersX& pars,
                                                      const ZoneCache& cache ) const {

  TrackCandidate        protoCand{zReference, allXHits, SciFiHits};
  static_vector<int, 6> otherPlanes{};

  const auto startIndexSize = allXHits.candidateStartIndex.size();
  assert( allXHits.candidateStartIndex.size() == allXHits.candidateEndIndex.size() );
  // unsigned to avoid warning
  for ( unsigned iStart{0}; iStart < startIndexSize; ++iStart ) {
    auto idx1   = allXHits.candidateStartIndex[iStart];
    auto idxEnd = allXHits.candidateEndIndex[iStart];
    for ( int idx2 = idx1 + pars.minXHits; idx2 <= idxEnd; idx2 = idx1 + pars.minXHits ) {
      // like all good containers it's [idx1,idx2)

      const float xWindow = pars.maxXWindow + ( std::abs( allXHits.coord( idx1 ) ) +
                                                std::abs( allXHits.coord( idx1 ) - veloSeed.xStraightAtRef ) ) *
                                                  pars.maxXWindowSlope;
      // hits must fit into a window
      if ( ( allXHits.coord( idx2 - 1 ) - allXHits.coord( idx1 ) ) > xWindow ) {
        ++idx1;
        continue;
      }

      protoCand.clear();
      for ( auto idx{idx1}; idx < idx2; ++idx ) { protoCand.addHit( idx ); }
      // try to find more hits to the right
      idx2 = protoCand.improveRightSide( idx1, idx2, idxEnd, pars.maxXGap, xWindow );

      if ( protoCand.nDifferentPlanes() < pars.minXHits ) {
        ++idx1;
        continue;
      }

      const auto nSinglePlanes = protoCand.nSinglePlanes();
      const auto multiple      = nSinglePlanes != protoCand.nDifferentPlanes();
      if ( nSinglePlanes >= m_minSingleHits && multiple ) {
        separateSingleHitsForFit( otherPlanes, protoCand, veloSeed );
        protoCand.solveLineFit();
        // add best other hit on empty plane, after this there is only one hit per plane
        addBestOtherHits( otherPlanes, protoCand, veloSeed );
      } else if ( !multiple && !secondLoop ) {
        // in the first loop wrong hits at the edges of the found "cluster" can be removed
        // efficiently by trying to find a very small subrange
        protoCand.tryToShrinkRange( idx1, idx2, m_minInterval, m_minPlanesComplX );
      } else {
        // if every plane has at least two hits these are sorted out by the linear fit
        prepareAllHitsForFit( allXHits, protoCand );
      }
      // at this point there can also be less than minXHits (coming from shrinking of range)
      initXFitParameters( protoCand, veloSeed );
      fitLinearXProjection<secondLoop>( protoCand, pars, SciFiHits, veloSeed );

      if ( fillEmptyXPlanes<secondLoop>( protoCand, pars, veloSeed, SciFiHits, m_maxChi2XAddLinear ) ) {
        fitLinearXProjection<secondLoop>( protoCand, pars, SciFiHits, veloSeed );
      }

      auto ok = protoCand.nDifferentPlanes() >= pars.minXHits;
      if ( ok ) {
        initYFitParameters( protoCand, veloSeed, cache );
        ok = fitXProjection( protoCand, pars, SciFiHits );
        // TODO: train a simple LDA to reject some obvious ghosts here?
        if ( ok ) {
          if ( fillEmptyXPlanes<secondLoop>( protoCand, pars, veloSeed, SciFiHits, m_maxChi2XAddFull ) ) {
            ok = fitXProjection( protoCand, pars, SciFiHits );
          }
        }
        if ( ok ) {
          // It's a track!
          ok                                 = removeUsedHits( idx1, idxEnd, protoCand, allXHits );
          allXHits.candidateEndIndex[iStart] = idxEnd;
          candidates.emplace_back( protoCand.getCoordsToFit(), protoCand.getXParams(), protoCand.getYParams(),
                                   protoCand.getChi2NDoF(), veloSeed.iTrack );
        }
      }
      idx1 += !ok;
    }
  }
}

/**
 * @brief select and add stereo hits to make a full candidate out of found x candidates
 * @param candidates container for found x candidates
 * @param stereoHits container for stereo hits matching the x candidate under consideration
 * @param xPars x parameter bundle
 * @param cache cache of zone positions
 * @param veloSeed useful info from the input track
 * @param SciFiHits PrSciFiHits container
 * @return a pair of bools, indicating if there's a good and/or a ok-ish track
 * @details A loop over all previously found x candidates is performed. For each x candidate stereo hits are
 * collected by extrapolating the track to the stereo layers. From these hits stereo candidates are constructed
 * the best of which is selected and added to the x candidate to form a full candidate. The full candidate is fitted
 * again to determine the final track parameters, followed by a neural network assigning a ghost quality. The first
 * return value is true if there's a track with MORE than the minimum number of hits, the second is true if there's any
 * valid track.
 */
template <typename TrackType>
template <bool secondLoop, typename Container, typename Container2, typename VeloSeedExtended>
auto PrForwardTracking<TrackType>::selectFullCandidates( Container& candidates, Container2& stereoHits,
                                                         PrParametersX& xPars, const ZoneCache& cache,
                                                         const VeloSeedExtended& veloSeed, const PrSciFiHits& SciFiHits,
                                                         float magScaleFactor ) const {

  PrParametersY  yPars{m_tolY,        m_tolYSlope,           m_tolYUVSearch,  m_tolYTriangleSearch, m_minStereoHits,
                      m_minYGap,     m_maxChi2StereoLinear, m_maxChi2Stereo, m_maxChi2StereoAdd,   m_tolYMag,
                      m_tolYMagSlope};
  TrackCandidate stereoCandidate{zReference, stereoHits, SciFiHits};
  std::array<float, 7> mlpInput{};
  auto                 anyGood{false};
  auto                 anyOK{false};
  for ( auto& track : candidates ) {

    xPars.minStereoHits = m_minStereoHits;
    yPars.minStereoHits = m_minStereoHits;
    if ( track.size() + xPars.minStereoHits < m_minTotalHits ) {
      xPars.minStereoHits = m_minTotalHits - track.size();
      yPars.minStereoHits = m_minTotalHits - track.size();
    }

    stereoHits.clear();
    collectStereoHits( stereoHits, track, SciFiHits, veloSeed, cache, yPars );

    if ( stereoHits.size() < yPars.minStereoHits ) continue;

    const auto modHitsBegin = std::begin( stereoHits.modHits );
    std::sort( modHitsBegin, std::next( modHitsBegin, stereoHits.size() ), ModSciFiHits::byCoord() );

    if ( !selectStereoHits( track, stereoCandidate, SciFiHits, stereoHits, yPars, cache, veloSeed ) ) continue;

    // make a fit of all hits
    if ( !fitXProjection( track, xPars, SciFiHits ) ) continue;

    if ( fillEmptyXPlanes<secondLoop>( track, xPars, veloSeed, SciFiHits, m_maxChi2XAddFull ) ) {
      if ( !fitXProjection( track, xPars, SciFiHits ) ) continue;
    }

    if ( track.size() >= m_minTotalHits ) {

      const auto qOverP = calculateQoP( track, magScaleFactor, veloSeed );

      const auto xAtRef      = track.getXParams().get( 0 );
      const auto dSlope1     = ( veloSeed.xStraightAtRef - xAtRef ) / veloSeed.dzMagRef;
      const auto zMagPrecise = veloSeed.zMag + zMagnetParams[1] * dSlope1 * dSlope1;
      const auto xMag        = veloSeed.seed.x( zMagPrecise );
      const auto slopeT      = ( xAtRef - xMag ) / ( zReference - zMagPrecise );
      const auto dSlope      = slopeT - veloSeed.seed.tx;
      const auto dyCoef      = dSlope * dSlope * veloSeed.seed.ty;

      const auto bx = slopeT;
      const auto ay = veloSeed.yStraightAtRef;
      const auto by = veloSeed.seed.ty + dyCoef * yParams[0];

      // ay,by,bx params
      const auto& yPars   = track.getYParams();
      const auto  ay1     = yPars.get( 0 );
      const auto  by1     = yPars.get( 1 );
      const auto  xParams = track.getXParams();
      const auto  bx1     = xParams.get( 1 );

      mlpInput[0] = track.size();
      mlpInput[1] = qOverP;
      // FIXME: need a better NN
      if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
        mlpInput[2] = std::abs( veloSeed.seed.qOverP ) < 1e-9f ? 0.f : veloSeed.seed.qOverP - qOverP;
      } else {
        mlpInput[2] = 0.f;
      }
      mlpInput[3] = veloSeed.seed.slope2;
      mlpInput[4] = by - by1;
      mlpInput[5] = bx - bx1;
      mlpInput[6] = ay - ay1;

      auto getQuality = [&] {
        if constexpr ( secondLoop ) {
          return 1.f - m_MLPReader_2nd.GetMvaValue( mlpInput );
        } else {
          return 1.f - m_MLPReader_1st.GetMvaValue( mlpInput );
        }
      };
      if ( const auto quality = getQuality(); quality < m_maxQuality ) {
        track.setQuality( quality );
        track.setQoP( qOverP );
        track.setValid();
        anyOK   = true;
        anyGood = track.size() > m_minTotalHits;
      }
    }
  }
  return std::pair{anyGood, anyOK};
}

/**
 * @brief uses a parameterisation to define the initial hit search window for an input track
 * @param seed information about direction of the input track
 * @return a pair of x values corresponding to the search window borders at station 3 layer 0
 * @details The basic search window defines borders at the first layer of station 3 based on the input track's
 * direction in x and y and a minimum momentum requirement (pt and p). The basic search window can be larger than the
 * dimensions of the detector if the momentum requirements are low. If a momentum estimate of the input track is
 * available (VeloUT), the search window is narrowed by taking into account sign and magnitude of the momentum.
 */
template <typename TrackType>
auto PrForwardTracking<TrackType>::calculateMomentumBorders( const VeloSeed& seed, float magScaleFactor ) const {

  /**  this factor picks up the correct sign. "Correct" is defined by the polarity which was used when
   *  determining the extrapolation parameters. It's chosen here such that it's consistent with the
   *  SciFiTrackForwarding, i.e. +1 for MagDown, -1 for MagUp which is opposite the usual def.
   *  In case magScaleFactor is zero (switched off magnet?) this will be +1.
   */
  const auto polarityFactor = magScaleFactor > 0.f ? -1.f : 1.f;
  const auto factor         = m_useMomentumSearchWindow ? std::copysign( 1.f, seed.qOverP ) * polarityFactor : 1.f;

  // this is a term occurring in the polynomial from which the x prediction is deduced
  const auto term1 =
      toSciFiExtParams[0] + seed.tx * ( -toSciFiExtParams[1] * factor + toSciFiExtParams[2] * seed.tx ) +
      seed.ty2 * ( toSciFiExtParams[3] + seed.tx * ( toSciFiExtParams[4] * factor + toSciFiExtParams[5] * seed.tx ) );

  // 1/p, given a minPT and minP cut and the tracks slopes
  const auto minInvPGeVfromPt = factor * float{Gaudi::Units::GeV} / m_minPT * seed.momProj;
  const auto minInvPGeV       = factor * float{Gaudi::Units::GeV} / m_minP;

  // calculate size of window for given cut
  const auto minPBorder =
      minInvPGeV * ( term1 + minInvPGeV * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * minInvPGeV ) );
  const auto minPTBorder =
      minInvPGeVfromPt *
      ( term1 + minInvPGeVfromPt * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * minInvPGeVfromPt ) );

  // choose smallest window resulting from momentum requirements
  const auto minMomentumBorder = std::min( minPBorder, minPTBorder );

  // window shutters for case without momentum estimate, i.e. velo tracks as input
  auto dxMin = -minMomentumBorder;
  auto dxMax = minMomentumBorder;

  if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
    if ( m_useMomentumSearchWindow ) {
      const auto qOverPGeV = seed.qOverP * float{Gaudi::Units::GeV} * polarityFactor;
      // correction for prediction of tracks x position with ut momentum estimate
      const auto xExt =
          qOverPGeV * ( term1 + qOverPGeV * ( toSciFiExtParams[6] * seed.tx + toSciFiExtParams[7] * qOverPGeV ) );

      // error boundaries depend on momentum!
      const auto upperError = std::clamp( m_upperLimitOffset + m_upperLimitSlope * std::abs( qOverPGeV ),
                                          m_upperLimitMin.value(), m_upperLimitMax.value() );
      // calculate a lower error boundary on the tracks x-prediction,
      const auto lowerError =
          std::min( m_lowerLimitOffset + m_lowerLimitSlope * std::abs( qOverPGeV ), m_lowerLimitMax.value() );

      // depending on the sign of q, set the lower and upper error in the right direction
      // on top of that make sure that the upper error isn't larger than the minPTBorder
      // and the lower error isn't bigger than xExt which means we don't look further than the straight line prediction
      dxMin = factor > 0 ? ( ( xExt < lowerError ) ? 0.f : ( xExt - lowerError ) )
                         : std::max( xExt - upperError, minMomentumBorder );

      dxMax = factor > 0 ? std::min( xExt + upperError, minMomentumBorder )
                         : ( ( xExt > -lowerError ) ? 0.f : ( xExt + lowerError ) );

      // handle tracks which are above the wrong sign PT threshold, they have the upper error boundary in both
      // directions
      if ( const auto pt = std::abs( 1.f / seed.qOverP ) * seed.momProj; pt > m_wrongSignPT && m_useWrongSignWindow ) {
        dxMin = -std::abs( xExt ) - upperError;
        dxMax = std::abs( xExt ) + upperError;
      }
    }
  }
  return std::pair{dxMin, dxMax};
}

/**
 * @brief remove duplicates from the final tracks
 * @param tracks all forwarded tracks
 * @param SciFiHits PrScFiHits container
 * @return vector of sorted LHCbIDS vector per track
 * @details This function is an important part of ghost and clone reduction. A duplicate is found if two tracks
 * share a certain fraction of hits. If one of the two tracks has a significantly lower quality (from ghost NN),
 * it is removed.
 */
template <typename TrackType>
template <typename Container>
auto PrForwardTracking<TrackType>::removeDuplicates( Container& tracks, const PrSciFiHits& SciFiHits ) const {

  const auto zEndT = StateParameters::ZEndT - zReference;
  std::sort( tracks.begin(), tracks.end(),
             [&]( auto const& t1, auto const& t2 ) { return t1.x( zEndT ) < t2.x( zEndT ); } );

  const auto                                                    nTotal = tracks.size();
  std::vector<static_vector<LHCb::LHCbID, PrFTInfo::NFTLayers>> idSets( nTotal );
  for ( const auto& [idx, track] : LHCb::range::enumerate( tracks ) ) {
    const auto& coordsToFit = track.getCoordsToFit();
    auto&       idVec       = idSets[idx];
    std::transform( coordsToFit.begin(), coordsToFit.end(), std::back_inserter( idVec ),
                    [&SciFiHits]( auto iHit ) { return LHCb::LHCbID( SciFiHits.ID( iHit ) ); } );
    std::sort( idVec.begin(), idVec.end() );
  }
  // unsigned to fix warning
  for ( unsigned i1{0}; i1 < nTotal; ++i1 ) {
    auto& t1 = tracks[i1];
    if ( !t1.valid() ) continue;
    for ( unsigned i2 = i1 + 1; i2 < nTotal; ++i2 ) {
      auto& t2 = tracks[i2];
      if ( !t2.valid() ) continue;
      if ( std::abs( t1.x( zEndT ) - t2.x( zEndT ) ) > m_minXInterspace )
        break; // The distance only gets larger because we sorted above!
      if ( std::abs( t1.y( zEndT ) - t2.y( zEndT ) ) > m_minYInterspace ) continue;

      const auto nCommon = std::set_intersection( idSets[i1].begin(), idSets[i1].end(), idSets[i2].begin(),
                                                  idSets[i2].end(), counting_inserter{} )
                               .count;

      if ( nCommon > m_maxCommonFrac * ( idSets[i1].size() + idSets[i2].size() ) ) {
        const auto deltaQ = t2.quality() - t1.quality();
        // it's a bit confusing, but lower quality is better, this will change with new NN
        if ( deltaQ > m_deltaQuality ) {
          t2.setInvalid();
        } else if ( deltaQ < -m_deltaQuality ) {
          t1.setInvalid();
        }
      }
    }
  }
  return idSets;
}

/**
 * @brief converts internal track representation to general long tracks and adds UT hits
 * @param tracks contains found internal tracks
 * @param idSets contains the LHCbIDs for each track
 * @param input_tracks the input track that was forwarded
 * @details It's the only place where LHCbIds come into play because they are needed elsewhere.
 */
template <typename TrackType>
template <typename Container, typename Container2>
auto PrForwardTracking<TrackType>::makeLHCbLongTracks( Container&& tracks, Container2&& idSets,
                                                       const TrackType& input_tracks ) const {

  TracksFT result = std::make_from_tuple<TracksFT>( get_ancestors( input_tracks ) );
  result.reserve( tracks.size() );

  auto const inputtracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( input_tracks );
  for ( auto&& [cand, ids] : Gaudi::Functional::details::zip::range( tracks, idSets ) ) {
    if ( !cand.valid() ) continue;
    const auto iTrack      = cand.track();
    const auto ancestTrack = inputtracks[iTrack];

    auto n_vphits    = scalar::int_v{0};
    auto n_uthits    = scalar::int_v{0};
    auto currentsize = result.size();
    result.resize( currentsize + 1 );
    result.store<TracksTag::trackSeed>( currentsize, scalar::int_v{-1} );
    if constexpr ( std::is_same_v<TrackType, TracksUT> ) {

      n_vphits = ancestTrack.nVPHits();
      n_uthits = ancestTrack.nUTHits();
      result.store<TracksTag::trackVP>( currentsize, ancestTrack.trackVP() );
      result.store<TracksTag::trackUT>( currentsize, scalar::int_v{iTrack} );
      result.store<TracksTag::nVPHits>( currentsize, n_vphits );
      result.store<TracksTag::nUTHits>( currentsize, n_uthits );
      for ( auto idx{0}; idx < n_vphits.cast(); ++idx ) {
        result.store_vp_index( currentsize, idx, ancestTrack.vp_index( idx ) );
        result.store_lhcbID( currentsize, idx, ancestTrack.lhcbID( idx ) );
      }
      for ( auto idx{0}; idx < n_uthits.cast(); ++idx ) {
        result.store_ut_index( currentsize, idx, ancestTrack.ut_index( idx ) );
        const auto uthit = n_vphits.cast() + idx;
        result.store_lhcbID( currentsize, uthit, ancestTrack.lhcbID( uthit ) );
      }
    } else {
      n_vphits = ancestTrack.nHits();
      result.store<TracksTag::trackVP>( currentsize, scalar::int_v{iTrack} );
      result.store<TracksTag::trackUT>( currentsize, scalar::int_v{-1} );
      result.store<TracksTag::nUTHits>( currentsize, scalar::int_v{0} );
      result.store<TracksTag::nVPHits>( currentsize, ancestTrack.nHits() );

      for ( auto idx{0}; idx < n_vphits.cast(); ++idx ) {
        result.store_vp_index( currentsize, idx, ancestTrack.vp_index( idx ) );
        result.store_lhcbID( currentsize, idx, ancestTrack.lhcbID( idx ) );
      }
    }

    const auto qOverP = cand.getQoP();
    result.store<TracksTag::StateQoP>( currentsize, scalar::float_v{qOverP} );
    LHCb::State tState;
    const auto  dz = static_cast<float>( StateParameters::ZEndT ) - zReference;
    tState.setLocation( LHCb::State::Location::AtT );
    tState.setState( cand.x( dz ), cand.y( dz ), static_cast<float>( StateParameters::ZEndT ), cand.xSlope( dz ),
                     cand.ySlope( dz ), qOverP );
    const auto pos = Vec3<scalar::float_v>( tState.x(), tState.y(), tState.z() );
    const auto dir = Vec3<scalar::float_v>( tState.tx(), tState.ty(), 1.f );
    result.store_StatePosDir( currentsize, 1, pos, dir );

    const auto [ancestpos, ancestdir] = [&] {
      if constexpr ( std::is_same_v<TrackType, TracksUT> ) {
        return std::pair{ancestTrack.StatePos(), ancestTrack.StateDir()};
      } else {
        return std::pair{ancestTrack.StatePos( 1 ), ancestTrack.StateDir( 1 )};
      }
    }();
    result.store_StatePosDir( currentsize, 0, ancestpos, ancestdir );

    assert( ids.size() <= LHCb::Pr::Long::Tracks::MaxFTHits );

    const auto n_fthits = ids.size();
    result.store<TracksTag::nFTHits>( currentsize, scalar::int_v( n_fthits ) );
    const auto& hits = cand.getCoordsToFit();
    // unsigned to avoid warning
    for ( unsigned idx{0}; idx < n_fthits; ++idx ) {
      const scalar::int_v ftidx = hits[idx];
      result.store_ft_index( currentsize, idx, ftidx );
      const auto          hitidx = n_vphits.cast() + n_uthits.cast() + idx;
      const scalar::int_v lhcbid = ids[idx].lhcbID();
      result.store_lhcbID( currentsize, hitidx, lhcbid );
    }
  } // next candidate
  /// avoid FPEs
  // TODO: move this to PrAddUTHitsTool
  if ( ( result.size() + simd::size ) > result.capacity() ) result.reserve( result.size() + simd::size );
  result.store<TracksTag::StateQoP>( result.size(), simd::float_v( 1.f ) );
  result.store_StatePosDir( result.size(), 0, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );
  result.store_StatePosDir( result.size(), 1, Vec3<simd::float_v>( 1.f, 1.f, 1.f ),
                            Vec3<simd::float_v>( 1.f, 1.f, 1.f ) );

  //=== add UT hits
  if constexpr ( std::is_same_v<TrackType, TracksVP> ) {
    if ( m_addUTHitsTool.isEnabled() ) {
      auto sc = m_addUTHitsTool->addUTHits( result );
      if ( sc.isFailure() ) error() << "adding UT clusters failed!" << endmsg;
    }
  }
  m_outputTracksCnt += result.size();
  return result;
}
