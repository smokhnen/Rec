/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
#include "PrKernel/PrSciFiHits.h"
#include "boost/container/static_vector.hpp"
#include <array>
#include <limits>

namespace Forward {

  using boost::container::static_vector;

  static constexpr auto nan = std::numeric_limits<float>::signaling_NaN();

  /**
   * @struct TrackPars PrForwardTrack.h
   * @brief aggregate for storage and handling of track parameters
   */
  template <auto P>
  struct TrackPars {
    std::array<float, P> pars{};
    auto                 get( int i ) const { return pars[i]; };
    template <auto N>
    void add( LHCb::span<const float, N> v ) {
      static_assert( N <= P );
      for ( size_t i{0}; i < N; ++i ) { pars[i] += v[i]; }
    }
  };

  /**
   * @struct VeloSeed PrForwardTrack.h
   * @brief struct for storing state of the input track plus some often used quantities
   */
  struct VeloSeed {
    VeloSeed( float x0_, float y0_, float z0_, float tx_, float ty_, float qOverP_ )
        : x0{x0_}
        , y0{y0_}
        , z0{z0_}
        , tx{tx_}
        , ty{ty_}
        , qOverP{qOverP_}
        , tx2{tx_ * tx_}
        , ty2{ty_ * ty_}
        , slope2{tx_ * tx_ + ty_ * ty_}
        , momProj{std::sqrt( ( tx_ * tx_ + ty_ * ty_ ) / ( 1.f + tx_ * tx_ + ty_ * ty_ ) )} {}

    float x0{0.f};
    float y0{0.f};
    float z0{0.f};
    float tx{0.f};
    float ty{0.f};
    float qOverP{1.f};
    float tx2{0.f};
    float ty2{0.f};
    float slope2{0.f};
    float momProj{1.f};

    auto x( float z ) const { return x0 + ( z - z0 ) * tx; }
    auto y( float z ) const { return y0 + ( z - z0 ) * ty; }
  };

  /**
   * @struct VeloSeedExtended PrForwardTrack.h
   * @brief struct for storing often used quantities that are derived from the input track
   * @details Contains a reference to the VeloSeed such that this struct is usually the one
   * handed to functions.
   */
  template <typename Cache, typename Container>
  struct VeloSeedExtended {
    VeloSeedExtended( int iTrack, float zReference, const VeloSeed& _seed, const Cache& cache,
                      const Container& zMagnetParams )
        : xStraightAtRef{_seed.x( zReference )}
        , zMag{zMagnetParams[0] + zMagnetParams[2] * _seed.tx2 + zMagnetParams[3] * _seed.ty2}
        , yStraightAtRef{_seed.y( zReference )}
        , iTrack{iTrack}
        , seed{_seed} {

      upperHalfTrack = yStraightAtRef > 0;
      for ( auto iLayer{0}; iLayer < PrFTInfo::NFTLayers; ++iLayer ) {
        const auto iZone           = 2 * iLayer + upperHalfTrack;
        const auto yStraightInZone = _seed.y( cache.zoneZPos[iZone] );
        betterZ[iLayer]            = cache.zoneZPos[iZone] + yStraightInZone * cache.zoneDzdy[iZone];
        xStraightInZone[iLayer]    = _seed.x( betterZ[iLayer] );
      }
      yStraightAtMag = _seed.y( zMag );
      dzMagRef       = zMag - zReference;
    }
    alignas( 64 ) std::array<float, PrFTInfo::NFTLayers> betterZ{nan};
    alignas( 64 ) std::array<float, PrFTInfo::NFTLayers> xStraightInZone{nan};
    float           xStraightAtRef{0.f};
    float           zMag{0.f};
    float           yStraightAtRef{0.f};
    int             iTrack{0};
    bool            upperHalfTrack{std::numeric_limits<int>::signaling_NaN()};
    float           yStraightAtMag{0.f};
    float           dzMagRef{0.f};
    const VeloSeed& seed;
  };

  /**
   * @class TrackCandidate PrForwardTrack.h
   * @brief It's almost a track but contains a lot of temporary quantities used for building a candidate.
   * @details Keeps track of hits and their planes, allows for more than one hit per plane. Provides a simple
   * straight line fit of positions on the reference plane.
   */
  template <typename Container>
  class TrackCandidate {
  public:
    // typically we look at 3 bins from the HoughTransformation each can have 16 x hits at most
    // so let's take up to 6 hits per xlayer
    static constexpr int planeMulti{6};
    static constexpr int nXPars{4};
    static constexpr int nYPars{3};

    TrackCandidate( float zRef, const Container& allXHits, const SciFiHits::PrSciFiHits& SciFiHits )
        : m_zReference{zRef}, m_allXHits{allXHits}, m_SciFiHits{SciFiHits} {}

    void tryToShrinkRange( int, int, float, int );

    auto  nSinglePlanes() const { return std::count( m_planeSize.begin(), m_planeSize.end(), 1 ); }
    auto  nDifferentPlanes() const { return m_nDifferentPlanes; }
    auto& nDifferentPlanes() { return m_nDifferentPlanes; }
    auto& getCoordsToFit() { return m_coordsToFit; }
    auto  getCoordsToFit() const { return m_coordsToFit; }
    auto& getBetterDz() { return m_betterDz; }
    auto  getBetterDz( int fulldex ) { return m_betterDz[m_SciFiHits.planeCode( fulldex ) / 2u]; }
    auto& getXParams() { return m_xParams; }
    auto  getXParams() const { return m_xParams; }
    auto& getYParams() { return m_yParams; }
    auto  getYParams() const { return m_yParams; }
    auto  nInSamePlane( int fulldex ) const { return m_planeSize[m_SciFiHits.planeCode( fulldex ) / 2u]; }
    auto  planeSize( int ip ) const { return m_planeSize[ip]; }
    auto& planeSize( int ip ) { return m_planeSize[ip]; }
    void  setPlaneSize( int ip, int size ) { m_planeSize[ip] = size; }
    void  setChi2NDoF( std::pair<float, float>&& chi2NDoF ) { m_chi2NDoF = chi2NDoF; }
    auto  getChi2NDoF() { return m_chi2NDoF; }
    auto  chi2PerDoF() const {
      assert( m_chi2NDoF.second > 0.f );
      return m_chi2NDoF.first / m_chi2NDoF.second;
    }
    void removeFromPlane( int fulldex ) {
      m_nDifferentPlanes -= !( --m_planeSize[m_SciFiHits.planeCode( fulldex ) / 2u] );
    }
    void  removePlane( int ip ) { m_nDifferentPlanes -= !( --m_planeSize[ip] ); }
    auto  xAtRef() const { return m_xAtRef; }
    auto& xAtRef() { return m_xAtRef; }
    auto  txNew() const { return m_txNew; }
    auto  getIdx( int index ) const { return m_idxOnPlane[index]; }
    auto  getIdxSpan( int ip ) const {
      const auto span = LHCb::span{m_idxOnPlane};
      return span.subspan( ip * planeMulti, m_planeSize[ip] );
    }
    template <auto N>
    void addXParams( LHCb::span<const float, N> pars ) {
      m_xParams.add( pars );
    }
    template <auto N>
    void addYParams( LHCb::span<const float, N> pars ) {
      m_yParams.add( pars );
    }

    void clear() {
      m_planeSize.fill( 0 );
      m_nDifferentPlanes = 0;
      m_s.fill( 0.f );
      m_coordsToFit.clear();
      m_xAtRef = 0.f;
    }

    void addHit( int idx ) {
      const int ip = m_SciFiHits.planeCode( m_allXHits.fulldex( idx ) ) / 2;
      if ( const auto pcSize = m_planeSize[ip]; pcSize < planeMulti ) {
        m_idxOnPlane[ip * planeMulti + pcSize] = idx;
        m_nDifferentPlanes += !pcSize;
        ++m_planeSize[ip];
      }
    }

    template <typename VeloSeedExtended>
    void addHitForLineFit( int idx, const VeloSeedExtended& veloSeed ) {
      const auto fulldex = m_allXHits.fulldex( idx );
      const auto c       = m_allXHits.coord( idx );
      const auto w       = m_SciFiHits.w( fulldex );
      const auto betterZ = veloSeed.betterZ[m_SciFiHits.planeCode( fulldex )];
      const auto dz      = betterZ - m_zReference;
      m_s[0] += w;
      m_s[1] += w * dz;
      m_s[2] += w * dz * dz;
      m_s[3] += w * c;
      m_s[4] += w * c * dz;
      m_coordsToFit.push_back( fulldex );
    }

    void solveLineFit() {
      // sz * sz - s0 * sz2
      const auto den = m_s[1] * m_s[1] - m_s[0] * m_s[2];
      // scz * sz - sc * sz2
      m_xAtRef = ( m_s[4] * m_s[1] - m_s[3] * m_s[2] ) / den;
      // sc * sz - s0 * scz
      m_txNew = ( m_s[3] * m_s[1] - m_s[0] * m_s[4] ) / den;
    }

    template <typename VeloSeedExtended>
    auto lineChi2( int idx, const VeloSeedExtended& veloSeed ) const {
      const auto fulldex = m_allXHits.fulldex( idx );
      const auto c       = m_allXHits.coord( idx );
      const auto z       = veloSeed.betterZ[m_SciFiHits.planeCode( fulldex )];
      const auto d       = ( c - m_xAtRef ) - ( z - m_zReference ) * m_txNew;
      return d * d * m_SciFiHits.w( fulldex );
    }

    auto yStraight( float z ) const { return m_yParams.get( 0 ) + ( z - m_zReference ) * m_yParams.get( 1 ); }

    auto x( float dz ) const {
      assert( dz < 4000.f && "need distance to reference plane here!" );
      return m_xParams.get( 0 ) + dz * ( m_xParams.get( 1 ) + dz * ( m_xParams.get( 2 ) + dz * m_xParams.get( 3 ) ) );
    }

    auto y( float dz ) const {
      assert( dz < 4000.f && "need distance to reference plane here!" );
      return m_yParams.get( 0 ) + dz * ( m_yParams.get( 1 ) + dz * m_yParams.get( 2 ) );
    }

    auto calcBetterDz( int fulldex ) const {
      const auto dz = m_SciFiHits.z( fulldex ) - m_zReference;
      return dz + m_SciFiHits.dzDy( fulldex ) * y( dz );
    }

    auto distanceXHit( int fulldex, float dz ) const { return m_SciFiHits.x( fulldex ) - x( dz ); }

    auto distanceStereoHit( int fulldex, float dz ) const {
      return ( x( dz ) - ( m_SciFiHits.x( fulldex ) + y( dz ) * m_SciFiHits.dxDy( fulldex ) ) ) /
             m_SciFiHits.dxDy( fulldex );
    }

    auto chi2XHit( int fulldex, float dz ) const {
      const auto d = distanceXHit( fulldex, dz );
      return d * d * m_SciFiHits.w( fulldex );
    }

    auto chi2StereoHits( int fulldex ) const {
      const auto dz = calcBetterDz( fulldex );
      const auto d  = ( m_SciFiHits.x( fulldex ) + y( dz ) * m_SciFiHits.dxDy( fulldex ) ) - x( dz );
      return d * d * m_SciFiHits.w( fulldex );
    }

    auto planeEmpty( int idx ) const {
      const int ip = m_SciFiHits.planeCode( m_allXHits.fulldex( idx ) ) / 2;
      return !m_planeSize[ip];
    }

    auto improveRightSide( int idx1, int idx2, int idxEnd, float maxXGap, float xWindow ) {
      for ( auto idxLast = idx2 - 1; idx2 < idxEnd; ++idx2 ) {
        const auto smallGap = m_allXHits.coord( idx2 ) < m_allXHits.coord( idxLast ) + maxXGap;
        const auto inWindow = m_allXHits.coord( idx2 ) - m_allXHits.coord( idx1 ) < xWindow;
        if ( !( smallGap || ( inWindow && planeEmpty( idx2 ) ) ) ) return idx2;
        addHit( idx2 );
        idxLast = idx2;
      }
      return idx2;
    }

  private:
    alignas( 64 ) std::array<int, PrFTInfo::NFTXLayers * planeMulti> m_idxOnPlane{};
    alignas( 64 ) std::array<int, PrFTInfo::NFTXLayers> m_planeSize{};
    alignas( 64 ) std::array<float, 5> m_s{nan, nan, nan, nan, nan};
    alignas( 64 ) std::array<float, PrFTInfo::NFTXLayers> m_betterDz{};
    static_vector<int, PrFTInfo::NFTXLayers * planeMulti> m_coordsToFit{};

    // unsigned to avoid warnings
    unsigned                      m_nDifferentPlanes{0};
    float                         m_txNew{nan};
    float                         m_xAtRef{nan};
    TrackPars<4>                  m_xParams{nan, nan, nan, nan};
    TrackPars<3>                  m_yParams{nan, nan, nan};
    std::pair<float, float>       m_chi2NDoF{nan, nan};
    float                         m_zReference{8520.f};
    const Container&              m_allXHits;
    const SciFiHits::PrSciFiHits& m_SciFiHits;
  };

  template <typename Container>
  void TrackCandidate<Container>::tryToShrinkRange( int idx1, int idx2, float interval, int nPlanes ) {
    auto idxWindowStart = idx1;
    auto idxWindowEnd   = idx1 + nPlanes; // pointing at last+1

    auto minInterval = interval;
    auto best        = idx1;
    auto bestEnd     = idx2;

    auto       otherNDifferentPlanes{0};
    std::array otherPlaneSize{0, 0, 0, 0, 0, 0};

    for ( auto idx{idxWindowStart}; idx < idxWindowEnd; ++idx ) {
      const int ip = m_SciFiHits.planeCode( m_allXHits.fulldex( idx ) ) / 2;
      otherNDifferentPlanes += !( otherPlaneSize[ip]++ );
    }

    while ( idxWindowEnd <= idx2 ) {
      if ( otherNDifferentPlanes >= nPlanes ) {
        // have nPlanes, check x distance
        if ( const float dist = m_allXHits.coord( idxWindowEnd - 1 ) - m_allXHits.coord( idxWindowStart );
             dist < minInterval ) {
          minInterval = dist;
          best        = idxWindowStart;
          bestEnd     = idxWindowEnd;
          // need to keep track of planes
          m_nDifferentPlanes = otherNDifferentPlanes;
          m_planeSize        = otherPlaneSize;
        }
      } else {
        // too few planes, add one hit
        ++idxWindowEnd;
        if ( idxWindowEnd > idx2 ) break;
        const int ip = m_SciFiHits.planeCode( m_allXHits.fulldex( idxWindowEnd - 1 ) ) / 2;
        otherNDifferentPlanes += !( otherPlaneSize[ip]++ );
        continue;
      }
      // move on to the right
      const int ip = m_SciFiHits.planeCode( m_allXHits.fulldex( idxWindowStart ) ) / 2;
      otherNDifferentPlanes -= !( --otherPlaneSize[ip] );
      ++idxWindowStart;
    }
    for ( auto idx{best}; idx < bestEnd; ++idx ) {
      const auto fulldex = m_allXHits.fulldex( idx );
      m_coordsToFit.push_back( fulldex );
      m_xAtRef += m_allXHits.coord( idx );
    }
    m_xAtRef /= static_cast<float>( m_coordsToFit.size() );
  }

  /**
   * @class PrForwardTrack PrForwardTrack.h
   * @brief Internal representation of a long track.
   * @details Only one hit per plane is allowed, has all information needed to make a final long track
   * out of it, i.e. knows its Velo(UT) track and q/p, and the quantities needed to obtain it.
   */
  class PrForwardTrack {
  public:
    static constexpr int nXPars{4};
    static constexpr int nYPars{3};
    using TC = TrackCandidate<Forward::ModSciFiHits::ModPrSciFiHitsSOA>;

    PrForwardTrack( static_vector<int, PrFTInfo::NFTXLayers * TC::planeMulti> coordsToFit, TrackPars<nXPars> xParams,
                    TrackPars<nYPars> yParams, std::pair<float, float> chi2NDoF, int iInputTrack )
        : m_xParams{xParams}, m_yParams{yParams}, m_chi2NDoF{chi2NDoF}, m_track{iInputTrack} {
      std::copy( coordsToFit.begin(), coordsToFit.end(), std::back_inserter( m_coordsToFit ) );
    }

    int   track() const { return m_track; }
    auto  size() const { return m_coordsToFit.size(); }
    auto  getXParams() const { return m_xParams; }
    auto& getXParams() { return m_xParams; }
    auto  getYParams() const { return m_yParams; }
    auto& getYParams() { return m_yParams; }

    auto&       getCoordsToFit() { return m_coordsToFit; }
    const auto& getCoordsToFit() const { return m_coordsToFit; }
    auto        valid() const { return m_valid; }
    void        setValid() { m_valid = true; }
    void        setInvalid() { m_valid = false; }

    void setQuality( float q ) { m_quality = q; }
    auto quality() const { return m_quality; }

    void setQoP( float qop ) { m_qop = qop; }
    auto getQoP() const { return m_qop; }

    template <typename Container>
    void addHits( Container&& hits ) {
      std::copy( hits.begin(), hits.end(), std::back_inserter( m_coordsToFit ) );
    }

    auto x( float dz ) const {
      assert( dz < 4000.f && "need distance to reference plane here!" );
      return m_xParams.get( 0 ) + dz * ( m_xParams.get( 1 ) + dz * ( m_xParams.get( 2 ) + dz * m_xParams.get( 3 ) ) );
    }

    auto y( float dz ) const {
      assert( dz < 4000.f && "need distance to reference plane here!" );
      return m_yParams.get( 0 ) + dz * ( m_yParams.get( 1 ) + dz * m_yParams.get( 2 ) );
    }

    auto distance( int fulldex, float dz, const SciFiHits::PrSciFiHits& SciFiHits ) const {
      return ( SciFiHits.x( fulldex ) + y( dz ) * SciFiHits.dxDy( fulldex ) ) - x( dz );
    }

    auto chi2( int fulldex, float dz, const SciFiHits::PrSciFiHits& SciFiHits ) const {
      const auto d = distance( fulldex, dz, SciFiHits );
      return d * d * SciFiHits.w( fulldex );
    }

    auto getBetterDz( int fulldex, float dz, const SciFiHits::PrSciFiHits& SciFiHits ) {
      dz += y( dz ) * SciFiHits.dzDy( fulldex );
      return dz;
    }

    auto xSlope( float dz ) const {
      return m_xParams.get( 1 ) + dz * ( 2.f * m_xParams.get( 2 ) + 3.f * dz * m_xParams.get( 3 ) );
    }

    auto ySlope( float dz ) const { return m_yParams.get( 1 ) + dz * 2.f * m_yParams.get( 2 ); }

    template <auto N>
    void addXParams( LHCb::span<const float, N> pars ) {
      m_xParams.add( pars );
    }

    void setChi2NDoF( std::pair<float, float>&& chi2NDoF ) { m_chi2NDoF = chi2NDoF; }
    auto getChi2PerNDoF() { return m_chi2NDoF.first / m_chi2NDoF.second; }

  private:
    static_vector<int, PrFTInfo::NFTLayers> m_coordsToFit{};
    TrackPars<nXPars>                       m_xParams{nan, nan, nan, nan};
    TrackPars<nYPars>                       m_yParams{nan, nan, nan};
    std::pair<float, float>                 m_chi2NDoF{nan, nan};
    float                                   m_quality{1.f};
    float                                   m_qop{nan};
    bool                                    m_valid{false};
    int                                     m_track{-1};
  };

} // namespace Forward
