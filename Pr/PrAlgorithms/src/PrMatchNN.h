/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRMATCHNN_H
#define PRMATCHNN_H 1

// Include files
// from Gaudi
#include "Event/PrLongTracks.h"
#include "Event/PrSeedTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/Track_v2.h"

#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRegistry.h"

#include "IPrAddUTHitsTool.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/bit_cast.h"
#include "PrKernel/IPrDebugMatchTool.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"

#include "weights/TMVAClassification_MLPMatching.class.C"

#include <memory>

/** @class PrMatchNN PrMatchNN.h
 *  Match Velo and Seed tracks
 *
 *  @author Michel De Cian (migration to Upgrade)
 *  @date 2013-11-15
 *
 *  @author Olivier Callot
 *  @date   2007-02-07
 */

namespace {
  using simd  = SIMDWrapper::avx256::types;
  using dType = SIMDWrapper::scalar::types;
  using I     = dType::int_v;
  using F     = dType::float_v;

  using SeedTracks = LHCb::Pr::Seeding::Tracks;
  using VeloTracks = LHCb::Pr::Velo::Tracks;

  LHCb::State
  getVeloState( const LHCb::Pr::detail::proxy_type<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous,
                                                   LHCb::Pr::Velo::Tracks const>& track,
                int                                                               index ) {

    LHCb::State           state;
    LHCb::StateVector     s;
    Gaudi::TrackSymMatrix c;

    // Add state closest to beam
    auto const pos  = track.StatePos( index );
    auto const dir  = track.StateDir( index );
    auto const covX = track.StateCovX( index );
    auto const covY = track.StateCovY( index );

    s.setX( pos.x.cast() );
    s.setY( pos.y.cast() );
    s.setZ( pos.z.cast() );
    s.setTx( dir.x.cast() );
    s.setTy( dir.y.cast() );
    s.setQOverP( 0. );

    c( 0, 0 ) = covX.x.cast();
    c( 2, 0 ) = covX.y.cast();
    c( 2, 2 ) = covX.z.cast();
    c( 1, 1 ) = covY.x.cast();
    c( 3, 1 ) = covY.y.cast();
    c( 3, 3 ) = covY.z.cast();
    c( 4, 4 ) = 1.f;

    state.setState( s );

    state.setCovariance( c );

    return state;
  }
} // namespace

class PrMatchNN : public Gaudi::Functional::Transformer<LHCb::Pr::Long::Tracks( const LHCb::Pr::Velo::Tracks&,
                                                                                const LHCb::Pr::Seeding::Tracks& )> {

  using Track = LHCb::Event::v2::Track;

public:
  /// Standard constructor
  PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  //  main method
  LHCb::Pr::Long::Tracks operator()( const LHCb::Pr::Velo::Tracks&, const LHCb::Pr::Seeding::Tracks& ) const override;

  /** @class MatchCandidate PrMatchNN.h
   *
   * Match candidate for PrMatcNNh algorithm
   *
   * @author Sevda Esen, Michel De Cian
   * @date   2015-02-07
   * 	initial implementation
   * @date   2020-06-23
   *  implemented SOA version
   */
  class MatchCandidate {
  public:
    MatchCandidate( int v, int s ) : m_vTr( v ), m_sTr( s ) {}

    int vTr() const { return m_vTr; }
    int sTr() const { return m_sTr; }

  private:
    int         m_vTr = 0;
    std::size_t m_sTr = 0;
  };

private:
  typedef std::vector<MatchCandidate> MatchCandidates;

  // calculate matching chi^2
  std::optional<float> getChi2Match( const Vec3<F> vState_pos, const Vec3<F> vState_dir, const Vec3<F> sState_pos,
                                     const Vec3<F> sState_dir, std::array<float, 6>& mLPReaderInput ) const;

  // merge velo and seed segment to output track
  LHCb::Pr::Long::Tracks makeTracks( const LHCb::Pr::Velo::Tracks& velos, const LHCb::Pr::Seeding::Tracks& seeds,
                                     MatchCandidates matches ) const;

  Gaudi::Property<std::vector<double>> m_zMagParams{
      this, "ZMagnetParams", {5287.6, -7.98878, 317.683, 0.0119379, -1418.42}};
  Gaudi::Property<std::vector<double>> m_momParams{
      this, "MomentumParams", {1.24386, 0.613179, -0.176015, 0.399949, 1.64664, -9.67158}};
  Gaudi::Property<std::vector<double>> m_bendYParams{this, "bendParamYParams", {-347.801, -42663.6}};
  // -- Parameters for matching in y-coordinate
  Gaudi::Property<float> m_zMatchY{this, "zMatchY", 10000. * Gaudi::Units::mm};
  // -- Tolerances
  Gaudi::Property<float> m_dxTol{this, "dxTol", 8. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dxTolSlope{this, "dxTolSlope", 80. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTol{this, "dyTol", 6. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTolSlope{this, "dyTolSlope", 300. * Gaudi::Units::mm};
  Gaudi::Property<float> m_fastYTol{this, "FastYTol", 250.};
  // -- The main cut values
  Gaudi::Property<float> m_maxChi2{this, "MaxMatchChi2", 15.0};
  Gaudi::Property<float> m_minNN{this, "MinMatchNN", 0.25};
  Gaudi::Property<float> m_maxdDist{this, "MaxdDist", 0.10};

  std::unique_ptr<IClassifierReader> m_MLPReader;

  using ErrorCounter = Gaudi::Accumulators::MsgCounter<MSG::ERROR>;
  mutable ErrorCounter m_maxTracksErr{this, "Number of tracks reached maximum!"};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCount{this, "#MatchingTracks"};
  mutable Gaudi::Accumulators::SummingCounter<float>        m_tracksMLP{this, "TracksMLP"};
  mutable Gaudi::Accumulators::SummingCounter<float>        m_tracksChi2{this, "#MatchingChi2"};

  ToolHandle<IPrAddUTHitsTool>       m_addUTHitsTool{this, "AddUTHitsToolName", "PrAddUTHitsTool"};
  ToolHandle<IPrDebugMatchTool>      m_matchDebugTool{this, "MatchDebugToolName", ""};
  ToolHandle<ITrackMomentumEstimate> m_fastMomentumTool{this, "FastMomentumToolName", "FastMomentumEstimate"};

  typedef std::vector<std::pair<int, float>> seedMLPPairs;
};

#endif // PRMATCH_H
