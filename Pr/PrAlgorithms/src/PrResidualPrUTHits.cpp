/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/PrLongTracks.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRegistry.h"
#include "PrKernel/PrHit.h"
#include "PrKernel/PrUTHitHandler.h"
#include "UTDAQ/UTInfo.h"
#include "UTDet/DeUTDetector.h"
#include <Vc/Vc>
#include <vector>

#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"
#include "boost/dynamic_bitset.hpp"
#include <memory>

//-----------------------------------------------------------------------------
// class : PrResidualPrUTHits
// Store residual PrUTHits after other Algorithms, e.g. PrMatchNN/PrForward used
//
// 2020-04-21 : Peilian Li
//
//-----------------------------------------------------------------------------

template <typename T>
class PrResidualPrUTHits
    : public Gaudi::Functional::Transformer<LHCb::Pr::UT::HitHandler( const T&, const LHCb::Pr::UT::HitHandler& )> {

public:
  using base_class_t =
      Gaudi::Functional::Transformer<LHCb::Pr::UT::HitHandler( const T&, const LHCb::Pr::UT::HitHandler& )>;

  LHCb::Pr::UT::HitHandler operator()( const T&, const LHCb::Pr::UT::HitHandler& ) const override;

  PrResidualPrUTHits( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t( name, pSvcLocator,
                      std::array{typename base_class_t::KeyValue{"TracksLocation", ""},
                                 typename base_class_t::KeyValue{"PrUTHitsLocation", ""}},
                      typename base_class_t::KeyValue{"PrUTHitsOutput", ""} ) {}
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( PrResidualPrUTHits<LHCb::Pr::Long::Tracks>, "PrResidualPrUTHits" )
DECLARE_COMPONENT_WITH_ID( PrResidualPrUTHits<LHCb::Pr::Upstream::Tracks>, "PrResidualPrUTHits_Upstream" )

//=============================================================================
// Main execution
//=============================================================================
template <typename T>
LHCb::Pr::UT::HitHandler PrResidualPrUTHits<T>::operator()( const T&                        tracks,
                                                            const LHCb::Pr::UT::HitHandler& uthithandler ) const {
  LHCb::Pr::UT::HitHandler tmp{};

  // mark used UT hits
  const unsigned int      nhits = uthithandler.nHits();
  boost::dynamic_bitset<> used{nhits, false};

  auto const longiter = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
  /// mark used SciFi Hits
  for ( const auto& track : longiter ) {
    const int nuthits = track.nUTHits().cast();
    for ( int idx = 0; idx < nuthits; idx++ ) {
      const int index = track.ut_index( idx ).cast();
      if ( index >= 0 ) used[index] = true;
    }
  }

  const auto& allhits = uthithandler.hits();
  const int   fullChanIdx =
      static_cast<int>( UTInfo::DetectorNumbers::Layers ) * static_cast<int>( UTInfo::DetectorNumbers::Stations ) *
      static_cast<int>( UTInfo::DetectorNumbers::Regions ) * static_cast<int>( UTInfo::DetectorNumbers::Sectors );

  for ( auto fullchan = 0; fullchan < fullChanIdx; fullchan++ ) {
    const auto indexs = uthithandler.indices( fullchan );

    for ( int idx = indexs.first; idx != indexs.second; idx++ ) {
      if ( used[idx] ) continue;
      tmp.copyHit( fullchan, idx, allhits );
    }
  }
  return tmp;
}
