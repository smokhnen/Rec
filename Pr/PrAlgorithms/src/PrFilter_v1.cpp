/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/Transformer.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi/ITrackFunctorFactory.h"
#include "LoKi/TrackTypes.h"
#include "LoKi_v2/ITrackFunctorFactory.h"
#include "LoKi_v2/TrackTypes.h"
#include "PrKernel/PrSelection.h"
#include "boost/algorithm/string/join.hpp"
#include <vector>

namespace {
  // Just shorthand for below
  template <typename T>
  using FilterTransform =
      Gaudi::Functional::MultiTransformerFilter<std::tuple<Pr::Selection<T>>( Pr::Selection<T> const& )>;

  // Helper class used to collect the type names we need to construct LoKi functors for various objects
  template <typename T>
  struct TypeHelper {};

  // LoKi type names for Filter<v1::Track>
  template <>
  struct TypeHelper<LHCb::Event::v1::Track> {
    using Predicate                          = LoKi::TrackTypes::TrCut;
    using FunctorFactory                     = LoKi::ITrackFunctorFactory;
    using BooleanConstant                    = LoKi::BasicFunctors<LHCb::Event::v1::Track const*>::BooleanConstant;
    constexpr static auto DefaultFunctorCode = "~TrALL";
    constexpr static auto FunctorFactoryName = "LoKi::Hybrid::TrackFunctorFactory/TrackFunctorFactory:PUBLIC";
  };

  // LoKi type names for Filter<v2::Track>
  template <>
  struct TypeHelper<LHCb::Event::v2::Track> {
    using Predicate       = LoKi::Pr::TrackTypes::TrCut;
    using FunctorFactory  = LoKi::Pr::ITrackFunctorFactory;
    using BooleanConstant = LoKi::BasicFunctors<LoKi::Pr::TrackType const*>::BooleanConstant;
    // constexpr static auto DefaultFunctorCode = "~TrALL";
    // constexpr static auto FunctorFactoryName = "LoKi::Hybrid::Pr::TrackFunctorFactory/PrTrackFunctorFactory:PUBLIC";
  };
} // namespace

namespace Pr {
  /** @class Filter PrFilter.cpp
   *
   *  Filter<T> applies a selection to an input Selection<T> and returns a new Selection<T> object.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). By contruction this is not copied, as the
   *            input/output type Selection<T> is just a view of some other underlying storage.
   */
  template <typename T>
  class LegacyFilter final : public FilterTransform<T> {
  public:
    using FilterTransform<T>::debug;
    using FilterTransform<T>::msgLevel;

    LegacyFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : FilterTransform<T>( name, pSvcLocator, {"Input", ""}, {"Output", ""} ) {}

    std::tuple<bool, Selection<T>> operator()( Selection<T> const& in ) const override {
      auto buffer            = m_cutEff.buffer();
      auto pred_with_counter = [this, &buffer]( auto const& x ) {
        auto dec = this->m_pred( &x );
        buffer += dec; // record selection efficiency
        return dec;
      };

      // Make a new Selection by applying an extra cut to 'in'
      auto filtered = in.select( pred_with_counter );

      // For use in the control flow: did we select anything?
      auto filter_pass = !filtered.empty();

      return {filter_pass, std::move( filtered )};
    }

    StatusCode initialize() override {
      auto sc = FilterTransform<T>::initialize();
      decode();
      return sc;
    }

  private:
    // Counter for recording cut retention statistics
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{this, "Cut selection efficiency"};

    // Boilerplate to load a LoKi filter predicate
    // These two flags are used to ensure that the values of both 'Code' and 'Preambulo' have been loaded before we try
    // to decode the functor
    bool m_code_updated{false};
    bool m_preambulo_updated{false};

    // This is the functor cut string
    Gaudi::Property<std::string> m_code{this, "Code", TypeHelper<T>::DefaultFunctorCode, [this]( auto& ) {
                                          this->m_code_updated = true;
                                          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                          if ( !this->m_preambulo_updated ) return;
                                          this->decode();
                                        }};

    // Finally we can set a list of preambulo statements
    Gaudi::Property<std::vector<std::string>> m_preambulo{this, "Preambulo", {}, [this]( auto& ) {
                                                            this->m_preambulo_updated = true;
                                                            if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED )
                                                              return;
                                                            if ( !this->m_code_updated ) return;
                                                            this->decode();
                                                          }};

    // Use the TypeHelper template defined above to choose the right types/defaults based on the template parameter T
    ToolHandle<typename TypeHelper<T>::FunctorFactory> m_factory{this, "Factory", TypeHelper<T>::FunctorFactoryName};
    typename TypeHelper<T>::Predicate                  m_pred = typename TypeHelper<T>::BooleanConstant( false );

    void decode() {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "decoding " << m_code.value() << endmsg;
      m_factory.retrieve().ignore();
      StatusCode sc = m_factory->get( m_code.value(), m_pred, boost::algorithm::join( m_preambulo.value(), "\n" ) );
      if ( sc.isFailure() ) {
        throw GaudiException{"Failure to decode: " + m_code.value(), "Pr::Filter<T>", StatusCode::FAILURE};
      }
      m_factory.release().ignore();
      m_code_updated      = false;
      m_preambulo_updated = false;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "decoded " << m_code.value() << endmsg;
    }
  };

  DECLARE_COMPONENT_WITH_ID( LegacyFilter<LHCb::Event::v1::Track>, "PrFilter__Track_v1" )
} // namespace Pr
