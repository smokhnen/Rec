/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/ODIN.h"
#include "Event/PrLongTracks.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/IRegistry.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrFTZoneHandler.h"
#include "PrKernel/PrHit.h"
#include <Vc/Vc>
#include <vector>

#include "boost/container/small_vector.hpp"
#include "boost/container/static_vector.hpp"
#include "boost/dynamic_bitset.hpp"
#include <memory>

//-----------------------------------------------------------------------------
// class : PrResidualSeeding
// Store residual Seeding tracks after other Algorithms, e.g. PrMatchNN used
//
// 2020-04-20: Peilian Li
//
//-----------------------------------------------------------------------------

class PrResidualSeeding : public Gaudi::Functional::Transformer<std::vector<LHCb::Event::v2::Track>(
                              const LHCb::Pr::Long::Tracks&, const std::vector<LHCb::Event::v2::Track>& )> {

  using Track  = LHCb::Event::v2::Track;
  using Tracks = std::vector<LHCb::Event::v2::Track>;

public:
  PrResidualSeeding( const std::string& name, ISvcLocator* pSvcLocator );

  Tracks operator()( const LHCb::Pr::Long::Tracks&, const Tracks& ) const override;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( PrResidualSeeding, "PrResidualSeeding" )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrResidualSeeding::PrResidualSeeding( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"MatchTracksLocation", ""}, KeyValue{"SeedTracksLocation", ""}},
                   KeyValue{"SeedTracksOutput", ""} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::vector<LHCb::Event::v2::Track> PrResidualSeeding::operator()( const LHCb::Pr::Long::Tracks& matchtracks,
                                                                   const Tracks&                 seedtracks ) const {
  Tracks tmptracks{};
  tmptracks.reserve( seedtracks.size() );

  if ( seedtracks.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "Seed Track container '" << inputLocation<Tracks>() << "' is empty" << endmsg;
    return tmptracks;
  }

  boost::dynamic_bitset<> used{seedtracks.size(), false};

  auto const longiter = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( matchtracks );
  /// mark used SciFi Hits
  for ( const auto& track : longiter ) {
    const auto trackseed = track.trackSeed().cast();
    used[trackseed]      = true;
  }
  int itrack = -1;
  for ( auto& track : seedtracks ) {
    itrack++;
    if ( used[itrack] ) continue;
    tmptracks.push_back( track );
  }

  return tmptracks;
}
