/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/SmartIF.h"
#include "Linker/LinkerWithKey.h"
#include "MCInterfaces/IMuonPad2MCTool.h"

/** @class TrackAssociator TrackAssociator.h
 *
 *  This algorithm computes the link between a Track and a MCParticle.
 *  The requirement is a match of both the Velo/VP and the T part of the
 *  Track. If there are not enough coordinates, the match is assumed so that
 *  a Velo only or a T only are matched properly.
 *  The required fraction of hits is a jobOption 'FractionOK', default 0.70.
 *
 *  Rewritten for the upgrade, handles all containers in one instance
 *
 *  @author Olivier Callot
 *  @date   2012-04-04
 */

class PrMuonStubAssociator : public GaudiAlgorithm {
public:
  // Standard constructor
  PrMuonStubAssociator( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_container{this, "Container", "Rec/Track/StandaloneMuonFitted"};
  Gaudi::Property<float>       m_matchThreshold{this, "MatchThreshold", 0.7};

  IMuonPad2MCTool* m_muonPad2MC = nullptr;
};

DECLARE_COMPONENT( PrMuonStubAssociator )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrMuonStubAssociator::PrMuonStubAssociator( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode PrMuonStubAssociator::initialize() {
  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return sc; }

  info() << "Processing container: " << m_container.value() << endmsg;

  m_muonPad2MC = tool<IMuonPad2MCTool>( "MuonPad2MCTool", this );
  if ( !m_muonPad2MC ) {
    error() << "Failed to create muonPad2MC tool!" << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PrMuonStubAssociator::execute() {
  // Retrieve the MCParticles
  auto mcParts = getIfExists<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
  if ( !mcParts ) {
    if ( msgLevel( MSG::ERROR ) ) {
      error() << "MCParticles at " << LHCb::MCParticleLocation::Default << "' do not exist" << endmsg;
    }
    return StatusCode::SUCCESS;
  }

  // Create the Linker table from Track to MCParticle
  // Sorted by decreasing weight, so first retrieved has highest weight
  // This has to be done, even if there are no tracks in the event, to satisfy the DST writer
  LinkerWithKey<LHCb::MCParticle, LHCb::Track> myLinker( evtSvc(), msgSvc(), m_container );
  myLinker.reset();

  // Retrieve the Tracks
  auto tracks = getIfExists<LHCb::Track::Range>( m_container );
  if ( tracks.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "No tracks in container '" << m_container.value() << "'. Skipping." << endmsg;
    }
    return StatusCode::SUCCESS;
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Loaded " << tracks.size() << " tracks from '" << m_container.value() << "'" << endmsg;
  }

  for ( const auto& track : tracks ) {
    auto                                      nMuon = 0;
    std::map<LHCb::MCParticle*, unsigned int> matches;
    for ( const auto& id : track->lhcbIDs() ) {
      if ( id.isMuon() ) {
        nMuon++;
        auto mc_part = m_muonPad2MC->Pad2MC( id.muonID() );
        if ( mc_part && mc_part->parent() == mcParts ) { matches[mc_part]++; }
      }
    }

    for ( const auto& match : matches ) {
      auto fraction = 1.0 * match.second / nMuon;
      if ( fraction > m_matchThreshold ) { myLinker.link( track, match.first, fraction ); }
    }
  }

  return StatusCode::SUCCESS;
}
