/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#ifndef PRPIXELFASTKALMAN_H_
#define PRPIXELFASTKALMAN_H_ 1

#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"
#include <cmath>

namespace PrPixel {

  /**
    Bundle of fitted track parameters.
  */
  template <typename F>
  struct SimpleState {
    Vec3<F> pos;

    F tx;
    F ty;

    F covXX;
    F covYY;
    F covXTx;
    F covYTy;
    F covTxTx;
    F covTyTy;
  };

  template <typename F, typename I>
  class SimplifiedKalmanFilter {
    using TracksVP = LHCb::Pr::Velo::Tracks;

  public:
    F m_scatterSensorParameters[4] = {0.54772, 1.478845, 0.626634, -0.78};
    F m_scatterFoilParameters[2]   = {1.67, 20.};

    std::tuple<PrPixel::SimpleState<F>, F, I> operator()( const LHCb::Pr::Velo::Hits& hits, const TracksVP& tracksVP,
                                                          const I idxVP, const F qop ) const {

      const F err = 0.0125;
      const F wx  = err * err;
      const F wy  = wx;

      auto const velotracks = LHCb::Pr::make_zip( tracksVP );
      auto       tracks     = velotracks.gather( idxVP );
      I          nHits      = tracks.nHits();
      I          idxHit0    = tracks.vp_index( 0 );
      const auto hitproxy   = hits.simd();

      PrPixel::SimpleState<F> state;
      state.tx        = tracks.StateDir( 0 ).x;
      state.ty        = tracks.StateDir( 0 ).y;
      auto hit_gather = hitproxy.gather( idxHit0 );
      state.pos       = hit_gather.template get<LHCb::Pr::Velo::VPHitsTag::pos>().vec3();

      state.covXX   = 100.;
      state.covYY   = 100.;
      state.covXTx  = 0.f; // no initial correlation
      state.covYTy  = 0.f;
      state.covTxTx = 0.0001f; // randomly large error
      state.covTyTy = 0.0001f;

      F chi2 = 0.f;

      for ( int i = 1; i < nHits.hmax(); i++ ) {
        // TODO: hit mask (for avx2/avx512)
        I       idxHit     = tracks.vp_index( i );
        auto    hit_gather = hitproxy.gather( idxHit );
        Vec3<F> hit        = hit_gather.template get<LHCb::Pr::Velo::VPHitsTag::pos>().vec3();

        chi2        = chi2 + filter_with_momentum( state.pos.z, state.pos.x, state.tx, state.covXX, state.covXTx,
                                            state.covTxTx, hit.z, hit.x, wx, qop );
        chi2        = chi2 + filter_with_momentum( state.pos.z, state.pos.y, state.ty, state.covYY, state.covYTy,
                                            state.covTyTy, hit.z, hit.y, wy, qop );
        state.pos.z = hit.z;
      }

      /// Convert state at first measurement to state at closest to beam
      const F t2 = sqrt( state.tx * state.tx + state.ty * state.ty );

      const F scat2RFFoil = m_scatterFoilParameters[0] * ( 1.0 + m_scatterFoilParameters[1] * t2 ) * qop * qop;
      state.covTxTx       = state.covTxTx + scat2RFFoil;
      state.covTyTy       = state.covTyTy + scat2RFFoil;

      F zBeam = state.pos.z;
      F denom = state.tx * state.tx + state.ty * state.ty;
      zBeam   = select( denom < 0.001f * 0.001f, zBeam,
                      state.pos.z - ( state.pos.x * state.tx + state.pos.y * state.ty ) / denom );

      const F dz  = zBeam - state.pos.z;
      const F dz2 = dz * dz;

      state.pos.x = state.pos.x + dz * state.tx;
      state.pos.y = state.pos.y + dz * state.ty;

      state.covXX  = state.covXX + dz2 * state.covTxTx + 2 * dz * state.covXTx;
      state.covXTx = state.covXTx + dz * state.covTxTx;
      state.covYY  = state.covYY + dz2 * state.covTyTy + 2 * dz * state.covYTy;
      state.covYTy = state.covYTy + dz * state.covTyTy;

      state.pos.z = zBeam;

      return {state, chi2, 2 * nHits - 4};
    }

  private:
    inline F filter_with_momentum( const F& z, F& x, F& tx, F& covXX, F& covXTx, F& covTxTx, const F& zhit,
                                   const F& xhit, const F& winv, const F& qop ) const {
      // compute prediction
      const F dz    = zhit - z;
      const F predx = x + dz * tx;

      const F dz_t_covTxTx = dz * covTxTx;
      const F dz_t_covXTx  = dz * covXTx;

      F predcovXX   = covXX + 2.f * dz_t_covXTx + dz * dz_t_covTxTx;
      F predcovXTx  = covXTx + dz_t_covTxTx;
      F predcovTxTx = covTxTx;

      // Add noise
      const F par1 = m_scatterSensorParameters[0];
      const F par2 = m_scatterSensorParameters[1];
      const F par6 = m_scatterSensorParameters[2];
      const F par7 = m_scatterSensorParameters[3];

      const F sigTx = par1 * 1e-5 + par2 * abs( qop );
      const F sigX  = par6 * sigTx * abs( dz );
      const F corr  = par7;

      predcovXX   = predcovXX + sigX * sigX;
      predcovXTx  = predcovXTx + corr * sigX * sigTx;
      predcovTxTx = predcovTxTx + sigTx * sigTx;

      // compute the gain matrix
      const F R   = 1.0f / ( winv + predcovXX );
      const F Kx  = predcovXX * R;
      const F KTx = predcovXTx * R;

      // update the state vector
      const F r = xhit - predx;
      x         = predx + Kx * r;
      tx        = tx + KTx * r;

      // update the covariance matrix
      covXX   = ( 1.f - Kx ) * predcovXX;
      covXTx  = ( 1.f - Kx ) * predcovXTx;
      covTxTx = predcovTxTx - KTx * predcovXTx;

      // return the chi2
      return r * r * R;
    }
  };

} // namespace PrPixel

#endif
