/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "PrKernel/PrFTInfo.h"

#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Utils.h"
#include <vector>

/**
 *  @brief Namespace for code related to SciFi hit container
 */
namespace SciFiHits {
  using scalar = SIMDWrapper::scalar::types;
  using simd   = SIMDWrapper::best::types;
  /**
   * @brief increase n by size of one float vector register and make it a multiple
   * @param n size to increase
   * @returns size as multiple of float vector register size plus one register size
   */
  constexpr static int align_size( int n ) { return ( n / simd::size + 1 ) * simd::size; }

  /**
   *  @brief SoA container for SciFi hits
   *  @author André Günther
   *  @date 2019-12-17
   */
  struct PrSciFiHits {

    /**
     * the maximum number of possible hits in the SciFiTracker is a hardware limit determined by the
     * output bandwidth of the TELL40. The more exact number is 45568.
     * (for more details ask Sevda Esen and Olivier Le Dortz)
     */
    constexpr static std::size_t maxNumberOfHits{align_size( 50000 )};
    /**
     * 24 zones plus 2 extra entries for padding
     * the index in the array corresponds to the zone,
     * i.e. zoneIndexes[0] gives the start index of zone 0
     * upper zones have uneven, lower zones have even numbers
     */
    constexpr static std::size_t paddedSize = PrFTInfo::NFTZones + 2;
    alignas( 64 ) std::array<int, paddedSize> zoneIndexes{};
    // TODO: many of these fields can be dropped if the LHCb::FTChannelID (unsigned int) is stored and a mats cache
    // is used in addtion to the zone cache
    std::vector<float>    _x{};
    std::vector<float>    _z0{};
    std::vector<int8_t>   _planeCodes{};
    std::vector<float>    _w{};
    std::vector<float>    _dzDy{};
    std::vector<float>    _dxDy{};
    std::vector<float>    _yMins{};
    std::vector<float>    _yMaxs{};
    std::vector<unsigned> _IDs{};

    PrSciFiHits() {
      _x.reserve( maxNumberOfHits );
      _z0.reserve( maxNumberOfHits );
      _planeCodes.reserve( maxNumberOfHits );
      _w.reserve( maxNumberOfHits );
      _dzDy.reserve( maxNumberOfHits );
      _dxDy.reserve( maxNumberOfHits );
      _yMins.reserve( maxNumberOfHits );
      _yMaxs.reserve( maxNumberOfHits );
      _IDs.reserve( maxNumberOfHits );
    };

    /**
     * accessors for all above defined variables
     * the vectors can also be accessed directly as they are public
     * this is the case when loading several values into vector registers
     */

    SOA_ACCESSOR( x, _x.data() )
    float    x( int index ) const { return _x[index]; }
    float    z( int index ) const { return _z0[index]; }
    float    yMin( int index ) const { return _yMins[index]; }
    float    yMax( int index ) const { return _yMaxs[index]; }
    int      planeCode( int index ) const { return _planeCodes[index]; }
    unsigned ID( int index ) const { return _IDs[index]; }
    float    w( int index ) const { return _w[index]; }
    float    dzDy( int index ) const { return _dzDy[index]; }
    float    dxDy( int index ) const { return _dxDy[index]; }

    /**
     * @brief get size of vectors in SciFiHits
     * @returns size of the _x vector as proxy for the others
     */
    auto size() const -> decltype( _x.size() ) {
      assert( _x.size() == _IDs.size() );
      return _x.size();
    }

    /**
     * @brief wraps std::lower_bound for indices
     * @param start first index of search interval
     * @param end last index of search interval (same behaviour as last iterator)
     * @param val value to compare elements to
     * @returns index of first element that is not less than val, or end if no such element is found
     */
    auto get_lower_bound( int start, int end, float val ) const noexcept {
      const auto begin_hits = _x.begin();
      return std::distance( begin_hits, std::lower_bound( begin_hits + start, begin_hits + end, val ) );
    }

    /**
     * @brief a faster binary search
     * @param N controls number of runrolled steps in the search
     * @param start first index of search interval
     * @param end last index of search interval (same behaviour as last iterator)
     * @param val value to compare elements to
     * @returns index of first element that is not less than val, or end if no such element is found
     */
    template <int N>
    [[gnu::always_inline]] inline auto get_lower_bound_fast( unsigned start, unsigned end, float value ) const {
      auto size = end - start;
      auto low  = start;
      LHCb::Utils::unwind<0, N>( [&]( int ) {
        auto half = size / 2;
        // this multiplication avoids branching
        low += ( _x[low + half] < value ) * ( size - half );
        size = half;
      } );
      do {
        auto half = size / 2;
        low += ( _x[low + half] < value ) * ( size - half );
        size = half;
      } while ( size > 0 );
      return low;
    }

    template <int N>
    auto get_lower_bound_fast_v( simd::int_v start, simd::int_v end, simd::float_v value ) const {
      auto size = end - start;
      auto low  = start;
      LHCb::Utils::unwind<0, N>( [&]( int ) {
        auto       half = size >> 1;
        const auto data = gather_x( low + half );
        low += select( data < value, size - half, 0 );
        size = half;
      } );
      do {
        auto       half = size >> 1;
        const auto data = gather_x( low + half );
        low += select( data < value, size - half, 0 );
        size = half;
      } while ( any( size > 0 ) );

      return low;
    }

    /**
     * @brief method to access zone indices
     * @param zone zone number as assigned in PrFTInfo.h
     * @returns std::pair containing the start and end index of the zone
     * @details A sentinel is placed at the end of each zone. Therefore decrement end index by 1.
     */
    auto getZoneIndices( int zone ) const { return std::pair{zoneIndexes[zone], zoneIndexes[zone + 2] - 1}; }
  };

} // namespace SciFiHits

namespace Forward {
  /**
   * @brief namespace for modifiable SciFiHits
   */
  namespace ModSciFiHits {

    /**
     * @brief SoA layout modifiable SciFiHits
     *
     */
    struct ModPrSciFiHitsSOA {

      std::vector<int>   fulldexes{};
      std::vector<float> coords{};
      std::vector<int>   candidateStartIndex{};
      std::vector<int>   candidateEndIndex{};
      int                _capacity{0};
      int                _size{0};

      ModPrSciFiHitsSOA( std::size_t cap, int candidateCap ) : _capacity{SciFiHits::align_size( cap )} {
        fulldexes.reserve( _capacity );
        coords.reserve( _capacity );
        candidateStartIndex.reserve( candidateCap );
        candidateEndIndex.reserve( candidateCap );
      }

      SOA_ACCESSOR( fulldex, fulldexes.data() )
      SOA_ACCESSOR( coord, coords.data() )
      void updateSizeBy( const std::size_t n ) { _size += n; }
      void clear() {
        _size = 0;
        candidateStartIndex.clear();
        candidateEndIndex.clear();
      }
      void reserve( const std::size_t n ) {
        _capacity = SciFiHits::align_size( n );
        fulldexes.reserve( _capacity );
        coords.reserve( _capacity );
      }
      auto size() const { return _size; }
      auto capacity() const { return _capacity; }
      auto coord( int index ) const { return coords[index]; }
      auto fulldex( int index ) const { return fulldexes[index]; }
    };

    /**
     * @brief AoS layout modifiable SciFiHits
     * @details using gsl::index here avoids a narrowing conversion warning
     */
    struct ModPrSciFiHit {
      float      _coord{0.f};
      gsl::index _fulldex{0};
      auto       coord() const { return _coord; }
      auto       fulldex() const { return _fulldex; }
    };

    template <int N>
    struct ModPrSciFiHitsAOS {

      alignas( 64 ) std::array<ModPrSciFiHit, N> modHits{};
      // unsigned to avoid warnings
      unsigned _size{0};

      void updateSizeBy( int n ) { _size += n; }
      void clear() { _size = 0; }
      auto size() const { return _size; }
      auto coord( int index ) const { return modHits[index].coord(); }
      auto fulldex( int index ) const { return modHits[index].fulldex(); }

      static constexpr auto capacity() { return _capacity; }

    private:
      static constexpr int _capacity{N};
    };

    /**
     * @brief use for comparison of ModPrSciFiHit
     */
    struct byCoord {
      template <typename LHS, typename RHS>
      bool operator()( const LHS& lhs, const RHS& rhs ) const noexcept {
        return lhs.coord() < rhs.coord();
      }
    };

  } // namespace ModSciFiHits
} // namespace Forward
