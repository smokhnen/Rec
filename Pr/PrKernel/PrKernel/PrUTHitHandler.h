/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STl
#include <cassert>
#include <cstdint>
#include <vector>

// Include files
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "Kernel/STLExtensions.h"
#include "PrKernel/UTHitInfo.h"
#include "UTDAQ/UTInfo.h"

#include "Event/PrUTHits.h"

//#include <boost/container/small_vector.hpp>

/**
 *  @class PrUTHitHandler PrUTHitHandler.h
 *  PrUTHitHandler contains the hits in the UT detector and the accessor to them
 *  @author Michel De Cian, based on UTHitHandler
 *  @date   2019-11-13
 *
 */
namespace LHCb::Pr::UT {

  class HitHandler {

  private:
    /// Internal indices storage for ranges
    using HitIndices = std::pair<std::size_t, std::size_t>;
    using HitsInUT   = std::array<HitIndices, static_cast<int>( UTInfo::DetectorNumbers::Layers ) *
                                                static_cast<int>( UTInfo::DetectorNumbers::Stations ) *
                                                static_cast<int>( UTInfo::DetectorNumbers::Regions ) *
                                                static_cast<int>( UTInfo::DetectorNumbers::Sectors )>;

  public:
    using allocator_type = LHCb::Pr::UT::Hits::allocator_type;

    // Method to add Hit in the container
    // This should be changed to float, but needs an adaptation of "DeUTSector"
    void AddHit( const DeUTSector* aSector, unsigned int fullChanIdx, unsigned int strip, double fracStrip,
                 LHCb::UTChannelID chanID, unsigned int /*size*/, bool /*highThreshold*/ ) {
      double dxDy{0};
      double dzDy{0};
      double xAtYEq0{0};
      double zAtYEq0{0};
      double yBegin{0};
      double yEnd{0};
      //--- this method allow to set the values
      const auto fracStripOvfour = fracStrip / 4;
      aSector->trajectory( strip, fracStripOvfour, dxDy, dzDy, xAtYEq0, zAtYEq0, yBegin, yEnd );
      const auto cos = aSector->cosAngle();
      // -- should this not depend on the cluster size?
      const auto pitch  = aSector->pitch();
      const auto weight = 12.0 / ( pitch * pitch );

      // NB : The way the indices are setup here assumes all hits for a given
      //      station, layer, region and sector come in order, which appears
      //      to be the case. This must remain so...
      //
      //      Currently, what I am seeing from the MC has this sorting.
      //      But this would also need to be the case for real data.

      // get the indices for this region
      auto& indices = m_indices[fullChanIdx];

      // if first for this range, set the begin and end indices
      if ( &indices != last_indices ) {
        // check to see if thi range has been filled previously.
        // If it has, assumed ordering is broken
        assert( indices.first == indices.second );
        // reset indices to current end of container
        indices = {m_index, m_index};
        // update used last index cache
        last_indices = &indices;
      }

      // scalar for the moment
      using dType = SIMDWrapper::scalar::types;
      bool mask   = true;
      m_hits.compressstore_channelID<dType::int_v>( m_index, mask, (int)chanID );
      m_hits.compressstore_weight<dType::float_v>( m_index, mask, float( weight ) );
      m_hits.compressstore_xAtYEq0<dType::float_v>( m_index, mask, float( xAtYEq0 ) );
      m_hits.compressstore_yBegin<dType::float_v>( m_index, mask, float( yBegin ) );
      m_hits.compressstore_yEnd<dType::float_v>( m_index, mask, float( yEnd ) );
      m_hits.compressstore_zAtYEq0<dType::float_v>( m_index, mask, float( zAtYEq0 ) );
      m_hits.compressstore_cos<dType::float_v>( m_index, mask, float( cos ) );
      m_hits.compressstore_dxDy<dType::float_v>( m_index, mask, float( dxDy ) );

      m_index++;

      // increment the end index for current range
      ++( indices.second );
    }

    // -- Add correct floating point values to the end
    // -- The just need to be valid floating point values and not meaningful
    // -- in the physics sense. This is to avoid floating point errors
    void addPadding() {
      // -- use the largest vector width possible
      using dType = SIMDWrapper::avx512::types;
      m_hits.store_channelID<dType::int_v>( m_index, 1 );
      m_hits.store_weight<dType::float_v>( m_index, 1e-6f );
      m_hits.store_xAtYEq0<dType::float_v>( m_index, 1e6f );
      m_hits.store_yBegin<dType::float_v>( m_index, 1e6f );
      m_hits.store_yEnd<dType::float_v>( m_index, 1e6f );
      m_hits.store_zAtYEq0<dType::float_v>( m_index, 1e6f );
      m_hits.store_cos<dType::float_v>( m_index, 1e6f );
      m_hits.store_dxDy<dType::float_v>( m_index, 1e6f );
      // -- Don't increase the number of hits
    }

    void copyHit( unsigned int fullChanIdx, int at, const LHCb::Pr::UT::Hits& allhits ) {
      auto& indices = m_indices[fullChanIdx];
      if ( &indices != last_indices ) {
        assert( indices.first == indices.second );
        indices      = {m_index, m_index};
        last_indices = &indices;
      }
      using F = SIMDWrapper::scalar::types::float_v;
      using I = SIMDWrapper::scalar::types::int_v;

      m_hits.store_channelID<I>( m_index, allhits.channelID<I>( at ) );
      m_hits.store_weight<F>( m_index, allhits.weight<F>( at ) );
      m_hits.store_xAtYEq0<F>( m_index, allhits.xAtYEq0<F>( at ) );
      m_hits.store_yBegin<F>( m_index, allhits.yBegin<F>( at ) );
      m_hits.store_yEnd<F>( m_index, allhits.yEnd<F>( at ) );
      m_hits.store_zAtYEq0<F>( m_index, allhits.zAtYEq0<F>( at ) );
      m_hits.store_cos<F>( m_index, allhits.cos<F>( at ) );
      m_hits.store_dxDy<F>( m_index, allhits.dxDy<F>( at ) );

      m_index++;

      ++( indices.second );
    }
    const std::pair<int, int> indices( const int fullChanIdx ) const { return m_indices[fullChanIdx]; }

    const LHCb::Pr::UT::Hits& hits() const { return m_hits; }

    int nHits() const { return m_index; }

    HitHandler( allocator_type alloc = {} ) : m_hits{alloc} {}

  private:
    // Indices for each range
    HitsInUT m_indices;
    int      m_index = 0;
    // single vector of all hits
    LHCb::Pr::UT::Hits m_hits;

    // cache pointer to last indices used
    HitIndices* last_indices = nullptr;
  };
} // namespace LHCb::Pr::UT
