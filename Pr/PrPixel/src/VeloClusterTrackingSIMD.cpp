/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <array>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"

// LHCb
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IConditionDerivationMgr.h"
#include "Event/PrVeloHits.h"
#include "Event/PrVeloTracks.h"
#include "Event/RawEvent.h"
#include "Event/StateParameters.h"
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/VPChannelID.h"
#include "VPDet/DeVP.h"
#include "VPDet/VPDetPaths.h"

// Local
#include "VPClusCache.h"
#include "VeloKalmanHelpers.h"

namespace V2        = LHCb::v2::Event;
namespace TracksTag = LHCb::Pr::Velo::Tag;

namespace LHCb::Pr::Velo {

  namespace VPInfos {
    constexpr int NPlanes           = 26;
    constexpr int NModulesPerPlane  = 2;
    constexpr int NModules          = NPlanes * NModulesPerPlane;
    constexpr int NSensorsPerModule = 4;
    constexpr int NSensors          = NModules * NSensorsPerModule;
    constexpr int NSensorsPerPlane  = NModulesPerPlane * NSensorsPerModule;
  } // namespace VPInfos

  namespace TrackParams {
    constexpr float max_scatter_seeding    = 0.1f;
    constexpr float max_scatter_forwarding = 0.1f;
    constexpr float max_scatter_3hits      = 0.02f;
    constexpr int   max_allowed_skip       = 1;
    constexpr int   max_allowed_skip_full  = 3;
  } // namespace TrackParams

  enum class SearchMode { Default, Fast, Full };

  namespace {
    //=============================================================================
    // Internal data structures:
    //=============================================================================

    constexpr static int reserved_tracks_size = 2048;
    constexpr static int max_hits             = 26;

    namespace LightTrackTag {
      struct p0 : V2::vec3_field {}; // vector
      struct p1 : V2::vec3_field {};
      struct p2 : V2::vec3_field {};
      struct nHits : V2::int_field {};
      struct skipped : V2::int_field {};
      struct sumScatter : V2::float_field {};
      struct hit : V2::ints_field<max_hits> {};

      template <typename T>
      using light_t = V2::SOACollection<T, p0, p1, p2, nHits, skipped, sumScatter, hit>;
    } // namespace LightTrackTag

    struct LightTracksSoA : LightTrackTag::light_t<LightTracksSoA> {
      using base_t = typename LightTrackTag::light_t<LightTracksSoA>;
      using base_t::base_t;
    };

    constexpr static int reserved_hits_inPlane = 1024;
    namespace HitsTag {
      struct pos : V2::vec3_field {}; // vector
      struct phi : V2::float_field {};
      struct idx : V2::int_field {};

      template <typename T>
      using hits_t = V2::SOACollection<T, pos, phi, idx>;
    } // namespace HitsTag

    struct HitsPlane : HitsTag::hits_t<HitsPlane> {
      using base_t = typename HitsTag::hits_t<HitsPlane>;
      using base_t::base_t;
    };
    template <typename T, typename M>
    inline __attribute__( ( always_inline ) ) void removeHits( HitsPlane* hits, M mask, T indices ) {
      auto hit      = hits->scalar();
      int  new_size = hits->size();
      for ( int i = 0; i < new_size; i++ ) {
        if ( any( mask && ( T( hit[i].get<HitsTag::idx>().cast() ) == indices ) ) ) {
          new_size--;
          hit[i].field<HitsTag::pos>().set( hit[new_size].get<HitsTag::pos>().vec3() );
          hit[i].field<HitsTag::phi>().set( hit[new_size].get<HitsTag::phi>() );
          hit[i].field<HitsTag::idx>().set( hit[new_size].get<HitsTag::idx>() );
          i--;
        }
      }
      hits->resize( new_size );
    }
    //=============================================================================
    // Union-Find functions used in clustering:
    //=============================================================================
    inline __attribute__( ( always_inline ) ) int find( int* L, int i ) {
      int ai = L[i];
      while ( ai != L[ai] ) ai = L[ai];
      return ai;
    }

    inline __attribute__( ( always_inline ) ) int merge( int* L, int ai, int j ) { // Union
      int aj = find( L, j );
      if ( ai < aj )
        L[aj] = ai;
      else {
        L[ai] = aj;
        ai    = aj;
      }
      return ai;
    }

    //=============================================================================
    // Clustering Algorithm
    //=============================================================================

    template <RawBank::BankType VP_banktype>
    void getHits( const uint32_t** VPRawBanks, const int sensor0, const int sensor1, const DeVP& vp, HitsPlane& Pout,
                  Velo::Hits& hits );

    template <>
    inline __attribute__( ( always_inline ) ) void
    getHits<RawBank::VP>( const uint32_t** VPRawBanks, // List of input Super-Pixels
                          const int sensor0, const int sensor1, const DeVP& vp, HitsPlane& Pout, Velo::Hits& hits ) {

      // Clustering buffers
      constexpr int MAX_CLUSTERS_PER_SENSOR = 1024;    // Max number of SP per bank
      uint32_t      pixel_SP[MAX_CLUSTERS_PER_SENSOR]; // SP to process
      int           pixel_L[MAX_CLUSTERS_PER_SENSOR];  // Label equivalences
      uint32_t      pixel_SX[MAX_CLUSTERS_PER_SENSOR]; // x sum of clusters' pixels
      uint32_t      pixel_SY[MAX_CLUSTERS_PER_SENSOR]; // y sum of clusters' pixels
      uint32_t      pixel_S[MAX_CLUSTERS_PER_SENSOR];  // number of clusters' pixels
      Pout.resize( 0 );
      int n_hits = 0; // Number of clusters after filtering
      int offset = hits.size();
      for ( int s = sensor0; s < sensor1; s++ ) {
        const uint32_t* bank    = VPRawBanks[s];
        const uint32_t  nsp     = *bank++;
        int             pixel_N = 0; // Pixels to cluster count
        int             labels  = 0; // Total number of generated clusters

        for ( unsigned int i = 0; i < nsp; ++i ) {
          const uint32_t sp_word = *bank++;
          uint8_t        sp      = SP_getPixels( sp_word );

          if ( 0 == sp ) continue; // protect against zero super pixels.

          const auto sp_row = SP_getRow( sp_word );
          const auto sp_col = SP_getCol( sp_word );

          // This SP is isolated, skip clustering :
          if ( SP_isIsolated( sp_word ) ) {
            uint8_t mask = s_SPMasks[sp];

            auto n_kx_ky = s_SPn_kx_ky[sp & mask];
            auto n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
            auto kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
            auto ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

            pixel_SX[labels] = sp_col * n * 2 + kx;
            pixel_SY[labels] = sp_row * n * 4 + ky;
            pixel_S[labels]  = n;
            labels++;

            if ( mask != 0xFF ) { // Add 2nd cluster
              n_kx_ky = s_SPn_kx_ky[sp & ( ~mask )];
              n       = SPn_kx_ky_getN( n_kx_ky );  // number of pixel in this sp
              kx      = SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
              ky      = SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

              pixel_SX[labels] = sp_col * n * 2 + kx;
              pixel_SY[labels] = sp_row * n * 4 + ky;
              pixel_S[labels]  = n;
              labels++;
            }

            continue;
          }

          // This one is not isolated, add it to clustering :
          uint32_t mask     = 0x7FFFFF00 | s_SPMasks[sp];
          pixel_SP[pixel_N] = sp_word & mask;
          pixel_L[pixel_N]  = pixel_N;
          pixel_N++;

          if ( mask != 0x7FFFFFFF ) {                      // Add 2nd cluster
            pixel_SP[pixel_N] = sp_word & ( mask ^ 0xFF ); // ~ of low 8 bits
            pixel_L[pixel_N]  = pixel_N;
            pixel_N++;
          }
        } // loop over super pixels in raw bank

        // SparseCCL: Connected Components Labeling and Analysis for sparse images
        // https://hal.archives-ouvertes.fr/hal-02343598/document
        // (This version assume SP are ordered by col then row)
        int start_j = 0;
        for ( int i = 0; i < pixel_N; i++ ) { // Pixel Scan
          uint32_t sp_word_i, sp_word_j;
          uint32_t ai;
          uint8_t  spi, spj;
          int      x_i, x_j, y_i, y_j;

          sp_word_i = pixel_SP[i];
          spi       = SP_getPixels( sp_word_i );
          y_i       = SP_getRow( sp_word_i );
          x_i       = SP_getCol( sp_word_i );

          pixel_L[i] = i;
          ai         = i;

          for ( int j = start_j; j < i; j++ ) {
            sp_word_j = pixel_SP[j];
            spj       = SP_getPixels( sp_word_j );
            y_j       = SP_getRow( sp_word_j );
            x_j       = SP_getCol( sp_word_j );
            if ( is_adjacent_8C_SP( x_i, y_i, x_j, y_j, spi, spj ) ) {
              ai = merge( pixel_L, ai, j );
            } else if ( x_j + 1 < x_i )
              start_j++;
          }
        }

        for ( int i = 0; i < pixel_N; i++ ) { // Transitive Closure + CCA
          uint32_t sp_word = pixel_SP[i];
          uint8_t  sp      = SP_getPixels( sp_word );
          uint32_t y_i     = SP_getRow( sp_word );
          uint32_t x_i     = SP_getCol( sp_word );

          auto n_kx_ky = s_SPn_kx_ky[sp];
          auto n       = SPn_kx_ky_getN( n_kx_ky );                // number of pixel in this sp
          auto kx      = x_i * n * 2 + SPn_kx_ky_getKx( n_kx_ky ); // sum of x in this sp
          auto ky      = y_i * n * 4 + SPn_kx_ky_getKy( n_kx_ky ); // sum of y in this sp

          uint32_t l;
          if ( pixel_L[i] == i ) {
            l           = labels++; // new label
            pixel_SX[l] = kx;
            pixel_SY[l] = ky;
            pixel_S[l]  = n;
          } else {
            l = pixel_L[pixel_L[i]]; // transitive closure
            pixel_SX[l] += kx;
            pixel_SY[l] += ky;
            pixel_S[l] += n;
          }
          pixel_L[i] = l;
        }

        auto ltg = vp.ltg( s );
        for ( int i = 0; i < labels; i++ ) {
          uint32_t n = pixel_S[i];

          uint32_t x = pixel_SX[i];
          uint32_t y = pixel_SY[i];

          const uint32_t cx = x / n;
          const uint32_t cy = y / n;

          // store target (3D point for tracking)
          const float fx      = x / static_cast<float>( n ) - cx;
          const float fy      = y / static_cast<float>( n );
          const float local_x = vp.local_x( cx ) + fx * vp.x_pitch( cx );
          const float local_y = ( 0.5f + fy ) * vp.pixel_size();

          const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          auto hit = hits.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          hit.field<Velo::VPHitsTag::pos>().set( gx, gy, gz );
          hit.field<Velo::VPHitsTag::ChannelId>().set( SIMDWrapper::scalar::int_v( VPChannelID( s, cx, cy ) ) );

          auto pout = Pout.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          pout.field<HitsTag::pos>().set( gx, gy, gz );
          pout.field<HitsTag::idx>().set( SIMDWrapper::scalar::int_v( offset + n_hits ) );

          n_hits++;
        }
      } // Loop over sensors

      // Pre-compute phi
      for ( auto pout : Pout.simd() ) {
        auto pos = pout.get<HitsTag::pos>().vec3();
        pout.field<HitsTag::phi>().set( pos.phi() );
      }
    }

    template <>
    inline __attribute__( ( always_inline ) ) void
    getHits<RawBank::VPRetinaCluster>( const uint32_t** VPRawBanks, // List of input Super-Pixels
                                       const int sensor0, const int sensor1, const DeVP& vp, HitsPlane& Pout,
                                       Velo::Hits& hits ) {

      Pout.resize( 0 );
      int n_hits = 0; // Number of clusters after filtering
      int offset = hits.size();
      for ( int s = sensor0; s < sensor1; s++ ) {
        const uint32_t* bank = VPRawBanks[s];
        const uint32_t  nrc  = *bank++;

        auto ltg = vp.ltg( s );
        for ( unsigned int i = 0; i < nrc; ++i ) {
          const uint32_t rc_word = *bank++;

          const uint32_t cx = ( rc_word >> 14 ) & 0x3FF;
          const float    fx = ( ( rc_word >> 11 ) & 0x7 ) / 8.f;
          const uint32_t cy = ( rc_word >> 3 ) & 0xFF;
          const float    fy = ( rc_word & 0x7FF ) / 8.f;

          const float local_x = vp.local_x( cx ) + fx * vp.x_pitch( cx );
          const float local_y = ( 0.5f + fy ) * vp.pixel_size();

          const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

          auto hit = hits.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          hit.field<Velo::VPHitsTag::pos>().set( gx, gy, gz );
          hit.field<Velo::VPHitsTag::ChannelId>().set( SIMDWrapper::scalar::int_v( VPChannelID( s, cx, cy ) ) );

          auto pout = Pout.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          pout.field<HitsTag::pos>().set( gx, gy, gz );
          pout.field<HitsTag::idx>().set( SIMDWrapper::scalar::int_v( offset + n_hits ) );

          n_hits++;
        }
      } // Loop over sensors

      // Pre-compute phi
      for ( auto pout : Pout.simd() ) {
        auto pos = pout.get<HitsTag::pos>().vec3();
        pout.field<HitsTag::phi>().set( pos.phi() );
      }
    }

    // ===========================================================================
    // Tracking algorithm
    // ===========================================================================

    template <int N, typename F, typename I>
    inline __attribute__( ( always_inline ) ) void closestsHitsInPhi( HitsPlane* P0, F phi1, Vec3<F> p0_pos[N],
                                                                      I p0_idx[N] ) {

      F distances[N];

      // fill the first hits
      const auto hitP0 = P0->scalar();
      for ( int i = 0; i < N; i++ ) {
        auto sPos = hitP0[i].get<HitsTag::pos>().vec3();
        auto sIdx = hitP0[i].get<HitsTag::idx>();
        auto sPhi = hitP0[i].get<HitsTag::phi>();

        p0_pos[i]    = Vec3<F>( sPos.x, sPos.y, sPos.z );
        p0_idx[i]    = sIdx.cast();
        distances[i] = abs( phi1 - F( sPhi ) );
      }

      for ( int i = N; i < static_cast<int>( P0->size() ); i++ ) {
        auto sPos = hitP0[i].get<HitsTag::pos>().vec3();
        auto sIdx = hitP0[i].get<HitsTag::idx>();
        auto sPhi = hitP0[i].get<HitsTag::phi>();

        Vec3<F> pos = Vec3<F>( sPos.x, sPos.y, sPos.z );
        I       idx = sIdx.cast();
        F       dis = abs( phi1 - F( sPhi ) );

        for ( int j = 0; j < N; j++ ) {
          auto mask = dis < distances[j];

          swap( mask, distances[j], dis );
          swap( mask, p0_pos[j].x, pos.x );
          swap( mask, p0_pos[j].y, pos.y );
          swap( mask, p0_pos[j].z, pos.z );
          swap( mask, p0_idx[j], idx );
        }
      }
    }

    void TrackSeeding( HitsPlane* P0, HitsPlane* P1, HitsPlane* P2, LightTracksSoA* tracks ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      using sF   = SIMDWrapper::scalar::float_v;

      const int N_CANDIDATES = 3;
      Vec3<F>   p0_candidates_pos[N_CANDIDATES];
      I         p0_candidates_idx[N_CANDIDATES];

      if ( P0->size() == 0 || P1->size() == 0 || P2->size() == 0 ) return;
      int        P1size = P1->size();
      const auto hitP1  = P1->simd();
      P1->resize( 0 );
      for ( int h1 = 0; h1 < P1size; h1 += simd::size ) {
        auto loop_mask = simd::loop_mask( h1, P1size );

        const Vec3<F> p1   = hitP1[h1].get<HitsTag::pos>().vec3();
        const F       phi1 = hitP1[h1].get<HitsTag::phi>();
        const I       vh1  = hitP1[h1].get<HitsTag::idx>();

        F bestFit = TrackParams::max_scatter_seeding;
        I vh0     = 0;
        I vh2     = 0;

        Vec3<F> bestP0 = Vec3<F>( 0, 0, 0 );
        Vec3<F> bestP2 = Vec3<F>( 0, 0, 0 );

        closestsHitsInPhi<N_CANDIDATES>( P0, phi1, p0_candidates_pos, p0_candidates_idx );

        for ( const auto& hitP2 : P2->scalar() ) {
          const I        cid2       = hitP2.get<HitsTag::idx>().cast();
          const Vec3<sF> sp2        = hitP2.get<HitsTag::pos>().vec3();
          const Vec3<F>  p2         = Vec3<F>( sp2.x, sp2.y, sp2.z );
          auto           m_found_h0 = simd::mask_false();

          for ( int i = 0; i < N_CANDIDATES; i++ ) {
            if ( i >= static_cast<int>( P0->size() ) ) break;

            const Vec3<F> p0 = p0_candidates_pos[i];
            const I       h0 = p0_candidates_idx[i];

            const F vr = ( p2.z - p0.z ) / ( p1.z - p0.z );
            const F x  = ( p1.x - p0.x ) * vr + p0.x;
            const F y  = ( p1.y - p0.y ) * vr + p0.y;

            const F dx      = x - p2.x;
            const F dy      = y - p2.y;
            const F scatter = dx * dx + dy * dy;

            auto m_bestfit = loop_mask && ( scatter < bestFit );

            if ( none( m_bestfit ) ) continue;

            m_found_h0 = m_found_h0 || m_bestfit;

            bestFit  = select( m_bestfit, scatter, bestFit );
            bestP0.x = select( m_bestfit, p0.x, bestP0.x );
            bestP0.y = select( m_bestfit, p0.y, bestP0.y );
            bestP0.z = select( m_bestfit, p0.z, bestP0.z );
            vh0      = select( m_bestfit, h0, vh0 );
          } // n_0_candidates
          bestP2.x = select( m_found_h0, p2.x, bestP2.x );
          bestP2.y = select( m_found_h0, p2.y, bestP2.y );
          bestP2.z = select( m_found_h0, p2.z, bestP2.z );
          vh2      = select( m_found_h0, cid2, vh2 );
        } // h2

        auto bestH0 = loop_mask && ( bestFit < TrackParams::max_scatter_seeding );

        // Remove used hits
        auto m_remove_p1 = loop_mask && !bestH0;
        auto newP1       = P1->compress_back<SIMDWrapper::InstructionSet::Best>( m_remove_p1 );
        newP1.field<HitsTag::pos>().set( p1 );
        newP1.field<HitsTag::phi>().set( phi1 );
        newP1.field<HitsTag::idx>().set( vh1 );
        if ( none( bestH0 ) ) continue;

        removeHits( P0, bestH0, vh0 );
        removeHits( P2, bestH0, vh2 );

        int i = tracks->size();
        tracks->resize( i + simd::popcount( bestH0 ) );

        tracks->store<LightTrackTag::sumScatter>( i, bestFit, bestH0 );
        tracks->store<LightTrackTag::p0>( i, 0, bestP0.x, bestH0 );
        tracks->store<LightTrackTag::p0>( i, 1, bestP0.y, bestH0 );
        tracks->store<LightTrackTag::p0>( i, 2, bestP0.z, bestH0 );
        tracks->store<LightTrackTag::p1>( i, 0, p1.x, bestH0 );
        tracks->store<LightTrackTag::p1>( i, 1, p1.y, bestH0 );
        tracks->store<LightTrackTag::p1>( i, 2, p1.z, bestH0 );
        tracks->store<LightTrackTag::p2>( i, 0, bestP2.x, bestH0 );
        tracks->store<LightTrackTag::p2>( i, 1, bestP2.y, bestH0 );
        tracks->store<LightTrackTag::p2>( i, 2, bestP2.z, bestH0 );
        tracks->store<LightTrackTag::nHits>( i, I( 3 ) );
        tracks->store<LightTrackTag::skipped>( i, I( 0 ) );
        tracks->store<LightTrackTag::hit>( i, 0, vh0, bestH0 );
        tracks->store<LightTrackTag::hit>( i, 1, vh1, bestH0 );
        tracks->store<LightTrackTag::hit>( i, 2, vh2, bestH0 );

      } // h1
    }

    template <SearchMode searchMode, size_t N>
    inline __attribute__( ( always_inline ) ) void
    TrackForwarding( const LightTracksSoA* tracks, std::array<HitsPlane*, N> P2s, LightTracksSoA* tracks_forwarded,
                     LightTracksSoA* tracks_finalized ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      using sF   = SIMDWrapper::scalar::float_v;

      tracks_forwarded->resize( 0 );

      for ( const auto& track : tracks->simd() ) {
        const auto mask = track.loop_mask();

        Vec3<F> p0 = track.get<LightTrackTag::p1>().vec3();
        Vec3<F> p1 = track.get<LightTrackTag::p2>().vec3();

        F       bestFit = TrackParams::max_scatter_forwarding;
        Vec3<F> bestP2  = p1;
        I       bestH2  = 0;

        const F tx = ( p1.x - p0.x ) / ( p1.z - p0.z );
        const F ty = ( p1.y - p0.y ) / ( p1.z - p0.z );

        for ( HitsPlane* P2 : P2s ) {
          for ( const auto hitP2 : P2->scalar() ) {
            const Vec3<sF> sp2 = hitP2.get<HitsTag::pos>().vec3();
            const Vec3<F>  p2  = Vec3<F>( sp2.x, sp2.y, sp2.z );

            const F dz = p2.z - p0.z;
            const F x  = tx * dz + p0.x;
            const F y  = ty * dz + p0.y;

            const F dx      = x - p2.x;
            const F dy      = y - p2.y;
            const F scatter = dx * dx + dy * dy;

            const auto m_bestfit = mask && ( scatter < bestFit );

            if ( none( m_bestfit ) ) continue;

            bestFit  = select( m_bestfit, scatter, bestFit );
            bestP2.x = select( m_bestfit, p2.x, bestP2.x );
            bestP2.y = select( m_bestfit, p2.y, bestP2.y );
            bestP2.z = select( m_bestfit, p2.z, bestP2.z );
            bestH2   = select( m_bestfit, I( hitP2.get<HitsTag::idx>().cast() ), bestH2 );
          }
        }

        auto bestH0 = mask && ( bestFit < TrackParams::max_scatter_forwarding );

        // Finish loading tracks
        const Vec3<F> tp0         = track.get<LightTrackTag::p0>().vec3();
        I             n_hits      = track.get<LightTrackTag::nHits>();
        I             skipped     = track.get<LightTrackTag::skipped>();
        F             sum_scatter = track.get<LightTrackTag::sumScatter>();

        // increment or reset to 0
        skipped = select( bestH0, 0, skipped + 1 );

        // increment only if we found a hit
        n_hits      = select( bestH0, n_hits + 1, n_hits );
        sum_scatter = select( bestH0, sum_scatter + bestFit, sum_scatter );

        p1.x = select( bestH0, p1.x, p0.x );
        p1.y = select( bestH0, p1.y, p0.y );
        p1.z = select( bestH0, p1.z, p0.z );

        constexpr auto max_skips =
            ( searchMode == SearchMode::Full ? TrackParams::max_allowed_skip_full : TrackParams::max_allowed_skip ) + 1;
        auto m_forward = mask && ( skipped < I( max_skips ) );

        // Remove used hits
        for ( auto P2 : P2s ) removeHits( P2, bestH0, bestH2 );

        // Forward tracks
        {
          auto track_fwd = tracks_forwarded->compress_back<SIMDWrapper::InstructionSet::Best>( m_forward );

          track_fwd.template field<LightTrackTag::sumScatter>().set( sum_scatter );
          track_fwd.template field<LightTrackTag::p0>().set( tp0 );
          track_fwd.template field<LightTrackTag::p1>().set( p1 );
          track_fwd.template field<LightTrackTag::p2>().set( bestP2 );
          track_fwd.template field<LightTrackTag::nHits>().set( n_hits );
          track_fwd.template field<LightTrackTag::skipped>().set( skipped );

          int max_n_hits = n_hits.hmax( m_forward );
          for ( int j = 0; j < max_n_hits; j++ ) {
            auto push_hit = bestH0 && ( n_hits == I( j + 1 ) );
            I    id       = select( push_hit, bestH2, track.get<LightTrackTag::hit>( j ) );
            track_fwd.template field<LightTrackTag::hit>( j ).set( id );
          }
        }

        // Finalize track
        auto m_final = mask && !m_forward && ( n_hits > 3 || sum_scatter < TrackParams::max_scatter_3hits );

        if ( none( m_final ) ) continue; // Nothing to finalize

        {
          auto track_final = tracks_finalized->compress_back<SIMDWrapper::InstructionSet::Best>( m_final );

          track_final.template field<LightTrackTag::sumScatter>().set( sum_scatter );
          track_final.template field<LightTrackTag::p0>().set( tp0 );
          track_final.template field<LightTrackTag::p1>().set( p1 );
          track_final.template field<LightTrackTag::p2>().set( bestP2 );
          track_final.template field<LightTrackTag::nHits>().set( n_hits );
          track_final.template field<LightTrackTag::skipped>().set( skipped );

          int max_n_hits = n_hits.hmax( m_final );
          for ( int j = 0; j < max_n_hits; j++ ) {
            I id = track.get<LightTrackTag::hit>( j );
            track_final.template field<LightTrackTag::hit>( j ).set( id );
          }
        }
      }
    }

    inline __attribute__( ( always_inline ) ) void copy_remaining( const LightTracksSoA* tracks_candidates,
                                                                   LightTracksSoA*       tracks ) {
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      for ( const auto& trackcand : tracks_candidates->simd() ) {
        auto loop_mask = trackcand.loop_mask();

        I n_hits      = trackcand.get<LightTrackTag::nHits>();
        F sum_scatter = trackcand.get<LightTrackTag::sumScatter>();

        auto m_final = loop_mask && ( n_hits > 3 || sum_scatter < TrackParams::max_scatter_3hits );

        auto track = tracks->compress_back( m_final );

        track.field<LightTrackTag::sumScatter>().set( sum_scatter );
        track.field<LightTrackTag::p0>().set( trackcand.get<LightTrackTag::p0>().vec3() );
        track.field<LightTrackTag::p1>().set( trackcand.get<LightTrackTag::p1>().vec3() );
        track.field<LightTrackTag::p2>().set( trackcand.get<LightTrackTag::p2>().vec3() );
        track.field<LightTrackTag::nHits>().set( n_hits );
        track.field<LightTrackTag::skipped>().set( trackcand.get<LightTrackTag::skipped>() );

        int max_n_hits = n_hits.hmax( m_final );
        for ( int j = 0; j < max_n_hits; j++ ) {
          const auto id = trackcand.get<LightTrackTag::hit>( j );
          track.field<LightTrackTag::hit>( j ).set( id );
        }
      }
    }

    template <typename T>
    inline __attribute__( ( always_inline ) ) void swap( T& a, T& b ) {
      auto tmp = a;
      a        = b;
      b        = tmp;
    }

    template <typename T>
    inline __attribute__( ( always_inline ) ) void rotate( T& a, T& b, T& c ) {
      auto tmp = a;
      a        = b;
      b        = c;
      c        = tmp;
    }

    template <typename T>
    inline __attribute__( ( always_inline ) ) void rotate( T& a, T& b, T& c, T& d, T& e ) {
      auto tmp = a;
      a        = b;
      b        = c;
      c        = d;
      d        = e;
      e        = tmp;
    }

  } // namespace

  /**
   * @class ClusterTrackingSIMD
   * Clustering and Velo Tracking algorithm using SIMD
   * https://arxiv.org/abs/1912.09901
   *
   * @author Arthur Hennequin (CERN, LIP6)
   */
  template <RawBank::BankType VP_banktype, SearchMode searchMode>
  class ClusterTrackingSIMD
      : public Gaudi::Functional::MultiTransformer<std::tuple<Hits, Tracks, Tracks>( const EventContext&,
                                                                                     const RawEvent&, const DeVP& ),
                                                   LHCb::DetDesc::usesConditions<DeVP>> {

  public:
    //=============================================================================
    // Standard constructor, initializes variables
    //=============================================================================
    ClusterTrackingSIMD( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer(
              name, pSvcLocator,
              {KeyValue{"RawEventLocation", RawEventLocation::Default}, KeyValue{"DEVP", LHCb::Det::VP::det_path}},
              {KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
               KeyValue{"TracksLocation", "Rec/Track/Velo"}} ) {}

    //=============================================================================
    // Main execution
    //=============================================================================
    std::tuple<Velo::Hits, Velo::Tracks, Velo::Tracks> operator()( const EventContext& evtCtx, const RawEvent& rawEvent,
                                                                   const DeVP& devp ) const override {
      using Tracks = Velo::Tracks;
      using Hits   = Velo::Hits;

      std::tuple<Hits, Tracks, Tracks> result{std::allocator_arg, LHCb::getMemResource( evtCtx ),
                                              Zipping::generateZipIdentifier(), Zipping::generateZipIdentifier(),
                                              Zipping::generateZipIdentifier()};
      auto& [hits, tracksBackward, tracksForward] = result;
      hits.reserve( 10000 );

      const auto& tBanks = rawEvent.banks( VP_banktype );
      if ( tBanks.empty() ) return result;

      const unsigned int version = tBanks[0]->version();
      if ( version != 2 ) {
        warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
        return result;
      }

      const uint32_t* VPRawBanks[VPInfos::NSensors];

      // Copy rawbanks pointers to protect against unordered data
      for ( auto iterBank = tBanks.begin(); iterBank != tBanks.end(); iterBank++ ) {
        const uint32_t sensor = ( *iterBank )->sourceID();
        VPRawBanks[sensor]    = ( *iterBank )->data();
      }

      // Tracking buffers
      HitsPlane raw_P0{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      HitsPlane raw_P1{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      HitsPlane raw_P2{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      raw_P0.reserve( reserved_hits_inPlane );
      raw_P1.reserve( reserved_hits_inPlane );
      raw_P2.reserve( reserved_hits_inPlane );
      HitsPlane *P0 = &raw_P0, *P1 = &raw_P1, *P2 = &raw_P2;

      LightTracksSoA t_candidates{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      LightTracksSoA t_forwarded{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      LightTracksSoA tracks{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      t_candidates.reserve( reserved_tracks_size );
      t_forwarded.reserve( reserved_tracks_size );
      tracks.reserve( reserved_tracks_size );
      LightTracksSoA *tracks_candidates = &t_candidates, *tracks_forwarded = &t_forwarded;

      // Do tracking backward
      if constexpr ( searchMode == SearchMode::Fast ) {
        HitsPlane raw_P0_2, raw_P1_2, raw_P2_2;
        raw_P0_2.reserve( reserved_hits_inPlane );
        raw_P1_2.reserve( reserved_hits_inPlane );
        raw_P2_2.reserve( reserved_hits_inPlane );
        HitsPlane *P0_2 = &raw_P0_2, *P1_2 = &raw_P1_2, *P2_2 = &raw_P2_2;

        const int i0 = VPInfos::NPlanes - 1, i1 = VPInfos::NPlanes - 2, i2 = VPInfos::NPlanes - 3;

        // Side 1
        int sensor0 = i0 * VPInfos::NSensorsPerPlane;
        int sensor1 = i1 * VPInfos::NSensorsPerPlane;
        int sensor2 = i2 * VPInfos::NSensorsPerPlane;
        getHits<VP_banktype>( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerModule, devp, *P0, hits );
        getHits<VP_banktype>( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerModule, devp, *P1, hits );
        getHits<VP_banktype>( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerModule, devp, *P2, hits );
        TrackSeeding( P0, P1, P2, tracks_candidates );

        // Side 2
        sensor0 += VPInfos::NSensorsPerModule;
        sensor1 += VPInfos::NSensorsPerModule;
        sensor2 += VPInfos::NSensorsPerModule;

        getHits<VP_banktype>( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerModule, devp, *P0_2, hits );
        getHits<VP_banktype>( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerModule, devp, *P1_2, hits );
        getHits<VP_banktype>( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerModule, devp, *P2_2, hits );
        TrackSeeding( P0_2, P1_2, P2_2, tracks_candidates );

        rotate( P0, P1, P2 );
        rotate( P0_2, P1_2, P2_2 );

        for ( int p = VPInfos::NPlanes - 4; p >= 0; p-- ) {
          const int sensor = p * VPInfos::NSensorsPerPlane;
          getHits<VP_banktype>( VPRawBanks, sensor, sensor + VPInfos::NSensorsPerModule, devp, *P2, hits );
          getHits<VP_banktype>( VPRawBanks, sensor + VPInfos::NSensorsPerModule,
                                sensor + 2 * VPInfos::NSensorsPerModule, devp, *P2_2, hits );

          TrackForwarding<searchMode>( tracks_candidates, std::array<HitsPlane*, 2>( {P2, P2_2} ), tracks_forwarded,
                                       &tracks );

          swap( tracks_candidates, tracks_forwarded );

          TrackSeeding( P0, P1, P2, tracks_candidates );
          TrackSeeding( P0_2, P1_2, P2_2, tracks_candidates );

          rotate( P0, P1, P2 );
          rotate( P0_2, P1_2, P2_2 );
        }

        copy_remaining( tracks_candidates, &tracks );

      } else if constexpr ( searchMode == SearchMode::Full ) {

        // Additional tracking buffers:
        HitsPlane raw_P3{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
        HitsPlane raw_P4{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
        raw_P3.reserve( reserved_hits_inPlane );
        raw_P4.reserve( reserved_hits_inPlane );
        HitsPlane *P3 = &raw_P3, *P4 = &raw_P4;

        // Prologue
        int sensor0 = ( 2 * VPInfos::NPlanes - 1 ) * VPInfos::NSensorsPerModule;
        int sensor1 = ( 2 * VPInfos::NPlanes - 2 ) * VPInfos::NSensorsPerModule;
        int sensor2 = ( 2 * VPInfos::NPlanes - 3 ) * VPInfos::NSensorsPerModule;
        getHits<VP_banktype>( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerModule, devp, *P0, hits );
        getHits<VP_banktype>( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerModule, devp, *P1, hits );
        getHits<VP_banktype>( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerModule, devp, *P2, hits );

        TrackSeeding( P0, P1, P2, tracks_candidates );

        int sensor3 = ( 2 * VPInfos::NPlanes - 4 ) * VPInfos::NSensorsPerModule;
        getHits<VP_banktype>( VPRawBanks, sensor3, sensor3 + VPInfos::NSensorsPerModule, devp, *P3, hits );

        TrackForwarding<searchMode>( tracks_candidates, std::array<HitsPlane*, 1>( {P3} ), tracks_forwarded, &tracks );

        swap( tracks_candidates, tracks_forwarded );
        TrackSeeding( P0, P1, P3, tracks_candidates );
        TrackSeeding( P0, P2, P3, tracks_candidates );
        TrackSeeding( P1, P2, P3, tracks_candidates );

        // Loop
        for ( int p = 2 * VPInfos::NPlanes - 5; p >= 0; p-- ) {
          const int sensor = p * VPInfos::NSensorsPerModule;
          getHits<VP_banktype>( VPRawBanks, sensor, sensor + VPInfos::NSensorsPerModule, devp, *P4, hits );
          TrackForwarding<searchMode>( tracks_candidates, std::array<HitsPlane*, 1>( {P4} ), tracks_forwarded,
                                       &tracks );

          swap( tracks_candidates, tracks_forwarded );
          TrackSeeding( P0, P2, P4, tracks_candidates );
          TrackSeeding( P1, P2, P4, tracks_candidates );
          TrackSeeding( P1, P3, P4, tracks_candidates );
          TrackSeeding( P2, P3, P4, tracks_candidates );

          rotate( P0, P1, P2, P3, P4 );
        }

        copy_remaining( tracks_candidates, &tracks );
      } else {

        const int i0 = VPInfos::NPlanes - 1, i1 = VPInfos::NPlanes - 2, i2 = VPInfos::NPlanes - 3;

        const int sensor0 = i0 * VPInfos::NSensorsPerPlane;
        const int sensor1 = i1 * VPInfos::NSensorsPerPlane;
        const int sensor2 = i2 * VPInfos::NSensorsPerPlane;

        getHits<VP_banktype>( VPRawBanks, sensor0, sensor0 + VPInfos::NSensorsPerPlane, devp, *P0, hits );
        getHits<VP_banktype>( VPRawBanks, sensor1, sensor1 + VPInfos::NSensorsPerPlane, devp, *P1, hits );
        getHits<VP_banktype>( VPRawBanks, sensor2, sensor2 + VPInfos::NSensorsPerPlane, devp, *P2, hits );
        TrackSeeding( P0, P1, P2, tracks_candidates );
        rotate( P0, P1, P2 );

        for ( int p = VPInfos::NPlanes - 4; p >= 0; p-- ) {
          const int sensor = p * VPInfos::NSensorsPerPlane;
          getHits<VP_banktype>( VPRawBanks, sensor, sensor + VPInfos::NSensorsPerPlane, devp, *P2, hits );
          TrackForwarding<searchMode>( tracks_candidates, std::array<HitsPlane*, 1>( {P2} ), tracks_forwarded,
                                       &tracks );
          swap( tracks_candidates, tracks_forwarded );
          TrackSeeding( P0, P1, P2, tracks_candidates );
          rotate( P0, P1, P2 );
        }

        copy_remaining( tracks_candidates, &tracks );
      } // backward tracking

      // Make LHCb tracks
      tracksBackward.reserve( reserved_tracks_size ); // reserve capacity at a reasonable large size
      tracksForward.reserve( reserved_tracks_size );
      using simd = SIMDWrapper::avx256::types;
      using I    = simd::int_v;
      using F    = simd::float_v;
      for ( const auto track : tracks.simd() ) {
        auto loop_mask = track.loop_mask();

        // Simple fit
        auto p1 = track.get<LightTrackTag::p0>().vec3();
        auto p2 = track.get<LightTrackTag::p2>().vec3();
        auto d  = p1 - p2;

        auto tx = d.x / d.z;
        auto ty = d.y / d.z;
        auto x0 = p2.x - p2.z * tx;
        auto y0 = p2.y - p2.z * ty;

        F z_beam = p1.z;
        F denom  = tx * tx + ty * ty;
        z_beam   = select( denom < 0.001f * 0.001f, z_beam, -( x0 * tx + y0 * ty ) / denom );

        Vec3<F>    dir      = Vec3<F>( tx, ty, 1.f );
        auto const bwd_size = tracksBackward.size();
        auto const fwd_size = tracksForward.size();

        auto backwards = ( z_beam > p2.z ) && loop_mask;
        tracksBackward.resize( bwd_size + simd::popcount( backwards ) );

        auto n_hits = track.get<LightTrackTag::nHits>();
        // in case the number of hits exceed the maximum 26
        auto max_hits = min( n_hits, int( LHCb::Pr::Velo::Tracks::MaxVPHits ) ).hmax( loop_mask );
        tracksBackward.store<TracksTag::nVPHits>( bwd_size, n_hits, backwards );
        // Store backward tracks
        const auto hitproxy = hits.simd();
        for ( int h = 0; h < max_hits; h++ ) {
          auto       hit_index = track.get<LightTrackTag::hit>( h );
          const auto indices   = select( backwards && h < n_hits, hit_index, 0 );
          auto       hit       = hitproxy.gather( indices );
          tracksBackward.store_vp_index( bwd_size, h, hit_index, backwards );
          tracksBackward.store_lhcbID(
              bwd_size, h, LHCbID::make( LHCbID::channelIDtype::VP, hit.template get<Velo::VPHitsTag::ChannelId>() ),
              backwards );
        }
        tracksBackward.store_StatePosDir( bwd_size, 0, Vec3<F>( 1.f, 1.f, 1.f ), dir, backwards );
        tracksBackward.store_StateCovXY( bwd_size, 1, Vec3<F>( 100.f, 0.f, 1.f ), Vec3<F>( 100.f, 0.f, 1.f ),
                                         backwards ); //  avoid NAN in chechers

        // Store forward tracks
        auto forwards = ( !backwards ) && loop_mask;
        tracksForward.resize( fwd_size + simd::popcount( forwards ) );
        tracksForward.store<TracksTag::nVPHits>( fwd_size, n_hits, forwards );
        auto end_pos = Vec3<F>( x0 + StateParameters::ZEndVelo * tx, y0 + StateParameters::ZEndVelo * ty,
                                StateParameters::ZEndVelo );
        for ( int h = 0; h < max_hits; h++ ) {
          auto       hit_index = track.get<LightTrackTag::hit>( h );
          const auto indices   = select( forwards && h < n_hits, hit_index, 0 );
          auto       hit       = hitproxy.gather( indices );
          tracksForward.store_vp_index( fwd_size, h, hit_index, forwards );
          tracksForward.store_lhcbID(
              fwd_size, h, LHCbID::make( LHCbID::channelIDtype::VP, hit.template get<Velo::VPHitsTag::ChannelId>() ),
              forwards );
        }
        tracksForward.store_StatePosDir( fwd_size, 1, end_pos, dir, forwards );
        tracksForward.store_StateCovXY( fwd_size, 1, Vec3<F>( 100.f, 0.f, 1.f ), Vec3<F>( 100.f, 0.f, 1.f ),
                                        forwards ); //  avoid NAN in chechers

      } // loop all tracks

      // Fit forwards
      auto const fwdtracks = LHCb::Pr::make_zip( tracksForward );
      for ( auto const& track : fwdtracks ) {
        auto    loop_mask     = track.loop_mask();
        I       nhits         = track.nHits();
        Vec3<F> dir           = track.StateDir( 1 );
        auto    vp_index      = track.vp_indices();
        auto    closestToBeam = fitBackward<F, I>( loop_mask, nhits, hits, dir, vp_index );
        closestToBeam.transportTo( closestToBeam.zBeam() );
        tracksForward.store_StatePosDir( track.offset(), 0, closestToBeam.pos(), closestToBeam.dir(), loop_mask );
        tracksForward.store_StateCovXY( track.offset(), 0, closestToBeam.covX(), closestToBeam.covY(), loop_mask );
      }
      // Fit backwards
      auto const bwdtracks = LHCb::Pr::make_zip( tracksBackward );
      for ( auto const& track : bwdtracks ) {
        auto    loop_mask     = track.loop_mask();
        I       nhits         = track.nHits();
        Vec3<F> dir           = track.StateDir( 0 );
        auto    vp_index      = track.vp_indices();
        auto    closestToBeam = fitForward<F, I>( loop_mask, nhits, hits, dir, vp_index );
        closestToBeam.transportTo( closestToBeam.zBeam() );
        tracksBackward.store_StatePosDir( track.offset(), 0, closestToBeam.pos(), closestToBeam.dir(), loop_mask );
        tracksBackward.store_StateCovXY( track.offset(), 0, closestToBeam.covX(), closestToBeam.covY(), loop_mask );
      }
      m_nbClustersCounter += hits.size();
      m_nbTracksCounter += tracks.size();
      return result;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };

  using VeloClusterTrackingSIMD             = ClusterTrackingSIMD<RawBank::VP, SearchMode::Default>;
  using VeloClusterTrackingSIMDFaster       = ClusterTrackingSIMD<RawBank::VP, SearchMode::Fast>;
  using VeloClusterTrackingSIMDFull         = ClusterTrackingSIMD<RawBank::VP, SearchMode::Full>;
  using VeloRetinaClusterTrackingSIMD       = ClusterTrackingSIMD<RawBank::VPRetinaCluster, SearchMode::Default>;
  using VeloRetinaClusterTrackingSIMDFaster = ClusterTrackingSIMD<RawBank::VPRetinaCluster, SearchMode::Fast>;
  DECLARE_COMPONENT_WITH_ID( VeloClusterTrackingSIMD, "VeloClusterTrackingSIMD" )
  DECLARE_COMPONENT_WITH_ID( VeloClusterTrackingSIMDFaster, "VeloClusterTrackingSIMDFaster" )
  DECLARE_COMPONENT_WITH_ID( VeloClusterTrackingSIMDFull, "VeloClusterTrackingSIMDFull" )
  DECLARE_COMPONENT_WITH_ID( VeloRetinaClusterTrackingSIMD, "VeloRetinaClusterTrackingSIMD" )
  DECLARE_COMPONENT_WITH_ID( VeloRetinaClusterTrackingSIMDFaster, "VeloRetinaClusterTrackingSIMDFaster" )
} // namespace LHCb::Pr::Velo
