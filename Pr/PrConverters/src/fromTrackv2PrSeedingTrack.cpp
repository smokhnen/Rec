/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "GaudiAlg/ScalarTransformer.h"
#include "GaudiAlg/Transformer.h"

// LHCb
#include "Event/PrSeedTracks.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/bit_cast.h"
#include "PrKernel/PrFTInfo.h"
#include "PrKernel/PrSciFiHits.h"
#include "SOAExtensions/ZipUtils.h"
// Gaudi
#include "GaudiKernel/StdArrayAsProperty.h"

/**
 * Converter between LHCb::Pr::Fitted::Forward::Tracks ( SoA PoD ) and vector<Track_v2>
 * @author Sascha Stahl
 */
namespace LHCb::Converters::Track::PrSeeding {
  namespace Tag = LHCb::Pr::Seeding::Tag;

  class fromTrackv2PrSeedingTracks
      : public Gaudi::Functional::Transformer<LHCb::Pr::Seeding::Tracks(
            const EventContext& evtCtx, const std::vector<Event::v2::Track>&, const SciFiHits::PrSciFiHits& )> {

  public:
    using base_class = Gaudi::Functional::Transformer<LHCb::Pr::Seeding::Tracks(
        const EventContext& evtCtx, const std::vector<Event::v2::Track>&, const SciFiHits::PrSciFiHits& )>;
    using KeyValue   = typename base_class::KeyValue;

    fromTrackv2PrSeedingTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : base_class( name, pSvcLocator,
                      {KeyValue{"InputTracks", ""}, KeyValue{"InputSciFiHits", PrFTInfo::SciFiHitsLocation}},
                      KeyValue{"OutputTracks", ""} ) {}

    LHCb::Pr::Seeding::Tracks operator()( const EventContext& evtCtx, const std::vector<Event::v2::Track>& inputTracks,
                                          const SciFiHits::PrSciFiHits& fthits ) const override {

      LHCb::Pr::Seeding::Tracks outputTracks{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
      outputTracks.reserve( inputTracks.size() );

      for ( unsigned int t = 0; t < inputTracks.size(); t++ ) {
        const auto& inTrack  = inputTracks[t];
        auto        outTrack = outputTracks.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

        const auto qop = inTrack.firstState().qOverP();
        outTrack.field<Tag::StateQoP>().set( qop );
        outTrack.field<Tag::Chi2PerDoF>().set( static_cast<float>( inTrack.chi2PerDoF() ) );

        // -- copy LHCbIDs
        int i = 0;
        for ( auto id : inTrack.lhcbIDs() ) {
          if ( i == LHCb::Pr::Seeding::Tracks::MaxFTHits ) {
            base_class::error() << "Reached maximum number of hits in LHCb::Pr::Seeding::Tracks "
                                << LHCb::Pr::Seeding::Tracks::MaxFTHits << " No more hits will be added" << endmsg;
            break;
          }
          const auto lhcbid = bit_cast<int, unsigned int>( id.lhcbID() );
          outTrack.field<Tag::lhcbIDs>( i ).set( lhcbid );
          for ( unsigned int ihit = 0; ihit != fthits._IDs.size(); ihit++ ) {
            if ( id.lhcbID() == ( LHCb::LHCbID( fthits.ID( ihit ) ) ).lhcbID() ) {
              outTrack.field<Tag::ft_indices>( i ).set( bit_cast<int, unsigned int>( ihit ) );
              break;
            }
          }
          ++i;
        }
        outTrack.field<Tag::nFTHits>().set( i );

        // -- copy states
        int istate = 0;
        for ( const auto& state : inTrack.states() ) {
          if ( istate == LHCb::Pr::Seeding::Tracks::NumSeedStates ) {
            base_class::error() << "Reached maximum number of states in LHCb::Pr::Seeding::Tracks "
                                << LHCb::Pr::Seeding::Tracks::NumSeedStates << "No more states will be added" << endmsg;
            break;
          }
          auto outState = outTrack.field<Tag::StatePositions>( istate );
          outState.setPosition( state.x(), state.y(), state.z() );
          outState.setDirection( state.tx(), state.ty() );
          ++istate;
        }
        if ( istate < static_cast<int>( LHCb::Pr::Seeding::Tracks::NumSeedStates ) ) {
          base_class::error() << "Missing states in LHCb::Pr::Seeding::Tracks got only" << istate << endmsg;
        }
      }
      m_nbTracksCounter += outputTracks.size();
      return outputTracks;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of converted Tracks"};
  }; // namespace

  DECLARE_COMPONENT( fromTrackv2PrSeedingTracks )

} // namespace LHCb::Converters::Track::PrSeeding
