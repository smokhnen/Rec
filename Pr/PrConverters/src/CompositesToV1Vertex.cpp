/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle_v2.h"
#include "Event/PrFittedForwardTracks.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "SOAExtensions/ZipUtils.h"
#include "SelKernel/TrackZips.h"

#include "GaudiAlg/Transformer.h"

#include <cassert>

namespace {
  /** @brief Find a track based on a set of LHCbIDs and add it to the vertex.
   *
   * Asserts that a track based on the LHCbIDs can be found.
   *
   * @param source_track_ids LHCbIDs used to find the track in converted_tracks
   * @param converted_tracks Container in which to search for the track to be added to vertex
   * @param vertex Vertex to which the found track will be added
   *
   * @todo This was copied from TrackCompactVertexToV1Vertex.cpp, the
   *       duplication should be removed in future.
   */
  void add_track_from_ids_to_vertex( const std::vector<LHCb::LHCbID>& source_track_ids,
                                     const std::vector<LHCb::Track>& converted_tracks, LHCb::RecVertex& vertex ) {
    auto track_in_converted_container =
        std::find_if( std::begin( converted_tracks ), std::end( converted_tracks ), [&]( const auto& t ) {
          return ( ( t.nLHCbIDs() == source_track_ids.size() ) && ( t.containsLhcbIDs( source_track_ids ) ) );
        } );
    assert( track_in_converted_container != std::end( converted_tracks ) );
    vertex.addToTracks( &*track_in_converted_container, 1 ); // TODO 1 -> real weight
  }

  /** @brief Create a RecVertex from a row of an LHCb::v2::Composites container
   *  @todo  Provide a nicer alias for generating the first argument type name.
   */
  LHCb::RecVertex
  convert_vertex( LHCb::v2::Composites::template proxy_type<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous,
                                                            LHCb::v2::Composites const>
                                                           composite,
                  LHCb::Pr::Fitted::Forward::Tracks const& tracks, std::vector<LHCb::Track> const& conv_tracks ) {
    LHCb::RecVertex output{Gaudi::XYZPoint{Sel::Utils::as_arithmetic( composite.x() ),
                                           Sel::Utils::as_arithmetic( composite.y() ),
                                           Sel::Utils::as_arithmetic( composite.z() )}};
    output.setChi2AndDoF( Sel::Utils::as_arithmetic( composite.chi2() ),
                          Sel::Utils::as_arithmetic( composite.nDoF() ) );
    {
      Gaudi::SymMatrix3x3 covMatrix{};
      for ( auto i = 0; i < 3; ++i ) {
        for ( auto j = i; j < 3; ++j ) {
          covMatrix( i, j ) = Sel::Utils::as_arithmetic( composite.posCovElement( i, j ) );
        }
      }
      output.setCovMatrix( covMatrix );
    }
    for ( auto i = 0u; i < composite.numChildren(); ++i ) {
      // Get the zip family identifier for the i-th child of the composite
      Zipping::ZipFamilyNumber child_container_family{Sel::Utils::as_arithmetic( composite.childRelationFamily( i ) )};
      // Check it matches the track container that we were given
      if ( UNLIKELY( child_container_family != tracks.zipIdentifier() ) ) {
        std::ostringstream oss;
        oss << "Mismatch in zip family identifier, needed " << child_container_family
            << " but the provided track container has " << tracks.zipIdentifier();
        throw GaudiException{oss.str(), "CompositesToV1Vertex::{anon}::convert_vertex", StatusCode::FAILURE};
      }
      auto const fittedtracks = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
      auto const childIdx     = Sel::Utils::as_arithmetic( composite.childRelationIndex( i ) );
      auto       ids          = fittedtracks[childIdx].lhcbIDs();
      // The LHCb::Event::v1::Track::containsLhcbIDs method implicitly
      // assumes that the input IDs are sorted; ordering is not guaranteed
      // by the fitted tracks so we must do that here
      using std::begin;
      using std::end;
      std::sort( begin( ids ), end( ids ) );
      add_track_from_ids_to_vertex( ids, conv_tracks, output );
    }

    return output;
  }
} // namespace

namespace LHCb::Converters::Composites {
  /** @brief Convert a LHCb::v2::Composites container to a vector of RecVertex.
   *
   * LHCb::v2::Composites only holds the indices and zip family IDs of child
   * particles, so the containers of children must be explicitly passed in to
   * the converter. Here we assume that there is only a single child container,
   * and that it is of type `tracks_zip_t`.
   *
   * @tparam tracks_zip_t The type of the tracks held by the vertex, this must
   *                      be a zip including LHCb::Pr::Fitted::Forward::Tracks
   *
   * @todo This algorithm shouldn't need to be a template (for the current two
   *       instantiations), as the configuration could do some more traversal
   *       of the data dependency tree and find the
   *       LHCb::Pr::Fitted::Forward::Tracks container that was originally used
   *       to make the zip (tracks_zip_t) that we actually pass in.
   */
  template <typename tracks_zip_t>
  using ToVectorOfRecVertex_base_t = Gaudi::Functional::Transformer<std::vector<LHCb::RecVertex>(
      LHCb::v2::Composites const&, tracks_zip_t const&, std::vector<LHCb::Track> const& )>;
  template <typename tracks_zip_t>
  struct ToVectorOfRecVertex : public ToVectorOfRecVertex_base_t<tracks_zip_t> {
    using base_t   = ToVectorOfRecVertex_base_t<tracks_zip_t>;
    using KeyValue = typename base_t::KeyValue;

    ToVectorOfRecVertex( const std::string& name, ISvcLocator* pSvcLocator )
        : base_t{name,
                 pSvcLocator,
                 {KeyValue{"InputComposites", ""}, KeyValue{"TracksInVertices", ""}, KeyValue{"ConvertedTracks", ""}},
                 KeyValue{"OutputVertices", ""}} {}
    std::vector<LHCb::RecVertex> operator()( LHCb::v2::Composites const& composites, tracks_zip_t const& tracks_zip,
                                             std::vector<LHCb::Track> const& conv_tracks ) const override {
      // Create the output container and reserve capacity
      std::vector<LHCb::RecVertex> converted_vertices;
      converted_vertices.reserve( composites.size() );

      // Extract the fitted tracks from the zip -- see @todo above
      auto const& tracks = tracks_zip.template get<LHCb::Pr::Fitted::Forward::Tracks>();
      for ( auto const composite : composites.scalar() ) {
        converted_vertices.emplace_back( convert_vertex( composite, tracks, conv_tracks ) );
      }
      return converted_vertices;
    }
  };
  DECLARE_COMPONENT_WITH_ID( ToVectorOfRecVertex<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                             "LHCb__Converters__Composites__TracksWithMuonIDToVectorOfRecVertex" )
  DECLARE_COMPONENT_WITH_ID( ToVectorOfRecVertex<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                             "LHCb__Converters__Composites__TracksWithPVsToVectorOfRecVertex" )
} // namespace LHCb::Converters::Composites
