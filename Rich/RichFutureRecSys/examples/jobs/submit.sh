#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Parse script arguments
if [ -z "$1" ] ; then
    echo "First argument is job name"
    exit 1
fi
JobName=$1
if [ -z "$2" ] ; then
    echo "Second argument is number of files"
    exit 1
fi
NumFiles=$2
if [ -z "$3" ] ; then
    echo "Third argument is files per job"
    exit 1
fi
FilesPerJob=$3

# Deduce number of jobs
if [ $((NumFiles%FilesPerJob)) != 0 ] ; then
    echo "Number files ("${NumFiles}") is not an exact multiple of batch size ("${FilesPerJob}")"
    exit 1
fi
NumJobs=$((NumFiles/FilesPerJob))

# Job directory
JobDir=`pwd`"/CondorJobs/"${JobName}
echo "Job Directory "${JobDir}
rm -rf ${JobDir}
mkdir -p ${JobDir}

# Jobs options
mkdir -p ${JobDir}"/jobs"
OptsRoot="${HOME}/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/"
cp ${OptsRoot}"RichFuture.py" ${JobDir}/"jobs/opts.py"
cp ${OptsRoot}"data/PMTs/RealTel40Format/MCLDstFiles.py" ${JobDir}"/jobs/data.py"
export RunOptions="${JobDir}/jobs/opts.py ${JobDir}/jobs/data.py"

# Create run script
JobScript=${JobDir}"/jobs/run.sh"
rm -f ${JobScript}
cat > ${JobScript} << EOFSCRIPT
#!/bin/bash

JobRootDir=\$1
JobBatch=\$2
FilesPerJob=\$3

JobDir=\${JobRootDir}"/jobs/job"\${JobBatch}
echo "Job Dir "\${JobDir}
mkdir -p \${JobDir}
cd \${JobDir}

LogFile=\${JobDir}"/job.log"

export CONDOR_FILE_BATCH=\${JobBatch}
export CONDOR_FILES_PER_BATCH=\${FilesPerJob}

gaudirun.py -T \${RunOptions} 2>&1 | cat > \${LogFile}

exit 0
EOFSCRIPT
chmod +x ${JobScript}

# Create condor file
CondorScript=${JobDir}"/jobs/condor.job"
rm -f ${CondorScript}
cat > ${CondorScript} << EOFCONDOR

# Condor environment
Universe                = vanilla
getenv                  = true
copy_to_spool           = true
should_transfer_files   = YES
when_to_transfer_output = ON_EXIT_OR_EVICT
environment = CONDOR_ID=\$(Cluster).\$(Process)
JobBatchName            = ${JobName}

# Requirements
#Requirements = ( OSTYPE == "CC7" && ( POOL == "GEN_FARM" || ( POOL == "GENERAL" && Machine >= "pcjn.hep.phy.cam.ac.uk" && ( Name == strcat("slot1@",Machine) || ( Name == strcat("slot3@",Machine) && Machine >= "pclq.hep.phy.cam.ac.uk" ) ) ) ) )
#Requirements = ( OSTYPE == "CC7" && ( POOL == "GEN_FARM" || POOL == "GENERAL" ) )
Requirements = ( OSTYPE == "CC7" && ( POOL == "GEN_FARM" || ( POOL == "GENERAL" && Machine >= "pclq.hep.phy.cam.ac.uk" ) ) )
#Requirements = ( OSTYPE == "CC7" && POOL == "GEN_FARM" )

# Rank hosts according to floating point speed
Rank = kflops

# Memory requirement (in MB)
request_memory = 1500

# Use suspendable slots when available
# +IsSuspendableJob = True

# Condor Output
output = ${HOME}/CondorLogs/out.\$(Process)
error  = ${HOME}/CondorLogs/err.\$(Process)
Log    = ${HOME}/CondorLogs/log.\$(Process)

# =============================================================================
# Submit the job script
# =============================================================================

# Arguments are <name> <job-number> <number files per job>
Executable       = ${JobScript}
Arguments        = ${JobDir} \$(Process) ${FilesPerJob}

# Submit the number of jobs required
Queue ${NumJobs}

EOFCONDOR

# Finally, submit the job
condor_submit ${CondorScript}

exit 0
