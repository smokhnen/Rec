/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecAlgBase.cpp
 *
 *  Implementation file for RICH reconstruction algorithm base class : RichRecAlgBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2003-05-10
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureRecBase/RichRecAlgBase.h"

// ============================================================================
// Force creation of templated class
#include "RichRecBase.icpp"
template class Rich::Future::Rec::CommonBase<Rich::Future::AlgBase<FixTESPath<Gaudi::Algorithm>>>;
template class Rich::Future::Rec::CommonBase<Rich::Future::AlgBase<Gaudi::Algorithm>>;
template class Rich::Future::Rec::CommonBase<Rich::Future::AlgBase<GaudiAlgorithm>>;
// ============================================================================
