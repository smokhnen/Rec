###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
gaudi_subdir(RichFutureRecInterfaces)

gaudi_depends_on_subdirs(Event/RecEvent
                         Rich/RichUtils)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_dictionary(RichFutureRecInterfaces
                     dict/RichFutureRecInterfacesDict.h
                     dict/RichFutureRecInterfacesDict.xml
                     LINK_LIBRARIES RecEvent RichUtils
                     OPTIONS "-DBOOST_DISABLE_ASSERTS")

gaudi_install_headers(RichFutureRecInterfaces)
