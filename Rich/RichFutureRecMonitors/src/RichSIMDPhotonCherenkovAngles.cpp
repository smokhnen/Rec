/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// RichDet
#include "RichDet/DeRichSystem.h"

// STD
#include <algorithm>
#include <mutex>
#include <sstream>

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final : public Consumer<void( const LHCb::Track::Selection&,             //
                                                                const Summary::Track::Vector&,             //
                                                                const Relations::PhotonToParents::Vector&, //
                                                                const LHCb::RichTrackSegment::Vector&,     //
                                                                const CherenkovAngles::Vector&,            //
                                                                const SIMDCherenkovPhoton::Vector& ),
                                                          Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Selection&             tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Get the per PD resolution histogram ID
    inline std::string pdResPlotID( const LHCb::RichSmartID hpd ) const {
      const Rich::DAQ::PDIdentifier hid( hpd );
      std::ostringstream            id;
      id << "PDs/pd-" << hid.number();
      return id.str();
    }

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    // CK theta histograms
    RadiatorArray<AIDA::IHistogram1D*>             h_thetaRec                = {{}};
    RadiatorArray<AIDA::IHistogram1D*>             h_phiRec                  = {{}};
    RadiatorArray<AIDA::IHistogram1D*>             h_ckResAll                = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>> h_ckResAllPerPanel        = {{}};
    RadiatorArray<AIDA::IHistogram1D*>             h_ckResAllBetaCut         = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>> h_ckResAllPerPanelBetaCut = {{}};

  private:
    // properties and tools

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.005f, 0.0025f}};

    /// Enable the per PD resolution plots, for the given radiators
    Gaudi::Property<RadiatorArray<bool>> m_pdResPlots{this, "EnablePerPDPlots", {false, false, false}};

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // load Rich System det. elem.
  const auto deRichSys = acquire<DeRichSystem>( detSvc(), DeRichLocations::RichSystem );

  // List of HPDs
  const auto& hpds = deRichSys->allPDRichSmartIDs();

  // Loop over radiators
  for ( const auto rad : activeRadiators() ) {

    // Which RICH ?
    const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );

    // beta cut string
    std::string betaS = "beta";
    if ( m_minBeta[rad] > 0.0 ) { betaS = std::to_string( m_minBeta[rad] ) + "<" + betaS; }
    if ( m_maxBeta[rad] < 1.0 ) { betaS = betaS + "<" + std::to_string( m_maxBeta[rad] ); }

    // inclusive plots
    ok &= saveAndCheck( h_thetaRec[rad], richHisto1D( HID( "thetaRec", rad ),                          //
                                                      "Reconstructed CKTheta - All photons",           //
                                                      m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                                      "Cherenkov Theta / rad" ) );
    ok &= saveAndCheck( h_phiRec[rad], richHisto1D( HID( "phiRec", rad ),                   //
                                                    "Reconstructed CKPhi - All photons",    //
                                                    0.0, 2.0 * Gaudi::Units::pi, nBins1D(), //
                                                    "Cherenkov Phi / rad" ) );
    ok &= saveAndCheck( h_ckResAll[rad], richHisto1D( HID( "ckResAll", rad ),                           //
                                                      "Rec-Exp CKTheta - All photons",                  //
                                                      -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                                      "delta(Cherenkov theta) / rad" ) );
    // with beta cut
    ok &= saveAndCheck( h_ckResAllBetaCut[rad],                                        //
                        richHisto1D( HID( "ckResAllBetaCut", rad ),                    //
                                     "Rec-Exp CKTheta - All photons - " + betaS,       //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov theta) / rad" ) );

    // loop over detector sides
    for ( const auto side : Rich::sides() ) {
      ok &= saveAndCheck( h_ckResAllPerPanel[rad][side],                                 //
                          richHisto1D( HID( "ckResAllPerPanel", side, rad ),             //
                                       "Rec-Exp CKTheta - All photons",                  //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
      // beta cut
      ok &= saveAndCheck( h_ckResAllPerPanelBetaCut[rad][side],
                          richHisto1D( HID( "ckResAllPerPanelBetaCut", side, rad ),      //
                                       "Rec-Exp CKTheta - All photons - " + betaS,       //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
    }

    // Enable per PD resolution plots ?
    if ( UNLIKELY( m_pdResPlots[rad] ) ) {
      // Loop over PDs
      for ( const auto PD : hpds ) {
        if ( PD.rich() != rich ) continue;
        // construct title
        const auto         copyN = deRichSys->copyNumber( PD );
        std::ostringstream title;
        title << "Rec-Exp Cktheta - All photons - PD#" << copyN << " " << PD << " - " << betaS;
        // construct ID
        const HID hID( pdResPlotID( PD ), rad );
        // book the plot
        ok &= ( nullptr != richHisto1D( hID, title.str(), -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),
                                        "delta(Cherenkov theta) / rad" ) );
      }
    }

  } // active rad loop

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const LHCb::Track::Selection&             tracks,        //
                                            const Summary::Track::Vector&             sumTracks,     //
                                            const Relations::PhotonToParents::Vector& photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&     segments,      //
                                            const CherenkovAngles::Vector&            expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&        photons ) const {

  // the lock
  std::lock_guard lock( m_updateLock );

  // loop over the track containers
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // Is this track selected ?
    if ( !m_tkSel.get()->accept( *tk ) ) continue;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) continue;

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto pid = Rich::Pion;

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      const auto betaC = ( beta >= m_minBeta[rad] && beta <= m_maxBeta[rad] );

      // expected CK theta
      const auto thetaExp = expCKangles[pid];

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( phot.validityMask()[i] ) {

          // reconstructed theta
          const auto thetaRec = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec = phot.CherenkovPhi()[i];
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // SmartID
          const auto id = phot.smartID()[i];

          // Detctor side
          const auto side = id.panel();

          // fill some plots
          fillHisto( h_thetaRec[rad], thetaRec );
          fillHisto( h_phiRec[rad], phiRec );
          fillHisto( h_ckResAll[rad], deltaTheta );
          fillHisto( h_ckResAllPerPanel[rad][side], deltaTheta );
          if ( betaC ) {
            fillHisto( h_ckResAllBetaCut[rad], deltaTheta );
            fillHisto( h_ckResAllPerPanelBetaCut[rad][side], deltaTheta );
          }

          // Per PD plots
          if ( UNLIKELY( m_pdResPlots[rad] && betaC ) ) {
            const auto pdID = id.pdID();
            auto       h    = richHisto1D( HID( pdResPlotID( pdID ), rad ) );
            if ( h ) { fillHisto( h, deltaTheta ); }
          }

        } // valid scalars
      }   // SIMD loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
