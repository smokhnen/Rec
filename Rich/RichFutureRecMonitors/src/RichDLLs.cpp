/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event model
#include "Event/RichPID.h"

// STL
#include <mutex>

namespace Rich::Future::Rec::Moni {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class DLLs RichDLLs.h
   *
   *  Basic plotting of RICH DLL values
   *
   *  @author Chris Jones
   *  @date   2020-02-20
   */

  class DLLs final : public Consumer<void( const LHCb::RichPIDs& ), Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DLLs( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // data inputs
                    KeyValue{"RichPIDsLocation", LHCb::RichPIDLocation::Default} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichPIDs& pids ) const override {

      // local count buffers
      auto withR1   = m_withR1.buffer();
      auto withR2   = m_withR2.buffer();
      auto withR1R2 = m_withR1R2.buffer();

      // the lock
      std::lock_guard lock( m_updateLock );

      // count PIDs
      m_nPIDs += pids.size();

      // loop over PID objects
      for ( const auto pid : pids ) {
        // Fill plots for particle types
        for ( const auto id : activeParticles() ) {
          if ( h_dlls[id] ) { fillHisto( h_dlls[id], pid->particleDeltaLL( id ) ); }
        }
        // fill counts
        withR1 += ( pid->usedRich1Gas() && !pid->usedRich2Gas() );
        withR2 += ( pid->usedRich2Gas() && !pid->usedRich1Gas() );
        withR1R2 += ( pid->usedRich2Gas() && pid->usedRich1Gas() );
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      for ( const auto id : activeParticles() ) {
        ok &= saveAndCheck( h_dlls[id], richHisto1D( HID( "dll", id ), "DLL", -50, 50, nBins1D() ) );
      }
      return StatusCode{ok};
    }

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;

    /// Histograms for each DLL value
    ParticleArray<AIDA::IHistogram1D*> h_dlls = {{}};

    /// PID count
    mutable Gaudi::Accumulators::StatCounter<> m_nPIDs{this, "# PIDs"};

    /// Only using RICH1
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1{this, "Used RICH1 only"};

    /// Only using RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR2{this, "Used RICH2 only"};

    /// Using both RICH1 and RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1R2{this, "Used RICH1 and RICH2"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DLLs )

} // namespace Rich::Future::Rec::Moni
