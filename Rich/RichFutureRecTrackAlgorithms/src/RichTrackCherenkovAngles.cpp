/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>
#include <cmath>
#include <iomanip>

// GaudiAlg
#include "GaudiAlg/Transformer.h"
// gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class TrackCherenkovAnglesBase RichTrackCherenkovAngles.h
   *
   *  Computes the expected Cherenkov angles for the given track
   *  segments and photon spectra data.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackCherenkovAnglesBase : public AlgBase<> {

  public:
    /// Standard constructor
    using AlgBase<>::AlgBase;

  protected:
    /// Algorithm operator
    CherenkovAngles::Vector run( const LHCb::RichTrackSegment::Vector& segments,  //
                                 const PhotonSpectra::Vector&          tkSpectra, //
                                 const PhotonYields::Vector&           tkYields,  //
                                 const Utils::TabulatedRefIndex&       refIndex ) const {

      // make the data to return
      CherenkovAngles::Vector anglesV;
      anglesV.reserve( segments.size() );

      // iterate over segments and spectra together.
      for ( auto&& [segment, spectra, yields] : Ranges::ConstZip( segments, tkSpectra, tkYields ) ) {
        // make a new object for the cherenkov angles
        anglesV.emplace_back();
        auto& angles = anglesV.back();

        //_ri_verbo << "Tk" << endmsg;

        // Loop over (real) PID types (Below Threshold excluded).
        for ( const auto id : activeParticlesNoBT() ) {

          // the angle
          CherenkovAngles::Type angle = 0;

          // Is this hypo above threshold ?
          if ( yields[id] > 0 ) {

            //_ri_verbo << std::setprecision(9) << id << " yield = " << yields[id] << endmsg;

            // compute track beta
            const auto beta = richPartProps()->beta( segment.bestMomentumMag(), id );
            if ( beta > 0 ) {

              // loop over energy bins
              for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {
                const auto refIn = refIndex.refractiveIndex( segment.radIntersections(), spectra.binEnergy( iEnBin ) );
                const auto temp  = beta * refIn;
                //_ri_verbo << std::setprecision(9)
                //          << " bin " << iEnBin << " " << beta << " " << refIn << endmsg;
                if ( temp > 1 ) {
                  const auto f  = Rich::Maths::fast_acos( 1.0 / temp );
                  const auto en = ( spectra.energyDist( id ) )[iEnBin];
                  //_ri_verbo << std::setprecision(9) << "     " << " " << f << " " << en << endmsg;
                  angle += ( en * f );
                }
              }
              // normalise the angle
              angle /= yields[id];
            }
          }

          // save the final angle for this hypo
          //_ri_verbo << std::setprecision(9) << id << " CK theta " << angle << endmsg;
          angles.setData( id, angle );
        }
      }

      // return the new data
      return anglesV;
    }
  };

  //=============================================================================

  /** @class TrackEmittedCherenkovAngles RichTrackCherenkovAngles.h
   *
   *  Functional implementation using emitted photon spectra.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackEmittedCherenkovAngles final
      : public Transformer<CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector&, //
                                                    const PhotonSpectra::Vector&,          //
                                                    const PhotonYields::Vector&,           //
                                                    const Utils::TabulatedRefIndex& ),
                           LHCb::DetDesc::usesBaseAndConditions<TrackCherenkovAnglesBase, Utils::TabulatedRefIndex>> {
  public:
    /// Constructor
    TrackEmittedCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"EmittedPhotonSpectraLocation", PhotonSpectraLocation::Emitted},
                        KeyValue{"EmittedPhotonYieldLocation", PhotonYieldsLocation::Emitted},
                        // conditions input
                        KeyValue{"TabulatedRefIndex", name + "-" + Utils::TabulatedRefIndex::DefaultConditionKey}},
                       // output data
                       {KeyValue{"EmittedCherenkovAnglesLocation", CherenkovAnglesLocation::Emitted}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }
    /// Algorithm execution via transform
    CherenkovAngles::Vector operator()( const LHCb::RichTrackSegment::Vector& segments,  //
                                        const PhotonSpectra::Vector&          tkSpectra, //
                                        const PhotonYields::Vector&           tkYields,  //
                                        const Utils::TabulatedRefIndex&       refIndex ) const override {
      return run( segments, tkSpectra, tkYields, refIndex );
    }
    /// Initialize
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] { Utils::TabulatedRefIndex::addConditionDerivation( this ); } );
    }
  };

  //=============================================================================

  /** @class TrackSignalCherenkovAngles RichTrackCherenkovAngles.h
   *
   *  Functional implementation using signal photon spectra.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackSignalCherenkovAngles final
      : public Transformer<CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector&, //
                                                    const PhotonSpectra::Vector&,          //
                                                    const PhotonYields::Vector&,           //
                                                    const Utils::TabulatedRefIndex& ),
                           LHCb::DetDesc::usesBaseAndConditions<TrackCherenkovAnglesBase, Utils::TabulatedRefIndex>> {
  public:
    /// Constructor
    TrackSignalCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"SignalPhotonSpectraLocation", PhotonSpectraLocation::Signal},
                        KeyValue{"SignalPhotonYieldLocation", PhotonYieldsLocation::Signal},
                        // conditions input
                        KeyValue{"TabulatedRefIndex", name + "-" + Utils::TabulatedRefIndex::DefaultConditionKey}},
                       // output data
                       {KeyValue{"SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }
    /// Algorithm execution via transform
    CherenkovAngles::Vector operator()( const LHCb::RichTrackSegment::Vector& segments,  //
                                        const PhotonSpectra::Vector&          tkSpectra, //
                                        const PhotonYields::Vector&           tkYields,  //
                                        const Utils::TabulatedRefIndex&       refIndex ) const override {
      return run( segments, tkSpectra, tkYields, refIndex );
    }
    /// Initialize
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] { Utils::TabulatedRefIndex::addConditionDerivation( this ); } );
    }
  };

  //=============================================================================

  // Declaration of the Algorithm Factories
  DECLARE_COMPONENT( TrackEmittedCherenkovAngles )
  DECLARE_COMPONENT( TrackSignalCherenkovAngles )

  //=============================================================================

} // namespace Rich::Future::Rec
