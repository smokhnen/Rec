/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <tuple>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureKernel/RichAlgBase.h"

// Event model
#include "Event/Track.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace {
    /// Output data type
    using OutData = std::tuple<LHCb::Track::Selection,  // Long
                               LHCb::Track::Selection,  // Downstream
                               LHCb::Track::Selection,  // Upstream
                               LHCb::Track::Selection>; // Seeds (SciFi)
  }                                                     // namespace

  /** @class TrackFilter RichTrackFilter.h
   *
   *  (Temporary) algorithm that takes an input track location and splits
   *  it in shared containers by type.
   *
   *  Stop gap solution until a more general LoKiFunctor implementation is available.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackFilter final : public MultiTransformer<OutData( const LHCb::Tracks& ), Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    TrackFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // input
                            {KeyValue{"InTracksLocation", LHCb::TrackLocation::Default}},
                            // outputs
                            {KeyValue{"OutLongTracksLocation", LHCb::TrackLocation::Default + "RichLong"},
                             KeyValue{"OutDownTracksLocation", LHCb::TrackLocation::Default + "RichDown"},
                             KeyValue{"OutUpTracksLocation", LHCb::TrackLocation::Default + "RichUp"},
                             KeyValue{"OutSeedTracksLocation", LHCb::TrackLocation::Default + "RichSeed"}} ) {
      // Debug messages
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::Tracks& tracks ) const override {

      OutData filteredTks;
      auto& [longTks, downTks, upTks, seedTks] = filteredTks;

      _ri_debug << "Found " << tracks.size() << " tracks" << endmsg;

      for ( const auto* tk : tracks ) {
        _ri_verbo << " -> type " << tk->type() << endmsg;
        switch ( tk->type() ) {
        case LHCb::Track::Types::Long:
          longTks.insert( tk );
          break;
        case LHCb::Track::Types::Downstream:
          downTks.insert( tk );
          break;
        case LHCb::Track::Types::Upstream:
          upTks.insert( tk );
          break;
        case LHCb::Track::Types::Ttrack:
          seedTks.insert( tk );
          break;
        default:
          break;
        }
      }

      _ri_debug << "Selected "                       //
                << longTks.size() << " Long, "       //
                << downTks.size() << " Downstream, " //
                << upTks.size() << " Upstream and "  //
                << seedTks.size() << " Seed tracks" << endmsg;

      return filteredTks;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackFilter )

} // namespace Rich::Future::Rec

//=============================================================================
