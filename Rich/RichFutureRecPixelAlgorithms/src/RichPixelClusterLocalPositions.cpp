/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichPixelCluster.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// DetDesc
#include "DetDesc/ConditionAccessorHolder.h"

namespace Rich::Future::Rec {

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelClusterLocalPositions RichPixelClusterLocalPositions.h
   *
   *  Computes the global space points for the given pixel clusters.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class PixelClusterLocalPositions final
      : public Transformer<SpacePointVector( const SpacePointVector&, //
                                             const Rich::Utils::RichSmartIDs& ),
                           LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    PixelClusterLocalPositions( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       {KeyValue{"RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal},
                        // input conditions data
                        KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}},
                       // output data
                       {KeyValue{"RichPixelLocalPositionsLocation", SpacePointLocation::PixelsLocal}} ) {
      // setProperty( "OutputLevel", MSG::DEBUG );
    }

    /// Initialize
    StatusCode initialize() override {
      // bare class initialise then conditions
      return Transformer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
      } );
    }

  public:
    /// Operator for each space point
    SpacePointVector operator()( const SpacePointVector&          gPoints,
                                 const Rich::Utils::RichSmartIDs& smartIDsHelper ) const override {

      // the container to return
      SpacePointVector lPoints;
      lPoints.reserve( gPoints.size() );

      for ( const auto& gPos : gPoints ) {
        lPoints.emplace_back( smartIDsHelper.globalToPDPanel( gPos ) );
        _ri_debug << gPos << " -> " << lPoints.back() << endmsg;
      }

      // return the final space points
      return lPoints;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PixelClusterLocalPositions )

} // namespace Rich::Future::Rec
