###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 =============================================================
 Class to configure MuonID tools and algos. To import do
 'from MuonID import ConfiguredMuonIDs'
 =============================================================
"""

from builtins import str
from builtins import object
import os, sys, copy

from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions

from Configurables import GaudiSequencer
from TrackFitter.ConfiguredFitters import *
from Configurables import CommonMuonTool, DLLMuonTool, MakeMuonTool, MVATool, MuonIDAlgLite, PrepareMuonHits


class ConfiguredMuonIDs(object):
    def __init__(self, data="DC06", version="def", specialData=[],
                 debug=False):
        """
    initialization for the class. Use as input data type (DC06,MC08,etc) and version of it if necessary.
    """
        self.debug = debug
        log.debug("# INITIALIZING")

        self.specialData = specialData
        log.debug("# SPECIAL DATA = %s" % self.specialData)

        self.data = data
        log.debug("# DATA TYPE = %s" % self.data)

        ## from datatype and version look for module with data. Store extra modules in case desired does not exist
        mod = [data + "_" + version, data + "_def", "2011_def"]
        mod = ["Muon_" + x for x in mod]

        log.debug("# \tmods -> %s" % mod)

        ## check if modules exist and load them
        locals = {}
        try:
            exec ("from MuonID import " + mod[0] + " as info", {}, locals)
        except:
            if version != "def":
                log.warning(
                    "ConfiguredMuonIDs: Not available info for DATA=%s,VERSION=%s. Loading default"
                    % (data, version))
            else:
                log.info(
                    "ConfiguredMuonIDs: Default seems not available for DATA=%s. Loading older default %s"
                    % (data, mod[2]))
            try:
                exec ("from MuonID import " + mod[1] + " as info", {}, locals)
            except:
                exec ("from MuonID import " + mod[2] + " as info", {}, locals)

        info = locals["info"]

        GaudiKernel.ProcessJobOptions.PrintOn(force=True)
        log.info(
            "ConfiguredMuonIDs: Loaded configuration info %s" % info.FILENAME)
        GaudiKernel.ProcessJobOptions.PrintOff()

        ## set final module with info to be loaded
        self.info = info
        self.info.DEBUG = debug
        self.initializeAll = True

    def configureMuonIDAlgLite(self, muonid):
        """
    general configuration of MuonIDAlgLite.
    """

        log.debug("# CONFIGURING MUONIDALGLITE")

        ## check if input is already an instance or this must be created
        if isinstance(muonid, MuonIDAlgLite): mymuid = muonid
        else: mymuid = MuonIDAlgLite(str(muonid))

        if "DLL_flag" in dir(self.info): mymuid.DLL_flag = self.info.DLL_flag

        return mymuid

    def configureIsMuonTool(self, ismutool):
        log.debug("# CONFIGURING ISMUON TOOL")
        if isinstance(ismutool, CommonMuonTool): myISMUT = ismutool
        else: myISMUT = CommonMuonTool(str(ismutool))

        return ismutool

    def configureMVATool(self, mvatool):
        log.debug("# CONFIGURING MVA TOOL")
        if isinstance(mvatool, MVATool): myMVAT = mvatool
        else: myMVAT = MVATool(str(mvatool))

        return mvatool

    def configureMakeMuonTool(self, mmtool):
        log.debug("# CONFIGURING MAKEMUON TOOL")
        if isinstance(mmtool, MakeMuonTool): myMMT = mmtool
        else: myMMT = MakeMuonTool(str(mmtool))

        return mmtool

    def configureDLLMuonTool(self, dlltool):
        log.debug("# CONFIGURING DLLMUON TOOL")
        if isinstance(dlltool, DLLMuonTool): myDLL = dlltool
        else: myDLL = DLLMuonTool(str(dlltool))

        if "OverrideDB" in dir(self.info):
            myDLL.OverrideDB = self.info.OverrideDB
        if "MomentumCuts" in dir(self.info):
            myDLL.MomentumCuts = self.info.MomentumCuts
        if "FOIfactor" in dir(self.info): myDLL.FOIfactor = self.info.FOIfactor
        if "PreSelMomentum" in dir(self.info):
            myDLL.PreSelMomentum = self.info.PreSelMomentum

        if "MupBinsR1" in dir(self.info): myDLL.MupBinsR1 = self.info.MupBinsR1
        if "MupBinsR2" in dir(self.info): myDLL.MupBinsR2 = self.info.MupBinsR2
        if "MupBinsR3" in dir(self.info): myDLL.MupBinsR3 = self.info.MupBinsR3
        if "MupBinsR4" in dir(self.info): myDLL.MupBinsR4 = self.info.MupBinsR4

        if "NonMupBinsR1" in dir(self.info):
            myDLL.NonMupBinsR1 = self.info.NonMupBinsR1
        if "NonMupBinsR2" in dir(self.info):
            myDLL.NonMupBinsR2 = self.info.NonMupBinsR2
        if "NonMupBinsR3" in dir(self.info):
            myDLL.NonMupBinsR3 = self.info.NonMupBinsR3
        if "NonMupBinsR4" in dir(self.info):
            myDLL.NonMupBinsR4 = self.info.NonMupBinsR4

        ## Configure Landau parameters for Non muons (DLLflag = 4)
        if "NonMuLandauParameterR1" in dir(self.info):
            myDLL.NonMuLandauParameterR1 = self.info.NonMuLandauParameterR1
        if "NonMuLandauParameterR2" in dir(self.info):
            myDLL.NonMuLandauParameterR2 = self.info.NonMuLandauParameterR2
        if "NonMuLandauParameterR3" in dir(self.info):
            myDLL.NonMuLandauParameterR3 = self.info.NonMuLandauParameterR3
        if "NonMuLandauParameterR4" in dir(self.info):
            myDLL.NonMuLandauParameterR4 = self.info.NonMuLandauParameterR4

        ## Configure hyperbolic tangent tanh(dist2) parameters
        if "tanhScaleFactorsMuonR1" in dir(self.info):
            myDLL.tanhScaleFactorsMuonR1 = self.info.tanhScaleFactorsMuonR1
        if "tanhScaleFactorsMuonR2" in dir(self.info):
            myDLL.tanhScaleFactorsMuonR2 = self.info.tanhScaleFactorsMuonR2
        if "tanhScaleFactorsMuonR3" in dir(self.info):
            myDLL.tanhScaleFactorsMuonR3 = self.info.tanhScaleFactorsMuonR3
        if "tanhScaleFactorsMuonR4" in dir(self.info):
            myDLL.tanhScaleFactorsMuonR4 = self.info.tanhScaleFactorsMuonR4

        if "tanhScaleFactorsNonMuonR1" in dir(self.info):
            myDLL.tanhScaleFactorsNonMuonR1 = self.info.tanhScaleFactorsNonMuonR1
        if "tanhScaleFactorsNonMuonR2" in dir(self.info):
            myDLL.tanhScaleFactorsNonMuonR2 = self.info.tanhScaleFactorsNonMuonR2
        if "tanhScaleFactorsNonMuonR3" in dir(self.info):
            myDLL.tanhScaleFactorsNonMuonR3 = self.info.tanhScaleFactorsNonMuonR3
        if "tanhScaleFactorsNonMuonR4" in dir(self.info):
            myDLL.tanhScaleFactorsNonMuonR4 = self.info.tanhScaleFactorsNonMuonR4

        ## Signal muons
        if "tanhCumulHistoMuonR1_1" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_1 = self.info.tanhCumulHistoMuonR1_1
        if "tanhCumulHistoMuonR1_2" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_2 = self.info.tanhCumulHistoMuonR1_2
        if "tanhCumulHistoMuonR1_3" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_3 = self.info.tanhCumulHistoMuonR1_3
        if "tanhCumulHistoMuonR1_4" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_4 = self.info.tanhCumulHistoMuonR1_4
        if "tanhCumulHistoMuonR1_5" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_5 = self.info.tanhCumulHistoMuonR1_5
        if "tanhCumulHistoMuonR1_6" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_6 = self.info.tanhCumulHistoMuonR1_6
        if "tanhCumulHistoMuonR1_7" in dir(self.info):
            myDLL.tanhCumulHistoMuonR1_7 = self.info.tanhCumulHistoMuonR1_7

        if "tanhCumulHistoMuonR2_1" in dir(self.info):
            myDLL.tanhCumulHistoMuonR2_1 = self.info.tanhCumulHistoMuonR2_1
        if "tanhCumulHistoMuonR2_2" in dir(self.info):
            myDLL.tanhCumulHistoMuonR2_2 = self.info.tanhCumulHistoMuonR2_2
        if "tanhCumulHistoMuonR2_3" in dir(self.info):
            myDLL.tanhCumulHistoMuonR2_3 = self.info.tanhCumulHistoMuonR2_3
        if "tanhCumulHistoMuonR2_4" in dir(self.info):
            myDLL.tanhCumulHistoMuonR2_4 = self.info.tanhCumulHistoMuonR2_4
        if "tanhCumulHistoMuonR2_5" in dir(self.info):
            myDLL.tanhCumulHistoMuonR2_5 = self.info.tanhCumulHistoMuonR2_5

        if "tanhCumulHistoMuonR3_1" in dir(self.info):
            myDLL.tanhCumulHistoMuonR3_1 = self.info.tanhCumulHistoMuonR3_1
        if "tanhCumulHistoMuonR3_2" in dir(self.info):
            myDLL.tanhCumulHistoMuonR3_2 = self.info.tanhCumulHistoMuonR3_2
        if "tanhCumulHistoMuonR3_3" in dir(self.info):
            myDLL.tanhCumulHistoMuonR3_3 = self.info.tanhCumulHistoMuonR3_3
        if "tanhCumulHistoMuonR3_4" in dir(self.info):
            myDLL.tanhCumulHistoMuonR3_4 = self.info.tanhCumulHistoMuonR3_4
        if "tanhCumulHistoMuonR3_5" in dir(self.info):
            myDLL.tanhCumulHistoMuonR3_5 = self.info.tanhCumulHistoMuonR3_5

        if "tanhCumulHistoMuonR4_1" in dir(self.info):
            myDLL.tanhCumulHistoMuonR4_1 = self.info.tanhCumulHistoMuonR4_1
        if "tanhCumulHistoMuonR4_2" in dir(self.info):
            myDLL.tanhCumulHistoMuonR4_2 = self.info.tanhCumulHistoMuonR4_2
        if "tanhCumulHistoMuonR4_3" in dir(self.info):
            myDLL.tanhCumulHistoMuonR4_3 = self.info.tanhCumulHistoMuonR4_3
        if "tanhCumulHistoMuonR4_4" in dir(self.info):
            myDLL.tanhCumulHistoMuonR4_4 = self.info.tanhCumulHistoMuonR4_4
        if "tanhCumulHistoMuonR4_5" in dir(self.info):
            myDLL.tanhCumulHistoMuonR4_5 = self.info.tanhCumulHistoMuonR4_5

        ## Bakground Comb muons: Also per regions AND momentum bins. Not suitable for low statistics
        if "tanhCumulHistoNonMuonR1_1" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_1 = self.info.tanhCumulHistoNonMuonR1_1
        if "tanhCumulHistoNonMuonR1_2" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_2 = self.info.tanhCumulHistoNonMuonR1_2
        if "tanhCumulHistoNonMuonR1_3" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_3 = self.info.tanhCumulHistoNonMuonR1_3
        if "tanhCumulHistoNonMuonR1_4" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_4 = self.info.tanhCumulHistoNonMuonR1_4
        if "tanhCumulHistoNonMuonR1_5" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_5 = self.info.tanhCumulHistoNonMuonR1_5
        if "tanhCumulHistoNonMuonR1_6" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_6 = self.info.tanhCumulHistoNonMuonR1_6
        if "tanhCumulHistoNonMuonR1_7" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR1_7 = self.info.tanhCumulHistoNonMuonR1_7

        if "tanhCumulHistoNonMuonR2_1" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR2_1 = self.info.tanhCumulHistoNonMuonR2_1
        if "tanhCumulHistoNonMuonR2_2" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR2_2 = self.info.tanhCumulHistoNonMuonR2_2
        if "tanhCumulHistoNonMuonR2_3" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR2_3 = self.info.tanhCumulHistoNonMuonR2_3
        if "tanhCumulHistoNonMuonR2_4" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR2_4 = self.info.tanhCumulHistoNonMuonR2_4
        if "tanhCumulHistoNonMuonR2_5" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR2_5 = self.info.tanhCumulHistoNonMuonR2_5

        if "tanhCumulHistoNonMuonR3_1" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR3_1 = self.info.tanhCumulHistoNonMuonR3_1
        if "tanhCumulHistoNonMuonR3_2" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR3_2 = self.info.tanhCumulHistoNonMuonR3_2
        if "tanhCumulHistoNonMuonR3_3" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR3_3 = self.info.tanhCumulHistoNonMuonR3_3
        if "tanhCumulHistoNonMuonR3_4" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR3_4 = self.info.tanhCumulHistoNonMuonR3_4
        if "tanhCumulHistoNonMuonR3_5" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR3_5 = self.info.tanhCumulHistoNonMuonR3_5

        if "tanhCumulHistoNonMuonR4_1" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR4_1 = self.info.tanhCumulHistoNonMuonR4_1
        if "tanhCumulHistoNonMuonR4_2" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR4_2 = self.info.tanhCumulHistoNonMuonR4_2
        if "tanhCumulHistoNonMuonR4_3" in dir(self.info):
            myDLL.tanhCumulHistoNonMuonR4_3 = self.info.tanhCumulHistoNonMuonR4_3

        return myDLL

    def getMuonIDSeq(self):
        """
    general method for MuonIDAlgLite configuration.
    Creates MuonIDAlgLite instance and configures it.
    Finally puts it in gaudi sequencer.
    """

        log.debug("# APPLYING GENERAL MUONID CONFIGURATION")
        ## create output gaudi sequencer
        myg = GaudiSequencer("MuonIDSeq")

        ## create and configure MuonIDAlgLite instance
        muid = MuonIDAlgLite()
        self.configureMuonIDAlgLite(muid)
        ismutool = CommonMuonTool()
        self.configureIsMuonTool(ismutool)
        dlltool = DLLMuonTool()
        self.configureDLLMuonTool(dlltool)
        mmtool = MakeMuonTool()
        self.configureMakeMuonTool(mmtool)
        mvatool = MVATool()
        self.configureMVATool(mvatool)

        muid.addTool(ismutool)
        muid.addTool(dlltool)
        muid.addTool(mmtool)
        muid.addTool(mvatool)

        ## add to gaudi sequencer and return
        myg.Members.append(PrepareMuonHits())
        myg.Members.append(muid)
        log.debug("# CONFIGURATION OF MUONID SEQUENCE FINALIZED")
        return myg
