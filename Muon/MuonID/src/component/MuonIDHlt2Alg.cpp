/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DLLMuonTool.h"
#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonID/ICommonMuonTool.h"
#include "MuonID/IMVATool.h"
#include <string>

class MuonIDHlt2Alg final
    : public Gaudi::Functional::Transformer<LHCb::MuonPIDs( const LHCb::Track::Range&, const MuonHitContainer& )> {

  using base_class_t =
      Gaudi::Functional::Transformer<LHCb::MuonPIDs( const LHCb::Track::Range&, const MuonHitContainer& )>;
  using base_class_t::debug;
  using base_class_t::inputLocation;
  using base_class_t::msgLevel;
  using base_class_t::warning;
  using typename base_class_t::KeyValue;

public:
  MuonIDHlt2Alg( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t( name, pSvcLocator,
                      // Inputs
                      {KeyValue{"TracksLocations", LHCb::TrackLocation::Default},
                       KeyValue{"InputMuonHits", MuonHitContainerLocation::Default}},
                      // Outputs
                      KeyValue{"MuonIDLocation", LHCb::MuonPIDLocation::Default} ) {}

  StatusCode initialize() override { return base_class_t::initialize(); }

  /** Iterates over all tracks in the current event and performs muon id on them.
   * Resulting PID objects as well as muon tracks are stored on the TES.
   */
  LHCb::MuonPIDs operator()( const LHCb::Track::Range& tracks, const MuonHitContainer& hitContainer ) const override {
    LHCb::MuonPIDs                                muPids;
    std::map<LHCb::MuonPID*, float>               m_muonDistMap;
    std::map<LHCb::MuonPID*, CommonConstMuonHits> m_muonMap;

    muPids.setVersion( 1 ); // TODO: still valid? do we need a new version?

    std::vector<int>   isMuonIndices;
    std::vector<float> batchMVAFeaturesStorage;

    // counter buffers
    auto momCutCount     = m_momCutCount.buffer();
    auto preselCount     = m_preselCount.buffer();
    auto goodCount       = m_goodCount.buffer();
    auto pidsCount       = m_pidsCount.buffer();
    auto acceptanceCount = m_acceptanceCount.buffer();
    auto isLooseCount    = m_isLooseCount.buffer();
    auto isMuonCount     = m_isMuonCount.buffer();
    auto isTightCount    = m_isTightCount.buffer();

    // Iterate over all tracks and do the offline identification for each of them
    for ( const auto newTrackPtr : tracks ) {
      const auto& track = *newTrackPtr;
      if ( !isGoodOfflineTrack( track ) ) { continue; }
      goodCount++;
      auto muPid = std::make_unique<LHCb::MuonPID>(); // TODO: Put to where the values are set after the
                                                      // muonMap thing has been figured out...
      muPid->setIDTrack( newTrackPtr );
      muPid->setIsMuon( false );
      muPid->setIsMuonLoose( false );
      muPid->setIsMuonTight( false );
      muPid->setNShared( 0 );
      muPid->setPreSelMomentum( false );
      muPid->setInAcceptance( true );
      muPid->setMuonLLMu( -10000. );
      muPid->setMuonLLBg( -10000. );
      muPid->setChi2Corr( 0 );
      muPid->setMuonMVA1( -999. );
      muPid->setMuonMVA2( -999. );
      muPid->setMuonMVA3( -999. );
      muPid->setMuonMVA4( -999. );

      double probMu = 0.00000000001, probNonMu = 0.00000000001;
      double Dsquare = -1;

      float       trackP        = track.p();
      bool        preSel        = muonTool_->preSelection( trackP );
      const auto& state         = track.closestState( 9450.0 ); // state closest to M1
      const auto  extrapolation = muonTool_->extrapolateTrack( state );
      if ( preSel ) {
        muPid->setPreSelMomentum( 1 );
        ++momCutCount;
      }
      // Store the muonPIDs only for tracks passing InAcceptance
      if ( !muonTool_->inAcceptance( extrapolation ) ) { continue; }
      muPid->setInAcceptance( 1 );
      ++acceptanceCount;

      CommonConstMuonHits hits, hitsTight;
      if ( preSel ) {
        preselCount++;

        ICommonMuonTool::MuonTrackOccupancies occupancies, occupanciesTight;
        std::tie( hits, occupancies )           = muonTool_->hitsAndOccupancies( trackP, extrapolation, hitContainer );
        std::tie( hitsTight, occupanciesTight ) = muonTool_->extractCrossed( hits );
        const auto isMuon                       = muonTool_->isMuon( occupancies, trackP );
        const auto isMuonTight                  = muonTool_->isMuon( occupanciesTight, trackP );
        const auto isMuonLoose                  = muonTool_->isMuonLoose( occupancies, trackP );
        muPid->setIsMuon( isMuon );
        muPid->setIsMuonLoose( isMuonLoose );
        muPid->setIsMuonTight( isMuonTight );
        if ( isMuon ) { isMuonCount++; }
        if ( isMuonTight ) { isTightCount++; }
        if ( isMuonLoose ) { isLooseCount++; }

        if ( isMuonLoose and runDLL_ ) {
          std::tie( probMu, probNonMu, Dsquare ) =
              DLLTool_->calcMuonLL_tanhdist_landau( trackP, extrapolation, hits, occupancies );

          muPid->setMuonLLMu( log( probMu ) );
          muPid->setMuonLLBg( log( probNonMu ) );

          // TODO(kazeevn) check return value
          DLLTool_->calcNShared( muPid.get(), &muPids, hits, extrapolation, &m_muonDistMap, &m_muonMap );
        }
      }
      int this_index = muPids.insert( muPid.get() ); // ownership goes here...
      pidsCount++;

      // Only compute the MVA for tracks passing isMuon, as the BDT has been trained with this cut
      // This follows the same logic as the ProbNN which is not computed
      // for tracks with IsMuonLoose == 1 and IsMuon == 0
      if ( muPid->IsMuonLoose() ) {

        if ( runMVA_ && muPid->IsMuon() ) {
          if ( MVATool_->getRunCatBoostBatch() ) {
            isMuonIndices.push_back( this_index );
            // TODO(kazeevn) test whether it will be more efficient to allocate in
            // full in the beginning
            const size_t these_features_start = batchMVAFeaturesStorage.size();
            batchMVAFeaturesStorage.resize( batchMVAFeaturesStorage.size() + MVATool_->getNFeatures() );
            LHCb::span<float> this_features( batchMVAFeaturesStorage.data() + these_features_start,
                                             batchMVAFeaturesStorage.data() + batchMVAFeaturesStorage.size() );
            MVATool_->compute_features( trackP, state, hits, this_features );
          } else {
            // compute the MVA variable
            IMVATool::MVAResponses mva = MVATool_->calcBDT( trackP, state, hits );
            muPid->setMuonMVA2( mva.CatBoost );
            m_muonMVA2Stat += mva.CatBoost;
          }
        }
      }
      muPid.release(); // ownership goes with muPids
    }
    if ( runMVA_ && MVATool_->getRunCatBoostBatch() ) {
      const std::vector<double> predictions = MVATool_->calcBDTBatch( batchMVAFeaturesStorage );
      for ( size_t track_index_index = 0; track_index_index < isMuonIndices.size(); ++track_index_index ) {
        muPids( isMuonIndices[track_index_index] )->setMuonMVA2( predictions[track_index_index] );
        m_muonMVA2Stat += predictions[track_index_index];
      }
    }
    return muPids;
  }

private:
  /** Offline requirement for a good track. The track mustn't be a clone. Also it
   * should either be a long track or a downstream track or in case TT tracks are
   * supposed to be used a TT track.
   */
  bool isGoodOfflineTrack( const LHCb::Track& track ) const {
    return !track.checkFlag( LHCb::Track::Flags::Clone ) &&
           ( track.checkType( LHCb::Track::Types::Long ) || track.checkType( LHCb::Track::Types::Downstream ) ||
             ( useTTrack_ && track.checkType( LHCb::Track::Types::Ttrack ) ) );
  }

  ToolHandle<ICommonMuonTool> muonTool_{this, "CommonMuonTool", "CommonMuonTool"};
  ToolHandle<DLLMuonTool>     DLLTool_{this, "DLLMuonTool", "DLLMuonTool"};
  ToolHandle<IMVATool>        MVATool_{this, "MVATool", "MVATool"};

  mutable Gaudi::Accumulators::Counter<>           m_momCutCount{this, "nMomentumCut"};
  mutable Gaudi::Accumulators::Counter<>           m_preselCount{this, "nPreSelTrack"};
  mutable Gaudi::Accumulators::Counter<>           m_goodCount{this, "nGoodOffline"};
  mutable Gaudi::Accumulators::Counter<>           m_pidsCount{this, "nMuonPIDs"};
  mutable Gaudi::Accumulators::Counter<>           m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<>           m_isLooseCount{this, "nIsMuonLoose"};
  mutable Gaudi::Accumulators::Counter<>           m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<>           m_isTightCount{this, "nIsMuonTight"};
  mutable Gaudi::Accumulators::StatCounter<double> m_muonMVA1Stat{this, "muonMVA1Stat"};
  mutable Gaudi::Accumulators::StatCounter<float>  m_muonMVA2Stat{this, "muonMVA2Stat"};

  Gaudi::Property<bool> useTTrack_{this, "useTTrack", false};
  Gaudi::Property<bool> runDLL_{this, "runDLL", true};
  Gaudi::Property<bool> runMVA_{this, "runMVA", true};
};

DECLARE_COMPONENT_WITH_ID( MuonIDHlt2Alg, "MuonIDHlt2Alg" )
