/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "MakeMuonTool.h"
#include "MuonID/IMuonMatchTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MakeMuonTool
//
// 2015-04-15 : Ricardo Vazquez Gomez
// 2015-11-18 : Giacomo Graziani - add the possibility to add proper (correlated) chi2 to muon track
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MakeMuonTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MakeMuonTool::MakeMuonTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<MakeMuonTool>( this );
}

//=============================================================================

StatusCode MakeMuonTool::initialize() {
  StatusCode sc = GaudiTool::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;         // error printed already by GaudiAlgorithm

  muonTool_ = tool<ICommonMuonTool>( "CommonMuonTool" );

  MuonBasicGeometry basegeometry( detSvc(), msgSvc() );
  m_NStation = basegeometry.getStations();

  m_mudet   = getDet<DeMuonDetector>( "/dd/Structure/LHCb/DownstreamRegion/Muon" );
  matchTool = tool<IMuonMatchTool>( "MuonChi2MatchTool", this );

  return sc;
}

// find closest hit to the seed and add them to track
void MakeMuonTool::addLHCbIDsToMuTrack( LHCb::Track* muTrack, CommonConstMuonHits& hits,
                                        const ICommonMuonTool::MuonTrackExtrapolation& extrapolation ) {

  float        minDist[5]     = {1e10, 1e10, 1e10, 1e10, 1e10};
  float        distSeedHit[5] = {1e6, 1e6, 1e6, 1e6, 1e6};
  LHCb::LHCbID idToAdd[5];
  for_each( std::begin( hits ), std::end( hits ), [&]( const CommonMuonHit* ih ) {
    const LHCb::LHCbID id = ih->tile();
    unsigned           s  = id.muonID().station();
    distSeedHit[s]        = ( ih->x() - extrapolation[s].first ) * ( ih->x() - extrapolation[s].first ) +
                     ( ih->y() - extrapolation[s].second ) * ( ih->y() - extrapolation[s].second );
    if ( distSeedHit[s] < minDist[s] ) {
      minDist[s] = distSeedHit[s];
      idToAdd[s] = id;
    }
  } );
  unsigned idCounter = 0;
  for ( unsigned i = 0; i != m_NStation; ++i ) {
    if ( idToAdd[i].isMuon() != 0 ) {
      muTrack->addToLhcbIDs( idToAdd[i] );
      idCounter += 1;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of LHCbIDs added = " << idCounter << endmsg;
}

LHCb::Track* MakeMuonTool::makeMuonTrack( LHCb::MuonPID* mupid, CommonConstMuonHits& hits,
                                          const ICommonMuonTool::MuonTrackExtrapolation& extrapolation ) const {
  return makeMuonTrackWithProperChi2( mupid, hits, extrapolation );
}

LHCb::Track*
MakeMuonTool::makeMuonTrackWithProperChi2( LHCb::MuonPID* mupid, CommonConstMuonHits& hits,
                                           const ICommonMuonTool::MuonTrackExtrapolation& extrapolation ) const {
  const LHCb::Track* mother = mupid->idTrack();

  LHCb::Track* mtrack = new LHCb::Track( mupid->key() );
  // add mother track to ancestors
  mtrack->addToAncestors( *mother );
  mtrack->addToStates( mother->closestState( m_mudet->getStationZ( 0 ) ) );

  // This add all of the hits in FoI per each station
  CommonConstMuonHits::iterator ih;
  for ( ih = hits.begin(); ih != hits.end(); ih++ ) mtrack->addToLhcbIDs( ( *ih )->tile() );

  if ( ( hits.size() > 0 ) && m_ComputeChi2 ) {
    // put match information in IMuonMatchTool format
    std::vector<TrackMuMatch> matches;
    for ( ih = hits.begin(); ih != hits.end(); ih++ ) {
      const std::pair<float, float>& trackxy = extrapolation[( *ih )->station()];
      matches.push_back( std::make_tuple( *ih, 0., trackxy.first, trackxy.second ) );
    }

    // run Cagliari algorithm
    StatusCode matchStatus = matchTool->run( mother, &matches );
    if ( matchStatus.isFailure() ) {
      warning() << " Failed to run the matchTool for proper chi2 computation! " << endmsg;
    } else {
      auto [chi2, ndof] = matchTool->getChisquare();
      mupid->setChi2Corr( chi2 / ndof );
    }
  }

  return mtrack;
}
