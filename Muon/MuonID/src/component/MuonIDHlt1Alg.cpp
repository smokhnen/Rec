/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of MuonIDHlt1Alg.  */
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/MuonPID_v2.h"
#include "Event/PrLongTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/TrackSkin.h"
#include "Event/Zip.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbMath/Utils.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/IMuonMatchTool.h"
#include "SOAContainer/SOAContainer.h"
#include "SOAExtensions/ZipAlgorithms.h"
#include "SOAExtensions/ZipContainer.h"
#include "vdt/vdtMath.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <limits>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

namespace {
  using uint4 = std::array<unsigned, 4>;
  using xy_t  = std::pair<float, float>;

  using MuonPID                = LHCb::Event::v2::MuonPID;
  using MuonPIDs               = LHCb::Pr::Muon::PIDs;
  using Tracks                 = LHCb::Pr::Long::Tracks;
  using MuonTrackExtrapolation = std::array<std::pair<float, float>, 4>;
  using MuonTrackOccupancies   = std::array<unsigned, 4>;
  using SigmaPadCache          = std::array<std::array<float, 4>, 4>;
  using ScatterCache           = std::array<float, 10>; // try to 15
  using CovType                = std::array<float, 10>; // unneeded

  // compute min/max limits for float exp(x) exponent
  constexpr float   margin   = 1000.0f; // safety margin (for any subsequent calculations)
  static const auto min_expo = vdt::fast_logf( std::numeric_limits<float>::min() * margin );
  static const auto max_expo = vdt::fast_logf( std::numeric_limits<float>::max() / margin );

} // namespace

namespace details {

  class Cache {
    static constexpr float m_lowMomentumThres  = 6000.f;
    static constexpr float m_highMomentumThres = 10000.f;

    const DeMuonDetector*  m_det           = nullptr;
    size_t                 m_stationsCount = 0;
    size_t                 m_regionsCount  = 0;
    MuonTrackExtrapolation m_regionInner, m_regionOuter;
    SigmaPadCache          m_sigmapadX, m_sigmapadY;
    ScatterCache           m_scattercache;
    std::array<float, 4>   m_stationZ{{}};
    double                 m_foiFactor = 1.;

  public:
    double m_preSelMomentum = 0.;

  private:
    std::array<std::vector<double>, 3> m_foiParamX, m_foiParamY;
    std::vector<double>                m_momentumCuts;

  public:
    Cache( DeMuonDetector const& det, const Condition& FoiFactor, const Condition& PreSelMomentum,
           const Condition& FoiParametersX, const Condition& FoiParametersY, const Condition& MomentumCuts )
        : m_det{&det}
        , m_stationsCount( det.stations() )
        , m_regionsCount{det.regions() / m_stationsCount}
        , m_foiFactor{FoiFactor.param<double>( "FOIfactor" )}
        , m_preSelMomentum{PreSelMomentum.param<double>( "PreSelMomentum" )}
        , m_foiParamX{FoiParametersX.paramVect<double>( "XFOIParameters1" ),
                      FoiParametersX.paramVect<double>( "XFOIParameters2" ),
                      FoiParametersX.paramVect<double>( "XFOIParameters3" )}
        , m_foiParamY{FoiParametersY.paramVect<double>( "YFOIParameters1" ),
                      FoiParametersY.paramVect<double>( "YFOIParameters2" ),
                      FoiParametersY.paramVect<double>( "YFOIParameters3" )}
        , m_momentumCuts{MomentumCuts.paramVect<double>( "MomentumCuts" )} {

      // Update the cached DeMuon geometry (should be done by the detector element..)
      // det.fillGeoArray();
      for ( int s = 0; s != det.stations(); ++s ) {
        m_regionInner[s] = std::make_pair( det.getInnerX( s ), det.getInnerY( s ) );
        m_regionOuter[s] = std::make_pair( det.getOuterX( s ), det.getOuterY( s ) );
        m_stationZ[s]    = det.getStationZ( s );
      }

      // TODO(kazeevn, raaij) fix the upgrade CondDB and remove the hack
      // Remove parameters we don't need, in particular in case of the upgrade
      // geometry, the first set of nRegions parameters
      for ( auto params : {std::ref( m_foiParamX ), std::ref( m_foiParamY )} ) {
        for ( auto& v : params.get() ) {
          if ( v.size() != m_stationsCount * m_regionsCount ) {
            v.erase( v.begin(), v.begin() + ( 5 - m_stationsCount ) * m_regionsCount );
          }
        }
      }

      // fill the matrix of pad sizes
      for ( int s = 0; s != det.stations(); ++s ) {
        for ( int r = 0; r != det.regions() / det.stations(); ++r ) {
          m_sigmapadX[s][r] = det.getPadSizeX( s, r ) / std::sqrt( 12 );
          m_sigmapadY[s][r] = det.getPadSizeY( s, r ) / std::sqrt( 12 );
        }
      }

      // fill the mcs
      constexpr auto mcs =
          std::array{std::pair<float, float>{12800. * Gaudi::Units::mm, 25.},   // ECAL + SPD + PS was 28?
                     std::pair<float, float>{14300. * Gaudi::Units::mm, 53.},   // HCAL
                     std::pair<float, float>{15800. * Gaudi::Units::mm, 47.5},  // M23 filter
                     std::pair<float, float>{17100. * Gaudi::Units::mm, 47.5},  // M34 filter
                     std::pair<float, float>{18300. * Gaudi::Units::mm, 47.5}}; // M45 filter

      for ( int i = 0; i < det.stations(); ++i ) {
        for ( int j = 0; j <= i; ++j ) {
          m_scattercache[i * ( i + 1 ) / 2 + j] =
              std::accumulate( mcs.begin(), mcs.begin() + j + 2, 0.f,
                               [i, j, z = m_stationZ]( float sum, const std::pair<float, float>& mcs ) {
                                 return sum + ( z[i] - mcs.first ) * ( z[j] - mcs.first ) * 13.6 * Gaudi::Units::MeV *
                                                  13.6 * Gaudi::Units::MeV * mcs.second;
                               } );
        }
      }
    }

    /** Checks 'isMuon' given the occupancies corresponding to a track and the
     * track's momentum. The requirement differs in bins of p.
     */
    bool isMuon( const MuonTrackOccupancies& occupancies, float p ) const {
      if ( p < m_preSelMomentum ) return false;
      auto has = [&]( unsigned station ) { return occupancies[station] != 0; };
      if ( !has( 0 ) || !has( 1 ) ) return false;
      if ( p < m_momentumCuts[0] ) return true;
      if ( p < m_momentumCuts[1] ) return has( 2 ) || has( 3 );
      return has( 2 ) && has( 3 );
    }

    /** Computes the correlated chi2
     */
    float chi2Corr( const float p, const MuonTrackExtrapolation& extrapolation, const MuonHitContainer& hitContainer,
                    const MuonTrackOccupancies& occupancies ) const {

      const unsigned int order =
          std::count_if( occupancies.begin(), occupancies.end(), []( unsigned i ) { return i != 0; } );
      if ( order < 2 ) return -10000.;

      struct MuonHit {
        unsigned sta, reg;
        double   deltax, deltay;
        MuonHit( unsigned s, unsigned r, double dx, double dy ) // remove with c++20
            : sta{s}, reg{r}, deltax{dx}, deltay{dy} {}
      };
      boost::container::static_vector<MuonHit, 4> cand;

      for ( unsigned i = 0; i < m_stationsCount; i++ ) {

        if ( !occupancies[i] ) continue;
        if ( i > 1 && p < 6000 ) break;

        // take the hit closest to the extrapolation point
        assert( hitContainer.station( i ).hits().begin() != hitContainer.station( i ).hits().end() );
        const auto chit = *std::min_element(
            hitContainer.station( i ).hits().begin(), hitContainer.station( i ).hits().end(),
            [i, extrapolation]( const CommonMuonHit& hit1, const CommonMuonHit& hit2 ) {
              float dr1 = pow( hit1.x() - extrapolation[i].first, 2 ) + pow( hit1.y() - extrapolation[i].second, 2 );
              float dr2 = pow( hit2.x() - extrapolation[i].first, 2 ) + pow( hit2.y() - extrapolation[i].second, 2 );
              return dr1 < dr2;
            } );
        cand.emplace_back( i, chit.region(), chit.x() - extrapolation[i].first, chit.y() - extrapolation[i].second );
      }

      class MuonCandCovariance {
      private:
        CovType              m_cov = {0};
        unsigned             m_n   = 0;
        static constexpr int index( int i, int j ) noexcept {
          auto mx = std::max( i, j );
          auto mn = std::min( i, j );
          return mx * ( mx + 1 ) / 2 + mn;
        }

      public:
        unsigned       size() const noexcept { return m_n; }
        const CovType& cov() const noexcept { return m_cov; }
        CovType&       cov() noexcept { return m_cov; }
        float          operator()( unsigned i, unsigned j ) const noexcept { return m_cov[index( i, j )]; }
        float&         operator()( unsigned i, unsigned j ) noexcept { return m_cov[index( i, j )]; }
        MuonCandCovariance( LHCb::span<const MuonHit> cand, float p, const ScatterCache& scattercache,
                            const SigmaPadCache& sigmapad )
            : m_n( cand.size() ) {
          const auto invp2 = 1. / ( p * p );
          for ( unsigned i = 0; i < size(); ++i ) {
            for ( unsigned j = 0; j <= i; ++j ) {
              operator()( i, j ) = scattercache[index( cand[i].sta, cand[j].sta )] * invp2;
            }
            const auto sigma = sigmapad[cand[i].sta][cand[i].reg];
                       operator()( i, i ) += sigma * sigma;
          }
        }
      };

      MuonCandCovariance covX( cand, p, m_scattercache, m_sigmapadX );
      MuonCandCovariance covY( cand, p, m_scattercache, m_sigmapadY );

      ROOT::Math::CholeskyDecompGenDim<float> decompX( covX.size(), &covX( 0, 0 ) );
      ROOT::Math::CholeskyDecompGenDim<float> decompY( covY.size(), &covY( 0, 0 ) );

      if ( !decompX || !decompY ) return -10000.;
      decompX.Invert( &covX( 0, 0 ) );
      decompY.Invert( &covY( 0, 0 ) );

      float chi2corr = 0;
      for ( unsigned i = 0; i < cand.size(); ++i ) {
        for ( unsigned j = 0; j < i; ++j ) {
          chi2corr +=
              2 * ( cand[i].deltax * covX( i, j ) * cand[j].deltax + cand[i].deltay * covY( i, j ) * cand[j].deltay );
        }
        chi2corr += cand[i].deltax * covX( i, i ) * cand[i].deltax + cand[i].deltay * covY( i, i ) * cand[i].deltay;
      }
      // printf("-> p=%f, chi2corr = %f, ndof=%lu\n", p, chi2corr, cand.size());

      return chi2corr / cand.size();
    }

    /** Helper function that returns the x, y coordinates of the FoI for a
     * given station and region and a track's momentum.
     */
    std::pair<float, float> foi( unsigned int station, unsigned int region, float p ) const {
      auto i = station * m_regionsCount + region;
      if ( p < 1000000.f ) { // limit to 1 TeV momentum tracks
        const auto pGev                = p / static_cast<float>( Gaudi::Units::GeV );
        auto       ellipticalFoiWindow = [&]( const auto& fp ) {
          const float expo = -fp[2][i] * pGev;
          return fp[0][i] + fp[1][i] * vdt::fast_expf( std::clamp( expo, min_expo, max_expo ) );
        };
        return {ellipticalFoiWindow( m_foiParamX ), ellipticalFoiWindow( m_foiParamY )};
      }
      return {m_foiParamX[0][i], m_foiParamY[0][i]};
    }

    /** Check whether track extrapolation is within detector acceptance.
     */
    bool inAcceptance( const MuonTrackExtrapolation& extrapolation ) const {
      auto abs_lt = []( const xy_t& p1, const xy_t& p2 ) {
        return std::abs( p1.first ) < p2.first && std::abs( p1.second ) < p2.second;
      };

      // Outer acceptance
      if ( !abs_lt( extrapolation[0], m_regionOuter[0] ) ||
           !abs_lt( extrapolation[m_stationsCount - 1], m_regionOuter[m_stationsCount - 1] ) ) {
        // Outside M2 - M5 region
        return false;
      }

      // Inner acceptance
      if ( abs_lt( extrapolation[0], m_regionInner[0] ) ||
           abs_lt( extrapolation[m_stationsCount - 1], m_regionInner[m_stationsCount - 1] ) ) {
        // Inside M2 - M5 chamber hole
        return false;
      }

      return true;
    }

    /** Project a given track into the muon stations.
     */
    template <typename State>
    MuonTrackExtrapolation extrapolateTrack( State const& state ) const {
      MuonTrackExtrapolation extrapolation;

      // Project the state into the muon stations
      // Linear extrapolation equivalent to TrackLinearExtrapolator
      for ( unsigned station = 0; station != m_stationsCount; ++station ) {
        // x(z') = x(z) + (dx/dz * (z' - z))
        extrapolation[station] = {LHCb::Utils::as_arithmetic( state( 0 ) ) +
                                      LHCb::Utils::as_arithmetic( state( 3 ) ) *
                                          ( m_stationZ[station] - LHCb::Utils::as_arithmetic( state( 2 ) ) ),
                                  LHCb::Utils::as_arithmetic( state( 1 ) ) +
                                      LHCb::Utils::as_arithmetic( state( 4 ) ) *
                                          ( m_stationZ[station] - LHCb::Utils::as_arithmetic( state( 2 ) ) )};
      }
      return extrapolation;
    }

    std::array<uint4, 2> countHits( const float p, const MuonTrackExtrapolation& extrapolation,
                                    const MuonHitContainer& hitContainer ) const {

      // decide the ordering to look for hits
      // The correct order to look should be momentum dependent
      // M3, M2                  3<p<6 GeV
      // (M4 or M5) and M3, M2   6<p<10 GeV
      // M5, M4, M3, M2          p>10 GeV
      const auto ordering{p >= m_lowMomentumThres ? uint4{3, 2, 1, 0} : uint4{1, 0, 3, 2}};
      uint4      occupancies{0, 0, 0, 0};
      uint4      occupancies_crossed{0, 0, 0, 0};

      /* Define an inline callable that makes it easier to check whether a hit is
       * within a given window. */
      class IsInWindow {
        xy_t                center_;
        std::array<xy_t, 4> foi_;

      public:
        // Constructor takes parameters by value and then moves them into place.
        IsInWindow( xy_t center, xy_t foi0, xy_t foi1, xy_t foi2, xy_t foi3, double sf )
            : center_{std::move( center )}
            , foi_{{std::move( foi0 ), std::move( foi1 ), std::move( foi2 ), std::move( foi3 )}} {
          std::transform( begin( foi_ ), end( foi_ ), begin( foi_ ), [sf]( const xy_t& p ) -> xy_t {
            return {p.first * sf, p.second * sf};
          } );
        }
        bool operator()( const CommonMuonHit& hit ) const {
          auto region = hit.region();
          assert( region < 4 );
          return ( std::abs( hit.x() - center_.first ) < hit.dx() * foi_[region].first ) &&
                 ( std::abs( hit.y() - center_.second ) < hit.dy() * foi_[region].second );
        }
      };

      auto makeIsInWindow = [&]( unsigned station ) {
        return IsInWindow{extrapolation[station], foi( station, 0, p ), foi( station, 1, p ),
                          foi( station, 2, p ),   foi( station, 3, p ), m_foiFactor};
      };

      for ( unsigned station : ordering ) {
        auto predicate = makeIsInWindow( station );

        for ( const auto& hit : hitContainer.station( station ).hits() ) {
          if ( predicate( hit ) ) {
            occupancies[station]++;
            if ( !hit.uncrossed() || m_det->mapInRegion( station, hit.region() ) == 1 ) {
              occupancies_crossed[station]++;
            }
          }
        }

        // evaluate the occupancies from less to more occupied to avoid
        // useless loops through stations
        if ( station < 2 && occupancies[station] == 0 ) break;
        if ( station == 2 && occupancies[station] == 0 && occupancies[station + 1] == 0 && p > m_lowMomentumThres &&
             p < m_highMomentumThres )
          break;
        if ( p > m_highMomentumThres && occupancies[station] == 0 ) break;
      }

      return {occupancies, occupancies_crossed};
    }
  };

} // namespace details

namespace MuonTag = LHCb::Pr::Muon::Tag;

class MuonIDHlt1Alg final
    : public Gaudi::Functional::Transformer<MuonPIDs( const Tracks&, const MuonHitContainer&, const details::Cache& ),
                                            LHCb::DetDesc::usesConditions<details::Cache>> {
  using base_class_t =
      Gaudi::Functional::Transformer<MuonPIDs( const Tracks&, const MuonHitContainer&, const details::Cache& ),
                                     LHCb::DetDesc::usesConditions<details::Cache>>;
  using base_class_t::addConditionDerivation;
  using base_class_t::debug;
  using base_class_t::inputLocation;
  using base_class_t::msgLevel;
  using base_class_t::warning;
  using typename base_class_t::KeyValue;

public:
  MuonIDHlt1Alg( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t{name,
                     pSvcLocator,
                     // Inputs
                     {KeyValue{"InputTracks", "/Event/Rec/Track/Forward"}, // LHCb::TrackLocation::Default,
                      KeyValue{"InputMuonHits", MuonHitContainerLocation::Default},
                      KeyValue{"ConditionsCache", "MuonIDHlt1Alg-" + name + "-ConditionsCache"}},
                     // Output
                     KeyValue{"OutputMuonPID", LHCb::Event::v2::MuonPIDLocation::Default}} {}

  StatusCode initialize() override {
    return base_class_t::initialize().andThen( [&] {
      using namespace std::string_literals;
      this->addConditionDerivation<details::Cache( const DeMuonDetector&, const Condition&, const Condition&,
                                                   const Condition&, const Condition&, const Condition& )>(
          {DeMuonLocation::Default, "Conditions/ParticleID/Muon/FOIfactor"s,
           "Conditions/ParticleID/Muon/PreSelMomentum"s, "Conditions/ParticleID/Muon/XFOIParameters"s,
           "Conditions/ParticleID/Muon/YFOIParameters"s, "Conditions/ParticleID/Muon/MomentumCuts"s},
          this->inputLocation<details::Cache>() );
    } );
  }

  /** Iterates over all tracks in the current event and performs muon id on them.
   * Resulting PID objects are stored on the TES.
   */
  MuonPIDs operator()( const Tracks& tracks, const MuonHitContainer& hitContainer,
                       const details::Cache& cond ) const override {
    // counter buffers
    auto muonIDlambda = [momCutCount = m_momCutCount.buffer(), goodCount = m_goodCount.buffer(),
                         acceptanceCount = m_acceptanceCount.buffer(), isMuonCount = m_isMuonCount.buffer(),
                         isTightCount = m_isTightCount.buffer(), &hitContainer, &cond]( auto const& track ) mutable {
      ++goodCount;
      // TODO: Put to where the values are set after the
      // muonMap thing has been figured out...
      MuonPID muPid;
      muPid.setIsMuon( false );
      muPid.setIsMuonTight( false );
      // muPid.setMuonLLMu(-10000.);
      // muPid.setMuonLLBg(-10000.);
      muPid.setChi2Corr( -10000. );

      auto trackmom = LHCb::Utils::as_arithmetic( track.p() );
      bool preSel   = ( trackmom > cond.m_preSelMomentum );
      muPid.setPreSelMomentum( preSel );
      if ( !preSel ) return muPid;
      ++momCutCount;

      // Store the muonPIDs only for tracks passing InAcceptance
      MuonTrackExtrapolation extrapolation = cond.extrapolateTrack( track.StatePosDir( 1 ) );
      bool                   inAcceptance  = cond.inAcceptance( extrapolation );
      muPid.setInAcceptance( inAcceptance );
      if ( !inAcceptance ) return muPid;
      ++acceptanceCount;

      auto [occupancies, occupanciesTight] = cond.countHits( trackmom, extrapolation, hitContainer );
      bool isM                             = cond.isMuon( occupancies, trackmom );
      bool isMTight                        = cond.isMuon( occupanciesTight, trackmom );

      muPid.setIsMuon( isM );
      muPid.setIsMuonTight( isMTight );

      if ( isM ) ++isMuonCount;
      if ( isMTight ) ++isTightCount;
      if ( isM ) {
        float chi2Corr = cond.chi2Corr( trackmom, extrapolation, hitContainer, occupancies );
        muPid.setChi2Corr( chi2Corr );
      }

      return muPid;
    };

    auto                 iterable_tracks_scalar = LHCb::Pr::make_zip<SIMDWrapper::InstructionSet::Scalar>( tracks );
    LHCb::Pr::Muon::PIDs muonPIDs{tracks.zipIdentifier()};
    for ( auto track : iterable_tracks_scalar ) {
      auto muPid = muonIDlambda( track );
      auto pid   = muonPIDs.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
      pid.field<MuonTag::Status>().set( muPid.Status() );
      pid.field<MuonTag::Chi2Corr>().set( muPid.chi2Corr() );
    }
    return muonPIDs;
  }

private:
  mutable Gaudi::Accumulators::Counter<> m_momCutCount{this, "nMomentumCut"};
  mutable Gaudi::Accumulators::Counter<> m_goodCount{this, "nGoodOffline"};
  mutable Gaudi::Accumulators::Counter<> m_acceptanceCount{this, "nInAcceptance"};
  mutable Gaudi::Accumulators::Counter<> m_isMuonCount{this, "nIsMuon"};
  mutable Gaudi::Accumulators::Counter<> m_isTightCount{this, "nIsMuonTight"};

  Gaudi::Property<bool> useTTrack_{this, "useTTrack", false};
  Gaudi::Property<bool> m_ComputeChi2{this, "ComputeChi2", true};
};

DECLARE_COMPONENT_WITH_ID( MuonIDHlt1Alg, "MuonIDHlt1AlgPr" )
