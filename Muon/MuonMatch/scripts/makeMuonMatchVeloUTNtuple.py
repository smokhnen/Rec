###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Moore script to produce a MuonMatchVeloUTNtuple
# execute with Moore/run gaudirun.py makeMuonMatchVeloUTNtuple.py

from Moore import options

from GaudiKernel.SystemOfUnits import MeV, mm

from Configurables import NTupleSvc, ApplicationMgr

from PyConf.application import configure_input, configure, make_odin, make_data_with_FetchDataFromFile
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Algorithms import MuonMatchVeloUTNtuple, DataPacking__Unpack_LHCb__MCMuonHitPacker_

from Hlt2Conf.data_from_file import mc_unpackers

from RecoConf.hlt1_tracking import require_gec, default_ft_decoding_version, make_pvs
from RecoConf.hlt1_muonmatch import make_tracks_with_muonmatch, make_tracks_with_muonmatch_ipcut
from RecoConf.hlt1_muonid import make_muon_hits
from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system, make_links_tracks_mcparticles

# adapt ft decoding version to the datasample used
default_ft_decoding_version.global_bind(value=6)


def standalone_hlt1_muonmatching_reco(velo_track_min_ip=0.0 * mm,
                                      tracking_min_pt=80. * MeV):
    odin_loc = make_odin()
    all_tracks = make_tracks_with_muonmatch_ipcut(velo_track_min_ip,
                                                  tracking_min_pt)
    muon_hits = make_muon_hits()
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=all_tracks["Upstream"], LinksToLHCbIDs=links_to_lhcbids)
    pvs = make_pvs()

    unpacker_muon_mchits = DataPacking__Unpack_LHCb__MCMuonHitPacker_(
        InputName=make_data_with_FetchDataFromFile("/Event/pSim/Muon/Hits"))

    somedata = make_data_with_FetchDataFromFile("/Event/MC/TrackInfo")

    ntuple = MuonMatchVeloUTNtuple(
        OdinLocation=odin_loc,
        PrimaryVertecies=pvs,
        VeloUTTracks=all_tracks["Upstream"]['v2'],
        MuonMatchTracks=all_tracks["MuonMatch"]['v2'],
        MuonHits=muon_hits,
        MCParticles=mc_unpackers()["MCParticles"],
        MCMuonHits=unpacker_muon_mchits.OutputName,
        VeloUTMCParticlesLinks=links_to_tracks,
        MCMuonDigitLinks=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/Muon/Digits'),
        onlyMCMuons=True)

    nodes = [require_gec()]
    nodes += [somedata, ntuple]

    return CompositeNode(
        'hlt1_muonmatching_ntuple',
        children=nodes,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)


options.set_input_from_testfiledb('upgrade-magup-sim10-up01-34112106-xdigi')
options.set_conds_from_testfiledb('upgrade-magup-sim10-up01-34112106-xdigi')

options.evt_max = 1000

config = configure_input(options)
top_cf_node = standalone_hlt1_muonmatching_reco()
config.update(configure(options, top_cf_node))

ApplicationMgr().HistogramPersistency = "ROOT"
ntSvc = NTupleSvc()
ntSvc.Output = [
    "FILE1 DATAFILE='VeloUTNtuple_withMuonMatch.root' OPT='NEW' TYP='ROOT'"
]
ApplicationMgr().ExtSvc += [ntSvc]
