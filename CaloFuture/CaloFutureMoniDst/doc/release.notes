! -----------------------------------------------------------------------------
! Package     : CaloFuture/CaloFutureMoniDst
! Responsible : Olivier Deschamps odescham@in2p3.fr
! Purpose     : CaloFuture monitoring of Dst tape 
! -----------------------------------------------------------------------------

! 2016-08-16 - Olivier Deschamps
  - implement counter() switch based on FutureCounterLevel tool 

! 2016-06-11 - Deschamps Olivier for J.-F. Marchand
 - ResolvedPi0Monitor : add 2d histo for pi0 mass versus cell index

!========================= CaloFutureMoniDst v5r21 2016-01-28 =========================
! 2015-12-08 - Marco Cattaneo
 - Fix untested StatusCodes exposed by previous fix

! 2015-12-06 - Gerhard Raven
 - replace explicit boost::assign::list_of with implicit std::initializer_list

!========================= CaloFutureMoniDst v5r20 2015-11-23 =========================
! 2015-11-06 - O. Deschamps
  -fix clang warnings

! 2015-10-22 - O. Deschamps
  -initialize base-class (CaloFutureMoniAlg) everywhere 

!========================= CaloFutureMoniDst v5r19 2015-10-13 =========================
! 2015-08-30 - Gerhard Raven
 - remove DECLARE_ALGORITHM_FACTORY( CaloFutureMoniAlg ) and DECLARE_ALGORITHM_FACTORY( CaloFutureNtpBase )
   as these are not 'concrete' algorithms, but base classes from which one first must inherit
   and implement execute before they are useful. 

! 2015-08-12 - Gerhard Raven
 - remove #include of obsolete Gaudi headers

!========================= CaloFutureMoniDst v5r18 2015-05-26 =========================
! 2015-05-12 - Olivier Deschamps
  - fix property naming in CaloFutureHypoMonitor
  - update CaloFuturePi0Ntp & CaloFutureElectronNtp

!========================= CaloFutureMoniDst v5r17 2014-05-13 =======================
! 2014-05-06 - Marco Cattaneo
 - CaloFutureMoniAlg.cpp: use setHistoTopDir in initialize() instead of
   setProperty("HistoTopDir") in constructor. Removes need for debug() in 
   constructor
 - CaloFutureEFlowAlg.cpp: remove unnecessary debug() in constructor
 - Above two changes fix unprotected debug() warnings

!========================= CaloFutureMoniDst v5r16 2014-03-18 =======================
! 2014-03-06 - Olivier Deschamps
 -  python/Configuration.py : propagate OutputLevel value to calo sequences only when the property is explicity set

!========================= CaloFutureMoniDst v5r15 2013-10-24 =======================
! 2013-10-09 - Olivier Deschamps
 -  python/CaloFutureDAQ/Monitors.py & Configuration.py :  possibility to set-up a configuration without active Spd/Prs  (NoSpdPrs = <False> flag)
 -  src/  :  protect monitoring algorithms against the upgrade configuration with no Spd/Prs

!========================= CaloFutureMoniDst v5r14 2013-07-18 =======================
! 2013-06-12 - Marco Cattaneo
 - Fix gcc48 warning
 - Change a global variable to local, fixes UNINIT_CTOR warning
 - Remove empty finalize methods

!========================= CaloFutureMoniDst v5r13 2012-11-28 =======================
! 2012-11-22 - Marco Clemencic
 - Added CMake configuration file.
 - Modified requirements to simplify auto conversion to CMake.

! 2012-10-08 - Marco Cattaneo
 - In Configuration.py, remove setting of MeasureTime=true in sequencers,
   should not be the default, and should in any case use TimingAuditor.
 - Fix UNINIT_CTOR defects
 - Fix trivial icc remarks

! 2012-10-08 - Olivier Deschamps
 - Fix coverity problems

!========================= CaloFutureMoniDst v5r12 2012-06-25 =======================
! 2012-05-13 - Olivier Deschamps
 - fix CaloFutureAlignmentNtp
 - update L0CaloFutureScale

!========================= CaloFutureMoniDst v5r11 2012-05-02 =======================
! 2012-04-17 - Olivier Deschamps
 - new algorithm : L0CaloFutureScale to compare CaloFutureCluster/CaloFutureHypo/L0Cluster transverse
   energy

!========================= CaloFutureMoniDst v5r10 2012-04-13 =======================
! 2012-04-05 - Olivier Deschamps
 - Add eta->gg monitoring (re-use resolvedPi0 algorithm)
 - SpdMonitor : set splitArea=true as the default for CaloFuture2Dview histos 

!========================= CaloFutureMoniDst v5r9 2011-09-06 ========================
! 2011-09-03 - Olivier Deschamps
 - fix CaloFutureAlignment.cpp

!========================= CaloFutureMoniDst v5r8 2011-07-27 ========================
! 2011-07-22 - Marco Cattaneo
 - Create debug() messages only when output level requires it,
   also using UNLIKELY macro
 - Replace all endreq by endmsg

!========================= CaloFutureMoniDst v5r7 2011-02-25 ========================
! 2011-02-07 - Olivier Deschamps
 - update tupling algorithms

!========================= CaloFutureMoniDst v5r6 2011-01-17 ========================
! 2010-10-07 - Olivier Deschamps
 - New base class : CaloFutureNtpBase for tupling algorithms
 - CaloFutureAlignmentNtp now inherits from CaloFutureNtpBase
 @TODO : make other XXNtp algorithm to inherit from the base class

!========================= CaloFutureMoniDst v5r5 2010-10-28 ========================
! 2010-10-07 - Olivier Deschamps
 - Fix windows warning

! 2010-10-01 - Rob Lambert
 - Fix windows warning, converting double to int

! 2010-09-30 - Marco Cattaneo
 - Undo part of previous fix, which was causing a relations table to not be 
   loaded, causing the algorithm to throw ane exception. This was masking the
   finalization crash, that is still there....

! 2010-09-30 - Olivier Deschamps 
  - CaloFuturePhotonChecker : fix finalization crash

! 2010-09-30 - Olivier Deschamps 
  - Improve CaloFuturePi0Ntp & CaloFutureElectronNtp
  - add new ntp algorithm : CaloFutureAlignmentNtp


!========================== CaloFutureMoniDst v5r4 2010-09-29 =======================
! 2010-09-27 - Marco Cattaneo
 - Fix uninitialised member variables in CaloFuturePhotonChecker

! 2010-09-01 - Olivier Deschamps
  - fix compilation warning on windows
  - improve CaloFutureHypoNtp

! 2010-08-31 - Olivier Deschamps
  - fix compilation warning on slc4

! 2010-08-27 - Olivier Deschamps
  - new algorithm : CaloFutureHypoNtp
  - CaloFutureHypoMonitor : new monitoring histogram Nhypo/Nclusters

!========================== CaloFutureMoniDst v5r3 2010-06-22 =======================
! 2010-06-05 - Rob Lambert
 - Fixes for mistake in yesterday's commit

! 2010-06-04 - Rob Lambert
 - Fixes for windows warnings savannah 15808

!========================== CaloFutureMoniDst v5r2 2010-05-24 =======================

! 2010-05-24 - Rob Lambert
 - fix for typo in CaloFuturePi0Ntp


! 2010-05-17 - Olivier Deschamps
  - python/Monitor.py : activate missing monitoring histo for ResolvedPi0 monitoring

!========================== CaloFutureMoniDst v5r1 2010-04-26 =======================
! 2010-04-12 - Dmitry GOLUBKOV
 - doc/release.notes : corrected the place of the second comment from 2010-04-12
 - Monitor.py : check if properties were already set before setting them
 - cmt/requirements: version incremented to v5r1 

!========================== CaloFutureMoniDst v5r0 2010-04-12 =======================
! 2010-04-12 - Dmitry GOLUBKOV
 - CaloFutureEMuPIDMon.cpp: fixed an unchecked StatusCode in the finalize() method

! 2010-04-09 - Rob Lambert
 - Fixed windows warnings in CaloFutureEMuPIDMon.cpp and CaloFuturePi0Checker.cpp

! 2010-04-02 - Dmitry Golubkov
 - CaloFutureEMuPIDMon.cpp: fixed an SLC5 warning: type qualifiers ignored on function return type 

! 2010-03-31 - Dmitry Golubkov
  - following Vanya's suggestion remove completely CaloFutureEMuMonitor, CaloFutureEMuChecker

! 2010-03-31 - Olivier Deschamps
  - fix typo in CaloFuturePi0Monitor.cpp

! 2010-03-29 - Dmitry Golubkov
 - CaloFutureMoniDstConf : introduced Histograms slot and passed it to pidsMoni(),
   test the value of Histograms in CaloFutureMoniDstConf.checkConfiguration()
 - Monitor.py : add 'CaloFutureEMuMonitor/CaloFuturePIDMon' to the monitoring sequence only
     if Histograms in ['OfflineFull', 'Expert']

! 2010-03-26 - Dmitry GOLUBKOV
 - CaloFutureEMuPIDMon.{h,cpp}: a new simplified PID monitoring algorithm
 - Monitor.py: added CaloFutureEMuPIDMon{Uncut,Soft,Hard} to the monitoring sequence
 - CaloFuturePIDsMonitor.opts: added CaloFutureEMuPIDMon to the CaloFuturePIDsMon sequence (?)
 - cmt/requirements: version increment to v4r6

!========================== CaloFutureMoniDst v4r5 2010-03-19 =======================
! 2010-03-22 - Rob Lambert
 - Fixed some windows warnings savannah 64668

! 2010-03-08 - Olivier Deschamps
  - adapt CaloFuturePhotonChecker to change in CaloFuturePIDs
  
  @TODO : review checkers (use CaloFutureMCTools ...)

! 2010-03-08 - Olivier Deschamps
 - make use of CaloFutureAlgUtils to define context-dependent TES inputs
 - add control counters
 - CaloFutureMoniDstConf 
    - add missing ProtoElectronMonitor
    - delegate the TES I/O to CaloFutureAlgUtils
 

 - Usage :

        from Configurables import CaloFutureProcessor
        from Configurables import CaloFutureMoniDst
        CaloFutureProcessor(Context = 'Repro' , Sequence = recSeq )
        CaloFutureMoniDst(Context = 'Repro' , Sequence = moniSeq )

    -> provides consistent CaloFutureReco + Monitoring sequences for 'Repro' context



!========================== CaloFutureMoniDst v4r4 2010-02-15 =======================

! 2010-02-11 - Marco Cattaneo
 - Replace dependency on DaVinciMCKernel with MCAssociators, to follow changes
   in LHCB v29r1

! 2010-01-30 - Dmitry Golubkov
 - CaloFutureEMuMonitor: add more histograms, crude windows based on rp6 of Dec 2009 data
 - CaloFutureEMuChecker: move include Kernel/Particle2MCLinker.h from CaloFutureEMuMonitor.h

! 2010-02-04 : Olivier Deschamps
  - CaloFuturePi0Monitor : fix bad initialization of base class
  - CaloFutureMoniAlg : init m_detData for non specific calo detector algo (default Ecal is assumed)

! 2010-01-28 : Olivier Deschamps
 - SpdMonitor    : speed up processing - inherits from CaloFuture2Dview (Albert Puig)
 - CalOEFlowAlg  : code update from Aurélien.
  Warning : new CaloFutureEFlow require MCEvent dependency (obtained through DaVinciMCKernel so far)
            -> to do : split the MC 'checker' part of the code


!========================== CaloFutureMoniDst v4r3 2010-01-21 =======================
! 2010-01-13 - Marco Cattaneo
 - Follow Gaudi fix #61116: GaudiCommon::Exception now returns void

! 2009-12-11 - Olivier Deschamps
 - CaloFutureProtoElectronMonitor : make use of proto->info(PrsE) instead of CaloFutureHypo2CaloFuture tool.

! 2009-12-11 - Olivier Deschamps 
 - CaloFutureMoniAlg : remove verbose line
 - new algorithm CaloFutureProtoElectronMonitor
 - CaloFuturePi0Monitor : add pi0 mass peak with Y-selection of clusters.


!========================== CaloFutureMoniDst v4r2 2009-12-11 =======================
! 2009-12-01 - Olivier Deschamps 
 - CaloFutureMoniAlg : protection against non booked histograms in hfillX methods

! 2009-11-30 - Olivier Deschamps 
 - CaloFutureMoniAlg : add saturation bin(s) by default for 1D histograms


!========================== CaloFutureMoniDst v4r1 2009-10-20 =======================
! 2009-10-16 - Olivier Deschamps for Aurelien Martens 
 - Update in CaloFutureEFlowAlg

! 2009-10-06 - Marco Cattaneo
 - Add link to LHCb-INT-2009-022 note in CaloFutureEMuMonitor.h doxygen header

! 2009-10-01 - Vanya BELYAEV
 - CaloFutureMoniAlg.h :
    minor fix for the recent modification in CaloFutureCellCode functions 

! 2009-09-14 - Olivier Deschamps
 - CaloFuturePi0Monitor : 
     - selection of non-converted photons
     - isolation criteria
     - Add background subtracted histo 

! 2009-09-08 - Olivier Deschamps
 - Add background histo in resolved pi0 monitor
 - Fix problem with HashMap<const string, IHistogram*> removing const !

!========================== CaloFutureMoniDst v4r0 2009-09-07 =======================
! 2009-09-07 - Dmitry GOLUBKOV
 - CaloFutureEMuMonitor, CaloFutureEMuChecker: add proper calls to the base-class finalize()
 - CaloFuturePIDsChecker.cpp : fix division by zero in divide()

! 2009-09-01 - Vanya BELYAEV
 - fix warnings in configurables 

! 2009-08-31 - Olivier Deschamps
 - fix bug in CaloFutureMoniAlg (SplitAreas/m_split property/member already exists in inherited CaloFuture2DView)

! 2009-08-11 - Vanya BELYAEV

 - futher polishing of configurables 


! 2009-08-05 - Vanya BELYAEV

 - add proepr configurable 
   version incremen to v4r0 

! 2009-07-30 - Dmitry GOLUBKOV
 - CaloFutureEMuMonitor.cpp add protection when no charged ProtoParticle
 - cmt/requirements version incremented to v3r10

!========================== CaloFutureMoniDst v3r9 2009-07-28 =======================
! 2009-07-28 - Marco Cattaneo
 - Remove obsolete file CaloFutureMoniDst_dll.cpp
 - Remove not needed includes
 - Clean up dependencies in requirements

! 2009-07-24 - Chris Jones
 - change std::map< std::string, XXX * > to 
   GaudiUtils::HashMap< const std::string, XXX * > for faster lookups.

!========================== CaloFutureMoniDst v3r8 2009-06-17 =======================
! 2009-06-05 - Olivier Deschamps
 - change _snprintf() to std::string

!========================== CaloFutureMoniDst v3r7 2009-06-03 =======================
! 2009-06-03 - Marco Cattaneo
 - Fix for windows, do not use fabs with int argument, use abs
 - Use _snprintf, not snprintf on windows
 
! 2009-05-22 - Marco Cattaneo
 - Fix invalid matrix indices in computation of "shape" in CaloFuturePhotonChecker.cpp

!========================== CaloFutureMoniDst v3r6 2009-05-08 =======================
! 2009-05-05 - Olivier Deschamps for Aurelien Martens
  - CaloFutureEFlowAlg.cpp  : add protection 
  - CaloFutureEFlowAlg.opts : change default setting 

! 2009-04-24 - Marco Cattaneo
 - Fix compilation warning for gcc43
 - Replace endreq by endmsg (obsolescent in Gaudi v21)

! 2009-04-21 - Olivier Deschamps for Dmitry Goloubkov
 - CaloFutureEMuMonitor/Checker.{cpp,h} : produce histogram (+analysis) of CaloFuturePID 

! 2009-04-20 - Olivier Deschamps for Aurelien Martens
 - CaloFutureEFlowAlg.{cpp,h,opts} : produce histogram for 'Energy-Flow' calibration
   method (Aurelien Martens)

!========================== CaloFutureMoniDst v3r5 2009-03-10 =======================
! 2009-03-06 - Olivier Deschamps 
 - set CaloFutureDQ.opt histo selection as default in CaloFutureMonitor.opts for Brunel
 - new options : CaloFutureFullMonitoring.opts (to be added to produce the whole histo set)

! 2009-02-20 - Olivier Deschamps 
 - Clean monitoring algorithms
 - add protection against missing data inputs here and there
 - new options : CaloFutureDQ.opts : selection of relevant monitoring histo for Data Quality stuff
 - CaloFutureMoniAlg : new property 'splitAreas' allow to produce histo per area (all cpp adapted)

!========================== CaloFutureMoniDst v3r4 2009-02-20 =======================
! 2009-01-20 - Marco Cattaneo
 - Migrate to LHCb::ParticlePropertySvc

!========================== CaloFutureMoniDst v3r3 2008-11-21 =======================
! 2008-11-06 - Marco Cattaneo
 - Fix for gcc 4.3

! 2008-10-17 - Olivier Deschamps
 - fix memory leak

!========================== CaloFutureMoniDst v3r2 2008-10-02 =======================
! 2008-09-23 - Marco Cattaneo
 - Fix compilation warning

! 2008-09-21 - Olivier Deschamps
 - review histogram production & prepare XXXMonitor for real data.
 - cleanup algorithm and options

! 2008-09-18 - Marco Cattaneo
 - Add #units directive to CaloFutureMonitor.opts, needed by Brunel pythonisation

!========================== CaloFutureMoniDst v3r1 2008-07-02 =======================
! 2008-06-26 - Juan PALACIOS
 - cmt/requirements
  . Increase version to v3r1
 - src/CaloFuturePhotonChecker.cpp
  . Change all Gaudi::XYZLine and Gaudi::Line for Gaudi::Math::XYZLine and
    Gaudi::Math::XYZLine respectively (adapting to Kernel/LHCbMath v3)

!========================== CaloFutureMoniDst v3r0p2 2008-04-23 ===================
! 2008-04-23 - Marco Cattaneo
 - Remove whitespace in options, needed for Python options parser

!========================== CaloFutureMoniDst v3r0p1 2007-12-03 ===================
! 2007-12-03 - Marco Cattaneo
 - Fix a compiler warning

! 2007-11-22 - Olivier Deschamps
 - fix in CaloFuturePhotonChecker.cpp

!========================== CaloFutureMoniDst v3r0 2007-10-08 ===================
! 2007-09-24 - Olivier Deschamps
 - fix against the  PatricleID constructor from int made explicit
 - fix SpdMonitor.cpp (Albert Puig)
 - remove dependency on CaloFutureAssociator in requirements
 - add dependency on DaVinciMCKernel in requirements

! 2007-08-24 - Olivier Deschamps
 - fix unchecked StatusCodes 

! 2007-08-22 - Albert Puig & Olivier Deschamps
 - new SpdMonitor algorithm

! 2007-07-25 - Konstantin Beloous & Olivier Deschamps
 - Major release 
    -  package updated for DC06
    -  First step toward an online usage of the Monitoring part 
    -  histograms production reviewed and improved
    -  some algorithm names have changed
    -  2 types of algorithms : Monitor's (relying on data only) and Checker's (relying on MC)
    - Monitors : CaloFutureDigit, CaloFutureCluster, CaloFutureHypo, ResolvedPi0 & PID's (CaloFutureClusterMatch, CaloFutureHypoMatch)
          - driven by option/CaloFutureMonitor.opts
    - Checkers : CaloFutureCluster, Photon, ResolvedPi0, CaloFuturePIDs
          - driven by option/CaloFutureChecker.opts

!========================== CaloFutureMoniDst v2r1 2005-12-08 ===================
! 2005-12-08 - Olivier Deschamps
 - CaloFuturePIDsMonitor.cpp use Track::History == 'Cnv' Tracks only 
!========================== CaloFutureMoniDst v2r0 2005-11-04 ===================
! 2005-11-04 - Olivier Deschamps
 - Adapt to new Track Event Model (TrackEvent v1r4)

  modified file :  
   src/CaloFuturePIDsMonitor.cpp
   src/CaloFuturePhotonMonitor.h/cpp
   src/CaloFutureHypoMatchMonitor.cpp
   src/CaloFutureClusterMatchMonitor.cpp

 - cmt/requirements 
   version increment to v2r0

!======================== CaloFutureMoniDst v1r2 2005-06-02 =========================
! 2005-06-02 - Marco Cattaneo
 - Adapt job options to change in phase name from BrunelMoni to Moni (for 
   monitoring without MC truth) and Check (for checking with MC truth)

========================= CaloFutureMoniDst v1r1 2005-05-13 =========================
! 2005-05-13 - Marco Cattaneo
 - Fix ambiguous call to overloaded log10 function, for Windows

! 2005-05-08 - Vanya BELYAEV
   - eliminate all associators 
   - a lot of minor cosmetic changes 
   - cmt/requirements 
    version increment to v1r1 

========================= CaloFutureMoniDst v1r0 2004-10-27 =========================
! 2004-10-27 - Vanya BELYAEV
 - tiny improvements in algorithms 


! 2004-10-25 - Vanya BELYAEV
 - the new package: the code is imported from CaloFuture/CaloFutureMonitor package 

! -----------------------------------------------------------------------------
! The END 
! -----------------------------------------------------------------------------
