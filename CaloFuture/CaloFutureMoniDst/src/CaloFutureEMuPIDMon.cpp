/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "AIDA/IAxis.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "Relations/IRelation.h"
#include "Relations/IRelationWeighted.h"

// =============================================================================

/** @class CaloFutureEMuPIDMon CaloFutureEMuPIDMon.cpp
 *
 *  Class for monitoring some CaloFuturePID quantities
 *
 *  @author Dmitry Golubkov
 *  @date   2010-03-25
 */

using Input = LHCb::ProtoParticle::Container;

class CaloFutureEMuPIDMon final
    : public Gaudi::Functional::Consumer<void( const Input& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input& ) const override;

  /// Standard constructor
  CaloFutureEMuPIDMon( const std::string& name, ISvcLocator* isvc );

private:
  Gaudi::Property<bool> m_uncut{this, "uncut", false, "true = do not apply the cuts"};

  Gaudi::Property<float> m_minPt{this, "pTmin", 250., "minimal pT of the track [MeV/c]"};

  Gaudi::Property<float> m_maxPt{this, "pTmax", 1.e10, "maximal pT of the track [MeV/c]"};

  Gaudi::Property<float> m_RichDLLe{this, "RichDLLe", -1.e10,
                                    "minimal ProtoParticle::additionalInfo::RichDLLe for electron id"};

  Gaudi::Property<float> m_maxEHcalE{this, "maxEHcalE", 1.e10,
                                     "maximal ProtoParticle::additionalInfo::CaloHcalE for electron id [MeV/c]"};

  Gaudi::Property<float> m_minPrsE{this, "minPrsE", 15.,
                                   "minimal ProtoParticle::additionalInfo::CaloPrsE for electron id [MeV/c]"};

  Gaudi::Property<unsigned long int> m_nEventMin{this, "nEventMin", 200, "minimal number of events to check"};

  Gaudi::Property<bool> m_muonLoose{this, "useIsMuonLoose", true,
                                    "use IsMuonLoose instead of IsMuon for muon selection"};

  mutable Gaudi::Accumulators::Counter<> m_nEvents{this, "nEvents"};
};

DECLARE_COMPONENT( CaloFutureEMuPIDMon )

// =============================================================================

/** Standard constructor
 *  @param name name of the algorithm instance
 *  @param isvc pointer to the Service Locator
 */
CaloFutureEMuPIDMon::CaloFutureEMuPIDMon( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"Input", LHCb::ProtoParticleLocation::Charged} ) {
  setProperty( "histoList", std::vector<std::string>{{"All"}} ).ignore();
  setProperty( "removeFromHistoList", std::vector<std::string>() ).ignore();
  setProperty( "SaturationBin1D", false ).ignore();
  setProperty( "SaturationBin2D", false ).ignore();
}

// =============================================================================

/**
 * initialize the algorithm, define the histograms
 */
StatusCode CaloFutureEMuPIDMon::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;

  hBook1( "eop", "E/p", 0.05, 2.55, 50 );

  // for the time being let's not split MIP plots into A and C sides
  bool old_m_splitSides = m_splitSides;
  m_splitSides          = false;

  hBook1( "prsem", "E Prs", 0.5, 25.5, 25 );
  hBook1( "ecalem", "E Ecal", 40., 2040., 50 );
  hBook1( "hcalem", "E Hcal", 100., 5100., 50 );

  m_splitSides = old_m_splitSides;

  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void CaloFutureEMuPIDMon::operator()( const Input& particles ) const {

  if ( !produceHistos() ) return; // StatusCode::SUCCESS;
  if ( m_counterStat->isQuiet() ) ++m_nEvents;

  // -------------------------------------------------------------------
  // Track loop
  // -------------------------------------------------------------------

  for ( const auto& proto : particles ) {
    if ( 0 == proto ) continue;
    const auto track = proto->track();
    if ( 0 == track ) continue;
    if ( track->type() < LHCb::Track::Types::Long ) continue;
    if ( ( track->pt() < m_minPt || track->pt() > m_maxPt ) && !m_uncut ) continue;

    const bool inprs  = ( proto->info( LHCb::ProtoParticle::additionalInfo::InAccPrs, double( false ) ) != 0 );
    const bool inecal = ( proto->info( LHCb::ProtoParticle::additionalInfo::InAccEcal, double( false ) ) != 0 );
    const bool inhcal = ( proto->info( LHCb::ProtoParticle::additionalInfo::InAccHcal, double( false ) ) != 0 );

    const float prse  = (float)proto->info( LHCb::ProtoParticle::additionalInfo::CaloPrsE, -1 * Gaudi::Units::GeV );
    const float ecale = (float)proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalE, -1 * Gaudi::Units::GeV );
    const float hcale = (float)proto->info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, -1 * Gaudi::Units::GeV );

    // -----------------------------------------------------------------
    // electron histograms
    // e.g.:  "pt>0.5&&inecal==1&&(hcale<=0||hcale>0&&inhcal==1&&hcale<1000.)&&rdlle>4"
    //   or:  "pt>250&&inecal==1&&prse>15"
    // -----------------------------------------------------------------

    do {
      if ( !inecal ) break; // ----
      float rdlle = (float)proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLe, -9999. );
      if ( rdlle < m_RichDLLe && !m_uncut ) break;            // ----
      if ( inhcal && hcale > m_maxEHcalE && !m_uncut ) break; // ----
      if ( prse < m_minPrsE && !m_uncut ) break;              // ----

      const auto            hypos      = proto->calo();
      const LHCb::CaloHypo* m_electron = NULL;

      for ( const auto& hypo : hypos ) {
        if ( LHCb::CaloHypo::Hypothesis::EmCharged == hypo->hypothesis() ) m_electron = hypo;
      }

      // E/p histogram
      if ( m_electron ) {
        double eoverp = m_electron->position()->e() / track->p();
        if ( eoverp > 0. ) {
          const auto clust = *( m_electron->clusters().begin() );
          if ( clust ) {
            const auto cell = clust->seed();
            hFill1( cell, "eop", eoverp );
          }
        }
      }
    } while ( false );

    // -----------------------------------------------------------------
    // muon histograms
    // -----------------------------------------------------------------

    bool ismuon =
        ( 0 != ( proto->muonPID() ? ( m_muonLoose ? proto->muonPID()->IsMuonLoose() : proto->muonPID()->IsMuon() )
                                  : false ) );

    if ( ismuon || m_uncut ) {
      if ( inprs ) hFill1( "prsem", prse );
      if ( inecal ) hFill1( "ecalem", ecale );
      if ( inhcal ) hFill1( "hcalem", hcale );
    }
  }
  return; // StatusCode::SUCCESS;
}
