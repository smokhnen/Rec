/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloFutureMoniAlg.h"
#include "Event/CaloClusters_v2.h"
#include "GaudiAlg/Consumer.h"

// =============================================================================

/** @class CaloFutureClusterMonitor CaloFutureClusterMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloFutureCluster" containers.
 *  The algorithm produces 8 histograms:
 *
 *  <ol>
 *  <li> @p CaloCluster multiplicity                    </li>
 *  <li> @p CaloCluster size (number of cells)          </li>
 *  <li> @p CaloCluster energy distribution             </li>
 *  <li> @p CaloCluster transverse energy distribution  </li>
 *  <li> @p CaloCluster x-distribution                  </li>
 *  <li> @p CaloCluster y-distribution                  </li>
 *  <li> @p CaloCluster x vs y-distribution             </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

// using Input = LHCb::CaloCluster::Container;
using Input = LHCb::Event::Calo::v2::Clusters;

class CaloFutureClusterMonitor final
    : public Gaudi::Functional::Consumer<void( const Input& ),
                                         Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
public:
  /// standard algorithm initialization
  StatusCode initialize() override;
  void       operator()( const Input& ) const override;

  CaloFutureClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );
};

// =============================================================================

DECLARE_COMPONENT( CaloFutureClusterMonitor )

// =============================================================================

CaloFutureClusterMonitor::CaloFutureClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name )} ) {}

// =============================================================================

/// standard algorithm initialization
StatusCode CaloFutureClusterMonitor::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  hBook1( "nClusters", "# of Clusters " + inputLocation(), m_multMin, m_multMax, m_multBin );
  hBook1( "nDigits", "Cluster digit multiplicity " + inputLocation(), m_sizeMin, m_sizeMax, m_sizeBin );
  hBook1( "energy", "Cluster Energy " + inputLocation(), m_energyMin, m_energyMax, m_energyBin );
  hBook1( "ET", "Cluster Et " + inputLocation(), m_etMin, m_etMax, m_etBin );
  hBook1( "x", "Cluster x " + inputLocation(), m_xMin, m_xMax, m_xBin );
  hBook1( "y", "Cluster y " + inputLocation(), m_yMin, m_yMax, m_yBin );
  hBook2( "x-y", "Cluster barycenter position x vs y " + inputLocation(), m_xMin, m_xMax, m_xBin, m_yMin, m_yMax,
          m_yBin );
  hBook2( "x-y-eW", "Energy-weighted cluster barycenter position x vs y " + inputLocation(), m_xMin, m_xMax, m_xBin,
          m_yMin, m_yMax, m_yBin );
  hBook1( "nDigits_forE", "Cluster digit used for Energy multiplicity " + inputLocation(), m_sizeMin, m_sizeMax,
          m_sizeBin );
  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

// StatusCode CaloFutureClusterMonitor::execute(){
void CaloFutureClusterMonitor::operator()( const Input& clusters ) const {

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << " Producing histo " << produceHistos() << endmsg;
  // produce histos ?
  if ( !produceHistos() ) return;

  if ( clusters.empty() ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Found empty cluster in " << inputLocation() << endmsg;
    return;
  }

  // fill multiplicity histogram
  initCounters();

  for ( const auto& cluster : clusters ) {
    const double e  = cluster.energy();
    const double x  = cluster.position().x();
    const double y  = cluster.position().y();
    const double z  = cluster.position().z();
    const double et = e * sqrt( x * x + y * y ) / sqrt( x * x + y * y + z * z );
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    const auto id = cluster.seed();
    count( id );
    hFill1( id, "nDigits", cluster.entries().size() );
    hFill1( id, "energy", e );
    hFill1( id, "ET", et );
    hFill1( id, "x", x );
    hFill1( id, "y", y );
    hFill2( id, "x-y", x, y );
    hFill2( id, "x-y-eW", x, y, e );

    const auto& entries = cluster.entries();
    int         iuse    = std::count_if( entries.begin(), entries.end(), []( const auto& e ) {
      return e.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
    } );
    hFill1( id, "nDigits_forE", iuse );
    if ( doHisto( "position2D" ) )
      fillCaloFuture2D( "position2D", id, 1., "Cluster position 2Dview " + inputLocation() );
    if ( doHisto( "position2D-eW" ) )
      fillCaloFuture2D( "position2D-eW", id, e, "Cluster Energy 2Dview " + inputLocation() );
  }
  // fill counter
  fillFutureCounters( "nClusters" );
}
