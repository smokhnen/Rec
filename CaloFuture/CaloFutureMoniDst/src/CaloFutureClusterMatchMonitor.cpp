/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureMoniAlg.h"
#include "CaloFutureMoniUtils.h" // Local
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/Track.h"
#include "GaudiAlg/Consumer.h"
#include "Relations/RelationWeighted2D.h"

// =============================================================================

/** @class CaloFutureClusterMatchMonitor CaloFutureClusterMatchMonitor.cpp
 *
 *  The algorithm for trivial monitoring of matching of
 *  "CaloFutureClusters" with Tracks.
 *  It produces 5 histograms:
 *
 *  <ol>
 *  <li> Total Link              distribution               </li>
 *  <li> Link multiplicity       distribution               </li>
 *  <li> Minimal Weight          distribution               </li>
 *  <li> Maximal Weight          distribution               </li>
 *  <li>         Weight          distribution               </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see     Algorithm
 *  @see    IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

namespace LHCb::Calo::Monitors {

  using Input    = LHCb::RelationWeighted2D<LHCb::CaloCellID, LHCb::Track, float>;
  using Clusters = LHCb::Event::Calo::Clusters;

  class ClusterMatch final
      : public Gaudi::Functional::Consumer<void( const Input&, const Clusters& ),
                                           Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>> {
  public:
    ClusterMatch( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;
    void       operator()( const Input&, const Clusters& ) const override;

  private:
    mutable Gaudi::Accumulators::StatCounter<> m_relations{this, "RelationsTable"};
    mutable Gaudi::Accumulators::StatCounter<> m_clusters{this, "Clusters"};
  };

  // =============================================================================

  DECLARE_COMPONENT_WITH_ID( ClusterMatch, "CaloFutureClusterMatchMonitor" )

  // =============================================================================

  ClusterMatch::ClusterMatch( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name,
                 pSvcLocator,
                 {KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )},
                  KeyValue{"InputClusters", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name )}}} {}

  // =============================================================================

  StatusCode ClusterMatch::initialize() {
    return Consumer::initialize().andThen( [&] {
      hBook1( "1", "log10(#Links+1) '" + inputLocation() + "'", 0, 4, 100 );
      hBook1( "2", "Tracks per cluster", 0, 25, 25 );
      hBook1( "3", "Minimal weight", 0, 100, 200 );
      hBook1( "4", "Maximal weight", 0, 1000, 200 );
      hBook1( "5", "Weights", 0, 1000, 500 );
      if ( m_split ) {
        Warning( "No area spliting allowed for ClusterMatch" ).ignore();
        m_split = false;
      }
    } );
  }

  // =============================================================================

  void ClusterMatch::operator()( const Input& table, const Clusters& clusters ) const {

    // produce histos ?
    if ( !produceHistos() ) return;

    // total number of links
    hFill1( "1", log10( table.relations().size() + 1. ) );

    // loop over all clusters
    for ( const auto& cluster : clusters ) {
      const auto& range = table.relations( cluster.seed() );
      // number of related tracks
      hFill1( "2", range.size() );
      if ( range.empty() ) continue;
      // minimal weight
      hFill1( "3", range.front().weight() );
      // maximal weight
      hFill1( "4", range.back().weight() );
      // all weights
      for ( const auto& relation : range ) { hFill1( "5", relation.weight() ); }
    } // end of loop over clusters

    m_relations += table.relations().size();
    m_clusters += clusters.size();
  }
} // namespace LHCb::Calo::Monitors
