/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureMoniUtils.h"
#include "Event/ODIN.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "IFutureCounterLevel.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

//------------------------------------------------------------------------------
// Implementation file for class : CaloFutureElectronNtp
//
// 2009-12-11 : Olivier Deschamps
//------------------------------------------------------------------------------

// List of Consumers dependencies
namespace {
  using ODIN     = LHCb::ODIN;
  using Protos   = LHCb::ProtoParticles;
  using Vertices = LHCb::RecVertices;
} // namespace

//==============================================================================

/** @class CaloFutureElectronNtp CaloFutureElectronNtp.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloFutureElectronNtp final
    : public Gaudi::Functional::Consumer<void( const ODIN&, const Protos&, const Vertices& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
public:
  CaloFutureElectronNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const ODIN&, const Protos&, const Vertices& ) const override;

private:
  DeCalorimeter* m_calo = nullptr;
  std::string    m_vertLoc;

  mutable std::mutex                            m_badtoolmutex;
  ToolHandle<IFutureCounterLevel>               m_counterStat{"FutureCounterLevel"};
  ToolHandle<IEventTimeDecoder>                 m_odin{"OdinTimeDecoder/OdinDecoder", this};
  ToolHandle<LHCb::Calo::Interfaces::IElectron> m_caloElectron{"CaloFutureElectron", this};
  ToolHandle<ITrackExtrapolator>                m_extrapolator{"TrackRungeKuttaExtrapolator/Extrapolator", this};

  Gaudi::Property<std::pair<double, double>> m_e{this, "EFilter", {0., 99999999}};
  Gaudi::Property<std::pair<double, double>> m_et{this, "EtFilter", {200., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_eop{this, "EoPFilter", {0., 2.5}};

  Gaudi::Property<bool> m_pairing{this, "ElectronPairing", true};
  Gaudi::Property<bool> m_histo{this, "Histo", true};
  Gaudi::Property<bool> m_tuple{this, "Tuple", true};
  Gaudi::Property<bool> m_trend{this, "Trend", false};
  Gaudi::Property<bool> m_usePV3D{this, "UsePV3D", false};
  Gaudi::Property<bool> m_splitFEBs{this, "splitFEBs", false};
  Gaudi::Property<bool> m_splitE{this, "splitE", false};

  Gaudi::Property<std::vector<int>> m_tracks{
      this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

  mutable Gaudi::Accumulators::Counter<> m_nProtoElectrons{this, "proto electron"};
  mutable Gaudi::Accumulators::Counter<> m_nSelectedElectrons{this, "Selected electron"};
  mutable Gaudi::Accumulators::Counter<> m_nSelectedDiElectrons{this, "Selected (di)electron"};

  bool   set_and_validate( const LHCb::Calo::Interfaces::IElectron& caloElectron, const LHCb::ProtoParticle& proto,
                           bool count = false ) const;
  double invar_mass_squared( const LHCb::Track* t1, const LHCb::Track* t2 ) const;
  void   fillH( double eOp, Gaudi::LorentzVector t, LHCb::CaloCellID id, std::string hat = "" ) const;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloFutureElectronNtp )

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloFutureElectronNtp::CaloFutureElectronNtp( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default},
                 KeyValue{"InputContainer", LHCb::ProtoParticleLocation::Charged},
                 KeyValue{"VertexLoc", LHCb::RecVertexLocation::Primary}} ) {}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloFutureElectronNtp::initialize() {
  StatusCode sc = Consumer::initialize();
  if ( sc.isFailure() ) return sc;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  if ( !m_tracks.empty() )
    info() << "Will only look at track type(s) = " << m_tracks.value() << endmsg;
  else
    info() << "Will look at any track type" << endmsg;

  // Get, retrieve, configure tools
  m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );

  // set vertex location
  m_vertLoc = inputLocation<2>(); // <-- Careful with the index, 'VertexLoc' is 3rd.
  // use fallback if null (forced by user)
  if ( m_vertLoc.empty() ) m_vertLoc = m_usePV3D ? LHCb::RecVertexLocation::Velo3D : LHCb::RecVertexLocation::Primary;
  updateHandleLocation( *this, "VertexLoc", m_vertLoc );

  return StatusCode::SUCCESS;
}

//==============================================================================

// Warning, not actually a const member function, as it modifies the internal
// state of m_caloElectron. Use with care.
// Note, extra flag count to count at the specific place. Because this method
// is used on both first-electron, and second-electron, the counting should only
// be enabled in the first one only.
bool CaloFutureElectronNtp::set_and_validate( const LHCb::Calo::Interfaces::IElectron& caloElectron,
                                              const LHCb::ProtoParticle& proto, bool count ) const {
  // abort if fail to set
  auto hypoPair = caloElectron.getElectronBrem( proto );
  if ( !hypoPair ) return false; // to keep the counter
  // counting
  if ( count && m_counterStat->isQuiet() ) ++m_nProtoElectrons;
  // abort if no hypo obtained
  const auto hypo = caloElectron.electron( proto );
  if ( !hypo ) return false;
  // abort if null track
  const auto track = proto.track();
  if ( !track ) return false;
  // abort if not matching track whitelist
  if ( !m_tracks.empty() ) {
    const auto ttype = proto.track()->type();
    if ( !std::any_of( m_tracks.begin(), m_tracks.end(), [ttype]( int itype ) { return itype == ttype; } ) ) {
      return false;
    }
  }
  // Abort if poor energy
  LHCb::Calo::Momentum mmt( hypo );
  const double         e  = mmt.e();
  const double         et = mmt.pt();
  if ( !inRange( m_et, et ) ) return false;
  if ( !inRange( m_e, e ) ) return false;
  const double eOp = caloElectron.eOverP( proto );
  if ( !inRange( m_eop, eOp ) ) return false;
  // finally
  if ( count && m_counterStat->isQuiet() ) ++m_nSelectedElectrons;
  return true;
}

// Calculated the squared of the invar mass of two tracks
// The result can be negative.
double CaloFutureElectronNtp::invar_mass_squared( const LHCb::Track* t1, const LHCb::Track* t2 ) const {
  auto st1 = t1->firstState();
  auto st2 = t2->firstState();
  auto sc  = m_extrapolator->propagate( st1, 0. );
  if ( sc.isFailure() ) Warning( "Propagation 1 failed" ).ignore();
  sc = m_extrapolator->propagate( st2, 0. );
  if ( sc.isFailure() ) Warning( "Propagation 2 failed" ).ignore();
  const auto p1 = st1.momentum();
  const auto p2 = st2.momentum();
  double     m2 = p1.R() * p2.R();
  m2 -= p1.X() * p2.X();
  m2 -= p1.Y() * p2.Y();
  m2 -= p1.Z() * p2.Z();
  m2 *= 2;
  return m2;
}

//==============================================================================
// Main execution
//==============================================================================

void CaloFutureElectronNtp::operator()( const ODIN& odin, const Protos& protos, const Vertices& verts ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // GET ODIN INFO
  m_odin->getTime();
  ulonglong evt = odin.eventNumber();
  int       run = odin.runNumber();
  int       tty = odin.triggerType();

  // Loop over each protoparticle
  for ( auto p = protos.begin(); protos.end() != p; ++p ) {
    const auto proto = *p;
    if ( !proto ) continue;
    if ( !set_and_validate( *m_caloElectron, *proto, true ) ) continue;

    // Retrieve info for tuple
    const auto track1       = proto->track();
    const auto hypo         = m_caloElectron->electron( *proto );
    const auto track1mmt    = momentum( track1 );
    const auto caloCluster  = firstCluster( hypo );
    const auto caloCellId   = caloCluster->seed();
    const auto caloState    = m_caloElectron->caloState( *proto );
    const auto caloStateMmt = momentum( caloState );
    const auto brem         = m_caloElectron->bremstrahlung( *proto );
    const auto bremCellId   = firstCluster( brem )->seed();
    const auto bremMmt      = momentum( m_caloElectron->bremMomentum( *proto ) );
    const auto eOp          = m_caloElectron->eOverP( *proto );
    const auto e            = LHCb::Calo::Momentum( hypo ).e();

    // dielectron filter
    double mas = 999999.;
    if ( m_pairing ) {
      for ( auto pp = p + 1; protos.end() != pp; ++pp ) {
        const auto proto2 = *pp;
        if ( !proto2 ) continue;
        if ( !set_and_validate( *m_caloElectron, *proto2 ) ) continue;
        // abort if same pair (shouldn't happen)
        const auto hypo2 = m_caloElectron->electron( *proto2 );
        if ( hypo == hypo2 ) continue;
        // Need opposite charge
        const auto t1 = proto->track();
        const auto t2 = proto2->track();
        if ( -1 != t1->charge() * t2->charge() ) continue;
        // compute mass
        const auto m2 = invar_mass_squared( t1, t2 );
        if ( m2 > 0 ) {
          const auto m = sqrt( m2 );
          if ( m < mas ) mas = m;
        }
      }
    }
    if ( mas > 0 && mas < 100 && m_counterStat->isQuiet() ) ++m_nSelectedDiElectrons;

    // proto info
    if ( m_tuple ) {
      auto ntp = nTuple( 500, "e_tupling", CLID_ColumnWiseTuple );
      ntp->column( "TrackMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 9999. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "ElecMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 9999. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "BremMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 9999. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "TrajectoryL", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 9999. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "VeloCharge", proto->info( LHCb::ProtoParticle::additionalInfo::VeloCharge, -1. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "DLLe", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLe, 0. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "RichDLLe", proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLe, 0. ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // hypo info
      ntp->column( "EoP", eOp ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "HypoE", e ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "HypoR", position3d( hypo ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "HypoTheta", position3d( hypo ).theta() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // track info
      ntp->column( "TrackP", track1mmt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "TrackR", position3d( caloState ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "caloState", caloStateMmt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "incidence", caloStateMmt.theta() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "incidenceX", atan2( caloStateMmt.X(), caloStateMmt.Z() ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "incidenceY", atan2( caloStateMmt.Y(), caloStateMmt.Z() ) )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "trackType", track1->type() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "trackProb", track1->probChi2() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // cluster info
      ntp->column( "id", caloCellId.index() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "ClusterE", caloCluster->e() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "ClusterR", position3d( caloCluster ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // brem info
      ntp->column( "BremId", bremCellId.index() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "BremP", bremMmt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // odin info
      ntp->column( "run", run ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "event", (double)evt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->column( "triggertype", tty ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      if ( m_pairing ) ntp->column( "MinMee", mas ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      ntp->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    // vertices
    const auto nVert = verts.size();
    if ( m_counterStat->isQuiet() ) counter( "#PV=" + Gaudi::Utils::toString( nVert ) + " [" + m_vertLoc + "]" ) += 1;

    if ( m_trend ) {
      std::string sNpv = "PV" + Gaudi::Utils::toString( nVert ) + "/";
      std::string sRun = "r" + Gaudi::Utils::toString( run ) + "/";
      std::string base = "Trend/";
      plot1D( eOp, base + "allPV/allRun/eOp", "e/p spectrum for all run & allPV", 0., 2.5, 250 );
      plot1D( eOp, base + "allPV/" + sRun + "eOp", "e/p spectrum for run = " + sRun, 0., 2.5, 250 );
      plot1D( eOp, base + sNpv + sRun + "eOp", "e/p spectrum for PV=" + sNpv + " (run = " + sRun + ")", 0., 2.5, 250 );
      plot1D( eOp, base + sNpv + "allRun/eOp", "e/p spectrum for PV=" + sNpv + " (all run)", 0., 2.5, 250 );
    }
  }
}
//=============================================================================

void CaloFutureElectronNtp::fillH( double eOp, Gaudi::LorentzVector t, LHCb::CaloCellID id, std::string hat ) const {
  if ( !m_histo ) return;
  std::string zone = id.areaName();
  plot1D( eOp, hat + "all/eOp", "all/eOp", 0., 3., 300 );
  plot1D( eOp, hat + zone + "/eOp", zone + "/eOp", 0., 3., 300 );
  if ( m_splitFEBs ) {
    std::ostringstream sid;
    int                feb = m_calo->cardNumber( id );
    sid << hat << "crate" << format( "%02i", m_calo->cardCrate( feb ) ) << "/"
        << "feb" << format( "%02i", m_calo->cardSlot( feb ) ) << "/"
        << Gaudi::Utils::toString( m_calo->cardColumn( id ) + nColCaloCard * m_calo->cardRow( id ) );
    plot1D( eOp, sid.str(), sid.str(), 0., 3., 300 );
  }
  if ( m_splitE && t.P() < 200. * Gaudi::Units::GeV ) { // put a limit at 200 GeV
    int                ebin = ( t.P() / 5000. ) * 5;    // 1 GeV binning
    std::ostringstream sid;
    sid << "/E_" << ebin << "/eOp";
    if ( m_counterStat->isQuiet() ) counter( sid.str() + "[<p>]" ) += t.P();
    plot1D( eOp, hat + zone + sid.str(), zone + sid.str(), 0., 3., 300 );
    plot1D( eOp, hat + "all" + sid.str(), "all" + sid.str(), 0., 3., 300 );
  }
}
