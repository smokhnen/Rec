/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureMoniAlg.h"
#include "CaloFutureUtils/CaloFutureParticle.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloHypos_v2.h"
#include "Event/Particle.h"
#include "GaudiAlg/Consumer.h"
#include <vector>

// =============================================================================

/** @class CaloFuturePi0Monitor CaloFuturePi0Monitor.cpp
 *
 *  Simple pi0 monitoring algorithm
 *
 *  @see   CaloFutureMoniAlg
 *  @see GaudiHistoAlg
 *  @see GaudiAlgorithm
 *  @see      Algorithm
 *  @see     IAlgorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::Event::Calo::Hypotheses;

class CaloFuturePi0Monitor final
    : public Gaudi::Functional::Consumer<void( const Input&, const DeCalorimeter& ),
                                         LHCb::DetDesc::usesBaseAndConditions<CaloFutureMoniAlg, DeCalorimeter>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const DeCalorimeter& ) const override;

  CaloFuturePi0Monitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  bool valid_photon( const LHCb::Event::Calo::Hypotheses::const_reference g ) const;

  Gaudi::Property<float> m_ptPhoton{this, "PhotonPtFilter", 250 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_ptMaxPhoton{this, "PhotonMaxPtFilter", 0};
  Gaudi::Property<float> m_isol{this, "IsolationFilter", 4};
  Gaudi::Property<float> m_prsPhoton{this, "PhotonPrsFilterMin", 10 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_yCut{this, "RejectedYBand", 300};
  Gaudi::Property<bool>  m_conv{this, "AllowConverted", false};
};

// =============================================================================

DECLARE_COMPONENT( CaloFuturePi0Monitor )

// =============================================================================

CaloFuturePi0Monitor::CaloFuturePi0Monitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Photons" )},
                KeyValue{"DetectorLocation", DeCalorimeterLocation::Ecal}}} {
  m_multMax = 150;
}

// =============================================================================

StatusCode CaloFuturePi0Monitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    hBook1( "1", "(gg) multiplicity " + inputLocation(), m_multMin, m_multMax, m_multBin );
    hBook1( "2", "(gg) energy " + inputLocation(), m_energyMin, m_energyMax, m_energyBin );
    hBook1( "3", "(gg) et     " + inputLocation(), m_etMin, m_etMax, m_etBin );
    hBook1( "4", "(gg) mass   " + inputLocation(), m_massMin, m_massMax, m_massBin );
    hBook1( "5", "(gg) combinatorial background" + inputLocation(), m_massMin, m_massMax, m_massBin );
    hBook1( "6", "bkg-substracted (gg) mass   " + inputLocation(), m_massMin, m_massMax, m_massBin );
    hBook1( "7", "(gg) mass  for |y|-gamma > " + Gaudi::Utils::toString( m_yCut ) + " " + inputLocation(), m_massMin,
            m_massMax, m_massBin );
    hBook2( "8", "(gg) mass per cell  " + inputLocation(), 0, 6016, 6016, m_massMin, m_massMax, m_massBin );

    // set mass window to histo range
    m_massFilterMin = m_massMin;
    m_massFilterMax = m_massMax;
  } );
}

// =============================================================================

void CaloFuturePi0Monitor::operator()( const Input& photons, const DeCalorimeter& det ) const {

  if ( !produceHistos() ) return;
  if ( photons.empty() ) return;

  // Params for lookup later down the loop
  const auto& cells = det.cellParams();

  // loop over the first photon
  initCounters();
  for ( auto g1 = photons.begin(); photons.end() != g1; ++g1 ) {
    if ( !valid_photon( *g1 ) ) continue;
    LHCb::Calo::Momentum momentum1( g1->clusters() );
    Gaudi::LorentzVector v1( momentum1.momentum() );

    // loop over the second photon
    for ( auto g2 = std::next( g1 ); photons.end() != g2; ++g2 ) {
      if ( !valid_photon( *g2 ) ) continue;
      LHCb::Calo::Momentum momentum2( g2->clusters() );
      if ( std::max( momentum1.pt(), momentum2.pt() ) < m_ptMaxPhoton ) continue;
      Gaudi::LorentzVector v2( momentum2.momentum() );

      // background shape from (x,y)->(-x,-y) symmetrized g2
      Gaudi::LorentzVector v2Sym( v2 );
      v2Sym.SetPx( -v2.Px() );
      v2Sym.SetPy( -v2.Py() );
      Gaudi::LorentzVector bkg( v1 + v2Sym );
      Gaudi::XYZPoint      p2Sym( -g2->position().x(), -g2->position().y(), g2->position().z() );
      bool                 isBkg = ( bkg.e() > m_eFilter && bkg.pt() > m_etFilter && bkg.mass() > m_massFilterMin &&
                     bkg.mass() < m_massFilterMax );

      // check pi0
      Gaudi::LorentzVector pi0( v1 + v2 );
      bool                 isPi0 = ( pi0.e() > m_eFilter && pi0.pt() > m_etFilter && pi0.mass() > m_massFilterMin &&
                     pi0.mass() < m_massFilterMax );

      if ( !isPi0 && !isBkg ) continue;

      // Get cellIDs
      auto id1 = g1->cellID();
      auto id2 = g2->cellID();

      // define pi0 area
      const auto id = ( id1.area() == id2.area() ) ? id1 : LHCb::CaloCellID();

      // isolation criteria
      Gaudi::XYZPoint p1      = g1->position();
      Gaudi::XYZPoint p2      = g2->position();
      const auto      vec     = p2 - p1;
      const auto      vecSym  = p2Sym - p1;
      const auto      cSize   = std::max( det.cellSize( id1 ), det.cellSize( id2 ) );
      const auto      isol    = ( cSize > 0 ) ? vec.Rho() / cSize : 0;
      const auto      isolSym = ( cSize > 0 ) ? vecSym.Rho() / cSize : 0;
      const auto      y1      = det.cellCenter( id1 ).Y();
      const auto      y2      = det.cellCenter( id2 ).Y();

      if ( isPi0 && isol > m_isol ) {
        count( id );
        hFill1( id, "2", pi0.e() );
        hFill1( id, "3", pi0.pt() );
        hFill1( id, "4", pi0.mass() );
        hFill1( id, "6", pi0.mass(), 1. );
        if ( fabs( y1 ) > m_yCut && fabs( y2 ) > m_yCut ) hFill1( "7", pi0.mass(), 1. );
        int index = cells.index( id );
        hFill2( "8", index, pi0.mass() );
      }
      if ( isBkg && isolSym > m_isol ) {
        hFill1( id, "5", bkg.mass() );
        hFill1( id, "6", bkg.mass(), -1. );
      }
    }
  }
  fillFutureCounters( "1" );
}

// =============================================================================

bool CaloFuturePi0Monitor::valid_photon( const LHCb::Event::Calo::Hypotheses::const_reference g ) const {
  // Return True if this photon is valid to continue the computation
  return LHCb::Calo::Momentum{g.clusters()}.pt() >= m_ptPhoton;
}
