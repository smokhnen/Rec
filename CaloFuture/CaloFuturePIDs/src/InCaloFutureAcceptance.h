/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloFutureTrackTool.h"
#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IInAcceptance.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// ============================================================================

/** @class InCaloFutureAcceptance InCaloFutureAcceptance.h
 *
 *  The general tool to determine if the reconstructed charged track
 *  "is in Calorimeter acceptance"
 *
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
namespace LHCb::Calo {
  class InAcceptance : public extends<TrackTool, IInAcceptance> {
  public:
    using extends::extends;
    /// initialization @see IAlgTool
    StatusCode initialize() override;

    /** check the track is in acceptance of given calorimeter
     *  @see IInAcceptance
     *  @param  track track to be checked
     *  @return true if the track is in acceptance
     */
    bool inAcceptance( const LHCb::Track* track ) const override;

  protected:
    /// check if the point "is in acceptance"
    bool ok( const Gaudi::XYZPoint& point ) const;
    /// get the plane
    const Gaudi::Plane3D& plane() const { return m_plane; }

  private:
    /// use fiducial volume or real volume ?
    Gaudi::Property<bool> m_fiducial{this, "UseFiducial", true};
    LHCb::State::Location m_loc;
    Gaudi::Plane3D        m_plane;
  };

  // ============================================================================
  /// check if the point "is in acceptance"
  // ============================================================================

  inline bool InAcceptance::ok( const Gaudi::XYZPoint& point ) const {
    const CellParam* cell = calo()->Cell_( point );
    if ( !cell || !cell->valid() ) { return false; }
    // check for fiducial volume ?
    if ( !m_fiducial ) { return true; }
    // check for neighbours
    const auto& neighbours = cell->neighbors();
    // regular cell: >= 8 valid neighbours
    if ( 8 <= neighbours.size() ) { return true; }
    // incomplete neibours: border of 2 zones ?
    return std::any_of( std::next( neighbours.begin() ), neighbours.end(),
                        [area = cell->cellID().area()]( const auto& n ) { return n.area() != area; } );
  }
} // namespace LHCb::Calo

// ===========================================================================
