/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureTrackMatchAlg.h"

// =============================================================================
/** @class BremMatchAlgFuture
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// =============================================================================
namespace LHCb::Calo {
  using TABLE           = RelationWeighted2D<LHCb::CaloCellID, Track, float>;
  using CALOFUTURETYPES = LHCb::Event::Calo::Hypotheses;

  struct BremMatchAlg final : TrackMatchAlg<TABLE, CALOFUTURETYPES> {
    static_assert( std::is_base_of_v<CaloFuture2Track::IHypothesis2TrackTable2D, TABLE>,
                   "TABLE must inherit from IHypoTrTable2D" );

    BremMatchAlg( const std::string& name, ISvcLocator* pSvc ) : TrackMatchAlg<TABLE, CALOFUTURETYPES>( name, pSvc ) {
      updateHandleLocation( *this, "Calos", CaloFutureAlgUtils::CaloFutureHypoLocation( "Photons" ) );
      updateHandleLocation( *this, "Output", CaloFutureAlgUtils::CaloFutureIdLocation( "BremMatch" ) );
      updateHandleLocation( *this, "Filter", CaloFutureAlgUtils::CaloFutureIdLocation( "InBrem" ) );

      setProperty( "Tool", "CaloFutureBremMatch/BremMatchFuture" ).ignore();
      setProperty( "Threshold", "10000" ).ignore();
      // track types:
      setProperty( "AcceptedType",
                   Gaudi::Utils::toString<int>( Track::Types::Velo, Track::Types::Long, Track::Types::Upstream ) )
          .ignore();
      setProperty( "TableSize", "1000" ).ignore();
    }
  };
  DECLARE_COMPONENT_WITH_ID( BremMatchAlg, "BremMatchAlgFuture" )
} // namespace LHCb::Calo
// =============================================================================
