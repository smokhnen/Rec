/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/CaloFutureMatch2D.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "SelectiveMatchUtils.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class SelectiveBremMatchAlg SelectiveBremMatchAlg.cpp
 *
 *  Matches tracks with local clusters in and around calo cell
 *  corresponding to track linear extrapolation from before magnet for brem recovery.
 *  Sufficient since resolution is about cell size / sqrt(12).
 *
 *  Returns per cluster a matching chi2 and a test statistic with the higher the value,
 *  the more 'first-state like' and the lower, the more 'last-state like', as real
 *  brem tends to come from the first state.
 *
 *  In addition calculates track-based brem energy with ecal digit search based on track extrapolations.
 *
 *  @date   2020-11
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveBremMatchAlg
 *  It takes inspiration from SelectiveTrackMatchAlg
 *
 *  @date   2020-11
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using CaloObjects = Event::Calo::Hypotheses;
  using Digits      = Event::Calo::Digits;
  using MatchTable  = RelationWeighted2D<CaloCellID, Track, float>;
  using DeltaXTable = RelationWeighted2D<CaloCellID, Track, float>;
  using EnergyTable = Relation1D<Track, float>;
  using Filter      = Relation1D<Track, bool>;

  using namespace SelectiveMatchUtils;

  // local helper objects / functions for class
  namespace {
    // scan along x from first to last state for cell IDs
    bool scanXRange( CaloCellID::Vector& cellids, Gaudi::XYZPoint& scanpos, const double dx, const double stepsize,
                     const DeCalorimeter& calo ) {
      double xpos   = scanpos.x();
      int    sdx    = ( dx > 0. ) - ( dx < 0. );
      int    nscans = int( std::ceil( std::fabs( dx ) / stepsize ) );
      for ( int i = 0; i <= nscans; i++ ) {
        scanpos.SetX( xpos + sdx * i * stepsize );
        const auto cpar = calo.Cell_( scanpos );
        if ( cpar && cpar->valid() ) cellids.push_back( cpar->cellID() );
      }
      // remove duplicates if there are any
      std::sort( cellids.begin(), cellids.end() );
      cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
      return true;
    }

    // to get additional cell IDs along state slopes for first state (most likely brem origin state)
    bool getTrackBasedEnergyCells( CaloCellID::Vector& cellids, const State& state, const DeCalorimeter& calo,
                                   const std::vector<double>& zscans ) {
      Gaudi::XYZPoint pos;
      for ( const double zscan : zscans ) {
        // y-tilt can be ignored for this scan
        pos             = state.position() + ( zscan - state.position().z() ) * state.slopes();
        const auto cpar = calo.Cell_( pos );
        if ( cpar && cpar->valid() ) cellids.push_back( cpar->cellID() );
      }
      // remove duplicates if there are any
      std::sort( cellids.begin(), cellids.end() );
      cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
      return true;
    }
  } // namespace

  class SelectiveBremMatchAlg
      : public Gaudi::Functional::MultiTransformer<std::tuple<MatchTable, DeltaXTable, EnergyTable>(
                                                       const DeCalorimeter&, const CaloObjects&, const Digits&,
                                                       const Track::Range&, const Filter&, const cellSizeCovariances& ),
                                                   DetDesc::usesConditions<DeCalorimeter, cellSizeCovariances>> {
  public:
    // standard constructor
    SelectiveBremMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    std::tuple<MatchTable, DeltaXTable, EnergyTable> operator()( const DeCalorimeter&, const CaloObjects&,
                                                                 const Digits&, const Track::Range&, const Filter&,
                                                                 const cellSizeCovariances& ) const override;

  private:
    // properties
    Gaudi::Property<std::vector<int>> m_type{
        this,
        "TrackTypes",
        {Track::Types::Long, Track::Types::Upstream, Track::Types::Downstream, Track::Types::Velo},
        "Types of input tracks"};
    int                    m_nmaxelements;
    Gaudi::Property<int>   m_nsquares{this,
                                    "nNeighborSquares",
                                    1,
                                    [this]( auto& ) { m_nmaxelements = std::pow( 2 * m_nsquares + 1, 2 ); },
                                    Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                    "Number of squares of cells around central cell to scan for clusters"};
    Gaudi::Property<bool>  m_usespread{this, "useSpread", false, "Use spread or cellsize/sqrt(12) for uncertainties"};
    Gaudi::Property<int>   m_caloplane{this, "CaloPlane", CaloPlane::ShowerMax,
                                     "Plane at the calorimeter of where to extrapolate to"};
    Gaudi::Property<int>   m_tablesize{this, "TableSize", 5000, "Size of track-cluster table"};
    Gaudi::Property<float> m_threshold{this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value"};

    // functions
    bool propagateToCalo( State& state_first, State& state_last, const Track& track,
                          const Gaudi::Math::Plane3D& plane ) const;

    // tools
    ToolHandle<ITrackExtrapolator> m_extrapolator_para{this, "ParabolicExtrapolator",
                                                       "TrackParabolicExtrapolator/FastExtrapolator"};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{this, "#match failure"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{this, "#above threshold"};
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{this, "average chi2"};
    mutable Gaudi::Accumulators::StatCounter<float>     m_etrackbased{this, "average energy (track based)"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{this, "Failed to generate index"};
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveBremMatchAlg, "SelectiveBremMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveBremMatchAlg::SelectiveBremMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          // Inputs
                          {KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                           KeyValue( "InputHypos", {CaloFutureAlgUtils::CaloFutureHypoLocation( "Photons" )} ),
                           KeyValue( "InputDigits", {CaloDigitLocation::Ecal} ), KeyValue( "InputTracks", {} ),
                           KeyValue( "Filter", {CaloFutureAlgUtils::CaloFutureIdLocation( "InBrem" )} ),
                           KeyValue( "cellSizeCovariances", {"AlgorithmSpecific-" + name + "-cellsizecovariances"} )},
                          // Outputs
                          {KeyValue( "OutputMatchTable", {} ), KeyValue( "OutputDeltaXTable", {} ),
                           KeyValue( "OutputEnergyTable", {} )} ) {}

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveBremMatchAlg::initialize() {
    return MultiTransformer::initialize().andThen( [&] {
      addConditionDerivation<cellSizeCovariances( const DeCalorimeter& )>(
          {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )}, inputLocation<cellSizeCovariances>() );
    } );
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  std::tuple<MatchTable, DeltaXTable, EnergyTable> SelectiveBremMatchAlg::
                                                   operator()( const DeCalorimeter& calo, const CaloObjects& caloobjects, const Digits& digits,
              const Track::Range& tracks, const Filter& filter, const cellSizeCovariances& cellsizecovs ) const {
    // output containers
    auto result = std::tuple{MatchTable( m_tablesize ), DeltaXTable( m_tablesize ), EnergyTable( tracks.size() )};
    auto& [trtable, dxtable, entable] = result;

    // obtain the tracks to be looped over by checking and filtering
    Track::ConstVector good_tracks;
    good_tracks.reserve( 2000 );
    std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( good_tracks ), [&]( const auto& track ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        return false;
      }
      const auto& r = filter.relations( track );
      return !r.empty() && r.front().to();
    } );

    // obtain the right calo objects
    const auto& hypoclusters = caloobjects.clusterContainer();
    auto        caloindex    = hypoclusters.index();
    if ( !caloindex ) {
      ++m_not_unique;
      return result;
    }

    // miscellaneous info
    const auto                plane_at_calo = calo.plane( m_caloplane );
    const auto                other_planes = std::array{calo.plane( CaloPlane::Front ), calo.plane( CaloPlane::Back )};
    const std::vector<double> z_energy_planes = {-calo.plane( CaloPlane::Front ).HesseDistance(),
                                                 -calo.plane( CaloPlane::Middle ).HesseDistance(),
                                                 -calo.plane( CaloPlane::Back ).HesseDistance()};
    const auto                cscovs = cellsizecovs();

    // declare what is needed in loop
    CaloCellID::Vector cellids, energycellids;
    cellids.reserve( 2 * m_nmaxelements );
    energycellids.reserve( 2 * m_nmaxelements );

    Gaudi::XYZPoint scanposition;
    State           state_first, state_last;
    Match2D::Vector track_fpos, track_lpos, calo_pos, diff_pos;
    Match2D::Matrix track_cov, comb_cov;

    // main loop over tracks
    for ( const auto& track : good_tracks ) {
      // propagate the two 'edge' states (first and last before magnet) to calo
      // NB: not calculating cov of first state (only last state cov is used)!
      if ( !propagateToCalo( state_first, state_last, *track, plane_at_calo ) ) continue;

      // clear up cellID bookkeeping
      cellids.clear();
      energycellids.clear();

      // look for CellIDs from first to last state
      auto       dx = state_last.x() - state_first.x();
      const auto cell_first = getClosestCellID( calo, state_first.position(), state_first.slopes(), other_planes );
      const auto cell_last = getClosestCellID( calo, state_last.position(), state_first.slopes(), other_planes );
      // scan along dx range if relevant (large enough)
      if ( cell_first && cell_first == cell_last ) {
        cellids.push_back( cell_first );
      } else {
        const auto stepsize = std::min( calo.cellSize( cell_first ), calo.cellSize( cell_last ) );
        scanposition = state_first.position();
        scanXRange( cellids, scanposition, dx, stepsize, calo );
      }

      // get energy of closest cells based on track state extrapolation
      energycellids.assign( cellids.begin(), cellids.end() );
      getTrackBasedEnergyCells( energycellids, state_first, calo, z_energy_planes );
      auto ebrem_trackbased =
          accumulate( begin( energycellids ), end( energycellids ), 0., [&]( double e, CaloCellID ci ) {
            if ( const auto digit = digits( ci ); digit ) { e += digit->energy(); }
            return e;
          } );
      entable.i_push( track, ebrem_trackbased );

      // add neighbouring cellids for calo obj search
      if ( !getNeighborCellIDs( cellids, calo, m_nsquares ) ) continue;

      // obtain relevant info for matching from track
      // take last state cov, as this should be largest
      track_fpos = {state_first.x(), state_first.y()};
      track_lpos = {state_last.x(), state_last.y()};
      track_cov = state_last.errPosition().Sub<Gaudi::SymMatrix2x2>( 0, 0 );

      // loop over local calo objects and calculate chi2
      for ( const auto caloid : cellids ) {
        // find calo obj ('cluster' storing info from hypo)
        const auto caloobj = caloindex.find( caloid );
        if ( caloobj == caloindex.end() ) continue;

        // obtain calo obj info
        calo_pos = {caloobj->position().x(), caloobj->position().y()};
        comb_cov = ( m_usespread ? caloobj->spread() : cscovs[caloobj->cellID().area()] ) + track_cov;

        // allow for a window between first and last state before magnet
        // calculate relative position between those states
        // (says roughly where along the track the brem emission is)
        diff_pos   = calo_pos - track_fpos;
        double rdx = ( std::fabs( dx ) > 0. ) ? diff_pos( 0 ) / dx : 0.;
        if ( rdx > 0. ) {
          if ( rdx > 1. ) {
            diff_pos( 0 ) -= dx;
            rdx = 1.;
          } else {
            diff_pos( 0 ) = 0.;
          }
        } else {
          rdx = 0.;
        }
        // brem tends to come from first state, non-brem random (uniform)
        // test statistic with the higher the value, the more 'first-state like'
        // and the lower, the more 'last-state like'
        double teststat_dx =
            ( comb_cov( 0, 0 ) > 0. ) ? std::fabs( dx ) * ( 0.5 - rdx ) / std::sqrt( comb_cov( 0, 0 ) ) : 0.;

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        auto chi2 = ROOT::Math::Similarity( diff_pos, comb_cov );

        // check if it has proper value
        if ( m_threshold < chi2 ) {
          m_nOverflow += 1;
          continue;
        }

        // only now push proper results
        trtable.i_push( caloobj->cellID(), track, chi2 );
        dxtable.i_push( caloobj->cellID(), track, teststat_dx );
      }
    }
    // sort tables (needed downstream of code!)
    trtable.i_sort();
    dxtable.i_sort();
    entable.i_sort();

    // monitor statistics: number of links and average chi2 and track-based energy
    const auto& mlinks  = trtable.i_relations();
    const auto  nMLinks = mlinks.size();
    m_nLinks += nMLinks;
    accumulate( m_chi2, mlinks, [nMLinks]( const auto& l ) { return l.weight() / nMLinks; } );
    const auto& elinks  = entable.i_relations();
    const auto  nELinks = elinks.size();
    accumulate( m_etrackbased, elinks, [nELinks]( const auto& l ) { return l.to() / nELinks; } );

    return result;
  }

  // ============================================================================
  //   Helper member functions
  // ============================================================================

  bool SelectiveBremMatchAlg::propagateToCalo( State& state_first, State& state_last, const Track& track,
                                               const Gaudi::Plane3D& plane ) const {
    bool status = true;
    // obtain first state
    state_first = track.firstState();
    // obtain last state
    const double zEnd = ( track.type() != Track::Types::Velo ) ? StateParameters::ZEndTT : StateParameters::ZEndVelo;
    state_last        = closestState( track, zEnd );
    if ( std::fabs( state_last.z() - zEnd ) > 10. * Gaudi::Units::cm ) {
      status = m_extrapolator_para->propagate( state_last, zEnd ).isSuccess();
    }
    // if selection of appropriate states does not work, first state should suffice
    if ( !status || state_last.z() < state_first.z() ) { state_last = state_first; }
    // quick linear extrapolation assuming only tilt of y-axis
    auto z_intersect_lambda = []( auto& state, auto& status, const auto& plane ) {
      auto denom = plane.C() + plane.B() * state.ty();
      if ( denom == 0. ) {
        status = false;
        return state.z();
      }
      return ( -plane.HesseDistance() - plane.B() * ( state.y() - state.ty() * state.z() ) ) / denom;
    };
    std::array<double, 2> zintersects = {z_intersect_lambda( state_first, status, plane ),
                                         z_intersect_lambda( state_last, status, plane )};
    // first state only needs position
    auto dz = zintersects[0] - state_first.z();
    state_first.setX( state_first.x() + state_first.tx() * dz );
    state_first.setY( state_first.y() + state_first.ty() * dz );
    state_first.setZ( state_first.z() + dz );
    // last is also used for covariance
    state_last.linearTransportTo( zintersects[1] );
    return status;
  }
} // namespace LHCb::Calo
