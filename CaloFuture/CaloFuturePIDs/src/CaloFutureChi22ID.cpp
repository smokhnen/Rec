/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "CaloFutureChi22ID.h"

// =============================================================================
/** @file
 *  Implementation file for class CaloFutureChi22ID
 *  @date 2006-06-18
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// =============================================================================
namespace LHCb::Calo {
  // =============================================================================
  /// Standard protected constructor
  // =============================================================================
  template <typename TABLEI, typename TABLEO>
  Chi22ID<TABLEI, TABLEO>::Chi22ID( const std::string& name, ISvcLocator* pSvcLocator )
      : base_type( name, pSvcLocator,
                   {KeyValue{"Tracks", CaloFutureAlgUtils::TrackLocations().front()}, KeyValue{"Input", ""}},
                   KeyValue{"Output", ""} ) {}

  // =============================================================================
  /// algorithm execution
  // =============================================================================
  template <typename TABLEI, typename TABLEO>
  TABLEO Chi22ID<TABLEI, TABLEO>::operator()( const Track::Range& tracks, const TABLEI& input ) const {

    TABLEO output( input.relations().size() + 10 );
    for ( auto const& track : tracks ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        continue;
      }

      if ( track->checkFlag( LHCb::Track::Flags::Backward ) ) { continue; }

      auto links = input.inverse()->relations( track );
      // fill the relation table
      auto chi2 = links.empty() ? m_large.value() : links.front().weight();
      output.i_push( track, chi2 );
    }
    /// MANDATORY: i_sort after i_push
    output.i_sort();

    m_nTracks += tracks.size();
    m_nLinks += output.i_relations().size();

    return output;
  }

  // =============================================================================

  template class Chi22ID<RelationWeighted2D<CaloCellID, Track, float>, Relation1D<Track, float>>;
} // namespace LHCb::Calo
