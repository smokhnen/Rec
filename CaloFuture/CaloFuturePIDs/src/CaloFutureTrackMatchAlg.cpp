/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureTrackMatchAlg.h"
#include "CaloFutureInterfaces/ICaloFutureTrackMatch.h"
#include "GaudiKernel/CommonMessaging.h"
#include "GaudiKernel/GaudiException.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloFutureTrackMatchAlg
 *  @date 2006-06-16
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
namespace LHCb::Calo {

  namespace {
    LHCb::CaloPosition position( LHCb::Event::Calo::Clusters::const_reference c ) {
      return LHCb::CaloPosition{}
          .setZ( c.position().z() )
          .setParameters( {c.position().x(), c.position().y(), c.energy()} )
          .setCenter( c.center() )
          .setSpread( c.spread() )
          .setCovariance( c.covariance() );
    }
    LHCb::CaloPosition position( LHCb::Event::Calo::Hypotheses::const_reference h ) {
      auto c = h.clusters();
      if ( c.size() != 1 ) throw "expected only a single cluster...";
      return position( c.front() );
    }
  } // namespace

  template <typename TABLE, typename CALOFUTURETYPES>
  TrackMatchAlg<TABLE, CALOFUTURETYPES>::TrackMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : base_type( name, pSvc, {KeyValue{"Tracks", ""}, KeyValue{"Calos", ""}, KeyValue{"Filter", ""}},
                   KeyValue{"Output", ""} ) {
    updateHandleLocation( *this, "Tracks", LHCb::CaloFutureAlgUtils::TrackLocations().front() );
  }

  // ============================================================================
  /// standard algorithm execution
  // ============================================================================
  template <typename TABLE, typename CALOFUTURETYPES>
  TABLE TrackMatchAlg<TABLE, CALOFUTURETYPES>::operator()( const Track::Range& tracks, const CALOFUTURETYPES& calos,
                                                           const Filter& filter ) const {
    LHCb::Track::ConstVector good_tracks;
    good_tracks.reserve( 100 );
    std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( good_tracks ), [&]( const auto& track ) {
      if ( m_type.value().end() == std::find( m_type.value().begin(), m_type.value().end(), track->type() ) ) {
        return false;
      }
      if ( track->checkFlag( LHCb::Track::Flags::Backward ) ) { return false; }
      const auto& r = filter.relations( track );
      return !r.empty() && r.front().to();
    } );
    const size_t nTracks = good_tracks.size();
    if ( 0 == nTracks )
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "No good tracks have been selected" << endmsg; }

    TABLE  table( m_tablesize );
    size_t nOverflow = 0;

    // loop over the calo objects
    Interfaces::ITrackMatch::MatchResults match_result;

    for ( const auto& calo : calos ) {
      // FIXME -- backwards compatiblity hack... create a position...
      const auto caloPos = position( calo );

      match_result.is_new_calo_obj = true;

      for ( const auto& track : good_tracks ) {
        if ( !track ) {
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "match(): track is not OK" << endmsg; }
          continue;
        }

        match_result = m_tool->match( caloPos, *track, match_result );

        match_result.is_new_calo_obj = false;

        if ( !match_result.chi2 ) {
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "Failure from Tool::match, skip" << endmsg; }
          m_nMatchFailure += 1;

          if ( match_result.skip_this_calo ) { break; }

          continue;
        }

        if ( m_threshold < *match_result.chi2 ) {
          ++nOverflow;
          continue;
        }
        table.i_push( calo.cellID(), track, *match_result.chi2 );
      }
    }

    // MANDATORY: i_sort after i_push
    table.i_sort(); // NB: i_sort

    const auto links = table.i_relations();
    m_nLinks += links.size();
    m_nTracks += nTracks;
    m_nCalos += calos.size();
    m_nOverflow += nOverflow;
    auto b = m_chi2.buffer();
    for ( const auto& l : links ) b += l.weight();

    return table;
  }

  template class TrackMatchAlg<LHCb::RelationWeighted2D<LHCb::CaloCellID, LHCb::Track, float>,
                               LHCb::Event::Calo::Clusters>;
  template class TrackMatchAlg<LHCb::RelationWeighted2D<LHCb::CaloCellID, LHCb::Track, float>,
                               LHCb::Event::Calo::Hypotheses>;
} // namespace LHCb::Calo
