/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "InCaloFutureAcceptance.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "Linear.h"

// ============================================================================
/** @class InBremFutureAcceptance
 *  The preconfigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================
namespace LHCb::Calo {
  struct InBremAcceptance final : InAcceptance {
    /// standard constructor
    InBremAcceptance( const std::string& type, const std::string& name, const IInterface* parent )
        : InAcceptance( type, name, parent ) {
      setProperty( "Calorimeter", DeCalorimeterLocation::Ecal ).ignore();
    }

    // ==========================================================================
    /** check the track is in acceptance of given calorimeter
     *  @see IInAcceptance
     *  @param  track track to be checked
     *  @return true if the track is in acceptance
     */
    bool inAcceptance( const LHCb::Track* track ) const override;
  };

  // ============================================================================

  // ============================================================================
  // check the expected bremstrahlung photon is in acceptance of Ecal
  // ============================================================================

  bool InBremAcceptance::inAcceptance( const LHCb::Track* track ) const {
    // find the appropriate state
    const LHCb::State& state = track->firstState();
    if ( state.z() > 4.0 * Gaudi::Units::meter ) {
      Error( "No appropriate states are found, see 'debug'" ).ignore();
      return false;
    }
    // get the line form the state
    const Line l = line( state );
    // get the point of intersection of the line with the plane
    Gaudi::XYZPoint point;
    double          mu = 0;
    Gaudi::Math::intersection( l, plane(), point, mu );
    //
    return ok( point );
  }
} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::InBremAcceptance, "InBremFutureAcceptance" )
// ============================================================================
