/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "SelectiveMatchUtils.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class SelectiveElectronMatchAlg SelectiveElectronMatchAlg.h
 *
 *  Matches tracks with local electron hypos in and around calo cell
 *  corresponding to track extrapolation from previous cluster-track
 *  matching.
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveElectronMatchAlg
 *  based on SelectiveTrackMatchAlg and CaloFutureTrackMatch
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using CaloObjects     = LHCb::Event::Calo::Hypotheses;
  using TrackMatchTable = LHCb::RelationWeighted2D<CaloCellID, Track, float>;

  // local helper functions for class
  namespace {

    // helper function, copied / adapted from DeRichBase.h
    template <typename OUTTYPE, std::size_t N, typename INTYPE = OUTTYPE>
    decltype( auto ) toarray( const std::vector<INTYPE>& v ) {
      if ( UNLIKELY( v.size() != N ) )
        throw GaudiException( "Vector to Array Size Error", __func__, StatusCode::FAILURE );
      std::array<OUTTYPE, N> a;
      std::copy( v.begin(), v.end(), a.begin() );
      return a;
    }

    // conditions for x-correction
    class electronXcorrections {
    private:
      std::array<double, 4> alphaPOut;
      std::array<double, 4> alphaNOut;
      std::array<double, 4> alphaPMid;
      std::array<double, 4> alphaNMid;
      std::array<double, 4> alphaPInn;
      std::array<double, 4> alphaNInn;

    public:
      electronXcorrections( Condition const& c )
          : alphaPOut{toarray<double, 4>( c.paramAsDoubleVect( "alphaPOut" ) )}
          , alphaNOut{toarray<double, 4>( c.paramAsDoubleVect( "alphaNOut" ) )}
          , alphaPMid{toarray<double, 4>( c.paramAsDoubleVect( "alphaPMid" ) )}
          , alphaNMid{toarray<double, 4>( c.paramAsDoubleVect( "alphaNMid" ) )}
          , alphaPInn{toarray<double, 4>( c.paramAsDoubleVect( "alphaPInn" ) )}
          , alphaNInn{toarray<double, 4>( c.paramAsDoubleVect( "alphaNInn" ) )} {}

      const std::array<double, 4>& operator()( const int area, const int charge, const int polarity ) const {
        bool qpolarity = charge * polarity > 0;
        switch ( area ) {
        case 0: // Outer  ECAL
          return qpolarity ? alphaPOut : alphaNOut;
        case 1: // Middle ECAL
          return qpolarity ? alphaPMid : alphaNMid;
        case 2: // Inner  ECAL
          return qpolarity ? alphaPInn : alphaNInn;
        default:
          throw GaudiException( "requested impossible Calo area", __func__, StatusCode::FAILURE );
        }
      }
    };

    // to apply x-correction to track state at calo
    bool applyXCorrection( double& x, const std::array<double, 4>& alphas, const double& momentum ) {
      if ( !alphas.empty() ) {
        // for expansion in p, 1, 1/p, 1/p^2 (all in GeV)
        double expansion = momentum / Gaudi::Units::GeV;
        double invmom    = 1. / expansion;
        for ( const auto alpha : alphas ) {
          x += alpha * expansion;
          expansion *= invmom;
        }
      }
      return true;
    }

    // obtain track position and momentum covariance
    bool get3DTrackCovariance( Match3D::Matrix& covariance, const LHCb::State& stateAtCalo ) {
      auto   statecov    = stateAtCalo.covariance();
      double qoverp      = stateAtCalo.qOverP();
      double f           = ( ( qoverp > 0. ) ? -1. : 1. ) / qoverp / qoverp;
      covariance( 0, 0 ) = statecov( 0, 0 );
      covariance( 0, 1 ) = statecov( 0, 1 );
      covariance( 1, 1 ) = statecov( 1, 1 );
      covariance( 0, 2 ) = statecov( 0, 4 ) * f;
      covariance( 1, 2 ) = statecov( 1, 4 ) * f;
      covariance( 2, 2 ) = statecov( 4, 4 ) * f * f;
      return true;
    }

  } // namespace

  using namespace LHCb::Calo::SelectiveMatchUtils;

  // main class
  class SelectiveElectronMatchAlg
      : public Gaudi::Functional::Transformer<TrackMatchTable( const DeCalorimeter&, const CaloObjects&,
                                                               const Track::Range&, const TrackMatchTable&,
                                                               const electronXcorrections& ),
                                              DetDesc::usesConditions<DeCalorimeter, electronXcorrections>> {
  public:
    // standard constructor
    SelectiveElectronMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    TrackMatchTable operator()( const DeCalorimeter&, const CaloObjects&, const Track::Range&, const TrackMatchTable&,
                                const electronXcorrections& ) const override;

  private:
    // properties
    Gaudi::Property<bool> m_addenergy{this, "AddEnergy", true, "add energy / momentum measurement in chi2"};
    Gaudi::Property<bool> m_onesided{this, "OneSidedE", false,
                                     "use only lower sided E/p (to avoid cluster overlap issues)"};
    Gaudi::Property<int>  m_caloplane{this, "CaloPlane", CaloPlane::ShowerMax,
                                     "Plane at the calorimeter of where to extrapolate to (ShowerMax for electrons)"};

    Gaudi::Property<float> m_zmin_linear{this, "zMinLinear", 10.0 * Gaudi::Units::m,
                                         "Minimum distance in z where we can linearly extrapolate to Calo"};
    Gaudi::Property<float> m_zmin_para{this, "zMinParabolic", 7.5 * Gaudi::Units::m,
                                       "Minimum distance in z where we can parabolicaly extrapolate to Calo"};

    Gaudi::Property<int>   m_tablesize{this, "TableSize", 5000, "Size of track-cluster table"};
    Gaudi::Property<float> m_threshold{this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value"};
    Gaudi::Property<float> m_tolerance{this, "ExtrapolationTolerance", 1. * Gaudi::Units::mm,
                                       "Tolerance for track extrapolation to calo plane"};

    Gaudi::Property<std::string> m_xcorrectionlocation{this, "XCorrectionLocation",
                                                       "/dd/Conditions/ParticleID/Calo/ElectronXCorrection",
                                                       "location in CondDB of x-corrections for electrons"};

    // functions
    bool propagateToCalo( State& state, const Track& track, const Gaudi::Math::Plane3D& plane ) const;

    // tools
    ToolHandle<ITrackExtrapolator> m_extrapolator_para{this, "ParabolicExtrapolator",
                                                       "TrackParabolicExtrapolator/FastExtrapolator"};
    ToolHandle<ITrackExtrapolator> m_extrapolator_ruku{this, "Extrapolator",
                                                       "TrackRungeKuttaExtrapolator/Extrapolator"};
    ServiceHandle<ILHCbMagnetSvc>  m_fieldSvc{this, "FieldSvc", "MagneticFieldSvc"};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{this, "#match failure"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{this, "#above threshold"};
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{this, "average chi2"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{this, "Failed to generate index"};
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveElectronMatchAlg, "SelectiveElectronMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveElectronMatchAlg::SelectiveElectronMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     {KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} ),
                      KeyValue( "InputHypos", {LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Electrons" )} ),
                      KeyValue( "InputTracks", {CaloFutureAlgUtils::TrackLocations().front()} ),
                      KeyValue( "InputTable", {CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch" )} ),
                      KeyValue( "XCorrections", {"AlgorithmSpecific-" + name + "-xcorrections"} )},
                     // Outputs
                     {KeyValue( "Output", {CaloFutureAlgUtils::CaloFutureIdLocation( "ElectronMatch" )} )} ) {}

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveElectronMatchAlg::initialize() {
    auto sc = Transformer::initialize().andThen( [&] {
      addConditionDerivation<electronXcorrections( Condition const& )>( {m_xcorrectionlocation.value()},
                                                                        inputLocation<electronXcorrections>() );
    } );
    return sc;
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  TrackMatchTable SelectiveElectronMatchAlg::operator()( const DeCalorimeter& calo, const CaloObjects& caloobjects,
                                                         const Track::Range&         tracks,
                                                         const TrackMatchTable&      clustertable,
                                                         const electronXcorrections& xcorrection ) const {
    // declare output
    TrackMatchTable table( m_tablesize );

    // get input (table)
    const auto tracktable = clustertable.inverse();

    // obtain the right calo objects
    const auto& hypoclusters = caloobjects.clusterContainer();
    auto        hypoindex    = hypoclusters.index();
    if ( !hypoindex || hypoclusters.size() != caloobjects.size() ) {
      ++m_not_unique;
      return table;
    }

    // miscellaneous info
    const int  polarity      = m_fieldSvc->isDown() ? -1 : 1;
    const auto plane_at_calo = calo.plane( m_caloplane );

    // declare what is needed in the loop
    LHCb::State     stateAtCalo;
    Match3D::Vector track_pos, track_pos_local, calo_pos, diff_pos;
    Match3D::Matrix track_cov, comb_cov;

    // main loop over tracks
    for ( const auto& track : tracks ) {
      // get cellIDs close to track from previous track matching if it exists
      const auto& local_caloids = tracktable->relations( track );
      if ( local_caloids.empty() ) continue;

      // get state at calo plane
      if ( !propagateToCalo( stateAtCalo, *track, plane_at_calo ) ) continue;

      // obtain relevant info for matching from track
      track_pos            = {stateAtCalo.x(), stateAtCalo.y(), stateAtCalo.p()};
      track_pos_local( 2 ) = track_pos( 2 );
      get3DTrackCovariance( track_cov, stateAtCalo );

      // obtain electron x-correction
      const auto  area   = getAreaForTrack( stateAtCalo.position(), calo );
      const auto& alphas = xcorrection( area, track->charge(), polarity );
      applyXCorrection( track_pos( 0 ), alphas, stateAtCalo.p() );

      // loop over local calo objects and calculate chi2
      for ( const auto& caloid : local_caloids ) {
        // find calo obj ('cluster' storing info from hypo)
        const auto calo_obj = hypoindex.find( caloid.to() );
        if ( calo_obj == hypoindex.end() ) continue;

        // adapt track state to calo obj location
        double delta_z       = calo_obj->position().z() - stateAtCalo.z();
        track_pos_local( 0 ) = track_pos( 0 ) + stateAtCalo.tx() * delta_z;
        track_pos_local( 1 ) = track_pos( 1 ) + stateAtCalo.ty() * delta_z;

        // obtain calo obj info
        calo_pos = {calo_obj->position().x(), calo_obj->position().y(), calo_obj->energy()};
        comb_cov = track_cov + calo_obj->covariance();
        diff_pos = calo_pos - track_pos_local;

        // adapt to options
        if ( !m_addenergy || ( m_onesided && diff_pos( 2 ) > 0. ) ) { diff_pos( 2 ) = 0.; }

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        auto chi2 = ROOT::Math::Similarity( diff_pos, comb_cov );

        // check if it has proper value
        if ( chi2 > m_threshold ) {
          m_nOverflow += 1;
          continue;
        }

        // only now push proper result
        table.i_push( calo_obj->cellID(), track, chi2 );
      }
    }
    // sort table (needed downstream of code!)
    table.i_sort();

    // monitor statistics: number of links and average chi2
    const auto& links  = table.i_relations();
    const auto  nLinks = links.size();
    m_nLinks += nLinks;
    accumulate( m_chi2, links, [nLinks]( const auto& l ) { return l.weight() / nLinks; } );

    return table;
  }

  // ============================================================================
  //   Helper member functions
  // ============================================================================

  bool SelectiveElectronMatchAlg::propagateToCalo( State& state, const Track& track,
                                                   const Gaudi::Plane3D& plane ) const {
    // obtain closest state to calo plane (tilt is small enough to ignore in closest state search)
    state = closestState( track, -plane.HesseDistance() );
    // check how far, and decide which extrapolation is good enough
    if ( state.z() > m_zmin_linear ) {
      // quick linear extrapolation assuming only tilt of y-axis
      auto denom = plane.C() + plane.B() * state.ty();
      if ( denom == 0. ) return false;
      auto zintersect = ( -plane.HesseDistance() - plane.B() * ( state.y() - state.ty() * state.z() ) ) / denom;
      state.linearTransportTo( zintersect );
      return true;
    }
    // in case one is beyond the magnet, parabolic is good enough w.r.t.
    // precision due to cell size, otherwise still use RK
    return ( state.z() > m_zmin_para ? m_extrapolator_para : m_extrapolator_ruku )
        ->propagate( state, plane, m_tolerance )
        .isSuccess();
  }
} // namespace LHCb::Calo
