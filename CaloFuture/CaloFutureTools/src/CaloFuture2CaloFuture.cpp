/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "Kernel/CaloCellCode.h"
// ============================================================================
/** @file
 *  Implementation file for class : CaloFuture2CaloFuture
 *  @date 2007-05-29
 *  @author Olivier Deschamps
 */
// ============================================================================

StatusCode CaloFuture2CaloFuture::initialize() {
  return GaudiTool::initialize().andThen( [&] {
    // DeCalorimeter* pointers
    m_det[CaloCellCode::CaloIndex::EcalCalo] = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    m_det[CaloCellCode::CaloIndex::HcalCalo] = getDet<DeCalorimeter>( DeCalorimeterLocation::Hcal );
    // CellSize reference (outer section)  Warning : factor 2 for Hcal
    m_refSize[CaloCellCode::CaloIndex::HcalCalo] =
        m_det[CaloCellCode::CaloIndex::HcalCalo]->cellParams().front().size() / 2.;
    m_refSize[CaloCellCode::CaloIndex::EcalCalo] =
        m_det[CaloCellCode::CaloIndex::EcalCalo]->cellParams().front().size();
    // CaloPlanes
    m_plane[CaloCellCode::CaloIndex::HcalCalo] =
        m_det[CaloCellCode::CaloIndex::HcalCalo]->plane( CaloPlane::ShowerMax );
    m_plane[CaloCellCode::CaloIndex::EcalCalo] =
        m_det[CaloCellCode::CaloIndex::EcalCalo]->plane( CaloPlane::ShowerMax );
  } );
}

//=======================================================================================================
std::vector<LHCb::Event::Calo::Digit> CaloFuture2CaloFuture::cellIDs( const LHCb::CaloCluster&         fromCluster,
                                                                      CaloCellCode::CaloIndex          toCalo,
                                                                      const LHCb::Event::Calo::Digits& digits ) const {
  std::vector<LHCb::Event::Calo::Digit> output;
  const auto&                           entries = fromCluster.entries();
  output.reserve( 2 * entries.size() );
  for ( const auto& ent : entries ) {
    if ( const LHCb::CaloDigit* digit = ent.digit(); digit ) {
      output = cellIDs_( digit->cellID(), toCalo, digits, std::move( output ) );
    }
  }
  return output;
}

//=======================================================================================================
std::vector<LHCb::Event::Calo::Digit>
CaloFuture2CaloFuture::cellIDs_( const LHCb::CaloCellID& fromId, CaloCellCode::CaloIndex toCalo,
                                 const LHCb::Event::Calo::Digits&        digits,
                                 std::vector<LHCb::Event::Calo::Digit>&& container ) const {
  auto fromCalo = fromId.calo();

  LHCb::CaloCellID toId = fromId;
  // ---- Assume ideal geometry : trivial mapping for detectors having the same granularity (Prs/Spd/Ecal)
  if ( ( m_geo && fromCalo == CaloCellCode::CaloIndex::EcalCalo && toCalo == CaloCellCode::CaloIndex::EcalCalo ) ||
       fromCalo == toCalo ) {
    toId.setCalo( toCalo );
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Add cell from trivial mapping" << endmsg;
    return addCell( toId, digits, std::move( container ) );
  }

  // ---- Else use the actual geometry to connet detectors
  double          scale  = 1.;
  Gaudi::XYZPoint center = m_det[fromCalo]->cellCenter( fromId );
  if ( fromCalo != toCalo ) {
    // z-scaling
    scale  = m_refSize[toCalo] / m_refSize[fromCalo];
    center = m_plane[toCalo].ProjectOntoPlane( m_det[fromCalo]->cellCenter( fromId ) * scale );
    // connect
    toId = m_det[toCalo]->Cell( center );
  }
  double fromSize = m_det[fromCalo]->cellSize( fromId ) * scale;
  // cell-center is outside 'toCalo' - check corners
  if ( !toId ) {
    for ( int i = 0; i != 2; ++i ) {
      for ( int j = 0; j != 2; ++j ) {
        double x = m_det[fromCalo]->cellCenter( fromId ).X() + ( i * 2 - 1 ) * fromSize;
        double y = m_det[fromCalo]->cellCenter( fromId ).Y() + ( j * 2 - 1 ) * fromSize;
        if ( auto cornerId = m_det[toCalo]->Cell( {x, y, center.Z()} ); cornerId ) toId = cornerId;
      }
    }
  }
  if ( !toId ) return std::move( container );
  int    pad = 1;
  double x0  = center.X();
  double y0  = center.Y();
  if ( fromCalo != toCalo ) {
    double toSize = m_det[toCalo]->cellSize( toId );
    pad           = std::max( 1, (int)floor( fromSize / toSize + 0.25 ) ); // warning int precision
    x0            = center.X() - ( pad - 1 ) * fromSize / 2. / pad;
    y0            = center.Y() - ( pad - 1 ) * fromSize / 2. / pad;
  }
  for ( int i = 0; i != pad; ++i ) {
    for ( int j = 0; j != pad; ++j ) {
      if ( fromCalo != toCalo )
        toId = m_det[toCalo]->Cell( {x0 + i * fromSize / pad, y0 + j * fromSize / pad, center.Z()} );
      if ( !toId ) continue;
      assert( toId.calo() == toCalo );
      container = addCell( toId, digits, std::move( container ) );
    }
  }
  return std::move( container );
}
