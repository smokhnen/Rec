/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "IGammaPi0SeparationTool.h"
#include <cmath>

//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0SeparationTool
//
// 2019-03-28 : Miriam Calvo Gomez
//-----------------------------------------------------------------------------

#include "TMV_MLP_inner.C"
#include "TMV_MLP_middle.C"
#include "TMV_MLP_outer.C"

/** @class FutureGammaPi0SeparationTool FutureGammaPi0SeparationTool.h
 *
 *
 *  @author Miriam Calvo Gomez
 *  @date   2019-03-28
 */
namespace LHCb::Calo {
  namespace {
    // TMVA discriminant
    constexpr auto inputVars =
        std::array<std::string_view, 6>{"isPhfr2", "isPhfr2r4", "abs(isPhasym)", "isPhkappa", "isPhEseed", "isPhE2"};
    const ReadMLPOuter  s_reader0{inputVars};
    const ReadMLPMiddle s_reader1{inputVars};
    const ReadMLPInner  s_reader2{inputVars};
  } // namespace

  class GammaPi0Separation : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double>      isPhoton( const LHCb::CaloHypo& hypo ) const override;
    std::optional<double>      isPhoton( Observables const& observables ) const override;
    std::optional<Observables> observables( const CaloHypo& hypo ) const override;

  private:
    std::optional<double> photonDiscriminant( int area, double r2, double r2r4, double asym, double kappa, double Eseed,
                                              double E2 ) const;

    Gaudi::Property<float> m_minPt{this, "MinPt", 2000.};

    const DeCalorimeter* m_ecal = nullptr;
  };
  DECLARE_COMPONENT_WITH_ID( GammaPi0Separation, "FutureGammaPi0SeparationTool" )

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode GammaPi0Separation::initialize() {

    return extends::initialize().andThen( [&] {
      /// Retrieve geometry of detector
      m_ecal = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
      return StatusCode{m_ecal != nullptr};
    } );
  }

  //=============================================================================
  // Main execution
  //=============================================================================

  std::optional<double> GammaPi0Separation::isPhoton( const LHCb::CaloHypo& hypo ) const {
    auto observables_ = observables( hypo );
    if ( !observables_ ) {
      return {};
    } else {
      return isPhoton( *observables_ );
    }
  }

  std::optional<double> GammaPi0Separation::isPhoton( Observables const& observables ) const {
    const auto input =
        std::array{observables.fr2,   observables.fr2r4, std::abs( observables.fasym ), observables.fkappa,
                   observables.Eseed, // already divided by Ecl
                   observables.E2};   // means (e2+eseed)/ecl
    switch ( observables.area ) {
    case 0:
      return s_reader0.GetMvaValue( input );
    case 1:
      return s_reader1.GetMvaValue( input );
    case 2:
      return s_reader2.GetMvaValue( input );
    default:
      return {};
    }
  }

  std::optional<Interfaces::IGammaPi0Separation::Observables>
  GammaPi0Separation::observables( const LHCb::CaloHypo& hypo ) const {

    if ( LHCb::Calo::Momentum( &hypo ).pt() < m_minPt ) return {};

    const LHCb::CaloCluster* cluster =
        LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo ); // OD 2014/05 - change to Split Or Main  cluster
    if ( !cluster ) return {};

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Inside ClusterVariables ------" << endmsg;

    const auto cxx    = cluster->position().spread()( 0, 0 );
    const auto cyy    = cluster->position().spread()( 1, 1 );
    const auto cxy    = cluster->position().spread()( 0, 1 );
    double     fr2    = cxx + cyy;
    double     fasym  = ( cxx > 0 && cyy > 0 ? cxy / std::sqrt( cxx * cyy ) : 0 );
    const auto arg    = ( fr2 > 0. ? 1. - 4. * ( cxx * cyy - cxy * cxy ) / ( fr2 * fr2 ) : 0. );
    double     fkappa = arg > 0. ? std::sqrt( arg ) : 0.;

    const auto xmean = cluster->position().x();
    const auto ymean = cluster->position().y();

    // OD : WARNING cluster->e() is cluster-shape dependent (3x3 / 2x2 ...) RE-EVALUATE E3x3 instead for back.
    // compatibility etot = cluster->e(); //same as position.e
    double Ecl{0.};

    const Gaudi::XYZPoint position( xmean, ymean, cluster->position().z() );

    double r4{0.};
    int    area{-1};
    int    ncells{0};
    double secondE{0.};
    double Eseed{0.};

    const auto& entries = cluster->entries();
    for ( const auto& entry : entries ) {
      const LHCb::CaloDigit* digit = entry.digit();
      if ( !digit ) { continue; }
      const auto energy = digit->e() * entry.fraction();

      if ( abs( (int)digit->cellID().col() - (int)cluster->seed().col() ) <= 1 &&
           abs( (int)digit->cellID().row() - (int)cluster->seed().row() ) <= 1 &&
           digit->cellID().area() == cluster->seed().area() ) {
        Ecl += energy;
      }

      if ( entry.status().test( LHCb::CaloDigitStatus::Mask::SeedCell ) ) {
        area  = digit->cellID().area();
        Eseed = energy;
      } else {
        if ( energy > secondE ) { secondE = energy; }
      }

      const auto& pos = m_ecal->cellCenter( digit->cellID() );
      const auto  x   = pos.x();
      const auto  y   = pos.y();

      if ( energy <= 0 ) { continue; }
      const double weight = energy > 0.0 ? energy : 0.0;

      auto rr = std::pow( x - xmean, 2 ) + std::pow( y - ymean, 2 );
      if ( entries.size() <= 1 || rr < 1.e-10 ) { rr = 0; } // to avoid huge unphysical value due to machine precision
      r4 += weight * rr * rr;

      ncells++;
    } // loop cluster cells

    double fr2r4{0.};
    double E2{0.};

    if ( Ecl > 0. ) {
      r4 /= Ecl;
      fr2r4 = ( r4 != 0 ) ? ( r4 - fr2 * fr2 ) / r4 : 0.;
      E2    = ( secondE + Eseed ) / Ecl;
      Eseed = Eseed / Ecl;
    } else {
      // should never happen
      r4    = 0;
      fr2r4 = 0.;
      E2    = 0.;
      Eseed = 0.;
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
    Observables observables{.fr2    = fr2,
                            .fasym  = fasym,
                            .fkappa = fkappa,
                            .fr2r4  = fr2r4,
                            .Eseed  = Eseed,
                            .E2     = E2,
                            .Ecl    = Ecl,
                            .area   = area};
#pragma GCC diagnostic pop

    return observables;
  }
} // namespace LHCb::Calo
