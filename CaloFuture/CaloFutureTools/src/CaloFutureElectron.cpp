/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/TrackDefaultParticles.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class CaloFutureElectron CaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */

namespace {
  // fix name clash with Track2Calo::closestState
  decltype( auto ) closest_state( const LHCb::Track& t, const Gaudi::Plane3D& p ) { return closestState( t, p ); }
} // namespace

namespace LHCb::Calo::Tools {

  class Electron final : public extends<GaudiTool, Interfaces::IElectron> {
  public:
    /// Standard constructor
    using extends::extends;

    Interfaces::hypoPairStruct getElectronBrem( ProtoParticle const& proto ) const override;
    State                      caloState( ProtoParticle const& proto ) const override;
    State                      caloState( Track const& ) const override;
    State                      closestState( ProtoParticle const& proto ) const override;
    Momentum                   bremMomentum( ProtoParticle const& proto ) const override;
    std::optional<double>      caloTrajectoryL( ProtoParticle const& proto, CaloPlane::Plane plane ) const override;

  private:
    // State caloState( ProtoParticle const& proto, CaloPlane::Plane plane, double delta = 0 ) const;
    State caloState( Track const& track, CaloPlane::Plane plane, double delta ) const;

    // configuration-dependent state
    ToolHandle<ITrackExtrapolator> m_extrapolator{this, "Extrapolator", "TrackRungeKuttaExtrapolator"};
    Gaudi::Property<float>         m_tolerance{this, "Tolerance", 0.01};
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( Electron, "CaloFutureElectron" )

  //=============================================================================
  Interfaces::hypoPairStruct Electron::getElectronBrem( ProtoParticle const& proto ) const {
    if ( !proto.track() ) return {nullptr, nullptr};
    Interfaces::hypoPairStruct hypoPair;
    for ( const CaloHypo* hypo : proto.calo() ) {
      if ( !hypo ) continue;
      switch ( hypo->hypothesis() ) {
      case CaloHypo::Hypothesis::EmCharged:
        hypoPair.electronHypo = hypo;
        break;
      case CaloHypo::Hypothesis::Photon:
        hypoPair.bremHypo = hypo;
        break;
      default:; // nothing;
      }
    }
    if ( !( hypoPair && hypoPair.electronHypo->position() ) ) { // Electron hypo is mandatory - brem. not
      return {nullptr, nullptr};
    }
    return hypoPair;
  }

  //=============================================================================
  State Electron::caloState( ProtoParticle const& proto ) const {
    auto hypo = electron( proto );
    if ( !hypo ) return {};
    return caloState( *proto.track(), CaloPlane::ShowerMax, 0. );
  }

  //=============================================================================
  State Electron::caloState( Track const& track ) const { return caloState( track, CaloPlane::ShowerMax, 0. ); }

  //=============================================================================
  Momentum Electron::bremMomentum( ProtoParticle const& proto ) const {
    auto hypoPair = getElectronBrem( proto );
    if ( !hypoPair || hypoPair.bremHypo == nullptr ) return {};
    Gaudi::XYZPoint     point;
    Gaudi::SymMatrix3x3 matrix;
    proto.track()->position( point, matrix );
    return {hypoPair.bremHypo, point, matrix};
  }

  //=============================================================================
  State Electron::closestState( ProtoParticle const& proto ) const {
    auto el = electron( proto );
    if ( !el ) return {};
    // get state on Front of Ecal
    State calostate = caloState( *proto.track(), CaloPlane::Front, 0. );
    if ( calostate.z() == 0 ) return {};
    // get frontPlane
    auto                detCalo    = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    ROOT::Math::Plane3D frontPlane = detCalo->plane( CaloPlane::Front );
    // get hypo position
    const auto& params = el->position()->parameters();
    auto        x      = params( CaloPosition::Index::X );
    auto        y      = params( CaloPosition::Index::Y );
    // Define calo line (from transversal barycenter) and track line in Ecal
    Gaudi::XYZVector  normal = frontPlane.Normal();
    auto              zEcal  = ( -normal.X() * x - normal.Y() * y - frontPlane.HesseDistance() ) / normal.Z(); // tilt
    Gaudi::XYZPoint   point{x, y, zEcal};
    Gaudi::Math::Line cLine{point, frontPlane.Normal()};
    Gaudi::Math::Line tLine{calostate.position(), calostate.slopes()};
    // Find points of closest distance between calo Line and track Line
    Gaudi::XYZPoint cP, tP;
    Gaudi::Math::closestPoints( cLine, tLine, cP, tP );
    // propagate the state the new Z of closest distance - default to electron
    StatusCode sc = m_extrapolator->propagate( calostate, tP.Z(), Tr::PID::Electron() );
    if ( sc.isFailure() ) return {};
    return calostate;
  }

  //=============================================================================
  std::optional<double> Electron::caloTrajectoryL( ProtoParticle const& proto, CaloPlane::Plane refPlane ) const {
    if ( !electron( proto ) ) return std::nullopt;
    const auto theState = closestState( proto );
    const auto refState = caloState( *proto.track(), refPlane, 0. );
    const auto depth    = ( theState.position() - refState.position() ).R();
    const auto detCalo  = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    const auto plane    = detCalo->plane( refPlane );
    const auto dist     = plane.Distance( theState.position() ); // signed distance to refPlane
    return std::copysign( depth, dist );
  }

  //=============================================================================
  State Electron::caloState( Track const& track, CaloPlane::Plane plane, double delta ) const {
    // get caloPlane
    auto                detCalo  = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
    ROOT::Math::Plane3D refPlane = detCalo->plane( plane );
    // propagate state to refPlane
    const Tr::PID pid       = Tr::PID::Pion();
    State         calostate = closest_state( track, refPlane );
    StatusCode    sc        = m_extrapolator->propagate( calostate, refPlane, m_tolerance, pid );
    if ( sc.isFailure() ) return {};
    if ( 0. == delta ) return calostate;
    Gaudi::XYZVector dir( calostate.tx(), calostate.ty(), 1. );
    auto             z = ( calostate.position() + delta * dir.Unit() ).z();
    // extrapolate to the new point
    sc = m_extrapolator->propagate( calostate, z, pid );
    if ( sc.isFailure() ) return {};
    return calostate;
  }
} // namespace LHCb::Calo::Tools
