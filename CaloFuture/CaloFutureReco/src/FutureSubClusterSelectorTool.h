/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "Event/CaloClusters_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloFutureSubClusterTag.h"

namespace LHCb::Calo {

  namespace CaloFutureClusterMask {
    enum Mask {
      area3x3    = 0, // MUST BE 0 FOR BACKWARD COMPATIBILITY
      area2x2    = 1,
      SwissCross = 2,
      Last
    };
    constexpr int            nMask           = Last + 1;
    inline const std::string maskName[nMask] = {"3x3", "2x2", "SwissCross", "Unknown"};
  } // namespace CaloFutureClusterMask

  static const InterfaceID IID_FutureSubClusterSelectorTool( "FutureSubClusterSelectorTool", 1, 0 );

  /** @class FutureSubClusterSelectorTool FutureSubClusterSelectorTool.h
   *
   *
   *  @author Olivier Deschamps
   *  @date   2014-06-20
   */

  class SubClusterSelectorTool : public GaudiTool {
  public:
    // Return the interface ID
    static const InterfaceID& interfaceID() { return IID_FutureSubClusterSelectorTool; }

    /// Standard constructor
    SubClusterSelectorTool( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override;

    StatusCode tagEnergy( LHCb::CaloCellID id, LHCb::Event::Calo::Clusters::Entries entries ) const {
      // get the tagger
      auto* tagger = ( id.area() < m_tagE.size() ) ? m_tagE[id.area()] : nullptr;
      if ( !tagger ) {
        ++m_no_tagger;
        return StatusCode::FAILURE;
      }
      return tagger->tag( entries );
    }

    StatusCode tagPosition( LHCb::CaloCellID id, LHCb::Event::Calo::Clusters::Entries entries ) const {
      // get the tagger
      auto* tagger = ( id.area() < m_tagP.size() ) ? m_tagP[id.area()] : nullptr;
      if ( !tagger ) {
        ++m_no_tagger;
        return StatusCode::FAILURE;
      }
      return tagger->tag( entries );
    }

    StatusCode tag( LHCb::CaloCellID id, LHCb::Event::Calo::Clusters::Entries entries ) const {
      return tagEnergy( id, entries ).andThen( [&] { return tagPosition( id, entries ); } );
    }

  private:
    StatusCode getParamsFromOptions();
    StatusCode updateParamsFromDB();

    Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags", {}};
    Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags", {}};
    std::vector<Interfaces::ISubClusterTag*>  m_tagE;
    std::vector<Interfaces::ISubClusterTag*>  m_tagP;

    // associate known cluster mask to tagger tool
    Gaudi::Property<std::map<std::string, std::string>> m_clusterTaggers{
        this,
        "ClusterTaggers",
        {
            {"", "useDB"},
            {"useDB", "useDB"},
            {"3x3", "FutureSubClusterSelector3x3"},
            {"2x2", "FutureSubClusterSelector2x2"},
            {"SwissCross", "FutureSubClusterSelectorSwissCross"},
        },
        "associate known cluster mask to tagger tool"};

    Gaudi::Property<std::string> m_condition{this, "ConditionName", ""};
    Gaudi::Property<std::string> m_det{this, "Detector"};

    DeCalorimeter*                m_detector   = nullptr;
    CaloFutureCorrectionBase*     m_dbAccessor = nullptr;
    std::vector<std::string>      m_DBtaggerE;
    std::vector<std::string>      m_DBtaggerP;
    LHCb::CaloDigitStatus::Status m_energyStatus = LHCb::CaloDigitStatus::Status{
        LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForCovariance};
    LHCb::CaloDigitStatus::Status m_positionStatus = LHCb::CaloDigitStatus::Status{
        LHCb::CaloDigitStatus::Mask::UseForPosition, LHCb::CaloDigitStatus::Mask::UseForCovariance};
    std::string m_sourceE;
    std::string m_sourceP;

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_tagger{this, "Tagger not found"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_energy_update_failed{this,
                                                                                 "Cannot update energy mask from DB"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_position_update_failed{
        this, "Cannot update position mask from DB"};
  };
} // namespace LHCb::Calo
