/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "FutureSubClusterSelectorTool.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/Transformer.h"
#include "ICaloFutureClusterTool.h"

/** @class CaloFutureClusterCovarianceAlg CaloFutureClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloFutureCluster object
 *
 *  @see CaloFutureClusterCovarianceAlg
 *  @see ICaloFutureClusterTool
 *  @see ICaloFutureSubClusterTag
 *
 *  @author Vanya Belyaev Ivan Belyaev
 *  @date   04/07/2001
 */

namespace LHCb::Calo::Algorithms {
  class ClusterCovariance
      : public Gaudi::Functional::Transformer<Event::Calo::Clusters( const Event::Calo::Clusters& )> {

  public:
    /** Standard constructor
     *  @param   name          algorithm name
     *  @param   pSvcLocator   pointer to Service Locator
     */
    ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator );

    /** standard execution method
     *  @see GaudiAlgorithm
     *  @see     Algorithm
     *  @see    IAlgorithm
     *  @return Container of
     */
    Event::Calo::Clusters operator()( const Event::Calo::Clusters& ) const override;

  private:
    // tool used for covariance matrix calculation
    ToolHandle<Interfaces::IClusterTool> m_cov{this, "CovarianceTool",
                                               "FutureClusterCovarianceMatrixTool/" +
                                                   toString( Utilities::CaloIndexFromAlg( name() ) ) + "CovarTool"};
    ToolHandle<SubClusterSelectorTool>   m_tagger{this, "TaggerTool",
                                                "FutureSubClusterSelectorTool/" +
                                                    toString( Utilities::CaloIndexFromAlg( name() ) ) + "ClusterTag"};
    // tool used for cluster spread estimation
    ToolHandle<Interfaces::IClusterTool> m_spread{this, "SpreadTool",
                                                  "FutureClusterSpreadTool/" +
                                                      toString( Utilities::CaloIndexFromAlg( name() ) ) + "SpreadTool"};

    // following properties are inherited by the selector tool when defined:
    Gaudi::Property<std::string>              m_tagName{this, "TaggerName"};
    Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags"};
    Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags"};

    // collection of known cluster shapes
    Gaudi::Property<std::map<std::string, std::vector<double>>> m_covParams{
        this, "CovarianceParameters", {}}; // KEEP IT UNSET ! INITIAL VALUE WOULD BYPASS DB ACCESS

    // counter
    mutable Gaudi::Accumulators::Counter<> m_clusters{this, "# clusters"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_cov_error{this, "Error from cov,    skip cluster "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_spread_error{this, "Error from spread, skip cluster "};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_tagger_error{this, "Error from tagger, skip cluster "};
  };
  DECLARE_COMPONENT_WITH_ID( ClusterCovariance, "CaloFutureClusterCovarianceAlg" )

  // ============================================================================
  /** Standard constructor
   *  @param   name          algorithm name
   *  @param   pSVcLocator   pointer to Service Locator
   */
  // ============================================================================
  ClusterCovariance::ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     KeyValue{"InputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" )},
                     KeyValue{"OutputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "Ecal" )} ) {}

  // ===========================================================================
  Event::Calo::Clusters ClusterCovariance::operator()( const Event::Calo::Clusters& clusters ) const {

    // create new container for output clusters
    Event::Calo::Clusters outputClusters = clusters;
    // copy clusters to new container, and apply corrections on the way...
    auto first = outputClusters.begin();
    while ( first != outputClusters.end() ) {
      // == APPLY TAGGER
      if ( m_tagger->tag( first->seed(), first->entries() ).isFailure() ) {
        ++m_tagger_error;
        first = outputClusters.erase( first );
        continue;
      }
      // == APPLY COVARIANCE ESTIMATOR
      if ( ( *m_cov )( *first ).isFailure() ) {
        ++m_cov_error;
        first = outputClusters.erase( first );
        continue;
      }

      // == APPLY SPREAD ESTIMATOR
      if ( ( *m_spread )( *first ).isFailure() ) {
        ++m_spread_error;
        first = outputClusters.erase( first );
        continue;
      }
      ++first;
    }

    // == counter
    m_clusters += clusters.size();

    return outputClusters;
  }
} // namespace LHCb::Calo::Algorithms
