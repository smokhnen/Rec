/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ICALOFUTUREDIGITFILTERTOOL_H
#define ICALOFUTUREDIGITFILTERTOOL_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/CaloCellID.h"

// from STL
#include <string>

/** @class ICaloFutureDigitFilterTool ICaloFutureDigitFilterTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-12-13
 */
struct ICaloFutureDigitFilterTool : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( ICaloFutureDigitFilterTool, 5, 0 );

  class Pileup_t {
    friend struct ICaloFutureDigitFilterTool;
    int n;
    Pileup_t( int n ) : n{n} {}

  public:
    operator int() const { return n; }
  };

  virtual int    method( CaloCellCode::CaloIndex det ) const          = 0;
  virtual double offset( LHCb::CaloCellID id, Pileup_t pileup ) const = 0;
  Pileup_t       pileup() const { return Pileup_t{i_pileup()}; }

protected:
  virtual int i_pileup() const = 0;
};

#endif // ICALOFUTUREDIGITFILTERTOOL_H
