/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloFutureDigitFilterTool.h"
#include "ICaloFutureHypoTool.h"
#include <map>
#include <string>

/** @file
 *  Implementation file for class : CaloFutureECorrection
 *
 *  @date 2003-03-10
 *  @author Deschamps Olivier
 */

/** @namespace CaloFutureECorrection_Local
 */

/** @class CaloFutureECorrection CaloFutureECorrection.h
 *
 *
 *  @author Deschamps Olivier

 *  @date   2003-03-10
 */
namespace {
  int shiftAs( unsigned int cellIDColOrRow, LHCb::span<const int, 3> shift, unsigned int area ) {
    assert( area < 3 );
    unsigned int colOrRow = cellIDColOrRow - shift[area] + 1;
    // leakage induced by Ecal module frame
    switch ( colOrRow % ( area + 1 ) ) {
    case 1:
      return +1;
    case 0:
      return -1;
    default:
      return 0;
    }
  }
} // namespace

class CaloFutureECorrection : public extends<CaloFutureCorrectionBase, LHCb::Calo::Interfaces::IProcessHypos> {

public:
  CaloFutureECorrection( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode process( LHCb::Event::Calo::Hypotheses::Type, LHCb::Event::Calo::Clusters::Range,
                      const LHCb::CaloFuture2Track::ICluster2TrackTable2D* ctable ) const override;
  StatusCode process( LHCb::Event::Calo::Hypotheses::Type h, LHCb::Event::Calo::Clusters::Range r ) const override {
    return process( h, r, nullptr );
  }

private:
  ToolHandle<ICaloFutureDigitFilterTool>        m_pileup{this, "FilterTool", "CaloFutureDigitFilterTool/FilterTool"};
  ToolHandle<LHCb::Calo::Interfaces::IElectron> m_caloElectron{this, "ElectronTool", "CaloFutureElectron"};

  struct ECorrInputParams {
    LHCb::CaloCellID cellID;
    Gaudi::XYZPoint  seedPos;
    double           x      = 0;
    double           y      = 0;
    double           z      = 0;
    double           eEcal  = 0;
    double           dtheta = 0;
    unsigned int     area   = 0;
  };

  struct ECorrOutputParams {
    double eCor = 0;
    // output Jacobian elements returned from calcECorrection() to process()
    double dEcor_dXcl = 0;
    double dEcor_dYcl = 0;
    double dEcor_dEcl = 0;

    // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
    double alpha = 0;
    double Asx   = 0;
    double Asy   = 0;
    double aG    = 0;
    double aE    = 0;
    double aB    = 0;
    double aX    = 0;
    double aY    = 0;
    double gT    = 0;
  };

  ECorrOutputParams calcECorrection( const ECorrInputParams& params ) const;

  /// debugging necessary in case if any new corrections are added or their sequence is changed!
  void debugDerivativesCalculation( const ECorrInputParams& inParams, const ECorrOutputParams& outParams ) const;

private:
  /**
   * Update the covariance matrix of the calo hypothesis
   * @param dEcor_dXcl jacobian element calculated for x
   * @param dEcor_dycl jacobian element calculated for y
   * @param dEcor_decl jacobian element calculated for energy
   * @param hypo hypothesis to be modified
   **/
  void updateCovariance( double dEcor_dXcl, double dEcor_dYcl, double dEcor_dEcl,
                         LHCb::Event::Calo::Clusters::reference ) const;
  /**
   * Update the position of the calo hypothesis
   * @param Ecor corrected energy to be applied
   * @param hypo hypothesis to be modified
   **/
  bool   isNotSeed( const LHCb::CaloDigit* seed ) const;
  double computeDTheta( LHCb::Event::Calo::Clusters::const_reference         cluster,
                        const LHCb::CaloFuture2Track::ICluster2TrackTable2D& ctable ) const;

  mutable IncCounter m_counterSkippedNegativeEnergyCorrection{this, "Skip negative energy correction"};

  mutable SCounter m_counterPileupOffset{this, "Pileup offset"};
  mutable SCounter m_counterPileupSubstractedRatio{this, "Pileup subtracted ratio"};
  mutable SCounter m_counterPileupScale{this, "Pileup scale"};

  mutable IncCounter m_counterUnphysical{this, "Unphysical d(Ehypo)/d(Ecluster)"};

  mutable IncCounter m_counterUnphysicalVariance{this, "Unphysical variance(Ehypo)"};

  mutable std::array<SCounter, k_numOfCaloAreas> m_countersAlpha = make_counters( this, "<alpha> " );
};

DECLARE_COMPONENT( CaloFutureECorrection )

CaloFutureECorrection::CaloFutureECorrection( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : extends( type, name, parent ) {

  // define conditionName
  const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
  if ( uName.find( "ELECTRON" ) != std::string::npos ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/ElectronECorrection" ).ignore();
  } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/SplitPhotonECorrection" ).ignore();
  } else if ( uName.find( "PHOTON" ) ) {
    setProperty( "ConditionName", "Conditions/Reco/Calo/PhotonECorrection" ).ignore();
  }
}

// ============================================================================

StatusCode CaloFutureECorrection::process( LHCb::Event::Calo::Hypotheses::Type                  hypo,
                                           LHCb::Event::Calo::Clusters::Range                   clusters,
                                           const LHCb::CaloFuture2Track::ICluster2TrackTable2D* ctable ) const {

  auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo );
  if ( m_hypos.end() == h ) { return Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS ); }

  auto counterSkippedNegativeEnergyCorrection = m_counterSkippedNegativeEnergyCorrection.buffer();
  auto counterPileupOffset                    = m_counterPileupOffset.buffer();
  auto counterPileupSubstractedRatio          = m_counterPileupSubstractedRatio.buffer();
  auto counterUnphysical                      = m_counterUnphysical.buffer();

  auto pileup_cache = m_pileup->pileup();
  m_counterPileupScale += pileup_cache;

  for ( auto&& cluster : clusters ) {

    if ( cluster.e() < 0. ) {
      ++counterSkippedNegativeEnergyCorrection;
      continue;
    }

    // Get position
    const auto&  position = cluster.position();
    double       eEcal    = cluster.e();
    const double xBar     = position.x();
    const double yBar     = position.y();

    // Cell ID for seed digit
    LHCb::CaloCellID cellID  = cluster.seed();
    Gaudi::XYZPoint  seedPos = m_det->cellCenter( cellID );

    double dtheta =
        ( ( LHCb::Event::Calo::Hypotheses::Type::EmCharged == hypo && ctable ) ? computeDTheta( cluster, *ctable )
                                                                               : 0. );

    // Pileup subtraction at the cluster level
    if ( m_pileup->method( CaloCellCode::CaloIndex::EcalCalo ) >= 10 ) {
      double offset = m_pileup->offset( cellID, pileup_cache );
      if ( offset < eEcal ) {
        const double eee = eEcal;
        eEcal -= offset;
        counterPileupOffset += offset;
        counterPileupSubstractedRatio += eEcal / eee;
      }
    }

    /** here all information is available
     *
     *  (1) Ecal energy in 3x3     :    eEcal
     *  ( ) Prs and Spd energies   :    ePrs, eSpd ( not used )
     *  (3) weighted barycenter    :    xBar, yBar
     *  (4) Zone/Area in Ecal      :    area
     *  (5) SEED digit             :    seed    (NO for split!)
     *  (6) CellID of seed digit   :    cellID  (OK for split!)
     *  (7) Position of seed cell  :    seedPos (OK for split!)
     *
     */
    ECorrInputParams params{cellID, seedPos, xBar, yBar, position.z(), eEcal, dtheta, cellID.area()};

    /////////////////////////////////////////////////////////

    /* Calculate corrected energy in a separate function call. Necessary for debugging the Jacobian by calculating
     * numeric derivatives w.r.t. (X, Y, E) in case of any changes in the correction code.
     *
     * Input positions and energies are passed as parameters for ease of numeric derivative calculation,
     * all the other paramers and results of intermediate calculations are shared between the two methods
     * using local ECorrInputParams _params, and [zero-initialized] ECorrOutputParams _results.
     */
    ECorrOutputParams results = calcECorrection( params );
    double            eCor    = results.eCor;

    // results of semi-analytic derivative calculation
    const double& dEcor_dXcl = results.dEcor_dXcl;
    const double& dEcor_dYcl = results.dEcor_dYcl;
    double&       dEcor_dEcl = results.dEcor_dEcl;

    // protection against unphysical d(Ehypo)/d(Ecluster) == 0
    if ( fabs( dEcor_dEcl ) < 1e-10 ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "unphysical d(Ehypo)/d(Ecluster) = " << dEcor_dEcl << " reset to 1 as if Ehypo = Ecluster" << endmsg;
      ++counterUnphysical;
      dEcor_dEcl = 1.;
    }

    // debugging necessary in case if any new corrections are added or their sequence is changed!
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      if ( m_correctCovariance ) debugDerivativesCalculation( params, results );
    }

    m_countersAlpha.at( cellID.area() ) += results.alpha;

    // update position
    cluster.e() = eCor;

    // ----------------------------------------- apply semi-analytic cov.m. propagation due to the (X,Y,E) corrections
    if ( m_correctCovariance ) { updateCovariance( dEcor_dXcl, dEcor_dYcl, dEcor_dEcl, cluster ); }
  }
  return StatusCode::SUCCESS;
}

void CaloFutureECorrection::updateCovariance( double dEcor_dXcl, double dEcor_dYcl, double dEcor_dEcl,
                                              LHCb::Event::Calo::Clusters::reference cluster ) const {

  auto                            counterUnphysicalVariance = m_counterUnphysicalVariance.buffer();
  LHCb::CaloPosition::Covariance& covariance                = cluster.covariance();

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { debug() << "before E-corr. cov.m. = \n" << covariance << endmsg; }

  // index numbering just follows ROOT::Math::SMatrix<double,3,3>::Array() for row/column indices (X:0, Y:1, E:2)
  double c0[6], c1[6];
  /*
   * Indexing following ROOT::Math::SMatrix<double,3,3,ROOT::Math::MatRepSym<double,3> >::Array() :
   *
   * The iterators access the matrix element in the order how they are
   * stored in memory. The C (row-major) convention is used, and in the
   * case of symmetric matrices the iterator spans only the lower diagonal
   * block. For example for a symmetric 3x3 matrices the order of the 6
   * elements \f${a_0,...a_5}\f$ is:
   * \f[
   * M = \left( \begin{array}{ccc}
   *     a_0 & a_1 & a_3  \\
   *     a_1 & a_2  & a_4  \\
   *     a_3 & a_4 & a_5   \end{array} \right)
   * \f]
   */
  c0[0] = covariance( LHCb::CaloPosition::Index::X,
                      LHCb::CaloPosition::Index::X ); // arr[0] not relying on LHCb::CaloFuturePosition::Index::X == 0
  c0[1] = covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ); // arr[1]
  c0[2] = covariance( LHCb::CaloPosition::Index::Y,
                      LHCb::CaloPosition::Index::Y ); // arr[2] not relying on LHCb::CaloFuturePosition::Index::Y == 1
  c0[3] = covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ); // arr[3]
  c0[4] = covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ); // arr[4]
  c0[5] = covariance( LHCb::CaloPosition::Index::E,
                      LHCb::CaloPosition::Index::E ); // arr[5] not relying on LHCb::CaloFuturePosition::Index::E == 2

  // cov1 = (J * cov0 * J^T) for the special case of Jacobian for (X,Y,E) -> (X1=X, Y1=Y, E1=E(X,Y,E))
  c1[0]      = c0[0];
  c1[1]      = c0[1];
  c1[2]      = c0[2];
  c1[3]      = c0[0] * dEcor_dXcl + c0[1] * dEcor_dYcl + c0[3] * dEcor_dEcl;
  c1[4]      = c0[1] * dEcor_dXcl + c0[2] * dEcor_dYcl + c0[4] * dEcor_dEcl;
  double tmp = c0[3] * dEcor_dXcl + c0[4] * dEcor_dYcl + c0[5] * dEcor_dEcl;
  c1[5]      = c1[3] * dEcor_dXcl + c1[4] * dEcor_dYcl + tmp * dEcor_dEcl;

  // additional protection against cov.m.(E,E) <= 0 due to numerical effects
  if ( c1[5] < 1.e-10 ) {
    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
      debug() << "unphysical variance(Ehypo) = " << c1[5]
              << " reset cov.m.(Ehypo,*) = cov.m.(Ecluster,*) as if Ehypo = Ecluster" << endmsg;
    ++counterUnphysicalVariance;
    c1[5] = c0[5];
    c1[3] = c0[3];
    c1[4] = c0[4];
  }

  // finally update CaloHypo::position()->covariance()
  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) = c1[0]; // cov1(0,0);
  covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) = c1[2]; // cov1(1,1);
  covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E ) = c1[5]; // cov1(2,2);
  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ) = c1[1]; // cov1(0,1);
  covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ) = c1[3]; // cov1(0,2);
  covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ) = c1[4]; // cov1(1,2);

  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) { debug() << "after E-corr. cov.m. = \n" << covariance << endmsg; }
}

CaloFutureECorrection::ECorrOutputParams
CaloFutureECorrection::calcECorrection( const ECorrInputParams& _params ) const {
  // local aliases for the input variables passed from process() to calcECorrection()
  const LHCb::CaloCellID& cellID  = _params.cellID;
  const Gaudi::XYZPoint&  seedPos = _params.seedPos;
  const double&           dtheta  = _params.dtheta;
  const unsigned int&     area    = _params.area;
  double                  xBar    = _params.x;
  double                  yBar    = _params.y;
  double                  eEcal   = _params.eEcal;

  double CellSize = m_det->cellSize( cellID );
  double Asx      = ( xBar - seedPos.x() ) / CellSize; // Asx0
  double Asy      = ( yBar - seedPos.y() ) / CellSize; // Asy0

  const double Asx0 = Asx;
  const double Asy0 = Asy;

  double bDist = sqrt( Asx * Asx + Asy * Asy ) * sqrt( 2. );

  int signX = shiftAs( cellID.col(), std::array{0, 0, 8}, area );
  int signY = shiftAs( cellID.row(), std::array{6, 12, 14}, area );
  Asx *= signX; // Asx1
  Asy *= signY; // Asy1

  // analytic derivatives of the correction functions
  double DaE( 0 ), DaB( 0 ), DaX( 0 ), DaY( 0 );

  //
  // apply corrections
  // NB: numeric derivative calculation calls and printouts which are commented-out below
  // are useful for debugging in case of changes in the correction function code
  //
  //// aG = const(X,Y,E), no need to calculate derivatives
  double aG = getCorrection( CaloFutureCorrection::alphaG, cellID ).value_or( 1. ); // global Ecal factor
  //// aE = alphaE(eEcal)
  const auto aECorDer = getCorrectionAndDerivative( CaloFutureCorrection::alphaE, cellID, eEcal )
                            .value_or( CorrectionResult{1., 0.} ); // longitudinal leakage
  const auto aE       = aECorDer.value;
  const auto aBCorDer = getCorrectionAndDerivative( CaloFutureCorrection::alphaB, cellID, bDist )
                            .value_or( CorrectionResult{1., 0.} ); // lateral leakage
  const auto aB = aBCorDer.value;
  //// aX = alphaX(Asx1)
  const auto aXCorDer = getCorrectionAndDerivative( CaloFutureCorrection::alphaX, cellID, Asx )
                            .value_or( CorrectionResult{1., 0.} ); // module frame dead material X-direction
  const auto aX = aXCorDer.value;
  //// aY = alphaY(Asy1)
  const auto aYCorDer = getCorrectionAndDerivative( CaloFutureCorrection::alphaY, cellID, Asy )
                            .value_or( CorrectionResult{1., 0.} ); // module frame dead material Y-direction
  const auto aY = aYCorDer.value;
  if ( m_correctCovariance ) {
    DaE = aECorDer.derivative;
    DaB = aBCorDer.derivative;
    DaX = aXCorDer.derivative;
    DaY = aYCorDer.derivative;
  }

  // angular correction
  // assume dtheta to be independent of X,Y,E, although it may still be implicitly a bit dependent on X,Y
  double gT = getCorrection( CaloFutureCorrection::globalT, cellID, dtheta ).value_or( 1. ); // incidence angle (delta)
  double dT = getCorrection( CaloFutureCorrection::offsetT, cellID, dtheta ).value_or( 0. ); // incidence angle (delta)

  // Energy offset
  double sinT   = m_det->cellSine( cellID );
  double offset = getCorrection( CaloFutureCorrection::offset, cellID, sinT ).value_or( 0. );

  // Apply Ecal leakage corrections
  double alpha = aG * aE * aB * aX * aY;
  double eCor  = eEcal * alpha * gT + dT + offset;

  /* DG,20190421: derivative calculation simplified by removal of SPD and PRS
   *
   * Asx0  = (Xcluster-seedPos.x)/CellSize
   * bDist = sqrt(2)*sqrt(Asx0**2+Asy0**2)
   * signX = signX(cellID); // const(X,Y,Ecluster)
   * Asx1  = signX*Asx0
   * eEcal = Ecluster - pileup_offset(cellID, eSpd = 0); // => d(eEcal)/d(Ecluster) = 1
   * aG    = alphaG(cellID); // const(X,Y, Ecluster)
   * aE    = alphaE(eEcal)
   * aB    = alphaB(bDist)
   * aX    = alphaX(Asx1)
   * aY    = alphaY(Asy1)
   * gT    = globalT(dtheta); // const(X,Y,Ecluster) although dtheta may indirectly depend on (X,Y)
   * dT    = offsetT(dtheta); // const(X,Y,Ecluster)
   * sinT  = cellSince(cellID); // const(X,Y,Ecluster)
   * offset= offset(cellID, sinT); // const(X,Y,Ecluster) at eSpd = 0
   * gC    = 1; // at eSpd = 0
   *
   * d(Asx0)/dX       = +1/CellSize
   * d(Asx1)/d(Asx0)  = signX
   * d(bDist)/d(Asx0) = sqrt(2)*2*Asx0/2/sqrt(Asx0**2+Asy0**2) = 2*Asx0/bDist; // if bDist != 0, otherwise 0
   *   if bDist=0 <=> (Asx=0,Asy=0), but for any Asy!=0 (if Asx=0 => d(bDist)/d(Asx) = 0)
   *   => for continuity similarly define the same for Asy=0, i.e. if bDist=0 => d(bDist)/d(Asx) = 0
   *
   * d(aB)/dX             = d(aB)/d(bDist)*d(bDist)/d(Asx0)*d(Asx0)/dX = DalphpaB*(2*Asx0/bDist)*(1/CellSize)
   * d(aX)/dX             = d(aX)/d(Asx1)*d(Asx1)/d(Asx0)*d(Asx0)/dX = DalphaX*signX*(1/CellSize)
   * d(eEcal)/d(Ecluster) = 1
   *
   * alpha = aG * aE(eEcal) * aB(bDist) * aX(Asx1) * aY(Asy1);
   * Ehypo = eCor = eEcal * alpha(eEcal, bDist, Asx1, Asy1) * (gC = 1) * gT + dT + offset;
   *
   * d(alpha)/d(eEcal) = (aG*aB*aX*aY) * d(aE)/d(eEcal) = (alpha/aE) * DalphaE; // if aE!=0, otherwise
   * aG*aB*aX*aY*DalphaE
   *
   *
   * d(Ehypo)/d(Ecluster) = gT*(eEcal*d(alpha)/d(eEcal) + alpha) = gT * alpha * (1. + DaE / aE * eEcal)
   * d(Ehypo)/d(Xcluster) = gT*eEcal*d(alpha)/dX = gT*eEcal*aG*aE*aY*(d(aB)/dX*aX+d(aX)/dX*aB)
   *                      = gT * eEcal * aG * aE * aY * (DaB*2.*Asx0/bDist*aX + signX*aB*DaX)/CellSize
   * d(Ehypo)/d(Ycluster) = [ same as for d(Ehypo)/d(Xcluster) with ( X <-> Y ) ]
   * 			  = gT * eEcal * aG * aE * aX * (DaB*2.*Asy0/bDist*aY + signY*aB*DaY)/CellSize
   */

  CaloFutureECorrection::ECorrOutputParams results;
  if ( m_correctCovariance ) {
    double d_alpha_dE =
        ( aE != 0 ) ? DaE * alpha / aE : DaE * aG * aB * aX * aY; // though in principle, aE should never be 0

    results.dEcor_dEcl = gT * ( alpha + d_alpha_dE * eEcal );

    results.dEcor_dXcl = gT * eEcal * aG * aE * aY *
                         ( ( bDist == 0 ? 0. : DaB * 2. * Asx0 / bDist * aX ) + signX * aB * DaX ) / CellSize;

    results.dEcor_dYcl = gT * eEcal * aG * aE * aX *
                         ( ( bDist == 0 ? 0. : DaB * 2. * Asy0 / bDist * aY ) + signY * aB * DaY ) / CellSize;
  }

  results.alpha = alpha;

  // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
    results.Asx = Asx; // Asx1
    results.Asy = Asy; // Asy1
    results.aG  = aG;
    results.aE  = aE;
    results.aB  = aB;
    results.aX  = aX;
    results.aY  = aY;
    results.gT  = gT;
  }
  results.eCor = eCor;
  return results;
}

void CaloFutureECorrection::debugDerivativesCalculation( const ECorrInputParams&  inParams,
                                                         const ECorrOutputParams& outParams ) const {
  const double dx_rel( 1e-5 ), dy_rel( 1e-5 ), de_rel( 1e-3 ); // dx,dy ~ few*0.1*mm, de ~ few MeV
  double       xBar       = inParams.x;
  double       yBar       = inParams.y;
  double       eEcal      = inParams.eEcal;
  double       eCor       = outParams.eCor;
  double       dEcor_dXcl = outParams.dEcor_dXcl;
  double       dEcor_dYcl = outParams.dEcor_dYcl;
  double       dEcor_dEcl = outParams.dEcor_dEcl;

  debug() << "\n ------------------------ ECorrection(x+dx, y, e) calculation follows ------------------- " << endmsg;
  ECorrInputParams inParams1( inParams );
  inParams1.x       = inParams1.x * ( 1 + dx_rel );
  auto   outParams1 = calcECorrection( inParams1 );
  double eCor_x     = outParams1.eCor;

  debug() << "\n ------------------------ ECorrection(x, y+dy, e) calculation follows ------------------- " << endmsg;
  ECorrInputParams inParams2( inParams );
  inParams2.y       = inParams2.y * ( 1 + dy_rel );
  auto   outParams2 = calcECorrection( inParams2 );
  double eCor_y     = outParams2.eCor;

  debug() << "\n ------------------------ ECorrection(e, y, e+de) calculation follows ------------------- " << endmsg;
  ECorrInputParams inParams3( inParams );
  inParams3.eEcal   = inParams2.eEcal * ( 1 + de_rel );
  auto   outParams3 = calcECorrection( inParams3 );
  double eCor_e     = outParams3.eCor;

  double dn_eCor_dx = ( eCor_x - eCor ) / xBar / dx_rel;
  double dn_eCor_dy = ( eCor_y - eCor ) / yBar / dy_rel;
  double dn_eCor_de = ( eCor_e - eCor ) / eEcal / de_rel;

  // avoid division in comparison for possible dE/dX == 0 or dE/dY == 0
  if ( fabs( dEcor_dXcl - dn_eCor_dx ) > fabs( dEcor_dXcl ) * 0.1 ||
       fabs( dEcor_dYcl - dn_eCor_dy ) > fabs( dEcor_dYcl ) * 0.1 ||
       fabs( dEcor_dEcl - dn_eCor_de ) > fabs( dEcor_dEcl ) * 0.1 ) {
    debug() << " some CaloFutureECorrection analytically-calculated Jacobian elements differ (by > 10%) from "
               "numerically-calculated ones! "
            << endmsg;
  }

  debug() << "********** Jacobian elements J(2,*) =" << endmsg;
  debug() << "   semi-analytic dEcor_dXcl = " << dEcor_dXcl << " numeric dn_eCor_dx = " << dn_eCor_dx << endmsg;
  debug() << "   semi-analytic dEcor_dYcl = " << dEcor_dYcl << " numeric dn_eCor_dy = " << dn_eCor_dy << endmsg;
  debug() << "   semi-analytic dEcor_dEcl = " << dEcor_dEcl << " numeric dn_eCor_de = " << dn_eCor_de << endmsg;
}

double CaloFutureECorrection::computeDTheta( LHCb::Event::Calo::Clusters::const_reference         cluster,
                                             const LHCb::CaloFuture2Track::ICluster2TrackTable2D& ctable ) const {

  const auto range = ctable.relations( cluster.seed() );
  if ( range.empty() ) return 0;
  //  incidence angle charged
  const LHCb::Track* ctrack    = range.front();
  const auto         calostate = m_caloElectron->caloState( *ctrack );
  const double       incidence =
      ( calostate.location() != LHCb::State::Location::LocationUnknown ? calostate.momentum().Theta() : 0 );
  auto cMomentum = LHCb::Calo::Momentum( cluster );
  return incidence - cMomentum.momentum().Theta();
}
