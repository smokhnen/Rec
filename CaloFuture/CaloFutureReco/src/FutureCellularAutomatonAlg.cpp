/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "CaloFutureUtils/CellMatrix.h"
#include "CaloFutureUtils/CellNeighbour.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloKernel/CaloVector.h"
#include "CellMatrix2x2.h"
#include "CellMatrix3x3.h"
#include "CellSwissCross.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "DetDesc/IGeometryInfo.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CellID.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/compose.h"
#include "Kernel/CaloCellID.h"
#include "boost/container/static_vector.hpp"
#include <iomanip>
#include <optional>
#include <string>
#include <variant>

/** @class FutureCellularAutomatonAlg FutureCellularAutomatonAlg.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */

namespace LHCb::Calo {
  namespace {

    const std::string s_3x3        = "3x3";
    const std::string s_2x2        = "2x2";
    const std::string s_SwissCross = "SwissCross";
    const std::string s_Neighbour  = "Neighbour";
    const std::string s_invalid    = "INVALID";

    class CellSelector final : public CellMatrix {
      std::variant<std::monostate, CellMatrix3x3, CellMatrix2x2, CellSwissCross, CellNeighbour> m_selector;

    public:
      enum class Selector { s3x3, s2x2, SwissCross, Neighbour };
      friend StatusCode         parse( Selector&, const std::string& );
      friend const std::string& toString( Selector s ) {
        switch ( s ) {
        case Selector::s3x3:
          return s_3x3;
        case Selector::s2x2:
          return s_2x2;
        case Selector::SwissCross:
          return s_SwissCross;
        case Selector::Neighbour:
          return s_Neighbour;
        }
        return s_invalid;
      }
      friend std::ostream& toStream( Selector s, std::ostream& os ) { return os << std::quoted( toString( s ), '\'' ); }

      CellSelector( const DeCalorimeter* det = nullptr, Selector = Selector{-1} );

      double operator()( LHCb::CaloCellID seed, LHCb::CaloCellID cell ) const {
        return std::visit(
            Gaudi::overload( [&]( const auto& s ) { return s( seed, cell ); }, []( std::monostate ) { return 1.; } ),
            m_selector );
      };
    };

    CellSelector::CellSelector( const DeCalorimeter* det, CellSelector::Selector selector ) : CellMatrix( det ) {
      switch ( selector ) {
      case Selector::s3x3:
        m_selector.emplace<CellMatrix3x3>( det );
        break;
      case Selector::s2x2:
        m_selector.emplace<CellMatrix2x2>( det );
        break;
      case Selector::SwissCross:
        m_selector.emplace<CellSwissCross>( det );
        break;
      case Selector::Neighbour:
        m_selector.emplace<CellNeighbour>( det );
        break;
      default:
        m_selector.emplace<std::monostate>();
        break;
      }
    }

    StatusCode parse( CellSelector::Selector& s, const std::string& str ) {
      constexpr auto values = std::array{CellSelector::Selector::s3x3, CellSelector::Selector::s2x2,
                                         CellSelector::Selector::SwissCross, CellSelector::Selector::Neighbour};
      auto           i = std::find_if( values.begin(), values.end(), [&]( auto v ) { return toString( v ) == str; } );
      return i != values.end() ? ( s = *i, StatusCode::SUCCESS ) : StatusCode::FAILURE;
    }

    // ============================================================================
    constexpr auto usedForE =
        CaloDigitStatus::Status{CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForCovariance};
    constexpr auto usedForP =
        CaloDigitStatus::Status{CaloDigitStatus::Mask::UseForPosition, CaloDigitStatus::Mask::UseForCovariance};
    constexpr auto seed =
        ( usedForP | usedForE | CaloDigitStatus::Mask::SeedCell | CaloDigitStatus::Mask::LocalMaximum );
    // ============================================================================

    class CelAutoTaggedCell final {
      // ==========================================================================
      enum class Tag : char { DefaultFlag, Clustered, Edge };

      enum class FlagState : char { NotTagged, Tagged };
      // ==========================================================================
    public:
      // ==========================================================================
      // Constructor
      CelAutoTaggedCell() = default;
      CelAutoTaggedCell( CaloCellID id, double energy ) : m_id{id}, m_energy{energy} {}
      // ==========================================================================
      // Getters
      LHCb::CaloCellID cellID() const { return m_id; }
      double           e() const { return m_energy; }
      bool             isEdge() const { return ( FlagState::Tagged == m_status ) && ( Tag::Edge == m_tag ); }
      bool             isClustered() const { return ( FlagState::Tagged == m_status ) && ( Tag::Clustered == m_tag ); }
      LHCb::CaloCellID seedForClustered() const { return m_seeds[0]; }
      const auto&      seeds() const { return m_seeds; }
      size_t           numberSeeds() const { return m_seeds.size(); }
      bool             isSeed() const { return m_seeds.size() == 1 && cellID() == m_seeds[0]; }
      bool             isWithSeed( LHCb::CaloCellID seed ) const {
        return m_seeds.end() != std::find( m_seeds.begin(), m_seeds.end(), seed );
      }
      Tag       tag() const { return m_tag; }
      FlagState status() const { return m_status; }
      // ==========================================================================
      // Setters
      void setIsSeed() {
        m_tag    = Tag::Clustered;
        m_status = FlagState::Tagged;
        m_seeds.push_back( cellID() );
      }
      // ==========================================================================
      void setEdge() { m_tag = Tag::Edge; }
      void setClustered() { m_tag = Tag::Clustered; }
      void setStatus() {
        if ( ( Tag::Edge == m_tag ) || ( Tag::Clustered == m_tag ) ) { m_status = FlagState::Tagged; }
      }
      void addSeed( const LHCb::CaloCellID& seed ) { m_seeds.push_back( seed ); }
      // ==========================================================================
    private:
      // ==========================================================================
      CaloCellID m_id;
      double     m_energy = 0;
      Tag        m_tag    = Tag::DefaultFlag;
      FlagState  m_status = FlagState::NotTagged;
      // ==========================================================================
      // Ident.seed(s)
      boost::container::static_vector<LHCb::CaloCellID, 12> m_seeds;
      // ==========================================================================
    };

    auto isClustered       = []( const CelAutoTaggedCell* c ) { return c->isClustered(); };
    auto isClusteredOrEdge = []( const CelAutoTaggedCell* c ) { return c->isClustered() || c->isEdge(); };

    auto isWithSeed = []( LHCb::CaloCellID id ) {
      return [id]( const CelAutoTaggedCell* c ) { return c->isWithSeed( id ); };
    };

    auto setStatus = []( CelAutoTaggedCell* c ) { c->setStatus(); };

    template <typename Digit, typename Hits>
    bool isLocMax( const Digit& digit, const Hits& hits, const DeCalorimeter& det, double et_cut ) {
      double e  = digit.e();
      auto   et = LHCb::CaloDataFunctor::EnergyTransverse{&det};
      double eT = et( &digit );
      for ( const auto& i : det.neighborCells( digit.cellID() ) ) {
        auto cell = hits[i];
        if ( !cell ) { continue; }
        if ( cell->e() > e ) { return false; }
        eT += et( cell );
      }
      return eT >= et_cut;
    }

    template <typename Digit, typename Hits>
    bool isLocMax( const Digit& digit, const Hits& hits, const DeCalorimeter& det ) {
      const auto& neighbours = det.neighborCells( digit.cellID() );
      return std::none_of( neighbours.begin(), neighbours.end(), [e = digit.e(), &hits]( const auto& i ) {
        auto cell = hits[i];
        return cell && cell->e() > e;
      } );
    }

    // ============================================================================
    /* Application of rules of tagging on one cell
     *   - No action if no clustered neighbor
     *   - Clustered if only one clustered neighbor
     *   - Edge if more then one clustered neighbor
     */
    // ============================================================================

    template <typename Hits>
    void appliRulesTagger( CelAutoTaggedCell* cell, const Hits& hits, const DeCalorimeter* det, bool release ) {

      // Find in the neighbors cells tagged before, the clustered neighbors cells
      const LHCb::CaloCellID& cellID               = cell->cellID();
      const CaloNeighbors&    ns                   = det->neighborCells( cellID );
      bool                    hasEdgeNeighbor      = false;
      bool                    hasClusteredNeighbor = false;
      for ( const auto& iN : ns ) {
        const CelAutoTaggedCell* nei = hits[iN];
        if ( !nei ) { continue; }
        //
        if ( nei->isEdge() && release ) {
          hasEdgeNeighbor = true;
          for ( const auto& id : nei->seeds() ) {
            if ( !cell->isWithSeed( id ) ) cell->addSeed( id );
          }
        }
        //
        if ( !nei->isClustered() ) { continue; }
        hasClusteredNeighbor         = true;
        const LHCb::CaloCellID& seed = nei->seedForClustered();
        if ( cell->isWithSeed( seed ) ) { continue; }
        cell->addSeed( seed );
      }

      // Tag or not the cell

      switch ( cell->numberSeeds() ) {
      case 0:
        if ( !release ) break;
        if ( hasEdgeNeighbor && !hasClusteredNeighbor ) cell->setEdge(); //
        break;
      case 1:
        cell->setClustered();
        break;
      default:
        cell->setEdge();
        break;
      }
    }
  } // namespace

  class CellularAutomaton
      : public Gaudi::Functional::Transformer<LHCb::Event::Calo::Clusters( const DeCalorimeter&,
                                                                           const LHCb::Event::Calo::Digits& ),
                                              LHCb::DetDesc::usesConditions<DeCalorimeter>> {

  public:
    /// Standard constructor
    CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode finalize() override; ///< Algorithm finalization

    LHCb::Event::Calo::Clusters operator()( const DeCalorimeter&,
                                            const LHCb::Event::Calo::Digits& ) const override; ///< Algorithm execution

  private:
    Gaudi::Property<unsigned int>           m_neig_level{this, "Level", 0};
    Gaudi::Property<bool>                   m_withET{this, "withET", false};
    Gaudi::Property<double>                 m_ETcut{this, "ETcut", -10. * Gaudi::Units::GeV};
    Gaudi::Property<CellSelector::Selector> m_usedE{this, "CellSelectorForEnergy", CellSelector::Selector::s3x3};
    Gaudi::Property<CellSelector::Selector> m_usedP{this, "CellSelectorForPosition", CellSelector::Selector::s3x3};
    Gaudi::Property<unsigned int>           m_passMax{this, "MaxIteration", 10};

    mutable Gaudi::Accumulators::StatCounter<> m_clusters{this, "# clusters"};
    mutable Gaudi::Accumulators::StatCounter<> m_passes{this, "# clusterization passes"};
    mutable Gaudi::Accumulators::StatCounter<> m_negative{this, "Negative energy clusters"};

    mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{this, "Cluster energy"};
    mutable Gaudi::Accumulators::StatCounter<> m_negativeSeed{this, "Negative seed energy"};
    mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{this, "Cluster size"};
  };

  // ============================================================================
  // Standard constructor, initializes variables
  // ============================================================================
  CellularAutomaton::CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"Detector", Utilities::DeCaloFutureLocation( name )},
                      KeyValue{"InputData", Utilities::CaloFutureDigitLocation( name )}},
                     KeyValue{"OutputData", Utilities::CaloFutureClusterLocation( name, "EcalRaw" )} ) {}

  // ============================================================================
  //  Finalize
  // ============================================================================
  StatusCode CellularAutomaton::finalize() {
    info() << "Built <" << m_clusters.mean() << "> cellular automaton clusters/event  with <" << m_passes.mean()
           << "> iterations (min,max)=(" << m_passes.min() << "," << m_passes.max() << ") on average " << endmsg;
    return Transformer::finalize(); // must be called after all other actions
  }

  // ============================================================================
  // Main execution
  // ============================================================================
  Event::Calo::Clusters CellularAutomaton::
                        operator()( const DeCalorimeter& detector, const LHCb::Event::Calo::Digits& hits ) const ///< Algorithm execution
  {
    Event::Calo::Clusters clusters;
    clusters.reserveForEntries( hits.size() );
    // cmb: this needs to be done every time in order
    // to allow different detectors in the same algorithm
    // --> to be revisited in a future round
    auto addDigit =
        [useForE = CellSelector{&detector, m_usedE.value()}, useForP = CellSelector{&detector, m_usedP.value()}](
            auto begin, auto end, auto& digits, LHCb::CaloCellID seedID, LHCb::CaloDigitStatus::Status init ) {
          // transform!
          std::for_each( begin, end, [&, seedID = seedID, init = init]( const auto* i ) {
            LHCb::CaloCellID cellID = i->cellID();
            auto             status = init;
            if ( useForE( seedID, cellID ) > 0 ) status |= usedForE;
            if ( useForP( seedID, cellID ) > 0 ) status |= usedForP;
            digits.emplace_back( cellID, i->e(), status );
          } );
        };
    bool                  releaseBool = false;
    bool                  useData     = false;
    LHCb::CaloCellID::Set out_cells;

    /*

     level > 0   ->  All local maxima + neighborhood(level)
     level = 0   ->  All data (default)
    */

    // fill with data if level >0
    boost::container::static_vector<LHCb::CaloCellID, LHCb::Calo::Index::max()> cell_list;
    if ( m_neig_level > 0 ) {
      useData = true;
      for ( const auto& i : hits ) {
        const CaloNeighbors& neighbors = detector.neighborCells( i.cellID() );
        if ( std::none_of( neighbors.begin(), neighbors.end(), [&, e = i.energy()]( const auto& n ) {
               if ( !detector.valid( n ) ) return false;
               auto dig = hits( n );
               return dig && dig->energy() > e;
             } ) ) {
          cell_list.push_back( i.cellID() );
        }
      }
    }

    // if list of "seed" is not empty
    if ( !cell_list.empty() ) {
      out_cells.insert( cell_list.begin(), cell_list.end() );

      /** find all neighbours for the given set of cells for the givel level
       *  @param cells    (UPDATE) list of cells
       *  @param level    (INPUT)  level
       *  @param detector (INPUT) the detector
       *  @return true if neighbours are added
       */
      LHCb::CaloFutureFunctors::neighbours( out_cells, m_neig_level, &detector );
    }

    size_t local_size = cell_list.empty() ? hits.size() : out_cells.size();
    boost::container::static_vector<CelAutoTaggedCell, LHCb::Calo::Index::max()> local_cells;

    // Create access direct and sequential on the tagged cells
    /// container to tagged  cells with direct (by CaloCellID key)  access
    CaloVector<CelAutoTaggedCell*> taggedCellsDirect{nullptr};
    taggedCellsDirect.reserve( local_size );
    taggedCellsDirect.setSize( 14000 );

    /// container to tagged  cells with sequential access
    boost::container::static_vector<CelAutoTaggedCell*, LHCb::Calo::Index::max()> taggedCellsSeq;

    if ( cell_list.empty() ) { // fill with the data
      for ( const auto& digit : hits ) {
        auto& taggedCell = local_cells.emplace_back( digit.cellID(), digit.energy() );
        taggedCellsDirect.addEntry( &taggedCell, digit.cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    } else { // fill for HLT
      for ( const auto& icell : out_cells ) {
        auto digit = hits( icell );
        if ( !digit ) continue;
        auto& taggedCell = local_cells.emplace_back( digit->cellID(), digit->energy() );
        taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    }

    // Find and mark the seeds (local maxima)
    if ( useData ) {
      for ( const auto& seed : cell_list ) taggedCellsDirect[seed]->setIsSeed();
    } else if ( m_withET ) {
      for ( const auto& i : taggedCellsSeq ) {
        if ( isLocMax( *i, taggedCellsDirect, detector, m_ETcut.value() ) ) i->setIsSeed();
      }
    } else {
      for ( const auto& i : taggedCellsSeq ) {
        if ( isLocMax( *i, taggedCellsDirect, detector ) ) i->setIsSeed();
      }
    }

    /// Tag the cells which are not seeds
    auto itTagLastSeed = std::stable_partition( taggedCellsSeq.begin(), taggedCellsSeq.end(), isClustered );

    auto         itTagLastClustered = itTagLastSeed;
    auto         itTagFirst         = itTagLastClustered;
    unsigned int nPass              = 0;
    while ( itTagLastClustered != taggedCellsSeq.end() ) {

      // Apply rules tagger for all not tagged cells
      std::for_each( itTagLastClustered, taggedCellsSeq.end(),
                     [&]( auto* t ) { appliRulesTagger( t, taggedCellsDirect, &detector, releaseBool ); } );

      // Valid result
      std::for_each( itTagFirst, taggedCellsSeq.end(), setStatus );

      itTagLastClustered = std::stable_partition( itTagFirst, taggedCellsSeq.end(), isClusteredOrEdge );

      // Test if cells are tagged in this pass
      if ( itTagLastClustered == itTagFirst && releaseBool ) {
        const long number = taggedCellsSeq.end() - itTagLastClustered;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " TAGGING NOT FULL - Remain " << number << " not clustered cells" << endmsg;
        itTagLastClustered = taggedCellsSeq.end();
      }
      if ( itTagLastClustered == itTagFirst )
        releaseBool = true; // try additional passes releasing appliRulesTagger criteria
      nPass++;
      itTagFirst = itTagLastClustered;
      if ( m_passMax > 0 && nPass >= m_passMax ) break;
    }

    itTagLastClustered         = std::stable_partition( itTagLastSeed, taggedCellsSeq.end(), isClustered );
    auto itTagClustered1       = itTagLastSeed;
    auto clusterEnergy_counter = m_clusterEnergy.buffer();
    auto clusterSize_counter   = m_clusterSize.buffer();
    int  nNegative             = 0;
    for ( const auto& itTagSeed :
          LHCb::make_span( taggedCellsSeq ).first( std::distance( taggedCellsSeq.begin(), itTagLastSeed ) ) ) {
      if ( itTagSeed->e() <= 0 ) {
        m_negativeSeed += itTagSeed->e();
        continue; // do not make cluster if seed energy <= 0
      }
      LHCb::CaloCellID seedID = itTagSeed->cellID();
      // Do partitions first
      auto itTagClustered2 = std::stable_partition( itTagClustered1, itTagLastClustered, isWithSeed( seedID ) );
      auto itTagLastEdge   = std::stable_partition( itTagLastClustered, taggedCellsSeq.end(), isWithSeed( seedID ) );

      // first Seed, then Owned, and finally  Shared cells...
      int firstEntry = clusters.totalNumberOfEntries();
      clusters.emplace_back( itTagSeed->cellID(), itTagSeed->e(), seed );
      addDigit( itTagClustered1, itTagClustered2, clusters, seedID, LHCb::CaloDigitStatus::Mask::OwnedCell );
      addDigit( itTagLastClustered, itTagLastEdge, clusters, seedID, LHCb::CaloDigitStatus::Mask::SharedCell );
      auto exy = LHCb::CaloDataFunctor::calculateClusterEXY( clusters.range_of_entries( firstEntry ), &detector );
      if ( exy ) { //  put cluster to the output
        if ( exy->Etot < 0 ) {
          ++nNegative;
          continue; // skip negative E clusters
        }
        const auto& c = clusters.emplace_back( seedID, Event::Calo::Clusters::Type::CellularAutomaton,
                                               {firstEntry, clusters.totalNumberOfEntries() - firstEntry}, exy->Etot,
                                               {exy->x, exy->y, detector.cellCenter( seedID ).z()} );
        clusterEnergy_counter += c.energy();
        clusterSize_counter += c.size();
      }
      itTagClustered1 = itTagClustered2;
    }
    // statistics
    if ( nNegative > 0 ) m_negative += nNegative;
    m_passes += nPass;
    m_clusters += clusters.size();

    if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
      debug() << "Built " << clusters.size() << " cellular automaton clusters  with " << nPass << " iterations"
              << endmsg;
      debug() << " ----------------------- Cluster List : " << endmsg;
      for ( auto&& c : clusters ) {
        debug() << " Cluster seed " << c.seed() << " energy " << c.e() << " #entries " << c.size() << endmsg;
      }
    }
    return clusters;
  }
} // namespace LHCb::Calo
// ============================================================================
// Declaration of the Algorithm Factory
// ============================================================================
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::CellularAutomaton, "FutureCellularAutomatonAlg" )
// =============================================================================
// the END
// =============================================================================
