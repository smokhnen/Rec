/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloClusters_v2.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Parsers/CommonParsers.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToStream.h"
#include "ICaloFutureClusterTool.h"
#include "Kernel/CaloCellID.h"
#include "boost/container/small_vector.hpp"
#include "fmt/format.h"
#include <functional>
#include <iostream>
#include <vector>

// ============================================================================
/** @file
 *
 *  Implementation file for class FutureClusterCovarianceMatrixTool
 *
 *  @date 02/11/2001
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  modified 02/07/2014 by O. Deschamps
 */
// ============================================================================

/** @class FutureClusterCovarianceMatrixTool
 *         FutureClusterCovarianceMatrixTool.h
 *
 *  Concrete tool for calculation of covariance matrix
 *  for the whole cluster object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

/** @class CovarianceEstimator CovarianceEstimator.h
 *
 *  simple helper class for estimation of covariance matrix
 *  for CaloCluster object.
 *
 *   Model:   ("M{}" means "expectation", "D{}" means "dispersion")
 *
 *   All cluster quantities could be calculated if one knows the matrix
 *
 *   Cov{i,j} = M{(e(i)-M{e(i)})*(e(j)-M{e(j)})}
 *
 *   where "e(i)" is energy deposited in cell with index "i":
 *
 *   e(i) =  Sg * E(i)  + Gain * ( noise1 + noise2 )
 *
 *   where: "Sg"     - relative gain fluctuations;
 *          "E(i)"   - intrinsic energy (with stochastic flustuations);
 *          "Gain"   - gain for given channel;
 *          "noise1" - incoherent noise;
 *          "noise2" - coherent noise;
 *
 *   This model results in following exporession for covariance matrix:
 *   (diagonal elements)
 *
 *   Cov{i,i} =   D{e(i)} = M{(e(i)-M{e(i)})*(e(i)-M{e(i)})}
 *
 *            =   E(i)      * E(i)      * D{Sg}     +
 *                M{Sg}     * M{Sg}     * D{E(i)}   +
 *                M{Gain}   * M{Gain}   * D{noise1} +
 *                M{Gain}   * M{Gain}   * D{noise2} +
 *                M{noise1} * M{noise1} * M{Gain}   +
 *                M{noise2} * M{noise2} * M{Gain}
 *
 *   for non-diagonal elements (i != j ) one has:
 *
 *   Cov{i,j} =   Cov{j,i} = M{(e(i)-M{e(i)})*(e(j)-M{e(j)})}
 *
 *            =   M{ Gain } * M{ Gain } * D(noise2)
 *
 *   According to the obvious definitions:
 *
 *      "M{ Sg     }"     = 1
 *
 *      "M{ noise1 }" = 0, "mean" value of noise is sero
 *
 *      "M{ noise2 }" = 0, "mean" value of noise is zero
 *
 *
 *   other parameters could be extractes from external parametrisation:
 *
 *      "D{ E(i)   }" = E(i) * "A" * "A" * GeV
 *
 *      "D{ Sg     }" = relative gain sigma squared
 *
 *      "D{ noise1 }" = sigma of incoherent noise squared
 *
 *      "D{ noise2 }" = sigma on   coherent noise squared
 *
 *      "M{ Gain   }" is extracted for each cell from DeCalorimeter object
 *
 *  At next step one is able to calculate calculate intermediate values:
 *
 *     Total cluster energy         "Etot" = Sum_i{ 1.0  * e(i) }
 *
 *     Energy weighted X            "Ex"   = Sum_i{ x(i) * e(i) }
 *
 *     Energy weighted Y            "Ey"   = Sum_i{ y(i) * e(i) }
 *
 *  Since transformation from "e(i)" to ("Etot","Ex","Ey")
 *  id a linear transformation,  the covariance matrix
 *  for ("Etot","Ex","Ey") quantities could be calculated
 *  in an easy and transparent way:
 *
 *  Cov{ Etot, Etot } = Sum_ij { 1.0  * Cov{i,j} * 1.0  }
 *
 *  Cov{ Etot, Ex   } = Sum_ij { x(i) * Cov{i,j} * 1.0  }
 *
 *  Cov{ Ex  , Ex   } = Sum_ij { x(i) * Cov{i,j} * x(i) }
 *
 *  Cov{ Etot, Ey   } = Sum_ij { y(i) * Cov{i,j} * 1.0  }
 *
 *  Cov{ Ex  , Ey   } = Sum_ij { x(i) * Cov{i,j} * y(i) }
 *
 *  Cov{ Ey  , Ey   } = Sum_ij { y(i) * Cov{i,j} * y(i) }
 *
 *  And the last step: we calculate the final quantities:
 *
 *   total energy of cluster:  "Ecl" = Etot
 *
 *   X-position of barycenter: "Xcl" = Ex/Etot
 *
 *   Y-position of barycenter: "Ycl" = Ey/Etot
 *
 *  The calculation of covariance matrix for final values
 *  is a little bit tedious, since transformation from
 *  ("Etot","Ex","Ey") to ("Ecl","Xcl","Ycl") is not linear,
 *  but it could be done using analytical expansion:
 *
 *  Cov{ Ecl , Ecl } =              Cov{ Etot , Etot }
 *
 *  Cov{ Ecl , Xcl } =              Cov{ Etot , Ex   } / Ecl -
 *                            Xcl * Cov{Etot,Etot}/Ecl
 *
 *  Cov{ Xcl , Xcl } =              Cov{ Ex   , Ex   } / Ecl / Ecl +
 *                      Xcl * Xcl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ex   } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ex   } / Ecl / Ecl
 *
 *  Cov{ Ecl , Ycl } =              Cov{ Etot , Ey   } / Ecl -
 *                            Ycl * Cov{ Etot , Etot } / Ecl
 *
 *  Cov{ Xcl , Ycl } =              Cov{ Ex   , Ey   } / Ecl / Ecl +
 *                      Xcl * Ycl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ey   } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ex   } / Ecl / Ecl
 *
 *  Cov{ Ycl , Ycl } =              Cov{ Ey   , Ey   } / Ecl / Ecl +
 *                      Ycl * Ycl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ey   } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ey   } / Ecl / Ecl
 *
 *  @author Ivan Belyaev
 *  @date   06/07/2001
 */
namespace {
  enum Parameter {
    Stochastic      = 0, // stochastig term     Cov(EE)_i +=  [ S  * sqrt(E_i_GeV) ]^2
    GainError       = 1, // constant term       Cov(EE)_i +=  [ G  * E_i           ]^2
    IncoherentNoise = 2, // noise (inc.) term   Cov(EE)_i +=  [ iN * gain_i        ]^2
    CoherentNoise   = 3, // noise (coh.) term   Cov(EE)_i +=  [ cN * gain_i        ]^2
    ConstantE       = 4, // additional term     Cov(EE) +=  [ cE               ]^2
    ConstantX       = 5, // additional term     Cov(XX) +=  [ cX               ]^2
    ConstantY       = 6, // additional term     Cov(XX) +=  [ cY               ]^2
    Last
  };
  struct Parameters {
    struct Iterator {
      unsigned            i;
      constexpr Parameter operator*() noexcept { return static_cast<Parameter>( i ); }
      constexpr Iterator& operator++() noexcept {
        ++i;
        return *this;
      }
      constexpr friend bool operator!=( Iterator lhs, Iterator rhs ) { return lhs.i != rhs.i; }
      constexpr friend int  operator-( Iterator lhs, Iterator rhs ) {
        return static_cast<int>( lhs.i ) - static_cast<int>( rhs.i );
      }
    };
    constexpr static auto begin() { return Iterator{0}; }
    constexpr static auto end() { return Iterator{Parameter::Last}; }
    constexpr static auto size() { return Parameter::Last; }

    static const char* name( Parameter p ) {
      constexpr auto names = std::array{"Stochastic", "GainError", "IncoherentNoise", "CoherentNoise",
                                        "ConstantE",  "ConstantX", "ConstantY"};
      return names[p];
    }
    static const char* unit( Parameter p ) {
      constexpr auto units = std::array{"Sqrt(GeV)", "", "ADC", "ADC", "MeV", "mm", "mm"};
      return units[p];
    }
  };

  Parameter to_parameter( std::string_view sv ) {
    auto ps = Parameters{};
    return *std::find_if( ps.begin(), ps.end(), [&]( auto p ) { return Parameters::name( p ) == sv; } );
  }

  class ParameterMap {
    std::bitset<Parameters::size()>                     m_present;
    std::array<std::vector<double>, Parameters::size()> m_params; // TODO: replace vector by static_vector<double,
                                                                  // maxareas>

  public:
    [[nodiscard]] bool                       empty() const { return m_present.none(); }
    [[nodiscard]] bool                       contains( Parameter p ) const { return m_present.test( p ); }
    [[nodiscard]] const std::vector<double>& at( Parameter p ) const {
      if ( UNLIKELY( !contains( p ) ) )
        throw std::out_of_range{fmt::format( "Parameter '{}' not present", Parameters::name( p ) )};
      return m_params[p];
    }
    [[nodiscard]] std::vector<double>& operator[]( Parameter p ) {
      if ( !contains( p ) ) insert( p, {} );
      return m_params[p];
    }
    void insert( Parameter p, std::initializer_list<double> i ) {
      m_params[p].assign( i );
      m_present.set( p );
    }
    friend StatusCode parse( ParameterMap& p, const std::string& s ) {
      std::map<std::string, std::vector<double>> m;
      return Gaudi::Parsers::parse( m, s ).andThen( [&]() -> StatusCode {
        for ( auto& [k, v] : m ) {
          auto idx = to_parameter( k );
          if ( idx == Parameter::Last ) return StatusCode::FAILURE;
          p.m_params[idx]  = std::move( v );
          p.m_present[idx] = true;
        }
        return StatusCode::SUCCESS;
      } );
    }
    friend std::string toString( ParameterMap const& pm ) {
      std::map<std::string, std::vector<double>> m;
      for ( auto p : Parameters{} ) {
        if ( pm.contains( p ) ) m.insert( {Parameters::name( p ), pm.at( p )} );
      }
      return Gaudi::Utils::toString( m );
    }
    friend std::ostream& operator<<( std::ostream& os, ParameterMap const& pm ) { return os << toString( pm ); }
  };
} // namespace

namespace std {
  template <>
  struct iterator_traits<Parameters::Iterator> {
    using difference_type   = int;
    using iterator_category = std::random_access_iterator_tag;
  };
} // namespace std

namespace LHCb::Calo {
  class ClusterCovarianceMatrixTool : public extends<GaudiTool, Interfaces::IClusterTool> {
  public:
    ClusterCovarianceMatrixTool( const std::string& type, const std::string& name, const IInterface* parent );
    StatusCode initialize() override;
    StatusCode operator()( LHCb::Event::Calo::Clusters::Range cluster ) const override;

  private:
    StatusCode setEstimatorParams_wrapper() {
      setEstimatorParams( false );
      return StatusCode::SUCCESS;
    }

    StatusCode getParamsFromOptions();
    StatusCode getParamsFromDB();
    void       setEstimatorParams( bool init = false );

    const DeCalorimeter*                 m_det = nullptr;
    std::bitset<Parameters::size()>      m_source_is_db;
    Gaudi::Property<ParameterMap>        m_parameters{this, "Parameters"};
    Gaudi::Property<std::string>         m_detData{this, "Detector"};
    Gaudi::Property<bool>                m_useDB{this, "UseDBParameters", true};
    Gaudi::Property<std::string>         m_conditionName{this, "ConditionName"};
    ToolHandle<CaloFutureCorrectionBase> m_dbAccessor = {this, "CorrectionBase", "CaloFutureCorrectionBase/DBAccessor"};

    mutable Gaudi::Accumulators::Counter<>              m_parUpdate{this, "parameter updated"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_negative_energy{this, "negative energy cluster"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_failureUpdating{
        this, "Failed updating the covariance parameters from DB"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_parExceeded{
        this, "Parameters vector exceeds the number of known parameters"};

    /** calorimeter resolution (A*A*GeV)
     *  @return A*A*GeV resolution parameter
     */
    [[nodiscard]] double a2GeV( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( Stochastic );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()] * Gaudi::Units::GeV;
    }

    /** get dispersion  of relative gain error
     *  @return dispersion of relative gain error
     */
    [[nodiscard]] double s2gain( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( GainError );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    /** get  dispersion of noise (both coherent and incoherent
     *  @return overall noise dispersion
     */
    [[nodiscard]] double s2noise( const LHCb::CaloCellID id ) const { return s2incoherent( id ) + s2coherent( id ); }

    /** get the dispersion of incoherent noise
     *  @return dispersion of incoherent noise
     */
    [[nodiscard]] double s2incoherent( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( IncoherentNoise );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    /**  dispersion of coherent  noise
     *  @return dispersion of coherent noise
     */
    [[nodiscard]] double s2coherent( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( CoherentNoise );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2E( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( ConstantE );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2X( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( ConstantX );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2Y( const LHCb::CaloCellID id ) const {
      const auto& param = m_parameters.value().at( ConstantY );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

  }; ///< end of class FutureClusterCovarianceMatrixTool

  DECLARE_COMPONENT_WITH_ID( ClusterCovarianceMatrixTool, "FutureClusterCovarianceMatrixTool" )

  // ============================================================================
  /** Standard constructor
   *  @param type tool type (useless)
   *  @param name tool name
   *  @param parent pointer to parent object (service, algorithm or tool)
   */
  // ============================================================================
  ClusterCovarianceMatrixTool::ClusterCovarianceMatrixTool( const std::string& type, const std::string& name,
                                                            const IInterface* parent )
      : extends( type, name, parent ) {

    // set default configuration as a function of detector
    m_detData = LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name );
    auto det  = LHCb::CaloFutureAlgUtils::CaloIndexFromAlg( name );
    assert( det == CaloCellCode::CaloIndex::EcalCalo || det == CaloCellCode::CaloIndex::HcalCalo );
    m_conditionName = "Conditions/Reco/Calo/" + toString( det ) + "Covariance";

    // get parameters from parent property when defined
    decltype( m_parameters ) p( "CovarianceParameters", m_parameters.value() );
    if ( const IProperty* prop = dynamic_cast<const IProperty*>( parent );
         prop && prop->getProperty( &p ).isSuccess() && !p.value().empty() ) {
      m_parameters = p.value();
      m_useDB      = false; // parent default settings win !
    }

    // apply local parameters if not defined in parent algorithm
    auto push_if_not_present = [&]( const auto& key, auto value ) {
      if ( !m_parameters.value().contains( key ) ) m_parameters.value().insert( key, {value} );
    };

    const auto ecal_default = {std::pair{Stochastic, 0.10},      std::pair{GainError, 0.01},
                               std::pair{IncoherentNoise, 1.20}, std::pair{CoherentNoise, 0.30},
                               std::pair{ConstantE, 0.},         std::pair{ConstantX, 0.},
                               std::pair{ConstantY, 0.}};

    const auto hcal_default = {std::pair{Stochastic, 0.70},      std::pair{GainError, 0.10},
                               std::pair{IncoherentNoise, 1.20}, std::pair{CoherentNoise, 0.30},
                               std::pair{ConstantE, 0.},         std::pair{ConstantX, 0.},
                               std::pair{ConstantY, 0.}};

    if ( det == CaloCellCode::CaloIndex::EcalCalo ) {
      for ( const auto& [key, value] : ecal_default ) push_if_not_present( key, value );
    } else if ( det == CaloCellCode::CaloIndex::HcalCalo ) {
      for ( const auto& [key, value] : hcal_default ) push_if_not_present( key, value );
    }
  }

  //==============================================================================

  StatusCode ClusterCovarianceMatrixTool::getParamsFromOptions() {
    m_source_is_db.reset();
    unsigned int nareas = m_det->numberOfAreas();
    // check all expected parameters are defined
    for ( auto p : Parameters{} ) {
      if ( !m_parameters.value().contains( p ) ) {
        return Error( fmt::format( "No default value for parameter '{}'", Parameters::name( p ) ),
                      StatusCode::FAILURE );
      }
      auto& pars = m_parameters.value()[p];
      if ( pars.size() == 1 ) pars = std::vector<double>( nareas, pars[0] );
      if ( pars.size() != nareas ) return Error( "Parameters must be set for each calo area", StatusCode::FAILURE );
      m_source_is_db.reset( p );
    }
    return StatusCode::SUCCESS;
  }

  //------
  StatusCode ClusterCovarianceMatrixTool::getParamsFromDB() {
    // overwrite m_parameters using DB value
    if ( !m_useDB ) return StatusCode::SUCCESS;
    unsigned int nareas = m_det->numberOfAreas();
    m_source_is_db.reset();

    ParameterMap parameters;
    for ( unsigned int area = 0; area < nareas; ++area ) { // loop over calo area
      const auto& params =
          m_dbAccessor->getParamVector( CaloFutureCorrection::ClusterCovariance, CellID{m_det->index(), area, 0, 0} );
      if ( params.size() > Parameter::Last ) ++m_parExceeded;
      for ( auto index : Parameters{} ) {
        if ( index < params.size() ) {
          parameters[index].push_back( params[index] );
          m_source_is_db.set( index );
        } else {
          if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
            debug() << "Parameter '" << Parameters::name( index ) << "' not found in DB - use default options value"
                    << endmsg;
          if ( !m_parameters.value().contains( index ) )
            return Error( fmt::format( "No default value for parameter '{}'", Parameters::name( index ) ),
                          StatusCode::FAILURE );
          parameters[index].push_back( m_parameters[index][area] );
          m_source_is_db.reset( index );
        }
      }
    }
    m_parameters = std::move( parameters );

    return m_parameters.empty() ? StatusCode::FAILURE // no parameters set
                                : StatusCode::SUCCESS;
  }

  //-------
  void ClusterCovarianceMatrixTool::setEstimatorParams( bool init ) {
    // update DB parameters
    if ( !init && !m_useDB ) return; // estimator setting via options :  at initialization only
    if ( m_useDB && getParamsFromDB().isFailure() ) {
      ++m_failureUpdating;
      return; // failed to update parameters from DB
    }
    ++m_parUpdate;
  }

  //---------
  StatusCode ClusterCovarianceMatrixTool::initialize() {
    StatusCode sc = extends::initialize();
    if ( sc.isFailure() ) return sc;

    // get detector
    m_det = getDet<DeCalorimeter>( m_detData );

    // set DB accessor
    m_dbAccessor.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    if ( m_useDB &&
         ( m_conditionName.empty() || m_dbAccessor->setConditionParams( m_conditionName, true ).isFailure() ) )
      return Error( "Cannot access DB", StatusCode::FAILURE );

    // always set default parameters from options (will be updated by DB if requested)
    sc = getParamsFromOptions();

    if ( m_useDB ) {
      registerCondition( m_conditionName.value(), &ClusterCovarianceMatrixTool::setEstimatorParams_wrapper );
    }

    // configure estimator (possibly from DB if requested)
    setEstimatorParams( true ); // force initialization
    info() << " Has initialized with parameters: " << endmsg << " \t 'Detector'         = '" << m_detData.value() << "'"
           << endmsg << " \t ==  Parameters for covariance estimation ==" << endmsg;
    for ( auto index : Parameters{} ) {
      info() << Parameters::name( index ) << " \t : " << m_parameters.value()[index] << " " << Parameters::unit( index )
             << "\t : from " << ( m_source_is_db.test( index ) ? "Covariance DB" : "options" ) << endmsg;
    }
    return sc;
  }

  // ============================================================================
  StatusCode ClusterCovarianceMatrixTool::operator()( LHCb::Event::Calo::Clusters::Range clusters ) const {

    /// check the argument
    if ( !m_det ) return Error( "DeCalorimeter* points to NULL!" ); // TODO: consider moving to initialize
    /// apply the estimator
    for ( auto&& cluster : clusters ) {
      // ignore trivial cases
      if ( cluster.entries().empty() ) continue;

      auto               entries = cluster.entries();
      const unsigned int size    = entries.size();

      // auxillary arrays
      boost::container::small_vector<double, 32> x( size, 0 );    ///< x-position of cell [i]
      boost::container::small_vector<double, 32> y( size, 0 );    ///< y-position of cell [i]
      boost::container::small_vector<double, 32> gain( size, 0 ); ///< gain of cell[i]

      // calculate intermediate values
      //    eT = sum_i { 1.0  * e(i) }
      //    eX = sum_i { x(i) * e(i) }
      //    eY = sum_i { y(i) * e(i) }
      // and their covariance matrix

      double eTE  = 0;
      double eTP  = 0;
      double eTEP = 0;
      double eX   = 0;
      double eY   = 0;

      const LHCb::CaloCellID seedID = cluster.seed();
      // the matrices:
      double See   = s2E( seedID ); // add constant term to global cov(EE)
      double Sex   = 0;
      double Sxx   = s2X( seedID ); // cov(XX)_0
      double Sey   = 0;
      double Sxy   = 0;
      double Syy   = s2Y( seedID ); // cov(YY)_0
      double SeeP  = s2E( seedID );
      double SexEP = 0;
      double SeyEP = 0;
      double SeeEP = s2E( seedID );
      using namespace LHCb::CaloDigitStatus;

      for ( auto&& [i, entry] : LHCb::range::enumerate( entries ) ) {
        /// check the status
        if ( entry.status().anyOf(
                 {LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForPosition} ) )
          entry.addStatus( LHCb::CaloDigitStatus::Mask::UseForCovariance );
        else
          entry.removeStatus( LHCb::CaloDigitStatus::Mask::UseForCovariance );
        if ( !entry.status().test( LHCb::CaloDigitStatus::Mask::UseForCovariance ) ) continue;

        const LHCb::CaloCellID id       = entry.cellID();
        const auto             fraction = entry.fraction();
        const auto             energy   = entry.energy() * fraction;
        const auto             e_i      = energy;

        // get cell position
        const Gaudi::XYZPoint& pos = m_det->cellCenter( id );
        const auto             x_i = pos.x();
        const auto             y_i = pos.y();

        // intrinsic resolution
        auto s2 = std::abs( energy ) * a2GeV( id );
        //  gain fluctuation
        if ( auto s2g = s2gain( id ); s2g != 0 ) s2 += energy * energy * s2g;

        //  noise (both coherent and incoherent)
        double g = 0;
        if ( auto noise = s2noise( id ); noise != 0 ) {
          g = m_det->cellGain( id );
          s2 += noise * g * g;
        }

        bool forE  = entry.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
        bool forP  = entry.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition );
        bool forEP = forE && forP;

        if ( forE ) eTE += e_i;
        if ( forP ) {
          eTP += e_i;
          eX += x_i * e_i;
          eY += y_i * e_i;
        }
        if ( forEP ) eTEP += e_i;

        const auto s_ii = s2;

        if ( forE ) See += s_ii;
        if ( forP ) {
          SeeP += s_ii;
          Sxx += x_i * s_ii * x_i;
          Sxy += x_i * s_ii * y_i;
          Syy += y_i * s_ii * y_i;
          Sex += x_i * s_ii;
          Sey += y_i * s_ii;
        }
        if ( forEP ) {
          SeeEP += s_ii;
          SexEP += x_i * s_ii;
          SeyEP += y_i * s_ii;
        }

        // second loop if there exist correlations
        if ( 0 == s2coherent( id ) ) { continue; } ///<  CONTINUE
        x[i]    = x_i;
        y[i]    = y_i;
        gain[i] = g;
        for ( auto&& [j, jt] : LHCb::range::enumerate( entries.first( i ) ) ) {
          if ( !jt.status().test( LHCb::CaloDigitStatus::Mask::UseForCovariance ) ) continue;

          // position of cell "j"
          const auto x_j = x[j];
          const auto y_j = y[j];

          // covariance between cell "i" and "j"
          const auto s_ij = s2coherent( id ) * gain[i] * gain[j];

          bool jforE  = jt.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
          bool jforP  = jt.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition );
          bool jforEP = jforE && jforP;
          //
          if ( jforE ) See += 2.0 * s_ij;
          if ( jforP ) {
            SeeP += 2.0 * s_ij;
            Sxx += 2.0 * x_i * s_ij * x_j;
            Sxy += x_i * s_ij * y_j + x_j * s_ij * y_i;
            Syy += 2.0 * y_i * s_ij * y_j;
            Sex += x_i * s_ij + x_j * s_ij;
            Sey += y_i * s_ij + y_j * s_ij;
          }
          if ( jforEP ) {
            SeeEP += 2.0 * s_ij;
            SexEP += x_i * s_ij + x_j * s_ij;
            SeyEP += y_i * s_ij + y_j * s_ij;
          }
        } // end of loop over all digits/diagonal elements
      }   // loop over entries

      // does energy have a reasonable value?
      if ( eTE <= 0 ) cluster.energy() = 0;
      if ( eTP <= 0 ) cluster.position() = {0, 0, cluster.position().z()};
      if ( eTE <= 0 || eTP <= 0 ) {
        ++m_negative_energy;
        continue;
      }

      // The last step: calculate final quantities
      //   Ecl  =  eT
      //   Xcl  =  eX / eT
      //   Ycl  =  eY / eT

      const auto Ecl = eTE;
      const auto Xcl = eX / eTP;
      const auto Ycl = eY / eTP;

      // and their covariance matrix:
      const auto CovEE = See;
      const auto CovXX = ( Sxx + Xcl * Xcl * SeeP - 2.0 * Xcl * Sex ) / eTP / eTP;
      const auto CovYY = ( Syy + Ycl * Ycl * SeeP - 2.0 * Ycl * Sey ) / eTP / eTP;
      const auto CovXY = ( Sxy + Xcl * Ycl * SeeP - Ycl * Sex - Xcl * Sey ) / eTP / eTP;
      const auto CovEY = SeyEP / eTEP - Ycl * SeeEP / eTEP;
      const auto CovEX = SexEP / eTEP - Xcl * SeeEP / eTEP;

      // update cluster patameters
      cluster.position() = {Xcl, Ycl, cluster.position().z()};
      cluster.e()        = Ecl;

      // update cluster matrix
      LHCb::CaloPosition::Covariance& covariance                               = cluster.covariance();
      covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) = CovXX;
      covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::X ) = CovXY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::X ) = CovEX;
      covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) = CovYY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::Y ) = CovEY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E ) = CovEE;
    }

    return StatusCode::SUCCESS;
  }
} // namespace LHCb::Calo
// ============================================================================
