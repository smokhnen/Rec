/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloClusters_v2.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/TaggedBool.h"

/** @class ICaloFutureShowerOverlapTool ICaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-03
 */
namespace LHCb::Calo::Interfaces {

  struct IShowerOverlap : extend_interfaces<IAlgTool> {
    // Return the interface ID
    DeclareInterfaceID( IShowerOverlap, 1, 0 );

    using propagateInitialWeights = Gaudi::tagged_bool<struct propagateInitialWeights_tag>;

    // apply or not S and L corrections
    using applyCorrections = Gaudi::tagged_bool<struct applyCorrections_tag>;

    virtual void process( LHCb::Event::Calo::Clusters::reference c1, LHCb::Event::Calo::Clusters::reference c2,
                          int niter = 5, propagateInitialWeights = propagateInitialWeights{false},
                          applyCorrections = applyCorrections{true} ) const = 0;
  };

} // namespace LHCb::Calo::Interfaces
