/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigitStatus.h"
#include "GaudiKernel/IAlgTool.h"

/** @class ICaloFutureSubClusterTag ICaloFutureSubClusterTag.h
 *
 *
 *  @author Ivan Belyaev
 *  @date   01/04/2002
 */

namespace LHCb::Calo::Interfaces {
  struct ISubClusterTag : extend_interfaces<IAlgTool> {
    /** static interface identification
     *  @see IInterface
     *  @return unique interface identifier
     */
    DeclareInterfaceID( ISubClusterTag, 1, 0 );

    /** The main method
     *  @param cluster reference to CaloCluster object to be selected/tagged
     *  @return status code
     */
    virtual StatusCode tag( LHCb::Event::Calo::Clusters::Entries entries ) const = 0;

    virtual void                          setMask( LHCb::CaloDigitStatus::Status mask ) = 0;
    virtual LHCb::CaloDigitStatus::Status mask() const                                  = 0;

    /** The main method
     *  @param cluster reference to CaloCluster object to be untagged
     *  @return status code
     */
    virtual StatusCode untag( LHCb::Event::Calo::Clusters::Entries entries ) const = 0;
  };
} // namespace LHCb::Calo::Interfaces
// ============================================================================
