/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "GaudiKernel/IAlgTool.h"
#include <functional>
/** @class ICaloFutureHypoTool ICaloFutureHypoTool.h CaloFutureInterfaces/ICaloFutureHypoTool.h
 *
 *  The generic interface for "CalorimeterFuture tools" , which deals with
 *  CaloClusters objects, which depend on the assumed hypothesis. Possible examples include
 *
 *    \li hypothesis processing
 *    \li dispatching
 *    \li subcomponent of CaloFutureParticle processing
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   30/10/2001
 */
namespace LHCb::Calo::Interfaces {
  struct IProcessHypos : extend_interfaces<IAlgTool> {
    /** static interface identification
     *  @see IInterface
     *  @return unique interface identifier
     */
    DeclareInterfaceID( IProcessHypos, 1, 0 );

    /** The main processing method
     *  @param  hypo   type of hypothesis to use for corrections
     *  @param  hypos  range of clusters to be processed
     *  @return status code
     */

    virtual StatusCode process( Event::Calo::Hypotheses::Type, Event::Calo::Clusters::Range ) const = 0;

    /** process function patch -- with an additional ctable parameter that is used in the ECorrection
     *  @param  hypo   type of hypothesis to use for corrections
     *  @param  ctable to associate Clusters to Tracks
     *  @return status code
     */

    virtual StatusCode process( Event::Calo::Hypotheses::Type hypo, Event::Calo::Clusters::Range range,
                                const CaloFuture2Track::ICluster2TrackTable2D* ) const {
      return process( hypo, range );
    };
  };
} // namespace LHCb::Calo::Interfaces
