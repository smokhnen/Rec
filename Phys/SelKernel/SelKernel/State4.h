/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "SelKernel/ParticleAccessors.h"

#include "LHCbMath/MatVec.h"

namespace Sel {
  /** @class  StateVector4
   *  @brief  Class representing an {x, y, tx, ty} state vector at given z.
   *  @tparam float_v Type representing a [SIMD vector of] float values.
   *
   *  Similar to LHCb::StateVector, but missing q/p (not needed for vertexing)
   *  and templated to allow explicit vectorisation over multiple states.
   */
  template <typename float_v>
  struct StateVector4 {
  public:
    StateVector4() = default;

    template <typename state_t>
    StateVector4( state_t const& s ) : m_x{{s.x(), s.y()}}, m_t{{s.tx(), s.ty()}}, m_z{s.z()} {}

    template <typename XYZPoint, typename XYZVector>
    StateVector4( XYZPoint const& point, XYZVector const& direction )
        : m_x{{Sel::get::x( point ), Sel::get::y( point )}}
        , m_t{{Sel::get::x( direction ) / Sel::get::z( direction ),
               Sel::get::y( direction ) / Sel::get::z( direction )}}
        , m_z{Sel::get::z( point )} {}
    auto  x() const { return m_x( 0 ); }
    auto  y() const { return m_x( 1 ); }
    auto  z() const { return m_z; }
    auto  tx() const { return m_t( 0 ); }
    auto  ty() const { return m_t( 1 ); }
    auto& slopes() const { return m_t; }
    auto& x() { return m_x( 0 ); }
    auto& y() { return m_x( 1 ); }
    auto& z() { return m_z; }
    auto& tx() { return m_t( 0 ); }
    auto& ty() { return m_t( 1 ); }

  private:
    LHCb::LinAlg::Vec<float_v, 2> m_x, m_t;
    float_v                       m_z;
  };

  /** @class  State4
   *  @brief  Class representing an {x, y, tx, ty} state vector and covariance.
   *  @tparam float_v Type representing a [SIMD vector of] float values.
   *
   *  Similar to LHCb::State, but missing q/p + status flags, and templated to
   *  allow explicit vectorisation over states.
   *
   *  Used for Composite children (as opposed to Resonance) during in the
   *  ParticleVertexFitter.
   */
  template <typename float_v>
  struct State4 : public StateVector4<float_v> {
    auto&       covTT() { return m_covTT; }
    const auto& covTT() const { return m_covTT; }
    auto&       covXT() { return m_covXT; }
    const auto& covXT() const { return m_covXT; }
    auto&       covXX() { return m_covXX; }
    const auto& covXX() const { return m_covXX; }
    /** Return the 4x4 covariance matrix.
     *  Order: {x, y, tx, ty}
     */
    [[nodiscard]] auto posSlopeCovariance() const {
      LHCb::LinAlg::MatSym<float_v, 4> out;
      out = out.template place_at<0, 0>( m_covXX );
      out = out.template place_at<0, 2>( m_covXT );
      return out.template place_at<2, 2>( m_covTT );
    }

    // Copied from LHCb::State
    void linearTransportTo( float_v const& z ) {
      float_v const dz  = z - this->z();
      float_v const dz2 = dz * dz;
      this->x() += dz * this->tx();
      this->y() += dz * this->ty();
      m_covXX( 0, 0 ) += dz2 * m_covTT( 0, 0 ) + 2 * dz * m_covXT( 0, 0 );
      m_covXX( 1, 1 ) += dz2 * m_covTT( 1, 1 ) + 2 * dz * m_covXT( 1, 1 );
      m_covXX( 1, 0 ) += dz2 * m_covTT( 1, 0 ) + dz * ( m_covXT( 0, 1 ) + m_covXT( 1, 0 ) );
      // would this make covXT always symmetric as well?
      m_covXT( 0, 0 ) += dz * m_covTT( 0, 0 );
      m_covXT( 0, 1 ) += dz * m_covTT( 0, 1 );
      m_covXT( 1, 0 ) += dz * m_covTT( 1, 0 );
      m_covXT( 1, 1 ) += dz * m_covTT( 1, 1 );
      // m_covXT      += dz*m_covTT ;
      this->z() = z;
    }

  private:
    LHCb::LinAlg::MatSym<float_v, 2> m_covXX;
    LHCb::LinAlg::MatSym<float_v, 2> m_covTT;
    LHCb::LinAlg::Mat<float_v, 2, 2> m_covXT;
  };
} // namespace Sel