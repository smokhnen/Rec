/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOAUtils.h"
#include "Kernel/AllocatorUtils.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/TypeMapping.h"
#include "SOAExtensions/ZipUtils.h"
#include "SelKernel/Utilities.h"

/** @file  VertexRelation.h
 *  @brief Types and functions for associating physics objects to vertices.
 */

/** @class BestVertexRelation
 *  @brief Stores a link to a vertex.
 *
 * This class is intended to be used to store a link to the 'best' associated
 * [primary] vertex. It is basically just an index, but can be extended to
 * allow more validation and caching of frequently-accessed quantities.
 *
 * @todo Add a zip-compatible-family ID. Base this on a specialisation of
 *       Zipping::ExportedSelection<> for the case that there is exactly one
 *       index.
 */
template <typename Index_t, typename Float_t>
class BestVertexRelation {
  Index_t m_index{invalid_index};
  Float_t m_ipchi2{0.f};

public:
  /** WARNING std::numeric_limits is very dangerous in generic code like this,
   *          as std::numeric_limits<T>::max() returns T() for unspecialised T
   *  NOTE    It would be nice if this was constexpr, but constructors such as
   *          SIMDWrapper::avx2::int_v::int_v(int) are not constexpr, and
   *          cannot easily be made constexpr without the SIMD intrinsics they
   *          use internally being made constexpr.
   */
  static_assert( std::numeric_limits<Index_t>::is_specialized );
  inline static auto const invalid_index = std::numeric_limits<Index_t>::max();

  BestVertexRelation( Index_t index, Float_t ipchi2 ) : m_index{index}, m_ipchi2{ipchi2} {}
  Index_t     index() const { return m_index; }
  Float_t     ipchi2() const { return m_ipchi2; }
  friend auto operator==( BestVertexRelation const& lhs, BestVertexRelation const& rhs ) {
    return lhs.index() == rhs.index() && lhs.ipchi2() == rhs.ipchi2();
  }
};

namespace Sel {
  /** @fn    calculateBestVertex
   *  @brief Calculate the 'best' vertex for the given [vector of] object[s] in
   *         the given container.
   *
   * For now we hardcode that this is based on impact parameter chi2. If you
   * pass in a vector type, a vector type will be returned.
   *
   * @todo Also propagate an identifier saying something about what class of
   *       containers the index is valid for.
   *
   * @param obj      Object (track, vertex, particle, ...) to be associated.
   * @param vertices Container of vertices. The caller is responsible for ensuring this
   *                 is not an empty container.
   *
   * @return Structure containing an index into the vertex container, plus some
   *         extra information for validation and optimisation.
   */
  template <typename Object, typename VContainer>
  auto calculateBestVertex( Object const& obj, VContainer const& vertices ) {
    // We want to know the index of the vertex (in 'vertices') that minimises
    // impactParameterChi2. This seems awkward to write using STL algorithms
    // without evaluating impactParameterChi2() an excessive number of times.
    if ( UNLIKELY( vertices.empty() ) ) {
      // To avoid having to handle the case that there are no vertices in "many"
      // different places, instead define that this is the caller's responsibility.
      throw GaudiException{"Empty vertex container passed to Sel::calculateBestVertex()", "Sel::calculateBestVertex",
                           StatusCode::FAILURE};
    }

    // First, deduce the float-like and int-like types that we will use
    using float_t = decltype( Utils::impactParameterChi2( obj, vertices.front() ) );
    using int_t   = typename LHCb::type_map<float_t>::int_t;

    // Make sure that select() works when mask is a plain bool
    using Sel::Utils::select;

    static_assert( std::numeric_limits<int_t>::is_specialized );
    static_assert( std::numeric_limits<float_t>::is_specialized );
    auto min_index  = std::numeric_limits<int_t>::max();
    auto min_ipchi2 = std::numeric_limits<float_t>::max();
    for ( std::size_t vertex_index = 0; vertex_index < vertices.size(); ++vertex_index ) {
      auto ipchi2 = Utils::impactParameterChi2( obj, vertices[vertex_index] );
      auto mask   = ipchi2 < min_ipchi2; // caution: nans can mess with that logic
      min_ipchi2  = select( mask, ipchi2, min_ipchi2 );
      min_index   = select( mask, int_t( vertex_index ), min_index );
    }

    // FIXME what do we do in case all ipchi2 calculations failed?
    // min_index is then still on numeric_limits<int>::max()
    auto min_index_san = select( min_index == std::numeric_limits<int_t>::max(), 0, min_index );

    // Return the index of the 'best' vertex, and the ipchi2 with respect to it
    return BestVertexRelation{/* vertices.zipIdentifier(), */ min_index_san, min_ipchi2};
  }
} // namespace Sel

/** SoA-friendly container version of BestVertexRelations...
 */
class BestVertexRelations {
  std::vector<int, LHCb::Allocators::EventLocal<int>>     m_indices;
  std::vector<float, LHCb::Allocators::EventLocal<float>> m_ipchi2s;
  Zipping::ZipFamilyNumber                                m_family, m_vertex_family;

public:
  /** Constructor from containers of objects and vertices.
   */
  template <typename ObjectsToRelate, typename Vertices>
  BestVertexRelations( ObjectsToRelate const& objects, Vertices const& vertices )
      : m_indices{LHCb::make_obj_propagating_allocator<decltype( m_indices )>( objects )}
      , m_ipchi2s{LHCb::make_obj_propagating_allocator<decltype( m_ipchi2s )>( objects )}
      , m_family{objects.zipIdentifier()}
      , m_vertex_family{Zipping::generateZipIdentifier() /* vertices.zipIdentifier() */} {
    // Avoid both reallocation and resizing, this makes sure the capacity it
    // rounded up to be a multiple of the vector width
    resize( objects.size() );

    // Assume the container is iterable -- for LHCb::Pr::*::Tracks this means
    // that you need to use LHCb::Pr::make_zip( ... )
    std::size_t old_size{0};
    for ( auto const& object_chunk : objects ) {
      // Get the relation for this [chunk of] track[s]
      auto rel = Sel::calculateBestVertex( object_chunk, vertices );

      // Copy the values into our storage vectors
      // TODO this is very SIMDWrapper-specific, it would be nice if it wasn't...
      rel.index().store( m_indices.data() + old_size );
      rel.ipchi2().store( m_ipchi2s.data() + old_size );

      // This is generally an overestimate in the last loop iteration, but it
      // doesn't matter...in that iteration we also copied some invalid junk,
      // but that also doesn't matter.
      old_size += object_chunk.width();
    }
  }

  // Special constructor for zipping
  BestVertexRelations( Zipping::ZipFamilyNumber family, BestVertexRelations const& old )
      : m_indices{old.m_indices.get_allocator()}
      , m_ipchi2s{old.m_ipchi2s.get_allocator()}
      , m_family{std::move( family )}
      , m_vertex_family{old.m_vertex_family} {}

  /** Return the size of our containers. */
  std::size_t size() const { return m_indices.size(); }
  auto        empty() const { return m_indices.empty(); }

  /** Make sure our containers have at least this capacity, rounded up to be a
   *  multiple of the vector unit size so that compressstore() will never write
   *  to out-of-bounds memory.
   */
  void reserve( std::size_t capacity ) {
    auto const safe_capacity = LHCb::v2::Event::safe_simd_capacity<float>( capacity );
    m_ipchi2s.reserve( safe_capacity );
    m_indices.reserve( safe_capacity );
  }

  /** Resize the containers to the given size, making sure that the capacity is
   *  rounded up appropriately
   */
  void resize( std::size_t new_size ) {
    // Our special reserve() makes sure that the capacity is rounded up
    reserve( new_size );
    m_ipchi2s.resize( new_size );
    m_indices.resize( new_size );
  }

  /** Identifier showing which family of containers these columns can be zipped
   *  into.
   */
  Zipping::ZipFamilyNumber zipIdentifier() const { return m_family; }

  /** Identifier showing which family of containers (vertices) these indices
   *  are valid into.
   */
  Zipping::ZipFamilyNumber vertexZipIdentifier() const { return m_vertex_family; }

  template <typename dType, typename Mask>
  void copy_back( BestVertexRelations const& from, int at, Mask mask ) {
    using I = typename dType::int_v;
    using F = typename dType::float_v;
    // How many elements are we adding?
    using Sel::Utils::popcount;
    auto to_add = popcount( mask );

    // How many elements do we already have?
    auto old_size = size();

    // Make sure there's space for the incoming values
    resize( old_size + to_add );

    // Do the copy
    I( &from.m_indices[at] ).compressstore( mask, m_indices.data() + old_size );
    F( &from.m_ipchi2s[at] ).compressstore( mask, m_ipchi2s.data() + old_size );
  }

  template <typename dType>
  auto bestPV( std::size_t offset ) const {
    return BestVertexRelation{typename dType::int_v{&m_indices[offset]}, typename dType::float_v{&m_ipchi2s[offset]}};
  }

  auto const& ipchi2s() const { return m_ipchi2s; }
  auto const& indices() const { return m_indices; }
};

namespace Sel {
  /** @fn    calculateBestVertices
   *  @brief Produce a container of vertex relations for the given objects to
   *         the given vertices
   *
   *  @param objects  Container of objects (tracks, particles, ...) for which
   *                  relations are to be calculated.
   *  @param vertices Container of vertices to which the relations will refer.
   *
   *  @return BestVertexRelations object containing the new relations.
   */
  template <typename OContainer, typename VContainer>
  BestVertexRelations calculateBestVertices( OContainer const& objects, VContainer const& vertices ) {
    return {objects, vertices};
  }

  /** @fn    getBestPVRel
   *  @brief Get relation to the 'best' one of the given primary vertices.
   *
   * If the given object has a .bestPV() method, check that the result is
   * compatible with the given vertex container and return the result.
   * Otherwise calculate the best vertex in the given container using
   * calculateBestVertex() and return that result. The caller has to apply
   * the result to the vertex container themself, but we should guarantee
   * compatibility.
   * @todo This should perform a compatibility test between the saved relation
   *       and the given container. If this fails, a new relation should be
   *       calculated. The family identifier used for this check could also be
   *       used to identify an empty relation.
   */
  template <typename Object, typename VContainer>
  auto getBestPVRel( Object const& obj, VContainer const& vertices ) {
    if constexpr ( Utils::has_bestPV_v<Object> ) {
      auto rel = obj.bestPV();
      // TODO: replace this with a check that rel is compatible with vertices
      if ( ( true ) ) { return rel; }
    }
    return calculateBestVertex( obj, vertices );
  }

  /** @fn    getBestPV
   *  @brief Get a reference to the 'best' one of the given vertices.
   *
   *  Defers to getBestPVRel for all the hard work, but then returns a
   *  reference to the given vertex instead of the relation itself.
   */
  template <typename Object, typename VContainer>
  decltype( auto ) getBestPV( Object const& obj, VContainer const& vertices ) {
    auto rel = getBestPVRel( obj, vertices );
    // rel could be a relation or a reference_wrapper around one
    // either way then std::cref( rel ).get() is a const reference
    // to a relation
    auto const& cref_rel = std::cref( rel ).get();
    auto        index    = cref_rel.index();
    using Sel::Utils::any;
    if ( UNLIKELY( any( index == std::decay_t<decltype( cref_rel )>::invalid_index ) ) ) {
      // This shouldn't happen, but better to provide a useful error message.
      throw GaudiException{"Invalid vertex index", "Sel::getBestPV", StatusCode::FAILURE};
    }
    // index could be int or int_v, if it's int_v, we need
    // to return multiple vertices
    if constexpr ( Sel::Utils::has_size_v<decltype( index )> ) { // FIXME has_size -> is_int_v
      std::array<int, index.size()> idxs;
      index.store( idxs.data() );
      std::array<typename VContainer::value_type const*, index.size()> bestVertices{};
      for ( int i = 0; i < popcount( obj.loop_mask() ); ++i ) { bestVertices[i] = &vertices[idxs[i]]; }
      return bestVertices; // array of vertex pointers
    } else {
      return vertices[index]; // vertex const&
    }
  }
} // namespace Sel
