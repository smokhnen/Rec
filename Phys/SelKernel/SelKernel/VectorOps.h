/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "SelKernel/VectorTraits.h"

namespace Sel::vector {
  /** Dot product of two vectors.
   */
  struct dot {
    template <typename T, typename U>
    auto operator()( T const& v1, U const& v2 ) -> decltype( v1.dot( v2 ) ) {
      return v1.dot( v2 );
    }
    template <typename T, typename U>
    auto operator()( T const& v1, U const& v2 ) -> decltype( v1.Dot( v2 ) ) {
      return v1.Dot( v2 );
    }
  };

  /** Perpendicular (x-y) component if a vector.
   */
  // Out-of-alphabetical order because it is used by eta.
  struct perp {
    template <typename T>
    auto operator()( T const& v ) {
      if constexpr ( type_traits::has_perp_v<T> ) {
        return v.perp();
      } else {
        using std::sqrt;
        return sqrt( v( 0 ) * v( 0 ) + v( 1 ) * v( 1 ) );
      }
    }
  };

  /** Get the z/3rd spatial component of a vector.
   */
  // Out-of-alphabetical order because it is used by eta.
  struct z_comp {
    template <typename T>
    auto operator()( T const& v ) {
      if constexpr ( type_traits::has_z_v<T> ) {
        return v.z();
      } else {
        // TODO check size is 3?
        return v( 2 );
      }
    }
  };

  struct eta {
    template <typename T>
    auto operator()( T const& v ) {
      if constexpr ( type_traits::has_eta_v<T> ) {
        return v.eta();
      } else {
        using std::log;
        auto const zs = z_comp{}( v ) / perp{}( v );
        return log( zs + sqrt( zs * zs + 1 ) );
      }
    }
  };

  struct magnitude {
    template <typename T>
    auto operator()( T const& v ) -> decltype( v.R() ) {
      return v.R();
    }
    template <typename T>
    auto operator()( T const& v ) -> decltype( v.mag() ) {
      return v.mag();
    }
  };

  struct magnitude2 {
    template <typename T>
    auto operator()( T const& v ) -> decltype( v.R2() ) {
      return v.R2();
    }
    template <typename T>
    auto operator()( T const& v ) -> decltype( v.mag2() ) {
      return v.mag2();
    }
  };

  struct similarity {
    template <typename T, typename U>
    auto operator()( T const& t, U const& u ) -> decltype( LHCb::LinAlg::similarity( t, u ) ) const {
      return LHCb::LinAlg::similarity( t, u );
    }
    template <typename T, typename U>
    auto operator()( T const& t, U const& u ) -> decltype( ROOT::Math::Similarity( t, u ) ) const {
      return ROOT::Math::Similarity( t, u );
    }
  };

  /** Extract a new_dim x new_dim symmetric matrix from another symmetric
   *  matrix, starting at offset {start_row_col, start_row_col}.
   */
  template <std::size_t start_row_col, std::size_t new_dim>
  struct sub_sym {
    template <typename T, int old_dim>
    auto operator()( LHCb::LinAlg::MatSym<T, old_dim> const& mat ) const {
      return mat.template sub<LHCb::LinAlg::MatSym<T, new_dim>, start_row_col, start_row_col>();
    }
    template <typename T, unsigned int N>
    using SMatrixSym = ROOT::Math::SMatrix<T, N, N, ROOT::Math::MatRepSym<T, N>>;
    template <typename T, unsigned int old_dim>
    auto operator()( SMatrixSym<T, old_dim> const& mat ) const {
      return mat.template Sub<SMatrixSym<T, new_dim>>( start_row_col, start_row_col );
    }
  };
} // namespace Sel::vector