/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Gaudi/Accumulators.h"
#include "Gaudi/PluginService.h"
#include "GaudiKernel/ThreadLocalContext.h"
#include "Kernel/EventLocalUnique.h"
#include "Kernel/SynchronizedValue.h"
#include "SelKernel/Utilities.h"

#include <typeinfo>

/** @file Core.h
 *  @brief Definition of the type-erased Functor<Output(Input)> type
 */
namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

/** @namespace Functors
 *
 *  Namespace for all new-style functors
 */
namespace Functors {
  /** @class Functor
   *  @brief Empty declaration of type-erased wrapper around a generic functor
   *
   *  This is specialised as Functors::Functor<OutputType(InputTypes...)>.
   */
  template <typename>
  class Functor;

  /** @class PreparedFunctor
   *  @brief Empty declaration of type-erased wrapper around a generic prepared functor.
   *
   *  This is specialised as Functors::PreparedFunctor<OutputType(InputTypes...)>.
   */
  template <typename>
  struct PreparedFunctor;

  /** @class AnyFunctor
   *  @brief Type-agnostic base class for functors, used in IFunctorFactory.
   *  @todo  Could consider using std::any's techniques for type-checking.
   */
  struct AnyFunctor {
    virtual ~AnyFunctor()                                                    = default;
    virtual                       operator bool() const                      = 0;
    virtual void                  bind( Gaudi::Algorithm* owning_algorithm ) = 0;
    virtual std::type_info const& rtype() const                              = 0;
    void                          setJITCompiled( bool value ) { m_was_jit_compiled = value; }
    bool                          wasJITCompiled() const { return m_was_jit_compiled; }

  private:
    bool m_was_jit_compiled{false};
  };

  /** @class TopLevelInfo
   *  @brief Small struct providing helpers for functor implementation.
   *
   *  This helper has two main functions. One is to be passed by non-const
   *  reference to the bind methods of functors. This is how the pointer to the
   *  owning algorithm is transmitted to individual functors' bind() methods.
   *  The second main function is that an instance of it is held by the
   *  top-level Functor<Out(In...)> wrapper and passed in by const reference
   *  when functors are invoked.
   */
  struct TopLevelInfo {
    /** Generate a unique property name that can be added to the owned
     *  algorithm. This is useful for functors that have to add, for example,
     *  ServiceHandles and use the functor configuration to propagate the
     *  relevant configuration.
     */
    std::string             generate_property_name();
    Gaudi::Algorithm*       algorithm() { return m_algorithm; }
    Gaudi::Algorithm const* algorithm() const { return m_algorithm; }
    void                    bind( Gaudi::Algorithm* alg ) { m_algorithm = alg; }
    void                    Warning( std::string_view message, std::size_t count = 1 ) const;

  private:
    Gaudi::Algorithm* m_algorithm{nullptr};
    using MsgMap             = std::map<std::string, Gaudi::Accumulators::MsgCounter<MSG::WARNING>, std::less<>>;
    using SynchronizedMsgMap = LHCb::cxx::SynchronizedValue<MsgMap>;
    // We have to use std::unique_ptr here to make sure that this structure,
    // and the functors that hold it, remain moveable. SynchronizedValue
    // contains an std::mutex, which is not moveable.
    mutable std::unique_ptr<SynchronizedMsgMap> m_msg_counters{std::make_unique<SynchronizedMsgMap>()};
  };

  struct Function;

  /** @class mask_arg_t
   *  @brief Tag type to indicate that a functor takes an explicit mask.
   *
   *  This tag can be used to indicate that the first argument given to a
   *  functor is a mask indicating the validity of the other functor arguments,
   *  which will be propagated to functors that need to know this mask. If no
   *  explicit mask is given, one will be determined from the other arguments.
   *
   *  For example,
   *    Functor<mask_v( Particle const& )>
   *  does not have an explicit mask, but one could be given by instead using
   *    Functor<mask_v( mask_arg_t, mask_v const&, Particle const& )>
   *
   *  For convenience, an instance of this type is also provided:
   *    Functors::mask_arg
   *  so the second signature could be invoked as:
   *    auto const my_prepared_functor = my_functor.prepare( evtCtx )
   *    my_prepared_functor( Functors::mask_arg, my_mask, my_particle )
   */
  struct mask_arg_t {
    explicit mask_arg_t() = default;
  };

  /** Convenient instance of Functors::mask_arg_t.
   */
  inline constexpr mask_arg_t mask_arg{};

  namespace detail {
    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_function_v = std::is_base_of_v<Function, std::decay_t<F>>;

    /** Helper to determine if the given type has a bind( TopLevelInfo& ) method */
    template <typename T>
    using has_bind_TopLevelInfo_ = decltype( std::declval<T>().bind( std::declval<TopLevelInfo&>() ) );

    template <typename T>
    inline constexpr bool has_bind_TopLevelInfo_v = Gaudi::cpp17::is_detected_v<has_bind_TopLevelInfo_, T>;

    /** Call obj.bind( alg ) if obj has an appropriate bind method, otherwise do nothing. */
    template <typename Obj, std::enable_if_t<is_functor_function_v<Obj>, int> = 0>
    void bind( Obj& obj, [[maybe_unused]] TopLevelInfo& top_level ) {
      if constexpr ( has_bind_TopLevelInfo_v<Obj> ) { obj.bind( top_level ); }
    }

    /** Helper for calling bind() on every element of a tuple of functors. */
    template <typename... F>
    void bind( std::tuple<F...>& functors, TopLevelInfo& top_level ) {
      std::apply( [&top_level]( auto&... fs ) { ( bind( fs, top_level ), ... ); }, functors );
    }

    /** Helper to determine if the given type has a prepare() method
     */
    template <typename T>
    using has_prepare_ = decltype( std::declval<T>().prepare() );

    template <typename T>
    inline constexpr bool has_prepare_v = Gaudi::cpp17::is_detected_v<has_prepare_, T>;

    /** Helper to determine if the given type has a
     *  prepare( TopLevelInfo const& ) method.
     */
    template <typename T>
    using has_prepare_TopLevelInfo_ = decltype( std::declval<T>().prepare( std::declval<TopLevelInfo>() ) );

    template <typename T>
    inline constexpr bool has_prepare_TopLevelInfo_v = Gaudi::cpp17::is_detected_v<has_prepare_TopLevelInfo_, T>;

    /** Helper to determine if the given type has a
     *  prepare( EventContext const&, TopLevelInfo const& ) method.
     */
    template <typename T>
    using has_prepare_EventContext_TopLevelInfo_ =
        decltype( std::declval<T>().prepare( std::declval<EventContext>(), std::declval<TopLevelInfo>() ) );

    template <typename T>
    inline constexpr bool has_prepare_EventContext_TopLevelInfo_v =
        Gaudi::cpp17::is_detected_v<has_prepare_EventContext_TopLevelInfo_, T>;

    /** Helper to check if the given type has a static member bool named
     *  requires_explicit_mask, returns its value if so and false otherwise.
     */
    template <typename T>
    using has_static_requires_explicit_mask_ = std::integral_constant<bool, T::requires_explicit_mask>;
    template <typename T>
    inline constexpr bool requires_explicit_mask_v =
        Gaudi::cpp17::detected_or_t<std::false_type, has_static_requires_explicit_mask_, T>::value;

    template <typename T>
    using has_loop_mask_ = decltype( std::declval<T>().loop_mask() );
    template <typename T>
    inline constexpr bool has_loop_mask_v = Gaudi::cpp17::is_detected_v<has_loop_mask_, T>;

    /** @fn    loop_mask
     *  @brief Extract a validity mask from an argument.
     *  @todo  Handle receiving >1 argument.
     */
    template <typename... Data>
    inline constexpr auto loop_mask( Data const&... x ) {
      if constexpr ( sizeof...( Data ) == 0 ) {
        // This is the best we can do
        return true;
      } else {
        static_assert( sizeof...( Data ) == 1, "Support for multiple arguments is not tested yet." );
        if constexpr ( ( has_loop_mask_v<Data> && ... ) ) {
          return ( x.loop_mask() && ... );
        } else {
          return true;
        }
      }
    }

    /** Struct that wraps a prepared functor and handles some invoke / visit
     *  and explicit mask details. There are three cases:
     *   - the functor does not want an explicit mask
     *   - the functor does want an explicit mask, and this was explicitly
     *     tagged/passed in 'args'
     *   - the functor does want an explicit mask, but this was not explicitly
     *     tagged/passed in 'args' and it must be derived from the values in
     *     'args'
     */
    template <typename Functor>
    class prepared {
      /** Implementation to "call f.prepare()" -- defined like this so we can
       *  deduce the return type and use it to define a member variable.
       */
      struct impl {
        auto operator()( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
          // Prepare the given functor
          if constexpr ( has_prepare_EventContext_TopLevelInfo_v<Functor> ) {
            return f.prepare( evtCtx, top_level );
          } else if constexpr ( has_prepare_TopLevelInfo_v<Functor> ) {
            return f.prepare( top_level );
          } else if constexpr ( has_prepare_v<Functor> ) {
            return f.prepare();
          } else {
            return std::cref( f );
          }
        }
      };
      using prepared_t = std::invoke_result_t<impl, Functor, EventContext, TopLevelInfo>;
      prepared_t m_prepared;

    public:
      prepared( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level )
          : m_prepared{impl{}( f, evtCtx, top_level )} {}

      /** Overload with an explicit mask tagged at the beginning of the
       *  parameter pack.
       */
      template <typename Mask, typename... Args>
      decltype( auto ) operator()( mask_arg_t, Mask&& mask, Args&&... args ) const {
        if constexpr ( requires_explicit_mask_v<Functor> ) {
          return Sel::Utils::invoke_or_visit( m_prepared, std::forward<Mask>( mask ), std::forward<Args>( args )... );
        } else {
          return Sel::Utils::invoke_or_visit( m_prepared, std::forward<Args>( args )... );
        }
      }

      /** Overload with no explicit mask, if the wrapped functor requires one
       *  it is obtained from the rest of the parameter pack.
       */
      template <typename... Args>
      decltype( auto ) operator()( Args&&... args ) const {
        if constexpr ( requires_explicit_mask_v<Functor> ) {
          auto const mask = loop_mask( static_cast<std::remove_reference_t<Args>&>( args )... );
          return Sel::Utils::invoke_or_visit( m_prepared, std::move( mask ), std::forward<Args>( args )... );
        } else {
          return Sel::Utils::invoke_or_visit( m_prepared, std::forward<Args>( args )... );
        }
      }
    };

    template <typename Functor>
    prepared<Functor> prepare( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level ) {
      // Return a wrapper that handles the different cases of functors that
      // require that a validity mask is explicitly passed.
      return {f, evtCtx, top_level};
    }
  } // namespace detail

  /** @brief Type-erased wrapper around a generic prepared functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around prepared functors without exposing their extremely
   *  verbose full types. This is very similar to std::function<Out(In)>, but
   *  it supports custom allocators -- in this case LHCb's event-local one.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */
  template <typename OutputType, typename... InputType>
  struct PreparedFunctor<OutputType( InputType... )> final {
    using result_type    = OutputType;
    using allocator_type = LHCb::Allocators::EventLocal<void>;

  private:
    struct IPreparedFunctor {
      virtual ~IPreparedFunctor()                                    = default;
      virtual result_type           operator()( InputType... ) const = 0;
      virtual std::type_info const& rtype() const                    = 0;
    };

    template <typename F>
    struct PreparedFunctorImpl : public IPreparedFunctor {
      PreparedFunctorImpl( F f ) : m_f{std::move( f )} {}
      result_type           operator()( InputType... in ) const override { return std::invoke( m_f, in... ); }
      std::type_info const& rtype() const override { return typeid( std::invoke_result_t<F, InputType...> ); }

    private:
      F m_f;
    };

    LHCb::event_local_unique_ptr<IPreparedFunctor> m_functor;

  public:
    /** Move constructor.
     */
    PreparedFunctor( PreparedFunctor&& ) = default;

    /** Move assignment.
     */
    PreparedFunctor& operator=( PreparedFunctor&& ) = default;

    /** Wrap the given functor up as a type-erased PreparedFunctor, using the
     *  given allocator.
     */
    template <typename F>
    PreparedFunctor( F f, allocator_type alloc )
        : m_functor{LHCb::make_event_local_unique<PreparedFunctorImpl<F>>( std::move( alloc ), std::move( f ) )} {}

    /** Invoke the prepared functor.
     */
    result_type operator()( InputType... in ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty PreparedFunctor<Out(In...)> called" ); }
      return std::invoke( *m_functor, in... );
    }

    /** Check if the prepared functor is invocable.
     */
    operator bool() const { return bool{m_functor}; }

    /** Query the return type of the wrapped functor.
     *  This will be convertible to result_type, but might differ from it.
     */
    std::type_info const& rtype() const {
      if ( UNLIKELY( !m_functor ) ) {
        throw std::runtime_error( "Empty PreparedFunctor<Out(In...)> return type queried" );
      }
      return m_functor->rtype();
    }
  };

  /** @brief Type-erased wrapper around a generic functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around functors without exposing the extremely verbose full types
   *  of [composite] functors. This is specialised as
   *  Functors::Functor<OutputType(InputType)>, which is rather similar to
   *  std::function<Output(Input)>, but includes the extra bind( ... ) method
   *  that is needed to set up functors' data dependencies and associate them
   *  with the owning algorithm.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */
  template <typename OutputType, typename... InputType>
  class Functor<OutputType( InputType... )> final : public AnyFunctor {
  public:
    using result_type   = OutputType;
    using prepared_type = PreparedFunctor<OutputType( InputType... )>;

  private:
    struct IFunctor {
      virtual ~IFunctor()                                 = default;
      virtual void                  bind( TopLevelInfo& ) = 0;
      virtual result_type           operator()( EventContext const&, TopLevelInfo const&, InputType... ) const = 0;
      virtual prepared_type         prepare( EventContext const&, TopLevelInfo const& ) const                  = 0;
      virtual std::type_info const& rtype() const                                                              = 0;
    };

    template <typename F>
    struct FunctorImpl : public IFunctor {
      FunctorImpl( F f ) : m_f{std::move( f )} {}
      void        bind( TopLevelInfo& top_level ) override { detail::bind( m_f, top_level ); }
      result_type operator()( EventContext const& evtCtx, TopLevelInfo const& top_level,
                              InputType... input ) const override {
        // Prepare and call in one go, avoiding the overhead of prepared_type
        // If the arguments are std::variant, use the prepared functor as a
        // visitor, otherwise just call it
        return std::invoke( detail::prepare( m_f, evtCtx, top_level ), input... );
      }
      prepared_type prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const override {
        // Prepare the functor once and store it as a 'member' of a lambda that
        // invokes/visits it; return that lambda wrapped up in the type-erasing
        // wrapper type prepared_type (which takes a handle to the memory
        // resource in order to avoid dynamic allocation using the system
        // allocator)
        return {detail::prepare( m_f, evtCtx, top_level ), LHCb::getMemResource( evtCtx )};
      }
      std::type_info const& rtype() const override {
        return typeid( std::invoke_result_t<detail::prepared<F>, InputType...> );
      }

    private:
      F m_f;
    };
    std::unique_ptr<IFunctor> m_functor;
    TopLevelInfo              m_top_level;

  public:
    /** @brief Construct an empty Functor object.
     *
     *  Note that an empty Functor will throw an exception if the function call
     *  operator is called.
     */
    Functor() = default;

    /** Move constructor
     */
    Functor( Functor&& other ) = default;

    /** Move assignment
     */
    Functor& operator=( Functor&& other ) = default;

    /** Construct a filled Functor object
     */
    template <typename F>
    Functor( F func ) : m_functor{std::make_unique<FunctorImpl<F>>( std::move( func ) )} {}

    /** Fill this Functor with new contents.
     */
    template <typename F>
    Functor& operator=( F func ) {
      m_functor = std::make_unique<FunctorImpl<F>>( std::move( func ) );
      return *this;
    }

    /** @brief Bind the contained object to the given algorithm.
     *
     *  The contained object may have data dependencies, which must be bound to the algorithm
     *  that owns the functor. Calling a functor with data dependencies without binding it to
     *  an algorithm will cause an exception to be thrown.
     *
     *  @param owning_algorithm The algorithm to which the data dependency should be associated.
     *
     *  @todo Change the argument type to the minimal type needed to initialise a
     *        DataObjectReadHandle, Gaudi::Algorithm is over the top.
     */
    void bind( Gaudi::Algorithm* owning_algorithm ) override {
      m_top_level.bind( owning_algorithm );
      m_functor->bind( m_top_level );
    }

    /** @brief Invoke the contained object.
     *
     * Invoke the contained object. If the Functor object is empty, or if the contained object
     * has data dependencies and has not been bound to an owning algorithm, an exception will
     * be thrown.
     *
     * @param input Input to the contained object
     * @return      Output of the contained object
     */
    result_type operator()( InputType... input ) const { return ( *this )( Gaudi::Hive::currentContext(), input... ); }

    /** @brief Invoke the contained object, using the given EventContext.
     */
    result_type operator()( EventContext const& evtCtx, InputType... input ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> called" ); }
      return std::invoke( *m_functor, evtCtx, m_top_level, input... );
    }

    prepared_type prepare( EventContext const& evtCtx = Gaudi::Hive::currentContext() ) const {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> prepared" ); }
      auto prepared = m_functor->prepare( evtCtx, m_top_level );
      if ( UNLIKELY( !prepared ) ) {
        throw std::runtime_error( "Functor<Out(In...)>::prepare() yielded an empty functor." );
      }
      return prepared;
    }

    /** Query whether the Functor is empty or not.
     *
     *  @return false if the Functor is empty, true if it is not
     */
    operator bool() const override { return bool{m_functor}; }

    /** Query the return type of the contained functor.
     *
     *  This is *not* supposed to be typeid( OutputType ), rather it is the
     *  typeid of the functor return type *before* that was converted to
     *  OutputType.
     */
    std::type_info const& rtype() const override {
      if ( UNLIKELY( !m_functor ) ) { throw std::runtime_error( "Empty Functor<Out(In...)> return type queried" ); }
      return m_functor->rtype();
    }

    /** This is useful for the functor cache.
     *
     * @todo Give a more useful description...
     */
    using Factory = typename ::Gaudi::PluginService::Factory<AnyFunctor*()>;
  };
} // namespace Functors
