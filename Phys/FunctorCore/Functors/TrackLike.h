/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/detected.h"
#include "SelKernel/ParticleAccessors.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

/** @file  TrackLike.h
 *  @brief Definitions of functors for track-like objects.
 */

/** @namespace Functors::Track
 *
 *  This defines some basic functors that operate on track-like, or maybe more
 *  accurately charged-particle-like, objects. Both predicates and functions
 *  are defined in the same file for the moment.
 */

namespace Functors {
  static constexpr int invalid_value = -1000;
  using Sel::Utils::is_legacy_particle;
} // namespace Functors

namespace Functors::Track {
  /** @brief x coordinate, as defined by the x() accessor.
   */
  struct X : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).x();
    }
  };

  /** @brief y coordinate, as defined by the y() accessor.
   */
  struct Y : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).y();
    }
  };

  /** @brief z coordinate, as defined by the z() accessor.
   */
  struct Z : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).z();
    }
  };

  /** @brief x slope, as defined by the tx() accessor.
   */
  struct TX : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).tx();
    }
  };

  /** @brief y slope, as defined by the ty() accessor.
   */
  struct TY : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).ty();
    }
  };

  /** @brief Given element of the covariance matrix.
   */
  struct Covariance : public Function {
    Covariance( int i, int j ) : m_i{i}, m_j{j} {}

    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).covariance()( m_i, m_j );
    }

  private:
    int m_i{0}, m_j{0};
  };

  /** @brief Charge, as defined by the charge() accessor.
   */
  struct Charge : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).charge();
    }
  };

  /** @brief Momentum, as defined by the p() accessor.
   */
  struct Momentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).p();
    }
  };

  /** @brief Azimuthal angle, as defined by the phi() accessor.
   */
  struct Phi : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> )
        return Sel::Utils::deref_if_ptr( d ).momentum().Phi();
      else
        return Sel::Utils::deref_if_ptr( d ).phi();
    }
  };

  /** @brief Transverse momentum, as defined by the pt() accessor.
   */
  struct TransverseMomentum : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).pt();
    }
  };

  /** @brief IsMuon, as defined by the accessor of the same name.
   */
  struct IsMuon : public Predicate {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return false; }
        auto pid = pp->muonPID();
        if ( pid ) return pid->IsMuon();
        return bool( pp->info( std::decay_t<decltype( *pp )>::additionalInfo::MuonPIDStatus, 0 ) );
      } else
        return Sel::Utils::deref_if_ptr( d ).IsMuon();
    }
  };

  /** @brief eta/pseudorapidity, as defined by the pseudoRapidity accessor.
   */
  struct PseudoRapidity : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> )
        return Sel::Utils::deref_if_ptr( d ).momentum().eta();
      else
        return d.pseudoRapidity();
    }
  };

  /** @brief Number of degrees of freedom, as defined by the nDoF accessor.
   */
  struct nDoF : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nDoF();
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor.
   */
  struct Chi2PerDoF : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      auto const& particle = Sel::Utils::deref_if_ptr( d );
      using Particle       = std::decay_t<decltype( particle )>;
      if constexpr ( Sel::Utils::is_legacy_particle<Particle> ) {
        return particle.endVertex()->chi2PerDoF();
      } else {
        return Sel::get::chi2PerDoF( particle );
      }
    }
  };

  /** @brief chi^2, as defined by the chi2 accessor.
   */
  struct Chi2 : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).chi2();
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor of the track
   */
  struct TrChi2PerDoF : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::get_track_property_from_particle(
          d, []( auto&& t ) { return t->chi2PerDoF(); }, invalid_value );
    }
  };

  /** @brief q/p, as defined by the qOverP accessor.
   */
  struct QoverP : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).qOverP();
    }
  };

  /** @brief Ghost probability, as defined by the ghostProbability accessor.
   */
  struct GhostProbability : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).ghostProbability();
    }
  };

  template <typename F>
  struct ClosestToBeamState : public std::conditional_t<detail::is_functor_predicate_v<F>, Predicate, Function> {
    static_assert( detail::is_functor_function_v<F>, "ClosestToBeamState adapter must wrap a functor!" );
    ClosestToBeamState( F f ) : m_f{std::move( f )} {}

    /* Improve error messages. */
    constexpr auto name() const { return "ClosestToBeamState( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [f = detail::prepare( m_f, evtCtx, top_level )]( auto const& track ) {
        return f( track.closestToBeamState() );
      };
    }

  private:
    F m_f;
  };
  /** @brief Ghost probability, as defined by the ghostProbability accessor.of the track
   */
  struct TrGhostProbability : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::get_track_property_from_particle(
          d, []( auto&& t ) { return t->ghostProbability(); }, invalid_value );
    }
  };

  /** @brief PIDmu, as defined by the CombDLLmu variable
   */
  struct PIDmu : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return double( invalid_value ); }
        return pp->info( std::decay_t<decltype( *pp )>::additionalInfo::CombDLLmu, invalid_value );
      } else {
        return d.CombDLLmu();
      }
    }
  };

  /** @brief PIDp, as defined by the CombDLLp variable
   */
  struct PIDp : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return double( invalid_value ); }
        return pp->info( std::decay_t<decltype( *pp )>::additionalInfo::CombDLLp, invalid_value );
      } else {
        return d.CombDLLp();
      }
    }
  };

  /** @brief PIDe, as defined by the CombDLLe variable
   */
  struct PIDe : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return double( invalid_value ); }
        return pp->info( std::decay_t<decltype( *pp )>::additionalInfo::CombDLLe, invalid_value );
      } else {
        return d.CombDLLe();
      }
    }
  };

  /** @brief PIDk, as defined by the CombDLLk variable
   */
  struct PIDk : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return double( invalid_value ); }
        return pp->info( std::decay_t<decltype( *pp )>::additionalInfo::CombDLLk, invalid_value );
      } else {
        return d.CombDLLk();
      }
    }
  };

  /** @brief PIDpi, as defined by the CombDLLpi variable
   */
  struct PIDpi : public Function {
    template <typename Data>
    auto operator()( Data const& d ) const {
      if constexpr ( is_legacy_particle<Data> ) {
        auto const* pp = Sel::Utils::deref_if_ptr( d ).proto();
        if ( not pp ) { return double( invalid_value ); }
        return pp->info( std::decay_t<decltype( *pp )>::additionalInfo::CombDLLpi, invalid_value );
      } else {
        return d.CombDLLpi();
      }
    }
  };

  /** @brief nHits, as defined by the nHits accessor.
   */
  struct nHits : public Function {
    static constexpr auto name() { return "nHits"; }
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d ).nHits();
    }
  };
} // namespace Functors::Track

namespace Functors::detail {
  template <typename StatePos_t, typename StateDir_t, typename VertexPos_t>
  auto impactParameterSquared( StatePos_t const& state_pos, StateDir_t const& state_dir, VertexPos_t const& vertex ) {
    return ( state_pos - vertex ).Cross( state_dir ).mag2() / state_dir.mag2();
  }

  struct MinimumImpactParameter : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameter"; }

    template <typename VContainer, typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      auto&& state     = track_chunk.closestToBeamState();
      auto&& state_pos = state.position();
      auto&& state_dir = state.slopes();
      using std::min;  // so min() below works when float_v is plain float or double
      using std::sqrt; // ...
      using vector_t = std::decay_t<decltype( state_pos )>;
      using float_v  = decltype( state_pos.X() );
      auto min_ip2 =
          std::accumulate( std::begin( vertices ), std::end( vertices ), float_v{std::numeric_limits<float>::max()},
                           [&]( float_v ip2, auto const& vertex ) {
                             auto const& PV = vertex.position();
                             auto        this_ip2 =
                                 impactParameterSquared( state_pos, state_dir, vector_t( PV.x(), PV.y(), PV.z() ) );
                             static_assert( std::is_same_v<decltype( this_ip2 ), float_v> );
                             return min( ip2, this_ip2 );
                           } );
      return sqrt( min_ip2 );
    }
  };

  struct MinimumImpactParameterChi2 : public Function {
    template <typename VContainer, typename TrackChunk>
    auto operator()( VContainer const& vertices, TrackChunk const& track_chunk ) const {
      // Make sure min() works for plain floating point types
      using std::min;
      // Accept generic containers
      using std::begin;
      using std::end;
      // Compatibility layer :( -- get something with value semantics; for the
      // non-legacy case then 'track' and 'track_chunk' will be the same thing
      auto const& track = [&]() -> decltype( auto ) {
        if constexpr ( is_legacy_particle<TrackChunk> ) {
          // this only works for chargedbasics..
          auto const* track = Sel::Utils::get_track_from_particle( track_chunk );
          assert( track );
          return *track;
        } else {
          return track_chunk;
        }
      }();
      // Figure out the return type (probably float or SIMDWrapper's float_v for now)
      using float_v = decltype( Sel::Utils::impactParameterChi2( track, vertices.front() ) );
      static_assert( std::numeric_limits<float_v>::is_specialized,
                     "Dangerous use of std::numeric_limits<T> for unspecialised T." );
      return std::accumulate( begin( vertices ), end( vertices ), std::numeric_limits<float_v>::max(),
                              [&track]( float_v ipchi2, auto const& vertex ) {
                                return min( ipchi2, Sel::Utils::impactParameterChi2( track, vertex ) );
                              } );
    }
  };

  struct ImpactParameterChi2ToVertex : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "ImpactParameterChi2ToVertex"; }

    template <typename VContainer, typename Particle>
    auto operator()( VContainer const& vertices, Particle const& particle ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      return Sel::getBestPVRel( particle, vertices ).ipchi2();
    }
  };

  struct MinimumImpactParameterCut : public Predicate {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    static constexpr bool requires_explicit_mask = true;

    MinimumImpactParameterCut( float cut_value ) : m_cut_value_squared{cut_value * cut_value} {}

    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameterCut"; }

    template <typename mask_t, typename VContainer, typename Particle>
    auto operator()( mask_t const& particle_mask, VContainer const& vertices, Particle const& particle ) const {
      auto const& state     = particle.closestToBeamState();
      auto const& state_pos = state.position();
      auto const& state_dir = state.slopes();
      using Sel::Utils::none; // so none() below works when mask_v is plain bool
      using std::min;         // so min() below works when float_v is plain float or double
      using vector_t = std::decay_t<decltype( state_pos )>;
      using float_v  = decltype( state_pos.X() );
      // This is very important, as for unspecialised types then ::max() returns
      // T(), which might be an uninitialised value
      static_assert( std::numeric_limits<float_v>::is_specialized,
                     "Dangerous use of std::numeric_limits<T> for unspecialised T." );
      // Return the mask we were given if there are no vertices
      auto    local_mask = particle_mask;
      float_v min_d{std::numeric_limits<float_v>::max()};
      for ( auto const& vertex : vertices ) {
        auto const& PV = vertex.position();
        min_d =
            min( min_d, detail::impactParameterSquared( state_pos, state_dir, vector_t( PV.x(), PV.y(), PV.z() ) ) );
        local_mask = local_mask && ( min_d > m_cut_value_squared );
        // Not completely obvious the overhead of short-circuiting is a net
        // gain for the vectorised version, but let's do it anyway for now.
        if ( none( local_mask ) ) { break; }
      }
      return local_mask;
    }

  private:
    float m_cut_value_squared = 0.f;
  };

  struct MinimumImpactParameterChi2Cut : public Predicate {
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument.
     */
    static constexpr bool requires_explicit_mask = true;

    MinimumImpactParameterChi2Cut( float cut_value ) : m_cut_value{cut_value} {}

    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "MinimumImpactParameterChi2Cut"; }

    template <typename mask_t, typename VContainer, typename Particle>
    auto operator()( mask_t const& mask, VContainer const& vertices, Particle const& particle ) const {
      // mask tells us how much of the 'width' of 'track_chunk' we should
      // operate on.
      // Figure out what the types are. It would be nice if we could retire this part...
      using float_v = decltype( Sel::Utils::impactParameterChi2( particle, vertices.front() ) );
      // This is very important, as for unspecialised types then ::max() returns
      // T(), which might be an uninitialised value
      static_assert( std::numeric_limits<float_v>::is_specialized,
                     "Dangerous use of std::numeric_limits<T> for unspecialised T." );
      // Keep track of the smallest ipchi2 as we loop, abort if/when all values
      // are below the cut value
      float_v min_ipchi2{std::numeric_limits<float_v>::max()};
      // Make sure min() works for basic arithmetic types
      using std::min;
      // Make sure none() works for plain bool
      using Sel::Utils::none;
      // pass through the mask we were given if there are no vertices
      mask_t local_mask = mask;
      // Unfortunately it seems difficult to avoid the explicit loop...
      for ( auto const& vertex : vertices ) {
        min_ipchi2 = min( min_ipchi2, Sel::Utils::impactParameterChi2( particle, vertex ) );
        local_mask = local_mask && ( min_ipchi2 > m_cut_value );
        // Check if all members of 'particle' have had an ipchi2 below
        // threshold w.r.t. one of the vertices we've already looped over.
        // If so, we can safely abort the loop over vertices.
        if ( none( local_mask ) ) { break; }
      }
      return local_mask;
    }

  private:
    float m_cut_value = 0.f;
  };
} // namespace Functors::detail

namespace Functors::Track {
  /** @brief Minimum impact parameter w.r.t. any of the given vertices.
   *
   *  Note that for the typical MINIP > x cut it is more efficient to use the
   *  dedicated functor that can abort the loop over vertices early.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameter( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::MinimumImpactParameter, VContainer>{std::move( vertex_location )};
  }

  /** @brief Minimum impact parameter chi2 w.r.t. any of the given vertices.
   *
   *  Note that for the typical MINIPCHI2 > x cut it is more efficiency to use
   *  the dedicated functor that can abort the loop over vertices early.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterChi2( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::MinimumImpactParameterChi2, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Impact parameter chi2 to the "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used. The link caches the ipchi2 value, so this should be the
   *  most efficient way of getting the ipchi2 value.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto ImpactParameterChi2ToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::ImpactParameterChi2ToVertex, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Require the minimum impact parameter w.r.t. any of the given
   *         vertices is greater than some threshold.
   *
   *  @param cut_value    Minimum impact parameter value.
   *  @param tes_location TES location of vertex container.
   *
   *  Note this is more efficient than computing the minimum and then cutting
   *  on it, as this functor can short circuit.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterCut( float cut_value, std::string vertex_location ) {
    return detail::add_data_deps<Predicate, VContainer>( detail::MinimumImpactParameterCut{cut_value},
                                                         std::move( vertex_location ) );
  }

  /** @brief Require the minimum impact parameter chi2 w.r.t. any of the given
   *         vertices is greater than some threshold.
   *
   *  @param cut_value    Minimum impact parameter chi2 value.
   *  @param tes_location TES location of vertex container.
   *
   *  Note this is more efficient than computing the minimum and then cutting
   *  on it, as this functor can short circuit.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto MinimumImpactParameterChi2Cut( float cut_value, std::string vertex_location ) {
    return detail::add_data_deps<Predicate, VContainer>( detail::MinimumImpactParameterChi2Cut{cut_value},
                                                         std::move( vertex_location ) );
  }
} // namespace Functors::Track
