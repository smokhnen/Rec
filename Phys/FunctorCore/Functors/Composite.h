/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/ParticleTraits.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VectorOps.h"
#include "SelKernel/VertexRelation.h"
#include "TrackKernel/TrackCompactVertex.h"

#include "GaudiKernel/detected.h"

#include <cassert>

/** @file  Composite.h
 *  @brief Definitions of functors for composite-particle-like objects.
 */

/** @namespace Functors::Composite
 *
 *  Functors that make sense for composite particles (i.e. ones that have a
 *  vertex as well as a trajectory)
 */
namespace Functors::detail {
  template <typename T>
  using has_threeMomentum_ = decltype( std::declval<T>().threeMomentum() );
  template <typename T>
  inline constexpr bool has_threeMomentum_v = Gaudi::cpp17::is_detected_v<has_threeMomentum_, T>;

  template <typename>
  struct is_array_of_v2_RecVertex_pointers : std::false_type {};

  template <std::size_t N>
  struct is_array_of_v2_RecVertex_pointers<std::array<LHCb::Event::v2::RecVertex const*, N>> : std::true_type {};

  template <typename T>
  inline constexpr bool is_array_of_v2_RecVertex_pointers_v = is_array_of_v2_RecVertex_pointers<T>::value;

  template <typename T>
  using has_static_NumChildren_ = decltype( T::NumChildren );
  template <typename T>
  inline constexpr bool has_static_NumChildren_v = Gaudi::cpp17::is_detected_v<has_static_NumChildren_, T>;

  /** Get the position of the best associated PV
   */
  template <typename Composite, typename Vertices>
  decltype( auto ) getBestPVPosition( Composite const& composite, Vertices const& vertices ) {
    // Get the best PV, which would ideally be a nice proxy thing with some
    // gather operations under the hood, but which may actually be a vector
    // of RecVertex pointers...
    auto const& bestPV = Sel::getBestPV( composite, vertices );
    using bestPV_t     = std::decay_t<decltype( bestPV )>;
    if constexpr ( detail::is_array_of_v2_RecVertex_pointers_v<bestPV_t> ) {
      // Assume that 'composite' is a new-style proxy object
      using float_v = typename Composite::dType::float_v;
      std::array<float, float_v::size()> x, y, z;
      std::size_t const                  num_valid = popcount( composite.loop_mask() );
      for ( auto i = 0ul; i < num_valid; ++i ) {
        x[i] = bestPV[i]->position().x();
        y[i] = bestPV[i]->position().y();
        z[i] = bestPV[i]->position().z();
      }
      for ( auto i = num_valid; i < x.size(); ++i ) { x[i] = y[i] = z[i] = std::numeric_limits<float>::lowest(); }
      return LHCb::LinAlg::Vec{float_v{x.data()}, float_v{y.data()}, float_v{z.data()}};
    } else {
      // If only...
      return bestPV.position();
    }
  }

  struct FlightDistanceChi2ToVertex : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "FlightDistanceChi2ToVertex"; }

    template <typename VContainer, typename Particle>
    auto operator()( VContainer const& vertices, Particle const& composite ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.
      auto const& bestPV = Sel::getBestPV( composite, vertices );

      // Now calculate the flight distance chi2 between 'composite' and 'bestPV'
      if constexpr ( Sel::Utils::is_legacy_particle<Particle> ) {
        assert( composite.endVertex() );
        return Sel::Utils::flightDistanceChi2( *composite.endVertex(), bestPV );
      } else {
        return Sel::Utils::flightDistanceChi2( composite, bestPV );
      }
    }
  };

  /** BPVDIRA */
  struct CosDirectionAngleToVertex : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "CosDirectionAngleToVertex"; }

    template <typename VContainer, typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      // Get the associated PV -- this uses a link if it's available and
      // computes the association if it's not.

      // Calculate the angle between the momentum vector of the composite and
      // the vector connecting its decay vertex to the origin
      auto const& mom = [&composite]() {
        if constexpr ( Sel::Utils::is_legacy_particle<Composite> ) {
          return composite.momentum().Vect();
        } else {
          return composite.threeMomentum();
        }
      }();

      // The flight direction vector
      auto const flight = Sel::Utils::endVertexPos( composite ) - getBestPVPosition( composite, vertices );
      using std::sqrt;
      return Sel::vector::dot{}( mom, flight ) /
             sqrt( Sel::vector::magnitude2{}(flight)*Sel::vector::magnitude2{}( mom ) );
    }
  };

  struct PseudoRapidityFromVertex : public Function {
    static constexpr auto name() { return "PseudoRapidityFromVertex"; }

    template <typename VContainer, typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      auto const& bestPV_position = getBestPVPosition( composite, vertices );
      auto        flight          = Sel::Utils::endVertexPos( composite ) - bestPV_position;
      return Sel::vector::eta{}( flight );
    }
  };

  struct CorrectedMass : public Function {
    float m_mass_hypo;
    CorrectedMass( float mass_hypo ) : m_mass_hypo{mass_hypo} {}

    static constexpr auto name() { return "CorrectedMass"; }

    template <typename VContainer, typename Composite>
    auto operator()( VContainer const& vertices, Composite const& composite ) const {
      // Get the decay vertex
      auto const& decay_vertex = composite.endVertex();

      // Get the associated [primary] vertex position
      auto const bestPV_position = getBestPVPosition( composite, vertices );

      // Three-momentum and uncorrected squared mass.
      auto const [mom, m2] = [&]() {
        if constexpr ( detail::has_threeMomentum_v<Composite> && Sel::type_traits::has_mass2_v<Composite> ) {
          return std::tuple{composite.threeMomentum(), composite.mass2()};
        } else if constexpr ( Sel::type_traits::has_momentum_v<Composite> ) {
          auto p4 = composite.momentum();
          return std::tuple{p4.Vect(), p4.M2()};
        } else {
          // Use m_mass_hypo to get the 4-momentum of the composite
          std::array<float, Composite::NumChildren> child_masses;
          child_masses.fill( m_mass_hypo );
          Gaudi::LorentzVector p4;
          // @todo This does too much work!
          Gaudi::XYZPoint     position;
          Gaudi::SymMatrix3x3 poscov;
          Gaudi::SymMatrix4x4 p4cov;
          Gaudi::Matrix4x3    momposcov;
          LHCb::TrackKernel::computeParticleParams( composite, child_masses, position, p4, poscov, p4cov, momposcov );
          return std::tuple{p4.Vect(), p4.M2()};
        }
      }();

      // TODO this is lifted from LoKi but some checks were dropped.

      // Calculate the corrected mass using the 4-momentum (p4) and the
      // flight vector:
      auto const d = decay_vertex - bestPV_position;

      // Get the pT variable that we need
      auto const dmag2 = Sel::vector::magnitude2{}( d );
      auto const perp  = mom - d * ( Sel::vector::dot{}( mom, d ) / dmag2 );
      auto const pt    = Sel::vector::magnitude{}( perp );

      // Calculate the corrected mass
      using std::sqrt;
      return pt + sqrt( m2 + pt * pt );
    }
  };
} // namespace Functors::detail

namespace Functors::Composite {
  /** @brief Flight distance chi2 to the "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto FlightDistanceChi2ToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::FlightDistanceChi2ToVertex, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Calculate the cosine of the angle between the momentum vector of
   *         the composite and the vector connecting its decay vertex to the
   *         "best" one of the given vertices.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto CosDirectionAngleToVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::CosDirectionAngleToVertex, VContainer>{
        std::move( vertex_location )};
  }

  /** @brief Pseudorapidity of the flight vector of the given composite. */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto PseudoRapidityFromVertex( std::string vertex_location ) {
    return detail::DataDepWrapper<Function, detail::PseudoRapidityFromVertex, VContainer>{std::move( vertex_location )};
  }

  /** @brief Calculate the corrected mass, using the given mass hypothesis. */
  template <typename VContainer = detail::DefaultPVContainer_t>
  auto CorrectedMass( float assumed_mass, std::string vertex_location ) {
    return detail::add_data_deps<Function, VContainer>( detail::CorrectedMass{assumed_mass},
                                                        std::move( vertex_location ) );
  }

  template <typename... MassInputs>
  struct Mass : public Function {
    /** Create a mass functor with a list of mass hypotheses that can be a mix
     *  of floating point values (in MeV) and names of particles. Particle
     *  names are translated into mass values using the ParticlePropertySvc.
     */
    Mass( std::tuple<MassInputs...> mass_inputs ) : m_mass_inputs{std::move( mass_inputs )} {}

    void bind( TopLevelInfo& top_level ) { bind( top_level, std::index_sequence_for<MassInputs...>{} ); }

    template <typename CombinationType>
    auto operator()( CombinationType const& combination ) const {
      if constexpr ( Sel::type_traits::has_mass_v<CombinationType> ) {
        // If there's a mass accessor then just call it
        return combination.mass();
      } else {
        // Otherwise calculate the mass from the child 3-momenta and the given
        // mass hypotheses. Start by checking we have the correct number.
        if constexpr ( detail::has_static_NumChildren_v<CombinationType> ) {
          // Check the number of children at compile time if possible
          static_assert( sizeof...( MassInputs ) == CombinationType::NumChildren,
                         "Wrong number of masses supplied to Mass functor." );
        } else {
          // The composite type erases the number of children, so we can only
          // make this check at runtime.
          if ( UNLIKELY( sizeof...( MassInputs ) != combination.numChildren() ) ) {
            throw GaudiException{"Mismatch between number of mass values given (" +
                                     std::to_string( sizeof...( MassInputs ) ) +
                                     ") and the number of children in the given object (" +
                                     std::to_string( combination.numChildren() ) + ")",
                                 "Functors::Composite::Mass", StatusCode::FAILURE};
          }
        }
        float E{0.f}; // TODO make SIMD-friendly?
        using std::sqrt;
        for ( std::size_t idau = 0; idau < m_mass_values.size(); ++idau ) {
          E += sqrt( std::pow( combination.daughters.P( idau ), 2 ) + m_mass_values[idau] * m_mass_values[idau] );
        }
        return sqrt( E * E - combination.threeMomentum().Mag2() );
      }
    }

  private:
    template <std::size_t... Ns>
    void bind( TopLevelInfo& top_level, std::index_sequence<Ns...> ) {
      // Avoid setting up the service if we don't need it (e.g. all hypotheses)
      // were specified numerically)
      std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> pp_svc{std::nullopt};
      // Helper to convert a member of m_mass_inputs to a numeric value.
      auto const converter = [&]( auto const& mass_or_name ) {
        if constexpr ( std::is_same_v<float, std::decay_t<decltype( mass_or_name )>> ) {
          return mass_or_name;
        } else {
          if ( !pp_svc ) {
            pp_svc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
          }
          auto const* pp = pp_svc.value()->find( mass_or_name );
          if ( !pp ) {
            throw GaudiException{"Couldn't get ParticleProperty for particle '" + mass_or_name + "'",
                                 "Functors::Composite::Mass::bind()", StatusCode::FAILURE};
          }
          return pp->mass();
        }
      };
      ( ( m_mass_values[Ns] = converter( std::get<Ns>( m_mass_inputs ) ) ), ... );
    }

    std::tuple<MassInputs...>                  m_mass_inputs;
    std::array<float, sizeof...( MassInputs )> m_mass_values{};
  };

  struct CompositeMass : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "CompositeMass"; }

    template <typename Particle>
    auto operator()( Particle const& composite ) const {
      if constexpr ( Sel::Utils::is_legacy_particle<Particle> )
        return composite.momentum().M();
      else
        return composite.mass();
    }
  };
} // namespace Functors::Composite
