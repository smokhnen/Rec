###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Functors import BPVFDCHI2, CHI2DOF, COMB, MINIPCUT, MVA, SUM
from Functors.math import log
from PyConf.Algorithms import FunctorExampleAlg, Gaudi__Examples__IntDataProducer, Gaudi__Examples__VectorDataProducer
from PyConf.dataflow import dataflow_config


def test_pyconf_integration():
    """PyConf should be able to deduce inputs of functor-holding algorithms.

    Data dependencies of a functor should be inherited by a PyConf.Algorithm
    that holds that functor.
    """
    # Pretend that we can load some PVs from an input file
    # Note that Python functor representations do not perform type-checking of
    # their data dependencies (it would be very difficult to implement, given
    # the templated nature of most functors)
    pvs = Gaudi__Examples__IntDataProducer().OutputLocation
    pvs2 = Gaudi__Examples__VectorDataProducer().OutputLocation

    # PyConf asks Algorithm properties for inputs to inject using the
    # `data_dependencies()` method, so check that works
    functor = MINIPCUT(IPCut=1.0, Vertices=pvs)
    functor2 = MINIPCUT(IPCut=2.0, Vertices=pvs2)
    assert len(functor.data_dependencies()) == 1
    # Should be propagated by composition and operations
    assert len((functor & functor2).data_dependencies()) == 2
    assert len((functor | functor2).data_dependencies()) == 2
    assert len((~functor).data_dependencies()) == 1
    assert len(log(functor).data_dependencies()) == 1

    # A more complex functor
    mva = MVA(
        MVAType="MatrixNet",
        Config={"MatrixnetFile": ""},
        Inputs={
            "chi2": CHI2DOF,
            "sumfdchi2": BPVFDCHI2(pvs) + BPVFDCHI2(pvs2),
        },
    )
    assert len(mva.data_dependencies()) == 2

    # Another complex functor, including a list of data dependencies
    # (`ChildContainers`) and a functor which takes bound functor which has
    # dependencies
    comb = COMB(
        Functor=SUM(BPVFDCHI2(pvs)),
        ChildContainers=[pvs2],
        # Not actually instantiating the C++ functor, so can give dummy types
        ChildContainerTypes=[('', '')])
    print(comb.data_dependencies())
    assert len(comb.data_dependencies()) == 2

    alg = FunctorExampleAlg(Cut=functor)
    assert len(alg.inputs) == 1
    assert next(iter(alg.inputs.values()))[0] is pvs


def test_configurable_instantiation():
    """PyConf should be able to instantiate a functor-holding configurable."""
    pvs = Gaudi__Examples__IntDataProducer().OutputLocation
    functor = MINIPCUT(IPCut=1.0, Vertices=pvs)
    alg = FunctorExampleAlg(Cut=functor)

    # Convert PyConf Algorithm to Gaudi Configurable
    c = dataflow_config()
    c.update(alg.configuration())
    c.apply()


def test_string_datahandle_warning(caplog):
    """Passing a str for a DataHandle should print a warning."""
    pvs = Gaudi__Examples__IntDataProducer().OutputLocation

    # No warning
    MINIPCUT(IPCut=1.0, Vertices=pvs)
    assert len(caplog.records) == 0

    # Use the DataHandle's concrete string location, get a warning
    MINIPCUT(IPCut=1.0, Vertices=pvs.location)
    assert len(caplog.records) == 1
    record, = caplog.records
    assert record.levelname == "WARNING", record
    assert "not be able to infer this data dependency" in record.msg, record
