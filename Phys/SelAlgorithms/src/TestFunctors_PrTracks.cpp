/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TestFunctors.h"

#include "Event/PrFittedForwardTracks.h"
#include "SelKernel/TrackZips.h"

DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Pr::Fitted::Forward::Tracks>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Pr::Fitted::Forward::Tracks>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Pr::Fitted::Forward::TracksWithPVs>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Pr::Fitted::Forward::TracksWithMuonID>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracksWithMuonID" )
