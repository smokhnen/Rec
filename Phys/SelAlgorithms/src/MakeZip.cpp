/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle_v2.h"
#include "Event/PrFittedForwardTracks.h"
#include "Event/PrMuonPIDs.h"
#include "Event/Zip.h"
#include "GaudiAlg/Transformer.h"
#include "SelKernel/IterableVertexRelations.h"

namespace {
  struct deduce_return_type_t {};

  template <typename OutputType, typename... Inputs>
  struct Helper {
    static constexpr std::size_t NumInputs            = sizeof...( Inputs );
    static constexpr bool        deduce_return_type_v = std::is_same_v<OutputType, deduce_return_type_t>;
    using Output   = std::conditional_t<deduce_return_type_v, LHCb::Pr::zip_t<Inputs...>, OutputType>;
    using Base     = typename Gaudi::Functional::Transformer<Output( Inputs const&... )>;
    using KeyValue = typename Base::KeyValue;
    static auto InputNames() {
      std::array<KeyValue, NumInputs> ret;
      for ( auto i = 0ul; i < NumInputs; ++i ) { ret[i] = KeyValue{"Input" + std::to_string( i + 1 ), ""}; }
      return ret;
    }
  };
} // namespace

/** @class MakeZip MakeZip.cpp
 *  @brief Puts an iterable zip of its input containers onto the TES
 *
 *  This algorithm is a very thin wrapper around LHCb::Pr::make_zip( in... ),
 *  given a set of N inputs (property name Input{1..N}) it stores the return
 *  value of LHCb::Pr::make_zip( in... ) on the TES (property name Output).
 *
 *  @tparam  OutputType Output type to force. If this is set to deduce_return_type_t then the type will be deduced from
 *                      the input types.
 *  @tparam  Inputs...  Types of the input containers taken from the TES
 *  @returns            Iterable, non-owning view type referring to the given
 *                      input containers.
 */
template <typename OutputType, typename... Inputs>
struct MakeZip final : public Helper<OutputType, Inputs...>::Base {
  using Help = Helper<OutputType, Inputs...>;
  MakeZip( const std::string& name, ISvcLocator* pSvcLocator )
      : Help::Base( name, pSvcLocator, Help::InputNames(), {"Output", ""} ) {}
  typename Help::Output operator()( Inputs const&... in ) const override { return LHCb::Pr::make_zip( in... ); }
};

using MakeZip__ChargedBasics_as_Particles = MakeZip<LHCb::v2::Particles, LHCb::v2::ChargedBasics>;
DECLARE_COMPONENT_WITH_ID( MakeZip__ChargedBasics_as_Particles, "MakeZip__ChargedBasics_as_Particles" )

using MakeZip__PrFittedForwardTracks__BestVertexRelations =
    MakeZip<deduce_return_type_t, LHCb::Pr::Fitted::Forward::Tracks, BestVertexRelations>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks__BestVertexRelations,
                           "MakeZip__PrFittedForwardTracks__BestVertexRelations" )

using MakeZip__PrFittedForwardTracks__PrMuonPIDs =
    MakeZip<deduce_return_type_t, LHCb::Pr::Fitted::Forward::Tracks, LHCb::Pr::Muon::PIDs>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks__PrMuonPIDs, "MakeZip__PrFittedForwardTracks__PrMuonPIDs" )
