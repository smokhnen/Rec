/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoANNPIDToolBase.h
 *
 * Header file for algorithm ChargedProtoANNPIDToolBase
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2010-03-09
 */
//-----------------------------------------------------------------------------

#pragma once
#include "ChargedProtoANNPIDCommonBase.h"
#include "GaudiAlg/GaudiTupleTool.h"

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDToolBase ChargedProtoANNPIDToolBase.h
   *
   *  Base class for all ProtoParticle ANN based PID tools
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  struct ChargedProtoANNPIDToolBase : ANNGlobalPID::ChargedProtoANNPIDCommonBase<GaudiTupleTool> {

    /// Standard constructor
    using ANNGlobalPID::ChargedProtoANNPIDCommonBase<GaudiTupleTool>::ChargedProtoANNPIDCommonBase;
  };

} // namespace ANNGlobalPID
