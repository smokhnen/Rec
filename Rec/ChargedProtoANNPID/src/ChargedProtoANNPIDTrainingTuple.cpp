/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ChargedProtoANNPIDAlgBase.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "RecInterfaces/IChargedProtoANNPIDTupleTool.h"

namespace ANNGlobalPID {

  //-----------------------------------------------------------------------------
  /** @class ChargedProtoANNPIDTrainingTuple ChargedProtoANNPIDTrainingTuple.h
   *
   *  Makes an ntuple for PID ANN training, starting from ProtoParticles.
   *
   *  @author Chris Jones
   *  @date   2010-03-09
   */
  //-----------------------------------------------------------------------------

  class ChargedProtoANNPIDTrainingTuple final : public ChargedProtoANNPIDAlgBase {

  public:
    /// Standard constructor
    ChargedProtoANNPIDTrainingTuple( const std::string& name, ISvcLocator* pSvcLocator )
        : ChargedProtoANNPIDAlgBase( name, pSvcLocator ) {
      // Turn off Tuple printing during finalize
      setProperty( "NTuplePrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    /// Execution
    StatusCode execute() override {

      bool sc = true;

      // Load the charged ProtoParticles
      auto protos = getIfExists<LHCb::ProtoParticles>( m_protoPath );
      if ( !protos ) return Warning( "No ProtoParticles at '" + m_protoPath + "'", StatusCode::SUCCESS );

      // Loop over all ProtoParticles and fill tuple
      for ( const auto* P : *protos ) {
        // Check proto is charged
        if ( !P->track() ) continue;
        // make a tuple
        auto tuple = nTuple( "annInputs", "ProtoParticle PID Information for ANN Training" );
        // Fill variables
        sc &= m_tuple->fill( tuple, P );
        // Finally, write the tuple for this ProtoParticle
        sc &= tuple->write();
      }

      // return
      return StatusCode{sc};
    }

  private:
    /// Pointer to the tuple tool
    ToolHandle<IChargedProtoANNPIDTupleTool> m_tuple{this, "TupleTool",
                                                     "ANNGlobalPID::ChargedProtoANNPIDTupleTool/Tuple"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoANNPIDTrainingTuple )

} // namespace ANNGlobalPID
