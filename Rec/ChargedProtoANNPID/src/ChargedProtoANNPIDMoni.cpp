/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "ChargedProtoANNPIDAlgBase.h"

namespace ANNGlobalPID {

  /** @class ChargedProtoANNPIDMoni ChargedProtoANNPIDMoni.h
   *
   *  Monitor for the ANNPID
   *
   *  @author Chris Jones
   *  @date   2012-01-12
   */

  class ChargedProtoANNPIDMoni final : public ChargedProtoANNPIDAlgBase {

  public:
    /// Standard constructor
    ChargedProtoANNPIDMoni( const std::string& name, ISvcLocator* pSvcLocator )
        : ChargedProtoANNPIDAlgBase( name, pSvcLocator ) {
      // histo base dir
      setProperty( "HistoTopDir", "PROTO/" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }

    /// Algorithm execution
    StatusCode execute() override {

      // Load the charged ProtoParticles
      const auto protos = getIfExists<LHCb::ProtoParticles>( m_protoPath );
      if ( !protos ) return Warning( "No ProtoParticles at '" + m_protoPath + "'", StatusCode::SUCCESS );

      // Loop over ProtoParticles
      for ( const auto P : *protos ) {

        // Skip velo tracks (never have ANN PID information)
        if ( P->track()->type() == LHCb::Track::Types::Velo || //
             P->track()->type() == LHCb::Track::Types::VeloR ) {
          continue;
        }

        // Get the track type
        const auto type = LHCb::Track::TypesToString( P->track()->type() );

        // Fill plots
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNe, 0 ), type + "/ElectronANN",
                type + " Electron ANN PID", 0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNmu, 0 ), type + "/MuonANN", type + " Muon ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNpi, 0 ), type + "/PionANN", type + " Pion ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNk, 0 ), type + "/KaonANN", type + " Kaon ANN PID",
                0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNp, 0 ), type + "/ProtonANN",
                type + " Proton ANN PID", 0.0, 1.0, 100 );
        // CRJ Do not enable by default just yet
        // plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNd, 0 ),
        //        type+"/DeuteronANN", type+" Deuteron ANN PID",
        //        0.0, 1.0, 100 );
        plot1D( P->info( LHCb::ProtoParticle::additionalInfo::ProbNNghost, 0 ), type + "/GhostANN",
                type + " Ghost ANN PID", 0.0, 1.0, 100 );
      }

      return StatusCode::SUCCESS;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( ChargedProtoANNPIDMoni )

} // namespace ANNGlobalPID
