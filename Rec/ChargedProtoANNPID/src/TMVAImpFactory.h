/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// gaudi
#include "GaudiKernel/GaudiException.h"

// STL
#include <cmath>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

// Event model / Kernel
#include "Event/Track.h"
#include "Kernel/RichParticleIDType.h"

// Include VDT here so it gets into all TMVA compilations
#include "vdt/exp.h"

// TMVA IClassifierReader interface
#include "TMVAIClassifierReader.h"

// So the TMVA networks pick up the std:: functions
using namespace std;

namespace ANNGlobalPID {

  /// Known tunes
  enum class Tunes { MCUpTune_V1 };

  namespace {

    /// Tune enum to string
    inline decltype( auto ) name( const Tunes tune ) {
      if ( tune == Tunes::MCUpTune_V1 ) { return "MCUpTuneV1"s; }
      return "UNKNOWN"s;
    }

    /// particle enum to string
    inline decltype( auto ) name( const Rich::ParticleIDType particle ) {
      return ( particle != Rich::BelowThreshold ? Rich::text( particle ) : "ghost"s );
    }

    /// Track enum to string
    inline decltype( auto ) name( const LHCb::Track::Types track ) { return LHCb::Track::TypesToString( track ); }

  } // namespace

  /** @class TMVAImpFactory TMVAImpFactory.h
   *
   *  Factory to create instances of standalone C++ TVMA objects for
   *  a given track, hypothesis and network tuning.
   *
   *  @author Chris Jones
   *  @date   2013-03-12
   */
  class TMVAImpFactory final {

  public:
    // vector of input names
    using InputNames = std::vector<std::string>;

  private:
    /** @class FactoryBase TMVAImpFactory.h
     *
     *  Base class for factories
     *
     *  @author Chris Jones
     *  @date   2013-03-12
     */
    class FactoryBase {
    public:
      /// Create an instance of the TMVA classifier for this factory
      virtual std::unique_ptr<IClassifierReader> create( const InputNames& inputs ) = 0;
      /// Destructor
      virtual ~FactoryBase() = default;
    };

    /** @class TMVAFactory TMVAImpFactory.h
     *
     *  Templated class for specific TMVA factories
     *
     *  @author Chris Jones
     *  @date   2013-03-12
     */
    template <class TMVATYPE>
    class TMVAFactory final : public FactoryBase {
    public:
      /// Create an instance of the TMVA classifier for this factory
      std::unique_ptr<IClassifierReader> create( const InputNames& inputs ) override {
        return std::make_unique<TMVATYPE>( inputs );
      }
    };

  public:
    /// Standard constructor
    TMVAImpFactory();

  private:
    /// Returns the id string for a given configuration
    decltype( auto ) id( const std::string& config,   //
                         const std::string& particle, //
                         const std::string& track ) const {
      return config + "-" + particle + "-" + track;
    }

    /// Register a new TMVA instance with the factory
    template <class TMVATYPE>
    void add( std::string config,   //
              std::string particle, //
              std::string track ) {
      const auto _id = id( config, particle, track );
      const auto i   = m_map.find( _id );
      if ( UNLIKELY( i != m_map.end() ) ) {
        throw GaudiException( _id + " already registered", "ANNGlobalPID::TMVAImpFactory", StatusCode::FAILURE );
      }
      m_map[_id] = std::make_unique<TMVAFactory<TMVATYPE>>();
    }

    /// Register a new TMVA instance with the factory
    template <class TMVATYPE>
    void add( const Tunes                tune,     //
              const Rich::ParticleIDType particle, //
              const LHCb::Track::Types   track ) {
      return add<TMVATYPE>( name( tune ), name( particle ), name( track ) );
    }

    /// Add named MVA.
    /// Explicit specialisations for each MVA should be defined in tmva/TUNE/TRACK-PID.cpp
    template <Tunes, LHCb::Track::Types, Rich::ParticleIDType>
    void add();

    // Add all MVAs for a given tune
    template <Tunes tune>
    void add() {
      add<tune, LHCb::Track::Types::Long>();
      add<tune, LHCb::Track::Types::Downstream>();
      add<tune, LHCb::Track::Types::Upstream>();
    }

    // Add all MVAs for a given tune and track type
    template <Tunes tune, LHCb::Track::Types track>
    void add() {
      add<tune, track, Rich::Electron>();
      add<tune, track, Rich::Muon>();
      add<tune, track, Rich::Pion>();
      add<tune, track, Rich::Kaon>();
      add<tune, track, Rich::Proton>();
      add<tune, track, Rich::BelowThreshold>();
    }

  public:
    /// Get an instance for a given set of parameters
    std::unique_ptr<IClassifierReader> create( const std::string&              config,   //
                                               const std::string&              particle, //
                                               const std::string&              track,    //
                                               const std::vector<std::string>& inputs ) const {
      const auto i = m_map.find( id( config, particle, track ) );
      return ( i != m_map.end() ? i->second->create( inputs ) : nullptr );
    }

  private:
    /// The map of known MVA instance generators linked to a given nickname
    std::unordered_map<std::string, std::unique_ptr<FactoryBase>> m_map;
  };

  /// Method to get a static instance of the factory
  const TMVAImpFactory& tmvaFactory();

} // namespace ANNGlobalPID
