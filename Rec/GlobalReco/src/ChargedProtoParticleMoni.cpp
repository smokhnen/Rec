/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "RichUtils/RichPoissonEffFunctor.h"
#include "RichUtils/RichStatDivFunctor.h"
#include "boost/format.hpp"
#include <map>
#include <mutex>
#include <utility>
#include <vector>

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleMoni.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleMoni
 *  Algorithm to build charged ProtoParticles from charged Tracks.
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */
//-----------------------------------------------------------------------------
namespace {
  /// Simple utility tally class
  class TrackTally {
  public:
    /// Default constructor
    TrackTally() = default;

  public:
    unsigned long long totTracks      = 0; ///< Number of considered tracks
    unsigned long long selTracks      = 0; ///< Number of tracks selected to creaste a ProtoParticle from
    unsigned long long ecalTracks     = 0; ///< Number of ProtoParticles created with CALO ECAL info
    unsigned long long bremTracks     = 0; ///< Number of ProtoParticles created with CALO BREM info
    unsigned long long spdTracks      = 0; ///< Number of ProtoParticles created with CALO SPD info
    unsigned long long prsTracks      = 0; ///< Number of ProtoParticles created with CALO PRS info
    unsigned long long hcalTracks     = 0; ///< Number of ProtoParticles created with CALO HCAL info
    unsigned long long richTracks     = 0; ///< Number of ProtoParticles created with RICH info
    unsigned long long muonTracks     = 0; ///< Number of ProtoParticles created with MUON info
    unsigned long long velodEdxTracks = 0; ///< Number of ProtoParticles created with VELO dE/dx info
  };
} // namespace

class ChargedProtoParticleMoni final : public GaudiHistoAlg {

public:
  /// Standard constructor
  ChargedProtoParticleMoni( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override final;  ///< Algorithm execution
  StatusCode finalize() override final; ///< Algorithm finalization

private:
  /// Print statistics
  void printStats( const MSG::Level level = MSG::INFO ) const;

  /// Find the ProtoParticle created from a given Track
  const LHCb::ProtoParticle* getProto( const LHCb::ProtoParticles* protos, const LHCb::Track* track ) const;

  /// Location of the ProtoParticles in the TES
  Gaudi::Property<std::string> m_protoPath{this, "OutputProtoParticleLocation", LHCb::ProtoParticleLocation::Charged};

  /// Location in TES of input Tracks
  Gaudi::Property<std::string> m_tracksPath{this, "InputTrackLocation", LHCb::TrackLocation::Default};

  std::mutex m_mutex;

  /// Event count
  unsigned long long m_nEvts = 0;

  /// Map type containing tally for various track types
  /// Total number of tracks considered and selected
  std::map<LHCb::Track::Types, TrackTally> m_nTracks;
};
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleMoni )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoParticleMoni::ChargedProtoParticleMoni( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiHistoAlg( name, pSvcLocator ) {
  // histo base dir
  setProperty( "HistoTopDir", "PROTO/" ).ignore();
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode ChargedProtoParticleMoni::finalize() {
  // summary printout
  printStats( MSG::INFO );

  // execute base class finalise and return
  return GaudiHistoAlg::finalize();
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleMoni::execute() {
  // FIXME/BUG/TODO: this code is not thread safe... so we brute force it...
  auto _ = std::scoped_lock{m_mutex};

  using namespace Gaudi::Units;

  // Load the Track objects
  const auto* tracks = getIfExists<LHCb::Tracks>( m_tracksPath.value() );
  if ( !tracks ) return Warning( "No Tracks at '" + m_tracksPath + "'", StatusCode::SUCCESS );

  // Load the ProtoParticles
  const auto* protos = getIfExists<LHCb::ProtoParticles>( m_protoPath.value() );
  if ( !protos ) return Warning( "No ProtoParticles at '" + m_protoPath + "'", StatusCode::SUCCESS );

  // count events
  ++m_nEvts;

  // Loop over tracks
  for ( const LHCb::Track* track : *tracks ) {
    // track tally
    auto& tally = m_nTracks[track->type()];

    // count all tracks
    ++tally.totTracks;

    // Does this track have a proto ?
    const auto* proto = getProto( protos, track );

    // Track Type
    const auto type = LHCb::Track::TypesToString( track->type() ) + "/";

    // Eff. for making ProtoParticles
    profile1D( track->p(), 100.0 * (int)( proto != nullptr ), type + "trackToProtoEff",
               "% Tracks with ProtoParticles V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Proceed only with tracks with ProtoParticles
    if ( !proto ) continue;

    // count tracks with ProtoParticles
    ++tally.selTracks;

    // Printout ProtoParticles + PID info
    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "ProtoParticle : " << *proto << endmsg;
      if ( proto->richPID() ) { verbose() << " -> RichPID : " << *( proto->richPID() ) << endmsg; }
      if ( proto->muonPID() ) { verbose() << " -> MuonPID : " << *( proto->muonPID() ) << endmsg; }
    }

    // RICH info
    const bool hasRICH = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::RichPIDStatus );
    if ( hasRICH ) { ++tally.richTracks; }
    profile1D( track->p(), 100.0 * (int)hasRICH, type + "protosWithRICH", "% ProtoParticles with RICH info V Momentum",
               0 * GeV, 100 * GeV, 100 );

    // Muon INFO
    const bool hasMUON = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccMuon );
    if ( hasMUON ) { ++tally.muonTracks; }
    profile1D( track->p(), 100.0 * (int)hasMUON, type + "protosWithMUON", "% ProtoParticles with MUON info V Momentum",
               0 * GeV, 100 * GeV, 100 );

    // ECAL info
    const bool hasECAL = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccEcal );
    if ( hasECAL ) { ++tally.ecalTracks; }
    profile1D( track->p(), 100.0 * (int)hasECAL, type + "protosWithECAL",
               "% ProtoParticles with CALO-ECAL info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Brem info
    const bool hasBREM = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccBrem );
    if ( hasBREM ) { ++tally.bremTracks; }
    profile1D( track->p(), 100.0 * (int)hasBREM, type + "protosWithBREM",
               "% ProtoParticles with CALO-BREM info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Spd info
    const bool hasSPD = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccSpd );
    if ( hasSPD ) { ++tally.spdTracks; }
    profile1D( track->p(), 100.0 * (int)hasSPD, type + "protosWithSPD",
               "% ProtoParticles with CALO-SPD info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // PRS info
    const bool hasPRS = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccPrs );
    if ( hasPRS ) { ++tally.prsTracks; }
    profile1D( track->p(), 100.0 * (int)hasPRS, type + "protosWithPRS",
               "% ProtoParticles with CALO-PRS info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Add Hcal info
    const bool hasHCAL = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::InAccHcal );
    if ( hasHCAL ) { ++tally.hcalTracks; }
    profile1D( track->p(), 100.0 * (int)hasHCAL, type + "protosWithHCAL",
               "% ProtoParticles with CALO-HCAL info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Add Velo dE/dx info
    const bool hasDEDX = proto->hasInfo( LHCb::ProtoParticle::additionalInfo::VeloCharge );
    if ( hasDEDX ) { ++tally.velodEdxTracks; }
    profile1D( track->p(), 100.0 * (int)hasDEDX, type + "protosWithVELOdEdx",
               "% ProtoParticles with VELO dE/dx info V Momentum", 0 * GeV, 100 * GeV, 100 );

    // Muon consistency checks
    if ( ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus ) ||
           proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonMuLL ) ||
           proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonBkgLL ) ||
           proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonNShared ) ) &&
         !proto->muonPID() ) {
      Warning( "ProtoParticle has MuonPIDStatus but no NuonPID SmartRef" ).ignore();
    }
    if ( proto->muonPID() && !proto->muonPID()->IsMuonLoose() && proto->muonPID()->IsMuon() ) {
      Warning( "ProtoParticle MuonPID has IsMuonLoose=False but IsMuon=True" ).ignore();
    }
  }

  // return
  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Find the ProtoParticle created from a given Track
//=============================================================================
const LHCb::ProtoParticle* ChargedProtoParticleMoni::getProto( const LHCb::ProtoParticles* protos,
                                                               const LHCb::Track*          track ) const {
  const LHCb::ProtoParticle* proto = nullptr;

  // Check Track pointer is OK
  if ( track ) {

    // First try using same track key convention
    proto = protos->object( track->key() );

    // if key search did not work, try brute force
    if ( !proto || proto->track() != track ) {
      auto iP = std::find_if( std::begin( *protos ), std::end( *protos ),
                              [&track]( const auto* i ) { return i->track() == track; } );
      proto   = ( iP != std::end( *protos ) ? *iP : nullptr );
    }

  } // track OK

  return proto;
}

//=============================================================================
// Print Stats
//=============================================================================
void ChargedProtoParticleMoni::printStats( const MSG::Level level ) const {
  MsgStream& msg = msgStream( level );

  // Statistical tools
  const Rich::PoissonEffFunctor eff( "%5.1f +-%4.1f" );
  const Rich::StatDivFunctor    occ( "%6.2f +-%5.2f" );

  const std::string lines( 148, '-' );
  const std::string LINES( 148, '=' );

  msg << LINES << endmsg;
  msg << " # Tracks Per Event         |    Track     |     RICH         MUON         ECAL         "
      << "BREM         SPD          PRS          HCAL     VELO(dE/dx)" << endmsg;
  msg << lines << endmsg;
  for ( const auto& iT : m_nTracks ) {
    const auto& tally = iT.second;
    msg << " " << occ( tally.selTracks, m_nEvts );
    msg << boost::format( " %|-12|" ) % iT.first;
    msg << "| " << eff( tally.selTracks, tally.totTracks ) << " |";
    msg << eff( tally.richTracks, tally.selTracks ) << " ";
    msg << eff( tally.muonTracks, tally.selTracks ) << " ";
    msg << eff( tally.ecalTracks, tally.selTracks ) << " ";
    msg << eff( tally.bremTracks, tally.selTracks ) << " ";
    msg << eff( tally.spdTracks, tally.selTracks ) << " ";
    msg << eff( tally.prsTracks, tally.selTracks ) << " ";
    msg << eff( tally.hcalTracks, tally.selTracks ) << " ";
    msg << eff( tally.velodEdxTracks, tally.selTracks );
    msg << endmsg;
  }
  msg << LINES << endmsg;
}
