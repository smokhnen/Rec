/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRACK_H
#define LOKI_TRACK_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
#include <boost/callable_traits.hpp>
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// Track Interfaces
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/ExtraInfo.h"
#include "LoKi/Interface.h"
#include "LoKi/TES.h"
#include "LoKi/TrackTypes.h"
// ============================================================================
namespace LoKi::Track {
  namespace details {
    template <size_t N, typename meta>
    using arg_t = std::tuple_element_t<N, boost::callable_traits::args_t<decltype( meta::fun )>>;

    template <size_t N, typename meta>
    using parg_t = std::add_pointer_t<arg_t<N, meta>>;

    /** @class details::UnaryFun
     *  type erasure wrapper to convert a small 'meta' struct which defines an
     *  unary callable 'fun' and a const char* 'symbol' into a LoKi functor
     *
     *  @author Gerhard Raven
     */

    template <typename meta>
    struct GAUDI_API UnaryFun final : LoKi::BasicFunctors<parg_t<0, meta>>::Function {
      // ======================================================================
      /// Default Constructor
      UnaryFun() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("virtual constructor")
      UnaryFun* clone() const override { return new UnaryFun( *this ); }
      /// mandatory: the only one essential method
      double operator()( parg_t<0, meta> t ) const override {
        if ( UNLIKELY( !t ) ) {
          this->Error( "LHCb::Track* points to NULL" ).ignore();
          return meta::invalid;
        }
        return std::invoke( meta::fun, *t );
      }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << meta::symbol; }
      // ======================================================================
    };

    /** @class details::UnaryPredicate
     *  type erasure wrapper to convert a small 'meta' struct which defines an
     *  unary callable 'fun' and a const char* 'symbol' into a LoKi Predicate
     *
     *  @author Gerhard Raven
     */

    template <typename meta>
    struct GAUDI_API UnaryPredicate final : LoKi::BasicFunctors<parg_t<0, meta>>::Predicate {
      // ======================================================================
      /// Default Constructor
      UnaryPredicate() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("virtual constructor")
      UnaryPredicate* clone() const override { return new UnaryPredicate( *this ); }
      /// mandatory: the only one essential method
      bool operator()( parg_t<0, meta> t ) const override {
        if ( UNLIKELY( !t ) ) {
          this->Error( "LHCb::Track* points to NULL" ).ignore();
          return meta::invalid;
        }
        return std::invoke( meta::fun, *t );
      }
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << meta::symbol; }
      // ======================================================================
    };
  } // namespace details
    // ==========================================================================
  /** @namespace LoKi::Track Track.h LoKi/Track.h
   *  Namespace with few basic "track"-functors
   *  @see LHCb::Track
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */

  // ========================================================================
  /** @class Key
   *  simple evaluator of the "key"
   *  @see LoKi::Cuts::TrKEY
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct Key {
      static constexpr auto       fun      = []( const LHCb::Track& t ) { return t.key(); };
      static constexpr const char symbol[] = "TrKEY";
      static constexpr auto       invalid  = -1;
    };
  } // namespace details
  using Key = details::UnaryFun<details::Key>;

  // ========================================================================
  /** @class InTES
   *  Simple predicate which check is teh obejct registered in TES
   *  @see LoKi::Cuts::TrINTES
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct InTES {
      static constexpr auto       fun      = []( const LHCb::Track& t ) { return t.parent() != nullptr; };
      static constexpr const char symbol[] = "TrINTES";
      static constexpr auto       invalid  = false;
    };
  } // namespace details
  using InTES = details::UnaryFun<details::InTES>;

  // ========================================================================
  /** @class Charge
   *  simple evaluator of the charge of the track
   *  @see LoKi::Cuts::TrQ
   *  @author Vanya BELYAEV ibelyaev@physiocs.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct Charge {
      static constexpr auto       fun      = &LHCb::Track::charge;
      static constexpr const char symbol[] = "TrQ";
      static constexpr auto       invalid  = LoKi::Constants::InvalidCharge;
    };
  } // namespace details
  using Charge = details::UnaryFun<details::Charge>;

  // ========================================================================
  /** @class TransverseMomentum
   *  simple evaluator of transverse momentum of the track
   *  @see LoKi::Cuts::TrPT
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct TransverseMomentum {
      static constexpr auto       fun      = &LHCb::Track::pt;
      static constexpr const char symbol[] = "TrPT";
      static constexpr auto       invalid  = LoKi::Constants::InvalidMomentum;
    };
  } // namespace details
  using TransverseMomentum = details::UnaryFun<details::TransverseMomentum>;

  // ========================================================================
  /** @class Momentum
   *  @see LoKi::Cuts::TrP
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct Momentum {
      static constexpr auto       fun      = &LHCb::Track::p;
      static constexpr const char symbol[] = "TrP";
      static constexpr auto       invalid  = LoKi::Constants::InvalidMomentum;
    };
  } // namespace details
  using Momentum = details::UnaryFun<details::Momentum>;

  // ========================================================================
  /** @class Phi
   *  @see LoKi::Cuts::TrPHI
   *  @see LHCb::Track::phi
   *  simple evaluator of phi angle for the track
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date   2015-01-30
   */
  namespace details {
    struct Phi {
      static constexpr auto       fun      = &LHCb::Track::phi;
      static constexpr const char symbol[] = "TrPHI";
      static constexpr auto       invalid  = LoKi::Constants::InvalidAngle;
    };
  } // namespace details
  using Phi = details::UnaryFun<details::Phi>;

  // ========================================================================
  /** @class Eta
   *  @see LoKi::Cuts::TrETA
   *  @see LHCb::Track::pseudoRapidity
   *  simple evaluator of pseudorapidity for the track
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date   2015-01-30
   */
  namespace details {
    struct Eta {
      static constexpr auto       fun      = &LHCb::Track::pseudoRapidity;
      static constexpr const char symbol[] = "TrETA";
      static constexpr auto       invalid  = LoKi::Constants::InvalidAngle;
    };
  } // namespace details
  using Eta = details::UnaryFun<details::Eta>;

  // ========================================================================
  /** @class MomentumX
   *  @see LoKi::Cuts::TrPX
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
   *  @date   2011-03-18
   */
  namespace details {
    struct MomentumX {
      static constexpr auto       fun      = []( const LHCb::Event::v1::Track& t ) { return t.momentum().X(); };
      static constexpr const char symbol[] = "TrPX";
      static constexpr auto       invalid  = LoKi::Constants::InvalidMomentum;
    };
  } // namespace details
  using MomentumX = details::UnaryFun<details::MomentumX>;

  // ========================================================================
  /** @class MomentumY
   *  @see LoKi::Cuts::TrPY
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
   *  @date   2011-03-18
   */
  namespace details {
    struct MomentumY {
      static constexpr auto       fun      = []( const LHCb::Event::v1::Track& t ) { return t.momentum().Y(); };
      static constexpr const char symbol[] = "TrPY";
      static constexpr auto       invalid  = LoKi::Constants::InvalidMomentum;
    };
  } // namespace details
  using MomentumY = details::UnaryFun<details::MomentumY>;

  // ========================================================================
  /** @class MomentumZ
   *  @see LoKi::Cuts::TrPZ
   *  simple evaluator of momentum of the track
   *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
   *  @date   2011-03-18
   */
  namespace details {
    struct MomentumZ {
      static constexpr auto       fun      = []( const LHCb::Event::v1::Track& t ) { return t.momentum().Z(); };
      static constexpr const char symbol[] = "TrPZ";
      static constexpr auto       invalid  = LoKi::Constants::InvalidMomentum;
    };
  } // namespace details
  using MomentumZ = details::UnaryFun<details::MomentumZ>;

  // ========================================================================
  /** @class CheckFlag
   *  @see LoKi::Cuts::TrISFLAG
   *  @see LoKi::Cuts::TrBACKWARD
   *  @see LoKi::Cuts::TrINVALID
   *  @see LoKi::Cuts::TrCLONE
   *  @see LoKi::Cuts::TrUSED
   *  @see LoKi::Cuts::TrIPSELECTED
   *  @see LoKi::Cuts::TrPIDSELECTED
   *  @see LoKi::Cuts::TrSELECTED
   *  @simple predicate to check the flag
   *  @author Vanya BELYAEV ibelyaev@physiocs.syr.edu
   *  @date   2007-06-08
   */
  class GAUDI_API CheckFlag final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate {
  public:
    // ======================================================================
    /// constructor form the flag ;
    CheckFlag( LHCb::Track::Flags flag );
    /// MANDATORY: clone method ("virtual constructor")
    CheckFlag* clone() const override { return new CheckFlag( *this ); }
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  private:
    // ======================================================================
    // the flag to be checked:
    LHCb::Track::Flags m_flag; ///< the flag to be checked:
    // ======================================================================
  };
  // ========================================================================
  /** @class Selector
   *  Simple class GAUDI_API to use "track-selector"
   *  @see ITrackSelector
   *  @see LoKi:Cuts::TrSELECTOR
   *  @author Vanya BELYAEV ibelayev@physics.syr.edu
   *  @date 2007-06-10
   */
  class GAUDI_API Selector : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate {
  public:
    // ======================================================================
    /// default constructor
    Selector() = default;
    /// constructor form the tool
    Selector( const ITrackSelector* tool );
    /// constructor form the tool
    Selector( const LoKi::Interface<ITrackSelector>& tool );
    /// MANDATORY: clone method ("virtual constructor")
    Selector* clone() const override { return new Selector( *this ); }
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  public:
    // ======================================================================
    /// conversion operator to the tool
                                           operator const LoKi::Interface<ITrackSelector>&() const { return m_tool; }
    const LoKi::Interface<ITrackSelector>& selector() const { return m_tool; }
    // ======================================================================
  protected:
    // ======================================================================
    /// set new selector tool
    void setSelector( const ITrackSelector* selector ) const;
    // ======================================================================
  protected:
    // ======================================================================
    /// the tool itself
    mutable LoKi::Interface<ITrackSelector> m_tool = nullptr; // the tool itself
    // ======================================================================
  };
  // ========================================================================
  /** @class HasInfo
   *  Trivial predicate which evaluates LHCb::Track::hasInfo
   *  function
   *
   *  It relies on the method LoKi::Info::hasInfo
   *
   *  @see LHCb::Track
   *  @see LoKi::Cuts::TrHASINFO
   *  @see LoKi::ExtraInfo::CheckInfo
   *  @see LoKi::ExtraInfo::hasInfo
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-02-15
   */
  class GAUDI_API HasInfo final : public LoKi::ExtraInfo::CheckInfo<const LHCb::Track*> {
  public:
    // ======================================================================
    /** constructor from "info"
     *  @param key info index/mark/key
     */
    HasInfo( const int key );
    /// clone method (mandatory!)
    HasInfo* clone() const override { return new HasInfo( *this ); }
    /// the specific printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  };
  // ========================================================================
  /** @class Info
   *  Trivial function which evaluates LHCb::Track::info
   *
   *  It relies on the method LoKi::ExtraInfo::info
   *
   *  @see LHCb::Track
   *  @see LoKi::Cuts::TrINFO
   *  @see LoKi::ExtraInfo::GeInfo
   *  @see LoKi::ExtraInfo::info
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-02-15
   */
  class GAUDI_API Info final : public LoKi::ExtraInfo::GetInfo<const LHCb::Track*> {
  public:
    // ======================================================================
    /** constructor from "info"
     *  @param key info index/mark/key
     *  @param def default value for missing key/invalid object
     */
    Info( const int key, const double def );
    /// clone method (mandatory!)
    Info* clone() const override { return new Info( *this ); }
    /// the specific printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  };
  // ========================================================================
  /** @class SmartInfo
   *  Trivial function which:
   *    - checks the presence of informnation in LHCb::Track::extraInfo
   *    - if the information present, it returns it
   *    - for missing infomation, use function to evaluate it
   *    - (optionally) fill th emissing field
   *
   *  @see LHCb::Track
   *  @see LoKi::Cuts::TrSINFO
   *  @see LoKi::ExtraInfo::GetSmartInfo
   *  @see LoKi::ExtraInfo::info
   *  @see LoKi::ExtraInfo::hasInfo
   *  @see LoKi::ExtraInfo::setInfo
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-02-15
   */
  class GAUDI_API SmartInfo final : public LoKi::ExtraInfo::GetSmartInfo<const LHCb::Track*> {
  public:
    // ======================================================================
    /** constructor from fuction, key and update-flag
     *  @param index the key in LHCb::Track::extraInfo table
     *  @param fun functionto be evaluated for missing keys
     *  @param update the flag to allow the insert of mnissing information
     */
    SmartInfo( const int index, const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun,
               const bool update = false );
    /// clone method (mandatory!)
    SmartInfo* clone() const override { return new SmartInfo( *this ); }
    /// the specific printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  };
  // ========================================================================
  /** @class Chi2
   *  simple evaluator of the LHcb::Track::chi2
   *  @see LoKi::Cuts::TrCHI2
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct Chi2 {
      static constexpr auto       fun      = &LHCb::Track::chi2;
      static constexpr const char symbol[] = "TrCHI2";
      static constexpr auto       invalid  = LoKi::Constants::InvalidChi2;
    };
  } // namespace details
  using Chi2 = details::UnaryFun<details::Chi2>;
  // ========================================================================
  /** @class Chi2PerDoF
   *  simple evaluator of the LHcb::Track::chi2PerDoF
   *  @see LoKi::Cuts::TrCHI2PDOF
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date   2007-06-08
   */
  namespace details {
    struct Chi2PerDoF {
      static constexpr auto       fun      = &LHCb::Track::chi2PerDoF;
      static constexpr const char symbol[] = "TrCHI2PDOF";
      static constexpr auto       invalid  = LoKi::Constants::InvalidChi2;
    };
  } // namespace details
  using Chi2PerDoF = details::UnaryFun<details::Chi2PerDoF>;
  // ========================================================================
  /** @class ProbChi2
   *  simple evaluator of the LHCb::Track::probChi2
   *  @see LoKi::Cuts::TrPROBCHI2
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct ProbChi2 {
      static constexpr auto       fun      = &LHCb::Track::chi2PerDoF;
      static constexpr const char symbol[] = "TrPROBCHI2";
      static constexpr auto       invalid  = LoKi::Constants::InvalidChi2;
    };
  } // namespace details
  using ProbChi2 = details::UnaryFun<details::ProbChi2>;
  // ========================================================================
  /** @class GhostProb
   *  simple evaluator of the LHCb::Track::ghostProbability
   *  @see LoKi::Cuts::TrGHOSTPROB
   *  @author Sascha Stahl sascha.stahl@cern.ch
   *  @date   2016-01-12
   */
  namespace details {
    struct GhostProb {
      static constexpr auto       fun      = &LHCb::Track::ghostProbability;
      static constexpr const char symbol[] = "TrGHOSTPROB";
      static constexpr auto       invalid  = LoKi::Constants::InvalidChi2;
    };
  } // namespace details
  using GhostProb = details::UnaryFun<details::GhostProb>;
  // ========================================================================
  /** @class HasStateAt
   *  Simple predicate which evaluates LHCb::Track::HasStateAt
   *  @see LoKi::Cuts::TrHASSTATE
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  class GAUDI_API HasStateAt final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate {
  public:
    // ======================================================================
    /// constructor
    HasStateAt( const LHCb::State::Location& loc );
    /// MANDATORY: clone method ("virtual constructor")
    HasStateAt* clone() const override { return new HasStateAt( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override { return s << "TrHASSTATE(" << loc() << ")"; }
    // ======================================================================
  public:
    // ======================================================================
    /// get the location
    int loc() const { return m_loc; }
    // ======================================================================
  private:
    // ======================================================================
    /// the location
    LHCb::State::Location m_loc; // the location
    // ======================================================================
  };
  // ========================================================================
  /** @class IsOnTrack
   *  Simple predicate which evaluates LHCb::Track::isOnTrack
   *  @see LoKi::Cuts::TrISONTRACK
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  class GAUDI_API IsOnTrack final : public LoKi::BasicFunctors<const LHCb::Track*>::Predicate {
  public:
    // ======================================================================
    IsOnTrack( const LHCb::LHCbID& id );
    /// MANDATORY: clone method ("virtual constructor")
    IsOnTrack* clone() const override { return new IsOnTrack( *this ); }
    /// mandatory: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: the nice printout
    std::ostream& fillStream( std::ostream& s ) const override {
      return s << "TrIsOnTRACK(LHCb.LHCbID(" << m_id.lhcbID() << "))";
    }
    // ======================================================================
  private:
    // ======================================================================
    LHCb::LHCbID m_id;
    // ======================================================================
  };
  // ========================================================================
  /** @class Type
   *  simple evaluator of the LHCb::Track::type
   *  @see LHCb::Track::type
   *  @see LHCb::Track::Types
   *  @see LoKi::Cuts::TrTYPE
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-06-08
   */
  namespace details {
    struct Type {
      static constexpr auto       fun      = &LHCb::Track::type;
      static constexpr const char symbol[] = "TrTYPE";
      static constexpr auto       invalid  = -1;
    };
  } // namespace details
  using Type = details::UnaryFun<details::Type>;
  // ========================================================================
  /** @class StateZ
   *  check Z-position for the given state
   *  @see LoKi::Cuts::TrSTATEZ
   *  @see LoKi::Cuts::TrFIRSTHITZ
   *  @see LHCb::State::Location
   *  @author Vanya Belyaev@nikhef.nl
   *  @date 2010-06-02
   */
  class GAUDI_API StateZ final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
  public:
    // ======================================================================
    /// constructor with the state indicator
    StateZ( const LHCb::State::Location location = LHCb::State::Location::FirstMeasurement );
    /// constructor with the state indicator & bad value
    StateZ( const LHCb::State::Location location, const double bad );
    /// MANDATORY: clone method ("virtual constructor")
    StateZ* clone() const override;
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  public:
    // ======================================================================
    /// get the string representation of the state
    const std::string& state() const; // the string representation
    // ======================================================================
  private:
    // ======================================================================
    /// the state itself
    LHCb::State::Location m_state; // the state itself
    /// the bad value
    double m_bad; //    the bad value
    /// the state
    mutable std::string m__state; //        the state
    // ======================================================================
  };
  // ========================================================================
  /** @class Cov2
   *  Get the element of covarinace element for the track state
   *  @see Tr_COV2
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2010-12-09
   */
  class GAUDI_API Cov2 final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    // ======================================================================
  public:
    // ======================================================================
    /// constructor from indices
    Cov2( const unsigned short i, const unsigned short j );
    /// constructor from indices & state location
    Cov2( const LHCb::State::Location location, const unsigned short i, const unsigned short j );
    /// constructor from the indices and Z-position:
    Cov2( const double z, const unsigned short i, const unsigned short j );
    /// MANDATORY: clone method ("virtual constructor")
    Cov2* clone() const override;
    /// MANDATORY: the only one essential method
    result_type operator()( argument t ) const override;
    /// OPTIONAL: nice printout
    std::ostream& fillStream( std::ostream& s ) const override;
    // ======================================================================
  private:
    // ======================================================================
    /// the default constructor is disabled
    Cov2(); // the default constructor is disabled
    // ======================================================================
  private:
    // ======================================================================
    enum _Case { _First, _Location, _Z };
    // ======================================================================
  private:
    // ======================================================================
    _Case                 m_case;
    double                m_z;
    LHCb::State::Location m_loc;
    unsigned short        m_i;
    unsigned short        m_j;
    // ======================================================================
  };
  // ========================================================================
  /** @class NVeloMissed
   *  @see Hlt::MissedVeloHits
   *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
   *  @date 2011-01-28
   */
  namespace details {
    struct NVeloMissed {
      static constexpr auto fun = []( const LHCb::Event::v1::Track& t ) {
        return t.hasInfo( LHCb::Track::AdditionalInfo::nPRVelo3DExpect )
                   ? ( t.info( LHCb::Track::AdditionalInfo::nPRVelo3DExpect, -1 ) - t.nLHCbIDs() )
                   : t.hasInfo( LHCb::Track::AdditionalInfo::nPRVeloRZExpect )
                         ? ( 2 * t.info( LHCb::Track::AdditionalInfo::nPRVeloRZExpect, -1 ) - t.nLHCbIDs() )
                         : -1;
      };
      static constexpr const char symbol[] = "TrNVPMISS";
      static constexpr auto       invalid  = LoKi::Constants::NegativeInfinity;
    };
  } // namespace details
  using NVeloMissed = details::UnaryFun<details::NVeloMissed>;
  // ========================================================================
  /** @class NTHits
   *  Count number of effective T-hits : 2x#IT + #OT
   *  @see LoKi::Cuts::TrNTHITS
   *  @see LoKi::Cuts::TrTNORMIDC
   *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
   *  @date 2011-02-02
   */
  namespace details {
    struct NTHits {
      static constexpr auto fun = []( const LHCb::Event::v1::Track& t ) {
        const auto& ids = t.lhcbIDs();
        return std::accumulate( ids.begin(), ids.end(), 0, []( int i, const auto& id ) {
          return id.isFT() ? i + 1 : i;
        } ); // PK-R3C needs a revision
      };
      static constexpr const char symbol[] = "TrNTHITS";
      static constexpr auto       invalid  = -1000;
    };
  } // namespace details
  using NTHits = details::UnaryFun<details::NTHits>;
  // ========================================================================
  /** @class HasT
   *  Check if track is of a type that goes thro T stations
   *  @see LHCb::Track::hasT
   *  @see LoKi::Cuts::TrHAST
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2011-03-18
   */
  namespace details {
    struct HasT {
      static constexpr auto       fun      = &LHCb::Track::hasT;
      static constexpr const char symbol[] = "TrHAST";
      static constexpr auto       invalid  = false;
    };
  } // namespace details
  using HasT = details::UnaryPredicate<details::HasT>;
  // ========================================================================
  /** @class HasVelo
   *  Check if track is of a type that goes thro Velo
   *  @see LHCb::Track::hasVelo
   *  @see LoKi::Cuts::TrHASVELO
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2011-03-18
   */
  namespace details {
    struct HasVelo {
      static constexpr auto       fun      = &LHCb::Track::hasVelo;
      static constexpr const char symbol[] = "TrHASVELO";
      static constexpr auto       invalid  = false;
    };
  } // namespace details
  using HasVelo = details::UnaryPredicate<details::HasVelo>;
  // ========================================================================
  /** @class HasUT
   *  Check if track is of a type that goes thro UT
   *  @see LHCb::Track::hasUT
   *  @see LoKi::Cuts::TrHASUT
   *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
   *  @date 2011-03-18
   */
  namespace details {
    struct HasUT {
      static constexpr auto       fun      = &LHCb::Track::hasUT;
      static constexpr const char symbol[] = "TrHASUT";
      static constexpr auto       invalid  = false;
    };
  } // namespace details
  using HasUT = details::UnaryPredicate<details::HasUT>;
  // ========================================================================
  /** @class MinimalImpactParameterChi2
   *  @see LoKi::Track::TrMINIPCHI2
   *  Evaluate the minimum impact parameter chi2 of the track to any vertex in the given location.
   *  @author Olli Lupton
   */
  class GAUDI_API MinimalImpactParameterChi2 final : public TrackTypes::TrFunc,
                                                     public LoKi::TES::DataHandle<VertexContainer> {
  public:
    MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm, const std::string& location );
    MinimalImpactParameterChi2( const GaudiAlgorithm* algorithm );
    MinimalImpactParameterChi2* clone() const override { return new MinimalImpactParameterChi2( *this ); };
    result_type                 operator()( argument track ) const override;
    std::ostream&               fillStream( std::ostream& s ) const override;
  };

  // ========================================================================
  /** @class MinimalImpactParameterChi2Cut
   *  @see LoKi::Track::TrMINIPCHI2CUT
   *  Check whether the minimum impact parameter chi2 of the track to any vertex in the given location exceeds the
   *  given cut.
   *  @author Olli Lupton
   */
  class GAUDI_API MinimalImpactParameterChi2Cut final : public TrackTypes::TrCuts,
                                                        public LoKi::TES::DataHandle<VertexContainer> {
    double m_ipchi2_cut{0.0};

  public:
    MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut, const std::string& location );
    MinimalImpactParameterChi2Cut( const GaudiAlgorithm* algorithm, double ipchi2_cut );
    MinimalImpactParameterChi2Cut* clone() const override { return new MinimalImpactParameterChi2Cut( *this ); };
    result_type                    operator()( argument track ) const override;
    std::ostream&                  fillStream( std::ostream& s ) const override;
  };

  // ========================================================================
  /// hash
  GAUDI_API std::size_t hash( const LHCb::Track* track );
  // ========================================================================
} //                                            end of namespace LoKi::Track
// ==========================================================================
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_TRACKS_H
