/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LOKI_TRACKIDS_H
#define LOKI_TRACKIDS_H 1
// ============================================================================
// Include files
// ============================================================================
#include <limits>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Kernel
// ============================================================================
#include "Kernel/FTChannelID.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/VPChannelID.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrackTypes.h"
// ============================================================================
/** @file LoKi/TrackIDs.h
 *  Collection of functors that deals with LHCbIDs for Track
 *  (on request from Wouter Hulsbergen)
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-12-17
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Track {
    // ========================================================================
    /** @class CountIDs
     *  Simple functor to count LHCbIDs that satisfy certain criteria
     *  @author  Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-12-17
     */
    class GAUDI_API CountIDs final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    public:
      // ======================================================================
      // the actual type of pointer to member function
      typedef bool ( LHCb::LHCbID::*PMF )() const;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from pointer to member function
      CountIDs( PMF fun );
      /// constructor from pointer to member function name
      CountIDs( const std::string& name );
      /// MANDATORY: clone method ("virtual consttructor")
      CountIDs* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the pointer to member function
      PMF m_pmf = nullptr; // the pointer to member function
      /// the actual fuinctor name
      std::string m_nick; // the actual fuinctor name
      // ======================================================================
    };
    // ========================================================================
    /** @class CountFTIDs
     *  Simple functor to count FTChannelID that satisfy certain criteria
     *  @author  Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-12-17
     */
    class GAUDI_API CountFTIDs final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    public:
      // ======================================================================
      /// the actual type of pointer to member function
      typedef unsigned int ( LHCb::FTChannelID::*PMF )() const;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from pointer to member function
      CountFTIDs( PMF fun, const unsigned int i );
      /// constructor from pointer to member function & list
      CountFTIDs( PMF fun, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function & range
      CountFTIDs( const unsigned int imin, PMF fun, const unsigned int imax );
      /// constructor from pointer to member function name
      CountFTIDs( const std::string& name, const unsigned int i );
      /// constructor from pointer to member function name  & list
      CountFTIDs( const std::string& name, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function name  & range
      CountFTIDs( const unsigned int imin, const std::string& nick, const unsigned int imax );
      /// MANDATORY: clone method ("virtual consttructor")
      CountFTIDs* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// set the nick properly
      void setNick( PMF pmf ); // set the nick properly
      /// set PMF properly
      void setPmf( const std::string& nick ); //      set PMF properly
      // ======================================================================
    private:
      // ======================================================================
      /// the pointer to member function
      PMF m_pmf = nullptr; // the pointer to member function
      /// the list of values
      std::vector<unsigned int> m_uints; //       the list of values
      /// the low edge
      unsigned int m_imin = std::numeric_limits<unsigned int>::max(); // the low edge
      /// the high edge
      unsigned int m_imax = std::numeric_limits<unsigned int>::min(); // the high edge
      /// the actual fuinctor name
      std::string m_nick; // the actual fuinctor name
      // ======================================================================
    };
    // ========================================================================
    /** @class CountUTIDs
     *  Simple functor to count UTChannelID that satisfy certain criteria
     *  @author  Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-12-17
     */
    class GAUDI_API CountUTIDs final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    public:
      // ======================================================================
      /// the actual type of pointer to member function
      typedef unsigned int ( LHCb::UTChannelID::*PMF )() const;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from pointer to member function
      CountUTIDs( PMF fun, const unsigned int i );
      /// constructor from pointer to member function & list
      CountUTIDs( PMF fun, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function & range
      CountUTIDs( const unsigned int imin, PMF fun, const unsigned int imax );
      /// constructor from pointer to member function name
      CountUTIDs( const std::string& name, const unsigned int i );
      /// constructor from pointer to member function name  & list
      CountUTIDs( const std::string& name, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function name  & range
      CountUTIDs( const unsigned int imin, const std::string& nick, const unsigned int imax );
      /// MANDATORY: clone method ("virtual consttructor")
      CountUTIDs* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// set the nick properly
      void setNick( PMF pmf ); // set the nick properly
      /// set PMF properly
      void setPmf( const std::string& nick ); //      set PMF properly
      // ======================================================================
    private:
      // ======================================================================
      /// the pointer to member function
      PMF m_pmf = nullptr; // the pointer to member function
      /// the list of values
      std::vector<unsigned int> m_uints; //       the list of values
      /// the low edge
      unsigned int m_imin = std::numeric_limits<unsigned int>::max(); // the low edge
      /// the high edge
      unsigned int m_imax = std::numeric_limits<unsigned int>::min(); // the high edge
      /// the actual fuinctor name
      std::string m_nick; // the actual fuinctor name
      // ======================================================================
    };
    // ========================================================================
    /** @class CountVPIDs
     *  Simple functor to count VPChannelID that satisfy certain criteria
     *  @author  Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-12-17
     */
    class GAUDI_API CountVPIDs final : public LoKi::BasicFunctors<const LHCb::Track*>::Function {
    public:
      // ======================================================================
      /// the actual type of pointer to member function
      typedef bool ( LHCb::VPChannelID::*PMF1 )() const;
      typedef unsigned int ( LHCb::VPChannelID::*PMF2 )() const;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from pointer to member function
      CountVPIDs( PMF2 fun, const unsigned int i );
      /// constructor from pointer to member function & list
      CountVPIDs( PMF2 fun, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function & range
      CountVPIDs( const unsigned int imin, PMF2 fun, const unsigned int imax );
      /// constructor from pointer to member function name
      CountVPIDs( const std::string& name, const unsigned int i );
      /// constructor from pointer to member function name  & list
      CountVPIDs( const std::string& name, const std::vector<unsigned int>& i );
      /// constructor from pointer to member function name  & range
      CountVPIDs( const unsigned int imin, const std::string& nick, const unsigned int imax );
      /// contructor from member-function
      CountVPIDs( PMF1 pmf );
      /// contructor from member-function
      CountVPIDs( const std::string& nick );
      /// MANDATORY: clone method ("virtual consttructor")
      CountVPIDs* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument t ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// set the nick properly
      void setNick( PMF2 pmf ); // set the nick properly
      /// set PMF properly
      void setPmf( const std::string& nick ); //      set PMF properly
      // ======================================================================
    private:
      // ======================================================================
      /// the pointer to member function
      PMF1 m_pmf1 = nullptr; // the pointer to member function
      /// the pointer to member function
      PMF2 m_pmf2 = nullptr; // the pointer to member function
      /// the list of values
      std::vector<unsigned int> m_uints; //      the list of values
      /// the low edge
      unsigned int m_imin = std::numeric_limits<unsigned int>::max(); //            the low edge
      /// the high edge
      unsigned int m_imax = std::numeric_limits<unsigned int>::min(); //           the high edge
      /// the actual fuinctor name
      std::string m_nick; // the actual functor name
      // ======================================================================
    };
    // ========================================================================
  } // namespace Track
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef TrIDC
     *  simple functor to count LHCbIDs
     *
     *  @code
     *
     *   const LHCb::Track* track = ... ;
     *
     *    TrIDC fun1 = TrIDC ( &LHCb::LHCbID::isOT ) ;
     *    TrIDC fun2 = TrIDC ( "isVP"            ) ;
     *
     *    const bool good = fun1 ( track ) > 10 && fun2 ( track ) > 6 ;
     *
     *  @endcode
     *
     *  @see LoKi::Track::CountIDs
     *  @see LHCb::LHCbIDs
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2010-01-01
     */
    typedef LoKi::Track::CountIDs TrIDC;
    // ========================================================================
    /** @typedef TrIDC
     *  simple functor to count FTChannelID
     *
     *  @code
     *
     *   const LHCb::Track* track = ... ;
     *
     *    TrIDC fun1 = TrIDC (     &LHCb::FTChannelID::layer  , 1   ) ;
     *
     *    TrIDC fun2 = TrIDC ( 3 , &LHCb::FTChannelID::module , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrFTIDC fun3 = TrFTIDC ( &LHCb::FTChannelID::station , lst  ) ;
     *
     *    TrFTIDC fun4 = TrFTIDC (     "layer"  , 1   ) ;
     *
     *    TrFTIDC fun5 = TrFTIDC ( 3 , "module" , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrFTIDC fun6 = TrFTIDC ( "station , lst  ) ;
     *
     *    const bool good =
     *               fun1 ( track ) > 1 &&
     *               fun2 ( track ) > 2 &&
     *               fun3 ( track ) > 3 &&
     *               fun4 ( track ) > 4 &&
     *               fun5 ( track ) > 5 &&
     *               fun6 ( track ) > 6   ;
     *
     *  @endcode
     *
     *  @see LoKi::Track::CountFTIDs
     *  @see LHCb::FTChannelID
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2010-01-01
     */
    typedef LoKi::Track::CountFTIDs TrFTIDC;
    // ========================================================================
    /** @typedef TrUTIDC
     *  simple functor to count UTChannelID
     *
     *  @code
     *
     *   const LHCb::Track* track = ... ;
     *
     *    TrUTIDC fun1 = TrUTIDC (     &LHCb::UTChannelID::sector  , 1   ) ;
     *
     *    TrUTIDC fun2 = TrUTIDC ( 3 , &LHCb::UTChannelID::layer , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrUTIDC fun3 = TrUTIDC ( &LHCb::UTChannelID::strip , lst  ) ;
     *
     *    TrUTIDC fun4 = TrUTIDC (     "sector"  , 1   ) ;
     *
     *    TrUTIDC fun5 = TrUTIDC ( 3 , "layer" , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrUTIDC fun6 = TrUTIDC ( "strip" , lst  ) ;
     *
     *    const bool good =
     *               fun1 ( track ) > 1 &&
     *               fun2 ( track ) > 2 &&
     *               fun3 ( track ) > 3 &&
     *               fun4 ( track ) > 4 &&
     *               fun5 ( track ) > 5 &&
     *               fun6 ( track ) > 6   ;
     *
     *  @endcode
     *
     *  @see LoKi::Track::CountUTIDs
     *  @see LHCb::UTChannelID
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2010-01-01
     */
    typedef LoKi::Track::CountUTIDs TrUTIDC;
    // ========================================================================
    /** @typedef TrVPIDC
     *  simple functor to count VPChannelID
     *
     *  @code
     *
     *   const LHCb::Track* track = ... ;
     *
     *    TrVPIDC fun1 = TrVPIDC (     &LHCb::VPChannelID::sensor  , 1   ) ;
     *
     *    TrVPIDC fun2 = TrVPIDC ( 3 , &LHCb::VPChannelID::strip , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrVPIDC fun3 = TrVPIDC ( &LHCb::VPChannelID::channelID , lst  ) ;
     *
     *    TrVPIDC fun4 = TrVPIDC (     "sensor"  , 1   ) ;
     *
     *    TrVPIDC fun5 = TrVPIDC ( 3 , "strip" , 10  ) ;
     *
     *    const std::vector<unsigned int>& lst = ... ;
     *    TrVPIDC fun6 = TrVPIDC ( "channelID" , lst  ) ;
     *
     *    const bool good =
     *               fun1 ( track ) > 1 &&
     *               fun2 ( track ) > 2 &&
     *               fun3 ( track ) > 3 &&
     *               fun4 ( track ) > 4 &&
     *               fun5 ( track ) > 5 &&
     *               fun6 ( track ) > 6   ;
     *
     *  @endcode
     *
     *  @see LoKi::Track::CountVPIDs
     *  @see LHCb::VPChannelID
     *  @see LHCb::Track
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2010-01-01
     */
    typedef LoKi::Track::CountVPIDs TrVPIDC;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_TRACKIDS_H
