/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToStream.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/TrackIDs.h"
// ============================================================================
// ============================================================================
/** @file
 *  Implementation file for class : TrackIDs
 *  Collection of functors that deals with LHCbIDs for Tracks
 *  (on request from Wouter Hulsbergen)
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-12-17
 */
// ============================================================================
namespace {
  static_assert( std::numeric_limits<unsigned int>::is_integer && std::numeric_limits<unsigned int>::is_specialized,
                 "" );

  static const auto s_countIDs_tbl =
      std::array{std::pair{&LHCb::LHCbID::isVP, "isVP"}, std::pair{&LHCb::LHCbID::isVP, "isVP"},
                 std::pair{&LHCb::LHCbID::isUT, "isUT"}, std::pair{&LHCb::LHCbID::isFT, "isFT"},
                 std::pair{&LHCb::LHCbID::isRich, "isRich"}, std::pair{&LHCb::LHCbID::isCalo, "isCalo"},
                 std::pair{&LHCb::LHCbID::isMuon, "isMuon"},
                 // Backwards compatability
                 std::pair{&LHCb::LHCbID::isVP, "isVPPix"}};

  // PK-R3C revision needed
  static const auto s_countFTIDs_tbl = std::array{std::pair{&LHCb::FTChannelID::channelID, "channelID"},
                                                  std::pair{&LHCb::FTChannelID::channelID, "channel"},
                                                  std::pair{&LHCb::FTChannelID::module, "module"},
                                                  std::pair{&LHCb::FTChannelID::quarter, "quarter"},
                                                  std::pair{&LHCb::FTChannelID::layer, "layer"},
                                                  std::pair{&LHCb::FTChannelID::station, "station"},
                                                  std::pair{&LHCb::FTChannelID::uniqueModule, "uniqueModule"},
                                                  std::pair{&LHCb::FTChannelID::uniqueQuarter, "uniqueQuarter"},
                                                  std::pair{&LHCb::FTChannelID::uniqueLayer, "uniqueLayer"}};

  static const auto s_countUTIDs_tbl = std::array{std::pair{&LHCb::UTChannelID::strip, "strip"},
                                                  std::pair{&LHCb::UTChannelID::sector, "sector"},
                                                  std::pair{&LHCb::UTChannelID::detRegion, "detRegion"},
                                                  std::pair{&LHCb::UTChannelID::detRegion, "region"},
                                                  std::pair{&LHCb::UTChannelID::layer, "layer"},
                                                  std::pair{&LHCb::UTChannelID::station, "station"},
                                                  std::pair{&LHCb::UTChannelID::uniqueLayer, "uniqueLayer"},
                                                  std::pair{&LHCb::UTChannelID::uniqueDetRegion, "uniqueDetRegion"},
                                                  std::pair{&LHCb::UTChannelID::uniqueDetRegion, "uniqueRegion"},
                                                  std::pair{&LHCb::UTChannelID::uniqueSector, "uniqueSector"},
                                                  std::pair{&LHCb::UTChannelID::type, "type"},
                                                  std::pair{&LHCb::UTChannelID::channelID, "channelID"},
                                                  std::pair{&LHCb::UTChannelID::channelID, "channel"}};

  static const auto s_countVPIDs_tbl2 =
      std::array{std::pair{&LHCb::VPChannelID::channelID, "channelID"},
                 std::pair{&LHCb::VPChannelID::channelID, "channel"}, std::pair{&LHCb::VPChannelID::sensor, "sensor"}};

  /* PK-R3C is this replaced by something ?
  static const auto s_countVPIDs_tbl1 = std::array{std::pair{&LHCb::VPChannelID::isPileUp, "isPileUp"},
                                                     std::pair{&LHCb::VPChannelID::isRType, "isRType"},
                                                     std::pair{&LHCb::VPChannelID::isPhiType, "isPhiType"}};
  static const auto s_countVPIDs_tbl3 = std::array{
      std::pair{"isPileUp", &LHCb::VPChannelID::isPileUp},   std::pair{"pileUp", &LHCb::VPChannelID::isPileUp},
      std::pair{"PileUp", &LHCb::VPChannelID::isPileUp},     std::pair{"isRType", &LHCb::VPChannelID::isRType},
      std::pair{"RType", &LHCb::VPChannelID::isRType},       std::pair{"rType", &LHCb::VPChannelID::isRType},
      std::pair{"R", &LHCb::VPChannelID::isRType},           std::pair{"r", &LHCb::VPChannelID::isRType},
      std::pair{"isPhiType", &LHCb::VPChannelID::isPhiType}, std::pair{"PhiType", &LHCb::VPChannelID::isPhiType},
      std::pair{"phiType", &LHCb::VPChannelID::isPhiType},   std::pair{"Phi", &LHCb::VPChannelID::isPhiType},
      std::pair{"phi", &LHCb::VPChannelID::isPhiType}};
  */

} // namespace
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs( LoKi::Track::CountIDs::PMF pmf ) : m_pmf( pmf ) {
  auto i =
      std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(), [&]( const auto& p ) { return p.first == pmf; } );
  if ( i != s_countIDs_tbl.end() ) m_nick = i->second;

  Assert( 0 != m_pmf, "Invalid LHCb::LHCbID member function!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountIDs::CountIDs( const std::string& nick ) : LoKi::AuxFunBase( std::tie( nick ) ), m_nick( nick ) {
  auto i =
      std::find_if( s_countIDs_tbl.begin(), s_countIDs_tbl.end(), [&]( const auto& p ) { return p.second == m_nick; } );
  if ( i != s_countIDs_tbl.end() ) m_pmf = i->first;
  if ( !m_pmf ) Exception( "Invalid LHCb::LHCbID member function: '" + m_nick + "'" );
}
// ============================================================================
// MANDATORY: clone method ("virtual consttructor")
// ============================================================================
LoKi::Track::CountIDs* LoKi::Track::CountIDs::clone() const { return new LoKi::Track::CountIDs( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountIDs::result_type LoKi::Track::CountIDs::operator()( LoKi::Track::CountIDs::argument t ) const {
  //
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return -1" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return -1;
  }
  //
  const auto& lhcbids = t->lhcbIDs();
  //
  return std::count_if( lhcbids.begin(), lhcbids.end(), [&]( const LHCb::LHCbID& id ) { return ( id.*m_pmf )(); } );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountIDs::fillStream( std::ostream& s ) const {
  return s << " TrIDC( '" << m_nick << "' ) ";
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( LoKi::Track::CountFTIDs::PMF pmf, const unsigned int i )
    : m_pmf( pmf ), m_uints( 1, i ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::FTchannelID member function!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( LoKi::Track::CountFTIDs::PMF pmf, const std::vector<unsigned int>& i )
    : m_pmf( pmf ), m_uints( i ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::FTchannelID member function!" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( const unsigned int imin, LoKi::Track::CountFTIDs::PMF pmf,
                                     const unsigned int imax )
    : m_pmf( pmf ), m_imin( imin ), m_imax( imax ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::FTChannelID member function!" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountFTIDs::setNick( LoKi::Track::CountFTIDs::PMF pmf ) {
  auto i =
      std::find_if( s_countFTIDs_tbl.begin(), s_countFTIDs_tbl.end(), [&]( const auto& p ) { return p.first == pmf; } );
  if ( i != s_countFTIDs_tbl.end() )
    m_nick = i->second;
  else
    Exception( "Invalid LHCb::FTChannelID member function!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( const std::string& nick, const std::vector<unsigned int>& i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_uints( i ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::FTChannelID member function: '" + m_nick + "'" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( const std::string& nick, const unsigned int i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_uints( 1, i ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::FTChannelID member function: '" + m_nick + "'" );
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountFTIDs::CountFTIDs( const unsigned int imin, const std::string& nick, const unsigned int imax )
    : LoKi::AuxFunBase( std::tie( imin, nick, imax ) ), m_imin( imin ), m_imax( imax ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::FTChannelID member function: '" + m_nick + "'" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountFTIDs::setPmf( const std::string& nick ) {
  auto i = std::find_if( s_countFTIDs_tbl.begin(), s_countFTIDs_tbl.end(),
                         [&]( const auto& p ) { return p.second == nick; } );
  if ( i != s_countFTIDs_tbl.end() )
    m_pmf = i->first;
  else
    Exception( "Invalid LHCb::FTChannelID member function '" + nick + "'" );
}
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountFTIDs* LoKi::Track::CountFTIDs::clone() const { return new LoKi::Track::CountFTIDs( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountFTIDs::result_type LoKi::Track::CountFTIDs::operator()( LoKi::Track::CountFTIDs::argument t ) const {
  //
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return -1" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return -1;
  }
  //
  const auto& lhcbids = t->lhcbIDs();
  //
  size_t res = 0;
  //
  for ( auto id = lhcbids.begin(); lhcbids.end() != id; ++id ) {
    if ( !id->isFT() ) { continue; }
    //
    const unsigned int r = ( id->ftID().*m_pmf )();
    if ( m_uints.empty() ) {
      if ( m_imin <= r && r <= m_imax ) { ++res; }
    } else if ( 1 == m_uints.size() ) {
      if ( m_uints.front() == r ) { ++res; }
    } else {
      if ( m_uints.end() != std::find( m_uints.begin(), m_uints.end(), r ) ) { ++res; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountFTIDs::fillStream( std::ostream& s ) const {
  s << " TrFTIDC( ";
  //
  if ( m_uints.empty() ) {
    s << m_imin << " ,'" << m_nick << "', " << m_imax;
  } else if ( 1 == m_uints.size() ) {
    s << "'" << m_nick << "', " << m_uints.front();
  } else {
    s << "'" << m_nick << "', ";
    Gaudi::Utils::toStream( m_uints, s );
  }
  //
  return s << " ) ";
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( LoKi::Track::CountUTIDs::PMF pmf, const unsigned int i )
    : m_pmf( pmf ), m_uints( 1, i ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::UTChannelID member function!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( LoKi::Track::CountUTIDs::PMF pmf, const std::vector<unsigned int>& i )
    : m_pmf( pmf ), m_uints( i ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::UTChannelID member function!" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( const unsigned int imin, LoKi::Track::CountUTIDs::PMF pmf,
                                     const unsigned int imax )
    : m_pmf( pmf ), m_imin( imin ), m_imax( imax ) {
  setNick( m_pmf );
  Assert( 0 != m_pmf, "Invalid LHCb::UTChannelID member function!" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountUTIDs::setNick( LoKi::Track::CountUTIDs::PMF pmf ) {
  auto i =
      std::find_if( s_countUTIDs_tbl.begin(), s_countUTIDs_tbl.end(), [&]( const auto& p ) { return p.first == pmf; } );
  if ( i != s_countUTIDs_tbl.end() )
    m_nick = i->second;
  else
    Exception( "Invalid LHCb::UTChannelID member function!" );
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( const std::string& nick, const std::vector<unsigned int>& i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::UTChannelID member function: '" + m_nick + "'" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( const std::string& nick, const unsigned int i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_uints( 1, i ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::UTChannelID member function: '" + m_nick + "'" );
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountUTIDs::CountUTIDs( const unsigned int imin, const std::string& nick, const unsigned int imax )
    : LoKi::AuxFunBase( std::tie( imin, nick, imax ) ), m_imin( imin ), m_imax( imax ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf, "Invalid LHCb::FTChannelID member function: '" + m_nick + "'" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountUTIDs::setPmf( const std::string& nick ) {
  auto i = std::find_if( s_countUTIDs_tbl.begin(), s_countUTIDs_tbl.end(),
                         [&]( const auto& p ) { return p.second == nick; } );
  if ( i != s_countUTIDs_tbl.end() )
    m_pmf = i->first;
  else
    Exception( "Invalid LHCb::UTChannelID member function '" + nick + "'" );
}

// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountUTIDs* LoKi::Track::CountUTIDs::clone() const { return new LoKi::Track::CountUTIDs( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountUTIDs::result_type LoKi::Track::CountUTIDs::operator()( LoKi::Track::CountUTIDs::argument t ) const {
  //
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return -1" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return -1;
  }
  //
  const auto& lhcbids = t->lhcbIDs();
  //
  size_t res = 0;
  //
  for ( auto id = lhcbids.begin(); lhcbids.end() != id; ++id ) {
    if ( !id->isUT() ) { continue; }
    //
    const unsigned int r = ( id->utID().*m_pmf )();
    if ( m_uints.empty() ) {
      if ( m_imin <= r && r <= m_imax ) { ++res; }
    } else if ( 1 == m_uints.size() ) {
      if ( m_uints.front() == r ) { ++res; }
    } else {
      if ( m_uints.end() != std::find( m_uints.begin(), m_uints.end(), r ) ) { ++res; }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountUTIDs::fillStream( std::ostream& s ) const {
  s << " TrUTIDC( ";
  //
  if ( m_uints.empty() ) {
    s << m_imin << " ,'" << m_nick << "', " << m_imax;
  } else if ( 1 == m_uints.size() ) {
    s << "'" << m_nick << "', " << m_uints.front();
  } else {
    s << "'" << m_nick << "', ";
    Gaudi::Utils::toStream( m_uints, s );
  }
  //
  return s << " ) ";
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( LoKi::Track::CountVPIDs::PMF2 pmf, const unsigned int i )
    : m_pmf2( pmf ), m_uints( 1, i ) {
  setNick( m_pmf2 );
  Assert( 0 != m_pmf2, "Invalid LHCb::VPChannelID member function!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( LoKi::Track::CountVPIDs::PMF2 pmf, const std::vector<unsigned int>& i )
    : m_pmf2( pmf ), m_uints( i ) {
  setNick( m_pmf2 );
  Assert( 0 != m_pmf2, "Invalid LHCb::VPChannelID member function!" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( const unsigned int imin, LoKi::Track::CountVPIDs::PMF2 pmf,
                                     const unsigned int imax )
    : m_pmf2( pmf ), m_imin( imin ), m_imax( imax ) {
  setNick( m_pmf2 );
  Assert( 0 != m_pmf2, "Invalid LHCb::FTChannelID member function!" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set nick
// ============================================================================
void LoKi::Track::CountVPIDs::setNick( LoKi::Track::CountVPIDs::PMF2 pmf ) {
  auto i = std::find_if( s_countVPIDs_tbl2.begin(), s_countVPIDs_tbl2.end(),
                         [&]( const auto& p ) { return p.first == pmf; } );
  if ( i != s_countVPIDs_tbl2.end() )
    m_nick = i->second;
  else
    Exception( "Invalid LHCb::VPChannelID member function!" );
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( const std::string& nick, const std::vector<unsigned int>& i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_uints( i ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf2, "Invalid LHCb::VPChannelID member function: '" + m_nick + "'" );
  Assert( !m_uints.empty(), "Empty vector of values is specified!" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( const std::string& nick, const unsigned int i )
    : LoKi::AuxFunBase( std::tie( nick, i ) ), m_uints( 1, i ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf2, "Invalid LHCb::VPChannelID member function: '" + m_nick + "'" );
}

// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( const unsigned int imin, const std::string& nick, const unsigned int imax )
    : LoKi::AuxFunBase( std::tie( imin, nick, imax ) ), m_imin( imin ), m_imax( imax ), m_nick( nick ) {
  setPmf( m_nick );
  Assert( 0 != m_pmf2, "Invalid LHCb::VPChannelID member function: '" + m_nick + "'" );
  Assert( imin <= imax, "Invalid range of values" );
}
// ============================================================================
// set PMF properly
// ============================================================================
void LoKi::Track::CountVPIDs::setPmf( const std::string& nick ) {
  auto i = std::find_if( s_countVPIDs_tbl2.begin(), s_countVPIDs_tbl2.end(),
                         [&]( const auto& p ) { return p.second == nick; } );
  if ( i != s_countVPIDs_tbl2.end() )
    m_pmf2 = i->first;
  else
    Exception( "Invalid LHCb::VPChannelID member function '" + nick + "'" );
}

// ============================================================================
// constructor
// ============================================================================
/* PK-R3C is this replaced by something ?
LoKi::Track::CountVPIDs::CountVPIDs( LoKi::Track::CountVPIDs::PMF1 pmf ) : m_pmf1( pmf ) {
  auto i = std::find_if( s_countVPIDs_tbl1.begin(), s_countVPIDs_tbl1.end(),
                         [&]( const auto& p ) { return p.first == m_pmf1; } );
  if ( i != s_countVPIDs_tbl1.end() )
    m_nick = i->second;
  else
    Exception( "Invalid LHCb::VPChannelID member function" );
  Assert( 0 != m_pmf1, "Invalid LHCb::VPChannelID member function:" );
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Track::CountVPIDs::CountVPIDs( const std::string& nick )
    : LoKi::AuxFunBase( std::tie( nick ) ), m_nick( nick ) {
  auto i = std::find_if( s_countVPIDs_tbl3.begin(), s_countVPIDs_tbl3.end(),
                         [&]( const auto& p ) { return p.first == m_nick; } );
  if ( i != s_countVPIDs_tbl3.end() ) m_pmf1 = i->second;
  Assert( 0 != m_pmf1, "Invalid LHCb::VPChannelID member function '" + m_nick + "'" );
}
*/
// ============================================================================
// MANDATORY: clone method ('virtual constructor')
// ============================================================================
LoKi::Track::CountVPIDs* LoKi::Track::CountVPIDs::clone() const { return new LoKi::Track::CountVPIDs( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::CountVPIDs::result_type LoKi::Track::CountVPIDs::operator()( LoKi::Track::CountVPIDs::argument t ) const {
  //
  //
  if ( UNLIKELY( !t ) ) {
    Error( "LHCb::Track* points to NULL, return -1" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return -1;
  }
  //
  const auto& lhcbids = t->lhcbIDs();
  //
  size_t res = 0;
  //
  for ( auto id = lhcbids.begin(); lhcbids.end() != id; ++id ) {
    if ( !id->isVP() ) { continue; }
    //
    const LHCb::VPChannelID velo = id->vpID();
    //
    if ( m_pmf1 ) {
      if ( ( velo.*m_pmf1 )() ) { ++res; }
    } else {
      const unsigned int r = ( velo.*m_pmf2 )();
      if ( m_uints.empty() ) {
        if ( m_imin <= r && r <= m_imax ) { ++res; }
      } else if ( 1 == m_uints.size() ) {
        if ( m_uints.front() == r ) { ++res; }
      } else {
        if ( m_uints.end() != std::find( m_uints.begin(), m_uints.end(), r ) ) { ++res; }
      }
    }
  }
  //
  return res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Track::CountVPIDs::fillStream( std::ostream& s ) const {
  s << " TrVELOIDC( ";
  //
  if ( m_pmf1 ) { return s << "'" << m_nick << "' ) "; }
  //
  if ( m_uints.empty() ) {
    s << m_imin << " ,'" << m_nick << "', " << m_imax;
  } else if ( 1 == m_uints.size() ) {
    s << "'" << m_nick << "', " << m_uints.front();
  } else {
    s << "'" << m_nick << "', ";
    Gaudi::Utils::toStream( m_uints, s );
  }
  //
  return s << " ) ";
}

// ============================================================================
// The END
// ============================================================================
