/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LHCb
// ============================================================================
#include "Kernel/HitPattern.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/VeloHitPatternFunctions.h"
// ============================================================================
/** @file
 *  Implementation file for classed form the file
 *  LoKi/VeloHitPatternFunctions.h
 *
 *  @author Wouter Hulsbergen
 *  @author Pieter David
 *  @date 2012-03-12
 */
// ============================================================================

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::MaxNumConsecutiveVeloSpacePoints::result_type LoKi::Track::MaxNumConsecutiveVeloSpacePoints::
                                                           operator()( LoKi::Track::MaxNumConsecutiveVeloSpacePoints::argument t ) const {
  LHCb::HitPattern p( t->lhcbIDs() );
  // get the R and Phi hit patterns
  std::bitset<LHCb::HitPattern::Number::NumVelo> veloR = p.veloRA() | p.veloRC();
  std::bitset<LHCb::HitPattern::Number::NumVelo> veloPhi = p.veloPhiA() | p.veloPhiC();
  // make a logical 'and' to get the number of stations with a cluster
  std::bitset<LHCb::HitPattern::Number::NumVelo> veloclusters = veloR & veloPhi;
  // now count the number of consecutive stations with a cluster
  int numconsecutive( 0 );
  int maxnumconsecutive( 0 );
  for ( int i = 0; i < LHCb::HitPattern::Number::NumVelo; ++i ) {
    if ( veloclusters.test( i ) ) {
      ++numconsecutive;
      if ( maxnumconsecutive < numconsecutive ) maxnumconsecutive = numconsecutive;
    } else {
      numconsecutive = 0;
    }
  }
  return maxnumconsecutive;
}

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::NumVeloSpacePoints::result_type LoKi::Track::NumVeloSpacePoints::
                                             operator()( LoKi::Track::NumVeloSpacePoints::argument t ) const {
  LHCb::HitPattern p( t->lhcbIDs() );
  return ( ( p.veloRA() | p.veloRC() ) & ( p.veloPhiA() | p.veloPhiC() ) ).count();
}

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::NumVeloACOverlapHits::result_type LoKi::Track::NumVeloACOverlapHits::
                                               operator()( LoKi::Track::NumVeloACOverlapHits::argument t ) const {
  LHCb::HitPattern p( t->lhcbIDs() );
  return p.numVeloStationsOverlap();
}

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::NumVeloACOverlapHitsR::result_type LoKi::Track::NumVeloACOverlapHitsR::
                                                operator()( LoKi::Track::NumVeloACOverlapHitsR::argument t ) const {
  LHCb::HitPattern p( t->lhcbIDs() );
  return ( p.veloRA() & p.veloRC() ).count();
}

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Track::NumVeloACOverlapHitsPhi::result_type LoKi::Track::NumVeloACOverlapHitsPhi::
                                                  operator()( LoKi::Track::NumVeloACOverlapHitsPhi::argument t ) const {
  LHCb::HitPattern p( t->lhcbIDs() );
  return ( p.veloPhiA() & p.veloPhiC() ).count();
}

// ============================================================================
// The END
// ============================================================================
