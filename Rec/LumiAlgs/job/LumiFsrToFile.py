#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Syntax is:
#   gaudirun.py ../job/LumiFsrToFile.py
#
import os
from Gaudi.Configuration import *

#--- switch on xml summary
from Configurables import LHCbApp
LHCbApp().XMLSummary = None
LHCbApp().DataType = "2012"

#--- determine application to run
from LumiAlgs.LumiFsrReaderConf import LumiFsrReaderConf as LumiFsrReader

#-- File catalogs. First one is read-write
FileCatalog().Catalogs = [
    "xmlcatalog_file:MyCatalog.xml",
]

files = [
    "DATAFILE='PFN:testFSRout.dst' TYP='POOL_ROOTTREE' ",
]

LumiFsrReader().inputFiles = files
LumiFsrReader().EvtMax = -1
LumiFsrReader().OutputLevel = WARNING
LumiFsrReader().asciiFile = 'test_ascii_fsr.dat'

EventSelector().PrintFreq = 1000
