/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIINTEGRATOR_H
#define LUMIINTEGRATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// event model
#include "Event/LumiFSR.h"
#include "Event/LumiIntegral.h"
#include "Event/TimeSpanFSR.h"

#include "LumiAlgs/ILumiIntegrator.h" // Interface

/** @class LumiIntegrator LumiIntegrator.h
 *
 *
 *  @author Jaap Panman
 *  @date   2009-03-16
 */
class LumiIntegrator : public extends<GaudiTool, ILumiIntegrator> {
public:
  /// Standard constructor
  using base_class::base_class;

  // Integrate Lumi FSR data
  StatusCode integrate( LHCb::LumiFSR& fsr ) override;

  // Integrate Lumi FSR data
  StatusCode integrate( LHCb::LumiFSR* fsr ) override;

  // Integrate Lumi FSR data
  StatusCode integrate( LHCb::LumiIntegral& fsr ) override;

  // Integrate Lumi FSR data
  StatusCode integrate( LHCb::LumiIntegral* fsr ) override;

  // Integrate Lumi FSR data with mask sum one scalar
  StatusCode integrate( LHCb::LumiIntegral& fsr, std::vector<double> v, double f = 1.0 ) override;

  // Integrate Lumi FSR data with mask sum one scalar
  StatusCode integrate( LHCb::LumiIntegral* fsr, std::vector<double> v, double f = 1.0 ) override;

  // Accumulate mu from Lumi FSR data with mask
  StatusCode accumulate_mu( LHCb::LumiIntegral& fsr, LHCb::TimeSpanFSR* timeSpanFSR, int mukey, std::vector<double> v,
                            double f = 1.0 ) override;

  // Accumulate mu from Lumi FSR data with mask
  StatusCode accumulate_mu( LHCb::LumiIntegral* fsr, LHCb::TimeSpanFSR* timeSpanFSR, int mukey, std::vector<double> v,
                            double f = 1.0 ) override;

  // Set absolute scale
  StatusCode setAbsolute( double scale, double relerror ) override;

  // Get absolute scale
  double absolute() override;

  // Get final result
  double lumiValue() override;

  // Get error on result
  double lumiError() override;

  // Get mu calculation items
  ulonglong   muFiles() override;
  ulonglong   muKeys( unsigned long index ) override;
  ulonglong   muRun( unsigned long index ) override;
  std::string muGuid( unsigned long index ) override;
  ulonglong   muTime0( unsigned long index ) override;
  ulonglong   muTime1( unsigned long index ) override;
  double      muDeltaLumi( unsigned long index ) override;
  double      muNorm( unsigned long index ) override;
  double      muMu( unsigned long index ) override;
  int         muCounter( unsigned long index, unsigned long key ) override;
  std::string muCounterName( unsigned long index, unsigned long key ) override;
  double      muCounterNorm( unsigned long index, unsigned long key ) override;
  double      muCounterValue( unsigned long index, unsigned long key ) override;

  // Retrieve the integrated Lumi FSR data
  const LHCb::LumiIntegral& integral() const override;

  // Retrieve duplicates
  std::vector<std::string> duplicates() const override;

  // Count the number of events for a given file
  void countEvents() override;

  // Retrieve the event count for a file
  unsigned long events() const override;

  // Check the event count for a file
  bool checkEvents() const override;

  // Get mu results
  std::vector<ILumiIntegrator::muTuple> muValues() override;

private:
  unsigned long            m_count_events = 0; // number of events
  std::vector<std::string> m_duplicateFiles;   // keep track of duplicates

  double m_integral       = 0.; // the sum
  double m_lumi_scale     = 0.; // absolute scale
  double m_lumi_rel_error = 0.; // absolute scale error

  LHCb::LumiIntegral m_LumiSum; // overall sum of FSRs

  std::vector<ILumiIntegrator::muTuple> m_muTuple; // result of mu calculation
};
#endif // LUMIINTEGRATOR_H
