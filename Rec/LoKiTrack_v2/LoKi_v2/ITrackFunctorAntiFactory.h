/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/StatusCode.h"
#include "LoKi_v2/TrackTypes.h"
#include <string>
// ============================================================================
namespace LoKi::Pr {
  // ==========================================================================
  /** @class ITrackFunctorAntiFactory LoKi_v2/ITrackfunctorAntiFactory
   *
   *  Helper interface for implementation of C++/Python "Hybrid" solution
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV, Sascha Stahl
   */
  struct ITrackFunctorAntiFactory : virtual IAlgTool {
    // ========================================================================
    /// InterfaceID
    DeclareInterfaceID( ITrackFunctorAntiFactory, 1, 0 );
    // ========================================================================
    /// set the C++ predicate for LHCb::Track
    virtual void set( const LoKi::Pr::Types::TrCuts& cut ) = 0;
    /// set the C++ function for LHCb::Track
    virtual void set( const LoKi::Pr::Types::TrFunc& fun ) = 0;
  };
  // ==========================================================================
} // namespace LoKi::Pr
