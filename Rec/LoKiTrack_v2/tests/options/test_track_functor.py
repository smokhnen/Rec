###############################################################################
# (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file Small script to test instantiation of functors for LHCb::Event::v2::Track class
#  @author Sascha Stahl
##
# =============================================================================

from Configurables import ApplicationMgr, LHCbApp
app = LHCbApp(DataType="Upgrade", Simulation=True)

from Configurables import Pr__SelectTracks
decode_functors = Pr__SelectTracks("DecodeFunctors")
decode_functors.Code = "(TrP>0.0) & (TrPT>0.0) & ~TrBACKWARD & (TrMINIP()>0.1) & (TrMINIPCUT(0.1)) & ~TrINVALID & (TrCHI2PDOF<3)"\
                       "&  (TrMINIPCHI2()>5) & TrMINIPCHI2CUT(5)"

app.EvtMax = 0
ApplicationMgr().TopAlg += [decode_functors]
