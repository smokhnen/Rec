###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (CondDB, LHCbApp, ExtrapolatorTester, ApplicationMgr,
                           TrackRungeKuttaExtrapolator, TrackKiselExtrapolator,
                           TrackHerabExtrapolator)
from PRConfig import TestFileDB

app = LHCbApp()
app.EvtMax = 0

CondDB().Upgrade = True

ex = ExtrapolatorTester()
ex.Extrapolators = []
add = lambda x: ex.Extrapolators.append(ex.addTool(x))
add(TrackRungeKuttaExtrapolator("Reference"))
add(
    TrackRungeKuttaExtrapolator(
        "BogackiShampine3", RKScheme="BogackiShampine3"))
add(TrackRungeKuttaExtrapolator("Verner7", RKScheme="Verner7"))
add(TrackRungeKuttaExtrapolator("Verner9", RKScheme="Verner9"))
add(
    TrackRungeKuttaExtrapolator(
        "Tsitouras5", RKScheme="Tsitouras5", OutputLevel=1))
add(TrackKiselExtrapolator("Kisel"))
add(TrackHerabExtrapolator("Herab"))

ApplicationMgr(TopAlg=[ex])

TestFileDB.test_file_db['MiniBrunel_2018_MinBias_FTv4_DIGI'].run(
    configurable=app)
