/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/IDetectorElement.h"
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/Material.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "MaterialLocatorBase.h"

/** @class DetailedMaterialLocator DetailedMaterialLocator.h
 *
 * Implementation of a IMaterialLocator that uses the TransportSvc for
 * finding materials on a trajectory.
 *
 *  @author Wouter Hulsbergen
 *  @date   21/05/2007
 */

class DetailedMaterialLocator : public MaterialLocatorBase {
public:
  /// Constructor
  using MaterialLocatorBase::MaterialLocatorBase;

  /// intialize
  StatusCode initialize() override;

  using MaterialLocatorBase::intersect;
  using MaterialLocatorBase::intersect_r;

  /// Intersect a line with volumes in the geometry
  size_t intersect( const Gaudi::XYZPoint& p, const Gaudi::XYZVector& v,
                    ILVolume::Intersections& intersepts ) const override {
    return intersect_r( p, v, intersepts, m_accelCache );
  }

  /// Intersect a line with volumes in the geometry
  size_t intersect_r( const Gaudi::XYZPoint& p, const Gaudi::XYZVector& v, ILVolume::Intersections& intersepts,
                      std::any& accelCache ) const override;

private:
  Gaudi::Property<double>      m_minRadThickness{this, "MinRadThickness", 1e-4};       ///< minimum radiation thickness
  Gaudi::Property<std::string> m_geometrypath{this, "Geometry", "/dd/Structure/LHCb"}; ///< name of the geometry
  IGeometryInfo*               m_geometry = nullptr;
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_outside{
      this, "No transport possible since destination is outside LHCb "};
};

DECLARE_COMPONENT( DetailedMaterialLocator )

StatusCode DetailedMaterialLocator::initialize() {
  return MaterialLocatorBase::initialize().andThen( [&] {
    // if it is zero, the transport services uses the 'standard' geometry
    auto mainvolume = !m_geometrypath.empty() ? getDet<IDetectorElement>( m_geometrypath ) : nullptr;
    m_geometry      = mainvolume ? mainvolume->geometry() : nullptr;
  } );
}

namespace {
  inline const std::string chronotag = "DetailedMaterialLocator";
}

size_t DetailedMaterialLocator::intersect_r( const Gaudi::XYZPoint& start, const Gaudi::XYZVector& vect,
                                             ILVolume::Intersections& intersepts, std::any& accelCache ) const {
  // check if transport is within LHCb
  constexpr double m_25m = 25 * Gaudi::Units::m;
  size_t           rc    = 0;
  intersepts.clear();

  if ( std::abs( start.x() ) > m_25m || std::abs( start.y() ) > m_25m || std::abs( start.z() ) > m_25m ||
       std::abs( start.x() + vect.x() ) > m_25m || std::abs( start.y() + vect.y() ) > m_25m ||
       std::abs( start.z() + vect.z() ) > m_25m ) {
    ++m_outside;
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "No transport between z= " << start.z() << " and " << start.z() + vect.z()
              << ", since it reaches outside LHCb"
              << "start = " << start << " vect= " << vect << endmsg;
  } else {
    try {
      chronoSvc()->chronoStart( chronotag );
      const double mintick = 0;
      const double maxtick = 1;
      rc = m_tSvc->intersections_r( start, vect, mintick, maxtick, intersepts, accelCache, m_minRadThickness,
                                    m_geometry );
      chronoSvc()->chronoStop( chronotag );
    } catch ( const GaudiException& exception ) {
      error() << "caught transportservice exception " << exception << '\n'
              << "propagating pos/vec: " << start << " / " << vect << endmsg;
      throw exception;
    }
  }
  return rc;
}
