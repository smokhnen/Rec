/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// -------------
// from Gaudi

#include "TrackFieldExtrapolatorBase.h"

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackFieldExtrapolatorBase::initialize() {
  StatusCode sc = TrackExtrapolator::initialize().andThen( [&] { return m_fieldSvc.retrieve(); } );

  if ( sc.isSuccess() ) {
    m_fieldGrid = m_fieldSvc->fieldGrid();

    if ( !m_fieldGrid )
      sc = Error( "Cannot retrieve field grid from magfieldvsc", StatusCode::FAILURE );
    else {
      m_fieldFunction = m_useGridInterpolation ? &LHCb::MagneticFieldGrid::fieldVectorLinearInterpolation
                                               : &LHCb::MagneticFieldGrid::fieldVectorClosestPoint;

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << "UseGridInterpolation: " << m_useGridInterpolation << endmsg;
        debug() << "Field in center of magnet (in Tesla): "
                << fieldVector( Gaudi::XYZPoint( 0, 0, 5000 ) ) / Gaudi::Units::tesla << endmsg;
      }
    }
  }

  return sc;
}
