/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ILVolume.h"
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/Material.h"
#include "Event/TrackParameters.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackExtrapolator.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "TrackInterfaces/ITrackExtraSelector.h"
#include "TrackKernel/CubicStateInterpolationTraj.h"

using namespace Gaudi;
using namespace Gaudi::Units;
using namespace LHCb;

/** @class TrackMasterExtrapolator TrackMasterExtrapolator.h \
 *         "TrackMasterExtrapolator.h"
 *
 *  A TrackMasterExtrapolator is a ITrackExtrapolator
 *  which calls the other extrapolators to do the extrapolating.
 *  It takes into account:
 *  @li Detector Material (multiple scattering , energy loss)
 *  @li Deals with electrons
 *  @li Checks the input state vector
 *  @li The actual extrapolation is chosen by the extrapolator selector \
 *       m_extraSelector
 *
 *  @author Edwin Bos (added extrapolation methods)
 *  @date   05/07/2005
 *  @author Jose A. Hernando
 *  @date   15-03-05
 *  @author Matt Needham
 *  @date   22-04-2000
 */

class TrackMasterExtrapolator : public TrackExtrapolator {

public:
  using TrackExtrapolator::TrackExtrapolator;

  using TrackExtrapolator::propagate;

  /// Propagate a state vector from zOld to zNew
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( Gaudi::TrackVector& stateVec, double zOld, double zNew, Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

  /// Propagate a state to a given z-position
  /// Transport matrix is calulated when transMat pointer is not NULL
  StatusCode propagate( LHCb::State& state, double z, Gaudi::TrackMatrix* transMat,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() ) const override;

private:
  /// extra selector
  ToolHandle<ITrackExtraSelector> m_extraSelector{this, "ExtraSelector", "TrackDistanceExtraSelector"};

  /// transport service
  ToolHandle<IMaterialLocator> m_materialLocator{this, "MaterialLocator", "DetailedMaterialLocator"};

  Gaudi::Property<bool>   m_applyMultScattCorr{this, "ApplyMultScattCorr",
                                             true}; ///< turn on/off multiple scattering correction
  Gaudi::Property<bool>   m_applyEnergyLossCorr{this, "ApplyEnergyLossCorr", true}; ///< turn on/off dE/dx correction
  Gaudi::Property<double> m_maxStepSize{this, "MaxStepSize", 1000. * Gaudi::Units::mm}; ///< maximum length of a step
  Gaudi::Property<double> m_maxSlope{this, "MaxSlope", 5.}; ///< maximum slope of state vector
  Gaudi::Property<double> m_maxTransverse{this, "MaxTransverse",
                                          10. * Gaudi::Units::m}; ///< maximum x,y position of state vector

  /// turn on/off electron energy loss corrections
  Gaudi::Property<bool> m_applyElectronEnergyLossCorr{this, "ApplyElectronEnergyLossCorr", true};
  // Gaudi::Property<double> m_startElectronCorr{ this, "StartElectronCorr", 2500.*mm };  ///< z start for electron
  // energy loss Gaudi::Property<double> m_stopElectronCorr { this, "StopElectronCorr",  9000.*mm };  ///< z start for
  // electron energy loss
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_absurd{
      this, "Protect against absurd tracks. See debug for details", 1};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_loop{
      this, "Protect against looping tracks. See debug for details", 1};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_extrapFail{this, "Transport to wall FAILED", 1};
};

DECLARE_COMPONENT( TrackMasterExtrapolator )

//=========================================================================
// Propagate a state vector from zOld to zNew
// Transport matrix is calulated when transMat pointer is not NULL
// Note: energy loss correction is _NOT_ applied.
//=========================================================================
StatusCode TrackMasterExtrapolator::propagate( Gaudi::TrackVector& stateVec, double zOld, double zNew,
                                               Gaudi::TrackMatrix* transMat, const LHCb::Tr::PID pid ) const {
  // Gaudi::Property<double> m_shortDist { this,  "shortDist", 100.0*Gaudi::Units::mm };
  // return std::abs(zEnd-zStart) < m_shortDist ?
  //        m_shortDistanceExtrapolator : m_longDistanceExtrapolator;
  const ITrackExtrapolator* thisExtrapolator = m_extraSelector->select( zOld, zNew );
  return thisExtrapolator->propagate( stateVec, zOld, zNew, transMat, pid );
}

//=========================================================================
//  Main method: Extrapolate a State
//=========================================================================
StatusCode TrackMasterExtrapolator::propagate( LHCb::State& state, double zNew, Gaudi::TrackMatrix* transMat,
                                               const LHCb::Tr::PID pid ) const {

  // Create transport update matrix. The reason to make a pointer to a
  // local object (rather than just create it with new) is all the
  // intermediate returns.
  TrackMatrix  updateMatrix = ROOT::Math::SMatrixIdentity();
  TrackMatrix* upMat        = nullptr;
  if ( transMat ) {
    *transMat = ROOT::Math::SMatrixIdentity();
    upMat     = &updateMatrix;
  }

  StatusCode sc( StatusCode::SUCCESS, true );
  // check if not already at required z position
  const double zStart = state.z();
  if ( std::abs( zNew - zStart ) < TrackParameters::propagationTolerance ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "already at required z position" << endmsg;
    return sc;
  }

  int    nbStep = (int)( std::abs( zNew - zStart ) / m_maxStepSize ) + 1;
  double zStep  = ( zNew - zStart ) / nbStep;
  size_t nWallsTot( 0 );

  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "state_in = " << state << "\nz_out = " << zNew << "num steps = " << nbStep << endmsg;

  for ( int step = 0; nbStep > step; ++step ) {
    TrackVector& tX = state.stateVector();
    XYZPoint     start( tX[0], tX[1], state.z() ); // Initial position
    XYZVector    vect( tX[2] * zStep, tX[3] * zStep, zStep );

    // protect against vertical or looping tracks
    if ( std::abs( start.x() ) > m_maxTransverse ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Protect against absurd tracks: x=" << start.x() << " (max " << m_maxTransverse << " allowed)."
                << endmsg;
      ++m_absurd;
      return StatusCode::FAILURE;
    }
    if ( std::abs( start.y() ) > m_maxTransverse ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Protect against absurd tracks: y=" << start.y() << " (max " << m_maxTransverse << " allowed)."
                << endmsg;
      ++m_absurd;
      return StatusCode::FAILURE;
    }
    if ( std::abs( state.tx() ) > m_maxSlope ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Protect against looping tracks: tx=" << state.tx() << " (max " << m_maxSlope << " allowed)."
                << endmsg;
      ++m_loop;
      return StatusCode::FAILURE;
    }
    if ( std::abs( state.ty() ) > m_maxSlope ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Protect against looping tracks: ty=" << state.ty() << " (max " << m_maxSlope << " allowed). "
                << endmsg;
      ++m_loop;
      return StatusCode::FAILURE;
    }

    // propagate the state, without any material corrections:
    double zorigin = state.z();
    double ztarget = zorigin + zStep;

    LHCb::State               stateAtOrigin( state );
    const ITrackExtrapolator* thisExtrapolator = m_extraSelector->select( zorigin, ztarget );
    sc                                         = thisExtrapolator->propagate( state, ztarget, upMat );

    // check for success
    if ( sc.isFailure() ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Transport to " << ztarget << "using " + thisExtrapolator->name() << " FAILED" << endmsg;
      return sc;
    }

    // update f
    if ( transMat ) {
      TrackMatrix tempMatrix = *transMat;
      *transMat              = updateMatrix * tempMatrix;
    }

    // now apply material corrections
    if ( m_applyMultScattCorr || m_applyEnergyLossCorr || m_applyElectronEnergyLossCorr ) {
      LHCb::CubicStateInterpolationTraj traj( stateAtOrigin, state );
      IMaterialLocator::Intersections   intersections;
      if ( m_materialLocator->intersect( traj, intersections ) > 0 ) {
        nWallsTot += intersections.size();
        m_materialLocator->applyMaterialCorrections( state, intersections, zorigin, pid, m_applyMultScattCorr,
                                                     m_applyEnergyLossCorr || m_applyElectronEnergyLossCorr );
      }
    }
  } // loop over steps

  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "state_out = " << state << std::endl << "number of walls = " << nWallsTot << endmsg;

  return sc;
}

//=============================================================================
