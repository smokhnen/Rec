/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKFITEVENT_FITNODE_H
#define TRACKFITEVENT_FITNODE_H 1

// from TrackEvent
#include "Event/ChiSquare.h"
#include "Event/Measurement.h"
#include "Event/State.h"
#include "LHCbMath/ValueWithError.h"

#include "GaudiKernel/SerializeSTL.h"
// From LHCbMath
#include "LHCbMath/MatrixManip.h"

namespace LHCb {
  class KalmanFitResult;
}

namespace LHCb {

  /** @class FitNode FitNode.h Event/FitNode.h
   *
   *  This File contains the declaration of the FitNode.
   *
   *  A FitNode is a basket of objects at a given z position.
   *  The information inside the FitNode has to be sufficient
   *  to allow smoothing and refitting.
   *
   *  At the moment a FitNode contains or allows you to access
   *  info on the the (kth) measurement,
   *  transport from k --> k + 1 , predicted state at k+1
   *  (predicted from filter step)  and the best state at k
   *  (notation note filter procedes from k -> k + 1 -> k + 2 ......)
   *
   *  @author Eduardo Rodrigues
   *  @date   2005-04-15 (adaptations to the new track event model)
   *
   *  @author Matthew Needham
   *  @date   19-08-1999
   */

  namespace Enum::FitNode {
    meta_enum_class( Type, unsigned char, Unknown, HitOnTrack, Outlier, Reference )
  }

  class FitNode final {
  public:
    /// enumerator for the type of Node
    using Type = Enum::FitNode::Type;

    // important note: for the Forward fit, smoothed means
    // 'classical'. for the backward fit, it means 'bidirectional'.
    enum FilterStatus { Uninitialized, Initialized, Predicted, Filtered, Smoothed };
    enum CachedBool { False = 0, True = 1, Unknown = 2 };
    enum Direction { Forward = 0, Backward = 1 };

    /// Default constructor
    FitNode();

    /// Constructor from a z-position
    FitNode( double z );

    /// Constructor from a z position and a location
    FitNode( double zPos, LHCb::State::Location location = LHCb::State::Location::LocationUnknown );

    /// Constructor from a Measurement
    FitNode( const Measurement& meas );

    /// Destructor
    ~FitNode();

    /// Retrieve the local chi^2
    double chi2() const;

    /// retrieve the state, overloading the inline function in Node
    const State& state() const;

    /// retrieve the residual, overloading the function in Node
    double residual() const;

    /// retrieve the residual, overloading the function in Node
    double errResidual() const;

    /// Update the state vector
    void setState( const Gaudi::TrackVector& stateVector, const Gaudi::TrackSymMatrix& stateCovariance,
                   const double& z );

    /// Update the reference vector
    void setRefVector( const Gaudi::TrackVector& refVector );

    /// Update the reference vector
    void setRefVector( const LHCb::StateVector& refVector );

    /// Retrieve unbiased residual
    double unbiasedResidual() const;

    /// Retrieve error on unbiased residual
    double errUnbiasedResidual() const;

    /// Retrieve const  the reference to the measurement
    const LHCb::Measurement& measurement() const { return *m_measurement; }

    /// Set the measurement
    void setMeasurement( const LHCb::Measurement& meas ) { m_measurement = &meas; }

    /// Return the error on the residual squared
    double errResidual2() const;

    /// Return the measure error squared
    double errMeasure2() const { return m_errMeasure * m_errMeasure; }

    /// Return true if this Node has a valid pointer to measurement
    bool hasMeasurement() const { return m_measurement != nullptr; }

    /// Retrieve the unbiased smoothed state at this position
    State unbiasedState() const;

    /// z position of Node
    double z() const { return m_refVector.z(); }

    /// Position of the State on the Node
    Gaudi::XYZPoint position() const { return m_state.position(); }

    /// Set the location of the state in this node.
    void setLocation( LHCb::State::Location& location ) { m_state.setLocation( location ); }

    /// Location of the state in this node
    auto location() const { return m_state.location(); }

    /// Retrieve const  type of node
    const Type& type() const { return m_type; }

    /// Update  type of node
    void setType( const Type& value ) { m_type = value; }

    /// Update  state
    void setState( const LHCb::State& value ) { m_state = value; }

    /// Retrieve const  the reference vector
    const LHCb::StateVector& refVector() const { return m_refVector; }

    /// Retrieve const  flag for the reference vector
    bool refIsSet() const { return m_refIsSet; }

    /// Update  the residual value
    void setResidual( double value ) { m_residual = value; }

    /// Update  the residual error
    void setErrResidual( double value ) { m_errResidual = value; }

    /// Retrieve const  the measure error
    double errMeasure() const { return m_errMeasure; }

    /// Update  the measure error
    void setErrMeasure( double value ) { m_errMeasure = value; }

    /// Retrieve const  the projection matrix
    const Gaudi::TrackProjectionMatrix& projectionMatrix() const { return m_projectionMatrix; }

    /// Update  the projection matrix
    void setProjectionMatrix( const Gaudi::TrackProjectionMatrix& value ) { m_projectionMatrix = value; }

    /// Retrieve const  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    double doca() const { return m_doca; }

    /// Update  Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    void setDoca( double value ) { m_doca = value; }

    /// Retrieve const  Unit vector perpendicular to state and measurement
    const Gaudi::XYZVector& pocaVector() const { return m_pocaVector; }

    /// Update  Unit vector perpendicular to state and measurement
    void setPocaVector( const Gaudi::XYZVector& value ) { m_pocaVector = value; }

    /// retrieve transport matrix
    const Gaudi::TrackMatrix& transportMatrix() const { return m_transportMatrix; }

    /// retrieve invert transport matrix
    const Gaudi::TrackMatrix& invertTransportMatrix() const { return m_invertTransportMatrix; }

    /// retrieve transport vector
    const Gaudi::TrackVector& transportVector() const { return m_transportVector; }

    /// retrieve noise matrix
    const Gaudi::TrackSymMatrix& noiseMatrix() const { return m_noiseMatrix; }

    /// retrieve noise matrix
    Gaudi::TrackSymMatrix& noiseMatrix() { return m_noiseMatrix; }

    /// set transport matrix
    void setTransportMatrix( const Gaudi::TrackMatrix& transportMatrix );

    /// set transport vector
    void setTransportVector( const Gaudi::TrackVector& transportVector ) { m_transportVector = transportVector; }

    /// set noise matrix
    void setNoiseMatrix( const Gaudi::TrackSymMatrix& noiseMatrix ) { m_noiseMatrix = noiseMatrix; }

    /// set the seed matrix
    void setSeedCovariance( const Gaudi::TrackSymMatrix& cov ) {
      m_predictedState[Forward].covariance()  = cov;
      m_predictedState[Backward].covariance() = cov;
    }

    /// update the projection
    void updateProjection( const Gaudi::TrackProjectionMatrix& H, double refresidual, double errmeasure ) {
      setProjectionMatrix( H );
      setRefResidual( refresidual );
      setErrMeasure( errmeasure );
      setResidual( 0 );
      setErrResidual( 0 );
      resetFilterStatus( Predicted );
    }

    /// retrieve state predicted by the kalman filter step
    const State& predictedStateForward() const { return predictedState( Forward ); }

    /// retrieve predicted state from backward filter
    const State& predictedStateBackward() const { return predictedState( Backward ); }

    /// retrieve state filtered by the kalman filter step
    const State& filteredStateForward() const { return filteredState( Forward ); }

    /// retrieve state filtered by the kalman filter step
    const State& filteredStateBackward() const { return filteredState( Backward ); }

    /// retrieve chisq contribution in upstream filter
    const LHCb::ChiSquare& deltaChi2Forward() const {
      filteredStateForward();
      return m_deltaChi2[Forward];
    }

    /// retrieve chisq contribution in downstream filter
    const LHCb::ChiSquare& deltaChi2Backward() const {
      filteredStateBackward();
      return m_deltaChi2[Backward];
    }

    /// retrieve the total chi2 of the filter including this node
    const LHCb::ChiSquare& totalChi2( int direction ) const {
      filteredState( direction );
      return m_totalChi2[direction % 2];
    }

    /// set the residual of the reference
    void setRefResidual( double res ) { m_refResidual = res; }

    /// retrieve the residual of the reference
    double refResidual() const { return m_refResidual; }

    /// set the delta-energy
    void setDeltaEnergy( double e ) { m_deltaEnergy = e; }

    /// get the delta-energy
    double deltaEnergy() const { return m_deltaEnergy; }

    // get the smoother gain matrix
    const Gaudi::TrackMatrix& smootherGainMatrix() const { return m_smootherGainMatrix; }

    /// get the filter status (only useful for debugging)
    FilterStatus filterStatus( int direction ) const { return m_filterStatus[direction]; }

    /// return whether or not this node has active nodes upstream
    bool hasInfoUpstream( int direction ) const;

    /// Deactivate this node (outlier)
    void deactivateMeasurement( bool deactivate = true );

    /// Get the index of this node. For debugging only.
    int index() const;

    /// set previous node
    void setPreviousNode( FitNode* previousNode ) {
      m_prevNode = previousNode;
      if ( m_prevNode ) m_prevNode->m_nextNode = this;
    }

    /// Unlink this node
    void unLink() {
      m_prevNode = m_nextNode = nullptr;
      m_parent                = nullptr;
    }

    /// set the parent
    void setParent( KalmanFitResult* p ) { m_parent = p; }

    /// get the parent
    KalmanFitResult* getParent() { return m_parent; }

    /// update node residual using a smoothed state
    Gaudi::Math::ValueWithError computeResidual( const LHCb::State& state, bool biased ) const;

  private:
    Type                         m_type;                  ///< type of node
    LHCb::State                  m_state;                 ///< state
    LHCb::StateVector            m_refVector;             ///< the reference vector
    bool                         m_refIsSet    = false;   ///< flag for the reference vector
    const LHCb::Measurement*     m_measurement = nullptr; ///< pointer to the measurement (not owner)
    double                       m_residual    = 0;       ///< the residual value
    double                       m_errResidual = 0;       ///< the residual error
    double                       m_errMeasure  = 0;       ///< the measure error
    Gaudi::TrackProjectionMatrix m_projectionMatrix;      ///< the projection matrix
    double           m_doca = 0;   ///< Signed doca (of ref-traj). For ST/velo this is equal to minus (ref)residual
    Gaudi::XYZVector m_pocaVector; ///< Unit vector perpendicular to state and measurement

    // ! check that the contents of the cov matrix are fine
    bool isPositiveDiagonal( const Gaudi::TrackSymMatrix& mat ) const;

  public:
    const FitNode* prevNode( int direction ) const { return direction == Forward ? m_prevNode : m_nextNode; }
    const FitNode* nextNode( int direction ) const { return direction == Forward ? m_nextNode : m_prevNode; }

    /// retrieve the predicted state
    const LHCb::State& predictedState( int direction ) const {
      if ( m_filterStatus[direction] < Predicted ) unConst().computePredictedState( direction );
      return m_predictedState[direction];
    }

    /// retrieve the filtered state
    const LHCb::State& filteredState( int direction ) const {
      if ( m_filterStatus[direction] < Filtered ) unConst().computeFilteredState( direction );
      return m_filteredState[direction];
    }

    /// retrieve the bismoothed state
    const LHCb::State& biSmoothedState() const {
      if ( m_filterStatus[Backward] < Smoothed ) unConst().computeBiSmoothedState();
      return m_state;
    }

    /// retrieve the classically smoothed state
    const LHCb::State& classicalSmoothedState() const {
      if ( m_filterStatus[Forward] < Smoothed ) unConst().computeClassicalSmoothedState();
      return m_classicalSmoothedState;
    }

    /// This is used from the projectors (or from any set method?)
    void resetFilterStatus( FilterStatus s = Initialized ) {
      resetFilterStatus( Forward, s );
      resetFilterStatus( Backward, s );
    }

  private:
    void computePredictedState( int direction );
    void computeFilteredState( int direction );
    void computeBiSmoothedState();
    void computeClassicalSmoothedState();

  private:
    /// update node residual using a smoothed state
    void updateResidual( const LHCb::State& state );

    /// unconst this node
    FitNode& unConst() const { return const_cast<FitNode&>( *this ); }

    /// reset the cache for the previous function
    void resetHasInfoUpstream( int direction );

    /// reset the filter status
    void resetFilterStatus( int direction, FilterStatus s = Initialized );

  private:
    Gaudi::TrackMatrix m_transportMatrix;       ///< transport matrix for propagation from previous node to this one
    Gaudi::TrackMatrix m_invertTransportMatrix; ///< transport matrix for propagation from this node to the previous one
    Gaudi::TrackVector m_transportVector;       ///< transport vector for propagation from previous node to this one
    Gaudi::TrackSymMatrix m_noiseMatrix;        ///< noise in propagation from previous node to this one
    double                m_deltaEnergy = 0;    ///< change in energy in propagation from previous node to this one
    double                m_refResidual = 0;    ///< residual of the reference
    FilterStatus          m_filterStatus[2];    ///< Status of the Node in the fit process
    CachedBool            m_hasInfoUpstream[2]; ///< Are the nodes with active measurement upstream of this node?
    State                 m_predictedState[2];  ///< predicted state of forward/backward filter
    State                 m_filteredState[2];   ///< filtered state of forward/backward filter
    LHCb::State           m_classicalSmoothedState;
    LHCb::ChiSquare       m_deltaChi2[2];       ///< chisq contribution in forward filter
    LHCb::ChiSquare       m_totalChi2[2];       ///< total chi2 after this filterstep
    Gaudi::TrackMatrix    m_smootherGainMatrix; ///< smoother gain matrix (smoothedfit only)
    FitNode*              m_prevNode = nullptr; ///< Previous Node
    FitNode*              m_nextNode = nullptr; ///< Next Node
    KalmanFitResult*      m_parent   = nullptr; ///< Owner

    /**
     * @brief      Helper functions to print FitNodes in a compact form.
     */
    std::ostream& print( std::ostream& s, const Gaudi::TrackVector& v ) const {
      return GaudiUtils::details::ostream_joiner( s, v, " " );
    }

    std::ostream& print( std::ostream& s, const Gaudi::TrackProjectionMatrix& v ) const {
      return GaudiUtils::details::ostream_joiner( s, v, " " );
    }

    std::ostream& print( std::ostream& s, const Gaudi::TrackSymMatrix& v ) const {
      return GaudiUtils::details::ostream_joiner( s, v, " " );
    }

    std::ostream& print( std::ostream& s, const Gaudi::TrackMatrix& v ) const {
      return GaudiUtils::details::ostream_joiner( s, v, " " );
    }

  public:
    /**
     * @brief      Prints a FitNode in a compact form.
     */
    friend std::ostream& operator<<( std::ostream& s, const LHCb::FitNode& node );
  };

} // namespace LHCb

inline LHCb::FitNode::FitNode( double z ) : m_type( Type::Reference ) { m_refVector.setZ( z ); }

inline void LHCb::FitNode::setState( const Gaudi::TrackVector&    stateVector,
                                     const Gaudi::TrackSymMatrix& stateCovariance, const double& z ) {
  m_state.setState( stateVector );
  m_state.setCovariance( stateCovariance );
  m_state.setZ( z );
}

inline void LHCb::FitNode::setRefVector( const Gaudi::TrackVector& refVector ) {
  m_refIsSet               = true;
  m_refVector.parameters() = refVector;
}

inline void LHCb::FitNode::setRefVector( const LHCb::StateVector& refVector ) {
  m_refIsSet  = true;
  m_refVector = refVector;
}

inline double LHCb::FitNode::unbiasedResidual() const {
  double r = residual();
  return type() == Type::HitOnTrack ? ( r * errMeasure2() / ( m_errResidual * m_errResidual ) ) : r;
}

inline double LHCb::FitNode::errUnbiasedResidual() const {
  double errRes = errResidual();
  return type() == Type::HitOnTrack ? ( errMeasure2() / errRes ) : errRes;
}

inline double LHCb::FitNode::errResidual2() const {
  double x = errResidual();
  return x * x;
}

inline LHCb::State LHCb::FitNode::unbiasedState() const {
  // we can redo this routine by smoothing the unfiltered states

  // This performs an inverse kalman filter step.
  // First calculate the gain matrix
  const auto& H       = projectionMatrix();
  const auto& biasedC = m_state.covariance();
  double      r       = residual();
  double      R       = errResidual2();

  ROOT::Math::SMatrix<double, 5, 1> K = ( biasedC * Transpose( H ) ) / R;

  // Forwarddate the state vectors
  Gaudi::TrackVector unbiasedX = m_state.stateVector() - K.Col( 0 ) * r;

  // Forwarddate the covariance matrix
  const Gaudi::TrackSymMatrix unit = ROOT::Math::SMatrixIdentity();
  Gaudi::TrackSymMatrix       unbiasedC;
  ROOT::Math::AssignSym::Evaluate( unbiasedC, ( unit + K * H ) * biasedC );
  return LHCb::State{unbiasedX, unbiasedC, z(), m_state.location()};
}

#endif // TRACKFITEVENT_FITNODE_H
