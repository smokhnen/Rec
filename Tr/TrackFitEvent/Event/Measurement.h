/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/TrackTypes.h"
#include "Kernel/LHCbID.h"
#include "Kernel/LineTraj.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Trajectory.h"
#include "Kernel/meta_enum.h"
#include <variant>

// VP
#include "Event/VPLightCluster.h"
#include "VPDet/DeVPSensor.h"

// UT
#include "Event/UTHit.h"
class DeUTSector;

// FT
#include "Event/FTLiteCluster.h"
class DeFTMat;

// Muon
class DeMuonChamber;

namespace LHCb {
  /** @class Measurement Measurement.h
   *
   * Measurement defines the functionality required for the
   * Kalman Filter, Alignment and Monitoring
   *
   * @author Jose Hernando, Eduardo Rodrigues
   * created Fri Jan  4 20:26:46 2019
   *
   */
  namespace Enum::Measurement {
    /// enumerator for the type of Measurement
    meta_enum_class( Type, unsigned char, Unknown, Unknown2, Unknown3, Unknown4, Unknown5, Unknown6, Unknown7, Unknown8,
                     Muon, Unknown9, Unknown10, VP, Calo, Origin, FT, UT = 19 )
  } // namespace Enum::Measurement

  namespace details::Measurement {
    template <typename... T>
    struct overloaded : T... {
      using T::operator()...;
    };
    template <typename... T>
    overloaded( T... )->overloaded<T...>;

    template <typename Dir>
    bool isX( const Dir& dir ) {
      return dir.x() > dir.y();
    }

    template <typename T, typename Variant, std::size_t... I>
    constexpr int index_helper( std::index_sequence<I...> ) {
      auto b = std::array{std::is_same_v<T, std::variant_alternative_t<I, Variant>>...};
      for ( int i = 0; i != static_cast<int>( size( b ) ); ++i ) {
        if ( b[i] ) return i;
      }
      return -1;
    }

    template <typename T, typename Variant>
    constexpr int index() {
      constexpr auto idx = index_helper<T, Variant>( std::make_index_sequence<std::variant_size_v<Variant>>() );
      static_assert( idx != -1, "T does not appear in Variant" );
      return idx;
    }

  } // namespace details::Measurement

  class Measurement final {
  public:
    struct FT {
      const DeFTMat* mat = nullptr;
    };
    struct Muon {
      const DeMuonChamber* chamber = nullptr;
    };
    struct UT {
      const DeUTSector* sector = nullptr;
    };
    struct VP {
      /// x or y measurement
      enum class Projection { X = 1, Y = 2 };
      const DeVPSensor* sensor = nullptr;
      bool              isX;
      Projection        projection() const {
        return isX ? VP::Projection::Y // measurement is perpendicular to direction
                   : VP::Projection::X;
      }
    };

  private:
    // Note: for now, use a variant. Alternatively, store this information 'elsewhere,
    //       and put a "pointer" (which could be "container + index" on top of SOA storage)
    //       to 'elsewhere' into the variant instead.
    using SubInfo = std::variant<VP, UT, FT, Muon>;

    double                 m_z;          ///< the z-position of the measurement
    double                 m_errMeasure; ///< the measurement error
    LHCbID                 m_lhcbID;     ///< the corresponding LHCbID
    LHCb::LineTraj<double> m_trajectory;
    SubInfo                m_sub; ///< subdetector specific information

    template <typename SubI, typename = std::enable_if_t<std::is_convertible_v<SubI, SubInfo>>>
    Measurement( LHCbID id, double z, double errMeas, LHCb::LineTraj<double> traj, SubI&& subi )
        : m_z{z}
        , m_errMeasure{errMeas}
        , m_lhcbID{id}
        , m_trajectory{std::move( traj )}
        , m_sub{std::forward<SubI>( subi )} {}

  public:
    /// VP constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeVPSensor* elem )
        : Measurement{id, z, errMeas, std::move( traj ), VP{elem, details::Measurement::isX( traj.direction( 0 ) )}} {}

    /// UT constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeUTSector* elem )
        : Measurement{id, z, errMeas, std::move( traj ), UT{elem}} {}

    /// FT constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeFTMat* elem )
        : Measurement{id, z, errMeas, std::move( traj ), FT{elem}} {}

    /// Muon constructor
    Measurement( LHCbID id, double z, LHCb::LineTraj<double> traj, double errMeas, const DeMuonChamber* elem )
        : Measurement{id, z, errMeas, std::move( traj ), Muon{elem}} {}

    // Observers

    /// invoke the callable which can be called for the detector-specific content in this measurement
    template <typename... Callables>
    decltype( auto ) visit( Callables&&... c ) const {
      return std::visit( details::Measurement::overloaded{std::forward<Callables>( c )...}, m_sub );
    }

    /// Get the sub-detector specific information
    template <typename SubI>
    const SubI* getIf() const {
      return std::get_if<SubI>( &m_sub );
    }

    /// Check whether this Measurements 'is-a' specific 'sub' measurement
    template <typename SubI>
    bool is() const {
      return std::holds_alternative<SubI>( m_sub );
    }

    /// Retrieve the measurement error
    double resolution( const Gaudi::XYZPoint& /*point*/, const Gaudi::XYZVector& /*vec*/ ) const {
      return errMeasure();
    }

    /// Retrieve the measurement error squared
    double resolution2( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vec ) const {
      auto r = resolution( point, vec );
      return r * r;
    }

    /// Retrieve the trajectory representing the measurement
    LHCb::LineTraj<double> const& trajectory() const { return m_trajectory; }

    /// Retrieve const  the corresponding LHCbID
    LHCbID lhcbID() const { return m_lhcbID; }

    /// Retrieve const  the z-position of the measurement
    double z() const { return m_z; }

    /// Retrieve const  the measurement error
    double errMeasure() const { return m_errMeasure; }

    /// This set of methods is wrapping DetectorElement ones so that user of Measurements
    /// do not have to see the DetectorElement class and thus can use the DetDesc or the
    /// Detector version the same way. Not really nice as an interface but the long term
    /// goal begin to drop Measurements, this is an easy solution
    Gaudi::XYZPoint  toLocal( const Gaudi::XYZPoint& globalPoint ) const;
    Gaudi::XYZVector toLocal( const Gaudi::XYZVector& globalPoint ) const;
    Gaudi::XYZPoint  toGlobal( const Gaudi::XYZPoint& globalPoint ) const;
    Gaudi::XYZVector toGlobal( const Gaudi::XYZVector& globalPoint ) const;
    std::string      name() const;
    bool             isSameDetectorElement( const Measurement& other ) const;

    using Type = Enum::Measurement::Type;

    template <typename SubI>
    static constexpr auto index = details::Measurement::index<SubI, SubInfo>();

    /// Retrieve the measurement type
    Type type() const {
      auto idx = m_sub.index();
      ASSUME( idx < std::variant_size_v<SubInfo> );
      switch ( idx ) {
      case index<VP>:
        return Type::VP;
      case index<UT>:
        return Type::UT;
      case index<FT>:
        return Type::FT;
      case index<Muon>:
        return Type::Muon;
      default:
        return Type::Unknown;
      }
    };

    /// Modifiers

    template <typename SubI>
    SubI* getIf() {
      return std::get_if<SubI>( &m_sub );
    }

  }; //< class Measurement

} // namespace LHCb
