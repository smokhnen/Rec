/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "TrackAssociator.h"

// from LHCb
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Linker/LinkerWithKey.h"

using namespace LHCb;

DECLARE_COMPONENT( TrackAssociator )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackAssociator::TrackAssociator( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator )
    , m_debugLevel( false )
    , m_fractionOK( 0. )
    , m_nTotVP( 0. )
    , m_nTotUT1( 0. )
    , m_nTotSeed( 0. )
    , m_nTotMuon( 0. )
    , m_muonLink( 0, 0, "" )
    , m_utLink( 0, 0, "" ) {
  declareProperty( "TracksInContainer", m_tracksInContainer = TrackLocation::Default );
  declareProperty( "LinkerOutTable", m_linkerOutTable = "" );
  declareProperty( "FractionOK", m_fractionOK = 0.70 );
  declareProperty( "DecideUsingMuons", m_decideUsingMuons = false );
}

//=============================================================================
// Destructor
//=============================================================================
TrackAssociator::~TrackAssociator() {}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode TrackAssociator::initialize() {

  // Mandatory initialization of GaudiAlgorithm
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return sc; }

  // Set the path for the linker table Track - MCParticle
  if ( m_linkerOutTable == "" ) m_linkerOutTable = m_tracksInContainer;

  m_debugLevel = msgLevel( MSG::DEBUG );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TrackAssociator::execute() {

  // Retrieve the Tracks
  LHCb::Track::Range tracks = getIfExists<LHCb::Track::Range>( m_tracksInContainer );

  // Retrieve the MCParticles
  MCParticles* mcParts = get<MCParticles>( MCParticleLocation::Default );

  // Create the Linker table from Track to MCParticle
  // Linker table is stored in "Link/" + m_linkerOutTable
  // Sorted by decreasing weight, so first retrieved has highest weight
  LinkerWithKey<MCParticle, Track> myLinker( evtSvc(), msgSvc(), m_linkerOutTable );
  myLinker.reset(); //== reset it, in case we work from a DST where associations are already there.

  // Get the linker table VpCluster => MCParticle
  LinkedTo<MCParticle, VPCluster> vpLink( evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default );
  if ( vpLink.notFound() ) {
    error() << "Unable to retrieve VPCluster to MCParticle linker table." << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the linker table TTCluster (or UTCluster) => MCParticle
  m_utLink = UTLink( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );
  if ( m_utLink.notFound() ) {
    error() << "Unable to retrieve UTCluster to MCParticle linker table." << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the linker table FTCluster => MCParticle
  LinkedTo<LHCb::MCParticle> ftLink( evtSvc(), msgSvc(), LHCb::FTClusterLocation::Default );
  if ( ftLink.notFound() ) {
    error() << "Unable to retrieve FTCluster to MCParticle linker table." << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the linker table MuonCoord => MCParticle
  if ( m_decideUsingMuons ) {
    m_muonLink = MuonLink( evtSvc(), msgSvc(), LHCb::MuonCoordLocation::MuonCoords );
    if ( m_muonLink.notFound() ) {
      error() << "Unable to retrieve MuonCoord to MCParticle linker table." << endmsg;
      return StatusCode::FAILURE;
    }
  }

  // Loop over the Tracks
  LHCb::Track::Range::const_iterator it;
  for ( it = tracks.begin(); tracks.end() != it; ++it ) {
    const Track* tr = *it;
    m_nTotVP        = 0.;
    m_nTotUT1       = 0.;
    m_nTotSeed      = 0.;
    m_nTotMuon      = 0.;
    m_parts.clear();
    m_nVP.clear();
    m_nUT1.clear();
    m_nSeed.clear();
    m_nMuon.clear();

    // Loop over the LHCbIDs of the Track
    static int nMeas;
    nMeas = 0;
    for ( std::vector<LHCbID>::const_iterator iId = tr->lhcbIDs().begin(); tr->lhcbIDs().end() != iId; ++iId ) {
      if ( ( *iId ).isVP() ) {
        ++nMeas;
        VPChannelID vID = ( *iId ).vpID();
        // Count number of VP hits
        m_nTotVP += 1.;
        MCParticle* mcParticle = vpLink.first( vID );
        if ( m_debugLevel && 0 == mcParticle ) { debug() << "No MCParticle linked with VPCluster " << vID << endmsg; }
        while ( 0 != mcParticle ) {
          if ( mcParts != mcParticle->parent() ) {
            if ( m_debugLevel ) debug() << " (other BX " << mcParticle->key() << ")" << endmsg;
          } else {
            countMCPart( mcParticle, 1., 0., 0., 0. );
          }
          mcParticle = vpLink.next();
        }
        continue;
      }
      if ( ( *iId ).isUT() ) { // UT
        ++nMeas;
        m_nTotUT1 += 1.;
        UTChannelID utID       = ( *iId ).utID();
        MCParticle* mcParticle = m_utLink.first( utID );
        if ( m_debugLevel && 0 == mcParticle ) { debug() << "No MCParticle linked with UTCluster " << utID << endmsg; }
        while ( 0 != mcParticle ) {
          if ( mcParts != mcParticle->parent() ) {
            if ( m_debugLevel ) debug() << " (other BX " << mcParticle->key() << ")" << endmsg;
          } else {
            countMCPart( mcParticle, 0., 1., 0., 0. );
          }
          mcParticle = m_utLink.next();
        }
        continue;
      }
      if ( m_decideUsingMuons ) {
        if ( ( *iId ).isMuon() ) {
          ++nMeas;
          // Count number of Muon hits
          m_nTotMuon += 1.;
          MuonTileID  muonID     = ( *iId ).muonID();
          MCParticle* mcParticle = m_muonLink.first( muonID );
          if ( m_debugLevel && 0 == mcParticle ) {
            debug() << "No MCParticle linked with MuonCoord " << muonID << endmsg;
          }
          while ( 0 != mcParticle ) {
            if ( mcParts != mcParticle->parent() ) {
              debug() << " (other BX " << mcParticle->key() << ")" << endmsg;
            } else {
              countMCPart( mcParticle, 0., 0., 0., 1. );
            }
            mcParticle = m_muonLink.next();
          }
        }
      }
    }

    //== Handling of decays between VP and T: Associate the hits also to the mother

    for ( unsigned int j1 = 0; m_parts.size() > j1; ++j1 ) {
      const MCVertex* vOrigin = m_parts[j1]->originVertex();
      if ( 0 == vOrigin ) continue;
      const MCParticle* mother = vOrigin->mother();
      if ( 0 == mother ) continue;
      if ( vOrigin->type() != MCVertex::MCVertexType::DecayVertex ) continue;
      for ( unsigned int j2 = 0; m_parts.size() > j2; ++j2 ) {
        if ( mother == m_parts[j2] ) {
          if ( m_debugLevel )
            debug() << "  *** Particle " << m_parts[j1]->key() << "[" << m_parts[j1]->particleID().pid() << "] ("
                    << m_nVP[j1] << "," << m_nUT1[j1] << "," << m_nSeed[j1] << ")"
                    << " is daughter at z=" << vOrigin->position().z() << " of " << m_parts[j2]->key() << "["
                    << m_parts[j2]->particleID().pid() << "] (" << m_nVP[j2] << "," << m_nUT1[j2] << "," << m_nSeed[j2]
                    << ")"
                    << ". Merge hits to tag both." << endmsg;

          //== Daughter hits are added to mother.
          m_nVP[j2] += m_nVP[j1];
          m_nUT1[j2] += m_nUT1[j1];
          m_nSeed[j2] += m_nSeed[j1];

          //== Mother hits overwrite Daughter hits
          m_nVP[j1]   = m_nVP[j2];
          m_nUT1[j1]  = m_nUT1[j2];
          m_nSeed[j1] = m_nSeed[j2];
        }
      }
    }

    // Add parent muon hits to daughter MCParticle
    if ( m_decideUsingMuons ) {
      unsigned int j1, j2;
      for ( j1 = 0; m_parts.size() > j1; ++j1 ) {
        if ( m_nMuon[j1] < m_fractionOK * m_nTotMuon ) { continue; }
        const MCVertex* vOrigin = m_parts[j1]->originVertex();
        while ( 0 != vOrigin ) {
          const MCParticle* mother = vOrigin->mother();
          if ( 0 == mother ) break;
          for ( j2 = 0; m_parts.size() > j2; ++j2 ) {
            if ( m_nSeed[j2] < m_fractionOK * m_nTotSeed ) { continue; }
            if ( mother == m_parts[j2] ) {
              m_nMuon[j2] += m_nMuon[j1];
              m_nMuon[j1] = m_nMuon[j2];
            }
          }
          vOrigin = mother->originVertex();
        }
      }
    }

    // For each MCParticle with a Measurement associated,
    //  Relate the Track to the MCParticles matching the below criteria,
    //  using as weight the ratio of ( total # Measurements associated
    //                                 to this MCParticle )
    //                           and ( total # Measurements )
    //   if total # VP hits > 2,
    //   or if 0.70 <= ( # VP hits associated to the MCParticle /
    //                   total # VP hits )
    //  AND
    //   if total # IT+OT hits > 2,
    //   or if 0.70 <= ( # IT+OT hits associated to the MCParticle /
    //                   total # IT+OT hits )
    //  AND
    //   if # associated UT hits > ( total # UT hits - 2 ) ??
    //   or if total # VP hits > 2 AND total # IT+OT hits > 2
    // When taking Muon hits into account, also:
    //  AND
    //   if 0.70 <= ( # Muon hits associated to the MCParticle /
    //                total # Muon hits )
    double ratio;
    for ( unsigned int jj = 0; m_parts.size() > jj; ++jj ) {
      bool vpOK = true;
      if ( 2 < m_nTotVP ) {
        vpOK  = false;
        ratio = m_nVP[jj] / m_nTotVP;
        if ( m_fractionOK <= ratio ) { vpOK = true; }
      }
      bool seedOK = true;
      if ( 2 < m_nTotSeed ) {
        seedOK = false;
        ratio  = m_nSeed[jj] / m_nTotSeed;
        if ( m_fractionOK <= ratio ) { seedOK = true; }
      }
      bool UT1OK = m_nUT1[jj] > m_nTotUT1 - 2;
      if ( 2 < m_nTotVP && 2 < m_nTotSeed ) { UT1OK = true; }
      bool muonOK = true;
      if ( m_decideUsingMuons ) {
        ratio = m_nMuon[jj] / m_nTotMuon;
        if ( m_fractionOK > ratio ) { muonOK = false; }
      }

      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " MC " << m_parts[jj]->key() << " VP " << m_nVP[jj] << "/" << m_nTotVP << " UT1 " << m_nUT1[jj]
                  << "/" << m_nTotUT1 << " Seed " << m_nSeed[jj] << "/" << m_nTotSeed << endmsg;

      //=== Decision. Use UT1. Fill Linker
      if ( vpOK && seedOK && UT1OK && muonOK ) {
        double muons = 0.;
        if ( m_decideUsingMuons ) { muons = m_nMuon[jj]; }
        ratio = ( m_nVP[jj] + m_nSeed[jj] + m_nUT1[jj] + muons ) / nMeas;
        myLinker.link( tr, m_parts[jj], ratio );
        if ( m_debugLevel ) debug() << "Track: " << tr->key() << " was associated with ratio: " << ratio << endmsg;
      }
    }
  } // End loop over Tracks

  return StatusCode::SUCCESS;
}

//=============================================================================
// Adjust the counters for this MCParticle, create one if needed
//=============================================================================
void TrackAssociator::countMCPart( const MCParticle* part, double incVP, double incUT1, double incSeed,
                                   double incMuon ) {
  bool found = false;
  // Loop over the MCParticles vector to see whether part is already in m_parts
  for ( unsigned int jj = 0; m_parts.size() > jj; ++jj ) {
    if ( m_parts[jj] == part ) {
      m_nVP[jj] += incVP;
      m_nUT1[jj] += incUT1;
      m_nSeed[jj] += incSeed;
      m_nMuon[jj] += incMuon;
      // Prevent next if loop to run when this one already did the work
      found = true;
      // Stop looping when successful
      break;
    }
  }
  // If part an element of m_parts if it was not already and update counters
  if ( !found ) {
    m_parts.push_back( part );
    m_nVP.push_back( incVP );
    m_nUT1.push_back( incUT1 );
    m_nSeed.push_back( incSeed );
    m_nMuon.push_back( incMuon );
  }
}
