/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AIDA/IHistogram1D.h"

#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"

#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"

#include "Event/RecVertex.h"
#include "Event/RecVertex_v2.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"

#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiUtils/HistoStats.h"
#include "Kernel/STLExtensions.h"

#include <string>
#include <tuple>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : PrimaryVertexChecker
//-----------------------------------------------------------------------------

namespace {

  struct MCPVInfo {
    LHCb::MCVertex*                pMCPV;             // pointer to MC PV
    int                            nRecTracks;        // number of reconstructed tracks from this MCPV
    int                            nRecBackTracks;    // number of reconstructed backward tracks
    int                            indexRecPVInfo;    // index to reconstructed PVInfo (-1 if not reco)
    int                            nCorrectTracks;    // correct tracks belonging to reconstructed PV
    int                            multClosestMCPV;   // multiplicity of closest reconstructable MCPV
    double                         distToClosestMCPV; // distance to closest reconstructible MCPV
    bool                           decayCharm;        // type of mother particle
    bool                           decayBeauty;
    bool                           decayStrange;
    std::vector<LHCb::MCParticle*> m_mcPartInMCPV;
    std::vector<LHCb::Track*>      m_recTracksInMCPV;
  };

  struct RecPVInfo {
  public:
    int                               nTracks; // number of tracks
    double                            chi2;
    double                            nDoF;
    int                               mother;
    Gaudi::XYZPoint                   position;      // position
    Gaudi::XYZPoint                   positionSigma; // position sigmas
    int                               indexMCPVInfo; // index to MCPVInfo
    const LHCb::Event::v2::RecVertex* pRECPV;
  };

  inline const std::string beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";

  struct Beamline_t {
    double X = std::numeric_limits<double>::signaling_NaN();
    double Y = std::numeric_limits<double>::signaling_NaN();
    Beamline_t( Condition const& c )
        : X{( c.param<double>( "ResolPosRC" ) + c.param<double>( "ResolPosLA" ) ) / 2}
        , Y{c.param<double>( "ResolPosY" )} {}
  };

  bool sortmlt( MCPVInfo const& first, MCPVInfo const& second ) { return first.nRecTracks > second.nRecTracks; }

  enum class recoAs {
    all,
    isolated,
    close,
    ntracks_low,
    ntracks_high,
    z_low,
    z_middle,
    z_high,
    beauty,
    charm,
    strange,
    other,
    first,
    second,
    third,
    fourth,
    fifth
  };

  constexpr auto All = std::array{
      recoAs::all,      recoAs::isolated, recoAs::close,  recoAs::ntracks_low, recoAs::ntracks_high, recoAs::z_low,
      recoAs::z_middle, recoAs::z_high,   recoAs::beauty, recoAs::charm,       recoAs::strange,      recoAs::other,
      recoAs::first,    recoAs::second,   recoAs::third,  recoAs::fourth,      recoAs::fifth};
  constexpr auto Part  = std::array{recoAs::all, recoAs::beauty, recoAs::charm, recoAs::strange, recoAs::other};
  constexpr auto Basic = std::array{recoAs::all,          recoAs::isolated, recoAs::close,    recoAs::ntracks_low,
                                    recoAs::ntracks_high, recoAs::z_low,    recoAs::z_middle, recoAs::z_high,
                                    recoAs::beauty,       recoAs::charm,    recoAs::strange,  recoAs::other};

  constexpr int size_basic  = static_cast<int>( recoAs::other ) + 1;
  constexpr int size_recoAs = static_cast<int>( recoAs::fifth ) + 1;
  constexpr int size_multi  = size_recoAs - size_basic;
  constexpr int begin_multi = static_cast<int>( recoAs::first );

} // namespace

class PrimaryVertexChecker
    : public Gaudi::Functional::Consumer<void( const LHCb::Tracks&, LHCb::Event::v2::RecVertices const&,
                                               const LHCb::MCVertices&, const LHCb::MCParticles&, const LHCb::MCHeader&,
                                               const LHCb::MCProperty&, const Beamline_t& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, Beamline_t>> {
public:
  /// Standard constructor
  PrimaryVertexChecker( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name,
                 pSvcLocator,
                 {
                     KeyValue{"inputTracksName", LHCb::TrackLocation::Default},
                     KeyValue{"inputVerticesName", LHCb::Event::v2::RecVertexLocation::Primary},
                     KeyValue{"MCVertexInput", LHCb::MCVertexLocation::Default},
                     KeyValue{"MCParticleInput", LHCb::MCParticleLocation::Default},
                     KeyValue{"MCHeaderLocation", LHCb::MCHeaderLocation::Default},
                     KeyValue{"MCPropertyInput", LHCb::MCPropertyLocation::TrackInfo},
                     KeyValue{"BeamSpotLocation", "AlgorithmSpecific-" + name + "-beamspot"},
                 }} {}

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( const LHCb::Tracks& tracks, LHCb::Event::v2::RecVertices const& vertices,
                   const LHCb::MCVertices& mcvtx, const LHCb::MCParticles& mcps, const LHCb::MCHeader& mcheader,
                   const LHCb::MCProperty& flags,
                   const Beamline_t& ) const override; ///< Algorithm execution
  StatusCode finalize() override;                            ///< Algorithm finalization

private:
  // bool debugLevel() const { return msgLevel( MSG::DEBUG ) || msgLevel( MSG::VERBOSE ); }

  Gaudi::Property<int>    m_nTracksToBeRecble{this, "nTracksToBeRecble", 4};  // min number of tracks in PV
  Gaudi::Property<bool>   m_produceHistogram{this, "produceHistogram", true}; // producing histograms (light version)
  Gaudi::Property<bool>   m_produceNtuple{this, "produceNtuple", false};      // producing NTuples (full version)
  Gaudi::Property<bool>   m_requireVelo{this, "RequireVelo", true};           // requiring VELO for tracks
  Gaudi::Property<double> m_dzIsolated{this, "dzIsolated", 10.0 * Gaudi::Units::mm}; // split close/isolated PVs
  Gaudi::Property<int>    m_nTracksToPrint{this, "nTracksToPrint", 10};              // split low/high multiplicity PVs
  Gaudi::Property<double> m_zToPrint{this, "zToPrint", 50.0 * Gaudi::Units::mm};     // split in z

  mutable Gaudi::Accumulators::Counter<>                            m_nevt{this, "nEvents"};
  mutable std::map<recoAs, Gaudi::Accumulators::BinomialCounter<>>  m_false;     // False PVs vs Reco PVs
  mutable std::map<recoAs, Gaudi::Accumulators::BinomialCounter<>>  m_eff;       // MC reconstructible vs Reconstructed
  mutable std::map<recoAs, Gaudi::Accumulators::BinomialCounter<>>  m_mcpv;      // MC vs MC reconstrucible
  mutable std::map<recoAs, Gaudi::Accumulators::AveragingCounter<>> m_av_mcp;    // average mc particles in MCPV
  mutable std::map<recoAs, Gaudi::Accumulators::AveragingCounter<>> m_av_tracks; // average tracks in RecoPV

  std::vector<MCPVInfo>::iterator closestMCPV( std::vector<MCPVInfo>& rblemcpv, const MCPVInfo& mc ) const;
  // std::vector<MCPVInfo>::iterator& itmc ) const;
  std::vector<MCPVInfo>::iterator closestMCPV( std::vector<MCPVInfo>& rblemcpv, const RecPVInfo& rec ) const;

  void collectProductss( LHCb::MCVertex* mcpv, LHCb::MCVertex* mcvtx, std::vector<LHCb::MCParticle*>& allprods ) const;
  void printRat( const std::string name, const Gaudi::Accumulators::BinomialCounter<>& m_mcpv,
                 const Gaudi::Accumulators::BinomialCounter<>& m_eff,
                 const Gaudi::Accumulators::BinomialCounter<>& m_false );
  void printAvTracks( const std::string name, const Gaudi::Accumulators::AveragingCounter<>& m_av_tracks,
                      const Gaudi::Accumulators::AveragingCounter<>& m_av_mcp );
  void printRes( std::string mes, double x, double y, double z );
  std::string toString( const recoAs& n ) const;
  bool        checkCondition( const MCPVInfo& MCPV, const recoAs& n ) const;

  void   match_mc_vertex_by_distance( int ipv, std::vector<RecPVInfo>& rinfo, std::vector<MCPVInfo>& mcpvvec ) const;
  int    count_velo_tracks( const std::vector<LHCb::Event::v2::WeightedTrack>& tracksPV );
  void   count_reconstructible_mc_particles( std::vector<MCPVInfo>& mcpvvec, const LHCb::MCProperty& flags ) const;
  double check_histogram( const AIDA::IHistogram1D* h, bool var );
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( PrimaryVertexChecker, "PrimaryVertexChecker" )

StatusCode PrimaryVertexChecker::initialize() {
  return Consumer::initialize().andThen(
      [&] { addConditionDerivation<Beamline_t( Condition const& )>( {beamSpotCond}, inputLocation<Beamline_t>() ); } );
}

//=============================================================================
// Main execution
//=============================================================================
void PrimaryVertexChecker::operator()( const LHCb::Tracks& tracks, LHCb::Event::v2::RecVertices const& recoVtx,
                                       const LHCb::MCVertices& mcvtx, const LHCb::MCParticles& mcps,
                                       const LHCb::MCHeader& mcheader, const LHCb::MCProperty& flags,
                                       const Beamline_t& beamline ) const {

  debug() << "==> Execute" << endmsg;
  // Event
  m_nevt++;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << " # tracks: " << tracks.size() << "  # vertices: " << recoVtx.size() << endmsg;

  // Fill reconstucted PV info
  std::vector<RecPVInfo> recpvvec;
  recpvvec.reserve( recoVtx.size() );

  for ( const auto& pv : recoVtx ) {
    RecPVInfo recinfo;
    recinfo.pRECPV            = ( &pv );
    recinfo.position          = pv.position();
    Gaudi::SymMatrix3x3 covPV = pv.covMatrix();
    double              sigx  = sqrt( covPV( 0, 0 ) );
    double              sigy  = sqrt( covPV( 1, 1 ) );
    double              sigz  = sqrt( covPV( 2, 2 ) );
    Gaudi::XYZPoint     a3d( sigx, sigy, sigz );
    recinfo.positionSigma = a3d;
    recinfo.nTracks       = pv.tracks().size();
    recinfo.chi2          = pv.chi2();
    recinfo.nDoF          = pv.nDoF();
    recinfo.indexMCPVInfo = -1;

    int mother            = 0;
    recinfo.mother        = mother;
    recinfo.indexMCPVInfo = -1;
    recpvvec.push_back( recinfo );
  }

  // Fill MC PV info
  std::vector<MCPVInfo> mcpvvec;
  mcpvvec.reserve( mcvtx.size() );

  for ( const auto& mcpv : mcvtx ) {
    const LHCb::MCParticle* motherPart = mcpv->mother();
    if ( 0 == motherPart ) {
      if ( mcpv->type() == LHCb::MCVertex::MCVertexType::ppCollision ) {
        MCPVInfo mcprimvert;
        mcprimvert.pMCPV             = mcpv;
        mcprimvert.nRecTracks        = 0;
        mcprimvert.nRecBackTracks    = 0;
        mcprimvert.indexRecPVInfo    = -1;
        mcprimvert.nCorrectTracks    = 0;
        mcprimvert.multClosestMCPV   = 0;
        mcprimvert.distToClosestMCPV = 999999.;
        mcprimvert.decayBeauty       = false;
        mcprimvert.decayCharm        = false;
        mcprimvert.decayStrange      = false;
        mcprimvert.m_mcPartInMCPV.clear();
        mcprimvert.m_recTracksInMCPV.clear();
        mcpvvec.push_back( mcprimvert );
      }
    }
  }

  count_reconstructible_mc_particles( mcpvvec, flags );

  std::sort( mcpvvec.begin(), mcpvvec.end(), sortmlt );

  std::vector<MCPVInfo> rblemcpv;
  std::vector<MCPVInfo> notrblemcpv;
  std::vector<MCPVInfo> not_rble_but_visible;
  std::vector<MCPVInfo> not_rble;
  int                   nmrc = 0;

  std::vector<MCPVInfo>::iterator itmc;
  for ( itmc = mcpvvec.begin(); mcpvvec.end() != itmc; itmc++ ) {
    if ( itmc->nRecTracks >= m_nTracksToBeRecble ) {
      rblemcpv.push_back( *itmc );
    } else {
      notrblemcpv.push_back( *itmc );
    }
    if ( itmc->nRecTracks < m_nTracksToBeRecble && itmc->nRecTracks > 1 ) { not_rble_but_visible.push_back( *itmc ); }
    if ( itmc->nRecTracks < m_nTracksToBeRecble && itmc->nRecTracks < 2 ) { not_rble.push_back( *itmc ); }
  }

  for ( int ipv = 0; ipv < static_cast<int>( recpvvec.size() ); ipv++ ) {
    match_mc_vertex_by_distance( ipv, recpvvec, rblemcpv );
  }

  rblemcpv.insert( rblemcpv.end(), notrblemcpv.begin(), notrblemcpv.end() );

  debug() << endmsg << " MC vertices " << endmsg;
  debug() << " ===================================" << endmsg;
  for ( const auto& [ipv, rmcpv] : LHCb::range::enumerate( rblemcpv ) ) {
    std::string     ff   = " ";
    LHCb::MCVertex* mcpv = rmcpv.pMCPV;
    if ( rmcpv.indexRecPVInfo < 0 ) ff = "  NOTRECO";
    debug() << format( " %3d %3d  xyz ( %7.4f %7.4f %8.3f )   nrec = %4d", ipv, rmcpv.indexRecPVInfo,
                       mcpv->position().x(), mcpv->position().y(), mcpv->position().z(), rmcpv.nRecTracks )
            << ff << endmsg;
  }

  debug() << " -----------------------------------" << endmsg << endmsg;

  debug() << endmsg << " REC vertices " << endmsg;
  debug() << " ===================================" << endmsg;
  for ( const auto& [ipv, recpv] : LHCb::range::enumerate( recpvvec ) ) {
    std::string ff = " ";
    if ( recpvvec[ipv].indexMCPVInfo < 0 ) ff = "  FALSE  ";
    debug() << format(
                   " %3d %3d  xyz ( %7.4f %7.4f %8.3f )  ntra = %4d   sigxyz ( %7.4f %7.4f %8.4f )   chi2/NDF = %7.2f",
                   ipv, recpv.indexMCPVInfo, recpv.position.x(), recpv.position.y(), recpv.position.z(), recpv.nTracks,
                   recpv.positionSigma.x(), recpv.positionSigma.y(), recpv.positionSigma.z(),
                   recpv.pRECPV->chi2PerDoF() )
            << ff << endmsg;
  }
  debug() << " -----------------------------------" << endmsg;

  // find nr of false PV
  int nFalsePV_real = 0;
  for ( const auto& [ipv, recpv] : LHCb::range::enumerate( recpvvec ) ) {
    int    fake   = 0;
    double x      = recpv.position.x();
    double y      = recpv.position.y();
    double z      = recpv.position.z();
    double r      = std::sqrt( x * x + y * y );
    double errx   = recpv.positionSigma.x();
    double erry   = recpv.positionSigma.y();
    double errz   = recpv.positionSigma.z();
    double errr   = std::sqrt( ( ( x * errx ) * ( x * errx ) + ( y * erry ) * ( y * erry ) ) / ( x * x + y * y ) );
    int    mother = recpv.mother;
    double chi2   = recpv.chi2;
    double nDoF   = recpv.nDoF;

    if ( recpv.indexMCPVInfo < 0 ) {
      fake        = 1;
      auto   cmc  = closestMCPV( rblemcpv, recpv );
      double dist = 999999.;

      if ( cmc != rblemcpv.end() ) {
        dist = ( cmc->pMCPV->position() - recpv.pRECPV->position() ).R();

        for ( const auto& n : Part ) m_false[n] += checkCondition( *cmc, n );

        if ( dist > m_dzIsolated.value() ) {
          m_false[recoAs::isolated] += true;
        } else {
          m_false[recoAs::close] += true;
        }
        if ( recpv.nTracks >= m_nTracksToPrint.value() ) {
          m_false[recoAs::ntracks_high] += true;
        } else {
          m_false[recoAs::ntracks_low] += true;
        }
        if ( recpv.position.z() < -m_zToPrint.value() ) {
          m_false[recoAs::z_low] += true;
        } else if ( recpv.position.z() < m_zToPrint.value() ) {
          m_false[recoAs::z_middle] += true;
        } else {
          m_false[recoAs::z_high] += true;
        }

        int idx = std::distance( rblemcpv.begin(), cmc );
        if ( idx < size_multi ) { m_false[All[begin_multi + idx]] += true; }
      }

      bool vis_found = false;
      for ( unsigned int imc = 0; imc < not_rble_but_visible.size(); imc++ ) {
        if ( not_rble_but_visible[imc].indexRecPVInfo > -1 ) continue;
        double dist = fabs( mcpvvec[imc].pMCPV->position().z() - recpv.position.z() );
        if ( dist < 5.0 * recpv.positionSigma.z() ) {
          vis_found                                = true;
          not_rble_but_visible[imc].indexRecPVInfo = 10;
          break;
        }
      } // imc

      if ( !vis_found ) nFalsePV_real++;
    }
    if ( m_produceNtuple ) {
      Tuple myTuple2 = nTuple( 102, "PV_nTuple2", CLID_ColumnWiseTuple );
      myTuple2->column( "fake", double( fake ) ).ignore();
      myTuple2->column( "r", double( r ) ).ignore();
      myTuple2->column( "x", double( x ) ).ignore();
      myTuple2->column( "y", double( y ) ).ignore();
      myTuple2->column( "z", double( z ) ).ignore();
      myTuple2->column( "errr", double( errr ) ).ignore();
      myTuple2->column( "errz", double( errz ) ).ignore();
      myTuple2->column( "errx", double( errx ) ).ignore();
      myTuple2->column( "erry", double( erry ) ).ignore();
      myTuple2->column( "mother", double( mother ) ).ignore();
      myTuple2->column( "chi2", double( chi2 ) ).ignore();
      myTuple2->column( "nDoF", double( nDoF ) ).ignore();
      myTuple2->write().ignore();
    }
  }
  // Fill distance to closest recble MC PV and its multiplicity
  for ( auto& mcpv : rblemcpv ) {
    std::vector<MCPVInfo>::iterator cmc  = closestMCPV( rblemcpv, mcpv );
    double                          dist = 999999.;
    int                             mult = 0;
    if ( cmc != rblemcpv.end() ) {
      dist = ( cmc->pMCPV->position() - mcpv.pMCPV->position() ).R();
      mult = cmc->nRecTracks;
    }
    mcpv.distToClosestMCPV = dist;
    mcpv.multClosestMCPV   = mult;
  }

  for ( const auto& itmc : rblemcpv ) {
    for ( const auto& n : Basic ) {
      bool cut = false;
      cut      = checkCondition( itmc, n );
      if ( cut ) {
        if ( itmc.nRecTracks < m_nTracksToBeRecble ) {
          m_mcpv[n] += false;
        } else {
          m_mcpv[n] += true;
          m_av_mcp[n] += itmc.nRecTracks;
          if ( itmc.indexRecPVInfo < 0 ) {
            m_eff[n] += false;
          } else {
            m_eff[n] += true;
            m_false[n] += false;
          }
        }
        if ( itmc.indexRecPVInfo > -1 ) { m_av_tracks[n] += recpvvec[itmc.indexRecPVInfo].nTracks; }
      }
    }
  }

  int mcpv = 0;
  int high = 0;

  for ( auto& itmc : rblemcpv ) {
    double x             = -99999.;
    double y             = -99999.;
    double dx            = -99999.;
    double dy            = -99999.;
    double dz            = -99999.;
    double r             = -99999.;
    double zMC           = -99999.;
    double yMC           = -99999.;
    double xMC           = -99999.;
    double rMC           = -99999.;
    double z             = -99999.;
    double errx          = -99999.;
    double erry          = -99999.;
    double errz          = -99999.;
    double errr          = -99999.;
    double chi2          = -999999.;
    double nDoF          = -999999.;
    int    indRec        = itmc.indexRecPVInfo;
    int    reconstructed = 0;
    int    ntracks_pvrec = 0;
    int    ntracks_pvmc  = 0;
    int    dtrcks        = 0;
    int    pvevt         = 0;
    int    mother        = 0;
    int    assoctrks     = 0;
    int    nassoctrks    = 0;

    zMC = itmc.pMCPV->position().z();
    yMC = itmc.pMCPV->position().y();
    xMC = itmc.pMCPV->position().x();
    rMC = std::sqrt( ( xMC - beamline.X ) * ( xMC - beamline.X ) + ( yMC - beamline.Y ) * ( yMC - beamline.Y ) );

    if ( mcpv < size_multi ) {
      if ( itmc.nRecTracks < m_nTracksToBeRecble ) {
        m_mcpv[All[begin_multi + mcpv]] += false;
      } else {
        m_mcpv[All[begin_multi + mcpv]] += true;
        m_av_mcp[All[begin_multi + mcpv]] += itmc.nRecTracks;
        if ( itmc.indexRecPVInfo < 0 ) {
          m_eff[All[begin_multi + mcpv]] += false;
        } else {
          m_eff[All[begin_multi + mcpv]] += true;
          m_false[All[begin_multi + mcpv]] += false;
        }
      }
    }

    if ( indRec > -1 ) {
      high++;
      pvevt++;
      reconstructed = 1;
      dx            = recpvvec[indRec].position.x() - itmc.pMCPV->position().x();
      dy            = recpvvec[indRec].position.y() - itmc.pMCPV->position().y();
      dz            = recpvvec[indRec].position.z() - itmc.pMCPV->position().z();
      x             = recpvvec[indRec].position.x();
      y             = recpvvec[indRec].position.y();
      z             = recpvvec[indRec].position.z();
      r             = std::sqrt( ( x - beamline.X ) * ( x - beamline.X ) + ( y - beamline.Y ) * ( y - beamline.Y ) );
      errx          = recpvvec[indRec].positionSigma.x();
      erry          = recpvvec[indRec].positionSigma.y();
      errz          = recpvvec[indRec].positionSigma.z();
      errr          = std::sqrt( ( ( x * errx ) * ( x * errx ) + ( y * erry ) * ( y * erry ) ) / ( x * x + y * y ) );
      ntracks_pvrec = recpvvec[indRec].nTracks;
      ntracks_pvmc  = itmc.pMCPV->products().size();
      dtrcks        = ntracks_pvmc - ntracks_pvrec;
      mother        = recpvvec[indRec].mother;
      chi2          = recpvvec[indRec].chi2;
      nDoF          = recpvvec[indRec].nDoF;

      if ( mcpv < size_multi ) { m_av_tracks[All[begin_multi + mcpv]] += recpvvec[indRec].nTracks; }
      // Filling histograms
      if ( m_produceHistogram ) {
        plot( itmc.pMCPV->position().x(), 1001, "xmc", -0.25, 0.25, 50 );
        plot( itmc.pMCPV->position().y(), 1002, "ymc", -0.25, 0.25, 50 );
        plot( itmc.pMCPV->position().z(), 1003, "zmc", -20, 20, 50 );
        plot( recpvvec[indRec].position.x(), 1011, "xrd", -0.25, 0.25, 50 );
        plot( recpvvec[indRec].position.y(), 1012, "yrd", -0.25, 0.25, 50 );
        plot( recpvvec[indRec].position.z(), 1013, "zrd", -20, 20, 50 );
        plot( dx, 1021, "dx", -0.25, 0.25, 50 );
        plot( dy, 1022, "dy", -0.25, 0.25, 50 );
        plot( dz, 1023, "dz", -0.5, 0.5, 50 );
        plot( dx / errx, 1031, "pullx", -5., 5., 50 );
        plot( dy / erry, 1032, "pully", -5., 5., 50 );
        plot( dz / errz, 1033, "pullz", -5., 5., 50 );
        if ( itmc.nRecTracks < 10 ) {
          plot( dx, 1101, "dx", -0.25, 0.25, 50 );
          plot( dy, 1102, "dy", -0.25, 0.25, 50 );
          plot( dz, 1103, "dz", -0.5, 0.5, 50 );
        } else if ( itmc.nRecTracks >= 10 && itmc.nRecTracks < 30 ) {
          plot( dx, 1111, "dx", -0.25, 0.25, 50 );
          plot( dy, 1112, "dy", -0.25, 0.25, 50 );
          plot( dz, 1113, "dz", -0.5, 0.5, 50 );
        } else {
          plot( dx, 1121, "dx", -0.25, 0.25, 50 );
          plot( dy, 1122, "dy", -0.25, 0.25, 50 );
          plot( dz, 1123, "dz", -0.5, 0.5, 50 );
        }

        if ( itmc.pMCPV->position().z() < -m_zToPrint ) {
          plot( dx, 1201, "dx", -0.25, 0.25, 50 );
          plot( dy, 1202, "dy", -0.25, 0.25, 50 );
          plot( dz, 1203, "dz", -0.5, 0.5, 50 );
        } else if ( itmc.pMCPV->position().z() < m_zToPrint ) {
          plot( dx, 1211, "dx", -0.25, 0.25, 50 );
          plot( dy, 1212, "dy", -0.25, 0.25, 50 );
          plot( dz, 1213, "dz", -0.5, 0.5, 50 );
        } else {
          plot( dx, 1221, "dx", -0.25, 0.25, 50 );
          plot( dy, 1222, "dy", -0.25, 0.25, 50 );
          plot( dz, 1223, "dz", -0.5, 0.5, 50 );
        }

        plot( double( ntracks_pvrec ), 1041, "ntracks_pvrec", 0., 150., 50 );
        plot( double( dtrcks ), 1042, "mcrdtracks", 0., 150., 50 );
        if ( pvevt == 1 ) {
          plot( double( recpvvec.size() ), 1051, "nPVperEvt", -0.5, 5.5, 6 );
          for ( int ipvrec = 0; ipvrec < (int)recpvvec.size(); ipvrec++ ) {
            assoctrks = assoctrks + recpvvec[ipvrec].nTracks;
          }
          nassoctrks = tracks.size() - assoctrks;
          plot( double( nassoctrks ), 1052, "nassoctrks", 0., 150., 50 );
        }
      }
    }
    mcpv++;

    int    isolated     = 0;
    double dist_closest = itmc.distToClosestMCPV;
    if ( dist_closest > m_dzIsolated.value() ) { isolated = 1; }

    // Filling ntuple
    if ( m_produceNtuple ) {
      Tuple myTuple = nTuple( 101, "PV_nTuple", CLID_ColumnWiseTuple );
      myTuple->column( "reco", double( reconstructed ) ).ignore();
      myTuple->column( "isol", double( isolated ) ).ignore();
      myTuple->column( "ntracks", double( ntracks_pvrec ) ).ignore();
      myTuple->column( "nrectrmc", double( itmc.nRecTracks ) ).ignore();
      myTuple->column( "dzclose", dist_closest ).ignore();
      myTuple->column( "nmcpv", double( rblemcpv.size() ) ).ignore();
      myTuple->column( "mtruemcpv", double( mcheader.numOfPrimaryVertices() ) ).ignore();
      myTuple->column( "nmcallpv", double( mcpvvec.size() ) ).ignore();
      myTuple->column( "nrecpv", double( recpvvec.size() ) ).ignore();
      myTuple->column( "decayCharm", double( itmc.decayCharm ) ).ignore();
      myTuple->column( "decayBeauty", double( itmc.decayBeauty ) ).ignore();
      myTuple->column( "decayStrange", double( itmc.decayStrange ) ).ignore();
      myTuple->column( "multirec", double( high ) ).ignore();
      myTuple->column( "multimc", double( mcpv ) ).ignore();
      myTuple->column( "dx", dx ).ignore();
      myTuple->column( "dy", dy ).ignore();
      myTuple->column( "dz", dz ).ignore();
      myTuple->column( "x", x ).ignore();
      myTuple->column( "y", y ).ignore();
      myTuple->column( "r", r ).ignore();
      myTuple->column( "zMC", zMC ).ignore();
      myTuple->column( "yMC", yMC ).ignore();
      myTuple->column( "xMC", xMC ).ignore();
      myTuple->column( "rMC", rMC ).ignore();
      myTuple->column( "z", z ).ignore();
      myTuple->column( "xBeam", beamline.X ).ignore();
      myTuple->column( "yBeam", beamline.Y ).ignore();
      myTuple->column( "errx", errx ).ignore();
      myTuple->column( "erry", erry ).ignore();
      myTuple->column( "errz", errz ).ignore();
      myTuple->column( "errr", errr ).ignore();
      myTuple->column( "mother", double( mother ) ).ignore();
      myTuple->column( "evtnr", double( m_nevt.nEntries() ) ).ignore();
      myTuple->column( "chi2", double( chi2 ) ).ignore();
      myTuple->column( "nDoF", double( nDoF ) ).ignore();
      myTuple->column( "size_tracks", double( tracks.size() ) ).ignore();
      myTuple->column( "size_mcp", double( mcps.size() ) ).ignore();
      myTuple->column( "mcpvrec", double( nmrc ) ).ignore();
      myTuple->write().ignore();
    }
  }

  return;
}

void PrimaryVertexChecker::match_mc_vertex_by_distance( int ipv, std::vector<RecPVInfo>& rinfo,
                                                        std::vector<MCPVInfo>& mcpvvec ) const {

  double mindist = 999999.;
  int    indexmc = -1;

  for ( const auto& [imc, mcpv] : LHCb::range::enumerate( mcpvvec ) ) {
    if ( mcpv.indexRecPVInfo > -1 ) continue;
    double dist = fabs( mcpv.pMCPV->position().z() - rinfo[ipv].position.z() );
    if ( dist < mindist ) {
      mindist = dist;
      indexmc = imc;
    }
  }
  if ( indexmc > -1 ) {
    if ( mindist < 5.0 * rinfo[ipv].positionSigma.z() ) {
      rinfo[ipv].indexMCPVInfo        = indexmc;
      mcpvvec[indexmc].indexRecPVInfo = ipv;
    }
  }
}

std::vector<MCPVInfo>::iterator PrimaryVertexChecker::closestMCPV( std::vector<MCPVInfo>& rblemcpv,
                                                                   const MCPVInfo&        mc ) const {

  auto   itret   = rblemcpv.end();
  double mindist = 999999.;
  if ( rblemcpv.size() < 2 ) return itret;
  std::vector<MCPVInfo>::iterator it;
  for ( it = rblemcpv.begin(); it != rblemcpv.end(); it++ ) {
    if ( it->pMCPV != mc.pMCPV ) {
      double dist = ( it->pMCPV->position() - mc.pMCPV->position() ).R();
      if ( dist < mindist ) {
        mindist = dist;
        itret   = it;
      }
    }
  }
  return itret;
}

std::vector<MCPVInfo>::iterator PrimaryVertexChecker::closestMCPV( std::vector<MCPVInfo>& rblemcpv,
                                                                   const RecPVInfo&       rec ) const {
  auto   itret   = rblemcpv.end();
  double mindist = 999999.;
  if ( rblemcpv.size() < 2 ) return itret;
  std::vector<MCPVInfo>::iterator it;
  for ( it = rblemcpv.begin(); it != rblemcpv.end(); it++ ) {
    double dist = ( it->pMCPV->position() - rec.pRECPV->position() ).R();
    if ( dist < mindist ) {
      mindist = dist;
      itret   = it;
    }
  }
  return itret;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode PrimaryVertexChecker::finalize() {

  debug() << "==> Finalize" << endmsg;
  info() << "     ************************************ " << endmsg;
  info() << " MC PV is reconstructible if at least " << m_nTracksToBeRecble.value() << "  tracks are reconstructed"
         << endmsg;
  info() << " MC PV is isolated if dz to closest reconstructible MC PV >  " << m_dzIsolated.value() << " mm" << endmsg;
  info() << " MC efficiency split by tracks with threshold: (" << m_nTracksToBeRecble.value() << ","
         << m_nTracksToPrint.value() << "), >= " << m_nTracksToPrint.value() << endmsg;
  info() << " MC efficiency split by z position: <-" << m_zToPrint.value() << ", (-" << m_zToPrint.value() << ","
         << m_zToPrint.value() << "), >" << m_zToPrint.value() << endmsg;
  std::string ff = "by distance";
  info() << " REC and MC vertices matched:  " << ff << endmsg;

  info() << " " << endmsg;
  for ( const auto& n : All ) { printRat( toString( n ), m_mcpv[n], m_eff[n], m_false[n] ); }
  info() << " " << endmsg;
  for ( const auto& n : All ) { printAvTracks( toString( n ), m_av_tracks[n], m_av_mcp[n] ); }
  info() << " " << endmsg;

  printRes( "1_res_all", check_histogram( histo( HistoID( 1021 ) ), true ),
            check_histogram( histo( HistoID( 1022 ) ), true ), check_histogram( histo( HistoID( 1023 ) ), true ) );

  printRes( "2_res_ntracks<10", check_histogram( histo( HistoID( 1101 ) ), true ),
            check_histogram( histo( HistoID( 1102 ) ), true ), check_histogram( histo( HistoID( 1103 ) ), true ) );
  printRes( "3_res_ntracks(10,30)", check_histogram( histo( HistoID( 1111 ) ), true ),
            check_histogram( histo( HistoID( 1112 ) ), true ), check_histogram( histo( HistoID( 1113 ) ), true ) );
  printRes( "4_res_ntracks>30", check_histogram( histo( HistoID( 1121 ) ), true ),
            check_histogram( histo( HistoID( 1122 ) ), true ), check_histogram( histo( HistoID( 1123 ) ), true ) );

  printRes( "5_res_z<-50", check_histogram( histo( HistoID( 1201 ) ), true ),
            check_histogram( histo( HistoID( 1202 ) ), true ), check_histogram( histo( HistoID( 1203 ) ), true ) );
  printRes( "6_res_z(-50,50)", check_histogram( histo( HistoID( 1211 ) ), true ),
            check_histogram( histo( HistoID( 1212 ) ), true ), check_histogram( histo( HistoID( 1213 ) ), true ) );
  printRes( "7_res_z>50", check_histogram( histo( HistoID( 1221 ) ), true ),
            check_histogram( histo( HistoID( 1222 ) ), true ), check_histogram( histo( HistoID( 1223 ) ), true ) );

  info() << " " << endmsg;

  printRes( "1_pull_width_all", check_histogram( histo( HistoID( 1031 ) ), true ),
            check_histogram( histo( HistoID( 1032 ) ), true ), check_histogram( histo( HistoID( 1033 ) ), true ) );
  printRes( "1_pull_mean_all", check_histogram( histo( HistoID( 1031 ) ), false ),
            check_histogram( histo( HistoID( 1032 ) ), false ), check_histogram( histo( HistoID( 1033 ) ), false ) );

  info() << " " << endmsg;

  return GaudiTupleAlg::finalize(); // Must be called after all other actions
}

void PrimaryVertexChecker::printRat( const std::string name, const Gaudi::Accumulators::BinomialCounter<>& m_mcpv,
                                     const Gaudi::Accumulators::BinomialCounter<>& m_eff,
                                     const Gaudi::Accumulators::BinomialCounter<>& m_false ) {
  unsigned int len  = 25;
  std::string  pmes = name;
  if ( pmes.size() < len ) pmes.resize( len, ' ' );
  pmes += " : ";
  info() << pmes
         << format( "%8d from %8d (%8d-%-8d) [ %5.2f %], false %4d from reco. %8d (%8d+%-4d) [ %5.2f %] ",
                    m_eff.nTrueEntries(), m_eff.nEntries(), m_mcpv.nEntries(), m_mcpv.nFalseEntries(),
                    m_eff.efficiency() * 100, m_false.nTrueEntries(), m_false.nEntries(), m_false.nFalseEntries(),
                    m_false.nTrueEntries(), m_false.efficiency() * 100 )
         << endmsg;
}

void PrimaryVertexChecker::printAvTracks( const std::string                              name,
                                          const Gaudi::Accumulators::AveragingCounter<>& m_av_tracks,
                                          const Gaudi::Accumulators::AveragingCounter<>& m_av_mcp ) {
  unsigned int len  = 25;
  std::string  pmes = name;
  if ( pmes.size() < len ) pmes.resize( len, ' ' );
  pmes += " : ";
  info() << pmes << format( "av. PV tracks: %6.2f [MC: %6.2f]", m_av_tracks.mean(), m_av_mcp.mean() ) << endmsg;
}

void PrimaryVertexChecker::printRes( std::string mes, double x, double y, double z ) {
  unsigned int len  = 25;
  std::string  pmes = mes;
  while ( pmes.length() < len ) { pmes += " "; }
  pmes += " : ";
  info() << pmes << format( " x: %+5.3f, y: %+5.3f, z: %+5.3f", x, y, z ) << endmsg;
}

double PrimaryVertexChecker::check_histogram( const AIDA::IHistogram1D* h, bool rms ) {
  return h ? ( rms ? h->rms() : h->mean() ) : 0.;
}

int PrimaryVertexChecker::count_velo_tracks( const std::vector<LHCb::Event::v2::WeightedTrack>& tracksPV ) {
  return std::count_if( tracksPV.begin(), tracksPV.end(), []( const auto& t ) { return t.track->hasVelo(); } );
}

void PrimaryVertexChecker::collectProductss( LHCb::MCVertex* mcpv, LHCb::MCVertex* mcvtx,
                                             std::vector<LHCb::MCParticle*>& allprods ) const {

  SmartRefVector<LHCb::MCParticle>           daughters = mcvtx->products();
  SmartRefVector<LHCb::MCParticle>::iterator idau;
  for ( idau = daughters.begin(); idau != daughters.end(); idau++ ) {
    double dv2 = ( mcpv->position() - ( *idau )->originVertex()->position() ).Mag2();
    if ( dv2 > ( 100. * Gaudi::Units::mm ) * ( 100. * Gaudi::Units::mm ) ) continue;
    LHCb::MCParticle* pmcp = *idau;
    allprods.push_back( pmcp );
    SmartRefVector<LHCb::MCVertex>           decays = ( *idau )->endVertices();
    SmartRefVector<LHCb::MCVertex>::iterator ivtx;
    for ( ivtx = decays.begin(); ivtx != decays.end(); ivtx++ ) { collectProductss( mcpv, *ivtx, allprods ); }
  }
}

void PrimaryVertexChecker::count_reconstructible_mc_particles( std::vector<MCPVInfo>&  mcpvvec,
                                                               const LHCb::MCProperty& flags ) const {

  const MCTrackInfo trInfo = {flags};
  for ( auto& itinfomc : mcpvvec ) {
    LHCb::MCVertex*                  avtx = itinfomc.pMCPV;
    std::vector<LHCb::MCParticle*>   mcPartInMCPV;
    SmartRefVector<LHCb::MCParticle> parts = avtx->products();
    std::vector<LHCb::MCParticle*>   allproducts;
    collectProductss( avtx, avtx, allproducts );

    for ( const auto pmcp : allproducts ) {
      if ( pmcp->particleID().isMeson() || pmcp->particleID().isBaryon() ) {
        if ( pmcp->particleID().hasBottom() ) { itinfomc.decayBeauty = true; }
        if ( pmcp->particleID().hasCharm() ) { itinfomc.decayCharm = true; }
        if ( pmcp->particleID().hasStrange() ) { itinfomc.decayStrange = true; }
      }
      if ( ( pmcp->particleID().isMeson() || pmcp->particleID().isBaryon() ) &&
           ( !m_requireVelo || trInfo.hasVelo( pmcp ) ) ) {
        double dv2 = ( avtx->position() - pmcp->originVertex()->position() ).Mag2();
        if ( dv2 < 0.0000001 && pmcp->p() > 100. * Gaudi::Units::MeV ) { mcPartInMCPV.push_back( pmcp ); }
      }
      itinfomc.nRecTracks = mcPartInMCPV.size();
    }
  }
}

bool PrimaryVertexChecker::checkCondition( const MCPVInfo& MCPV, const recoAs& n ) const {
  switch ( n ) {
  case recoAs::all:
    return true;
  case recoAs::isolated:
    return ( MCPV.distToClosestMCPV > m_dzIsolated );
  case recoAs::close:
    return ( MCPV.distToClosestMCPV <= m_dzIsolated );
  case recoAs::ntracks_low:
    return ( MCPV.nRecTracks >= m_nTracksToBeRecble && MCPV.nRecTracks < m_nTracksToPrint );
  case recoAs::ntracks_high:
    return ( MCPV.nRecTracks >= m_nTracksToPrint );
  case recoAs::z_low:
    return ( MCPV.pMCPV->position().z() < -m_zToPrint );
  case recoAs::z_middle:
    return ( MCPV.pMCPV->position().z() >= -m_zToPrint && MCPV.pMCPV->position().z() < m_zToPrint );
  case recoAs::z_high:
    return ( MCPV.pMCPV->position().z() >= m_zToPrint );
  case recoAs::beauty:
    return ( MCPV.decayBeauty );
  case recoAs::charm:
    return ( MCPV.decayCharm );
  case recoAs::strange:
    return ( MCPV.decayStrange );
  case recoAs::other:
    return ( !( MCPV.decayBeauty ) && !( MCPV.decayCharm ) && !( MCPV.decayStrange ) );
  default:
    return false;
  }
}

std::string PrimaryVertexChecker::toString( const recoAs& n ) const {
  switch ( n ) {
  case recoAs::all:
    return format( "0%d all", int( recoAs::all ) );
  case recoAs::isolated:
    return format( "0%d isolated", int( recoAs::isolated ) );
  case recoAs::close:
    return format( "0%d close", int( recoAs::close ) );
  case recoAs::ntracks_low:
    return format( "0%d ntracks<%d", int( recoAs::ntracks_low ), int( m_nTracksToPrint ) );
  case recoAs::ntracks_high:
    return format( "0%d ntracks>=%d", int( recoAs::ntracks_high ), int( m_nTracksToPrint ) );
  case recoAs::z_low:
    return format( "0%d z<%2.1f", int( recoAs::z_low ), -m_zToPrint );
  case recoAs::z_middle:
    return format( "0%d z in (%2.1f, %2.1f)", int( recoAs::z_middle ), -m_zToPrint, +m_zToPrint );
  case recoAs::z_high:
    return format( "0%d z >=%2.1f", int( recoAs::z_high ), +m_zToPrint );
  case recoAs::beauty:
    return format( "0%d decayBeauty", int( recoAs::beauty ) );
  case recoAs::charm:
    return format( "0%d decayCharm", int( recoAs::charm ) );
  case recoAs::strange:
    return format( "%d decayStrange", int( recoAs::strange ) );
  case recoAs::other:
    return format( "%d other", int( recoAs::other ) );
  case recoAs::first:
    return format( "%d 1MCPV", int( recoAs::first ) );
  case recoAs::second:
    return format( "%d 2MCPV", int( recoAs::second ) );
  case recoAs::third:
    return format( "%d 3MCPV", int( recoAs::third ) );
  case recoAs::fourth:
    return format( "%d 4MCPV", int( recoAs::fourth ) );
  case recoAs::fifth:
    return format( "%d 5MCPV", int( recoAs::fifth ) );
  default:
    return "not defined";
  }
}
