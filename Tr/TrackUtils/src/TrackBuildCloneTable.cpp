/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/TrackUnitsConverters.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "LHCbMath/MatrixUtils.h"
#include "LHCbMath/Similarity.h"
#include "Linker/LinkerWithKey.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"
#include <string>
#include <vector>

using namespace LHCb;
/** @class TrackBuildCloneTable TrackBuildCloneTable.h
 *
 *  Creates a summary linker object container clone information,
 *  using a Kullbeck Liebler Distance
 *
 *  @author M.Needham
 *  @date   30/05/2006
 */

class TrackBuildCloneTable : public GaudiAlgorithm {
public:
  /// Constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; /// Initialisation
  StatusCode execute() override;    /// Execution

private:
  Gaudi::Property<double>                   m_klCut{this, "klCut",
                                  5000}; ///< Maximum distance for the information to be stored in the linker
  Gaudi::Property<std::vector<std::string>> m_inputLocations{
      this, "inputLocations", {TrackLocation::Default}}; ///< Input TES locations for tracks
  Gaudi::Property<std::string> m_outputLocation{
      this, "outputLocation", TrackLocation::Default + "Clones"}; ///< Output TES location for the linker object

  Gaudi::Property<std::vector<double>> m_zStates{this, "zStates", {0.0}}; ///< The z position(s) to use for the
                                                                          ///< comparision
  Gaudi::Property<double> m_maxDz{this, "maxDz", 2.0 * Gaudi::Units::m};  ///< max tolerance on finding a state close to
                                                                          ///< the comparison z position

  PublicToolHandle<ITrackExtrapolator> m_extrapolator{
      this, "Extrapolator", "TrackHerabExtrapolator"}; ///< Pointer to the state extrapolator to use
};

namespace {

  void invertCovMatrix( Gaudi::TrackSymMatrix& m ) {
    TrackUnitsConverters::convertToG3( m );
    m.Invert();
    TrackUnitsConverters::convertToG4( m );
  }

  /** @class CloneState TrackBuildCloneTable.h
   *
   *  Working state object for clone finder
   *
   *  @author M.Needham
   *  @date   30/05/2006
   */
  struct CloneState final {
    /// Contructor from a state and parent track object
    CloneState( const LHCb::State& _state ) : state( _state ), invCov( _state.covariance() ) {
      invertCovMatrix( invCov );
    }
    LHCb::State           state;  ///< The state to use for the comparisions
    Gaudi::TrackSymMatrix invCov; ///< Cached inverted matrix
  };

  /** @class CloneTrack TrackBuildCloneTable.h
   *
   *  Working track object for clone finder
   *
   *  @author M.Needham
   *  @date   30/05/2006
   */
  struct CloneTrack final {
    CloneTrack( const LHCb::Track* _track = nullptr ) : track( _track ) {}
    [[nodiscard]] const CloneState*            findState( double z ) const;
    std::vector<std::pair<double, CloneState>> states;
    const LHCb::Track*                         track; ///< Pointer to the parent track
    /// Container of working tracks
    using Vector = std::vector<CloneTrack>;
  };

  const CloneState* CloneTrack::findState( double z ) const {
    auto i = std::find_if( states.begin(), states.end(),
                           [&]( const std::pair<double, CloneState>& cs ) { return fabs( z - cs.first ) < 1e-3; } );
    return i != states.end() ? &( i->second ) : nullptr;
  }

  /// Computes the KL distance between the two tracks
  double kullbeckLieblerDist( const CloneState& c1, const CloneState& c2 ) {
    Gaudi::TrackSymMatrix diffCov = c1.state.covariance() - c2.state.covariance();
    Gaudi::TrackSymMatrix invDiff = c2.invCov - c1.invCov;
    // trace
    return Gaudi::Math::trace( diffCov * invDiff ) +
           LHCb::Math::Similarity( c1.state.stateVector() - c2.state.stateVector(), c2.invCov + c1.invCov );
  }
} // namespace

DECLARE_COMPONENT( TrackBuildCloneTable )

StatusCode TrackBuildCloneTable::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] {
    info() << "Will build clone table for z=" << m_zStates << "mm at '" << m_outputLocation << "'" << endmsg;
    info() << "Max Kullbeck Liebler Distance = " << m_klCut << endmsg;
  } );
}

StatusCode TrackBuildCloneTable::execute() {

  // new linker to store results
  LinkerWithKey<Track, Track> myLink( eventSvc(), msgSvc(), m_outputLocation );

  // working objects ---> invert the covariance matrix only once...
  CloneTrack::Vector tracks;

  // loop and make working tracks
  for ( const auto& loc : m_inputLocations ) {
    // Tracks
    auto inCont = getIfExists<Tracks>( loc );
    if ( !inCont ) {
      error() << "Container " << loc << " does not exist" << endmsg;
      continue;
    }
    if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Found " << inCont->size() << " Tracks at " << loc << endmsg; }
    tracks.reserve( tracks.size() + inCont->size() );

    for ( const auto& track : *inCont ) {
      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
        debug() << "Track " << track->key() << " " << track->history() << endmsg;

      // make working track object, directly in the vector
      tracks.emplace_back( track );
      CloneTrack& cloneTrack = tracks.back();
      cloneTrack.states.reserve( m_zStates.size() );

      // Loop over all z positions
      for ( auto z : m_zStates ) {
        // get state closest to reference z pos
        const State& cState = closestState( *track, z );

        // only take ones that are close in z
        if ( fabs( cState.z() - z ) > m_maxDz ) continue;
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) )
          debug() << " -> Found state closest to z=" << z << " at z=" << cState.z() << endmsg;

        // clone state
        State tmpState( cState );

        // extrapolate to desired z pos
        const StatusCode sc = m_extrapolator->propagate( tmpState, z );
        if ( sc.isFailure() ) {
          Warning( "Problem extrapolating state", StatusCode::SUCCESS ).ignore();
          continue;
        }

        // Add Clone state to clone track
        cloneTrack.states.emplace_back( z, tmpState );

      } // z states

      // Did we find any states ? If not remove clone track
      if ( cloneTrack.states.empty() ) tracks.pop_back();

    } // inCont
  }
  // need more than one track to do something usefull...
  if ( tracks.size() < 2 ) return StatusCode::SUCCESS;

  // loop over the working objects
  if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) debug() << "Looping over all Track pairs to find clones :-" << endmsg;
  for ( auto iterC1 = tracks.cbegin(); iterC1 != tracks.cend(); ++iterC1 ) {
    // KL dist is symmetric between tracks, so only compute for each pair once
    for ( auto iterC2 = std::next( iterC1 ); iterC2 != tracks.cend(); ++iterC2 ) {

      // overall distance for this track pair
      auto overall_dist =
          std::accumulate( m_zStates.begin(), m_zStates.end(), std::optional<double>{},
                           [iterC1, iterC2, this]( std::optional<double> dist, double z ) {
                             // Do both clone tracks have states at this z ?
                             const CloneState* c1 = iterC1->findState( z );
                             if ( !c1 ) return dist;
                             const CloneState* c2 = iterC2->findState( z );
                             if ( !c2 ) return dist;
                             const double kl = kullbeckLieblerDist( *c1, *c2 );
                             if ( msgLevel( MSG::VERBOSE ) || ( msgLevel( MSG::DEBUG ) && kl < m_klCut ) )
                               debug() << " -> Tracks " << iterC1->track->key() << " and " << iterC2->track->key()
                                       << " both have a state at z=" << z << " KL-dist=" << kl << endmsg;
                             if ( !dist ) {
                               dist = kl;
                             } else {
                               *dist += kl;
                             }
                             return dist;
                           } );

      if ( overall_dist && *overall_dist < m_klCut ) {
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "  -> Possible clones Track1 : key = " << iterC1->track->key()
                  << " history = " << iterC1->track->history() << endmsg;
          debug() << "                     Track2 : key = " << iterC2->track->key()
                  << " history = " << iterC2->track->history() << endmsg;
          debug() << "                            : KL distance = " << *overall_dist << endmsg;
        }
        // fill twice, for each way around for the track pair
        myLink.link( iterC1->track, iterC2->track, *overall_dist );
        myLink.link( iterC2->track, iterC1->track, *overall_dist );
      }

    } // iterC2
  }   // iterC1

  return StatusCode::SUCCESS;
}
