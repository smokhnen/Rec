/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrDownstreamTracks.h"
#include "Event/PrLongTracks.h"
#include "Event/Track.h"
#include "Event/Track_v1.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/PluginServiceV2.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/SIMDWrapper.h"
#include "TrackKernel/TrackCloneData.h"
#include <algorithm>
#include <functional>
#include <stdexcept>

namespace {

  struct SimpleTrackData : public LHCb::TrackCloneDataUtils::TrackCloneDataBaseBloomSliceWithLHCbIDs<> {
    SimpleTrackData( std::vector<LHCb::LHCbID> const& trackids, float qp ) : m_qOverP( qp ) {
      for ( auto it = trackids.cbegin(); trackids.cend() != it; ++it ) {
        switch ( it->detectorType() ) {
        case LHCb::LHCbID::channelIDtype::VP: {
          auto jt = it + 1;
          for ( ; trackids.cend() != jt && ( LHCb::LHCbID::channelIDtype::VP == jt->detectorType() ); ++jt ) {}
          m_ids[HitType::VP] = LHCbIDs( it, jt );
          this->m_bloomfilters[HitType::VP].insert( m_ids[HitType::VP] );
          it = --jt;
        } break;
        case LHCb::LHCbID::channelIDtype::UT: {
          auto jt = it + 1;
          for ( ; trackids.cend() != jt && ( LHCb::LHCbID::channelIDtype::UT == jt->detectorType() ); ++jt ) {}
          m_ids[HitType::UT] = LHCbIDs( it, jt );
          this->m_bloomfilters[HitType::UT].insert( m_ids[HitType::UT] );
          it = --jt;
        } break;
        case LHCb::LHCbID::channelIDtype::FT: {
          auto jt = it + 1;
          for ( ; trackids.cend() != jt && ( LHCb::LHCbID::channelIDtype::FT == jt->detectorType() ); ++jt ) {}
          m_ids[HitType::T] = LHCbIDs( it, jt );
          this->m_bloomfilters[HitType::T].insert( m_ids[HitType::T] );
          it = --jt;
        } break;
        default:
          break;
        }
      }
    }

    SimpleTrackData( LHCb::Event::Track const* tr ) : SimpleTrackData( tr->lhcbIDs(), tr->firstState().qOverP() ) {}

    float qOverP() const { return m_qOverP; }
    float m_qOverP{0};
  };

} // namespace

/** @brief Kills clones of fitted tracks wrt to reference container.
 */
template <typename PrTracks>
class PrCloneKiller final : public Gaudi::Functional::Transformer<PrTracks( PrTracks const&, LHCb::Tracks const& )> {
public:
  using base_class_t = Gaudi::Functional::Transformer<PrTracks( PrTracks const&, LHCb::Tracks const& )>;
  PrCloneKiller( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t( name, pSvcLocator,
                      {typename base_class_t::KeyValue{"TracksInContainer", ""},
                       typename base_class_t::KeyValue{"TracksRefContainer", ""}},
                      typename base_class_t::KeyValue{"TracksOutContainer", ""} ) {}

  PrTracks operator()( const PrTracks& inTracks, const LHCb::Tracks& refTracks ) const override {

    auto const input_tracks = inTracks.scalar();

    // FIXME how do I simply return a copy?
    if ( refTracks.empty() ) {
      auto output = std::get<0>( input_tracks.filter( []( auto const& ) { return true; } ) );
      m_counter_selected += output.size();
      return output;
    }

    auto refdatapool = std::vector<SimpleTrackData>{refTracks.begin(), refTracks.end()};

    // those are the only ones needed for a light sequence so let's keep it simple
    constexpr bool islong = std::is_same_v<PrTracks, LHCb::Pr::Long::Tracks>;
    static_assert( islong or std::is_same_v<PrTracks, LHCb::Pr::Downstream::Tracks> );

    auto const in_type = []() {
      if constexpr ( islong ) {
        return LHCb::Event::Track::Types::Long;
      } else {
        return LHCb::Event::Track::Types::Downstream;
      }
    }();

    auto const ref_type = ( *refTracks.begin() )->type();

    // this algorithm is implemented with the assumption that all tracks
    // in the ref container are of the same type
    // inTracks can only be one type as it's a PrTracks container
    assert( std::all_of( refTracks.begin(), refTracks.end(),
                         [ref_type]( auto const* tr ) { return tr->type() == ref_type; } ) );

    auto const areClones = getCloneLambda( in_type, ref_type );

    std::vector<LHCb::LHCbID> id_vec;
    id_vec.reserve( LHCb::Pr::Long::Tracks::MaxLHCbIDs );

    auto const pred = [&refdatapool, &areClones, &id_vec]( auto const& track ) {
      id_vec.clear();
      float qop;
      if constexpr ( islong ) {
        namespace tag = LHCb::Pr::Long::Tag;
        auto nhits    = track.template get<tag::nVPHits>().cast() + track.template get<tag::nUTHits>().cast() +
                     track.template get<tag::nFTHits>().cast();
        for ( int i{0}; i < nhits; ++i ) { id_vec.emplace_back( track.template get<tag::lhcbIDs>( i ).cast() ); }
        qop = track.template get<tag::StateQoP>().cast();
      } else {
        namespace tag = LHCb::Pr::Downstream::Tag;
        auto nhits    = track.template get<tag::nUTHits>().cast() + track.template get<tag::nFTHits>().cast();
        for ( int i{0}; i < nhits; ++i ) { id_vec.emplace_back( track.template get<tag::lhcbIDs>( i ).cast() ); }
        qop = track.template get<tag::StateQoP>().cast();
      }

      std::sort( id_vec.begin(), id_vec.end() );
      auto const t = SimpleTrackData{id_vec, qop};
      return std::none_of( refdatapool.begin(), refdatapool.end(),
                           [&]( SimpleTrackData const& t2 ) { return areClones( t, t2 ); } );
    };

    auto output = std::get<0>( input_tracks.filter( pred ) );
    m_counter_selected += output.size();
    return output;
  }

  std::function<bool( SimpleTrackData const&, SimpleTrackData const& )> getCloneLambda( int const itype,
                                                                                        int const jtype ) const {
    const int offset = 256;

    switch ( itype + offset * jtype ) {
    case LHCb::Track::Types::Long + offset* LHCb::Track::Types::Long:
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        const auto dqop = it.qOverP() - jt.qOverP();
        return it.overlapFraction( jt, SimpleTrackData::T ) > m_maxOverlapFracT &&
               ( std::abs( dqop ) < m_minLongLongDeltaQoP ||
                 it.overlapFraction( jt, SimpleTrackData::VP ) > m_maxOverlapFracVelo );
      };

    case LHCb::Track::Types::Long + offset*       LHCb::Track::Types::Downstream:
    case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Long:
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        const auto dqop = it.qOverP() - jt.qOverP();
        return it.overlapFraction( jt, SimpleTrackData::T ) > m_maxOverlapFracT &&
               ( std::abs( dqop ) < m_minLongDownstreamDeltaQoP ||
                 it.overlapFraction( jt, SimpleTrackData::UT ) > m_maxOverlapFracUT );
      };

    case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Downstream:
      // it seems that there are no down stream tracks that share T hits ...
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        return it.overlapFraction( jt, SimpleTrackData::T ) > m_maxOverlapFracT &&
               it.overlapFraction( jt, SimpleTrackData::UT ) > m_maxOverlapFracUT;
      };

    case LHCb::Track::Types::Long + offset* LHCb::Track::Types::Upstream:
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        return it.overlapFraction( jt, SimpleTrackData::VP ) > m_maxOverlapFracVelo &&
               it.overlapFraction( jt, SimpleTrackData::UT ) > m_maxOverlapFracUT;
      };

    case LHCb::Track::Types::Long + offset* LHCb::Track::Types::Velo:
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        return it.overlapFraction( jt, SimpleTrackData::VP ) > m_maxOverlapFracVelo;
      };

    case LHCb::Track::Types::Long + offset*       LHCb::Track::Types::Ttrack:
    case LHCb::Track::Types::Downstream + offset* LHCb::Track::Types::Ttrack:
      return [&]( SimpleTrackData const& it, SimpleTrackData const& jt ) {
        return it.overlapFraction( jt, SimpleTrackData::T ) > m_maxOverlapFracT;
      };

    default:
      return [&]( SimpleTrackData const&, SimpleTrackData const& ) {
        throw std::runtime_error( "PrCloneKiller unhandeld switch case" );
        return false;
      };
    }
  }

private:
  Gaudi::Property<double> m_maxOverlapFracVelo{this, "MaxOverlapFracVelo", 0.5};
  Gaudi::Property<double> m_maxOverlapFracT{this, "MaxOverlapFracT", 0.5};
  Gaudi::Property<double> m_maxOverlapFracUT{this, "MaxOverlapFracUT", 0.35, "essentially: max 1 common hit"};
  Gaudi::Property<double> m_minLongLongDeltaQoP{this, "MinLongLongDeltaQoP", -1};
  Gaudi::Property<double> m_minLongDownstreamDeltaQoP{this, "MinLongDownstreamDeltaQoP", 5e-6};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_counter_selected{this, "nTracksSelected"};
};
DECLARE_COMPONENT_WITH_ID( PrCloneKiller<LHCb::Pr::Long::Tracks>, "PrCloneKillerLong" )
DECLARE_COMPONENT_WITH_ID( PrCloneKiller<LHCb::Pr::Downstream::Tracks>, "PrCloneKillerDown" )
