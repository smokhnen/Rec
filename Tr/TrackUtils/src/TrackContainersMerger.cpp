/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class TrackContainersMerger TrackContainersMerger.h
 *
 *  Merge different track containers into new one.
 *
 *  @author Andrii Usachov
 *  @date   29/05/2020
 */

#include "Event/Track.h"
#include "GaudiAlg/MergingTransformer.h"
#include <string>

template <typename TrackListType, typename TrackLists = Gaudi::Functional::vector_of_const_<TrackListType>>
struct TrackContainersMergerT final : Gaudi::Functional::MergingTransformer<TrackListType( const TrackLists& )> {
  TrackContainersMergerT( const std::string& name, ISvcLocator* pSvcLocator )
      : Gaudi::Functional::MergingTransformer<TrackListType( const TrackLists& )>(
            name, pSvcLocator, {"InputLocations", {}}, {"OutputLocation", {}} ) {}

  TrackListType operator()( const TrackLists& lists ) const override {
    TrackListType out;
    for ( const auto& list : lists ) {
      for ( const auto& track : list ) {
        // make sure the track is not yet there!
        if ( std::find( out.begin(), out.end(), track ) == out.end() ) { out.insert( new LHCb::Track( *track ) ); }
      }
    }
    return out;
  }
};

DECLARE_COMPONENT_WITH_ID( TrackContainersMergerT<LHCb::Tracks>, "TrackContainersMerger" )
