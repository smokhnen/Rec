/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class VertexListRefiner VertexListRefiner.h
 *
 *  Make a subselection of a track list
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "TrackKernel/TrackPredicates.h"
#include <string>

class VertexListRefiner : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  Gaudi::Property<std::string> m_inputLocation{this, "InputLocation", {}};
  Gaudi::Property<std::string> m_outputLocation{this, "OutputLocation", {}};
  Gaudi::Property<int>         m_minNumTracks{this, "MinNumTracks", 0};
  Gaudi::Property<int>         m_minNumBackwardTracks{this, "MinNumBackwardTracks", 0};
  Gaudi::Property<int>         m_minNumForwardTracks{this, "MinNumForwardTracks", 0};
  Gaudi::Property<int>         m_minNumLongTracks{this, "MinNumLongTracks", 0};
  Gaudi::Property<double>      m_maxChi2PerDoF{this, "MaxChi2PerDoF", -1};
  Gaudi::Property<double>      m_minX{this, "MinX", 1};
  Gaudi::Property<double>      m_maxX{this, "MaxX", -1};
  Gaudi::Property<double>      m_minY{this, "MinY", 1};
  Gaudi::Property<double>      m_maxY{this, "MaxY", -1};
  Gaudi::Property<double>      m_minZ{this, "MinZ", 1};
  Gaudi::Property<double>      m_maxZ{this, "MaxZ", -1};
  Gaudi::Property<bool>        m_deepCopy{this, "DeepCopy", false};
};

DECLARE_COMPONENT( VertexListRefiner )

StatusCode VertexListRefiner::execute() {
  LHCb::RecVertex::Range              verticesin = get<LHCb::RecVertex::Range>( m_inputLocation );
  std::vector<const LHCb::RecVertex*> verticesout;

  std::copy_if(
      verticesin.begin(), verticesin.end(), std::back_inserter( verticesout ), [&]( const LHCb::RecVertex* vertex ) {
        bool accept = ( m_maxChi2PerDoF < 0 || vertex->chi2PerDoF() < m_maxChi2PerDoF );
        if ( accept && m_minX < m_maxX ) accept = m_minX < vertex->position().x() && vertex->position().x() < m_maxX;
        if ( accept && m_minY < m_maxY ) accept = m_minY < vertex->position().y() && vertex->position().y() < m_maxY;
        if ( accept && m_minZ < m_maxZ ) accept = m_minZ < vertex->position().z() && vertex->position().z() < m_maxZ;
        if ( !accept ) return false;

        // unfortunately stl doesn't work with the smartrefs in
        // vertex. furthermore, when reading a dst, track pointers can be
        // zero.

        std::vector<const LHCb::Track*> tracks;
        tracks.reserve( vertex->tracks().size() );
        std::copy_if( vertex->tracks().begin(), vertex->tracks().end(), std::back_inserter( tracks ),
                      []( const LHCb::Track* t ) { return t != nullptr; } );

        accept = accept && int( tracks.size() ) >= m_minNumTracks;
        accept = accept && ( m_minNumLongTracks == 0 ||
                             std::count_if( tracks.begin(), tracks.end(),
                                            TrackPredicates::Type( LHCb::Track::Types::Long ) ) >= m_minNumLongTracks );

        if ( accept && ( m_minNumBackwardTracks > 0 || m_minNumForwardTracks > 0 ) ) {
          int numback =
              std::count_if( tracks.begin(), tracks.end(), TrackPredicates::Flag( LHCb::Track::Flags::Backward ) );
          int numforward = tracks.size() - numback;
          accept         = numback >= m_minNumBackwardTracks && numforward >= m_minNumForwardTracks;
        }

        return accept;
      } );

  if ( m_deepCopy.value() ) {
    LHCb::RecVertices* copies = new LHCb::RecVertices();
    put( copies, m_outputLocation );
    for ( const LHCb::RecVertex* vertex : verticesout ) copies->insert( vertex->clone() );
  } else {
    LHCb::RecVertex::Selection* selection = new LHCb::RecVertex::Selection();
    put( selection, m_outputLocation );
    for ( const LHCb::RecVertex* vertex : verticesout ) selection->insert( vertex->clone() );
  }

  return StatusCode::SUCCESS;
}
