/*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"

// boost includes
#include <boost/container/static_vector.hpp>

// std includes
#include <array>
#include <fstream>
#include <limits>
#include <vector>

/**
 * PV finding strategy:
 * step 1: select tracks with velo info and cache some information useful for PV finding
 * step 2: fill a histogram with the z of the poca to the beamline
 * step 3: do a peak search in that histogram ('vertex seeds')
 * step 4: assign tracks to the closest seed ('partitioning')
 * step 5: fit the vertices with an adapative vertex fit
 *
 *  @author Wouter Hulsbergen (Nikhef, 2018)
 **/
namespace {

  // c++20's remove_cvref
  template <typename T>
  struct remove_cvref {
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
  };

  template <typename T>
  using remove_cvref_t = typename remove_cvref<T>::type;

  inline const std::string beamSpotCond = "/dd/Conditions/Online/Velo/MotionSystem";

  using simd    = SIMDWrapper::best::types;
  using float_v = simd::float_v;
  using int_v   = simd::int_v;

  // template <typename T>
  // void iota( T& container, int size, int start ) {
  //  auto idx = simd::indices( start );
  //  for ( auto i = 0; i < size; i += simd::size ) {
  //    idx.store( &container[i] );
  //    idx = idx + simd::size;
  //  }
  //}
  /// template<class ForwardIt, class T> //c++
  /// constexpr // since C++20
  /// void iota(ForwardIt first, ForwardIt last, T value){
  ///  while(first != last) {
  ///      *first++ = value;
  ///      ++value;
  ///  }
  ///}

  template <typename T>
  auto to_std_array( T&& some_v ) {
    if constexpr ( std::is_same_v<remove_cvref_t<T>, float_v> ) {
      std::array<float, simd::size> tmp;
      some_v.store( tmp.data() );
      return tmp;
    } else if ( std::is_same_v<remove_cvref_t<T>, int_v> ) {
      std::array<int, simd::size> tmp;
      some_v.store( tmp.data() );
      return tmp;
    }
  }

  struct Beamline_t {
    double X = std::numeric_limits<double>::signaling_NaN();
    double Y = std::numeric_limits<double>::signaling_NaN();
    Beamline_t( Condition const& c )
        : X{( c.param<double>( "ResolPosRC" ) + c.param<double>( "ResolPosLA" ) ) / 2}
        , Y{c.param<double>( "ResolPosY" )} {}
  };
} // namespace

class TrackBeamLineVertexFinderSoA
    : public Gaudi::Functional::Transformer<LHCb::Event::v2::RecVertices(
                                                const EventContext&, const LHCb::Pr::Velo::Tracks&,
                                                const LHCb::Pr::Velo::Tracks&, const Beamline_t& ),
                                            LHCb::DetDesc::usesConditions<Beamline_t>> {
public:
  /// Standard constructor
  TrackBeamLineVertexFinderSoA( const std::string& name, ISvcLocator* pSvcLocator );
  /// Initialization
  StatusCode initialize() override;
  /// Execution
  LHCb::Event::v2::RecVertices operator()( const EventContext&, const LHCb::Pr::Velo::Tracks&,
                                           const LHCb::Pr::Velo::Tracks&, const Beamline_t& ) const override;

private:
  int                       m_nBins = 0;
  void                      sync_nBins() { m_nBins = ( m_zmax - m_zmin ) / m_dz; }
  Gaudi::Property<uint32_t> m_minNumTracksPerVertex{this, "MinNumTracksPerVertex", 4};
  Gaudi::Property<float>    m_zmin{this, "MinZ", -300 * Gaudi::Units::mm, [this]( auto& ) { this->sync_nBins(); },
                                "Min z position of vertex seed"};
  Gaudi::Property<float>    m_zmax{this, "MaxZ", +300 * Gaudi::Units::mm, [this]( auto& ) { this->sync_nBins(); },
                                "Max z position of vertex seed"};
  Gaudi::Property<float>    m_dz{this,
                              "ZBinSize",
                              0.25 * Gaudi::Units::mm,
                              [this]( auto& ) { this->sync_nBins(); },
                              Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                              "Z histogram bin size"};
  Gaudi::Property<float>    m_maxTrackZ0Err{this, "MaxTrackZ0Err", 1.5 * Gaudi::Units::mm,
                                         "Maximum z0-error for adding track to histo"};
  Gaudi::Property<float>    m_minDensity{this, "MinDensity", 0. / Gaudi::Units::mm,
                                      "Minimal density at cluster peak  (inverse resolution)"};
  Gaudi::Property<float>    m_minDipDensity{this, "MinDipDensity", 3.0 / Gaudi::Units::mm,
                                         "Minimal depth of a dip to split cluster (inverse resolution)"};
  Gaudi::Property<float>    m_minTracksInSeed{this, "MinTrackIntegralInSeed", 2.5};
  Gaudi::Property<float>    m_maxVertexRho{this, "BeamSpotRCut", 0.3 * Gaudi::Units::mm,
                                        "Maximum distance of vertex to beam line"};
  Gaudi::Property<uint32_t> m_maxFitIter{this, "MaxFitIter", 5, "Maximum number of iterations for vertex fit"};
  Gaudi::Property<float> m_maxDeltaChi2{this, "MaxDeltaChi2", 12, "Maximum chi2 contribution of track to vertex fit"};
  Gaudi::Property<float> m_maxVertexZErr{this, "MaxVertexZErr", 0.5f * Gaudi::Units::mm,
                                         "Maximum error on z for accepting vertex"};
  Gaudi::Property<float> m_maxTrackBLChi2{this, "MaxTrackBLChi2", 10,
                                          "Maximum chi2 of track to beam line contributing to seeds"};
  static constexpr uint16_t Nztemplatebins        = 16;
  static constexpr uint16_t Nztemplates           = 32;
  static constexpr uint16_t TemplateNormalization = 128;
  uint16_t                  m_ztemplates[2 * Nztemplates][Nztemplatebins]; // odd and even, see note
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_nbPVsCounter{this, "Nb PVs"};
};

DECLARE_COMPONENT( TrackBeamLineVertexFinderSoA )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBeamLineVertexFinderSoA::TrackBeamLineVertexFinderSoA( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
                    KeyValue{"TracksLocation", "Rec/Track/Velo"},
                    KeyValue{"BeamSpotLocation", "AlgorithmSpecific-" + name + "-beamspot"}},
                   KeyValue{"OutputVertices", LHCb::Event::v2::RecVertexLocation::Primary} ) {}

//=============================================================================
// ::initialize()
//=============================================================================
StatusCode TrackBeamLineVertexFinderSoA::initialize() {
  auto sc = Transformer::initialize().andThen(
      [&] { addConditionDerivation<Beamline_t( Condition const& )>( {beamSpotCond}, inputLocation<Beamline_t>() ); } );

  // Fill the odd templates first
  const double sqrthalf = std::sqrt( 0.5 );
  {
    // of course, since the thing is symmetric we can do this more efficiently, but that's not quite worth it now
    const double zmaxodd = m_dz * ( Nztemplatebins / 2 - 1 + 0.5 );
    const double zminodd = -zmaxodd;
    for ( int itemplate = 0; itemplate < Nztemplates; ++itemplate ) {
      const double sigmaz   = m_maxTrackZ0Err * double( itemplate + 1 ) / Nztemplates;
      double       integral = 0.5 * std::erf( sqrthalf * zminodd / sigmaz );
      for ( int ibin = 0; ibin < Nztemplatebins - 1; ++ibin ) {
        double thisintegral                   = 0.5 * std::erf( sqrthalf * ( zminodd + ( ibin + 1 ) * m_dz ) / sigmaz );
        double bincontent                     = thisintegral - integral;
        m_ztemplates[2 * itemplate + 1][ibin] = int( bincontent * TemplateNormalization );
        integral                              = thisintegral;
      }
      m_ztemplates[2 * itemplate + 1][Nztemplatebins - 1] = 0;
    }
  }

  // even templates
  {
    // of course, since the thing is symmetric we can do this more efficiently, but that's not quite worth it now
    const double zmaxeven = m_dz * Nztemplatebins / 2;
    const double zmineven = -zmaxeven;
    for ( int itemplate = 0; itemplate < Nztemplates; ++itemplate ) {
      const double sigmaz   = m_maxTrackZ0Err * double( itemplate + 1 ) / Nztemplates;
      double       integral = 0.5 * std::erf( sqrthalf * zmineven / sigmaz );
      for ( int ibin = 0; ibin < Nztemplatebins; ++ibin ) {
        double thisintegral               = 0.5 * std::erf( sqrthalf * ( zmineven + ( ibin + 1 ) * m_dz ) / sigmaz );
        double bincontent                 = thisintegral - integral;
        m_ztemplates[2 * itemplate][ibin] = int( bincontent * TemplateNormalization );
        integral                          = thisintegral;
      }
    }
  }

  return sc;
}

//=============================================================================
// ::execute()
//=============================================================================

namespace {

  namespace V2 = LHCb::v2::Event;
  // structure with minimal track info needed for PV search
  namespace PVTag {
    struct x : V2::float_field {};
    struct y : V2::float_field {};
    struct z : V2::float_field {};
    struct tx : V2::float_field {};
    struct ty : V2::float_field {};
    struct W_00 : V2::float_field {};
    struct W_11 : V2::float_field {};
    struct index : V2::int_field {};
    template <typename T>
    using pvtrack_t = V2::SOACollection<T, x, y, z, tx, ty, W_00, W_11, index>;
  } // namespace PVTag

  struct PVTracks : PVTag::pvtrack_t<PVTracks> {
    using base_t = typename PVTag::pvtrack_t<PVTracks>;
    using base_t::base_t;
  };

  inline void permute( PVTracks const& pvtracks, std::vector<int>& idxs, PVTracks& new_pvtracks ) {
    for ( int i = 0; i < static_cast<int>( pvtracks.size() ); i++ ) {
      int        idx     = idxs[i];
      const auto pvtrack = pvtracks.scalar()[idx];
      new_pvtracks.store<PVTag::x>( i, pvtrack.get<PVTag::x>() );
      new_pvtracks.store<PVTag::y>( i, pvtrack.get<PVTag::y>() );
      new_pvtracks.store<PVTag::z>( i, pvtrack.get<PVTag::z>() );
      new_pvtracks.store<PVTag::tx>( i, pvtrack.get<PVTag::tx>() );
      new_pvtracks.store<PVTag::ty>( i, pvtrack.get<PVTag::ty>() );
      new_pvtracks.store<PVTag::W_00>( i, pvtrack.get<PVTag::W_00>() );
      new_pvtracks.store<PVTag::W_11>( i, pvtrack.get<PVTag::W_11>() );
      new_pvtracks.store<PVTag::index>( i, pvtrack.get<PVTag::index>() );
    }
    // return new_pvtracks;
  }

  template <typename FTYPE>
  inline auto sqr( FTYPE x ) {
    return x * x;
  }
  //
  struct Extremum {
    Extremum( uint16_t _index, uint16_t _value, uint32_t _integral )
        : index{_index}, value{_value}, integral{_integral} {}
    uint16_t index;
    uint16_t value;
    uint32_t integral;
  };
  //
  struct Cluster {
    Cluster( uint16_t _izfirst, uint16_t _izlast, uint16_t _izmax )
        : izfirst{_izfirst}, izlast{_izlast}, izmax{_izmax} {}
    uint16_t izfirst;
    uint16_t izlast;
    uint16_t izmax;
  };

  // inline std::ostream& operator<<( std::ostream& os, Cluster const& c ) {
  //   os << "[" << c.izfirst << ", " << c.izlast << ", " << c.izmax << "]";
  //   return os;
  // }

  //
  //// Need a small extension to the track when fitting the
  //// vertex. Caching this information doesn't seem to help much
  //// though.
  namespace PVExtTag {
    // 3x2 matrix, HW_00 = W_00, HW_11 = W_11
    // sym 3x3 matrix, HWH_00 = W_00, HWH_11 = W_11,
    //                HWH_20 = HW_20, HWH_21 = HW_21,
    struct HW_20 : V2::float_field {};
    struct HW_21 : V2::float_field {};
    struct HWH_22 : V2::float_field {};
    struct weight : V2::float_field {};
    template <typename T>
    using pvext_t = V2::SOACollection<T, HW_20, HW_21, HWH_22, weight>;
  } // namespace PVExtTag

  struct PVTracksExtension : PVExtTag::pvext_t<PVTracksExtension> {
    using base_t = typename PVExtTag::pvext_t<PVTracksExtension>;
    using base_t::base_t;
  };

  //
  struct Vertex {
    using allocator_type = LHCb::Allocators::EventLocal<std::pair<unsigned, float>>;
    Gaudi::XYZPoint                                         position;
    Gaudi::SymMatrix3x3                                     poscov;
    std::vector<std::pair<unsigned, float>, allocator_type> tracks; // index to track + weight in vertex fit
    double                                                  chi2;
    Vertex( allocator_type alloc = {} ) : tracks{alloc} {}
  };

  // This implements the adapative vertex fit with Tukey's weights.
  [[gnu::pure]] inline Vertex fitAdaptive( PVTracks const& pvtracks, PVTracksExtension& trks_ext, int start, int end,
                                           const Gaudi::XYZPoint&                                         seedposition,
                                           std::vector<uint16_t, LHCb::Allocators::EventLocal<uint16_t>>& unusedtracks,
                                           uint16_t maxNumIter, float chi2max ) {

    bool                 converged = false;
    Vertex               vertex{unusedtracks.get_allocator()};
    auto&                vtxpos = vertex.position;
    Gaudi::SymMatrix3x3F vtxcov;
    vtxpos = seedposition;
    const float maxDeltaZConverged{0.001};
    float       chi2tot{0};
    uint16_t    nselectedtracks{0};
    uint16_t    iter{0};
    for ( ; iter < maxNumIter && !converged; ++iter ) {
      Gaudi::SymMatrix3x3F halfD2Chi2DX2;
      Gaudi::Vector3F      halfDChi2DX;
      nselectedtracks = 0;
      // Gaudi::Vector2F vtxposvec{float( vtxpos.x() ), float( vtxpos.y() )};
      float      vtxpos_x = vtxpos.x();
      float      vtxpos_y = vtxpos.y();
      float      vtxpos_z = vtxpos.z();
      float_v    halfD2Chi2DX2_00{0.f};
      float_v    halfD2Chi2DX2_11{0.f};
      float_v    halfD2Chi2DX2_20{0.f};
      float_v    halfD2Chi2DX2_21{0.f};
      float_v    halfD2Chi2DX2_22{0.f};
      float_v    halfDChi2DX_0{0.f};
      float_v    halfDChi2DX_1{0.f};
      float_v    halfDChi2DX_2{0.f};
      float_v    chi2_tot{0.f};
      const auto pvtrack_simd = pvtracks.simd();
      for ( int i = start; i < end; i += simd::size ) {
        auto loop_mask = simd::loop_mask( i, end );
        // compute the chi2
        float_v const dz = vtxpos_z - pvtrack_simd[i].get<PVTag::z>();
        // const Gaudi::Vector2F res  = vtxposvec - ( trk.x + dz * trk.tx );
        float_v const res_x = vtxpos_x - ( pvtrack_simd[i].get<PVTag::x>() + dz * pvtrack_simd[i].get<PVTag::tx>() );
        float_v const res_y = vtxpos_y - ( pvtrack_simd[i].get<PVTag::y>() + dz * pvtrack_simd[i].get<PVTag::ty>() );
        // float                 chi2 = ROOT::Math::Similarity( res, trk.W );
        float_v const chi2 =
            res_x * pvtrack_simd[i].get<PVTag::W_00>() * res_x + res_y * pvtrack_simd[i].get<PVTag::W_11>() * res_y;

        auto const chi2_mask = chi2 < chi2max && loop_mask;
        nselectedtracks += simd::popcount( chi2_mask );
        // Tukey's weight
        trks_ext.store<PVExtTag::weight>( i, select( chi2_mask, sqr( 1.f - chi2 / chi2max ), 0.f ), loop_mask );

        const auto trk_ext = trks_ext.simd();
        // auto loop_mask_arr = to_std_array(loop_mask);
        // halfD2Chi2DX2 += trk.weight * trk.HWH;
        halfD2Chi2DX2( 0, 0 ) +=
            ( trk_ext[i].get<PVExtTag::weight>() * pvtrack_simd[i].get<PVTag::W_00>() ).hadd( chi2_mask );
        halfD2Chi2DX2( 1, 1 ) +=
            ( trk_ext[i].get<PVExtTag::weight>() * pvtrack_simd[i].get<PVTag::W_11>() ).hadd( chi2_mask );
        halfD2Chi2DX2( 2, 0 ) +=
            ( trk_ext[i].get<PVExtTag::weight>() * trk_ext[i].get<PVExtTag::HW_20>() ).hadd( chi2_mask );
        halfD2Chi2DX2( 2, 1 ) +=
            ( trk_ext[i].get<PVExtTag::weight>() * trk_ext[i].get<PVExtTag::HW_21>() ).hadd( chi2_mask );
        halfD2Chi2DX2( 2, 2 ) +=
            ( trk_ext[i].get<PVExtTag::weight>() * trk_ext[i].get<PVExtTag::HWH_22>() ).hadd( chi2_mask );

        float_v const hw_res_x = pvtrack_simd[i].get<PVTag::W_00>() * res_x;
        float_v const hw_res_y = pvtrack_simd[i].get<PVTag::W_11>() * res_y;
        halfDChi2DX( 0 ) += ( trk_ext[i].get<PVExtTag::weight>() * hw_res_x ).hadd( chi2_mask );
        halfDChi2DX( 1 ) += ( trk_ext[i].get<PVExtTag::weight>() * hw_res_y ).hadd( chi2_mask );
        halfDChi2DX( 2 ) += ( trk_ext[i].get<PVExtTag::weight>() * ( hw_res_x * -pvtrack_simd[i].get<PVTag::tx>() +
                                                                     hw_res_y * -pvtrack_simd[i].get<PVTag::ty>() ) )
                                .hadd( chi2_mask );
        chi2_tot = chi2_tot + select( chi2_mask, trk_ext[i].get<PVExtTag::weight>() * chi2, 0.f );
        chi2tot += ( trk_ext[i].get<PVExtTag::weight>() * chi2 ).hadd( chi2_mask );
      }

      if ( nselectedtracks >= 2 ) {
        // compute the new vertex covariance
        vtxcov = halfD2Chi2DX2;
        vtxcov.InvertChol();

        // compute the delta w.r.t. the reference
        Gaudi::Vector3F delta = -1.f * vtxcov * halfDChi2DX;

        // note: this is only correct if chi2 was chi2 of reference!
        chi2tot += ROOT::Math::Dot( delta, halfDChi2DX );

        // update the position
        vtxpos.SetX( vtxpos.x() + delta( 0 ) );
        vtxpos.SetY( vtxpos.y() + delta( 1 ) );
        vtxpos.SetZ( vtxpos.z() + delta( 2 ) );
        converged = std::abs( delta( 2 ) ) < maxDeltaZConverged;
      } else {
        break;
      }
    } // end iteration loop
    for ( int irow = 0; irow < 3; ++irow )
      for ( int icol = 0; icol <= irow; ++icol ) vertex.poscov( irow, icol ) = vtxcov( irow, icol );
    vertex.chi2 = chi2tot;
    vertex.tracks.reserve( pvtracks.size() * 0.2f ); // just some number
    const auto pvtrack = pvtracks.scalar();
    const auto trk_ext = trks_ext.scalar();
    for ( int i = start; i < end; ++i ) {
      if ( trk_ext[i].get<PVExtTag::weight>().cast() > 0 )
        vertex.tracks.emplace_back( pvtrack[i].get<PVTag::index>().cast(), trk_ext[i].get<PVExtTag::weight>().cast() );
      else
        unusedtracks.push_back( pvtrack[i].get<PVTag::index>().cast() );
    }
    return vertex;
  }

  // Temporary: class to time the different steps
} // namespace

LHCb::Event::v2::RecVertices TrackBeamLineVertexFinderSoA::operator()( const EventContext&           evtCtx,
                                                                       const LHCb::Pr::Velo::Tracks& tracksBackward,
                                                                       const LHCb::Pr::Velo::Tracks& tracksForward,
                                                                       const Beamline_t&             beamline ) const {
  // Get the beamline. this only accounts for position, not
  // rotation. that's something to improve! I have considered caching
  // this (with a handle for changes in the geometry, but the
  // computation is so fast that it isn't worth it.)
  // const auto beamline = Gaudi::XYZVector{beamline.X, beamline.Y, 0};
  const Vec3<float_v> BL = Vec3<float_v>( beamline.X, beamline.Y, 0 );

  // Get the memory resource
  auto memResource = LHCb::getMemResource( evtCtx );

  // Step 1: select tracks with velo info, compute the poca to the
  // beamline. cache the covariance matrix at this position. I'd
  // rather us a combination of copy_if and transform, but don't know
  // how to do that efficiently.
  PVTracks          pvtracks;
  PVTracksExtension trks_ext;
  pvtracks.reserve( tracksForward.size() + tracksBackward.size() );
  trks_ext.reserve( tracksForward.size() + tracksBackward.size() );
  {
    const float halfwindow = ( Nztemplatebins / 2 + 1 ) * m_dz;
    const float zmin       = m_zmin + halfwindow;
    const float zmax       = m_zmax - halfwindow;

    auto fill = [&]( LHCb::Pr::Velo::Tracks const& tracks, bool const record_idx ) {
      auto const velozipped = LHCb::Pr::make_zip( tracks );
      for ( auto const& track : velozipped ) {
        auto loop_mask = track.loop_mask();

        Vec3<float_v> pos  = track.StatePos( 0 );
        Vec3<float_v> dir  = track.StateDir( 0 );
        Vec3<float_v> covX = track.StateCovX( 0 );
        Vec3<float_v> covY = track.StateCovY( 0 );

        auto P    = BL - pos;
        auto dz   = ( dir.x * P.x + dir.y * P.y ) / dir.perp2();
        auto newZ = pos.z + dz;

        auto index = pvtracks.size();
        auto mask  = zmin < newZ && newZ < zmax && loop_mask;
        pvtracks.resize( index + simd::popcount( mask ) );

        pvtracks.store<PVTag::z>( index, pos.z + dz, mask );
        pvtracks.store<PVTag::x>( index, pos.x + dz * dir.x, mask );
        pvtracks.store<PVTag::y>( index, pos.y + dz * dir.y, mask );
        pvtracks.store<PVTag::tx>( index, dir.x, mask );
        pvtracks.store<PVTag::ty>( index, dir.y, mask );
        pvtracks.store<PVTag::W_00>( index, 1.f / ( covX.x + 2 * dz * covX.y + dz * dz * covX.z ), mask );
        pvtracks.store<PVTag::W_11>( index, 1.f / ( covY.x + 2 * dz * covY.y + dz * dz * covY.z ), mask );

        if ( not record_idx ) {
          pvtracks.store<PVTag::index>( index, int_v{-1}, mask );
        } else {
          pvtracks.store<PVTag::index>( index, track.indices(), mask );
        }
      }
    };

    fill( tracksBackward, false );
    fill( tracksForward, true );
  }

  // Step 2: fill a histogram with the z position of the poca. Use the
  // projected vertex error on that position as the width of a
  // gauss. Divide the gauss properly over the bins. This is quite
  // slow: some simplification may help here.

  // we need to define what a bin is: integral between
  //   zmin + ibin*dz and zmin + (ibin+1)*dz
  // we'll have lot's of '0.5' in the code below. at some point we may
  // just want to shift the bins.

  // this can be changed into an std::accumulate
  // NOTE/TODO: As discussed in Rec#122, this could (and perhaps should) be drawn from an algorithm-local pool instead
  //            of the event-local pool used here. Alternatively, Wouter suggests that specific example could just be
  //            std::array, but this doesn't invalidate the algorithm-local idea!
  std::vector<uint16_t, LHCb::Allocators::EventLocal<uint16_t>> zhisto( m_nBins, 0, memResource );
  for ( const auto pvtrack : pvtracks.simd() ) {
    auto const loop_mask = pvtrack.loop_mask();
    // bin in which z0 is, in floating point
    auto const jbin = int_v{4 * ( pvtrack.get<PVTag::z>() - m_zmin.value() ) / m_dz.value()}; // we need factor 4 to
                                                                                              // determine odd or even
    int_v const dbin = jbin & 3;                                                              // & 3 is the same as % 4
    auto const  minbin =
        to_std_array( ( jbin >> 2 ) - Nztemplatebins / 2 + select( dbin == 0, int_v( 0 ), int_v( 1 ) ) );
    auto const oddtemplate = to_std_array( select( ( dbin == 0 ) || ( dbin == 3 ), int_v( 0 ), int_v( 1 ) ) );
    //// make sure the template fits. make sure first and last bin
    //// remain empty. there will be lot's of funny effects at edge of
    //// histogram but we do not care.

    //// about half the time is spent in logic here and computation of
    //// zerr; and the other half in the additions.
    // (tx,ty)^T * W * (tx,ty) for diagonal W
    float_v const zweight = pvtrack.get<PVTag::tx>() * pvtrack.get<PVTag::W_00>() * pvtrack.get<PVTag::tx>() +
                            pvtrack.get<PVTag::ty>() * pvtrack.get<PVTag::W_11>() * pvtrack.get<PVTag::ty>();
    float_v const zerr = 1 / sqrt( zweight ); // Q_rsqrt(zweight) ;
    //// compute the bealine chi2. we make a cut for tracks contributing to seeding
    float_v const bl_x = BL.x - pvtrack.get<PVTag::x>();
    float_v const bl_y = BL.y - pvtrack.get<PVTag::y>();
    // (bl_x, bl_y)^T * W * (bl_x, bl_y) for diagonal W
    float_v const blchi2  = bl_x * pvtrack.get<PVTag::W_00>() * bl_x + bl_y * pvtrack.get<PVTag::W_11>() * bl_y;
    auto const    zerrbin = int_v{Nztemplates / m_maxTrackZ0Err.value() * zerr};
    auto const    mask =
        to_std_array( int_v( float_v( zerrbin ) < Nztemplates && blchi2 < m_maxTrackBLChi2.value() && loop_mask ) );

    auto const zerrbin_arr = to_std_array( zerrbin );

    for ( std::size_t simd_idx = 0; simd_idx < simd::size; ++simd_idx ) {
      if ( mask[simd_idx] ) {
        for ( int j = 0; j < Nztemplatebins; ++j ) {
          zhisto[j + minbin[simd_idx]] += m_ztemplates[2 * zerrbin_arr[simd_idx] + oddtemplate[simd_idx]][j];
        }
      }
    }
  }

  ////
  ////// Step 3: perform a peak search in the histogram. This used to be
  ////// very simple but the logic needed to find 'significant dips' made
  ////// it a bit more complicated. In the end it doesn't matter so much
  ////// because it takes relatively little time.
  ////
  std::vector<Cluster, LHCb::Allocators::EventLocal<Cluster>> clusters{memResource};
  clusters.reserve( 20 );
  {
    // step A: make 'ProtoClusters'
    // Step B: for each such ProtoClusters
    //    - find the significant extrema (an odd number, start with a minimum. you can always achieve this by adding a
    //    zero bin at the beginning)
    //      an extremum is a bin-index, plus the integral till that point, plus the content of the bin
    //    - find the highest extremum and
    //       - try and partition at the lowest minimum besides it
    //       - if that doesn't work, try the other extremum
    //       - if that doesn't work, accept as cluster

    // Step A: make 'proto-clusters': these are subsequent bins with non-zero content and an integral above the
    // threshold.
    const uint32_t minTracksInSeed = m_minTracksInSeed * TemplateNormalization;
    const uint32_t mindip          = m_minDipDensity * m_dz * TemplateNormalization; // need to invent something
    const uint32_t minpeak         = m_minDensity * m_dz * TemplateNormalization;

    using BinIndex = uint16_t;
    // FIXME: how dangerous is it to use a fixed capacity vector?
    boost::container::small_vector<BinIndex, 64> clusteredges;
    // std::vector<BinIndex> clusteredges ;
    {
      bool     prevempty = true;
      uint32_t integral  = zhisto[0];
      for ( BinIndex i = 1; i < zhisto.size(); ++i ) {
        integral += zhisto[i];
        bool empty = zhisto[i] < 1;
        if ( empty != prevempty ) {
          if ( empty ) {
            if ( integral >= minTracksInSeed )
              clusteredges.emplace_back( i );
            else
              clusteredges.pop_back();
            integral = 0;
          } else {
            clusteredges.emplace_back( i - 1 );
          }
          prevempty = empty;
        }
      }
    }
    // Step B: turn these into clusters. There can be more than one cluster per proto-cluster.
    const size_t Nproto = clusteredges.size() / 2;
    for ( uint16_t i = 0; i < Nproto; ++i ) {
      const BinIndex ibegin = clusteredges[i * 2];
      const BinIndex iend   = clusteredges[i * 2 + 1];
      // find the extrema. all this complicated logic is to be able to
      // split close seeds.
      boost::container::small_vector<Extremum, 32> extrema;
      {
        bool     rising   = true;
        uint32_t integral = zhisto[ibegin];
        extrema.emplace_back( ibegin, zhisto[ibegin], integral );
        for ( uint16_t i = ibegin; i < iend; ++i ) {
          const auto value = zhisto[i];
          if ( value != zhisto[i + 1] ) {
            bool stillrising = zhisto[i + 1] > value;
            if ( stillrising != rising ) { // found a local extremum
              const uint8_t n          = extrema.size();
              const int16_t deltavalue = value - extrema.back().value;
              if ( rising ) {       // found a local maximum
                if ( n % 2 == 1 ) { // the last one was a minimum. check that this maximum is high enough. we always
                                    // accept the first maximum.
                  if ( ( n == 1 && value >= minpeak ) || deltavalue >= int( mindip ) )
                    extrema.emplace_back( i, value, integral + value / 2 );
                } else { // the last one was a maximum, but apparently there were no good minima in between
                  if ( deltavalue > 0 ) {
                    extrema.pop_back();
                    extrema.emplace_back( i, value, integral + value / 2 );
                  }
                }
              } else {              // found a local minimum
                if ( n % 2 == 0 ) { // the last one was a maximum. check that this minimum is small enough
                  if ( -1 * deltavalue >= int( mindip ) ) extrema.emplace_back( i, value, integral + 0.5f * value );
                } else { // the last one was a minimum, but apparently there were no good maxima in between
                  if ( deltavalue < 0 ) {
                    extrema.pop_back();
                    extrema.emplace_back( i, value, integral + value / 2 );
                  }
                }
              }
            }
            rising = stillrising;
          }
          integral += value;
        }
        if ( extrema.size() % 2 == 1 ) { // last was minimum. this one should replace it
          extrema.pop_back();
        }
        extrema.emplace_back( iend, zhisto[iend], integral );
      }

      // FIXME: temporary logic check
      // if( extrema.size()%2==0 ) {
      // warning() << "ERROR: even number of extrema found." << extrema.size() << endmsg ;
      //}

      if ( extrema.size() >= 3 ) {
        // now partition on  extrema
        const auto                                  N = extrema.size();
        boost::container::small_vector<Cluster, 16> subclusters;
        // std::vector<Cluster> subclusters ;
        if ( N > 3 ) {
          for ( uint32_t i = 1; i < N / 2 + 1; ++i ) {
            if ( extrema[2 * i].integral - extrema[2 * i - 2].integral > minTracksInSeed ) {
              subclusters.emplace_back( extrema[2 * i - 2].index, extrema[2 * i].index, extrema[2 * i - 1].index );
            }
          }
        }

        if ( subclusters.empty() ) {
          clusters.emplace_back( extrema.front().index, extrema.back().index, extrema[1].index );
        } else {
          // adjust the limit of the first and last to extend to the entire protocluster
          subclusters.front().izfirst = ibegin;
          subclusters.back().izlast   = iend;
          clusters.insert( std::end( clusters ), std::begin( subclusters ), std::end( subclusters ) );
        }
      }
    }
  }

  // Step 4: partition the set of tracks by vertex seed: just
  // choose the closest one. The easiest is to loop over tracks and
  // assign to closest vertex by looping over all vertices. However,
  // that becomes very slow as time is proportional to both tracks and
  // vertices. A better method is to rely on the fact that vertices
  // are sorted in z, and then use std::partition, to partition the
  // track list on the midpoint between two vertices. The logic is
  // slightly complicated to deal with partitions that have too few
  // tracks. I checked it by comparing to the 'slow' method.

  // I found that this funny weighted 'maximum' is better than most other inexpensive solutions.
  auto zClusterMean = [this, &zhisto]( auto izmax ) -> float {
    const uint16_t* b   = zhisto.data() + izmax;
    int             d1  = *b - *( b - 1 );
    int             d2  = *b - *( b + 1 );
    float           idz = d1 + d2 > 0 ? ( 0.5f * ( d1 - d2 ) ) / ( d1 + d2 ) : 0.0f;
    return m_zmin + m_dz * ( izmax + idz + 0.5f );
  };

  using seed_tuple_t = std::tuple<int, int, float>;
  std::vector<seed_tuple_t, LHCb::Allocators::EventLocal<seed_tuple_t>> seeds{memResource}; // saves pair of seed
                                                                                            // indices + z position of
                                                                                            // seed
  std::vector<int> idxs;
  idxs.resize( pvtracks.size() );

  std::iota( idxs.begin(), idxs.end(), 0 );
  if ( !clusters.empty() ) {
    auto end = idxs.begin() + pvtracks.size();
    seeds.reserve( clusters.size() );
    // auto const& zs = pvtracks.zs; //lipl
    auto pvtrack = pvtracks.scalar();
    int  iprev   = 0;
    auto it      = idxs.begin();
    for ( int i = 0; i < int( clusters.size() ) - 1; ++i ) {
      const float zmid  = m_zmin + m_dz * 0.5f * ( clusters[i].izlast + clusters[i + 1].izfirst + 1 );
      auto        newit = std::partition(
          it, end, [zmid, &pvtrack]( const int idx ) { return pvtrack[idx].get<PVTag::z>().cast() < zmid; } );
      // complicated logic to get rid of partitions that are too small, doign the least amount of work
      if ( std::distance( it, newit ) >= m_minNumTracksPerVertex ) {
        seeds.emplace_back( std::distance( idxs.begin(), it ), std::distance( idxs.begin(), newit ),
                            zClusterMean( clusters[i].izmax ) );
        iprev = i;
      } else {
        // if the partition is too small, then repartition the stuff we
        // have just isolated and assign to the previous and next. You
        // could also 'skip' this partition, but then you do too much
        // work for the next.
        if ( !seeds.empty() && newit != it ) {
          const float zmid = m_zmin + m_dz * ( clusters[iprev].izlast + clusters[i + 1].izfirst + 0.5f );
          newit            = std::partition(
              it, newit, [zmid, &pvtrack]( const int idx ) { return pvtrack[idx].get<PVTag::z>().cast() < zmid; } );
          // update the last one
          std::get<1>( seeds.back() ) = std::distance( idxs.begin(), newit );
        }
      }
      it = newit;
    }
    // Make sure to add the last partition
    if ( std::distance( it, end ) >= m_minNumTracksPerVertex ) {
      seeds.emplace_back( std::distance( idxs.begin(), it ), pvtracks.size(), zClusterMean( clusters.back().izmax ) );
    } else if ( !seeds.empty() ) {
      std::get<1>( seeds.back() ) = pvtracks.size();
    }
  }
  // reorder pvtracks with idxs
  PVTracks sorted_pvtracks;
  sorted_pvtracks.reserve( pvtracks.size() );
  sorted_pvtracks.resize( pvtracks.size() );
  permute( pvtracks, idxs, sorted_pvtracks );

  // fill the tracks extension
  trks_ext.resize( sorted_pvtracks.size() );
  for ( const auto& pvtrack : sorted_pvtracks.simd() ) {
    int i = pvtrack.offset();
    trks_ext.store<PVExtTag::weight>( i, simd::float_v{0} );
    trks_ext.store<PVExtTag::HW_20>( i, -pvtrack.get<PVTag::tx>() * pvtrack.get<PVTag::W_00>() );
    trks_ext.store<PVExtTag::HW_21>( i, -pvtrack.get<PVTag::ty>() * pvtrack.get<PVTag::W_11>() );
    auto trk_ext = trks_ext.simd();
    trks_ext.store<PVExtTag::HWH_22>( i, trk_ext[i].get<PVExtTag::HW_20>() * -pvtrack.get<PVTag::tx>() +
                                             trk_ext[i].get<PVExtTag::HW_21>() * -pvtrack.get<PVTag::ty>() );
  }

  //
  //// Step 5: perform the adaptive vertex fit for each seed.

  std::vector<Vertex, LHCb::Allocators::EventLocal<Vertex>> vertices{memResource};
  vertices.reserve( 10 );
  std::vector<uint16_t, LHCb::Allocators::EventLocal<uint16_t>> unusedtracks{memResource};
  unusedtracks.reserve( sorted_pvtracks.size() );
  std::transform( seeds.begin(), seeds.end(), std::back_inserter( vertices ), [&]( auto const& seed ) {
    return fitAdaptive( sorted_pvtracks, trks_ext, std::get<0>( seed ), std::get<1>( seed ),
                        Gaudi::XYZPoint{beamline.X, beamline.Y, std::get<2>( seed )}, unusedtracks, m_maxFitIter,
                        m_maxDeltaChi2 );
  } );

  // Steps that we could still take:
  // * remove vertices with too little tracks
  // * assign unused tracks to other vertices
  // * merge vertices that are close

  // create the output container
  LHCb::Event::v2::RecVertices recvertexcontainer{memResource};
  recvertexcontainer.reserve( vertices.size() );
  const auto maxVertexRho2 = sqr( m_maxVertexRho.value() );
  for ( const auto& vertex : vertices ) {
    const auto beamlinedx   = vertex.position.x() - beamline.X;
    const auto beamlinedy   = vertex.position.y() - beamline.Y;
    const auto beamlinerho2 = sqr( beamlinedx ) + sqr( beamlinedy );
    if ( vertex.tracks.size() >= m_minNumTracksPerVertex && beamlinerho2 < maxVertexRho2 ) {
      int   nDoF      = 2 * vertex.tracks.size() - 3;
      auto& recvertex = recvertexcontainer.emplace_back( vertex.position, vertex.poscov,
                                                         LHCb::Event::v2::Track::Chi2PerDoF{vertex.chi2 / nDoF, nDoF} );
      recvertex.setTechnique( LHCb::Event::v2::RecVertex::RecVertexType::Primary );
      // for ( const auto& dau : vertex.tracks ) recvertex.addToTracks( &( tracks[dau.first] ), dau.second );
      recvertex.reserve( vertex.tracks.size() );
      for ( const auto& dau : vertex.tracks )
        recvertex.addToTracks( nullptr, dau.second ); // TODO: make a PoD SoA RecVertex that contains indexes
    }
  }
  m_nbPVsCounter += recvertexcontainer.size();
  return recvertexcontainer;
}
