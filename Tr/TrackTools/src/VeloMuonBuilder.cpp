/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/LHCbID.h"
#include "Kernel/MuonLayout.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "math.h"
#include <memory>
#include <stdio.h>
#include <stdlib.h>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : VeloMuonBuilder
//
// 2010-09-16 : Paul Seyfert
//-----------------------------------------------------------------------------
namespace {
  static const float s_xscale[4] = {0.06f, 0.1f, 0.15f, 0.15f};
}

/** @class VeloMuonBuilder VeloMuonBuilder.h
 *
 * \brief  Make a ValoMuonTrack: match velo and muon tracks. afterwards apply kalman fit.
 *
 * Parameters:
 * - MuonLocation: where the muon stubs come from
 * -  VeloLocation: where the velo tracks come from
 * -  OutputLocation: where the velomuontracks go to
 * -  zmagnet: where the magnet's bending plane is (optimised for matching)
 * -  zmatch: where in the y-z plane matching is done
 * -  chamberhit: if for y-z matching the first muon measurement shall be used instead of zmatch
 * -  distancecut: obsolete, kept for compatibility. hard coded.
 * -  lhcbids: obsolete used for elaborate chi^2 fit in the muon stations with unknown hit pattern
 *             not applicable for existing muon algorithms but for private code
 * -  cut: if the hard coded distancecut shall be scaled
 * -  MaxVeloTracks: if there are more velo tracks, don't do anything (saving cpu time in hopeless events)
 * -  MaxMuonTracks: if there are more muon tracks, don't do anything (saving cpu time in hopeless events)
 *
 *  @author Paul Seyfert
 *  @date   2010-09-16
 */

class VeloMuonBuilder : public GaudiTupleAlg {

  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode    initialize() override;
  StatusCode    execute() override;
  StatusCode    buildVeloMuon( LHCb::Tracks& veloTracks, LHCb::Tracks& muonTracks, LHCb::Tracks* trackvector );
  LHCb::Tracks* buildVeloMuon( LHCb::Tracks& veloTracks, LHCb::Tracks& muonTracks );

private:
  Gaudi::Property<float>       m_distcutmultiplyer{this, "cut", 1};
  Gaudi::Property<bool>        m_chamberhit{this, "chamberhit", true};
  Gaudi::Property<double>      m_zmagnet{this, "zmagnet", 5400.};
  Gaudi::Property<float>       m_zmatch{this, "zmatch", 15000.};
  Gaudi::Property<std::string> m_muonpath{this, "MuonLocation", "Hlt1/Tracks/MuonSeg"};
  Gaudi::Property<std::string> m_velopath{this, "VeloLocation", LHCb::TrackLocation::Velo};
  Gaudi::Property<std::string> m_output{this, "OutputLocation", "Rec/Track/VeloMuon"};

  Gaudi::Property<unsigned int> m_maxmuons{this, "MaxMuonTracks", 30};
  Gaudi::Property<unsigned int> m_maxvelos{this, "MaxVeloTracks", 1000};

  Gaudi::Property<int> n_lhcbids{this, "lhcbids", 4};

  IMuonFastPosTool*   m_iPosTool           = nullptr;
  ITrackExtrapolator* m_linearextrapolator = nullptr;
  ITrackFitter*       m_tracksFitter       = nullptr;
  ILHCbMagnetSvc*     m_magFieldSvc        = nullptr;
};

DECLARE_COMPONENT( VeloMuonBuilder )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VeloMuonBuilder::initialize() {
  return GaudiTupleAlg::initialize().andThen( [&] {
    m_iPosTool           = tool<IMuonFastPosTool>( "MuonFastPosTool" );
    m_linearextrapolator = tool<ITrackExtrapolator>( "TrackLinearExtrapolator" );
    m_tracksFitter       = tool<ITrackFitter>( "TrackMasterFitter", this );
    m_magFieldSvc        = svc<ILHCbMagnetSvc>( "MagneticFieldSvc", true );
  } );
}

using namespace LHCb;
//=============================================================================
// Main execution
//=============================================================================
StatusCode VeloMuonBuilder::execute() {

  LHCb::Tracks* veloTracks = getIfExists<Tracks>( m_velopath );
  if ( !veloTracks ) return StatusCode::SUCCESS;

  if ( veloTracks->size() > m_maxvelos ) return StatusCode::SUCCESS;

  LHCb::Tracks* muonTracks = getIfExists<Tracks>( m_muonpath );
  if ( !muonTracks ) return StatusCode::SUCCESS;

  if ( muonTracks->size() > m_maxmuons ) return StatusCode::SUCCESS;

  //  LinkedTo<MCParticle,Track> veloLink( evtSvc(), msgSvc(), m_velopath );
  Tracks* tracks = getIfExists<Tracks>( m_output );
  if ( !tracks ) {
    tracks = new Tracks();
    put( tracks, m_output );
  }

  buildVeloMuon( *veloTracks, *muonTracks, tracks ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Method kept for compatibility
//=============================================================================
Tracks* VeloMuonBuilder::buildVeloMuon( Tracks& veloTracks, Tracks& muonTracks ) {
  auto       trackvector = std::make_unique<Tracks>();
  StatusCode sc          = buildVeloMuon( veloTracks, muonTracks, trackvector.get() );
  return sc.isSuccess() ? trackvector.release() : nullptr;
}

//=============================================================================
// Reconstruction of VeloMuon tracks
//=============================================================================
StatusCode VeloMuonBuilder::buildVeloMuon( Tracks& veloTracks, Tracks& muonTracks, Tracks* trackvector ) {
  StatusCode sc = StatusCode::SUCCESS;

  for ( auto muonIter = muonTracks.begin(); muonIter != muonTracks.end(); ++muonIter ) {

    Gaudi::XYZPoint chamber = ( *muonIter )->position(); // this is at the first state
    Gaudi::XYZPoint muonpunktx, muonpunkty;
    if ( m_chamberhit ) {
      muonpunkty = chamber;
    } else {
      sc = m_linearextrapolator->position( *( *muonIter ), m_zmatch, muonpunkty, LHCb::Tr::PID::Muon() );
      if ( sc.isFailure() ) continue;
    }
    sc = m_linearextrapolator->position( *( *muonIter ), m_zmagnet, muonpunktx, LHCb::Tr::PID::Muon() );
    if ( sc.isFailure() ) continue;

    int reg = ( *muonIter )->lhcbIDs()[2].muonID().region();

    // go through the velos
    float                  minweight = std::numeric_limits<float>::infinity();
    float                  mindist   = std::numeric_limits<float>::infinity();
    float                  minreg    = -1;
    std::unique_ptr<Track> goodCopy;
    for ( auto veloIter = veloTracks.begin(); veloIter != veloTracks.end(); ++veloIter ) {
      if ( ( *veloIter )->history() != LHCb::Track::History::PatVelo &&
           ( *veloIter )->history() != LHCb::Track::History::PatVeloGeneral &&
           ( *veloIter )->history() != LHCb::Track::History::PatFastVelo )
        continue;

      Gaudi::XYZPoint velopunktx, velopunkty;
      sc = m_linearextrapolator->position( *( *veloIter ), m_zmagnet, velopunktx, LHCb::Tr::PID::Muon() );
      if ( sc.isFailure() ) continue;
      if ( m_chamberhit ) {
        sc = m_linearextrapolator->position( *( *veloIter ), chamber.z(), velopunkty, LHCb::Tr::PID::Muon() );
      } else {
        sc = m_linearextrapolator->position( *( *veloIter ), m_zmatch, velopunkty, LHCb::Tr::PID::Muon() );
      }
      if ( sc.isFailure() ) continue;

      // now calculate distance
      float weighteddistance =
          float( ( velopunktx.x() - muonpunktx.x() ) * ( velopunktx.x() - muonpunktx.x() ) * s_xscale[reg] +
                 ( 1 - s_xscale[reg] ) * ( velopunkty.y() - muonpunkty.y() ) * ( velopunkty.y() - muonpunkty.y() ) );

      double distancecut;
      // -- hard coded after determination on private ntuple
      switch ( reg ) {
      case 0:
        distancecut = 30 * 30;
        break; // 100;
      case 1:
        distancecut = 60 * 60;
        break; // 200;
      case 2:
        distancecut = 110 * 110;
        break; // 9000;
      case 3:
        distancecut = 200 * 200;
        break; // 38000;
      default:
        distancecut = 0;
      }

      distancecut *= m_distcutmultiplyer;
      if ( weighteddistance > distancecut ) continue;

      std::unique_ptr<LHCb::Track> aCopy;
      {
        auto        monitorstate = std::make_unique<LHCb::State>( ( *veloIter )->firstState() );
        float       xkick = (float)( chamber.x() - monitorstate->x() ); // jstefaniak used interpolated value here
        const float m_ptkickConstant = 1265.f;
        float       qp               = float( xkick / m_ptkickConstant / ( (float)chamber.z() - m_zmagnet ) );
        qp                           = qp * m_magFieldSvc->signedRelativeCurrent();
        aCopy                        = std::make_unique<LHCb::Track>();
        aCopy->addToAncestors( *veloIter );
        aCopy->addToAncestors( *muonIter );
        std::vector<LHCb::State*> velostates = ( *veloIter )->states();
        std::vector<LHCb::State*> copiedstates;
        for ( auto stateiter = velostates.begin(); stateiter != velostates.end(); ++stateiter ) {
          copiedstates.push_back( new LHCb::State{**stateiter} );
          copiedstates.back()->setQOverP( qp );
          Gaudi::TrackSymMatrix cov;
          cov( 0, 0 ) = 1.f;
          cov( 1, 1 ) = 1.f;
          cov( 2, 2 ) = 1.f;
          cov( 3, 3 ) = 1.f;
          cov( 4, 4 ) = qp * qp * 0.15 * 0.15;
          copiedstates.back()->setCovariance( cov );
        }
        aCopy->addToStates( copiedstates );
        aCopy->addToLhcbIDs( ( *veloIter )->lhcbIDs() );
        copiedstates.clear();
        Gaudi::TrackSymMatrix cov;
        cov( 0, 0 ) = 1.f;
        cov( 1, 1 ) = 1.f;
        cov( 2, 2 ) = 1.f;
        cov( 3, 3 ) = 1.f;
        cov( 4, 4 ) = qp * qp * 0.15 * 0.15;
        for ( const auto& stateiter : ( *muonIter )->states() ) {
          copiedstates.push_back( new LHCb::State{*stateiter} );
          copiedstates.back()->setQOverP( qp );
          copiedstates.back()->setCovariance( cov );
        }

        aCopy->addToStates( copiedstates );
        aCopy->addToLhcbIDs( ( *muonIter )->lhcbIDs() ); // the other ids -- insert them here
        if ( sc.isFailure() ) { continue; }
        // m_linearextrapolator->propagate(*monitorstate, chamber.z(),LHCb::Tr::PID::Muon());

        aCopy->firstState().setQOverP( qp );

        aCopy->setFitStatus( LHCb::Track::FitStatus::FitStatusUnknown );
        aCopy->setFitHistory( LHCb::Track::FitHistory::FitUnknown );

        TrackFitResult* result = new TrackFitResult();
        if ( 0.f == qp ) {
          result->setPScatter( 1 / qp ); // -- needed in fitter
        } else {
          result->setPScatter( 1e6 ); // straight track => p = 1TeV
        }

        aCopy->setFitResult( result );

        aCopy->setType( LHCb::Track::Types::Long );
        // sc = m_tracksFitter->fit(*aCopy,LHCb::Tr::PID::Muon());
        if ( sc.isFailure() ) { continue; }

        aCopy->clearAncestors();
        // -- unfortunately this will not be stored on DST :(
        aCopy->addToAncestors( *veloIter );
        aCopy->addToAncestors( *muonIter );
      }

      float weight = weighteddistance;
      if ( weight < minweight ) {
        // -- if we created a Track and want to use another one. then we should delete the old one
        minweight = weight;
        mindist   = weighteddistance;
        minreg    = reg;
        goodCopy  = std::move( aCopy );
      }
    }
    if ( minweight == std::numeric_limits<float>::infinity() ) {
      continue; // -- nothing was found
    }

    goodCopy->addInfo( 4444, 1 );
    goodCopy->addInfo( 4445, mindist );
    goodCopy->addInfo( 4446, minreg );
    sc = m_tracksFitter->operator()( *goodCopy, LHCb::Tr::PID::Muon() );

    trackvector->add( goodCopy.release() );
  }
  return StatusCode::SUCCESS;
}
