/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Material.h"
#include "Event/State.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "TrackInterfaces/IStateCorrectionTool.h"
#include "vdt/exp.h"
#include "vdt/log.h"
#include <algorithm>
#include <cmath>
#include <unordered_map>

using namespace Gaudi::Units;

namespace {
  // tell gcc/clang/icc that vdt_pow has no side-effects whatsoever
  double vdt_pow( const double x, const double y ) noexcept __attribute__( ( const ) );
  double vdt_pow( const double x, const double y ) noexcept { return vdt::fast_exp( y * vdt::fast_log( x ) ); }

  /// stupid little helper for converting scoped enums to unterlying integer type
  template <typename T>
  static constexpr typename std::underlying_type<T>::type toi( T value ) {
    return static_cast<typename std::underlying_type<T>::type>( value );
  }
} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : StateDetailedBetheBlochEnergyCorrectionTool
//
// 2006-08-18 : Eduardo Rodrigues
//-----------------------------------------------------------------------------

/** @class StateDetailedBetheBlochEnergyCorrectionTool
 *
 *  This state correction tool applies a dE/dx energy loss correction
 *  with the full version of the Bethe-Bloch equation.
 *
 *  @author Stephanie Hansmann-Menzemer
 *  @date   2008-05-02
 *
 */
class StateDetailedBetheBlochEnergyCorrectionTool : public extends<GaudiTool, IStateCorrectionTool> {
public:
  /// Standard constructor
  using extends::extends;

  /// Correct a State for dE/dx energy losses with a simplified Bethe-Bloch equiaton
  void correctState( LHCb::State& state, const Material* material, std::any& cache, double wallThickness, bool upstream,
                     double mass ) const override;

  std::any createBuffer() const override { return MaterialCache(); }

private:
  // Job options
  Gaudi::Property<double> m_energyLossCorr{this, "EnergyLossFactor", 1.0}; ///< tunable energy loss correction
  Gaudi::Property<double> m_maxEnergyLoss{this, "MaximumEnergyLoss",
                                          100. * Gaudi::Units::MeV}; ///< maximum energy loss in dE/dx correction

  enum class Mat { X0, C, X1, a, m, DensityFactor, LogI };
  typedef std::unordered_map<const Material*, std::tuple<double, double, double, double, double, double, double>>
      Material2FactorMap;

  struct MaterialCache {
    Material2FactorMap           mat2factors;
    Material2FactorMap::iterator lastCachedMaterial = std::end( mat2factors );
  };
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( StateDetailedBetheBlochEnergyCorrectionTool )

//=============================================================================
// Correct a State for dE/dx energy losses
//=============================================================================
void StateDetailedBetheBlochEnergyCorrectionTool::correctState( LHCb::State& state, const Material* material,
                                                                std::any& cache, double wallThickness, bool upstream,
                                                                const double mass ) const {
  // mass zero is quite UNLIKELY here, and we'll save a lot of computation in
  // that case
  if ( UNLIKELY( 0 == mass ) ) return;

  MaterialCache matCache           = std::any_cast<MaterialCache&>( cache );
  auto&         lastCachedMaterial = matCache.lastCachedMaterial;
  auto&         material2factors   = matCache.mat2factors;

  // material lookups are slightly different: the "hot spot" occupied by the
  // last material used is likely to be missed (about 85% of the time), but if
  // we can save the hash by looking at the last used element first, that's a
  // good thing
  if ( LIKELY( std::end( material2factors ) == lastCachedMaterial || lastCachedMaterial->first != material ) ) {
    lastCachedMaterial = material2factors.find( material );
    // cache material quantities:
    // - X0, C, X1, a, m
    // - const. * Z / A * density ("DensityFactor")
    // - log(I)
    // we trade nine virtual function calls plus associated memory accesses, four
    // floating point operations and a logarithm for a lookup
    if ( UNLIKELY( std::end( material2factors ) == lastCachedMaterial ) ) {
      // coming here is the very rare exception - after the first couple of
      // events, the material cache miss rate is essentially zero
      lastCachedMaterial =
          material2factors
              .insert( std::make_pair(
                  material,
                  std::make_tuple( material->X0(), material->C(), material->X1(), material->a(), material->m(),
                                   ( 30.71 * MeV * mm2 / mole ) * material->Z() * material->density() / material->A(),
                                   std::log( material->I() ) ) ) )
              .first;
    }
  }
  const auto& mat = lastCachedMaterial->second;

  // apply correction - note: for now only correct the state vector
  auto& qOverP = state.stateVector()[4];

  const auto eta2_inv = ( qOverP * mass ) * ( qOverP * mass );
  const auto x4       = -vdt::fast_log( eta2_inv );
  const auto x        = x4 / 4.606;

  double rho_2 = 0;
  if ( LIKELY( x > std::get<toi( Mat::X0 )>( mat ) ) ) {
    // branch taken in about 3/4 of cases
    rho_2 = x4 - std::get<toi( Mat::C )>( mat );
    if ( LIKELY( x < std::get<toi( Mat::X1 )>( mat ) ) ) {
      // branch taken in about 9/10 cases
      rho_2 += std::get<toi( Mat::a )>( mat ) *
               vdt_pow( std::get<toi( Mat::X1 )>( mat ) - x, std::get<toi( Mat::m )>( mat ) );
    }
    rho_2 *= 0.5;
  }
  static const auto log_2_me = std::log( 2 * Gaudi::Units::electron_mass_c2 );
  // correct wall thickness for angle of incidence
  const auto tx = state.tx(), ty = state.ty();
  wallThickness *= std::sqrt( 1 + tx * tx + ty * ty );
  const auto beta2_inv = 1 + eta2_inv;
  auto       eLoss     = m_energyLossCorr * wallThickness * std::get<toi( Mat::DensityFactor )>( mat ) *
               ( beta2_inv * ( log_2_me + x4 - std::get<toi( Mat::LogI )>( mat ) - rho_2 ) - 1 );

  eLoss = std::copysign( std::min( m_maxEnergyLoss.value(), eLoss ), ( 2 * int( upstream ) - 1 ) * qOverP );

  qOverP /= 1 + eLoss * qOverP;
}

//=============================================================================
