/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FTCluster.h"
#include "Event/GhostTrackInfo.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "Event/UTCluster.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/DataHandle.h"
#include "GaudiKernel/IIncidentListener.h"
#include "Kernel/HitPattern.h"
#include "Linker/LinkerTool.h"
#include "PrKernel/UTHitHandler.h"
#include "TMVA/TMVA_1_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_3_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_4_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_5_25nsLL_2017_MLP.class.C"
#include "TMVA/TMVA_6_25nsLL_2017_MLP.class.C"
#include "TMVA/UpdateFlattenDownstream.C"
#include "TMVA/UpdateFlattenLong.C"
#include "TMVA/UpdateFlattenTtrack.C"
#include "TMVA/UpdateFlattenUpstream.C"
#include "TMVA/UpdateFlattenVelo.C"
#include "TMath.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackKernel/TrackFunctors.h"

//-----------------------------------------------------------------------------
// Implementation file for class : UpgradeGhostId
//
// 2014-12-30 : Paul Seyfert following an earlier version by Angelo Di Canto
// 2019-06-18 : Menglin Xu
//-----------------------------------------------------------------------------

/** @class UpgradeGhostId UpgradeGhostId.h
 *
 *  @author Paul Seyfert
 *  @date   30-12-2014
 */
class UpgradeGhostId : public extends<GaudiTool, IGhostProbability> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode finalize() override;   ///< Algorithm initialization
  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute( LHCb::Track& aTrack ) const override;
  StatusCode beginEvent() override { return StatusCode::SUCCESS; };
  /** reveal the variable names for a track type */
  std::vector<std::string> variableNames( LHCb::Track::Types type ) const override;

  /** reveal the variable values for a track */
  std::vector<float> netInputs( LHCb::Track& ) const override {
    fatal() << "UpgradeGhostId::netInputs is NOT IMPLEMENTED" << endmsg;
    return std::vector<float>();
  }

private:
  std::vector<std::unique_ptr<IClassifierReader>>          m_readers;
  std::vector<std::unique_ptr<Track::TabulatedFunction1D>> m_flatters;

  std::vector<IHitExpectation*>             m_Expectations;
  std::vector<std::string>                  m_inNames;
  Gaudi::Property<std::vector<std::string>> m_expectorNames{
      this, "Expectors", {"UTHitExpectation", "FTHitExpectation"}};

  DataObjectReadHandle<LHCb::VPLightClusters> m_vpClusters{this, "VPClusterLocation", LHCb::VPClusterLocation::Light,
                                                           "Location of VP light clusters"};
  DataObjectReadHandle<UT::HitHandler>        m_utClusters{this, "UTClusterLocation", UT::Info::HitLocation,
                                                    "Location of UT clusters"};
};

DECLARE_COMPONENT( UpgradeGhostId )

//=============================================================================
//

namespace {
  static const int largestTrackTypes = 1 + LHCb::Track::Types::Ttrack;

  static const std::vector<std::string> veloVars = {"UpgradeGhostInfo_obsVP",
                                                    "UpgradeGhostInfo_FitVeloChi2",
                                                    "UpgradeGhostInfo_FitVeloNDoF",
                                                    "UpgradeGhostInfo_veloHits",
                                                    "UpgradeGhostInfo_utHits",
                                                    "TRACK_CHI2",
                                                    "TRACK_NDOF",
                                                    "TRACK_ETA"};

  static const std::vector<std::string> upstreamVars = {"UpgradeGhostInfo_obsVP",
                                                        "UpgradeGhostInfo_FitVeloChi2",
                                                        "UpgradeGhostInfo_FitVeloNDoF",
                                                        "UpgradeGhostInfo_obsUT",
                                                        "UpgradeGhostInfo_UToutlier",
                                                        "UpgradeGhostInfo_veloHits",
                                                        "UpgradeGhostInfo_utHits",
                                                        "TRACK_CHI2",
                                                        "TRACK_NDOF",
                                                        "TRACK_PT",
                                                        "TRACK_ETA"};

  static const std::vector<std::string> downstreamVars = {"UpgradeGhostInfo_obsFT",
                                                          "UpgradeGhostInfo_FitTChi2",
                                                          "UpgradeGhostInfo_FitTNDoF",
                                                          "UpgradeGhostInfo_obsUT",
                                                          "UpgradeGhostInfo_UToutlier",
                                                          "UpgradeGhostInfo_veloHits",
                                                          "UpgradeGhostInfo_utHits",
                                                          "TRACK_CHI2",
                                                          "TRACK_PT",
                                                          "TRACK_ETA"};

  static const std::vector<std::string> longVars = {"UpgradeGhostInfo_obsVP",
                                                    "UpgradeGhostInfo_FitVeloChi2",
                                                    "UpgradeGhostInfo_FitVeloNDoF",
                                                    "UpgradeGhostInfo_obsFT",
                                                    "UpgradeGhostInfo_FitTChi2",
                                                    "UpgradeGhostInfo_FitTNDoF",
                                                    "UpgradeGhostInfo_obsUT",
                                                    "UpgradeGhostInfo_FitMatchChi2",
                                                    "UpgradeGhostInfo_UToutlier",
                                                    "UpgradeGhostInfo_veloHits",
                                                    "UpgradeGhostInfo_utHits",
                                                    "TRACK_CHI2",
                                                    "TRACK_PT",
                                                    "TRACK_ETA"};

  static const std::vector<std::string> ttrackVars = {"UpgradeGhostInfo_obsFT",
                                                      "UpgradeGhostInfo_FitTChi2",
                                                      "UpgradeGhostInfo_FitTNDoF",
                                                      "UpgradeGhostInfo_veloHits",
                                                      "UpgradeGhostInfo_utHits",
                                                      "TRACK_CHI2",
                                                      "TRACK_NDOF",
                                                      "TRACK_PT",
                                                      "TRACK_ETA"};

} // namespace

StatusCode UpgradeGhostId::finalize() {

  std::for_each( m_readers.begin(), m_readers.end(), []( auto& up ) { up.reset(); } );
  std::for_each( m_flatters.begin(), m_flatters.end(), []( auto& up ) { up.reset(); } );
  return GaudiTool::finalize();
}

StatusCode UpgradeGhostId::initialize() {
  if ( !GaudiTool::initialize() ) return StatusCode::FAILURE;

  if ( largestTrackTypes <=
       std::max( LHCb::Track::Types::Ttrack,
                 std::max( std::max( LHCb::Track::Types::Velo, LHCb::Track::Types::Upstream ),
                           std::max( LHCb::Track::Types::Ttrack, LHCb::Track::Types::Downstream ) ) ) )
    return Warning( "ARRAY SIZE SET WRONG (largestTrackTypes is smaller than enum LHCb::Track::Types",
                    StatusCode::FAILURE );

  m_readers.clear();
  m_readers.resize( largestTrackTypes );
  m_readers[LHCb::Track::Types::Velo]       = std::make_unique<ReadMLP_1>( veloVars );
  m_readers[LHCb::Track::Types::Upstream]   = std::make_unique<ReadMLP_4>( upstreamVars );
  m_readers[LHCb::Track::Types::Downstream] = std::make_unique<ReadMLP_5>( downstreamVars );
  m_readers[LHCb::Track::Types::Long]       = std::make_unique<ReadMLP_3>( longVars );
  m_readers[LHCb::Track::Types::Ttrack]     = std::make_unique<ReadMLP_6>( ttrackVars );

  m_flatters.clear();
  m_flatters.resize( largestTrackTypes );
  m_flatters[LHCb::Track::Types::Velo]       = Update_VeloTable();
  m_flatters[LHCb::Track::Types::Upstream]   = Update_UpstreamTable();
  m_flatters[LHCb::Track::Types::Downstream] = Update_DownstreamTable();
  m_flatters[LHCb::Track::Types::Long]       = Update_LongTable();
  m_flatters[LHCb::Track::Types::Ttrack]     = Update_TtrackTable();

  return StatusCode::SUCCESS;
}

namespace {
  class SubDetHits {
    std::array<int, 3>   m_hits = {};
    constexpr static int idx( LHCb::LHCbID::channelIDtype t ) {
      switch ( t ) {
      case LHCb::LHCbID::channelIDtype::VP:
        return 0;
      case LHCb::LHCbID::channelIDtype::UT:
        return 1;
      case LHCb::LHCbID::channelIDtype::FT:
        return 2;
      default:
        throw;
      }
    }
    static constexpr bool validType( LHCb::LHCbID::channelIDtype t ) {
      return t == LHCb::LHCbID::channelIDtype::VP || t == LHCb::LHCbID::channelIDtype::UT ||
             t == LHCb::LHCbID::channelIDtype::FT;
    }

  public:
    SubDetHits( const LHCb::Track& aTrack ) {
      for ( auto lhcbid : aTrack.lhcbIDs() ) {
        if ( !validType( lhcbid.detectorType() ) ) continue; // may be a hit in a non-tracking detector
        ++m_hits[idx( lhcbid.detectorType() )];
      }
    }

    double operator[]( LHCb::LHCbID::channelIDtype t ) const { return m_hits[idx( t )]; }
  };
} // namespace

//=============================================================================
StatusCode UpgradeGhostId::execute( LHCb::Track& aTrack ) const {
  auto                        obsarray = SubDetHits{aTrack};
  const LHCb::TrackFitResult* fit      = fitResult( aTrack );

  std::vector<double> variables;
  variables.reserve( 15 );
  // if (LHCb::Track::Types::Velo == tracktype || LHCb::Track::Types::Long == tracktype || LHCb::Track::Types::Upstream
  // == tracktype) {
  if ( aTrack.hasVelo() ) {
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::VP] );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitVeloChi2, -999 ) );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitVeloNDoF, -999 ) );
  }
  // if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype ||
  // LHCb::Track::Types::Ttrack == tracktype) {
  if ( aTrack.hasT() ) {
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::FT] );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitTChi2, -999 ) );
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitTNDoF, -999 ) );
  }
  // if (LHCb::Track::Types::Downstream == tracktype || LHCb::Track::Types::Long == tracktype ||
  // LHCb::Track::Types::Upstream == tracktype) {
  if ( aTrack.hasUT() ) { // includes longtracks w/o ut hits
    variables.push_back( obsarray[LHCb::LHCbID::channelIDtype::UT] );
  }
  if ( LHCb::Track::Types::Long == aTrack.type() ) {
    variables.push_back( aTrack.info( LHCb::Track::AdditionalInfo::FitMatchChi2, -999 ) );
  }
  if ( aTrack.hasUT() ) {
    variables.push_back(
        fit ? ( fit->nMeasurements<LHCb::Measurement::UT>() - fit->nActiveMeasurements<LHCb::Measurement::UT>() )
            : 0 ); // "UpgradeGhostInfo_UToutlier",'F'
  }
  variables.push_back( m_vpClusters.get()->size() );
  variables.push_back( m_utClusters.get()->nbHits() );
  variables.push_back( aTrack.chi2() );
  if ( LHCb::Track::Types::Long != aTrack.type() && LHCb::Track::Types::Downstream != aTrack.type() ) {
    variables.push_back( aTrack.nDoF() );
  }
  if ( LHCb::Track::Types::Velo != aTrack.type() ) { variables.push_back( aTrack.pt() ); }
  variables.push_back( aTrack.pseudoRapidity() );

  // float netresponse = m_readers[aTrack.type()]->GetRarity(variables); // TODO rarity would be nice, see
  // https://sft.its.cern.ch/jira/browse/ROOT-7050
  float netresponse = m_readers[aTrack.type()]->GetMvaValue( variables );
  netresponse       = m_flatters[aTrack.type()]->value( netresponse );
  aTrack.setGhostProbability( 1. - netresponse );

  return StatusCode::SUCCESS;
}

std::vector<std::string> UpgradeGhostId::variableNames( LHCb::Track::Types type ) const {
  switch ( type ) {
  case LHCb::Track::Types::Velo:
    return veloVars;
  case LHCb::Track::Types::Long:
    return longVars;
  case LHCb::Track::Types::Upstream:
    return upstreamVars;
  case LHCb::Track::Types::Downstream:
    return downstreamVars;
  case LHCb::Track::Types::Ttrack:
    return ttrackVars;
  default:
    return {};
  }
}
