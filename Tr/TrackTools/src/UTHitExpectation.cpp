/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class UTHitExpectation UTHitExpectation.h
 *
 * Implementation of UTHitExpectation tool
 * see interface header for description
 *
 *  @author M.Needham
 *  @date   22/5/2007
 */

#include "Event/State.h"
#include "Event/StateParameters.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "LHCbMath/GeomFun.h"
#include "Line3D.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSensor.h"
#include <algorithm>
#include <string>

using namespace LHCb;
using namespace Gaudi;

class UTHitExpectation : public extends<GaudiTool, IHitExpectation> {

public:
  /** constructer */
  using extends::extends;

  /** intialize */
  StatusCode initialize() override;

  /** Returns number of hits expected, from zFirst to inf
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return number of hits expected
   */
  unsigned int nExpected( const LHCb::Track& aTrack ) const override;

  /** Returns number of hits expected
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return Info info including likelihood
   */
  IHitExpectation::Info expectation( const LHCb::Track& aTrack ) const override;

  /** Collect all the expected hits
   *
   * @param aTrack Reference to the Track to test
   * @param hits collected lhcbIDs
   *
   **/
  void collect( const LHCb::Track& aTrack, std::vector<LHCb::LHCbID>& ids ) const override;

private:
  void collectHits( std::vector<LHCb::UTChannelID>& chans, LHCb::StateVector stateVec,
                    const unsigned int station ) const;

  bool insideSensor( const DeUTSensor* sensor, const Tf::Tsa::Line3D& line ) const;

  Gaudi::XYZPoint intersection( const Tf::Tsa::Line3D& line, const Gaudi::Plane3D& aPlane ) const;

  bool select( const LHCb::UTChannelID& chan ) const;

  IUTChannelIDSelector* m_selector     = nullptr;
  ITrackExtrapolator*   m_extrapolator = nullptr;
  DeUTDetector*         m_utDet        = nullptr;
  double                m_zUTa;
  double                m_zUTb;

  Gaudi::Property<std::string> m_selectorType{this, "SelectorType", "UTSelectChannelIDByElement"};
  Gaudi::Property<std::string> m_extrapolatorName{this, "extrapolatorName", "TrackParabolicExtrapolator"};
  Gaudi::Property<std::string> m_selectorName{this, "SelectorName", "ALL"};
  Gaudi::Property<bool>        m_allStrips{this, "allStrips", false};
};

DECLARE_COMPONENT( UTHitExpectation )

//=============================================================================
//
//=============================================================================

bool UTHitExpectation::insideSensor( const DeUTSensor* sensor, const Tf::Tsa::Line3D& line ) const {

  bool            isIn = false;
  Gaudi::XYZPoint point;
  double          mu;
  if ( Gaudi::Math::intersection( line, sensor->plane(), point, mu ) == true ) {
    isIn = sensor->globalInActive( point );
  }
  return isIn;
}

bool UTHitExpectation::select( const LHCb::UTChannelID& chan ) const {
  return m_selector == 0 ? true : m_selector->select( chan );
}

StatusCode UTHitExpectation::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  m_extrapolator = tool<ITrackExtrapolator>( m_extrapolatorName );
  m_utDet        = getDet<DeUTDetector>( DeUTDetLocation::location() );

  if ( m_utDet->nStation() != 2u ) { return Error( "2 stations needed in UT", StatusCode::FAILURE ); }

  const DeUTDetector::Stations& utStations = m_utDet->stations();
  m_zUTa                                   = utStations[0]->globalCentre().z();
  m_zUTb                                   = utStations[1]->globalCentre().z();

  // (selector
  if ( m_selectorName != "ALL" ) m_selector = tool<IUTChannelIDSelector>( m_selectorType, m_selectorName );

  return StatusCode::SUCCESS;
}

IHitExpectation::Info UTHitExpectation::expectation( const LHCb::Track& aTrack ) const {

  IHitExpectation::Info info;
  info.likelihood = 0.0;
  info.nFound     = 0;
  info.nExpected  = nExpected( aTrack );
  return info;
}

unsigned int UTHitExpectation::nExpected( const LHCb::Track& aTrack ) const {

  // make a line at UTa and UTb
  const State& UTaState = closestState( aTrack, m_zUTa );
  StateVector  stateVectorUTa( UTaState.position(), UTaState.slopes() );

  const State& UTbState = closestState( aTrack, m_zUTb );
  StateVector  stateVectorUTb( UTbState.position(), UTbState.slopes() );

  // determine which modules should be hit
  std::vector<UTChannelID> expectedHitsA;
  expectedHitsA.reserve( 4 );
  std::vector<UTChannelID> expectedHitsB;
  expectedHitsB.reserve( 4 );
  collectHits( expectedHitsA, stateVectorUTa, 1 );
  collectHits( expectedHitsB, stateVectorUTb, 2 );

  return expectedHitsA.size() + expectedHitsB.size();
}

void UTHitExpectation::collect( const LHCb::Track& aTrack, std::vector<LHCb::LHCbID>& ids ) const {

  // make a line at UTa and UTb
  const State& UTaState = closestState( aTrack, m_zUTa );
  StateVector  stateVectorUTa( UTaState.position(), UTaState.slopes() );

  const State& UTbState = closestState( aTrack, m_zUTb );
  StateVector  stateVectorUTb( UTbState.position(), UTbState.slopes() );

  // determine which modules should be hit
  std::vector<UTChannelID> expectedHits;
  expectedHits.reserve( 8 );
  collectHits( expectedHits, stateVectorUTa, 1 );
  collectHits( expectedHits, stateVectorUTb, 2 );

  // convert to LHCb ids
  ids.reserve( expectedHits.size() );
  std::transform( expectedHits.begin(), expectedHits.end(), std::back_inserter( ids ),
                  []( UTChannelID chan ) { return LHCbID{chan}; } );
}

void UTHitExpectation::collectHits( std::vector<LHCb::UTChannelID>& chans, LHCb::StateVector stateVec,
                                    const unsigned int station ) const {

  // loop over the sectors
  const DeUTDetector::Sectors&          sectorVector = m_utDet->sectors();
  DeUTDetector::Sectors::const_iterator iterS        = sectorVector.begin();
  for ( ; iterS != sectorVector.end(); ++iterS ) {
    // propagate to z of sector
    m_extrapolator->propagate( stateVec, ( *iterS )->globalCentre().z() )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    const UTChannelID elemID = ( *iterS )->elementID();
    if ( elemID.station() == station ) {
      // loop over sensors
      const DeUTSector::Sensors&          tsensors = ( *iterS )->sensors();
      DeUTSector::Sensors::const_iterator iter     = tsensors.begin();
      for ( ; iter != tsensors.end(); ++iter ) {
        Tf::Tsa::Line3D aLine3D( stateVec.position(), stateVec.slopes() );
        if ( select( ( *iterS )->elementID() ) && insideSensor( *iter, aLine3D ) == true ) {

          // get the list of strips that could be in the cluster
          Gaudi::XYZPoint globalEntry = intersection( aLine3D, ( *iter )->entryPlane() );
          Gaudi::XYZPoint globalExit  = intersection( aLine3D, ( *iter )->exitPlane() );
          Gaudi::XYZPoint localEntry  = ( *iter )->toLocal( globalEntry );
          Gaudi::XYZPoint localExit   = ( *iter )->toLocal( globalExit );

          unsigned int firstStrip = ( *iter )->localUToStrip( localEntry.x() );
          unsigned int lastStrip  = ( *iter )->localUToStrip( localExit.x() );

          // might have to swap...
          if ( firstStrip > lastStrip ) std::swap( firstStrip, lastStrip );

          // allow for capacitive coupling....
          if ( ( *iter )->isStrip( firstStrip - 1 ) == true ) --firstStrip;
          if ( ( *iter )->isStrip( lastStrip + 1 ) == true ) ++lastStrip;

          bool         found       = false;
          unsigned int middleStrip = ( firstStrip + lastStrip ) / 2;
          for ( unsigned int iStrip = firstStrip; iStrip != lastStrip; ++iStrip ) {
            const UTChannelID chan = ( *iterS )->stripToChan( iStrip );
            if ( ( *iterS )->isOKStrip( chan ) == true ) { // check it is alive
              if ( m_allStrips == true ) {
                chans.push_back( chan ); // take them all
              } else {
                found = true; // take just the sector
              }
            } // ok strip
          }   // loop strips

          if ( !m_allStrips && found == true ) {
            UTChannelID midChan( elemID.type(), elemID.station(), elemID.layer(), elemID.detRegion(), elemID.sector(),
                                 middleStrip );
            chans.push_back( midChan );
          }
        } // select
      }   // iter
    }     // station
  }       // sector
}

Gaudi::XYZPoint UTHitExpectation::intersection( const Tf::Tsa::Line3D& line, const Gaudi::Plane3D& aPlane ) const {

  // make a plane
  Gaudi::XYZPoint inter;
  double          mu = 0;
  Gaudi::Math::intersection( line, aPlane, inter, mu );
  return inter;
}
