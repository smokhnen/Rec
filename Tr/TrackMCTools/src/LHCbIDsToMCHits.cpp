/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// GaudiKernel
#include "GaudiKernel/IIncidentSvc.h"

#include "LHCbIDsToMCHits.h"

#include "Event/FTLiteCluster.h"
#include "Event/MCHit.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "Event/VPCluster.h"

DECLARE_COMPONENT( LHCbIDsToMCHits )
using namespace LHCb;

LHCbIDsToMCHits::LHCbIDsToMCHits( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_utLinks( 0, 0, "" )
    , m_vPLinks( 0, 0, "" )
    , m_muonLinks( 0, 0, "" )
    , m_ftLinks( 0, 0, "" )
    , m_configuredUT( false )
    , m_configuredVP( false )
    , m_configuredMuon( false )
    , m_configuredFT( false )
    , m_endString( "2MCHits" ) {

  // constructer
  declareInterface<ILHCbIDsToMCHits>( this );
}

LHCbIDsToMCHits::~LHCbIDsToMCHits() {
  // destructer
}

StatusCode LHCbIDsToMCHits::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  incSvc()->addListener( this, IncidentType::BeginEvent );

  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCHits::link( LHCbIDs::const_iterator& start, LHCbIDs::const_iterator& stop,
                                  LinkMap& output ) const {

  for ( LHCbIDs::const_iterator iter = start; iter != stop; ++iter ) {
    link( *iter, output ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  } // iter

  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCHits::link( const Track& aTrack, LinkMap& output ) const {
  LHCbIDs::const_iterator start = aTrack.lhcbIDs().begin();
  LHCbIDs::const_iterator stop  = aTrack.lhcbIDs().end();
  return link( start, stop, output );
}

StatusCode LHCbIDsToMCHits::link( const LHCbID& id, LinkMap& output ) const {

  // switch statement from hell
  switch ( id.detectorType() ) {
  case LHCbID::channelIDtype::UT:
    linkUT( id, output );
    break;
  case LHCbID::channelIDtype::VP:
    linkVP( id, output );
    break;
  case LHCbID::channelIDtype::Muon:
    linkMuon( id, output );
    break;
  case LHCbID::channelIDtype::FT:
    linkFT( id, output );
    break;

  default:
    Warning( "Unknown type !", StatusCode::SUCCESS, 10 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    break;
  }

  return StatusCode::SUCCESS;
}

void LHCbIDsToMCHits::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) {
    m_configuredUT   = false;
    m_configuredFT   = false;
    m_configuredVP   = false;
    m_configuredMuon = false;
  }
}

void LHCbIDsToMCHits::linkUT( const LHCbID& lhcbid, LinkMap& output ) const {
  if ( !m_configuredUT ) {
    m_configuredUT = true;
    m_utLinks      = UTLinks( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters + m_endString );
    if ( m_utLinks.notFound() ) { throw GaudiException( "no UTLinker", "LHCbIDsToMCHits", StatusCode::FAILURE ); }
  }
  linkToDetTruth( lhcbid.utID(), m_utLinks, output );
}

void LHCbIDsToMCHits::linkVP( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredVP ) {
    m_configuredVP = true;
    m_vPLinks      = VPLinks( evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default + m_endString );
    if ( m_vPLinks.notFound() ) { throw GaudiException( "no vPLinker", "LHCbIDsToMCHits", StatusCode::FAILURE ); }
  }
  linkToDetTruth( lhcbid.vpID(), m_vPLinks, output );
}

void LHCbIDsToMCHits::linkFT( const LHCbID& lhcbid, LinkMap& output ) const {
  if ( !m_configuredFT ) {
    m_configuredFT = true;
    m_ftLinks      = FTLinks( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default + m_endString );
    if ( m_ftLinks.notFound() ) { throw GaudiException( "no FTLinker", "LHCbIDsToMCHits", StatusCode::FAILURE ); }
  }
  linkToDetTruth( lhcbid.ftID(), m_ftLinks, output );
}

void LHCbIDsToMCHits::linkMuon( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredMuon ) {
    m_configuredMuon = true;
    m_muonLinks      = MuonLinks( evtSvc(), msgSvc(), LHCb::MuonCoordLocation::MuonCoords + m_endString );
    if ( m_muonLinks.notFound() ) { throw GaudiException( "no MuonLinker", "LHCbIDsToMCHits", StatusCode::FAILURE ); }
  }
  linkToDetTruth( lhcbid.muonID(), m_muonLinks, output );
}
