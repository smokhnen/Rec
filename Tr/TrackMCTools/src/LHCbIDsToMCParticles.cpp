/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// GaudiKernel
#include "GaudiKernel/IIncidentSvc.h"

#include "LHCbIDsToMCParticles.h"

#include "Event/FTLiteCluster.h"
#include "Event/MCParticle.h"
#include "Event/MuonCoord.h"
#include "Event/Track.h"
#include "Event/UTCluster.h"
#include "Event/VPCluster.h"

DECLARE_COMPONENT( LHCbIDsToMCParticles )
using namespace LHCb;

LHCbIDsToMCParticles::LHCbIDsToMCParticles( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_muonLinks( 0, 0, "" )
    , m_vpLinks( 0, 0, "" )
    , m_utLinks( 0, 0, "" )
    , m_ftLinks( 0, 0, "" )
    , m_configuredMuon( false )
    , m_configuredVP( false )
    , m_configuredUT( false )
    , m_configuredFT( false ) {

  // constructer
  declareInterface<ILHCbIDsToMCParticles>( this );
}

LHCbIDsToMCParticles::~LHCbIDsToMCParticles() {
  // destructer
}

StatusCode LHCbIDsToMCParticles::initialize() {

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  incSvc()->addListener( this, IncidentType::BeginEvent );

  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCParticles::link( LHCbIDs::const_iterator& start, LHCbIDs::const_iterator& stop,
                                       LinkMap& output ) const {

  for ( LHCbIDs::const_iterator iter = start; iter != stop; ++iter ) {
    StatusCode sc = link( *iter, output );
    if ( sc.isFailure() ) { return sc; }
  } // iter

  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCParticles::link( const Track& aTrack, LinkMap& output ) const {
  LHCbIDs::const_iterator start = aTrack.lhcbIDs().begin();
  LHCbIDs::const_iterator stop  = aTrack.lhcbIDs().end();
  return link( start, stop, output );
}

StatusCode LHCbIDsToMCParticles::link( const LHCbID& id, LinkMap& output ) const {

  // switch statement from hell
  switch ( id.detectorType() ) {
  case LHCbID::channelIDtype::Muon:
    return linkMuon( id, output );
    break;
  case LHCbID::channelIDtype::VP:
    return linkVP( id, output );
    break;
  case LHCbID::channelIDtype::UT:
    return linkUT( id, output );
    break;
  case LHCbID::channelIDtype::FT:
    return linkFT( id, output );
    break;

  default:
    return Warning( "Unknown type !", StatusCode::SUCCESS, 10 );
    break;
  }

  return StatusCode::SUCCESS;
}

void LHCbIDsToMCParticles::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) {
    m_configuredMuon = false;
    m_configuredVP   = false;
    m_configuredUT   = false;
    m_configuredFT   = false;
  }
}

StatusCode LHCbIDsToMCParticles::linkMuon( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredMuon ) {
    m_configuredMuon = true;
    m_muonLinks      = MuonLinks( evtSvc(), msgSvc(), LHCb::MuonCoordLocation::MuonCoords );
    if ( m_muonLinks.notFound() ) { return Error( "no MuonLinker", StatusCode::FAILURE, 10 ); }
  }
  linkToDetTruth( lhcbid.muonID(), m_muonLinks, output );
  return StatusCode::SUCCESS;
}

// -- upgrade
StatusCode LHCbIDsToMCParticles::linkVP( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredVP ) {
    m_configuredVP = true;
    m_vpLinks      = VPLinks( evtSvc(), msgSvc(), LHCb::VPClusterLocation::Default );
    if ( m_vpLinks.notFound() ) { return Error( "no VPLinker", StatusCode::FAILURE, 10 ); }
  }
  linkToDetTruth( lhcbid.vpID(), m_vpLinks, output );
  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCParticles::linkUT( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredUT ) {
    m_configuredUT = true;
    m_utLinks      = UTLinks( evtSvc(), msgSvc(), LHCb::UTClusterLocation::UTClusters );
    if ( m_utLinks.notFound() ) { return Error( "no UTLinker", StatusCode::FAILURE, 10 ); }
  }
  linkToDetTruth( lhcbid.utID(), m_utLinks, output );
  return StatusCode::SUCCESS;
}

StatusCode LHCbIDsToMCParticles::linkFT( const LHCbID& lhcbid, LinkMap& output ) const {

  if ( !m_configuredFT ) {
    m_configuredFT = true;
    m_ftLinks      = FTLinks( evtSvc(), msgSvc(), LHCb::FTLiteClusterLocation::Default );
    if ( m_ftLinks.notFound() ) { return Error( "no FTLinker", StatusCode::FAILURE, 10 ); }
  }
  linkToDetTruth( lhcbid.ftID(), m_ftLinks, output );
  return StatusCode::SUCCESS;
}
