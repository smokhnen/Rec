/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef _LHCbIDsToMCHits_H
#define _LHCbIDsToMCHits_H

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"

// from LinkerEvent
#include "Event/MCProperty.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"

#include "MCInterfaces/ILHCbIDsToMCHits.h"

/** @class LHCbIDsToMCHits LHCbIDsToMCHits.h
 *
 *  Link ids to MCHits
 *
 *  @author M.Needham
 *  @date   31/04/2006
 */

namespace LHCb {
  class MuonCoord;
  class VPCluster;
  class UTCluster;
} // namespace LHCb

class LHCbIDsToMCHits : public GaudiTool, virtual public ILHCbIDsToMCHits, virtual public IIncidentListener {

public:
  /// constructer
  LHCbIDsToMCHits( const std::string& type, const std::string& name, const IInterface* parent );

  /** destructer */
  virtual ~LHCbIDsToMCHits();

  /** initialize */
  StatusCode initialize() override;

  /**
     Trivial link from list of IDs to all MCHits contributing
     @param start  iterator to first id
     @param stop   iterator to last id
     @param output vector by reference
     @return StatusCode
  */
  StatusCode link( LHCbIDs::const_iterator& start, LHCbIDs::const_iterator& stop, LinkMap& output ) const override;

  /**
     Trivial link from list of ALL ids in track to MCHits contributing
     @param aTrack track
     @param output vector by reference
     @return StatusCode
  */
  StatusCode link( const LHCb::Track& aTrack, LinkMap& output ) const override;

  /**
     Trivial link from single id to MCHits contributing
     @param id
     @param output vector by reference
     @return StatusCode
  */
  StatusCode link( const LHCb::LHCbID& id, LinkMap& output ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  typedef LinkedTo<LHCb::MCHit, LHCb::UTCluster> UTLinks;
  typedef LinkedTo<LHCb::MCHit, LHCb::VPCluster> VPLinks;
  typedef LinkedTo<LHCb::MCHit, LHCb::MuonCoord> MuonLinks;
  typedef LinkedTo<LHCb::MCHit>                  FTLinks;

  template <typename ID, typename LINKER>
  void linkToDetTruth( const ID& id, LINKER& aLinker, LinkMap& output ) const;
  void linkUT( const LHCb::LHCbID& id, LinkMap& output ) const;
  void linkVP( const LHCb::LHCbID& id, LinkMap& output ) const;
  void linkMuon( const LHCb::LHCbID& id, LinkMap& output ) const;
  void linkFT( const LHCb::LHCbID& id, LinkMap& output ) const;

  mutable UTLinks   m_utLinks;
  mutable VPLinks   m_vPLinks;
  mutable MuonLinks m_muonLinks;
  mutable FTLinks   m_ftLinks;

  mutable bool m_configuredUT;
  mutable bool m_configuredVP;
  mutable bool m_configuredMuon;
  mutable bool m_configuredFT;

  std::string m_endString;
};

#include "Event/MCHit.h"

/// Link LHCbID to MCHits in detector in question
template <typename ID, typename LINKER>
void LHCbIDsToMCHits::linkToDetTruth( const ID& id, LINKER& aLinker, LinkMap& output ) const {

  LHCb::MCHit* aHit = aLinker.first( id );
  if ( 0 != aHit ) {
    while ( 0 != aHit ) {
      output[aHit] += 1;
      aHit = aLinker.next();
    }
  } else {
    output[0] += 1;
  }
}
#endif
