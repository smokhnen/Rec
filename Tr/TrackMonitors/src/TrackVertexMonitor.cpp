/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IAxis.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/TwoProngVertex.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackVertexer.h"
#include "TrackKernel/TrackStateVertex.h"
#include <algorithm>

namespace {
  std::vector<const LHCb::State*> firstStates( LHCb::span<const LHCb::Track* const> tracks ) {
    std::vector<const LHCb::State*> states;
    states.reserve( tracks.size() );
    for ( const auto& track : tracks ) { states.push_back( &track->firstState() ); }
    return states;
  }
} // namespace

class TrackVertexMonitor
    : public Gaudi::Functional::Consumer<void( LHCb::RecVertex::Range const&, LHCb::Track::Range const& ),
                                         Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /** Standard construtor */
  TrackVertexMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  void operator()( LHCb::RecVertex::Range const& pvcontainer, LHCb::Track::Range const& alltracks ) const override;

private:
  Gaudi::Property<double> m_ipmax{this, "MaxIP", 0.5 * Gaudi::Units::mm};
  Gaudi::Property<double> m_ipmaxprof{this, "MaxIPProfile", 0.1 * Gaudi::Units::mm};
  Gaudi::Property<double> m_dzmax{this, "MaxDz", 5 * Gaudi::Units::mm};
  Gaudi::Property<double> m_xpvmax{this, "MaxXPV", 2 * Gaudi::Units::mm};
  Gaudi::Property<double> m_ypvmax{this, "MaxYPV", 1 * Gaudi::Units::mm};
  Gaudi::Property<double> m_zpvmin{this, "MinZPV", -20 * Gaudi::Units::cm};
  Gaudi::Property<double> m_zpvmax{this, "MaxZPV", 20 * Gaudi::Units::cm};
  Gaudi::Property<double> m_zpvmin_wide{this, "MinZPV_Wide", -150 * Gaudi::Units::cm, "Wide z window for PV plot"};
  Gaudi::Property<double> m_zpvmax_wide{this, "MaxZPV_Wide", 150 * Gaudi::Units::cm, "Wide z window for PV plot"};
  Gaudi::Property<double> m_maxLongTrackChisqPerDof{this, "MaxLongTrackChisqPerDof", 5};
  Gaudi::Property<double> m_minLongTrackMomentum{this, "MinLongTrackMomentum", 5};
  Gaudi::Property<size_t> m_nprbins{this, "NumProfileBins", 20};
  Gaudi::Property<uint>   m_ntracksPV{this, "NumTracksPV", 2};

  ToolHandle<ITrackVertexer> m_vertexer{"TrackVertexer"};
  AIDA::IHistogram1D*        m_trackXIP;
  AIDA::IProfile1D*          m_trackXIPVsPhi;
  AIDA::IProfile1D*          m_trackXIPVsEta;
  AIDA::IHistogram1D*        m_trackYIP;
  AIDA::IProfile1D*          m_trackYIPVsPhi;
  AIDA::IProfile1D*          m_trackYIPVsEta;

  AIDA::IHistogram1D* m_fastTrackTransverseIP;
  AIDA::IProfile1D*   m_fastTrackTransverseIPVsPhi;
  AIDA::IProfile1D*   m_fastTrackTransverseIPVsEta;
  AIDA::IHistogram1D* m_fastTrackLongitudinalIP;
  AIDA::IProfile1D*   m_fastTrackLongitudinalIPVsPhi;
  AIDA::IProfile1D*   m_fastTrackLongitudinalIPVsEta;
  AIDA::IHistogram1D* m_fastTrackXIP;
  AIDA::IProfile1D*   m_fastTrackXIPVsPhi;
  AIDA::IProfile1D*   m_fastTrackXIPVsEta;
  AIDA::IHistogram1D* m_fastTrackYIP;
  AIDA::IProfile1D*   m_fastTrackYIPVsPhi;
  AIDA::IProfile1D*   m_fastTrackYIPVsEta;

  AIDA::IHistogram1D* m_twoprongMass;
  AIDA::IHistogram1D* m_twoprongMomentum;
  AIDA::IHistogram1D* m_twoprongDoca;
  AIDA::IHistogram1D* m_twoprongDocaPull;
  AIDA::IHistogram1D* m_twoprongDecaylength;
  AIDA::IHistogram1D* m_twoprongDecaylengthSignificance;
  AIDA::IHistogram1D* m_twoprongCTau;
  AIDA::IHistogram1D* m_twoprongTau;
  AIDA::IHistogram1D* m_twoprongIPChisquare;
  AIDA::IProfile1D*   m_twoprongDocaVsEta;
  AIDA::IProfile1D*   m_twoprongDocaVsPhi;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackVertexMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackVertexMonitor::TrackVertexMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"PVContainer", LHCb::RecVertexLocation::Primary},
                KeyValue{"TrackContainer", LHCb::TrackLocation::Default}}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackVertexMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already by GaudiAlgorithm

  // book histograms
  static const std::string histoDir = "Track/";
  if ( "" == histoTopDir() ) setHistoTopDir( histoDir );

  // impact parameters of the hightest Pt track wrt the vertex
  m_trackXIP      = book1D( "track IP X", "track IP X (biased)", -m_ipmax, m_ipmax );
  m_trackYIP      = book1D( "track IP Y", "track IP Y (biased)", -m_ipmax, m_ipmax );
  m_trackXIPVsPhi = bookProfile1D( "track IP X vs phi", "track IP X vs phi (biased)", -Gaudi::Units::pi,
                                   Gaudi::Units::pi, m_nprbins );
  m_trackXIPVsEta = bookProfile1D( "track IP X vs eta", "track IP X vs eta (biased)", 2.0, 5.0, m_nprbins );
  m_trackYIPVsPhi = bookProfile1D( "track IP Y vs phi", "track IP Y vs phi (biased)", -Gaudi::Units::pi,
                                   Gaudi::Units::pi, m_nprbins );
  m_trackYIPVsEta = bookProfile1D( "track IP Y vs eta", "track IP Y vs eta (biased)", 2.0, 5.0, m_nprbins );

  m_fastTrackTransverseIP   = book1D( "fast track transverse IP", -m_ipmax, m_ipmax );
  m_fastTrackLongitudinalIP = book1D( "fast track longitudinal IP", -m_ipmax, m_ipmax );
  m_fastTrackTransverseIPVsPhi =
      bookProfile1D( "fast track transverse IP vs phi", -Gaudi::Units::pi, Gaudi::Units::pi, m_nprbins );
  m_fastTrackTransverseIPVsEta = bookProfile1D( "fast track transverse IP vs eta", 2.0, 5.0, m_nprbins );
  m_fastTrackLongitudinalIPVsPhi =
      bookProfile1D( "fast track longitudinal IP vs phi", -Gaudi::Units::pi, Gaudi::Units::pi, m_nprbins );
  m_fastTrackLongitudinalIPVsEta = bookProfile1D( "fast track longitudinal IP vs eta", 2.0, 5.0, m_nprbins );

  m_fastTrackXIP      = book1D( "fast track IP X", "fast track IP X", -m_ipmax, m_ipmax );
  m_fastTrackYIP      = book1D( "fast track IP Y", "fast track IP Y", -m_ipmax, m_ipmax );
  m_fastTrackXIPVsPhi = bookProfile1D( "fast track IP X vs phi", "fast track IP X vs phi", -Gaudi::Units::pi,
                                       Gaudi::Units::pi, m_nprbins );
  m_fastTrackXIPVsEta = bookProfile1D( "fast track IP X vs eta", "fast track IP X vs eta", 2.0, 5.0, m_nprbins );
  m_fastTrackYIPVsPhi = bookProfile1D( "fast track IP Y vs phi", "fast track IP Y vs phi", -Gaudi::Units::pi,
                                       Gaudi::Units::pi, m_nprbins );
  m_fastTrackYIPVsEta = bookProfile1D( "fast track IP Y vs eta", "fast track IP Y vs eta", 2.0, 5.0, m_nprbins );

  // impact parameter and vertex chisquare of the two highest Pt tracks
  m_twoprongMass                    = book1D( "twoprong mass (GeV)", 0, 10 );
  m_twoprongMomentum                = book1D( "twoprong momentum (GeV)", 0, 200 );
  m_twoprongDoca                    = book1D( "twoprong doca", -m_ipmax, m_ipmax );
  m_twoprongDocaPull                = book1D( "twoprong doca pull", -5, 5 );
  m_twoprongDecaylength             = book1D( "twoprong decaylength", -2, 2 );
  m_twoprongDecaylengthSignificance = book1D( "twoprong decaylength significance", -5, 5 );
  m_twoprongCTau                    = book1D( "twoprong ctau", -0.1, 0.1 );
  m_twoprongTau                     = book1D( "twoprong proper lifetime (ps)", -0.2, 0.2 );
  m_twoprongIPChisquare             = book1D( "twoprong IP chi2 per dof", 0, 10 );

  m_twoprongDocaVsEta = bookProfile1D( "twoprong doca vs eta", 2.0, 5.0, m_nprbins );
  m_twoprongDocaVsPhi = bookProfile1D( "twoprong doca vs phi", -Gaudi::Units::pi, Gaudi::Units::pi, m_nprbins );

  return sc;
}

namespace {

  template <class TrackContainer, class Predicate>
  std::vector<const LHCb::Track*> myselect( const TrackContainer& tracks, Predicate&& selector ) {
    std::vector<const LHCb::Track*> rc;
    std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( rc ), std::forward<Predicate>( selector ) );
    return rc;
  }

  std::vector<const LHCb::Track*> myconvert( const SmartRefVector<LHCb::Track>& tracks ) {
    return myselect( tracks, []( const LHCb::Track* t ) { return t != nullptr; } );
  }

  auto TrackTypePredicate = []( int atype ) {
    return [=]( const LHCb::Track* track ) { return track->type() == atype; };
  };

  auto TrackFlagPredicate = []( LHCb::Track::Flags flag, bool positive ) {
    return [=]( const LHCb::Track* track ) {
      auto f = track->checkFlag( flag );
      return positive ? f : !f;
    };
  };

  auto TrackVeloSidePredicate = []( int asign ) {
    return [=]( const LHCb::Track* track ) {
      return track->firstState().tx() * asign * ( track->checkFlag( LHCb::Track::Flags::Backward ) ? -1 : 1 ) > 0;
    };
  };

} // namespace

void TrackVertexMonitor::operator()( LHCb::RecVertex::Range const& pvcontainer,
                                     LHCb::Track::Range const&     alltracks ) const {

  const auto isLong     = TrackTypePredicate( LHCb::Track::Types::Long );
  const auto isBackward = TrackFlagPredicate( LHCb::Track::Flags::Backward, true );
  const auto isForward  = TrackFlagPredicate( LHCb::Track::Flags::Backward, false );

  // lists needed
  // - primary vertices
  // - all tracks
  // - long tracks
  // - backward tracks
  // for now I'll just create the track lists from the Best container

  // number of primary vertices
  plot( pvcontainer.size(), "NumPrimaryVertices", -0.5, 10.5, 11 );

  for ( const LHCb::RecVertex* pv : pvcontainer ) {
    auto tracks         = myconvert( pv->tracks() );
    auto forwardtracks  = myselect( tracks, isForward );
    auto backwardtracks = myselect( tracks, isBackward );
    auto longtracks     = myselect( tracks, isLong );

    // number of tracks per primary vertex
    plot( tracks.size(), "NumTracksPerPV", -0.5, 99.5, 50 );
    // number of long tracks per primary vertex
    plot( longtracks.size(), "NumLongTracksPerPV", -0.5, 99.5, 50 );
    // number of backward tracks per primary vertex
    plot( backwardtracks.size(), "NumBackTracksPerPV", -0.5, 99.5, 50 );
    // chisquare
    plot( pv->chi2() / pv->nDoF(), "PV chisquare per dof", 0., 3., 150 );
    // position with crap hack for vertices at exactly 0
    if ( std::abs( pv->position().x() ) > 0.00001 && std::abs( pv->position().y() ) > 0.00001 ) {
      // info() << "pvx " << pv->position().x() << endmsg;
      plot( pv->position().x(), "PV x position", -m_xpvmax, m_xpvmax, 200 );
      plot( pv->position().y(), "PV y position", -m_ypvmax, m_ypvmax );
      plot( pv->position().z(), "PV z position", m_zpvmin, m_zpvmax );
      plot( pv->position().z(), "PV z position (wide)", m_zpvmin_wide, m_zpvmax_wide );
    }

    if ( std::abs( pv->position().y() ) < m_ypvmax )
      profile1D( pv->position().z(), pv->position().y(), "PV y versus z", m_zpvmin, m_zpvmax, m_nprbins );
    if ( std::abs( pv->position().x() ) < m_xpvmax )
      profile1D( pv->position().z(), pv->position().x(), "PV x versus z", m_zpvmin, m_zpvmax, m_nprbins );

    // refit the primary vertex with only the long tracks
    if ( longtracks.size() >= 2 ) {
      auto longvertex = m_vertexer->fit( firstStates( longtracks ) );
      if ( longvertex ) plot( longvertex->chi2() / longvertex->nDoF(), "PV long chisquare per dof", 0, 10 );
    }

    // now split the primary vertex in left and right tracks
    auto lefttracks  = myselect( tracks, TrackVeloSidePredicate( +1 ) );
    auto righttracks = myselect( tracks, TrackVeloSidePredicate( -1 ) );
    if ( lefttracks.size() >= m_ntracksPV && righttracks.size() >= m_ntracksPV ) {
      // fit two vertices
      auto leftvertex = m_vertexer->fit( firstStates( lefttracks ) );

      if ( leftvertex ) {
        plot( leftvertex->position().x(), "PV left x", -m_xpvmax, m_xpvmax, 200 );
        plot( leftvertex->position().y(), "PV left y", -m_ypvmax, m_ypvmax );
        plot( leftvertex->position().z(), "PV left z", m_zpvmin, m_zpvmax );
        /* PK-R3C undefined
        if ( m_leftSensor ) {
          plot( -( m_leftSensor->globalToVeloHalfBox( leftvertex->position() ) ).x(), "PV left-Left half x",
                -m_xpvmax / 4, m_xpvmax / 4 );
          plot( -( m_leftSensor->globalToVeloHalfBox( leftvertex->position() ) ).y(), "PV left-Left half y",
                -m_ypvmax / 2, m_ypvmax / 2 );
        }
        */
      }
      auto rightvertex = m_vertexer->fit( firstStates( righttracks ) );
      if ( rightvertex ) {
        plot( rightvertex->position().x(), "PV right x", -m_xpvmax, m_xpvmax, 200 );
        plot( rightvertex->position().y(), "PV right y", -m_ypvmax, m_ypvmax );
        plot( rightvertex->position().z(), "PV right z", m_zpvmin, m_zpvmax );
        /* PK-R3C
        if ( m_rightSensor ) {
          plot( -( m_rightSensor->globalToVeloHalfBox( rightvertex->position() ) ).x(), "PV right-Right half x",
                -m_xpvmax / 4, m_xpvmax / 4 );
          plot( -( m_rightSensor->globalToVeloHalfBox( rightvertex->position() ) ).y(), "PV right-Right half y",
                -m_ypvmax / 2, m_ypvmax / 2 );
        }
        */
      }
      if ( leftvertex && rightvertex ) {
        // draw the difference
        Gaudi::XYZVector dx = leftvertex->position() - rightvertex->position();

        plot( dx.x(), "PV left-right delta x", -0.1, 0.1 );
        plot( dx.y(), "PV left-right delta y", -0.1, 0.1 );
        plot( dx.z(), "PV left-right delta z", -1, 1 );
        if ( std::abs( dx.y() ) < m_ipmaxprof )
          profile1D( pv->position().z(), dx.y(), "PV left-right delta y versus z", m_zpvmin, m_zpvmax, m_nprbins );
        if ( std::abs( dx.x() ) < m_ipmaxprof )
          profile1D( pv->position().z(), dx.x(), "PV left-right delta x versus z", m_zpvmin, m_zpvmax, m_nprbins );

        // draw the pull of the difference
        Gaudi::SymMatrix3x3 cov = leftvertex->covMatrix() + rightvertex->covMatrix();

        // cov(0,0)
        if ( cov( 0, 0 ) > 1e-10 ) {
          plot( dx.x() / std::sqrt( cov( 0, 0 ) ), "PV left-right delta x pull", -5, 5 );
        } else {
          Info( "cov(0,0) too small", StatusCode::SUCCESS, 10 ).ignore();
        }
        // cov(1,1)
        if ( cov( 1, 1 ) > 1e-10 ) {
          plot( dx.y() / std::sqrt( cov( 1, 1 ) ), "PV left-right delta y pull", -5, 5 );
        } else {
          Info( "cov(1,1) too small", StatusCode::SUCCESS, 10 ).ignore();
        }
        // cov(2,2)
        if ( cov( 2, 2 ) > 1e-10 ) {
          plot( dx.z() / std::sqrt( cov( 2, 2 ) ), "PV left-right delta z pull", -5, 5 );
        } else {
          Info( "cov(2,2) too small", StatusCode::SUCCESS, 10 ).ignore();
        }

        // draw the chisquares
        if ( leftvertex->nDoF() > 0 ) {
          plot( leftvertex->chi2() / leftvertex->nDoF(), "PV left chisquare per dof", 0, 10 );
        } else {
          Info( "left ndof = 0", StatusCode::SUCCESS, 10 ).ignore();
        }
        if ( rightvertex->nDoF() > 0 ) {
          plot( rightvertex->chi2() / rightvertex->nDoF(), "PV right chisquare per dof", 0, 10 );
        } else {
          Info( "right ndof = 0", StatusCode::SUCCESS, 10 ).ignore();
        }
      }
    }

    if ( forwardtracks.size() >= 2 && backwardtracks.size() >= 2 ) {
      // fit two vertices
      auto forwardvertex  = m_vertexer->fit( firstStates( forwardtracks ) );
      auto backwardvertex = m_vertexer->fit( firstStates( backwardtracks ) );
      if ( forwardvertex && backwardvertex ) {
        Gaudi::XYZVector dx = forwardvertex->position() - backwardvertex->position();

        // draw the difference
        plot( dx.x(), "PV forward-backward delta x", -m_ipmax, m_ipmax );
        plot( dx.y(), "PV forward-backward delta y", -m_ipmax, m_ipmax );
        plot( dx.z(), "PV forward-backward delta z", -m_dzmax, m_dzmax );
        if ( std::abs( dx.y() ) < m_ipmaxprof )
          profile1D( pv->position().z(), dx.y(), "PV forward-backward delta y versus z", m_zpvmin, m_zpvmax,
                     m_nprbins );
        if ( std::abs( dx.x() ) < m_ipmaxprof )
          profile1D( pv->position().z(), dx.x(), "PV forward-backward delta x versus z", m_zpvmin, m_zpvmax,
                     m_nprbins );

        // draw the pull of the difference
        Gaudi::SymMatrix3x3 cov = forwardvertex->covMatrix() + backwardvertex->covMatrix();
        // cov(0,0)
        if ( cov( 0, 0 ) > 1e-10 ) {
          plot( dx.x() / std::sqrt( cov( 0, 0 ) ), "PV forward-backward delta x pull", -5, 5 );
        } else {
          Info( "cov(0,0) too small", StatusCode::SUCCESS, 10 ).ignore();
        }
        // cov(1,1)
        if ( cov( 1, 1 ) > 1e-10 ) {
          plot( dx.y() / std::sqrt( cov( 1, 1 ) ), "PV forward-backward delta y pull", -5, 5 );
        } else {
          Info( "cov(1,1) too small", StatusCode::SUCCESS, 10 ).ignore();
        }
        // cov(2,2)
        if ( cov( 2, 2 ) > 1e-10 ) {
          plot( dx.z() / std::sqrt( cov( 2, 2 ) ), "PV forward-backward delta z pull", -5, 5 );
        } else {
          Info( "cov(2,2) too small", StatusCode::SUCCESS, 10 ).ignore();
        }
        // draw the chisquares
        if ( forwardvertex->nDoF() > 0 ) {
          plot( forwardvertex->chi2() / forwardvertex->nDoF(), "PV forward chisquare/dof", 0, 10 );
        } else {
          Info( "forward ndof = 0", StatusCode::SUCCESS, 10 ).ignore();
        }
        if ( backwardvertex->nDoF() > 0 ) {
          plot( backwardvertex->chi2() / backwardvertex->nDoF(), "PV backward chisquare/dof", 0, 10 );
        } else {
          Info( "backward ndof = 0", StatusCode::SUCCESS, 10 ).ignore();
        }
      }
    }

    // for events with a single vertex, do something with IP of
    // highest momentum track, as function of phi and eta.
    if ( pvcontainer.size() == 1 && tracks.size() >= 10 ) {

      // now get all good long tracks from the best container:
      auto goodlongtracks = myselect( alltracks, [&]( const LHCb::Track* tr ) {
        return isLong( tr ) && tr->chi2PerDoF() < m_maxLongTrackChisqPerDof && tr->p() > m_minLongTrackMomentum;
      } );

      for ( const LHCb::Track* tr : goodlongtracks ) {
        const LHCb::State& firststate = tr->firstState();
        double             dz         = pv->position().z() - firststate.z();
        double             dx         = firststate.x() + dz * firststate.tx() - pv->position().x();
        double             dy         = firststate.y() + dz * firststate.ty() - pv->position().y();
        Gaudi::XYZVector   p3         = firststate.momentum();
        m_trackXIP->fill( dx );
        m_trackYIP->fill( dy );
        // apply a cut for the profiles
        if ( std::abs( dx ) < m_ipmaxprof && std::abs( dy ) < m_ipmaxprof ) {
          double phi = p3.phi();
          double eta = p3.eta();
          m_trackXIPVsEta->fill( eta, dx );
          m_trackXIPVsPhi->fill( phi, dx );
          m_trackYIPVsEta->fill( eta, dy );
          m_trackYIPVsPhi->fill( phi, dy );
        }
      }

      if ( goodlongtracks.size() >= 2 ) {

        std::sort( goodlongtracks.begin(), goodlongtracks.end(), []( const LHCb::Track* lhs, const LHCb::Track* rhs ) {
          return lhs->firstState().pt() < rhs->firstState().pt();
        } );

        const LHCb::Track* firsttrack = goodlongtracks.back();
        goodlongtracks.pop_back();

        // now pick a 2nd track that makes the highest possible invariant mass with this one
        double             highestmass2( 0 );
        const LHCb::Track* secondtrack = nullptr;
        Gaudi::XYZVector   firstp3     = firsttrack->firstState().momentum();
        for ( const auto& t : goodlongtracks ) {
          Gaudi::XYZVector p3    = t->firstState().momentum();
          double           mass2 = p3.r() * firstp3.r() - p3.Dot( firstp3 );
          if ( secondtrack == 0 || highestmass2 < mass2 ) {
            highestmass2 = mass2;
            secondtrack  = t;
          }
        }

        // recompute the vertex without these tracks
        auto newend = tracks.end();
        newend      = std::remove( tracks.begin(), newend, firsttrack );
        newend      = std::remove( tracks.begin(), newend, secondtrack );
        tracks.erase( newend, tracks.end() );
        auto restvertex = m_vertexer->fit( firstStates( tracks ) );
        if ( restvertex && firsttrack->nStates() != 0 ) {
          const LHCb::State& firststate = firsttrack->firstState();
          double             dz         = restvertex->position().z() - firststate.z();
          double             dx         = firststate.x() + dz * firststate.tx() - restvertex->position().x();
          double             dy         = firststate.y() + dz * firststate.ty() - restvertex->position().y();
          double             nt = std::sqrt( firststate.tx() * firststate.tx() + firststate.ty() * firststate.ty() );
          // transverse and longitudinal impact parameter
          double           iptrans = ( dx * firststate.ty() - dy * firststate.tx() ) / nt;
          double           iplong  = ( dx * firststate.tx() + dy * firststate.ty() ) / nt;
          Gaudi::XYZVector p3      = firststate.momentum();
          double           phi     = p3.phi();
          double           eta     = p3.eta();

          m_fastTrackTransverseIP->fill( iptrans );
          m_fastTrackLongitudinalIP->fill( iplong );
          m_fastTrackXIP->fill( dx );
          m_fastTrackYIP->fill( dy );
          // apply a cut for the profiles
          if ( std::abs( iptrans ) < m_ipmaxprof && std::abs( iplong ) < m_ipmaxprof ) {
            m_fastTrackTransverseIPVsEta->fill( eta, iptrans );
            m_fastTrackTransverseIPVsPhi->fill( phi, iptrans );
            m_fastTrackLongitudinalIPVsEta->fill( eta, iplong );
            m_fastTrackLongitudinalIPVsPhi->fill( phi, iplong );
          }
          if ( std::abs( dx ) < m_ipmaxprof && std::abs( dy ) < m_ipmaxprof ) {
            m_fastTrackXIPVsEta->fill( eta, dx );
            m_fastTrackXIPVsPhi->fill( phi, dx );
            m_fastTrackYIPVsEta->fill( eta, dy );
            m_fastTrackYIPVsPhi->fill( phi, dy );
          }

          // The two-track cuts we only make for relatively heavy objects
          double mass = std::sqrt( highestmass2 );
          m_twoprongMass->fill( mass / Gaudi::Units::GeV );
          if ( mass > 1 * Gaudi::Units::GeV ) {
            // compute doca of two tracks
            Gaudi::XYZVector dx3  = firsttrack->firstState().position() - secondtrack->firstState().position();
            Gaudi::XYZVector n3   = firsttrack->firstState().slopes().Cross( secondtrack->firstState().slopes() );
            double           doca = dx3.Dot( n3 ) / n3.R();
            m_twoprongDoca->fill( doca );
            if ( std::abs( doca ) < m_twoprongDoca->axis().upperEdge() ) {
              m_twoprongDocaVsEta->fill( firstp3.eta(), doca );
              m_twoprongDocaVsPhi->fill( firstp3.phi(), doca );
            }
            // the easiest way to compute the pull is with a vertex fit
            auto twoprong = m_vertexer->fit( firsttrack->firstState(), secondtrack->firstState() );
            if ( twoprong ) {
              double pc = twoprong->p3().R();
              m_twoprongMomentum->fill( pc / Gaudi::Units::GeV );
              m_twoprongDocaPull->fill( std::sqrt( twoprong->chi2() ) * ( doca > 0 ? 1 : -1 ) );
              double chi2, decaylength, decaylengtherr;
              m_vertexer->computeDecayLength( *twoprong, *restvertex, chi2, decaylength, decaylengtherr );
              m_twoprongDecaylength->fill( decaylength );
              m_twoprongDecaylengthSignificance->fill( decaylength / decaylengtherr );
              m_twoprongIPChisquare->fill( chi2 / 2 );
              m_twoprongCTau->fill( decaylength * mass / pc );
              m_twoprongTau->fill( decaylength * mass / ( pc * Gaudi::Units::c_light * Gaudi::Units::picosecond ) );
            }
          }
        }
      }
    }
  }
}
