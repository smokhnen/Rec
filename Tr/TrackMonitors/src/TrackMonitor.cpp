/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToStream.h"
#include "Kernel/HitPattern.h"
#include "Kernel/LHCbID.h"
#include "Map.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackMonitorBase.h"
#include "UTDet/DeUTDetector.h"
#include <map>

using namespace LHCb;
using namespace Gaudi;

/** @class TrackMonitor TrackMonitor.h "TrackCheckers/TrackMonitor"
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackMonitor : public TrackMonitorBase {

public:
  /** Standard construtor */
  using TrackMonitorBase::TrackMonitorBase;

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  StatusCode execute() override;

private:
  void fillHistograms( const LHCb::Track& track, const std::string& type ) const;

  void findRefStates( const LHCb::Track& track, const LHCb::State*& firstMeasurementState,
                      const LHCb::State*& lastMeasurementState ) const;

  Gaudi::Property<double> m_maxMomentum{this, "MaxMomentum", 100.};
  Gaudi::Property<double> m_maxChi2Dof{this, "MaxChi2Dof", 5.};

  mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#Tracks"};

  ToolHandle<IHitExpectation>             m_utExpectation{this, "UTExpectation", "UTHitExpectation"};
  DataObjectReadHandle<LHCb::RecVertices> m_pvs{this, "PrimaryVertices", LHCb::RecVertexLocation::Primary};
};

DECLARE_COMPONENT( TrackMonitor )

StatusCode TrackMonitor::initialize() {
  return TrackMonitorBase::initialize().andThen( [&] {
    if ( !getDetIfExists<DeUTDetector>( DeUTDetLocation::location() ) ) m_utExpectation.disable();
  } );
}

//=============================================================================
// Execute
//=============================================================================
StatusCode TrackMonitor::execute() {

  // get the input data
  LHCb::Track::Range tracks = inputContainer();

  std::map<std::string, unsigned int> multiplicityMap;

  // # number of tracks
  plot( tracks.size(), "nTracks", "# tracks", 0., 500., 50 );
  plot( tracks.size(), "TrackMultiplicityFine", "# tracks", 0.0, 2000., 200 );

  // histograms per track
  for ( const LHCb::Track* track : tracks ) {
    std::string type = histoDirName( *track );
    multiplicityMap[type] += 1;
    fillHistograms( *track, type );
    plot( track->type(), "trackType", "track type", -0.5, 10.5, 11 );
    plot( track->history(), "history", "track history", -0.5, 23.5, 24 );
  }

  // fill counters....
  m_nTracks += tracks.size();
  for ( const auto& [label, count] : multiplicityMap ) {
    counter( "#" + label ) += count;
    plot( count, label + "/multiplicity", label + " multiplicity", -1., 201., 101 );
  }

  return StatusCode::SUCCESS;
}

void TrackMonitor::findRefStates( const LHCb::Track& track, const LHCb::State*& firstMeasurementState,
                                  const LHCb::State*& lastMeasurementState ) const {
  firstMeasurementState = lastMeasurementState = 0;
  for ( const auto& node : nodes( track ) ) {
    if ( node->type() == LHCb::FitNode::Type::HitOnTrack ) {
      if ( firstMeasurementState == 0 || node->z() < firstMeasurementState->z() )
        firstMeasurementState = &( node->state() );
      if ( lastMeasurementState == 0 || node->z() > lastMeasurementState->z() )
        lastMeasurementState = &( node->state() );
    }
  }
}

namespace {
  enum HitType { VPX = 0, VPY, UT, FT, Muon, HitTypeUnknown };
  const std::string HitTypeName[] = {"VPX", "VPY", "UT", "FT", "Muon"};
  // TODO: not sure if these values still make sense for Run3?
  double HitTypeMaxRes[] = {0.1, 0.1, 0.5, 2.0, 10};

  inline HitType hittypemap( const LHCb::Measurement& meas ) {
    return meas.visit(
        []( const LHCb::Measurement::VP& vp ) {
          return vp.projection() == LHCb::Measurement::VP::Projection::X ? VPX : VPY;
        },
        []( const LHCb::Measurement::UT& ) { return HitType::UT; },
        []( const LHCb::Measurement::FT& ) { return HitType::FT; },
        []( const LHCb::Measurement::Muon& ) { return HitType::Muon; }, []( ... ) { return HitType::HitTypeUnknown; } );
  }
} // namespace

void TrackMonitor::fillHistograms( const LHCb::Track& track, const std::string& type ) const {
  // plots we should always make...
  plot( track.probChi2(), type + "/probChi2", "probChi2", 0, 1, 50 );
  plot( track.chi2PerDoF(), type + "/ndof", "chi2/ndof", 0, m_maxChi2Dof );
  plot( track.ghostProbability(), type + "/ghostProb", "ghostProb", 0, 1.0 );
  plot( track.nLHCbIDs(), type + "/nLHCbIDs", "#nLHCbIDs", -0.5, 60.5, 61 );
  plot( track.pseudoRapidity(), type + "/eta", "eta", 0.95, 6.05, 50 );
  plot( track.phi(), type + "/phi", "phi", -M_PI, M_PI, 50 );
  plot( track.nDoF(), type + "/ndof", "ndof", -0.5, 50.5, 51 );
  plot( track.flag(), type + "/flag", "flag", -0.5, 255.5, 256 );
  plot( track.history(), type + "/history", "history", -0.5, 23.5, 24 );
  plot( track.fitStatus(), type + "/fitstatus", "fit status", -0.5, 5.5, 6 );
  plot( nMeasurements( track ), type + "/nMeasurements", "#nMeas", -0.5, 60., 61 );

  const LHCb::State& firststate = track.firstState();
  plot( firststate.x(), type + "/x_firststate", "x of first state", -100, 100 );
  plot( firststate.y(), type + "/y_firststate", "y of first state", -100, 100 );
  plot( firststate.z(), type + "/z_firststate", "z of first state", -500, 500 );
  plot( firststate.tx(), type + "/tx_firststate", "tx of first state", -1.0, 1.0 );
  plot( firststate.ty(), type + "/ty_firststate", "ty of first state", -1.0, 1.0 );
  plot( firststate.qOverP(), type + "/qop_firststate", "q/p of first state", -0.001, 0.001 );

  if ( firststate.tx() != 0 || firststate.ty() != 0 ) {
    const TrackVector& vec = firststate.stateVector();
    double             z   = firststate.z();
    z -= ( vec[0] * vec[2] + vec[1] * vec[3] ) / ( vec[2] * vec[2] + vec[3] * vec[3] );
    plot( z, type + "/z_closet_tozaxis", "z closest to z-axis", -2000, 2000 );
  }

  if ( firststate.qOverP() != 0 ) {
    plot( track.p() / Gaudi::Units::GeV, type + "/p", "momentum", 0., m_maxMomentum, 100 );
    plot( track.pt() / Gaudi::Units::GeV, type + "/pt", "pt", 0, 10, 100 );
  }

  const LHCb::TrackFitResult* fit = fitResult( track );
  if ( fit ) {
    plot( fit->nIter(), type + "/numiter", "number of fit iterations", -0.5, 10.5, 11 );
    plot( fit->pScatter(), type + "/pscatter", "momentum used for material corrections", 0, 100 * Gaudi::Units::GeV );
  }

  // found hits of each type
  const std::vector<LHCb::LHCbID>& ids = track.lhcbIDs();
  const auto nUTHits   = std::count_if( ids.begin(), ids.end(), []( const LHCb::LHCbID& id ) { return id.isUT(); } );
  const auto nVPHits   = std::count_if( ids.begin(), ids.end(), []( const LHCb::LHCbID& id ) { return id.isVP(); } );
  const auto nFTHits   = std::count_if( ids.begin(), ids.end(), []( const LHCb::LHCbID& id ) { return id.isFT(); } );
  const auto nMuonHits = std::count_if( ids.begin(), ids.end(), []( const LHCb::LHCbID& id ) { return id.isMuon(); } );

  plot( nMuonHits, type + "/nMuonHits", "# Muon hits", -0.5, 20.5, 21 );
  plot( nUTHits, type + "/nUTHits", "# UT hits", -0.5, 10.5, 11 );
  plot( nVPHits, type + "/nVPHits", "# UT hits", -0.5, 26.5, 27 );
  plot( nFTHits, type + "/nFTHits", "# UT hits", -0.5, 15.5, 16 );

  size_t  numoutliers( 0 );
  HitType mtype = VPX; // initialize to avoid compiler warning
  for ( const auto& node : nodes( track ) ) {
    if ( node->type() == LHCb::FitNode::Type::HitOnTrack
         // discard extremely small fraction of hits with zero error
         // on residual. (e.g. a downstream track with only one
         // active TT hit)
         && node->errResidual2() > TrackParameters::lowTolerance &&
         ( mtype = hittypemap( node->measurement() ) ) != HitTypeUnknown ) {

      const std::string& name   = HitTypeName[mtype];
      const double       resmax = HitTypeMaxRes[mtype];

      // factor for unbiasing the rms (not the mean!)
      double f = std::sqrt( node->errMeasure2() / node->errResidual2() );
      plot( f * node->residual(), type + "/" + name + "Residual", name + " residual (rms-unbiased)", -resmax, resmax,
            50 );
      plot( node->residual() / node->errResidual(), type + "/" + name + "residualPull", name + " residual pull", -5, 5,
            50 );
      // these should be expert plots because 2D
      if ( ( mtype == VPX || mtype == VPY ) && fullDetail() ) {
        // calculate R in the local frame
        Gaudi::XYZPoint globalPoint = node->state().position();
        Gaudi::XYZPoint localPoint  = node->measurement().toLocal( globalPoint );
        double          r           = localPoint.Rho();

        // factor to calculate residual in detector plane
        double cosalpha( 1.0 );

        Gaudi::XYZVector localUnitPocaVector = node->measurement().toLocal( node->pocaVector() );
        cosalpha                             = localUnitPocaVector.Rho() / localUnitPocaVector.R();

        plot2D( r, node->residual() * f / cosalpha, type + "/" + name + "residualVsR", name + " residual vs R", 10, 42,
                -resmax, resmax, 16, 50 );
      }
    } else if ( node->type() == LHCb::FitNode::Type::Outlier ) {
      ++numoutliers;
    }
  }
  plot( numoutliers, type + "/noutliers", "#outliers", -0.5, 10.5, 11 );

  if ( fit != nullptr ) {
    LHCb::ChiSquare tmp;
    if ( ( tmp = fit->chi2Velo() ).nDoF() > 0 )
      plot( tmp.chi2PerDoF(), type + "/chi2PerDofVelo", "chi/dof for velo segment", 0, m_maxChi2Dof );
    if ( ( tmp = fit->chi2Downstream() ).nDoF() > 0 )
      plot( tmp.chi2PerDoF(), type + "/chi2PerDofDownstream", "chi/dof for T(Muon) segment", 0, m_maxChi2Dof );
    if ( ( tmp = fit->chi2Match() ).nDoF() > 0 )
      plot( tmp.chi2PerDoF(), type + "/chi2PerDofMatch", "chi/dof upstream-downstream match", 0, m_maxChi2Dof );
    if ( ( tmp = fit->chi2Muon() ).nDoF() > 0 )
      plot( tmp.chi2PerDoF(), type + "/chi2PerDofMuon", "chi/dof for muon segment", 0, m_maxChi2Dof );
    const double mom = track.p() / Gaudi::Units::GeV;
    const double phi = track.phi();
    if ( fit->chi2Velo().nDoF() > 0 ) {
      auto prob = fit->chi2Velo().prob();
      profile1D( mom, prob, type + "/chi2ProbVeloVsMom", "chi2 prob for velo segment versus momentum", 0, m_maxMomentum,
                 50 );
      profile1D( phi, prob, type + "/chi2ProbVeloVsPhi", "chi2 prob for velo segment versus phi", -M_PI, M_PI, 50 );
    }
    if ( fit->chi2Downstream().nDoF() > 0 ) {
      const LHCb::State* Tstate = track.stateAt( LHCb::State::Location::AtT );
      const double       phiT   = Tstate ? std::atan2( Tstate->y(), Tstate->x() ) : phi;
      auto               prob   = fit->chi2Downstream().prob();
      profile1D( mom, prob, type + "/chi2ProbDownstreamVsMom", "chi2 prob for T(muon) segment versus momentum", 0,
                 m_maxMomentum, 50 );
      profile1D( phiT, prob, type + "/chi2ProbDownstreamVsPhi", "chi2 prob for T(muon) segment versus phi", -M_PI, M_PI,
                 50 );
    }
    if ( fit->chi2Match().nDoF() > 0 ) {
      auto prob = fit->chi2Match().prob();
      profile1D( mom, prob, type + "/chi2ProbMatchVsMom", "chi2 prob upstream-downstream match versus momentum", 0,
                 m_maxMomentum, 50 );
      profile1D( phi, prob, type + "/chi2ProbMatchVsPhi", "chi2 prob upstream-downstream match versus phi", -M_PI, M_PI,
                 50 );
    }
    if ( fit->chi2().nDoF() > 0 ) {
      auto prob = fit->chi2().prob();
      profile1D( mom, prob, type + "/chi2ProbVsMom", "chi2 prob versus momentum", 0, m_maxMomentum, 50 );
      profile1D( track.pseudoRapidity(), prob, type + "/chi2ProbVsEta", "chi2 prob versus eta", 2, 5, 30 );
      profile1D( phi, prob, type + "/chi2ProbVsPhi", "chi2 prob versus phi", -M_PI, M_PI, 50 );
    }
  }

  // expert checks
  if ( fullDetail() ) {

    static const double halfOverLog10 = 0.5 / std::log( 10.0 );
    // find first and last node with measurement
    // First locate the first and last node that actually have information
    const LHCb::State *firstMState( 0 ), *lastMState( 0 );
    findRefStates( track, firstMState, lastMState );
    if ( firstMState ) {
      plot( log( firstMState->covariance()( 0, 0 ) ) * halfOverLog10, type + "/xerrorAtFirst",
            "10log(x error) at first measurement", -3, 2 );
      plot( log( firstMState->covariance()( 1, 1 ) ) * halfOverLog10, type + "/yerrorAtFirst",
            "10log(y error) at first measurement", -3, 2 );
      plot( log( firstMState->covariance()( 2, 2 ) ) * halfOverLog10, type + "/txerrorAtFirst",
            "10log(tx error) at first measurement", -7, 0 );
      plot( log( firstMState->covariance()( 3, 3 ) ) * halfOverLog10, type + "/tyerrorAtFirst",
            "10log(ty error) at first measurement", -7, 0 );
      plot( log( firstMState->covariance()( 4, 4 ) ) * halfOverLog10, type + "/qoperrorAtFirst",
            "10log(qop error) at first measurement", -8, 0 );
    }
    if ( lastMState ) {
      plot( log( lastMState->covariance()( 0, 0 ) ) * halfOverLog10, type + "/xerrorAtLast",
            "10log(x error) at last measurement", -3, 2 );
      plot( log( lastMState->covariance()( 1, 1 ) ) * halfOverLog10, type + "/yerrorAtLast",
            "10log(y error) at last measurement", -3, 2 );
      plot( log( lastMState->covariance()( 2, 2 ) ) * halfOverLog10, type + "/txerrorAtLast",
            "10log(tx error) at last measurement", -7, 0 );
      plot( log( lastMState->covariance()( 3, 3 ) ) * halfOverLog10, type + "/tyerrorAtLast",
            "10log(ty error) at last measurement", -7, 0 );
      plot( log( lastMState->covariance()( 4, 4 ) ) * halfOverLog10, type + "/qoperrorAtLast",
            "10log(qop error) at last measurement", -8, 0 );
    }

    for ( const auto& info : track.extraInfo() ) {
      LHCb::Track::AdditionalInfo          infoName = LHCb::Track::AdditionalInfo( info.first );
      std::string                          title    = Gaudi::Utils::toString( infoName );
      const TrackMonitorMaps::InfoHistMap& histMap  = TrackMonitorMaps::infoHistDescription();
      auto                                 iterM    = histMap.find( infoName );
      if ( iterM != histMap.end() ) {
        const auto range = histMap.find( infoName )->second;
        plot( info.second, type + "/info/" + range.fid, title, range.fxMin, range.fxMax, 100 );
      }
    }

    const LHCb::RecVertices* pvcontainer = m_pvs.getIfExists();
    if ( pvcontainer ) {
      for ( auto ipv = pvcontainer->begin(); ipv != pvcontainer->end(); ++ipv ) {
        // Note: this is all already done in trackvertexmonitor!
        const LHCb::State* aState = track.stateAt( LHCb::State::Location::ClosestToBeam );
        if ( !aState ) aState = &( track.firstState() );
        double dz = ( *ipv )->position().z() - aState->z();
        double dx = aState->x() + dz * aState->tx() - ( *ipv )->position().x();
        double dy = aState->y() + dz * aState->ty() - ( *ipv )->position().y();
        plot( dx, type + "/IPx", "IPx", -1.0, 1.0, 100 );
        plot( dy, type + "/IPy", "IPy", -1.0, 1.0, 100 );
      }
    }
  }
}
