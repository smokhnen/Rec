/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IProfile1D.h"
#include "Event/FitNode.h"
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackKernel/TrackFunctors.h"

class TrackFitMatchMonitor : public Gaudi::Functional::Consumer<void( const LHCb::Track::Range& tracks ),
                                                                Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  enum ConstrainMethod { All = 0, QOverP = 1, Projective = 2 };

  /** Standard construtor */
  TrackFitMatchMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm initialize */
  StatusCode initialize() override;

  /** Algorithm execute */
  void operator()( const LHCb::Track::Range& tracks ) const override;

private:
  void plotDelta( const std::string& name, const LHCb::FitNode& node, bool upstream ) const;
  void myPlot1D( double x, const std::string& path, const std::string& title, double xmin, double xmax ) const;
  void myProfile1D( double x, double y, const std::string& path, const std::string& title, double xmin, double xmax,
                    size_t nbins ) const;
  void plotCurvatureMatch( const LHCb::Track& track ) const;

private:
  Gaudi::Property<int> m_constrainMethod{this, "ConstrainMethod", Projective};
  AIDA::IProfile1D*    m_curvatureRatioUToLongPr;
  AIDA::IProfile1D*    m_curvatureRatioVeloUTToLongPr;
  AIDA::IProfile1D*    m_curvatureRatioUToLongVsTxPos;
  AIDA::IProfile1D*    m_curvatureRatioVeloUTToLongVsTxPos;
  AIDA::IProfile1D*    m_curvatureRatioUToLongVsTxNeg;
  AIDA::IProfile1D*    m_curvatureRatioVeloUTToLongVsTxNeg;
  AIDA::IHistogram1D*  m_curvatureRatioUToLongH1;
  AIDA::IHistogram1D*  m_curvatureRatioVeloUTToLongH1;
  AIDA::IHistogram1D*  m_curvatureRatioUToLongPullH1;
  AIDA::IHistogram1D*  m_curvatureRatioVeloUTToLongPullH1;
  AIDA::IHistogram1D*  m_kickZH1;
  AIDA::IProfile1D*    m_kickZVsXPr;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFitMatchMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackFitMatchMonitor::TrackFitMatchMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"TrackContainer", LHCb::TrackLocation::Default}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackFitMatchMonitor::initialize() {
  setHistoTopDir( "Track/" );
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;        // error printed already

  setHistoTopDir( "Track/" );
  m_curvatureRatioUToLongPr =
      bookProfile1D( "curvatureRatioUToLongVsQoP", "curvature ratio T to Long versus q/p", -0.4, 0.4, 40 );
  m_curvatureRatioVeloUTToLongPr =
      bookProfile1D( "curvatureRatioVeloUTToLongVsQoP", "curvature ratio Velo-UT to Long versus q/p", -0.4, 0.4, 40 );
  m_curvatureRatioUToLongVsTxPos =
      bookProfile1D( "curvatureRatioUToLongVsTx", "curvature ratio T to Long versus tx for pos", -0.25, 0.25, 40 );
  m_curvatureRatioVeloUTToLongVsTxPos = bookProfile1D(
      "curvatureRatioVeloUTToLongVsTx", "curvature ratio Velo-UTT to Long versus tx for pos", -0.25, 0.25, 40 );
  m_curvatureRatioUToLongVsTxNeg =
      bookProfile1D( "curvatureRatioUToLongVsTx", "curvature ratio T to Long versus tx for neg", -0.25, 0.25, 40 );
  m_curvatureRatioVeloUTToLongVsTxNeg = bookProfile1D(
      "curvatureRatioVeloUTToLongVsTx", "curvature ratio Velo-UTT to Long versus tx for neg", -0.25, 0.25, 40 );

  m_curvatureRatioUToLongH1      = book1D( "curvatureRatioUToLong", "curvature ratio T to Long", 0, 2 );
  m_curvatureRatioVeloUTToLongH1 = book1D( "curvatureRatioVeloUTToLong", "curvature ratio Velo-UT to Long", 0, 2 );
  m_curvatureRatioUToLongPullH1  = book1D( "curvatureRatioUToLongPull", "curvature ratio T to Long pull", -5, 5 );
  m_curvatureRatioVeloUTToLongPullH1 =
      book1D( "curvatureRatioVeloUTToLongPull", "curvature ratio Velo-UT to Long pull", -5, 5 );

  m_kickZH1    = book1D( "kickZ", "Z position of magnet kick", 4900, 5400 );
  m_kickZVsXPr = bookProfile1D( "kickZVsX", "Z position of magnet kick versus x", -1500, 1500 );

  return sc;
}

//=============================================================================
inline void TrackFitMatchMonitor::myPlot1D( double x, const std::string& path, const std::string& title, double xmin,
                                            double xmax ) const {
  std::string id = path + "/" + title;
  plot( x, id, title, xmin, xmax );
}

inline void TrackFitMatchMonitor::myProfile1D( double x, double y, const std::string& path, const std::string& title,
                                               double xmin, double xmax, size_t nbins ) const {
  std::string id = path + "/" + title;
  profile1D( x, y, id, title, xmin, xmax, nbins );
}

void TrackFitMatchMonitor::plotDelta( const std::string& thisname, const LHCb::FitNode& node, bool upstream ) const {
  // TODO This is not supported for the moment.
  // It's the only place where the predicted state is requested.

  const LHCb::State& stateUp   = upstream ? node.filteredStateForward() : node.predictedStateForward();
  const LHCb::State& stateDown = upstream ? node.predictedStateBackward() : node.filteredStateBackward();

  // compute the difference
  Gaudi::TrackVector    delta = stateUp.stateVector() - stateDown.stateVector();
  Gaudi::TrackSymMatrix cov   = stateUp.covariance() + stateDown.covariance();

  // now, if we just look at the difference, then the problem is that
  // there are very large correlations: e.g. when you step through the
  // magnet, you don't know the momentum yet. so, if the momentum is
  // off, then so are x and tx. To solve this problem we compute a
  // 'constrained' difference: compute the difference constraining the
  // differenc ein other parameters to zero. There are two modes of operation:
  // a) constrain all 'other' variables (so for 'dx' constraint 'dy=dty=dtx=dqop=0')
  // b) constrain only qop
  // The results are different, mainly because there is something
  // wrong in the fitted tracks already: We find for MC tracks that
  // things don't match very well.

  Gaudi::TrackVector deltac, deltacpull;
  if ( m_constrainMethod == All ) {
    // now remove the contribution from the difference in the 'other' variables
    for ( size_t irow = 0; irow < 5; ++irow ) {
      // remove this row from delta and cov
      Gaudi::Vector4      subdelta, subcor;
      Gaudi::SymMatrix4x4 subcov;
      size_t              krow( 0 );
      for ( size_t jrow = 0; jrow < 5; ++jrow )
        if ( jrow != irow ) {
          subdelta( krow ) = delta( jrow );
          subcor( krow )   = cov( irow, jrow );
          size_t kcol( 0 );
          for ( size_t jcol = 0; jcol <= jrow; ++jcol )
            if ( jcol != irow ) {
              subcov( krow, kcol ) = cov( jrow, jcol );
              ++kcol;
            }
          ++krow;
        }

      // now invert the subcov
      subcov.Invert();
      // compute delta and its covariance
      Gaudi::Vector4 tmp = subcov * subcor;
      deltac( irow )     = delta( irow ) - ROOT::Math::Dot( subdelta, tmp );
      double covc        = cov( irow, irow ) - ROOT::Math::Dot( subcor, tmp );
      if ( covc > 0 )
        deltacpull( irow ) = deltac( irow ) / std::sqrt( covc );
      else
        warning() << "problem with covc: " << irow << " " << covc << " " << cov( irow, irow ) << " "
                  << ROOT::Math::Dot( subcor, tmp ) << endmsg;
    }
  } else {
    int map[4] = {2, 3, 0, 1};
    for ( size_t irow = 0; irow < 4; ++irow ) {
      int ref            = m_constrainMethod == QOverP ? 4 : map[irow];
      deltac( irow )     = delta( irow ) - cov( irow, ref ) / cov( ref, ref ) * delta( ref );
      double covc        = cov( irow, irow ) - cov( irow, ref ) / cov( ref, ref ) * cov( ref, irow );
      deltacpull( irow ) = deltac( irow ) / std::sqrt( covc );
    }
  }

  // these titles are only right if you choose 'Projective'
  myPlot1D( deltac( 0 ), thisname, "dx for dtx==0", -20, 20 );
  myPlot1D( deltac( 1 ), thisname, "dy for dty==0", -20, 20 );
  myPlot1D( deltac( 2 ), thisname, "dtx for dx==0", -0.010, 0.010 );
  myPlot1D( deltac( 3 ), thisname, "dty for dy==0", -0.010, 0.010 );
  // if(!m_constrainQoPOnly) plot(deltac(4),std::string("dqop"), -1e-4,1e-4) ;

  myPlot1D( deltacpull( 0 ), thisname, "dx pull", -10, 10 );
  myPlot1D( deltacpull( 1 ), thisname, "dy pull", -10, 10 );
  myPlot1D( deltacpull( 2 ), thisname, "dtx pull", -10, 10 );
  myPlot1D( deltacpull( 3 ), thisname, "dty pull", -10, 10 );
  // if(!m_constrainQoPOnly) plot(deltacpull(4),std::string("dqop pull"), -10,10) ;

  if ( std::abs( deltacpull( 0 ) ) < 5 ) {
    myProfile1D( node.state().tx(), deltacpull( 0 ), thisname, "dx pull vs tx", -0.25, 0.25, 20 );
    myProfile1D( node.state().tx(), deltac( 0 ), thisname, "dx vs tx", -0.25, 0.25, 20 );
    myProfile1D( node.state().ty(), deltacpull( 0 ), thisname, "dx pull vs ty", -0.25, 0.25, 20 );
    myProfile1D( node.state().qOverP() * Gaudi::Units::GeV, deltacpull( 0 ), thisname, "dx pull vs qop", -0.2, 0.2,
                 40 );
    myProfile1D( node.state().qOverP() * Gaudi::Units::GeV, deltac( 0 ), thisname, "dx vs qop", -0.2, 0.2, 40 );
    myProfile1D( node.state().x(), deltacpull( 0 ), thisname, "dx pull vs x", -2400, 2400, 48 );
    myProfile1D( node.state().x(), deltac( 0 ), thisname, "dx vs x", -2400, 2400, 48 );
    // profile1D(node.state().qOverP(),deltacpull(0), std::string("dx pull vs qop"), -4e-4,4e-4,20) ;
  }
  if ( std::abs( deltacpull( 1 ) ) < 5 ) {
    myProfile1D( node.state().tx(), deltacpull( 1 ), thisname, "dy pull vs tx", -0.25, 0.25, 20 );
    myProfile1D( node.state().ty(), deltacpull( 1 ), thisname, "dy pull vs ty", -0.25, 0.25, 20 );
    // profile1D(node.state().qOverP(),deltacpull(1), std::string("dy pull vs qop"), -4e-4,4e-4,20) ;
  }
  if ( std::abs( deltacpull( 2 ) ) < 5 ) {
    myProfile1D( node.state().x(), deltac( 2 ), thisname, "dtx vs x", -2400, 2400, 48 );
    myProfile1D( node.state().x(), deltacpull( 2 ), thisname, "dtx pull vs x", -2400, 2400, 48 );
    myProfile1D( node.state().qOverP() * Gaudi::Units::GeV, deltac( 2 ), thisname, "dtx vs qop", -0.2, 0.2, 40 );
    myProfile1D( node.state().qOverP() * Gaudi::Units::GeV, deltacpull( 2 ), thisname, "dtx pull vs qop", -0.2, 0.2,
                 40 );
  }
  if ( fullDetail() ) {
    if ( std::abs( deltacpull( 2 ) ) < 5 ) {
      myProfile1D( node.state().tx(), deltacpull( 2 ), thisname, "dtx pull vs tx", -0.25, 0.25, 20 );
      myProfile1D( node.state().ty(), deltacpull( 2 ), thisname, "dty pull vs ty", -0.25, 0.25, 20 );
      // profile1D(node.state().qOverP(),deltacpull(2), std::string("dtx pull vs qop"), -4e-4,4e-4,20) ;
    }
  }
}

void TrackFitMatchMonitor::operator()( const LHCb::Track::Range& tracks ) const {

  for ( const LHCb::Track* track : tracks ) {
    const auto* fit = fitResult( *track );
    if ( fit && fit->nodes().size() > 0 ) {
      plotCurvatureMatch( *track );

      const LHCb::FitNode *lastVelo( nullptr ), *firstUT( nullptr ), *lastUT( nullptr ), *firstT( nullptr );
      for ( const LHCb::FitNode* node : fit->nodes() )
        if ( node->hasMeasurement() ) {
          node->measurement().visit( [&]( const auto& arg ) {
            using arg_t = std::decay_t<decltype( arg )>;
            if constexpr ( std::is_base_of_v<LHCb::Measurement::VP, arg_t> ) {
              if ( !lastVelo || ( lastVelo->z() < node->z() ) ) lastVelo = node;
            } else if constexpr ( std::is_base_of_v<LHCb::Measurement::UT, arg_t> ) {
              if ( !firstUT || ( firstUT->z() > node->z() ) ) firstUT = node;
              if ( !lastUT || ( lastUT->z() < node->z() ) ) lastUT = node;
            } else if constexpr ( std::is_base_of_v<LHCb::Measurement::FT, arg_t> ) {
              if ( !firstT || ( firstT->z() > node->z() ) ) firstT = node;
            }
          } );
        }

      // take only tracks with VELO
      if ( lastVelo ) {

        // now split this between tracks with and tracks without UT
        if ( lastUT ) {

          // you can take either of these two: they give identical results.
          plotDelta( "Velo-UT", *firstUT, true );
          // plotDelta("Velo-UT",*lastVelo,false) ;

          if ( firstT ) plotDelta( "T-UT", *firstT, true );
          // plotDelta("T-UT",*lastUT,false) ;

        } else if ( firstT ) {
          plotDelta( "Velo-T", *firstT, true );
          // plotDelta("Velo-T",*lastVelo,false) ;
        }
      }
    }
  }
}

void TrackFitMatchMonitor::plotCurvatureMatch( const LHCb::Track& track ) const {
  // inspired by the problems we see in the field. see also UT field study
  auto* fit = fitResult( track );
  if ( track.hasT() && track.hasVelo() && track.hasUT() && std::abs( track.firstState().qOverP() ) > 0 ) {

    // first make sure that we have hits in all 3 T stations
    int hitsInStation[3] = {0, 0, 0};
    for ( LHCb::FitNode* node : fit->nodes() ) {
      if ( node->type() == LHCb::FitNode::Type::HitOnTrack ) {
        LHCb::LHCbID id = node->measurement().lhcbID();
        if ( id.isOT() ) {
          hitsInStation[id.otID().station() - 1] += 1;
        } else if ( id.isIT() ) {
          hitsInStation[id.stID().station() - 1] += 1;
        }
      }
    }

    if ( hitsInStation[0] >= 3 && hitsInStation[1] >= 3 && hitsInStation[2] >= 3 ) {

      // first get the 3 measurements of the curvature with error
      // nodes are sorted in decreasing z. find the nodes around the magnet
      LHCb::FitNode *nodeAfter( nullptr ), *nodeBefore( nullptr ), *firstNodeAfterT1( nullptr );
      for ( LHCb::FitNode* node : fit->nodes() ) {
        if ( node->z() > 5200 ) {
          if ( !nodeAfter || nodeAfter->z() > node->z() ) nodeAfter = node;
        } else {
          if ( !nodeBefore || nodeBefore->z() < node->z() ) nodeBefore = node;
        }
        if ( node->z() > 8100 )
          if ( !firstNodeAfterT1 || firstNodeAfterT1->z() > node->z() ) firstNodeAfterT1 = node;
      }

      // NOTE: we dont have the filtered states, so we take the
      // predicted state at the next node! for q/p this is okay. don't
      // do this for any of the other parameters!!

      double qop = nodeBefore->state().qOverP();
      double tx  = nodeBefore->state().tx();

      // extract the 'upstream' filtered state of T segment
      bool upstream = fit->nodes().front()->z() > fit->nodes().back()->z();

      LHCb::State stateT;
      LHCb::State stateVeloUT;

      if ( nodeAfter != nullptr and nodeBefore != nullptr ) {
        // TrackMasterFit
        stateT      = upstream ? nodeAfter->filteredStateForward() : nodeAfter->filteredStateBackward();
        stateVeloUT = upstream ? nodeBefore->filteredStateBackward() : nodeBefore->filteredStateForward();
      }

      double qopT    = stateT.qOverP();
      double qoperrT = std::sqrt( stateT.covariance()( 4, 4 ) );

      // extract the 'downstream' filtered state of Velo-UT segment
      double qopVeloUT    = stateVeloUT.qOverP();
      double qoperrVeloUT = std::sqrt( stateVeloUT.covariance()( 4, 4 ) );

      m_curvatureRatioUToLongH1->fill( qopT / qop );
      m_curvatureRatioVeloUTToLongH1->fill( qopVeloUT / qop );
      m_curvatureRatioUToLongPullH1->fill( ( qopT - qop ) / qoperrT );
      m_curvatureRatioVeloUTToLongPullH1->fill( ( qopVeloUT - qop ) / qoperrVeloUT );

      if ( std::abs( qopT / qop - 1 ) < 1 ) {
        m_curvatureRatioUToLongPr->fill( qop * Gaudi::Units::GeV, qopT / qop );
        if ( qop > 0 )
          m_curvatureRatioUToLongVsTxPos->fill( tx, qopT / qop );
        else
          m_curvatureRatioUToLongVsTxNeg->fill( tx, qopT / qop );
      }

      if ( std::abs( qopVeloUT / qop - 1 ) < 1 ) {
        m_curvatureRatioVeloUTToLongPr->fill( qop * Gaudi::Units::GeV, qopVeloUT / qop );

        if ( std::abs( qopT / qop - 1 ) < 1 ) m_curvatureRatioUToLongPr->fill( qop * Gaudi::Units::GeV, qopT / qop );
        if ( std::abs( qopVeloUT / qop - 1 ) < 1 )
          m_curvatureRatioVeloUTToLongPr->fill( qop * Gaudi::Units::GeV, qopVeloUT / qop );
      }

      if ( qop > 0 )
        m_curvatureRatioVeloUTToLongVsTxPos->fill( tx, qopVeloUT / qop );
      else
        m_curvatureRatioVeloUTToLongVsTxNeg->fill( tx, qopVeloUT / qop );

      // compute the (x,z) point of the intersection of the 2 segments for linear propagation
      // FIXME: it must be better to take a fixed z position in T.
      if ( 1 / std::abs( qop ) > 5 * Gaudi::Units::GeV ) {
        double zkick =
            ( stateVeloUT.z() * stateVeloUT.tx() - stateVeloUT.x() + stateT.x() - stateT.z() * stateT.tx() ) /
            ( stateVeloUT.tx() - stateT.tx() );
        double xkick = stateT.x() + ( zkick - stateT.z() ) * stateT.tx();
        //	  double xkickprime = stateVeloUT.x() + (zkick - stateVeloUT.z()) * stateVeloUT.tx() ;
        m_kickZH1->fill( zkick );
        if ( 5000 * Gaudi::Units::mm < zkick && zkick < 5300 * Gaudi::Units::mm ) m_kickZVsXPr->fill( xkick, zkick );
      }
    }
  }
}
