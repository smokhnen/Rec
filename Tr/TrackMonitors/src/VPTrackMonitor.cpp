/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ConditionAccessorHolder.h"
#include "Event/FitNode.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "Event/VPLightCluster.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Linker/LinkedTo.h"
#include "TrackKernel/TrackFunctors.h"
#include "VPDet/DeVP.h"

/** @class VPTrackMonitor VPTrackMonitor.h
 *
 *
 *  @author Christoph Hombach
 *  @date   2015-01-08
 */
class VPTrackMonitor
    : public Gaudi::Functional::Consumer<void( LHCb::Tracks const&, LHCb::VPLightClusters const&, DeVP const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeVP>> {
public:
  /// Standard constructor
  VPTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( LHCb::Tracks const&, LHCb::VPLightClusters const&,
                   DeVP const& ) const override; ///< Algorithm execution

private:
  Gaudi::Property<std::string> m_linkedHitsLocation{this, "LinkedHitsLocation",
                                                    LHCb::VPClusterLocation::Light + "2MCHits"};
};

namespace {
  //=============================================================================
  // Calculate the residuals.
  //=============================================================================
  Gaudi::XYZVector getResidual( const Gaudi::XYZPoint& clusterGlobal, const DeVPSensor& sens,
                                const LHCb::FitNode& fitnode ) {
    const LHCb::State     state = fitnode.unbiasedState();
    const Gaudi::XYZPoint trackInterceptGlobal( state.x(), state.y(), state.z() );
    const auto            trackInterceptLocal = sens.globalToLocal( trackInterceptGlobal );
    const auto            clusterLocal        = sens.globalToLocal( clusterGlobal );
    const double resx = trackInterceptLocal.x() - clusterLocal.x(); // comparing track_x in local with cluster_x_local
    const double resy = trackInterceptLocal.y() - clusterLocal.y(); // unbiased
    const double resz = trackInterceptLocal.z() - clusterLocal.z();
    return Gaudi::XYZVector( resx, resy, resz );
  }
} // namespace

DECLARE_COMPONENT( VPTrackMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPTrackMonitor::VPTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"TrackContainer", LHCb::TrackLocation::Default},
                KeyValue{"ClusterContainer", LHCb::VPClusterLocation::Light},
                KeyValue{"VPDetectorLocation", DeVPLocation::Default}}} {}

//=============================================================================
// Main execution
//=============================================================================
void VPTrackMonitor::operator()( const LHCb::Tracks& tracks, const LHCb::VPLightClusters& clusters,
                                 const DeVP& det ) const {

  const Gaudi::XYZPoint origin( 0., 0., 0. );

  // FIXME use of LinkedTo (direct use of evtSvc()) should be replaced
  auto links = LinkedTo<LHCb::MCHit, LHCb::VPLightCluster>( evtSvc(), msgSvc(), m_linkedHitsLocation );

  size_t tracknumber = 0;

  for ( const LHCb::Track* track : tracks ) { // start of track-loop
    // Skip tracks which are not VELO tracks and not long tracks.
    const bool bwd  = track->checkFlag( LHCb::Track::Flags::Backward );
    const auto type = track->type();
    if ( type != LHCb::Track::Types::Velo && type != LHCb::Track::Types::Long && !bwd ) continue;
    const bool fitted = track->checkFitStatus( LHCb::Track::FitStatus::Fitted );
    if ( !fitted ) continue;
    const double chi2NDOF  = track->chi2PerDoF();
    const double probChi2  = track->probChi2();
    const double ghostProb = track->ghostProbability();
    const double phi       = track->phi();
    const double eta       = track->pseudoRapidity();

    const double p  = track->p();
    const double pt = track->pt();

    const auto&  nodes_     = nodes( *track );
    unsigned int nClusters  = nodes_.size();
    size_t       nodenumber = 0;

    auto velonodenumber = std::count_if( std::begin( nodes_ ), std::end( nodes_ ), []( const LHCb::FitNode* node ) {
      return node->hasMeasurement() && node->type() == LHCb::FitNode::Type::HitOnTrack &&
             node->measurement().is<LHCb::Measurement::VP>();
    } );

    for ( const LHCb::FitNode* node : nodes_ ) { // start cluster loop
      if ( !node->hasMeasurement() ) continue;
      if ( node->type() != LHCb::FitNode::Type::HitOnTrack ) continue;
      // Skip non-VP measurements.
      if ( !node->measurement().is<LHCb::Measurement::VP>() ) continue;
      // Get the sensor.
      const LHCb::LHCbID id = ( node->measurement() ).lhcbID();

      auto iclus = std::find_if( begin( clusters ), end( clusters ),
                                 [vpx_id = id.vpID()]( const auto& c ) { return c.channelID() == vpx_id; } );
      if ( iclus == end( clusters ) ) throw Error( "fail to find cluster" );

      const LHCb::VPLightCluster* node_cluster = &( *iclus );

      // const size_t number_pixels = node_cluster->pixels().size();
      warning() << "FIXME: number of VP pixels to clusters always 1" << endmsg;
      const size_t number_pixels = 1;

      const DeVPSensor& sens = det.sensor( id.vpID() );

      const double x = node_cluster->x();
      const double y = node_cluster->y();
      const double z = node_cluster->z();

      // Get MC hit for this cluster and plot residuals
      const LHCb::MCHit* hit = links.first( node_cluster->channelID() );
      if ( !hit ) continue;
      // Get true track direction for this hit
      const double yangle = atan( hit->dydz() ) / Gaudi::Units::degree;
      const double xangle = atan( hit->dxdz() ) / Gaudi::Units::degree;
      const double theta  = sqrt( xangle * xangle + yangle * yangle );
      // Get hit position.
      const Gaudi::XYZPoint mchitPoint = hit->midPoint();

      // Calculate the truth-residuals.
      const double dx = x - mchitPoint.x();
      const double dy = y - mchitPoint.y();
      const double dz = z - mchitPoint.z();
      const double d3 = sqrt( dx * dx + dy * dy + dz * dz );

      const auto corner   = sens.localToGlobal( origin );
      const auto cluPos   = node->state().position();
      const auto residual = getResidual( cluPos, sens, *node );

      Tuple theTuple = nTuple( "VPTrackMonitor", "" );
      theTuple->column( "resX", residual.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "resY", residual.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "resZ", residual.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "nodeResidual", node->residual() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      // GLOBAL:
      theTuple->column( "clusX", cluPos.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "clusY", cluPos.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "clusZ", cluPos.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "node_X", x ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_Y", y ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_Z", z ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "number_pixels", number_pixels ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "bwd", bwd ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const LHCb::State state = node->unbiasedState();

      theTuple->column( "unbiasedNode_X", state.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "unbiasedNode_Y", state.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "unbiasedNode_Z", state.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      // const Gaudi::XYZPoint clusGlobal(cluPos.x(),cluPos.y(), cluPos.z());
      const auto clusLocal = sens.globalToLocal( cluPos );
      theTuple->column( "clusXLocal", clusLocal.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "clusYLocal", clusLocal.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "clusZLocal", clusLocal.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const Gaudi::XYZPoint nodeGlobal( x, y, z );
      const auto            nodeLocal = sens.globalToLocal( nodeGlobal );

      theTuple->column( "node_XLocal", nodeLocal.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_YLocal", nodeLocal.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_ZLocal", nodeLocal.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const Gaudi::XYZPoint node_unbiasedGlobal( state.x(), state.y(), state.z() );
      const auto            node_unbiasedLocal = sens.globalToLocal( node_unbiasedGlobal );
      theTuple->column( "node_XunbiasedLocal", node_unbiasedLocal.x() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_YunbiasedLocal", node_unbiasedLocal.y() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "node_ZunbiasedLocal", node_unbiasedLocal.z() )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "mchitX", mchitPoint.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // MC truth
                                                                                                            // particle
                                                                                                            // position
                                                                                                            // x
      theTuple->column( "mchitY", mchitPoint.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // MC truth
                                                                                                            // particle
                                                                                                            // position
                                                                                                            // y
      theTuple->column( "mchitZ", mchitPoint.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // MC truth
                                                                                                            // particle
                                                                                                            // position
                                                                                                            // z

      theTuple->column( "sensEdgeX", corner.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "sensEdgeY", corner.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "Error", node->errMeasure() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "module", sens.module() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "station", sens.station() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "isRight", sens.isRight() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "isLeft", sens.isLeft() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "phi", phi ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "eta", eta ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "theta", theta ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "xangle", xangle ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "yangle", yangle ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "chi2PerDoF", chi2NDOF ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "probChi2", probChi2 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "ghostProb", ghostProb ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "nClusters", nClusters ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "TrackType", type ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->column( "truth_resX", dx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "truth_resY", dy ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "truth_resZ", dz ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "truth_res3", d3 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "p", p ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "pt", pt ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "nodenumber", nodenumber ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "tracknumber", tracknumber ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      theTuple->column( "velonodenumber", velonodenumber ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      theTuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      nodenumber++;
    }

    tracknumber++;
  }
}
